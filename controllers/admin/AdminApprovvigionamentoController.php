<?php

class AdminApprovvigionamentoControllerCore extends AdminController
{
    public function __construct()
    {
        $this->displayWarning('WARNING: Page under construction!');
        $this->displayInformation('Testare calcolo parziali!');

        $this->bootstrap = true;
        $this->table = 'product';
        $this->lang = false;
        $this->explicitSelect = true;

        $this->allow_export = true;

        $this->context = Context::getContext();

        $this->_select = 'pl.name, m.name, s.name, a.quantity, a.stock_quantity, a.ordinato_quantity, a.scorta_minima,';
        $this->_select .= '
            IF(ord.ordinati > 0, ord.ordinati, 0) AS ordinati,
            IF((a.scorta_minima + ord.ordinati - a.stock_quantity - a.ordinato_quantity) > 0, a.scorta_minima + ord.ordinati - a.stock_quantity - a.ordinato_quantity, 0) AS da_ordinare
        ';
        
        $this->_join = '
            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (a.`id_product` = pl.`id_product`)
            LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (a.`id_manufacturer` = m.`id_manufacturer`)
            LEFT JOIN `'._DB_PREFIX_.'supplier` s ON (a.`id_supplier` = s.`id_supplier`)
        ';
        $this->_join .= '
            LEFT JOIN (
                SELECT o.id_cart, ca.riferimento, od.product_id, sum(product_quantity - (REPLACE(cons,"0_",""))) as ordinati
                FROM `'._DB_PREFIX_.'order_detail` od 
                    JOIN `'._DB_PREFIX_.'orders` o ON od.`id_order` = o.`id_order`
                    JOIN `'._DB_PREFIX_.'cart` ca ON o.`id_cart` = ca.`id_cart` 
                    JOIN `'._DB_PREFIX_.'order_history` oh
                        ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM `'._DB_PREFIX_.'order_history` moh WHERE moh.id_order_state != 18 AND moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) 
                    JOIN `'._DB_PREFIX_.'order_state` os ON (os.`id_order_state` = oh.`id_order_state`)
                WHERE os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16 AND os.id_order_state !=20 AND os.id_order_state !=26 AND os.id_order_state !=29 AND os.id_order_state !=30 AND os.id_order_state !=31 AND os.id_order_state !=32 AND os.id_order_state != 35 
                    AND ca.riferimento NOT LIKE "%BDL%"
                GROUP BY od.product_id
            ) ord ON (a.`id_product` = ord.`product_id`)
        ';

        $this->_where = '
            AND pl.id_lang = '.(int)$this->context->language->id.'
            AND a.fuori_produzione = 0
            AND (
                a.scorta_minima > 0 
                OR (a.scorta_minima > 0 AND ord.ordinati > 0) 
                OR (a.active = 1 AND a.ordinato_quantity > 0) 
                OR ((a.scorta_minima_amazon - a.impegnato_amazon) > 0)
                OR (a.scorta_minima > 0 AND ord.ordinati = 0) 
                OR (a.scorta_minima = 0 AND ord.ordinati > 0) 
                OR (a.scorta_minima > 0 AND ord.ordinati IS NULL) 
            )
        ';

        // Filtri: prodotti che hanno scorta minima e da ordinare al fornitore (pulsanti in list header)
        $this->_where .= '
            '.(Tools::getIsset('sm') && Tools::getValue('sm') == 's' ? ' AND a.scorta_minima > 0 ' : '').'
            '.(Tools::getIsset('do') && Tools::getValue('do') == 's' ? ' AND (a.scorta_minima + ord.ordinati - a.stock_quantity - a.ordinato_quantity) > 0 ' : '').'
        ';

        $this->_group = 'GROUP BY a.id_product';

        $this->_orderBy = 'a.reference';
        $this->_orderWay = 'ASC';

        $prodotti = Db::getInstance()->executeS('
            SELECT pl.name, CONCAT(p.reference, " - ", pl.name) as stringa_nome
            FROM '._DB_PREFIX_.'product p
            JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product = pl.id_product 
            WHERE pl.id_lang = '.$this->context->language->id.'
            ORDER BY name ASC
        ');

        $productsList = array();
        foreach($prodotti as $prodotto){
            $productsList = $productsList + array($prodotto['name'] => $prodotto['stringa_nome']);
        }

        $this->fields_list = array(
            'reference' => array(
                'title' => $this->l('Codice'), 
                'align' => 'left',
                'remove_onclick' => true
            ),
            'prodotto' => array(
                'title' => $this->l('Prodotto'),
                'align' => 'left', 
                'type' => 'select',
                'list' => $productsList,
                'select2' => true, // parametro aggiunto in helpers/list/list_header.tpl
                'filter_key' => 'pl!name',
                'callback' => 'cleanName',
                'remove_onclick' => true
            ),
            'costruttore' => array(
                'title' => $this->l('Costruttore'),
                'align' => 'left', 
                'filter_key' => 'm!name',
                'remove_onclick' => true
            ),
            'fornitore' => array(
                'title' => $this->l('Fornitore'),
                'align' => 'left', 
                'filter_key' => 's!name',
                'remove_onclick' => true
            ),
            'stock_quantity' => array(
                'title' => $this->l('Mag. EZ'),
                'align' => 'center',
                'remove_onclick' => true
            ), // TESTARE CON PARZIALI!
            'ordinati' => array(
                'title' => $this->l('Impegnati'),
                'align' => 'center',
                'callback' => 'printParziale', // correggere: farlo nella query o filtrare e ordinare per valore calcolato e non per value
                'remove_onclick' => true
            ),
            'scorta_minima' => array(
                'title' => $this->l('Scorta min.'),
                'align' => 'center',
                'callback' => 'printRightIcon',
                'remove_onclick' => true
            ),
            'da_ordinare' => array(
                'title' => $this->l('Da ordinare'),
                'align' => 'center',
                'search' => false,
                'callback' => 'printColor', // TESTARE CON PARZIALI!
                'remove_onclick' => true
            ),
            'supplier_quantity' => array(
                'title' => $this->l('Allnet'),
                'align' => 'center',
                'remove_onclick' => true
            ),
            'esprinet_quantity' => array(
                'title' => $this->l('Esprinet'),
                'align' => 'center',
                'remove_onclick' => true
            ),
            'itancia_quantity' => array(
                'title' => $this->l('Itancia'),
                'align' => 'center',
                'remove_onclick' => true
            ),
            'quantity' => array(
                'title' => $this->l('Mag. Tot.'),
                'align' => 'center',
                'remove_onclick' => true
            ),
            'ordinato_quantity' => array(
                'title' => $this->l('Ordinato'),
                'align' => 'center',
                'remove_onclick' => true
            ),
            'id_product' => array(
                'title' => $this->l('Ordini associati'),
                'align' => 'center',
                'callback' => 'printOrdini',
                'orderby' => false,
                'search' => false,
                'remove_onclick' => true
            ),
        );

        $this->_use_found_rows = true;

        parent::__construct();
    }

    public function renderList()
    {
        $this->addjQueryPlugin(array(
            'autocomplete',
            'select2',
        ));

        $this->addJS(_PS_JS_DIR_.'jquery/plugins/select2/select2_locale_'.$this->context->language->iso_code.'.js');
        
        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'select2_ready.js');

        return parent::renderList();
    }

    public function initToolbarTitle()
    {
        parent::initToolbarTitle();

        array_pop($this->toolbar_title);
        $this->toolbar_title[] = sprintf($this->l('Approvvigionamento'));
        
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }
    
    public function initToolbar()
    {
        parent::initToolbar();

        unset($this->toolbar_btn['new']); // Toglie il pulsante ADD NEW dalla toolbar
    }

    public function cleanName($value)
    {
        return preg_replace('/"/', "", $value);  
    }

    public function printRightIcon($value, $tr)
    {
        $quantita = $tr['stock_quantity'] - $tr['ordinati'] + $tr['ordinato_quantity'];

        if($value > 0 && $value > $quantita)
            $stringa = '<span>'.$value.' <i class="icon-warning-sign" style="color:red" title="Sotto scorta"></i></span>';
        else if($value > 0 && $tr['ordinato_quantity'] > 0 && $value < $quantita)
            $stringa = '<span>'.$value.' <i class="icon-truck" style="color:green" title="In arrivo"></i></span>'; // Oppure <img src='../img/admin/delivery.gif'>

        return $stringa;
    }

    // TESTARE CON PARZIALI!
    public function printColor($value, $tr)
    {
        // Consegne parziali (ricalcolo ordinati e da_ordinare)
        //$ordinati = $this->printParziale($value, $tr);
        //$da_ordinare = $value - $tr['ordinati'] + $ordinati;
        $da_ordinare = $value;

        return ($da_ordinare > 0 ? '<div style="height:100%; background-color:#fff0c7">'.$da_ordinare.'</div>' : $da_ordinare);
    }

    public function printOrdini($value)
    {
        // Correggere: stati ordine diversi da quelli della query sopra!
        $ordini_associati = Db::getInstance()->executeS("
            SELECT o.id_order AS ordine, o.id_customer 
            FROM "._DB_PREFIX_."order_detail od 
                JOIN "._DB_PREFIX_."orders o ON od.id_order = o.id_order 
                JOIN "._DB_PREFIX_."order_history oh ON (oh.id_order_history = (SELECT MAX( id_order_history ) FROM "._DB_PREFIX_."order_history moh WHERE moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order )) 
                JOIN "._DB_PREFIX_."order_state os ON (os.id_order_state = oh.id_order_state) 
            WHERE os.id_order_state !=4 AND os.id_order_state !=5 AND os.id_order_state !=6 AND os.id_order_state !=7 AND os.id_order_state !=8 AND os.id_order_state !=14 AND os.id_order_state !=16  AND os.id_order_state !=20 AND os.id_order_state !=26 AND os.id_order_state !=29 AND os.id_order_state !=31 AND os.id_order_state !=32 
                AND od.product_id = ".$value
        );

        $lunghezza_array = count($ordini_associati);
        $i = 0;
        
        foreach($ordini_associati as $ordine_associato) {
            $i++;
            $fatt = Db::getInstance()->getValue('SELECT rif_ordine FROM '._DB_PREFIX_.'fattura WHERE rif_ordine = "'.$ordine_associato['ordine'].'"');
            if($fatt > 0) { 
                // niente
            }
            else {
                $href = '/ezadmin/index.php?controller=AdminOrders&id_order='.$ordine_associato['ordine'].'&vieworder&token='.Tools::getAdminTokenLite('AdminOrders');
                $stringa .= '<a href="'.$href.'" target="_blank">'.$ordine_associato['ordine'].($i == $lunghezza_array ? "</a>" : "</a> - ").'';
            }
        }

        return $stringa;  
    }

    // TESTARE!
    // Consegne parziali (ricalcolo ordinati)
    public function printParziale($value, $tr)
    {
        $ordinati = $value;

        $parziali = Db::getInstance()->executeS('
            SELECT o.id_order, product_quantity 
            FROM '._DB_PREFIX_.'order_detail od 
                JOIN '._DB_PREFIX_.'orders o ON o.id_order = od.id_order 
                JOIN '._DB_PREFIX_.'order_history oh ON (oh.id_order_history = (
                    SELECT MAX(id_order_history) 
                    FROM '._DB_PREFIX_.'order_history moh 
                    WHERE moh.id_order_state != 18 AND moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order 
                    GROUP BY moh.id_order)
                ) 
                JOIN '._DB_PREFIX_.'order_state os ON (os.id_order_state = oh.id_order_state) 
            WHERE os.id_order_state = 15
                AND od.product_id = '.$tr['id_product'].'
        ');
        
        foreach($parziali as $parziale)
            $ordinati -= $parziale['product_quantity'];
            // $ordinati -= ($parziale['product_quantity'] - $parziale['ordinati']); // Correggere: nella 1.4 è così, ma ordinati non esiste. Serve?

        return $ordinati;
    }
}