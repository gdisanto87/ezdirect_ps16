<?php

class AdminCercaSerialiControllerCore extends AdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->lang = false;

        $this->display = 'view';

        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Cerca seriali'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }

    public function renderView()
    {
        if(Tools::getIsset('cercaseriale')){

            $seriali = Db::getInstance()->executeS('
                SELECT dtp.seriale, o.* 
                FROM dati_tecnici_prodotti dtp 
                    JOIN '._DB_PREFIX_.'order_detail od ON dtp.id_order_detail = od.id_order_detail 
                    JOIN '._DB_PREFIX_.'orders o ON o.id_order = od.id_order 
                WHERE seriale LIKE "%'.Tools::getValue('seriale').'%"
            ');

        }

        $this->tpl_view_vars = array(
            'seriali' => $seriali,
        );
		
        return parent::renderView();
    }
}