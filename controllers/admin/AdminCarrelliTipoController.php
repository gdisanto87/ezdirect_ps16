<?php

class AdminCarrelliTipoControllerCore extends AdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->context = Context::getContext();

        Tools::redirectAdmin($this->context->link->getAdminLink('AdminCustomers').'&id_customer=44431&viewcustomer&tab_name=carts');
        
        parent::__construct();
    }
}