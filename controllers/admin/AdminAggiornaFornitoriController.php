<?php

class AdminAggiornaFornitoriControllerCore extends AdminController
{
    public function __construct()
    {        
        $this->bootstrap = true;
        $this->lang = false;

        $this->display = 'view';

        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Aggiorna fornitori'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }
    
    public function renderView()
    {
        $updated = false;

        if(Tools::getIsset('vai-costruttore'))
        {
            $id_costruttore_selezionato = Tools::getValue('vai-costruttore');
            
            $costruttore_selezionato = Db::getInstance()->getValue('
                SELECT name 
                FROM '._DB_PREFIX_.'manufacturer 
                WHERE id_manufacturer = '.$id_costruttore_selezionato
            );

            $fornitori = Db::getInstance()->executeS('
                SELECT id_supplier, name
                FROM '._DB_PREFIX_.'supplier 
                ORDER BY name ASC
            ');

            // Fornitore principale
			$id_fornitore = Db::getInstance()->getValue('
                SELECT supplier 
                FROM '._DB_PREFIX_.'manufacturer 
                WHERE id_manufacturer = '.$id_costruttore_selezionato
            );

            $altri_fornitori = Db::getInstance()->getValue('
                SELECT other_suppliers 
                FROM '._DB_PREFIX_.'manufacturer 
                WHERE id_manufacturer = '.$id_costruttore_selezionato
            );
            $altri_fornitori = unserialize($altri_fornitori);
        }
        else if(Tools::getIsset('vai-fornitori')) 
        {
            $id_supplier = Tools::getValue('fornitore-principale');
            $id_manufacturer = Tools::getValue('vai-fornitori');
            
            Db::getInstance()->execute("
                UPDATE "._DB_PREFIX_."manufacturer 
                SET supplier = ".$id_supplier."
                WHERE id_manufacturer = ".$id_manufacturer
            );
            
            Db::getInstance()->execute("
                UPDATE "._DB_PREFIX_."product 
                SET id_supplier = ".$id_supplier."
                WHERE id_manufacturer = ".$id_manufacturer
            );
            
            $other_suppliers = array();

            foreach ($_POST['other_suppliers'] as $supplier) {
                $other_suppliers[] = $supplier;
            }
            $other_suppliers = serialize($other_suppliers);
            
            Db::getInstance()->execute("
                UPDATE "._DB_PREFIX_."manufacturer 
                SET other_suppliers = '".$other_suppliers."' 
                WHERE id_manufacturer = ".$id_manufacturer
            );
            
            Db::getInstance()->execute("
                UPDATE "._DB_PREFIX_."product 
                SET other_suppliers = '".$other_suppliers."' 
                WHERE id_manufacturer = ".$id_manufacturer
            );

            $costruttori = Db::getInstance()->executeS('
                SELECT id_manufacturer, name
                FROM '._DB_PREFIX_.'manufacturer 
                ORDER BY name ASC
            ');

            $updated = true;
        }
        else
        {
            $costruttori = Db::getInstance()->executeS('
                SELECT id_manufacturer, name
                FROM '._DB_PREFIX_.'manufacturer 
                ORDER BY name ASC
            ');
        }

        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'override_aggiorna_fornitori.js');

        $this->addjQueryPlugin(array(
            'autocomplete',
            'select2',
        ));

        $this->addJS(_PS_JS_DIR_.'jquery/plugins/select2/select2_locale_'.$this->context->language->iso_code.'.js');
        
        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'select2_ready.js');
        $this->addJS(_PS_BO_DEFAULT_THEME_DIR_.'plugins/TaTa/dist/tata.js');

        $this->tpl_view_vars = array(
            'costruttori' => $costruttori,
            'id_costruttore_selezionato' => $id_costruttore_selezionato,
            'costruttore_selezionato' => $costruttore_selezionato,
			'fornitori' => $fornitori,
			'id_fornitore' => $id_fornitore,
			'altri_fornitori' => $altri_fornitori,
			'updated' => $updated,
        );

        return parent::renderView();
    }
}