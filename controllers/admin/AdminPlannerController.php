<?php

class AdminPlannerControllerCore extends AdminController
{
    public function __construct()
    {
		$this->displayWarning('Correggere sample.php e fare azioni staff; correggere select di ricerca di datatables: posso aggiungere dei valori manualmente o impostare una option default al caricamento? Devo poter selezionare aperto + in lavorazione');

        $this->bootstrap = true;
        $this->lang = false;

        $this->context = Context::getContext();

		$this->display = 'view';

        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Planner'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }

    public function renderView()
    {
        /* TPL:
        1. Ultime attività (fare in un file a parte da includere ovunque serva)
        2-1. (tab sx) Planner e azioni clienti
        2-2. (tab dx) Azioni staff
        */
        
        $id_employee = $this->context->employee->id;
        $employee = $this->context->employee->firstname;
        
        // true: può vedere i planner dei tecnici
        //$check_employee = (($id_employee == 4 || $id_employee == 5 || $id_employee == 6 || $id_employee == 12 || $id_employee == 17 || $id_employee == 22 || $id_employee == 26) ? true : false);
        $check_employee = 1; // prova!
        $is_agente = ($this->context->employee->id_profile == 7 ? true : false); // true: l'employee loggato è un agente

        /* Opzioni */

        // Azioni
        // Correggere!
        $link_azione = '/ezadmin/index.php?controller=AdminPlanner&id_employee='.$id_employee.'&viewaction&aprinuovaazione&token='.$this->token;

        $todo_li = array(
            array(
                'tipo' => 'Attivita',
                'icona' => 'attivita',
                'title' => 'Attivita',
                'nome' => 'Attivit&agrave;'
            ),
            array(
                'tipo' => 'Telefonata',
                'icona' => 'telefonata',
                'title' => 'Telefonata',
                'nome' => 'Telefonata'
            ),
            array(
                'tipo' => 'Visita',
                'icona' => 'visita',
                'title' => 'Visita',
                'nome' => 'Visita'
            ),
            array(
                'tipo' => 'Caso',
                'icona' => 'caso',
                'title' => 'Caso',
                'nome' => 'Caso'
            ),
            array(
                'tipo' => 'Intervento',
                'icona' => 'intervento',
                'title' => 'Intervento',
                'nome' => 'Intervento'
            ),
            array(
                'tipo' => 'Richiesta_RMA',
                'icona' => 'richiesta_rma',
                'title' => 'Richiesta RMA',
                'nome' => 'Richiesta RMA'
            ),
        );

        /* Planner */
        
        // Correggere: Gestito in ajax nella 1.4 ma basterebbe un tasto collapse
        $hidden_planner = (Db::getInstance()->getValue('SELECT value FROM fixed_ck WHERE id_employee = '.$id_employee.' AND name="hidden_planner"'));
        $hidden_planner = 0;

        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'datatable_ready.js');
        $this->addJS(_PS_BO_DEFAULT_THEME_DIR_.'plugins/DataTables/datatables.min.js');
        $this->addCSS(_PS_BO_DEFAULT_THEME_DIR_.'plugins/DataTables/datatables.min.css');

        /* TEST Ultime attività */
        $ultime_attivita = $this->inserisciUltimeAttivita();

        /* Le mie attività */
        $mie_att = $this->inserisciLeMieAttivita(); // uso datatables per ordinamento e ricerca (assegnare id # e non classe)

        // Variabili per i filtri di ricerca di $mie_att -> correggere: come usarle in datatables?

        $the_employees = Db::getInstance()->executeS('
            SELECT `id_employee`, CONCAT(firstname," ",LEFT(lastname, 1),".") AS "name"
            FROM `'._DB_PREFIX_.'employee`
            WHERE `active` = 1
            ORDER BY `email`
        ');
		
        // Gli agenti possono vedere solo le proprie attività
		if($is_agente){
			$the_employees = Db::getInstance()->executeS('
				SELECT `id_employee`, CONCAT(firstname," ",LEFT(lastname, 1),".") AS "name"
				FROM `'._DB_PREFIX_.'employee`
				WHERE `id_employee` = '.$id_employee.'
			');
		}
		
        // in carico a
		$contactEmployees = array();
		foreach($the_employees as $employee) 
			$contactEmployees[$employee['id_employee']] = $employee['name'];
		
        // aperto da
		$contactEmployeesFirstOpen = array();
		foreach($the_employees as $employee) 
			$contactEmployeesFirstOpen[$employee['id_employee']] = $employee['name'];
		
		$contactEmployeesFirstOpen[0] = 'Cliente';
		
        // tipo azione
		$contactArray = array();
		$contacts = Contact::getContacts($this->context->language->id);
		foreach ($contacts AS $contact)
			$contactArray[$contact['id_contact']] = $contact['name'];
			
        // stato
		$statusArray = array(
			'open' => $this->l('Open'),
			'closed' => $this->l('Closed'),
			'pending1' => $this->l('In lavorazione'),
			'pen' => $this->l('Aperto + In lavorazione')
		);
		
        // icona stato
		$imagesArray = array(
			'open' => '<i class="icon-circle" style="color:red" title="Aperto" /></i>', // data-sort: 0
			'closed' => '<i class="icon-circle" style="color:green" title="Chiuso" /></i>', // data-sort: 2
			'pending1' => '<i class="icon-circle" style="color:orange" title="In lavorazione" /></i>' // data-sort: 1
		);
		
		$contactArray['Telefonata'] = 'Telefonata';
		$contactArray['Visita'] = 'Visita';
		$contactArray['Attivita'] = 'Attivita';
		$contactArray['Caso'] = 'Caso';
		$contactArray['Intervento'] = 'Intervento';
		$contactArray['preventivo'] = 'Commerciale';
		$contactArray['Richiesta_RMA'] = 'Richiesta RMA';
		$contactArray['tirichiamiamonoi'] = 'Ti richiamiamo noi';
		$contactArray['TODO Amazon'] = 'TODO Amazon';
		$contactArray['TODO Social'] = 'TODO Social';
		$contactArray['TODO Sendinblue'] = 'TODO Sendinblue';

        /* Ricerca testo nel ticket */
        $cercaticket = $_POST['cercaticket'];

        $this->tpl_view_vars = array(
            'id_employee' => $id_employee,
            'employee' => $employee,
            'check_employee' => $check_employee,
            'hidden_planner' => $hidden_planner,
            'link_azione' => $link_azione,
            'todo_li' => $todo_li,
            // TEST ULTIME ATTIVITA'
            'ultime_attivita' => $ultime_attivita,
            // LE MIE ATTIVITA'
            'mie_att' => $mie_att,
            // Ricerca testo nel ticket
            'cercaticket' => $cercaticket,
        );

        return parent::renderView();
    }

    public function postProcess()
    {
        // Cambia status dal listing
        if(Tools::getIsset('cambiastatus')) 
        {
			if(Tools::getValue('tipo') == 'ticket') { // ticket e messaggi
				Db::getInstance()->execute("UPDATE "._DB_PREFIX_."customer_thread SET status = '".Tools::getValue('cambiastatus')."' WHERE id_customer_thread = '".Tools::getValue('id_thread')."'");
                $tipo_azione = 'T';
			}
			else if(Tools::getValue('tipo') == 'preventivo' || Tools::getValue('tipo') == 'tirichiamiamonoi' ) {
				Db::getInstance()->execute("UPDATE form_prevendita_thread SET status = '".Tools::getValue('cambiastatus')."' WHERE id_thread = '".Tools::getValue('id_thread')."'");
                $tipo_azione = 'P';
			}
			else if(Tools::getValue('tipo') == 'to-do') {
				Db::getInstance()->execute("UPDATE action_thread SET status = '".Tools::getValue('cambiastatus')."' WHERE id_action = '".Tools::getValue('id_thread')."'");
				
				$subject = Db::getInstance()->getValue('SELECT subject FROM action_thread WHERE id_action = '.Tools::getValue('id_thread'));
				$current_action_status = Db::getInstance()->getValue('SELECT status FROM action_thread WHERE id_action = '.Tools::getValue('id_thread'));
			
				if(substr($subject,0,20) == '*** VERIFICA TECNICA')
				{	
					if(Tools::getValue('cambiastatus') == 'closed'  && $current_action_status != 'closed')
						Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$this->context->employee->id.', '.substr($subject, -6,5).', 25, "'.date('Y-m-d H:i:s').'")');
					else if((Tools::getValue('cambiastatus') == 'open' || Tools::getValue('cambiastatus') == 'pending1') && $current_action_status == 'closed')
						Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$this->context->employee->id.', '.substr($subject, -6,5).', 24, "'.date('Y-m-d H:i:s').'")');
				}
				
				Customer::chiudiOrdineDaAzione(Tools::getValue('id_thread'));
                $tipo_azione = 'A';
			}
			else if(Tools::getValue('tipo') == 'to-do-employee') {
				Db::getInstance()->execute("UPDATE action_thread_employee SET status = '".Tools::getValue('cambiastatus')."' WHERE id_action = '".Tools::getValue('id_thread')."'");
			}

            switch(Tools::getValue('cambiastatus'))
            {
                case 'open': $switch_status = 3; break;
                case 'pending1': $switch_status = 4; break;
                case 'closed': $switch_status = 5; break;
                default: $switch_status = 10; break;
            }

            Customer::Storico(Tools::getValue('id_thread'), $tipo_azione, $this->context->employee->id, $switch_status);
			Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.$this->token);
		}
		
        // Cambia in carico dal listing
		if(Tools::getIsset('cambiaincarico')) 
        {
			Customer::cambiaIncaricoVeloce(Tools::getValue('tipo'), Tools::getValue('cambiaincarico'), Tools::getValue('id_thread'), Tools::getValue('precedente_incarico'), Tools::getValue('id_ticket_univoco'), Tools::getValue('id_customer'));
			Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.$this->token);
		}

        // Delete ticket / azioni cliente / to-do / to-do employee
        if(isset($_GET['del']) && isset($_GET['typedel'])) 
        {
			if(is_numeric($_GET['typedel']) || $_GET['typedel'] == 'messaggio' || $_GET['typedel'] == 'ticket') 
            {
				Db::getInstance()->execute("DELETE "._DB_PREFIX_."customer_thread, "._DB_PREFIX_."customer_message FROM "._DB_PREFIX_."customer_thread JOIN "._DB_PREFIX_."customer_message ON "._DB_PREFIX_."customer_message.id_customer_thread = "._DB_PREFIX_."customer_thread.id_customer_thread WHERE "._DB_PREFIX_."customer_thread.id_customer_thread = ".$_GET['id_customer_thread']."");
				Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."customer_thread WHERE id_customer_thread = ".$_GET['id_customer_thread']."");
				
				if(Tools::getValue('typedel') == 9) { // RMA
					Db::getInstance()->execute("DELETE FROM rma WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
				}
			}
			else if($_GET['typedel'] == 'preventivo' || $_GET['typedel'] == 'tirichiamiamonoi') 
            {
				Db::getInstance()->execute("DELETE form_prevendita_thread, form_prevendita_message FROM form_prevendita_thread JOIN form_prevendita_message ON form_prevendita_message.id_thread = form_prevendita_thread.id_thread WHERE form_prevendita_thread.id_thread = ".$_GET['id_customer_thread']."");
				Db::getInstance()->execute("DELETE FROM form_prevendita_thread WHERE form_prevendita_thread.id_thread = ".$_GET['id_customer_thread']."");
			}
			else if(is_numeric($_GET['typedel']) || $_GET['typedel'] == 'to-do-employee') 
            {
				Db::getInstance()->execute("DELETE action_thread_employee, action_message_employee FROM action_thread_employee JOIN action_message_employee ON action_message_employee.id_action = action_thread_employee.id_action WHERE action_thread_employee.id_action = ".$_GET['id_employee_thread']."");
				Db::getInstance()->execute("DELETE FROM action_thread WHERE action_thread.id_action = ".$_GET['id_customer_thread']."");
				Db::getInstance()->execute("DELETE FROM ticket_promemoria WHERE tipo = 'ae' AND msg = ".$_GET['id_employee_thread']."");
				
                // correggere dopo aver fatto view di to-do employee
                Tools::redirectAdmin(self::$currentIndex.'&id_employee='.Tools::getValue('id_employee').'&updateemployee&tab-container-1=3&conf=1&token='.$this->token);
			}
			else 
            {
				Db::getInstance()->execute("DELETE action_thread, action_message FROM action_thread JOIN action_message ON action_message.id_action = action_thread.id_action WHERE action_thread.id_action = ".$_GET['id_customer_thread']."");
				Db::getInstance()->execute("DELETE FROM action_thread WHERE action_thread.id_action = ".$_GET['id_customer_thread']."");
				Db::getInstance()->execute("DELETE FROM ticket_promemoria WHERE tipo = 'a' AND msg = ".$_GET['id_customer_thread']."");
			}

            if(Tools::getIsset('backtocustomers')) {
				Tools::redirectAdmin('index.php?controller=AdminCustomers&id_customer='.Tools::getValue('backtocustomers').'&viewcustomer&tab_name='.Tools::getValue('tab_name').'&conf=1&token='.Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$this->context->employee->id));
			}
            else if(Tools::getIsset('backtorma')) {
				Tools::redirectAdmin('index.php?controller=AdminRMA&conf=1&token='.Tools::getAdminToken('AdminRMA'.(int)Tab::getIdFromClassName('AdminRMA').(int)$this->context->employee->id));
            }
			else {
				Tools::redirectAdmin(self::$currentIndex.'&conf=1&token='.$this->token);
			}
		}

        // Cambia status dei to-do employee (da dove?)
        if(Tools::getIsset('cambiastatusae_listing')) 
        {	
            Db::getInstance()->ExecuteS("UPDATE action_thread_employee SET status = '".Tools::getValue('cambiastatusae_listing')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_action = '".addslashes(Tools::getValue('id_action'))."'");

            switch(Tools::getValue('cambiastatusae_listing'))
            {
                case 'open': $switch_status = 3; break;
                case 'pending1': $switch_status = 4; break;
                case 'closed': $switch_status = 5; break;
                default: $switch_status = 10; break;
            }	
            
            // correggere link
            Customer::Storico(Tools::getValue('id_action'), 'AE', $this->context->employee->id, $switch_status);
            Tools::redirectAdmin($currentIndex.'&id_employee='.$this->context->employee->id.'&updateemployee&tab-container-1=3&conf=4&token='.$this->token);
        }
        
        // Cambia in carico dei to-do employee (da dove?)
        if(Tools::getIsset('cambiaincaricoae')) 
        {
            // correggere link
            Customer::cambiaIncaricoVeloce(Tools::getValue('tipo'), Tools::getValue('cambiaincaricoae'), Tools::getValue('id_thread'), Tools::getValue('precedente_incarico'), Tools::getValue('id_ticket_univoco'), Tools::getValue('id_employee'));
            Tools::redirectAdmin(self::$currentIndex.'&id_employee='.$this->context->employee->id.'&updateemployee&tab-container-1=3&conf=4&token='.$this->token);
        }

        // Reset ricerca testo nel ticket
        if(Tools::getIsset('vairesetcerca'))
        {
            unset($_POST['cercaticket']);
        }

        return parent::postProcess();
    }

    // CORREGGERE: Spostare in una classe
    public function inserisciUltimeAttivita()
    {
        if(!$this->context)
            $context = Context::getContext();
        else
            $context = $this->context;
        
        $is_agente = ($context->employee->id_profile == 7 ? true : false); // true: l'employee loggato è un agente

        if(!$is_agente)
        {
            $ultime_attivita_impiegato_group = Db::getInstance()->executeS('
                SELECT DISTINCT id_attivita 
                FROM storico_attivita 
                WHERE id_employee > 0 
                ORDER BY data_attivita DESC 
                LIMIT 20
            ');
            
            $ultime_att_gr = '(';
            foreach($ultime_attivita_impiegato_group as $u)
                $ultime_att_gr .= $u['id_attivita'].',';

            $ultime_att_gr .= '9999999999999)';		
            $ultime_attivita_impiegato = Db::getInstance()->executeS('
                SELECT * 
                FROM (
                    SELECT * 
                    FROM storico_attivita 
                    WHERE tipo_attivita != "AE" 
                        AND id_attivita IN '.$ultime_att_gr.' 
                    ORDER BY data_attivita DESC 
                    LIMIT 20
                ) auz 
                GROUP BY id_attivita 
                ORDER BY data_attivita DESC 
            ');
            
            // classe "ultime-att" per paginazione in testa -> correggere: si può eliminare?
        
            // Correggere: bootstrappizzare; la variabile serve solo se usata come return di funzione
            // Correggere: Non posso usare datatables se voglio caricare correttamente le righe nascoste; per ora, uso un tooltip al posto di Espandi
            $content = '<div><table id="table-ultime-att" class="table"><thead><tr><th>Tipo attivit&agrave;</th><th>Id attivit&agrave;</th><th>Cliente</th><th>In carico a</th><th>Status</th><th data-sorter="shortDate" data-date-format="ddmmyyyy">Data attivit&agrave;</th></tr></thead><tbody>';
            
            $uaii = 0;
                
            foreach($ultime_attivita_impiegato as $uai)
            {
                if($uai['tipo_attivita'] == 'T')
                {
                    $id_contact = Db::getInstance()->getValue('
                        SELECT id_contact 
                        FROM '._DB_PREFIX_.'customer_thread 
                        WHERE id_customer_thread = '.$uai['id_attivita']
                    );

                    if($id_contact == 7)
                        $uai['tipo_attivita'] = 'M';
                }

                switch($uai['tipo_attivita'])
                {
                    case 'A': 
                        $tipo_attivita_uai = 'Azione'; 
                        $table = 'action_thread'; 
                        $attivita_key = 'id_action'; 
                        $id_attivita = 'TODO'.$uai['id_attivita']; 
                        break;
                    case 'P': 
                        $tipo_attivita_uai = 'Richiesta preventivo'; 
                        $table = 'form_prevendita_thread'; 
                        $attivita_key = 'id_thread'; 
                        $id_attivita = Customer::trovaSigla($uai['id_attivita'], 'P'); 
                        break;
                    case 'C': 
                        $tipo_attivita_uai = 'Carrello'; 
                        $table = _DB_PREFIX_.'cart'; 
                        $attivita_key = 'id_cart'; 
                        $id_attivita = $uai['id_attivita']; 
                        break;
                    case 'O': 
                        $tipo_attivita_uai = 'Ordine'; 
                        $table = _DB_PREFIX_.'orders'; 
                        $attivita_key = 'id_order'; 
                        $id_attivita = $uai['id_attivita']; 
                        break;
                    case 'L': 
                        $tipo_attivita_uai = 'BDL'; 
                        $table = _DB_PREFIX_.'bdl'; 
                        $attivita_key = 'id_bdl'; 
                        $id_attivita = 'BDL'.$uai['id_attivita']; 
                        break;
                    case 'T': 
                        $tipo_attivita_uai = 'Ticket'; 
                        $table = _DB_PREFIX_.'customer_thread'; 
                        $attivita_key = 'id_customer_thread'; 
                        $id_attivita = Customer::trovaSigla($uai['id_attivita'], 'T'); 
                        break;	
                    case 'M': 
                        $tipo_attivita_uai = 'Messaggio'; 
                        $table = _DB_PREFIX_.'customer_thread'; 
                        $attivita_key = 'id_customer_thread'; 
                        $id_attivita = Customer::trovaSigla($uai['id_attivita'], 'M'); 
                        break;	
                }
                
                $id_customer = Db::getInstance()->getValue('
                    SELECT id_customer 
                    FROM '.$table.' 
                    WHERE '.$attivita_key.' = '.$uai['id_attivita']
                );
                
                $uai_link = '/ezadmin/index.php?controller=AdminCustomers&id_customer='.$id_customer.'&viewcustomer';
                $tokenCustomers = Tools::getAdminTokenLite('AdminCustomers');
                $tokenCarts = Tools::getAdminTokenLite('AdminCarts');
                $tokenOrders = Tools::getAdminTokenLite('AdminOrders');
                
                $uai_employee_act = Db::getInstance()->getValue('
                    SELECT CONCAT(firstname," ",LEFT(lastname,1),".") impiegato 
                    FROM '._DB_PREFIX_.'employee 
                    WHERE id_employee = '.$uai['id_employee']
                );
                
                switch($uai['tipo_attivita'])
                {
                    case 'A': 
                        $uai_link .= '&tab_name=todo&azione=todo&viewaction&id_action='.$uai['id_attivita'].'&token='.$tokenCustomers; 
                        $uai_status = Db::getInstance()->getValue('SELECT status FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                        $in_carico_a = Db::getInstance()->getValue('SELECT action_to FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                        break;
                    case 'P': 
                        $uai_link .= '&tab_name=actions&azione=actions&viewpreventivo&id_thread='.$uai['id_attivita'].'&token='.$tokenCustomers; 
                        $uai_status = Db::getInstance()->getValue('SELECT status FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                        $in_carico_a = Db::getInstance()->getValue('SELECT id_employee FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                        break;
                    case 'C': 
                        $uai_link = '/ezadmin/index.php?controller=AdminCarts&id_cart='.$uai['id_attivita'].'&viewcart&token='.$tokenCarts; 
                        $uai_status = Db::getInstance()->getValue('SELECT visualizzato FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                        $in_carico_a = Db::getInstance()->getValue('SELECT in_carico_a FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                        break;
                    case 'O': 
                        $uai_link = '/ezadmin/index.php?controller=AdminOrders&id_order='.$uai['id_attivita'].'&vieworder&token='.$tokenOrders;
                        $uai_status = Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'order_state_lang WHERE id_lang = '.$context->language->id.' AND id_order_state = (SELECT id_order_state FROM '._DB_PREFIX_.'order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `'._DB_PREFIX_.'order_history` moh WHERE moh.`id_order` = '.$uai['id_attivita'].' GROUP BY moh.`id_order`))'); 
                        $in_carico_a = ''; 
                        break;
                    case 'L': 
                        $uai_link .= '&tab_name=bdl&azione=bdl_edit&id_bdl='.$uai['id_attivita'].'&token='.$tokenCustomers; 
                        $uai_status = Db::getInstance()->getValue('SELECT (CASE WHEN a.rif_ordine > 0 THEN "closed" WHEN a.id_bdl < 384 THEN "closed" WHEN a.rif_ordine = 0 AND a.invio_controllo = 1 AND a.invio_contabilita = 0 THEN "pending1" WHEN a.rif_ordine = 0 AND a.invio_controllo = 1 AND a.invio_contabilita = 1 THEN "closed" WHEN a.invio_controllo = 2 AND a.invio_contabilita = 2 THEN "closed" WHEN a.rif_ordine = 0 AND a.invio_controllo = 0 THEN "open" ELSE "open" END) stato FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                        $in_carico_a = Db::getInstance()->getValue('SELECT effettuato_da FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                        break;
                    case 'T': 
                        $uai_link .= '&tab_name=tickets&azione=tickets&viewticket&id_customer_thread='.$uai['id_attivita'].'&token='.$tokenCustomers; 
                        $uai_status = Db::getInstance()->getValue('SELECT status FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                        $in_carico_a = Db::getInstance()->getValue('SELECT id_employee FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                        break;
                    case 'M': 
                        $uai_link .= '&tab_name=actions&azione=actions&viewmessage&id_mex='.$uai['id_attivita'].'&token='.$tokenCustomers; 
                        $uai_status = Db::getInstance()->getValue('SELECT status FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                        $in_carico_a = Db::getInstance()->getValue('SELECT id_employee FROM '.$table.' WHERE '.$attivita_key.' = '.$uai['id_attivita']); 
                        break;	
                }	
                
                $uai_employee = Db::getInstance()->getValue('
                    SELECT CONCAT(firstname," ",LEFT(lastname,1),".") impiegato 
                    FROM '._DB_PREFIX_.'employee 
                    WHERE id_employee = '.$in_carico_a
                );
                
                // Correggere: copia style di Attività recenti Employeees
                switch($uai_status)
                {
                    case 'open': 
                        $uai_status = 'Aperto'; 
                        break;
                    case 'closed': 
                        $uai_status = 'Chiuso'; 
                        break;
                    case 'pending1': 
                        $uai_status = 'In lavorazione'; 
                        break;
                    case 'pending2': 
                        $uai_status = 'In lavorazione'; 
                        break;
                    case 1: // <img src="../img/admin/enabled.gif" alt="Letto" title="Letto" />
                        $uai_status = 'Letto';
                        break;
                    case 0: // <img src="../img/admin/red_no.gif" alt="Non letto" title="Non letto" />
                        $uai_status = 'Non letto'; 
                        break;
                    case 999: 
                        $uai_status = 'Carrello Amazon convertito'; 
                        break;
                    default: 
                        $uai_status; 
                        break;
                }
                
                $dati_cliente = Db::getInstance()->getValue('
                    SELECT (CASE WHEN is_company = 1 THEN company ELSE CONCAT(firstname," ",lastname) END) cliente 
                    FROM '._DB_PREFIX_.'customer 
                    WHERE id_customer = '.$id_customer
                );
                
                if($uai['tipo_attivita'] == 'T')
                {
                    $check_resp = 1;

                    $id_contact = Db::getInstance()->getValue('
                        SELECT id_contact 
                        FROM '._DB_PREFIX_.'customer_thread 
                        WHERE id_customer_thread = '.$uai['id_attivita']
                    );

                    if($id_contact == 7)
                    {
                        $tipo_attivita_uai = 'Messaggio';
                        $count_msg = Db::getInstance()->getValue('
                            SELECT count(id_customer_message) 
                            FROM '._DB_PREFIX_.'customer_message 
                            WHERE id_employee = 0 
                                AND id_customer_thread = '.$uai['id_attivita']
                        );

                        if($count_msg == 0)
                            $check_resp = 0;
                    }	
                }
                
                if($uai['tipo_attivita'] == 'P')
                {
                    $id_contact = Db::getInstance()->getValue('
                        SELECT tipo_richiesta 
                        FROM form_prevendita_thread 
                        WHERE id_thread = '.$uai['id_attivita']
                    );

                    if($id_contact == 'tirichiamiamonoi')
                        $tipo_attivita_uai = 'Ti richiamiamo noi';
                }
                
                if(substr($uai['desc_attivita'],0,22) == 'Ha salvato il carrello')
                    $desc_attivita = 'Ha lavorato sul carrello';
                else
                    $desc_attivita = $uai['desc_attivita'];
                
                if(is_numeric(substr($desc_attivita, -1)))
                {	
                    $employee = filter_var($desc_attivita, FILTER_SANITIZE_NUMBER_INT);
                    $name_employee = Db::getInstance()->getValue('
                        SELECT concat(firstname, " ", LEFT(lastname,1),".") 
                        FROM '._DB_PREFIX_.'employee 
                        WHERE id_employee = '.$employee
                    );
                    $desc_attivita = 'Ha messo l\'attivit&agrave; in carico a '.$name_employee;
                }
                
                if($dati_cliente && $dati_cliente != '')
                {
                    // Test: tooltip al posto di Espandi dettagli riga
                    $tooltip_espandi = $uai_employee_act.' '.$desc_attivita;

                    $content .= '<tr '.($uaii%2 == 0 ? '' : 'style="background-color:#f5f5f5"').' class="riga"><td data-sort="'.$tipo_attivita_uai.'" id="td_espandi_'.$uai['id_attivita'].'"><i class="icon-question-sign pointer" title="'.$tooltip_espandi.'"></i> | '.$tipo_attivita_uai.'';
                    // $content .= '<tr '.($uaii%2 == 0 ? '' : 'style="background-color:#f5f5f5"').' class="riga"><td data-sort="'.$tipo_attivita_uai.'" id="td_espandi_'.$uai['id_attivita'].'"><img class="pointer" src="../img/admin/add.gif" alt="Espandi" title="Espandi" data-toggle="collapse" data-target="#dett_att_'.$uai['id_attivita'].'" aria-expanded="false" />'.$tipo_attivita_uai.'';
                    
                    // Espandi dettagli riga
                    if($uai['tipo_attivita'] == 'C' || $uai['tipo_attivita'] == 'O')
                    {
                        // Correggere: spostare in un file js
                        /*$content .= '
                        <script type="text/javascript">
                            $(document).ready(function(){
                                $("#espandi_'.$uai['id_attivita'].', #td_espandi_'.$uai['id_attivita'].'").click(function(){
                                    
                                    if($(this).attr("src") == ("../img/admin/add.gif")) {
                                        $(this).attr("src", "../img/admin/forbbiden.gif");
                                        $.ajax({
                                            url:"ajax.php?get'.($uai['tipo_attivita'] == 'O' ? 'Order' : 'Cart').'Display=y",
                                            type: "POST",
                                            data: {
                                            id_'.($uai['tipo_attivita'] == 'O' ? 'order' : 'cart').': '.$uai['id_attivita'].'
                                            },
                                            success:function(r){
                                                $("#cart_riga_show_'.$uai['id_attivita'].'_td").html(r);
                                                $("#cart_riga_show_'.$uai['id_attivita'].'").show();
                                            },
                                            error: function(xhr,stato,errori){
                                                alert("Errore durante l\'operazione:"+xhr.status);
                                            }
                                        });
                                    }
                                    else {
                                        $(this).attr("src", "../img/admin/add.gif");
                                        $("#cart_riga_show_'.$uai['id_attivita'].'").hide();
                                    }
                                    return false;
                                });
                                
                            });
                        </script>';*/
                    }
                    else
                    {
                        // Correggere: spostare in un file js
                        /*$content .= '
                        <script type="text/javascript">
                            $(document).ready(function(){
                                $("#espandi_'.$uai['id_attivita'].', #td_espandi_'.$uai['id_attivita'].'").click(function(){
                                    $("tr.sub_'.$uai['id_attivita'].'").toggle();
                                    if($("tr.sub_'.$uai['id_attivita'].':visible").length) {
                                        $(this).attr("src", "../img/admin/forbbiden.gif");	
                                    }
                                    else {
                                        $(this).attr("src", "../img/admin/add.gif");
                                        $("tr.sub_'.$uai['id_attivita'].'").hide();
                                    }
                                    return false;
                                });
                                
                            });
                        </script>';*/
                    }

                    $content .= '</td>';

                    $content .= '<td><a href="'.$uai_link.'">'.$id_attivita.'</a></td><td><a href="'.$uai_link.'">'.$dati_cliente.'</a></td><td><a href="'.$uai_link.'">'.$uai_employee.'</a></td><td><a href="'.$uai_link.'">'.($tipo_attivita_uai == 'Messaggio' && $check_resp == 0 ? '' : $uai_status).'</a></td><td><a href="'.$uai_link.'">'.Tools::displayDate($uai['data_attivita'], $context->language->id, true).'</a></td></tr>';
                    
                    // Correggere
                    if($uai['tipo_attivita'] == 'O') {
                        $content .= '<tr id="cart_riga_show_'.$uai['id_attivita'].'" class="invisible-table-row subs sub_'.$uai['id_attivita'].'" style="display:none '.($uaii%2 == 0 ? '' : '; background-color:#f5f5f5"').'"><td id="cart_riga_show_'.$uai['id_attivita'].'_td"></td><td></td><td></td><td></td><td></td><td></td></tr>';
                    }
                    else if($uai['tipo_attivita'] == 'C') {
                        $content .= '<tr id="cart_riga_show_'.$uai['id_attivita'].'" class="invisible-table-row subs sub_'.$uai['id_attivita'].'" style="display:none '.($uaii%2 == 0 ? '' : '; background-color:#f5f5f5"').'"><td id="cart_riga_show_'.$uai['id_attivita'].'_td"></td><td></td><td></td><td></td><td></td><td></td></tr>';
                    }	
                    else {
                        // Non compatibile con datatables
                        /*$content .= '
                            <tr>
                                <td colspan="6">
                                    <div class="collapse" id="dett_att_'.$uai['id_attivita'].'">
                                        <div class="row" '.($uaii % 2 == 0 ? '' : 'style="background-color:#F5F5F5;"').'>
                                            <div class="col-md-7">'.$uai_employee_act.' '.$desc_attivita.'</div>
                                            <div class="col-md-1"></div>
                                            <div class="col-md-1"></div>
                                            <div class="col-md-1"></div>
                                            <div class="col-md-1"></div>
                                            <div class="col-md-1"></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        ';*/
                    }
                }

                $uaii++;
            }
                
            $content .= '</tbody></table></div>';
        }
        else
            $content = 'Non hai i permessi necessari per vedere le ultime attivit&agrave;.';

        return $content;
    }

    // CORREGGERE
    public function inserisciLeMieAttivita()
    {
        if(!$this->context)
            $context = Context::getContext();
        else
            $context = $this->context;

        $is_agente = ($context->employee->id_profile == 7 ? true : false); // true: l'employee loggato è un agente
        
        // ticket
        $union0 = '
            SELECT 
                a.id_customer_thread,
                a.id_contact,
                a.id_customer,
                a.id_employee,
                a.date_add,

                a.status AS stato,
                a.date_upd AS data_act,
                
                cm.id_employee AS first_open,

                '.$context->language->id.' AS id_lang,

                CONCAT(
                    CASE a.id_contact
                    WHEN 2
                    THEN "A"
                    WHEN 4
                    THEN "T"
                    WHEN 3
                    THEN "V"
                    WHEN 6
                    THEN "R"
                    WHEN 7
                    THEN "M"
                    WHEN 8
                    THEN "D"
                    WHEN 9
                    THEN "S"
                    END, 
                    SUBSTRING(a.date_add, 3, 2), 
                    a.id_customer_thread 
                ) AS ticket,

                (CASE c.id_default_group
                WHEN 1
                THEN "Clienti web"
                WHEN 3
                THEN "Rivenditori"
                WHEN 15
                THEN "Rich. Riv."
                WHEN 4
                THEN "Call center"
                WHEN 5
                THEN "PA"
                WHEN 8
                THEN "Clienti top"
                WHEN 11
                THEN "Fornitori"
                WHEN 12
                THEN "Distributori"
                WHEN 16
                THEN "C. Amazon"
                WHEN 17
                THEN "C. ePrice"
                ELSE "Altro"
                END
                ) AS gruppo_clienti,
            
                c.company,
                c.vat_number,
                c.tax_code,
                
                (CASE c.is_company
                WHEN 1 
                THEN c.vat_number
                ELSE c.tax_code
                END) AS cfpi,

                (CASE c.is_company
                WHEN 0
                THEN CONCAT(c.firstname," ",c.lastname)
                WHEN 1
                THEN c.company
                ELSE
                CONCAT(c.firstname," ",c.lastname)
                END
                ) AS customer, 

                cl.id_contact as contact_type, 

                (CASE
                WHEN cl.name LIKE "%contabil%"
                THEN "Contabilita"
                WHEN cl.name LIKE "%ordini%"
                THEN "Ordini"
                WHEN cl.name = "Assistenza tecnica postvendita"
                THEN "Assistenza"
                WHEN cl.name = "Rivenditori"
                THEN "Rivenditori"
                WHEN cl.name = "Messaggi"
                THEN "Messaggi"
                WHEN cl.name = "RMA"
                THEN "RMA"
                ELSE "Altro"
                END	
                ) AS contact,

                (CASE 
                WHEN a.priorita = "high" 
                THEN "ALTA" 
                WHEN a.priorita = "medium" 
                THEN "Media" 
                WHEN a.priorita = "low" 
                THEN "Bassa" 
                ELSE "--" 
                END) AS priority,
                
                (SELECT IFNULL(CONCAT(e.firstname," ",LEFT(e.lastname, 1),"."), "--")
                FROM `'._DB_PREFIX_.'customer_thread` ct 
                INNER JOIN '._DB_PREFIX_.'employee e ON e.id_employee = ct.id_employee
                WHERE ct.id_employee > 0 AND ct.`id_customer_thread` = a.`id_customer_thread` 
                ORDER BY ct.date_add DESC 
                LIMIT 1
                ) AS employee
                
            FROM '._DB_PREFIX_.'customer_thread a 
            LEFT JOIN '._DB_PREFIX_.'customer c ON c.id_customer = a.id_customer
            LEFT JOIN (SELECT id_employee, id_customer_thread FROM '._DB_PREFIX_.'customer_message ORDER BY date_add ASC) cm on a.id_customer_thread = cm.id_customer_thread
            LEFT JOIN '._DB_PREFIX_.'contact_lang cl ON (cl.id_contact = a.id_contact AND cl.id_lang = '.(int)$context->language->id.')
            '.(!empty($_POST['cercaticket']) ? 'JOIN (
                SELECT
                    aa.id_customer_thread,
                    bb.message as m
                FROM
                    '._DB_PREFIX_.'customer_thread aa
                INNER JOIN '._DB_PREFIX_.'customer_message bb ON
                    aa.id_customer_thread = bb.id_customer_thread
                WHERE
                    bb.message LIKE "%'.$_POST['cercaticket'].'%"
                    
                UNION
                    
                SELECT
                    aa.id_customer_thread,
                    cc.note as m
                FROM
                    '._DB_PREFIX_.'customer_thread aa
                INNER JOIN note_attivita cc ON
                    aa.id_customer_thread = cc.id_attivita
                WHERE
                    cc.note LIKE "%'.$_POST['cercaticket'].'%"
                    AND cc.tipo_attivita = "T"
            ) mess_note_0
            on a.id_customer_thread = mess_note_0.id_customer_thread' : '').'
            WHERE a.id_contact != 7 
                '.(!Tools::getIsset('cercaticket') ? " AND cm.id_employee = ".$context->employee->id : "").'
            GROUP BY a.id_customer_thread
        ';

        // messaggi
        $union1 = ' UNION ALL ';
        $union1 .= ' 
            SELECT 
                ct.id_customer_thread,
                ct.id_contact,
                ct.id_customer,
                ct.id_employee,
                ct.date_add,
                
                ct.status AS stato,
                ct.date_upd AS data_act,
                
                cm.id_employee AS first_open, 

                '.$context->language->id.' AS id_lang,

                CONCAT("M", SUBSTRING(ct.date_add, 3, 2), ct.id_customer_thread) AS ticket,

                (CASE c.id_default_group
                WHEN 1
                THEN "Clienti web"
                WHEN 3
                THEN "Rivenditori"
                WHEN 15
                THEN "Rich. Riv."
                WHEN 4
                THEN "Call center"
                WHEN 5
                THEN "PA"
                WHEN 8
                THEN "Clienti top"
                WHEN 11
                THEN "Fornitori"
                WHEN 12
                THEN "Distributori"
                WHEN 16
                THEN "C. Amazon"
                WHEN 17
                THEN "C. ePrice"
                ELSE "Altro"
                END
                ) AS gruppo_clienti,

                c.company,
                c.vat_number,
                c.tax_code,
                
                (CASE c.is_company
                WHEN 1 
                THEN c.vat_number
                ELSE c.tax_code
                END) AS cfpi,

                (CASE c.is_company
                WHEN 0
                THEN CONCAT(c.firstname," ",c.lastname)
                WHEN 1
                THEN c.company
                ELSE
                CONCAT(c.firstname," ",c.lastname)
                END
                ) AS customer,

                "" AS contact_type, 
                "Messaggi" AS contact, 
                "--" AS priority, 
                 
                (SELECT IFNULL(CONCAT(e.firstname," ",LEFT(e.lastname, 1),"."), "--") 
                FROM '._DB_PREFIX_.'customer_thread cz 
                INNER JOIN '._DB_PREFIX_.'employee e ON e.id_employee = cz.id_employee 
                WHERE cz.id_employee > 0 AND cz.`id_customer_thread` = ct.`id_customer_thread` 
                ORDER BY cz.date_add DESC 
                LIMIT 1
                ) AS employee

            FROM '._DB_PREFIX_.'customer_thread ct 
            JOIN '._DB_PREFIX_.'customer c ON c.id_customer = ct.id_customer 
            JOIN '.(!empty($_POST['cercaticket']) ? '(SELECT * FROM ' : '').' '._DB_PREFIX_.'customer_message '.(!empty($_POST['cercaticket']) ? 'WHERE message LIKE "%'.$_POST['cercaticket'].'%")' : '').' cm ON cm.id_customer_thread = ct.id_customer_thread 
            '.(!empty($_POST['cercaticket']) ? 'JOIN (
				SELECT
					aa.id_customer_thread,
					bb.message as m
				FROM
                    '._DB_PREFIX_.'customer_thread aa
				INNER JOIN '._DB_PREFIX_.'customer_message bb ON
					aa.id_customer_thread = bb.id_customer_thread
				WHERE
					bb.message LIKE "%'.$_POST['cercaticket'].'%"
					
				UNION
					
				SELECT
					aa.id_customer_thread,
					cc.note as m
				FROM
                    '._DB_PREFIX_.'customer_thread aa
				INNER JOIN note_attivita cc ON
					aa.id_customer_thread = cc.id_attivita
				WHERE
					cc.note LIKE "%'.$_POST['cercaticket'].'%"
					AND cc.tipo_attivita = "T"
			) mess_note_1
			on ct.id_customer_thread = mess_note_1.id_customer_thread' : '').'
            WHERE ct.id_contact = 7 
                '.(!Tools::getIsset('cercaticket') ? " AND cm.id_employee = ".$context->employee->id : "").'
                AND (ct.status = "open" OR ct.status = "pending1" OR ct.status = "pending2" '.(Tools::getIsset('cercaticket') ? 'OR ct.status = "closed"' : '').')
            GROUP BY ct.id_customer_thread 
        ';

        // preventivi e ti richiamiamo noi
        $union2 = ' UNION ALL ';
        $union2 .= ' 
            SELECT 
                ft.id_thread AS id_customer_thread, 
                ft.tipo_richiesta AS id_contact, 
                ft.id_customer,
                ft.id_employee, 
                ft.date_add,
                
                ft.status AS stato,
                ft.date_upd AS data_act,
                
                fm2.id_employee AS first_open,

                '.$context->language->id.' AS id_lang,

                (CASE ft.tipo_richiesta 
                WHEN "preventivo" 
                THEN CONCAT("P", SUBSTRING(ft.date_add, 3, 2) , ft.id_thread) 
                WHEN "tirichiamiamonoi" 
                THEN CONCAT("R", SUBSTRING(ft.date_add, 3, 2) , ft.id_thread) 
                ELSE "--" END 
                ) AS ticket, 

                (CASE c.id_default_group
                WHEN 1
                THEN "Clienti web"
                WHEN 3
                THEN "Rivenditori"
                WHEN 15
                THEN "Rich. Riv."
                WHEN 4
                THEN "Call center"
                WHEN 5
                THEN "PA"
                WHEN 8
                THEN "Clienti top"
                WHEN 11
                THEN "Fornitori"
                WHEN 12
                THEN "Distributori"
                WHEN 16
                THEN "C. Amazon"
                WHEN 17
                THEN "C. ePrice"
                ELSE "Altro"
                END
                ) AS gruppo_clienti,

                c.company,
                ft.vat_number AS vat_number, 
                ft.tax_code AS tax_code, 
                
                (CASE c.is_company
                WHEN 1 
                THEN c.vat_number
                ELSE c.tax_code
                END) AS cfpi,

                (CASE c.is_company
                WHEN 0
                THEN CONCAT(c.firstname," ",c.lastname)
                WHEN 1
                THEN c.company
                ELSE
                CONCAT(c.firstname," ",c.lastname)
                END
                ) AS customer,

                "" AS contact_type,
                
                (CASE ft.tipo_richiesta 
                WHEN "preventivo" 
                THEN "Commerciale" 
                WHEN "tirichiamiamonoi" 
                THEN "Richiamiamo" 
                ELSE "--"
                END 
                ) AS contact, 

                "--" AS priority,

                (SELECT IFNULL(CONCAT(e.firstname," ",LEFT(e.lastname, 1),"."), "--") 
                FROM form_prevendita_thread ft2
                INNER JOIN '._DB_PREFIX_.'employee e ON e.id_employee = ft2.id_employee 
                WHERE ft2.id_employee > 0 AND ft2.`id_thread` = ft.`id_thread` 
                ORDER BY ft2.date_add DESC 
                LIMIT 1
                ) AS employee

            FROM form_prevendita_thread ft 
            JOIN '._DB_PREFIX_.'customer c ON c.id_customer = ft.id_customer
            JOIN (SELECT id_thread, id_employee FROM form_prevendita_message '.(!empty($_POST['cercaticket']) ? 'WHERE message LIKE "%'.$_POST['cercaticket'].'%"' : '').') fm2 ON ft.id_thread = fm2.id_thread             
            '.(!empty($_POST['cercaticket']) ? 'JOIN (
				SELECT
					aa.id_thread,
					bb.message as m
				FROM
					form_prevendita_thread aa
				INNER JOIN form_prevendita_message bb ON
					aa.id_thread = bb.id_thread
				WHERE
					bb.message LIKE "%'.$_POST['cercaticket'].'%"
					
				UNION
					
				SELECT
					aa.id_thread,
					cc.note as m
				FROM
					form_prevendita_thread aa
				INNER JOIN note_attivita cc ON
					aa.id_thread = cc.id_attivita
				WHERE
					cc.note LIKE "%'.$_POST['cercaticket'].'%"
					AND cc.tipo_attivita = "P"
			) mess_note_2
			on ft.id_thread = mess_note_2.id_thread' : '').'
            WHERE 1 
                '.(!Tools::getIsset('cercaticket') ? "AND ft.id_employee = ".$context->employee->id : "").'
                AND (ft.status = "open" OR ft.status = "pending1" OR ft.status = "pending2" '.(Tools::getIsset('cercaticket') ? 'OR ft.status = "closed"' : '').')
            GROUP BY ft.id_thread 
        ';

        // CORREGGERE: sbagliato AND (ft.status = "open" OR ft.status = "pending1" OR ft.status = "pending2" '.(Tools::getIsset('cercaticket') ? 'OR ft.status = "closed"' : '').')
        // vedi AdminCustomerThreads 1.4, ma non so se si può applicare quel ragionamento a datatables (_tmptablefilter)
        // andrebbe selezionato aperto + in lavorazione di default ma trovare comunque tutti i valori possibili con la query per poi filtrarli
        // stessa cosa per In charge (id_employee) per tutte le union

        // to-do
        $union3 = ' UNION ALL ';
        $union3 .= ' 
            SELECT 
                ac.id_action AS id_customer_thread, 
                ac.action_type AS id_contact, 
                ac.id_customer, 
                ac.action_to AS id_employee, 
                ac.date_add,
                
                ac.status AS stato, 
                (CASE ac.action_date WHEN "0000-00-00 00:00:00" THEN ac.date_upd ELSE ac.action_date END) AS data_act, 
                
                ac.action_from AS first_open,

                '.$context->language->id.' AS id_lang,

                CONCAT("TODO", ac.id_action) AS ticket, 

                (CASE c.id_default_group
                WHEN 1
                THEN "Clienti web"
                WHEN 3
                THEN "Rivenditori"
                WHEN 15
                THEN "Rich. Riv."
                WHEN 4
                THEN "Call center"
                WHEN 5
                THEN "PA"
                WHEN 8
                THEN "Clienti top"
                WHEN 11
                THEN "Fornitori"
                WHEN 12
                THEN "Distributori"
                WHEN 16
                THEN "C. Amazon"
                WHEN 17
                THEN "C. ePrice"
                ELSE "Altro"
                END
                ) AS gruppo_clienti,

                c.company,
                c.vat_number,
                c.tax_code,
                
                (CASE c.is_company
                WHEN 1 
                THEN c.vat_number
                ELSE c.tax_code
                END) AS cfpi,

                (CASE c.is_company
                WHEN 0
                THEN CONCAT(c.firstname," ",c.lastname)
                WHEN 1
                THEN c.company
                ELSE
                CONCAT(c.firstname," ",c.lastname)
                END
                ) AS customer,

                "" AS contact_type,
                ac.action_type AS contact,
                "--" AS priority,

                (SELECT IFNULL(CONCAT(e.firstname," ",LEFT(e.lastname, 1),"."), "--") 
                FROM action_thread ac2 
                JOIN '._DB_PREFIX_.'employee e ON e.id_employee = ac2.action_to 
                WHERE ac2.action_to > 0 AND ac2.`id_action` = ac.`id_action` 
                ORDER BY ac2.date_add DESC 
                LIMIT 1
                ) AS employee 

            FROM action_thread ac 
            JOIN '._DB_PREFIX_.'customer c ON c.id_customer = ac.id_customer
			'./*(!empty($_POST['cercaticket']) ? 'JOIN (SELECT * FROM action_message WHERE action_message LIKE "%'.$_POST['cercaticket'].'%") am ON ac.id_action = am.id_action' : '').*/'
            '.(!empty($_POST['cercaticket']) ? 'JOIN (
				SELECT
					aa.id_action,
					bb.action_message as m
				FROM
					action_thread aa
				INNER JOIN action_message bb ON
					aa.id_action = bb.id_action
				WHERE
					bb.action_message LIKE "%'.$_POST['cercaticket'].'%"
					
				UNION
					
				SELECT
					aa.id_action,
					cc.note as m
				FROM
					action_thread aa
				INNER JOIN note_attivita cc ON
					aa.id_action = cc.id_attivita
				WHERE
					cc.note LIKE "%'.$_POST['cercaticket'].'%"
					AND cc.tipo_attivita = "A"
			) mess_note_3
			on ac.id_action = mess_note_3.id_action' : '').'
            WHERE 1
                '.(!Tools::getIsset('cercaticket') ? "AND ac.action_to = ".$context->employee->id : "").'
                AND (ac.status = "open" OR ac.status = "pending1" OR ac.status = "pending2" '.(Tools::getIsset('cercaticket') ? 'OR ac.status = "closed"' : '').')
            GROUP BY ac.id_action 
        ';

        // to-do employees
        $union4 = ' UNION ALL ';
        $union4 .= ' 
            SELECT 
                ae.id_action AS id_customer_thread, 
                ae.action_type AS id_contact, 
                "Staff" AS id_customer, 
                ae.action_to AS id_employee, 
                ae.date_add,
                
                ae.status AS stato, 
                (CASE ae.action_date WHEN "0000-00-00 00:00:00" THEN ae.date_upd ELSE ae.action_date END) AS data_act, 
                
                ae.action_from AS first_open,

                '.$context->language->id.' AS id_lang,

                CONCAT("STAFF", ae.id_action) AS ticket, 

                "Att. interna" AS gruppo_clienti,

                "Staff" AS company,
                "" AS vat_number,
                "" AS tax_code,
                "" AS cfpi,

                (SELECT IFNULL(CONCAT(e.firstname," ",LEFT(e.lastname, 1),"."), "--")
                FROM action_thread_employee ae2 
                JOIN '._DB_PREFIX_.'employee e ON e.id_employee = ae2.action_to 
                WHERE ae2.action_to > 0 AND ae2.`id_action` = ae.`id_action` 
                ORDER BY ae2.date_add DESC 
                LIMIT 1
                ) AS customer,

                "" AS contact_type,
                ae.action_type AS contact,
                "--" AS priority,

                (SELECT IFNULL(CONCAT(e.firstname," ",LEFT(e.lastname, 1),"."), "--")
                FROM action_thread_employee ae2 
                JOIN '._DB_PREFIX_.'employee e ON e.id_employee = ae2.action_to 
                WHERE ae2.action_to > 0 AND ae2.`id_action` = ae.`id_action` 
                ORDER BY ae2.date_add DESC 
                LIMIT 1
                ) AS employee 

            FROM action_thread_employee ae
            JOIN (SELECT * FROM action_message_employee './*(!empty($_POST['cercaticket']) ? 'WHERE action_message LIKE "%'.$_POST['cercaticket'].'%"' : '').*/') am ON ae.id_action = am.id_action
            '.(!empty($_POST['cercaticket']) ? 'JOIN (
				SELECT
					aa.id_action,
					bb.action_message_employee as m
				FROM
					action_thread_employee aa
				INNER JOIN action_message_employee bb ON
					aa.id_action = bb.id_action
				WHERE
					bb.action_message_employee LIKE "%'.$_POST['cercaticket'].'%"
					
				UNION
					
				SELECT
					aa.id_action,
					cc.note as m
				FROM
					action_thread_employee aa
				INNER JOIN note_attivita cc ON
					aa.id_action = cc.id_attivita
				WHERE
					cc.note LIKE "%'.$_POST['cercaticket'].'%"
					AND cc.tipo_attivita = "AE"
			) mess_note_4
			on ae.id_action = mess_note_4.id_action' : '').'
            WHERE 1
                '.(!Tools::getIsset('cercaticket') ? "AND ae.action_to = ".$context->employee->id : "").'
                AND (ae.status = "open" OR ae.status = "pending1" OR ae.status = "pending2" '.(Tools::getIsset('cercaticket') ? 'OR ae.status = "closed"' : '').')
            GROUP BY ae.id_action
            ORDER BY date_add DESC
        ';

        $unions = $union0.' '.$union1.' '.$union2.' '.$union3.' '.$union4;

        // correggere: togliere where e order by e farli con datatable
        // perchè si controlla id_employee di customer_message e non di customer_thread?
        $query_attivita = '
            SELECT * 
            FROM ('.$unions.') tmpTable
            ORDER BY (
                CASE WHEN stato = "open" THEN 3 WHEN stato = "pending1" THEN 2 WHEN stato = "pending2" THEN 1 WHEN stato = "closed" THEN 0 END
            ) DESC, data_act ASC
        ';

        // correggere filtri: fare con datatable se possibile

        $attivita = Db::getInstance()->executeS($query_attivita);

        $employees = Employee::getEmployees();
        $tokenCustomers = Tools::getAdminTokenLite('AdminCustomers');
        
        // finire di correggere le variabili!
        foreach($attivita as &$att) 
        {
            $type = ($att['id_customer'] == 'Staff' ? 'to-do-employee' : Tools::getValue('type'));
            $tipo = (substr($att['ticket'],0,1) == 'P' ? 'preventivo' : (substr($att['ticket'],0,1) == 'R' ? 'tirichiamiamonoi' : (substr($att['ticket'],0,4) == 'TODO' ? 'to-do' : 'ticket')));
            $id_contact = (is_numeric($att['id_contact']) ? $att['id_contact'] : $tipo);

            $tipo_m = ($tipo == 'ticket' && $id_contact == 7 ? 'messaggio' : $tipo);

            switch($tipo_m) {
                case 'preventivo':
                    $tab_name = 'actions';
                    $get_azione = 'actions';
                    $view = 'preventivo';
                    $thread_key = 'id_thread';
                    break;
                case 'tirichiamiamonoi':
                    $tab_name = 'actions';
                    $get_azione = 'actions';
                    $view = 'preventivo';
                    $thread_key = 'id_thread';
                    break;
                case 'to-do':
                    $tab_name = 'todo';
                    $get_azione = 'todo';
                    $view = 'action';
                    $thread_key = 'id_action';
                    break;
                case 'ticket':
                    $tab_name = 'tickets';
                    $get_azione = 'tickets';
                    $view = 'ticket';
                    $thread_key = 'id_customer_thread';
                    break;
                case 'messaggio':
                    $tab_name = 'actions';
                    $get_azione = 'actions';
                    $view = 'message';
                    $thread_key = 'id_mex';
                    break;
                default:
                    break;
            }
            switch($att['stato']) {
                case 'open': 
                    $status_azione = "<i class='icon-circle' style='color:red' title='Aperto' /></i>"; 
                    $status_azione_sort = 0;
                    break;
                case 'closed': 
                    $status_azione = "<i class='icon-circle' style='color:green' title='Chiuso' /></i>"; 
                    $status_azione_sort = 2;
                    break;
                case 'pending1': 
                    $status_azione = "<i class='icon-circle' style='color:orange' title='In lavorazione' /></i>"; 
                    $status_azione_sort = 1;
                    break;
                default: 
                    $status_azione = "<i class='icon-circle' style='color:red' title='Aperto' /></i>"; 
                    $status_azione_sort = 0;
                    break;
            }

            $att['stato_icona'] = $status_azione;
            $att['stato_sort'] = $status_azione_sort;

            if($att['first_open'] != 0){
                $att['aperto_da'] = Db::getInstance()->getValue('
                    SELECT CONCAT(firstname," ",LEFT(lastname, 1),".")
                    FROM '._DB_PREFIX_.'employee
                    WHERE id_employee = '.$att['first_open']
                );
            }
            else
                $att['aperto_da'] = 'Cliente';

            if($id_contact == 7)
                $ctrl_mex = Db::getInstance()->getValue("
                    SELECT COUNT(id_customer_message) 
                    FROM "._DB_PREFIX_."customer_thread ct 
                    JOIN "._DB_PREFIX_."customer_message cm ON ct.id_customer_thread = cm.id_customer_thread 
                    WHERE cm.id_employee = 0 
                        AND ct.id_customer_thread = ".$att['id_customer_thread']."
                ");	
            else
                $ctrl_mex = 1;

            $att['status_edit'] = ($ctrl_mex >= 1 ? 
                '<form style="margin-left:5px; display:block;" name="cambiastato_'.$att['id_customer_thread'].'" id="cambiastato_'.$att['id_customer_thread'].'" action="" method="post" />	
                    <input type="hidden" name="id_customer" value="'.$att['id_customer'].'" />
                    <input type="hidden" name="id_thread" value="'.$att['id_customer_thread'].'" />
                    <input type="hidden" name="type" value="'.$type.'" />
                    <input type="hidden" name="tipo" value="'.$tipo.'" />
                    <select name="cambiastatus" onchange="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { document.forms[\'cambiastato_'.$att['id_customer_thread'].'\'].submit(); } else { this.value=\''.$att['stato'].'\'}">
                        <option style="padding-left:30px;" value="closed" '.($att['stato'] == 'closed' ? 'selected="selected"' : '').'>C</option>
                        <option style="padding-left:30px;" value="pending1" '.($att['stato'] == 'pending1' ? 'selected="selected"' : '').'>L</option>
                        <option style="padding-left:30px;" value="open" '.($att['stato'] == 'open' ? 'selected="selected"' : '').'>A</option>
                    </select>
                </form>'
            : '');
            
            $att['id_azione'] = Customer::trovaSigla($att['id_customer_thread'], $tipo);

            $att['in_carico_edit'] = '
                <form style="margin-left:5px; display:block;" name="cambiaincarico_'.$att['id_customer_thread'].'" id="cambiaincarico_'.$att['id_customer_thread'].'" action="" method="post" />
                    <a id="'.$att['id_customer_thread'].'"> </a>
                    <input type="hidden" name="id_customer" value="'.$att['id_customer'].'" />
                    <input type="hidden" name="id_thread" value="'.$att['id_customer_thread'].'" />
                    <input type="hidden" name="id_ticket_univoco" value="'.$att['id_azione'].'" />
                    <input type="hidden" name="precedente_incarico" value="'.$att['id_employee'].'" />
                    <input type="hidden" name="type" value="'.$type.'" />
                    <input type="hidden" name="tipo" value="'.$tipo.'" />
                    <select name="cambiaincarico" onchange="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { document.forms[\'cambiaincarico_'.$att['id_customer_thread'].'\'].submit(); } else { this.value=\''.$att['id_employee'].'\'}">
                        <option value="0" '.($att['id_employee'] == '0' ? 'selected="selected"' : '').'>--Nessuno--</option>
            ';

            foreach($employees as $employee)
                $att['in_carico_edit'] .= '<option value="'.$employee['id_employee'].'" '.($att['id_employee'] == $employee['id_employee'] ? 'selected="selected"' : '').'>'.$employee['firstname'].' '.substr($employee['lastname'], 0, 1).'</option>';
            
            $att['in_carico_edit'] .= '
                    </select>
                </form>
            ';

            $att['href'] = 'index.php?controller=AdminCustomers&id_customer='.$att['id_customer'].'&viewcustomer&tab_name='.$tab_name.'&azione='.$get_azione.'&view'.$view.'&'.$thread_key.'='.$att['id_customer_thread'].'&token='.$tokenCustomers;
            $att['href_cancella'] = self::$currentIndex.'&id_customer_thread='.$att['id_customer_thread'].'&del'.(Tools::getIsset('id_employee') ? '&id_employee='.Tools::getValue('id_employee') : '').'&typedel='.$id_contact.'&tab_name='.$tab_name.'&token='.Tools::getAdminToken('AdminPlanner'.(int)Tab::getIdFromClassName('AdminPlanner').(int)$context->employee->id);			
        }

        return $attivita;
    }
}
