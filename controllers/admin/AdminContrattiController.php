<?php

// Potrei togliere Core e trasformare in override
class AdminContrattiControllerCore extends AdminController
{
    protected $_defaultOrderBy = 'c!data_fine';
    protected $_defaultOrderWay = 'DESC';

    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'customer';
        $this->lang = false;
        $this->explicitSelect = true;

        $this->allow_export = true;

        $this->context = Context::getContext();
        $this->context->currency = Currency::getDefaultCurrency();

        $this->_select = '
            c.data_fine,
            
            (CASE a.is_company
            WHEN 1
            THEN a.company
            ELSE
            CONCAT(a.firstname," ",a.lastname)
            END
            ) cliente,
            
            (CASE WHEN c.data_fine < DATE(NOW()) THEN 3
            ELSE c.status
            END) status_enhanced,
            
            (CASE c.tipo
            WHEN 1
            THEN "Base"
            WHEN 2
            THEN "Top"
            WHEN 3
            THEN "Gold"
            WHEN 4
            THEN "Teleassistenza"
            WHEN 5
            THEN "Noleggio"
            END)
            tipo_contratto,
            
            (CASE a.is_company
            WHEN 1
            THEN a.vat_number
            WHEN 0
            THEN a.tax_code
            ELSE a.tax_code
            END
            ) piva_cf
		';
		
        $this->_join = 'JOIN '._DB_PREFIX_.'contratto_assistenza c ON c.id_customer = a.id_customer';
        		
		$statusArray = array(
			0 => 'Attivo', // verde
			1 => 'Sospeso', // arancione
			2 => 'Cancellato', // rosso
            3 => 'Disdetto', // rosso
            4 => 'Scaduto' // rosso
        );

		$tipiArray = array(
			1 => 'Base',
			2 => 'Top',
			3 => 'Gold',
			4 => 'Teleassistenza',
			5 => 'Noleggio'
		);
        
        $this->_use_found_rows = false;

        $this->fields_list = array(
            'id_contratto' => array(
                'title' => $this->l('ID Contratto'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
            ),
            'id_customer' => array(
                'title' => $this->l('ID CRM'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
            ),
            'cliente' => array(
                'title' => $this->l('Customer'),
                'align' => 'text-left',
                'filter_key' => 'cliente',
                'filter_type' => 'string',
                'tmpTableFilter' => true,
            ),
            'piva_cf' => array(
                'title' => $this->l('PI/CF'),
                'align' => 'text-left',
                'class' => 'fixed-width-md',
                'tmpTableFilter' => true,
            ),
            'rif_fattura' => array(
                'title' => $this->l('Rif. Fattura'),
                'align' => 'text-left',
            ),
            'status_enhanced' => array(
                'title' => $this->l('Stato'),
                'align' => 'text-center', 
                'type' => 'select',
                'list' => $statusArray,
                'filter_key' => 'status_enhanced',
                'tmpTableFilter' => true,
                'callback' => 'statusStyle'
            ),
            'tipo_contratto' => array(
                'title' => $this->l('Tipo'),
                'align' => 'text-center', 
                'type' => 'select', 
                'list' => $tipiArray,
                'filter_key' => 'c!tipo', 
            ),
            'prezzo' => array(
                'title' => $this->l('Prezzo'),
                'align' => 'text-right',
                'callback' => 'format_prezzo',
                'prefix' => '<b>',
                'suffix' => '</b>',
            ),
            'data_inizio' => array(
                'title' => $this->l('Data inizio'),
                'align' => 'text-right',
                'type' => 'date',
                'callback' => 'format_data',
            ),
            'data_fine' => array(
                'title' => $this->l('Scadenza'),
                'align' => 'text-right', 
                'type' => 'date',
                'callback' => 'format_data',
            ),/*
            'connect' => array(
                'title' => $this->l('Connection'), 
                'type' => 'datetime', 
                'search' => false
            ),*/
        );

        $this->redirect_after = ' '; // necessario per function redirect()

        $this->addRowAction('view');
        
        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Contratti assistenza e noleggi'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
	}

    public function initToolbar()
    {
        parent::initToolbar();

        unset($this->toolbar_btn['new']); // Toglie il pulsante ADD NEW dalla toolbar
    }

    // Redirect all'anagrafica del cliente se action = view
    protected function redirect()
    {
        if(Tools::getIsset('id_customer') && Tools::getIsset('viewcustomer')){
            //$id_contratto = $this->id_contratto; // come lo trovo? Forse solo usando contratto_assistenza come table principale
            $this->currentIndex = 'AdminCustomers';
            $this->token = Tools::getAdminTokenLite('AdminCustomers');
            $this->redirect_after = '/ezadmin/index.php?controller='.$this->currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&tab_name=contracts&token='.$this->token;
            //$this->redirect_after = '/ezadmin/index.php?controller='.$this->currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&tab_name=contracts&azione=contratto_view&id_contratto='.$id_contratto.'&token='.$this->token;
            Tools::redirectAdmin($this->redirect_after);
        }
    }

    public function format_data($value)
	{
        $data = new DateTime($value);
        $data = $data->format('d/m/Y');

        return '<span>'.$data.'</span>';
    }

    public function format_prezzo($value)
	{
        // CON SIMBOLO EURO E . PER I DECIMALI
        /* $prezzo = (float)(str_replace(",",".",$value));
        $prezzo = Tools::displayPrice($prezzo, $this->context->currency); */

        $prezzo = number_format($value, 2, ',', '.');

        return '<span>'.$prezzo.'</span>';
    }

    public function statusStyle($value)
    {
        return ($value == 0 ? '<span class="badge badge-success squadrato thick">Attivo</span>' : 
                ($value == 1 ? '<span class="badge badge-warning squadrato thick">Sospeso</span>' : 
                ($value == 2 ? '<span class="badge badge-danger squadrato thick">Cancellato</span>' :
                ($value == 3 ? '<span class="badge badge-danger squadrato thick">Disdetto</span>' :
                '<span class="badge badge-danger squadrato thick">Scaduto</span>')
                ))
        );
    }
}