<?php

class AdminAgentiControllerCore extends AdminController
{
	public function __construct()
	{
        $this->displayInformation('Per ora è una lista normale, ma dovrebbe diventare un\'area agente (vedi appunti cartacei!)');
        
        $this->bootstrap = true;
        $this->table = 'agente';
        //$this->className = 'Agente'; // Correggere: rompe filtri e ordinamento
        $this->lang = false;
        $this->explicitSelect = true;

        $this->allow_export = true;

        $this->context = Context::getContext();

        $this->_orderBy = 'company';
        $this->_orderWay = 'ASC';

        $this->fields_list = array(
            'id_agente' => array(
                'title' => $this->l('ID'), 
                'align' => 'left', 
                'class' => 'fixed-width-xs'
            ),
            'company' => array(
                'title' => $this->l('Company') // Ragione sociale
            ),
            'lastname' => array(
                'title' => $this->l('Last Name')
            ),
            'firstname' => array(
                'title' => $this->l('First Name')
            ),
            'email' => array(
                'title' => $this->l('Email address')
            ),
            'city' => array(
                'title' => $this->l('City')
            ),
            'vat_number' => array(
                'title' => $this->l('VAT number') // P.IVA
            ),
        );

        $this->_use_found_rows = false;

        parent::__construct();
    }

	public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Agenti'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
	}
	
	public function renderView()
	{
		if(Tools::getValue('tab_name'))
			$tab_name = Tools::getValue('tab_name');
				
        $is_agente = ($this->context->employee->id_profile == 7 ? true : false); // true: l'employee loggato è un agente

        $this->tpl_view_vars = array(
			'is_agente' => $is_agente,
			'tab_name' => $tab_name,
            //'show_toolbar' => true
        );

        return parent::renderView();
    }
/* // CORREGGERE: test per mettere altra roba in list
    public function renderList()
    {
        // Here we retrieve the list (without doing any strange thing)
        $list = parent::renderList();

        // Assign some vars to pass to our custom tpl
        $this->context->smarty->assign(
            array( 
                'var1' => "Test",
                'var2' => "Test2"
                )
            );

        // Get the custom tpl rendered
        $content = $this->context->smarty->fetch(_PS_MODULE_DIR_ . "avanto_key/views/templates/admin/avantokeystock/customcontent.tpl");

        // return the list plus your content
        return $list . $content;
    }*/
}