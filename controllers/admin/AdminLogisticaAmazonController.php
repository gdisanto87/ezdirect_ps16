<?php

// Potrei togliere Core e trasformare in override
class AdminLogisticaAmazonControllerCore extends AdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->lang = false;

        $this->display = 'view';

        // I file si trovano nella cartella ezadmin
        $path_file_confronto = 'qta-logistica-amazon.php'; 
        $path_file_quantita = 'template-amazon-prime.php';

        $this->tpl_view_vars = array(
            'path_file_confronto' => $path_file_confronto,
            'path_file_quantita' => $path_file_quantita,
        );

        /* // COMMENTATO NELLA 1.4 - va nel tpl
        <form name='esporta-amazon' method='post'>
		<!--	Marca il flag per scegliere i campi da inserire nel foglio di Excel:<br />
		<input type='checkbox' checked='checked' name='fnsku' readonly='readonly' /> FNSKU (obbligatorio)<br />
		<input type='checkbox' checked='checked' name='asin' readonly='readonly' /> ASIN (obbligatorio)<br />
		<input type='checkbox' checked='checked' name='sku' readonly='readonly' /> SKU (obbligatorio)<br />
		<input type='checkbox' checked='checked' name='nome' readonly='readonly' /> Nome prodotto (obbligatorio)<br />
		
		<input type='checkbox' checked='checked' name='costruttore' />Costruttore<br />
		<input type='checkbox' checked='checked' name='prezzo_web' />Prezzo web<br />
		<input type='checkbox' name='prezzo_amazon'  />Prezzo Amazon Italia<br />
		<input type='checkbox' checked='checked' name='qt_ez' />Qt. Magazzino EZ<br />
		<input type='checkbox' checked='checked' name='qt_aln'  />Qt. Allnet<br />
		<input type='checkbox' checked='checked' name='qt_eds'  />Qt. EDS<br />
		<input type='checkbox' checked='checked' name='ord_ez'  />Ordinato EZ<br />
		-->
		<br /><br />
		<!-- <input onclick='window.onbeforeunload = null' type='submit' name='submitamazon' value='Esporta lista' /> -->
		</form>
        */

        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Logistica Amazon'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
	}
}