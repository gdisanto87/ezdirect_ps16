<?php

// Potrei togliere Core e trasformare in override
class AdminControlloPagamentiControllerCore extends AdminController
{
    public function __construct()
    {
        $this->displayWarning('ELIMINARE?');
        
        $this->bootstrap = true;
        $this->lang = false;

        $this->default_form_language = $this->context->language->id;
        
        parent::__construct();
    }
}