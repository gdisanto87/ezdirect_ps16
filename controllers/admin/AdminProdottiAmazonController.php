<?php

// Potrei togliere Core e trasformare in override
class AdminProdottiAmazonControllerCore extends AdminController
{
    public function __construct()
    {
        $this->displayInformation('Manca esportazione in excel');
        
        $this->bootstrap = true;
        $this->lang = false;

        $this->display = 'view';

        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Prodotti esposti su Amazon'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }
    
    public function renderView()
    {
        // Correggere: CREA EXCEL PER ESPORTAZIONE CATALOGO (vedi AmazonProducts 1.4)

        // Se è stato premuto il pulsante "Cerca"
        if(Tools::getIsset('cerca'))
        {

            $prodotti = "";
			$categorie_di_default = "";
			$macrocategorie = "";
			$macrocategorie_di_default = "";
			$categorie = "";
            $marchi = "";
            
            if(isset($_POST['per_prodotto'])){

                $strisciaprodotti = "Questi prodotti precisi: ";
				
				foreach($_POST['per_prodotto'] as $prodotto){
					$nomeprodotto = Db::getInstance()->getValue("
                        SELECT name 
                        FROM "._DB_PREFIX_."product_lang 
                        WHERE id_lang = ".(int)$this->context->language->id."
                            AND id_product = ".$prodotto
                    );
					
					$strisciaprodotti .= "- ".$nomeprodotto."<br />";

					$where_prodotti .= "p.id_product = ".$prodotto." OR ";
				}
            }

            if(isset($_POST['per_macrocategoria'])){

                $strisciamacrocat = "Prodotti in queste macrocategorie: ";
                
				foreach($_POST['per_macrocategoria'] as $macrocategoria){
					$nomemacrocat = Db::getInstance()->getValue("
                        SELECT name 
                        FROM "._DB_PREFIX_."category_lang 
                        WHERE id_lang = ".(int)$this->context->language->id."
                            AND id_category = ".$macrocategoria
                    );

					$strisciamacrocat .= "- ".$nomemacrocat."<br />";
					
					$children = Db::getInstance()->executeS("
                        SELECT id_category 
                        FROM (
                            SELECT t4.id_category
                            FROM "._DB_PREFIX_."category AS t1
                                LEFT JOIN "._DB_PREFIX_."category AS t2 ON t1.id_category = t2.id_parent
                                LEFT JOIN "._DB_PREFIX_."category AS t3 ON t2.id_category = t3.id_parent
                                LEFT JOIN "._DB_PREFIX_."category AS t4 ON t3.id_category = t3.id_parent
                            WHERE t2.id_parent = ".$macrocategoria."

                            UNION 

                            SELECT t3.id_category
                            FROM "._DB_PREFIX_."category AS t1
                                LEFT JOIN "._DB_PREFIX_."category AS t2 ON t1.id_category = t2.id_parent
                                LEFT JOIN "._DB_PREFIX_."category AS t3 ON t2.id_category = t3.id_parent
                            WHERE t2.id_parent = ".$macrocategoria."

                            UNION 

                            SELECT t2.id_category
                            FROM "._DB_PREFIX_."category AS t1
                                LEFT JOIN "._DB_PREFIX_."category AS t2 ON t1.id_category = t2.id_parent
                            WHERE t2.id_parent = ".$macrocategoria."

                            UNION 

                            SELECT id_category 
                            FROM "._DB_PREFIX_."category 
                            WHERE id_category = ".$macrocategoria."
                        ) ccc 
                        WHERE id_category IS NOT NULL 
                        ORDER BY id_category ASC
					");

					foreach ($children as $child) {
						$macrocategorie_di_default.= "p.id_category_default = ".$child['id_category']." OR ";
						$where_macrocategorie .= "cp.id_category = ".$child['id_category']." OR ";
					}
				}
            }
            
            if(isset($_POST['per_sottocategoria'])){

                $strisciacat = "Prodotti in queste sottocategorie: ";
		
				foreach ($_POST['per_sottocategoria'] as $categoria) {
					$nomecat = Db::getInstance()->getValue("
                        SELECT name 
                        FROM "._DB_PREFIX_."category_lang 
                        WHERE id_lang = ".(int)$this->context->language->id."
                            AND id_category = ".$categoria
                    );
                    
                    $strisciacat .= "- ".$nomecat."<br />";
                    
					$categorie_di_default .= "p.id_category_default = ".$categoria." OR ";
					$where_categorie .= "cp.id_category = ".$categoria." OR ";
				}	
            }

            if(isset($_POST['per_marchio'])){

                $strisciamarchi = "Prodotti in questi marchi: ";
		
				foreach ($_POST['per_marchio'] as $marchio) {
					$nomemarchio = Db::getInstance()->getValue("
                        SELECT name 
                        FROM "._DB_PREFIX_."manufacturer 
                        WHERE id_manufacturer = ".$marchio
                    );

					$strisciamarchi .= "- ".$nomemarchio."<br />";
                    
					$where_marchi .= "p.id_manufacturer = ".$marchio." OR ";
				}
            }

            $where = " ";
            // Introdotto per semplificare gestione errore query
            $id_default = '9999999999999999999999';
			
            if(isset($_POST['per_prodotto']))
				$where .= " AND (".$where_prodotti." p.id_product = '".$id_default."')";
            
            if(isset($_POST['per_macrocategoria']))
				$where .= " AND (".$where_macrocategorie." cp.id_category = ".$id_default.")";

			if(isset($_POST['per_sottocategoria']))
				$where .= " AND (".$where_categorie." cp.id_category = ".$id_default.")";

			if(isset($_POST['per_marchio']))
                $where .= " AND (".$where_marchi." p.id_manufacturer = ".$id_default.")";
            
            $sql = '
                SELECT p.id_product, reference, supplier_reference, ean13, asin, m.name as manufacturer, pl.name as nome_prodotto, 
                    amazon, id_supplier, quantity as quantita, stock_quantity, supplier_quantity as allnet_quantity, 
                    amazon_quantity, scorta_minima_amazon, itancia_quantity, esprinet_quantity, attiva_quantity, 
                    techdata_quantity, intracom_quantity, it, fr, es, uk, de, nl, se, pl, p.price as prezzo, p.sku_amazon, 
                    p.no_amazon, ap.price as amazon_price, ap.last_price, ap.qt_last, ap.price_prime, 
                    p.id_manufacturer, supplier_quantity, p.amazon_it 
                FROM '._DB_PREFIX_.'product p 
                    JOIN '._DB_PREFIX_.'manufacturer m ON p.id_manufacturer = m.id_manufacturer 
                    LEFT JOIN amazon_products ap ON p.id_product = ap.id_product 
                    JOIN '._DB_PREFIX_.'category_product cp ON p.id_product = cp.id_product 
                    JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product = pl.id_product 
                WHERE (p.amazon != "" AND p.amazon != "0") 
                    '.$where.' 
                    AND pl.id_lang = '.(int)$this->context->language->id.'
                GROUP BY p.id_product 
                ORDER BY p.reference ASC
            ';
            
            $prodotti = Db::getInstance()->executeS($sql);
            $lista_prodotti = array();

			$tipo_qt = '';
			
			foreach($prodotti as $p)
			{
                // $prezzo_standard = Product::trovaMigliorPrezzo($p['id_product'], 1, 99);
				$prezzo_standard = $p['prezzo'];        

                // id_tax = 1: IVA IT 22%
                $tax_rate = Db::getInstance()->getValue('SELECT rate FROM '._DB_PREFIX_.'tax WHERE id_tax = 1');
				
                $id_speciale = Product::trovaIdPrezzoSpeciale($p['id_product']);
            
				if($id_speciale > 0)
				{
					$speciale = Product::trovaPrezzoSpeciale($p['id_product'], 1);
					// $speciale = $speciale + (($speciale*22)/100);
					// $speciale = round($speciale, 2);
					$prezzo_standard = $speciale;
				
					if($speciale != 0){
						if($p['prezzo'] < $speciale){
							$speciale = '';
							$data_speciale = '';
							$data_inizio_speciale = '';
							$sale = '';
						}
						else{
							$data_speciale = Db::getInstance()->getValue('
                                SELECT sp.to 
                                FROM '._DB_PREFIX_.'specific_price sp 
                                WHERE sp.id_specific_price = '.$id_speciale
                            );
							$data_speciale = substr($data_speciale, 0, 10);
							$data_inizio_speciale = date("Y-m-d");
                            
							/* $sale = '<Sale>
                            <StartDate>'.$data_inizio_speciale.'T00:00:00Z</StartDate>
                            <EndDate>'.$data_speciale.'T23:59:59Z</EndDate>
                            <SalePrice currency="EUR">'.$speciale.'</SalePrice>
                            </Sale>'; */
						}
					}
					else
					{
						$speciale = '';
						$data_speciale = '';
						$data_inizio_speciale = '';
						$sale = '';
					}
				}
				else
				{
					$speciale = '';
					$data_speciale = '';
					$data_inizio_speciale = '';
					$sale = '';
				}
				
                $prezzo_standard = $prezzo_standard - ($prezzo_standard * ($p['amazon_it'] / 100));
				$prezzo = $prezzo_standard + (($prezzo_standard * $tax_rate) / 100);
				$prezzo = round($prezzo, 2);
				$p['amazon_price'] = round($p['amazon_price'], 2);
				
				if(($p['id_manufacturer'] == 117 || $p['id_manufacturer'] == 37 || $p['id_manufacturer'] == 43 || $p['id_manufacturer'] == 25 || $p['id_manufacturer'] == 108 || $p['id_manufacturer'] == 133 || $p['id_manufacturer'] == 184 || $p['id_manufacturer'] == 116 || $p['id_manufacturer'] == 182 || $p['id_manufacturer'] == 27 || $p['id_manufacturer'] == 120 || $p['id_manufacturer'] == 130 || $p['id_manufacturer'] == 158))
				{
					$supplier =  Db::getInstance()->getValue('
                        SELECT id_supplier 
                        FROM '._DB_PREFIX_.'product 
                        WHERE id_product = '.$p['id_product']
                    );
			
					switch($supplier){
                        case 11: 
                            $main_supplier = 'supplier_quantity '; 
                            break;
                        case 8: 
                            $main_supplier = 'esprinet_quantity '; 
                            break;
                        case 42: 
                            $main_supplier = 'itancia_quantity '; 
                            break;
                        case 76: 
                            $main_supplier = 'asit_quantity '; 
                            break;
                        case 1450: 
                            $main_supplier = 'esprinet_quantity '; 
                            break;
                        default: 
                            $main_supplier = 'quantity'; 
                            break;
					}
				
					if($supplier == 11 || $supplier == 8 || $supplier == 42  || $supplier == 76){
						$quantita = Db::getInstance()->getValue('
                            SELECT '.$main_supplier.' AS quantita 
                            FROM '._DB_PREFIX_.'product 
                            WHERE id_product = '.$p['id_product']
                        );
						$quantita += $p['stock_quantity'];
					}
					else{
						$quantita = Db::getInstance()->getValue('
                            SELECT quantity AS quantita 
                            FROM '._DB_PREFIX_.'product 
                            WHERE id_product = '.$p['id_product']
                        );
                    }
                    
					if($p['id_manufacturer'] == 182){
						$quantita = Db::getInstance()->getValue('
                            SELECT quantity 
                            FROM '._DB_PREFIX_.'product 
                            WHERE id_product = '.$p['id_product']
                        );
                    }

					$tipo_qt = 'EZ+F';
				}
				else
				{
					$quantita = $p['stock_quantity'];
					$tipo_qt = 'EZ';
				}
				
				if($p['id_manufacturer'] == 17){
					$quantita = Db::getInstance()->getValue('
                        SELECT stock_quantity AS main_quantity 
                        FROM '._DB_PREFIX_.'product 
                        WHERE id_product = '.$p['id_product']
                    );
                }
					
				if($quantita < 0)
                    $quantita = 0;
                
                $qta_ord_clienti = Db::getInstance()->getValue('
                    SELECT SUM(product_quantity - (REPLACE(cons,"0_",""))) 
                    FROM '._DB_PREFIX_.'order_detail od 
                        JOIN '._DB_PREFIX_.'orders o ON o.id_order = od.id_order
						JOIN '._DB_PREFIX_.'cart c ON c.id_cart = o.id_cart
                        JOIN '._DB_PREFIX_.'order_history oh ON 
                            (oh.id_order_history = (
                                SELECT MAX(id_order_history) 
                                FROM '._DB_PREFIX_.'order_history moh 
                                WHERE moh.id_order_state != 18 
                                    AND moh.id_order_state != 27 
                                    AND moh.id_order_state != 28 
                                    AND moh.id_order = od.id_order 
                                GROUP BY moh.id_order)
                            ) 
                        JOIN '._DB_PREFIX_.'order_state os ON (os.id_order_state = oh.id_order_state) 
                    WHERE c.name NOT LIKE "%prime%" 
                        AND od.product_id = '.$p['id_product'].' 
                        AND os.id_order_state != 4 
                        AND os.id_order_state != 5 
                        AND os.id_order_state != 6 
                        AND os.id_order_state != 7 
                        AND os.id_order_state != 8 
                        AND os.id_order_state != 14 
                        AND os.id_order_state != 16 
                        AND os.id_order_state != 20 
                        AND os.id_order_state != 26 
                        AND os.id_order_state != 31 
                        AND os.id_order_state != 32 
                        AND os.id_order_state != 35 
                    GROUP BY od.product_id
                ');

				$quantita -= $qta_ord_clienti;
			
				if($quantita != $p['it'])
                    $red = 'y';
                else
                    $red = 'n';
					
				if($p['reference'] != '')
				{
                    $tooltip = Product::showProductTooltip($p['id_product']);

					switch($p['id_supplier']){
                        case 11: 
                            $main_supplier = 'supplier_quantity'; 
                            break;
                        case 8: 
                            $main_supplier = 'esprinet_quantity'; 
                            break;
                        case 42: 
                            $main_supplier = 'itancia_quantity'; 
                            break;
                        case 76: 
                            $main_supplier = 'asit_quantity'; 
                            break;
                        case 1450: 
                            $main_supplier = 'attiva_quantity'; 
                            break;
                        default: 
                            $main_supplier = 'stock_quantity'; 
                            break;
					}
					
					$quantita_fornitore = Db::getInstance()->getValue('
                        SELECT '.$main_supplier.' AS main_quantity 
                        FROM '._DB_PREFIX_.'product 
                        WHERE id_product = '.$p['id_product']
                    ); 
					
					if($quantita_fornitore < 0)
						$quantita_fornitore = 0;
					
					if($main_supplier == 'stock_quantity')
						$quantita_fornitore = 0;

					if($p['no_amazon'] != '')
						$mancata_pubblicazione = $p['no_amazon'];
					else
                        $mancata_pubblicazione = 'Motivo mancata pubblicazione sconosciuto';

                    $row_style = ($red == 'y' && $p['ean13'] != '' && $p['asin'] != '' ? 'outline:1px solid red' : '');
                    $manufacturer_prime = ($p['sku_amazon'] != '' ? '<img src="'._PS_ADMIN_IMG_.'prime.png" alt="Prime" title="'.number_format($p['price_prime'], 2, ",", ".").'" />' : '');
                    $prezzo_it = (($p['ean13'] != '' && $p['asin'] != '') ? 'EZ: '.number_format($prezzo, 2, ",", "").'<br />AMZ: '.number_format($p['last_price'], 2, ",", "") : '');
                    $prezzo_it_style = ($p['ean13'] != '' && $p['asin'] != '' && ($prezzo - $p['last_price']) > (0.02) || ($p['last_price'] - $prezzo) > (0.02) ? 'border:1px solid red' : '');

                    // I numeri cercati con strpos() corrispondono all'id della lingua del marketplace nel database
                    $it_icon = ($p['it'] > 0 ? '<i class="icon-check" style="color:green" title="Presente su Amazon">'.$p["it"].'</i>' : ((strpos($p['amazon'], '5') !== false) ? '<i class="icon-check" title="Flaggato ma non presente su Amazon. '.$mancata_pubblicazione.'">'.$p["it"].'</i>' : ''));
                    $fr_icon = ($p['fr'] > 0 ? '<i class="icon-check" style="color:green" title="Presente su Amazon">'.$p['fr'].'</i>' : ((strpos($p['amazon'], '2') !== false) ? '<i class="icon-check" title="Flaggato ma non presente su Amazon. '.$mancata_pubblicazione.'">'.$p['fr'].'</i>' : ''));
                    $uk_icon = ($p['uk'] > 0 ? '<i class="icon-check" style="color:green" title="Presente su Amazon">'.$p['uk'].'</i>' : ((strpos($p['amazon'], '1') !== false) ? '<i class="icon-check" title="Flaggato ma non presente su Amazon. '.$mancata_pubblicazione.'">'.$p['uk'].'</i>' : ''));
                    $es_icon = ($p['es'] > 0 ? '<i class="icon-check" style="color:green" title="Presente su Amazon">'.$p['es'].'</i>' : ((strpos($p['amazon'], '3') !== false) ? '<i class="icon-check" title="Flaggato ma non presente su Amazon. '.$mancata_pubblicazione.'">'.$p['es'].'</i>' : ''));
                    $de_icon = ($p['de'] > 0 ? '<i class="icon-check" style="color:green" title="Presente su Amazon">'.$p['de'].'</i>' : ((strpos($p['amazon'], '4') !== false) ? '<i class="icon-check" title="Flaggato ma non presente su Amazon. '.$mancata_pubblicazione.'">'.$p['de'].'</i>' : ''));
                    $nl_icon = ($p['nl'] > 0 ? '<i class="icon-check" style="color:green" title="Presente su Amazon">'.$p['nl'].'</i>' : ((strpos($p['amazon'], '6') !== false) ? '<i class="icon-check" title="Flaggato ma non presente su Amazon. '.$mancata_pubblicazione.'">'.$p['nl'].'</i>' : ''));
                    $se_icon = ($p['se'] > 0 ? '<i class="icon-check" style="color:green" title="Presente su Amazon">'.$p['se'].'</i>' : ((strpos($p['amazon'], '7') !== false) ? '<i class="icon-check" title="Flaggato ma non presente su Amazon. '.$mancata_pubblicazione.'">'.$p['se'].'</i>' : ''));
                    $pl_icon = ($p['pl'] > 0 ? '<i class="icon-check" style="color:green" title="Presente su Amazon">'.$p['pl'].'</i>' : ((strpos($p['amazon'], '8') !== false) ? '<i class="icon-check" title="Flaggato ma non presente su Amazon. '.$mancata_pubblicazione.'">'.$p['pl'].'</i>' : ''));

                    $array_prodotto = array(
                        'id' => $p['id_product'],
                        'reference' => $p['reference'],
                        'tooltip' => $tooltip,
                        'supplier_reference' => $p['supplier_reference'],
                        'ean13' => $p['ean13'],
                        'asin' => $p['asin'],
                        'name' => $p['nome_prodotto'],
                        'manufacturer' => $p['manufacturer'],
                        'manufacturer_prime' => $manufacturer_prime,
                        'stock_quantity' => $p['stock_quantity'],
                        'quantita_fornitore' => $quantita_fornitore,
                        'tipo_qt' => $tipo_qt,
                        'prezzo_it' => $prezzo_it,
                        'prezzo_it_style' => $prezzo_it_style,
                        'prezzo' => $prezzo,
                        'last_price' => $p['last_price'],
                        'amazon_quantity' => $p['amazon_quantity'],
                        'scorta_minima_amazon' => $p['scorta_minima_amazon'],
                        //'it' => $p['it'],
                        //'fr' => $p['fr'],
                        //'uk' => $p['uk'],
                        //'de' => $p['de'],
                        //'es' => $p['es'],
                        //'nl' => $p['nl'],
                        //'se' => $p['se'],
                        //'pl' => $p['pl'],
                        'it' => $it_icon,
                        'fr' => $fr_icon,
                        'uk' => $uk_icon,
                        'de' => $de_icon,
                        'es' => $es_icon,
                        'nl' => $nl_icon,
                        'se' => $se_icon,
                        'pl' => $pl_icon,
                        'mancata_pubblicazione' => $mancata_pubblicazione,
                        'row_style' => $row_style,
                    );

                    $lista_prodotti[] = $array_prodotto;
				}
            }

            $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'tooltip_prodotto.js');

            $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'datatable_ready.js');
            $this->addJS(_PS_BO_DEFAULT_THEME_DIR_.'plugins/DataTables/datatables.min.js');
            $this->addCSS(_PS_BO_DEFAULT_THEME_DIR_.'plugins/DataTables/datatables.min.css');
        }
        // View iniziale
        else
        {
            // Ricerca per codice prodotto
            $prodotti = Db::getInstance()->executeS('
                SELECT p.id_product as id, p.reference, pl.name
                FROM '._DB_PREFIX_.'product p 
                    JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product = pl.id_product 
                WHERE active = 1
                    AND (p.amazon != "" AND p.amazon != "0") 
                    AND pl.id_lang = '.(int)$this->context->language->id.'
                GROUP BY p.id_product 
                ORDER BY p.reference ASC
            ');

            // Per macrocategoria
            $macrocat = Db::getInstance()->executeS("
                SELECT c.id_category as id, cl.name 
                FROM "._DB_PREFIX_."category c
                    JOIN "._DB_PREFIX_."category_lang cl ON c.id_category = cl.id_category 
                WHERE cl.id_lang = ".(int)$this->context->language->id."
                    AND c.id_parent = 1 
                ORDER BY cl.name
            ");
        
            // Per sottocategoria
            $sottocat = Db::getInstance()->executeS("
                SELECT c.id_category as id, cl.name 
                FROM "._DB_PREFIX_."category c
                    JOIN "._DB_PREFIX_."category_lang cl ON c.id_category = cl.id_category 
                WHERE cl.id_lang = ".(int)$this->context->language->id."
                    AND c.id_parent != 1 
                ORDER BY cl.name
            ");

            // Per marchio
            $marchi = Db::getInstance()->executeS("
                SELECT id_manufacturer as id, name 
                FROM "._DB_PREFIX_."manufacturer 
                ORDER BY name
            ");
        }
    
        $this->tpl_view_vars = array(
            'prodotti' => $prodotti,
            'macrocat' => $macrocat,
            'sottocat' => $sottocat,
            'marchi' => $marchi,
            'striscia_prodotti' => $strisciaprodotti,
            'striscia_macrocat' => $strisciamacrocat,
            'striscia_cat' => $strisciacat,
            'striscia_marchi' => $strisciamarchi,
            'lista_prodotti' => $lista_prodotti,
        );

        $this->addjQueryPlugin(array(
            'select2'
        ));

        $this->addJS(_PS_JS_DIR_.'jquery/plugins/select2/select2_locale_'.$this->context->language->iso_code.'.js');
        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'select2_ready.js');

        return parent::renderView();
    }
}