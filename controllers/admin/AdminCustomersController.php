<?php
/*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @property Customer $object
 */
class AdminCustomersControllerCore extends AdminController
{
    protected $delete_mode;

    protected $_defaultOrderBy = 'date_add';
    protected $_defaultOrderWay = 'DESC';
    protected $can_add_customer = true;
    protected static $meaning_status = array();

    public function __construct()
    {
        $this->bootstrap = true;
        $this->required_database = true;
        $this->required_fields = array('newsletter','optin');
        $this->table = 'customer';
        $this->className = 'Customer';
        $this->lang = false;
        $this->deleted = true;
        $this->explicitSelect = true;

        $this->allow_export = true;

        $this->addRowAction('edit');
        $this->addRowAction('view');
        $this->addRowAction('delete');
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?'),
                'icon' => 'icon-trash'
            )
        );

        $this->context = Context::getContext();

        $this->default_form_language = $this->context->language->id;

        $titles_array = array();
        $genders = Gender::getGenders($this->context->language->id);
        foreach ($genders as $gender) {
            /** @var Gender $gender */
            $titles_array[$gender->id_gender] = $gender->name;
        }

        $this->_join = 'LEFT JOIN '._DB_PREFIX_.'gender_lang gl ON (a.id_gender = gl.id_gender AND gl.id_lang = '.(int)$this->context->language->id.')
            LEFT JOIN '._DB_PREFIX_.'group_lang grl ON (a.id_default_group = grl.id_group AND grl.id_lang = '.(int)$this->context->language->id.')
        ';

        // Se sono un agente, vedo solo i miei clienti
        if($this->context->employee->id_profile == 7){
            $this->_join .= 'LEFT JOIN customer_amministrazione ca ON ca.`id_customer` = a.`id_customer`';
            $this->_where .= 'AND ca.agente = '.$this->context->employee->id;
        }

        // correggere, join sbagliato: esclude i clienti che non hanno indirizzi
        /*LEFT JOIN `'._DB_PREFIX_.'address` ad ON ad.`id_customer` = a.`id_customer`
            LEFT JOIN `'._DB_PREFIX_.'state` s ON s.`id_state` = ad.`id_state`*/

        // controllare regola nella 1.4
        //$this->_where .= 'AND ad.active = 1 AND ad.fatturazione = 1';
        
        $this->_use_found_rows = false;

        // Questo stampa l'header della tab
        $this->fields_list = array(
            'id_customer' => array(
                'title' => $this->l('ID'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs'
            ),/*
            'title' => array(
                'title' => $this->l('Social title'),
                'filter_key' => 'a!id_gender',
                'type' => 'select',
                'list' => $titles_array,
                'filter_type' => 'int',
                'order_key' => 'gl!name'
            ),*/
            'persona' => array(
                'title' => $this->l('Person'), 
                'filter_key' => 'CONCAT(a!firstname, " ", a!lastname)',
                'prefix' => '<a onclick="alert(\'I am an alert box!\');">', // prova! eliminare
                'suffix' => '</a>', // prova! eliminare
            ),/*
            'firstname' => array(
                'title' => $this->l('First name')
            ),
            'lastname' => array(
                'title' => $this->l('Last name')
            ),*/
        );

        // si potrebbe anche togliere da B2B
        if (Configuration::get('PS_B2B_ENABLE')) {
            $this->fields_list = array_merge($this->fields_list, array(
                // rinominato per evitare ambiguità nella query
                'azienda' => array(
                    'title' => $this->l('Company'),
                    'filter_key' => 'a!company'
                ),
            ));
        }

        $this->fields_list = array_merge($this->fields_list, array(
            /*'total_spent' => array(
                'title' => $this->l('Sales'),
                'type' => 'price',
                'search' => false,
                'havingFilter' => true,
                'align' => 'text-right',
                'badge_success' => true
            ),*/
            'name' => array(
                'title' => $this->l('Group'),
                'align' => 'text-left',
                'filter_key' => 'a!id_default_group'
            ),/*
            'ex_rivenditore' => array(
                'title' => $this->l('Ex Riv.?'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
                'type' => 'select',
                'list' => array(1 => 'Sì', 0 => 'No'),
                'filter_key' => 'a!ex_rivenditore',
                'callback' => 'printIcon'
            ),
            'supplier' => array(
                'title' => $this->l('Forn.?'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
                'type' => 'select',
                'list' => array(1 => 'Sì', 0 => 'No'),
                'filter_key' => 'a!supplier',
                'callback' => 'printIcon'
            ),*//*
            'provincia' => array(
                'title' => $this->l('Prov.'), 
                'filter_key' => 's!name'
            ),
            'regione' => array(
                'title' => $this->l('Reg.'), 
                'filter_key' => 's!regione'
            ),*/
            // manca vat_number, va nello stesso campo di tax_code; se è comunque contenuto in entrambi, va bene così
            'tax_code'  => array(
                'title' => $this->l('PI/CF'), 
                'filter_key' => 'a!tax_code'
            ),
            'email' => array(
                'title' => $this->l('Email address')
            ), // OVERRIDE: gestione di active = 2 nella list
            'active' => array(
                'title' => $this->l('Enabled'),
                'align' => 'text-center',
                //'active' => 'status', 
                //'type' => 'bool',
                'type' => 'select',
                'list' => array(1 => 'Sì', 0 => 'No', 2 => 'Cessata Attività'),
                'callback' => 'printActiveIcon',
                'orderby' => false,
                'filter_key' => 'a!active'
            ),
            'newsletter' => array(
                'title' => $this->l('Newsletter'),
                'align' => 'text-center',
                'type' => 'bool',
                'callback' => 'printNewsIcon', // stampa icona e rende value modificabile dalla lista
                'orderby' => false
            ),
            'optin' => array(
                'title' => $this->l('Opt-in'),
                'align' => 'text-center',
                'type' => 'bool',
                'callback' => 'printOptinIcon', // stampa icona e rende value modificabile dalla lista
                'orderby' => false
            ),
            'date_add' => array(
                'title' => $this->l('Registration'),
                'type' => 'date',
                'align' => 'text-right'
            ),
            'connect' => array(
                'title' => $this->l('Last visit'),
                'type' => 'date',
                'search' => false,
                'havingFilter' => true
            )
        ));

        $this->shopLinkType = 'shop';
        $this->shopShareDatas = Shop::SHARE_CUSTOMER;

        parent::__construct();

        $this->_select = '
        grl.name, a.date_add, gl.name as title, (
            SELECT SUM(total_paid_real / conversion_rate)
            FROM '._DB_PREFIX_.'orders o
            WHERE o.id_customer = a.id_customer
            '.Shop::addSqlRestriction(Shop::SHARE_ORDER, 'o').'
            AND o.valid = 1
        ) as total_spent, (
            SELECT c.date_add FROM '._DB_PREFIX_.'guest g
            LEFT JOIN '._DB_PREFIX_.'connections c ON c.id_guest = g.id_guest
            WHERE g.id_customer = a.id_customer
            ORDER BY c.date_add DESC
            LIMIT 1
        ) as connect';

        // Check if we can add a customer
        if (Shop::isFeatureActive() && (Shop::getContext() == Shop::CONTEXT_ALL || Shop::getContext() == Shop::CONTEXT_GROUP)) {
            $this->can_add_customer = false;
        }

        self::$meaning_status = array(
            'open' => $this->l('Open'),
            'closed' => $this->l('Closed'),
            'pending1' => $this->l('Pending 1'),
            'pending2' => $this->l('Pending 2')
        );
    }

    // override
    public function printIcon($value)
    {
        return ($value ? '<i class="icon-check" style="color:green;"></i>' : '<i class="icon-remove" style="color:red;"></i>');
    }

    public function postProcess()
    {
        if (!$this->can_add_customer && $this->display == 'add') {
            $this->redirect_after = $this->context->link->getAdminLink('AdminCustomers');
        }

        // override: correggere!

        //if(Tools::getIsset('submitAddcustomer')) // vedi 1.4
        //if(Tools::getIsset('submitAdd')) // vedi 1.4

        parent::postProcess();
    }

    public function initContent()
    {
        if ($this->action == 'select_delete') {
            $this->context->smarty->assign(array(
                'delete_form' => true,
                'url_delete' => htmlentities($_SERVER['REQUEST_URI']),
                'boxes' => $this->boxes,
            ));
        }

        if (!$this->can_add_customer && !$this->display) {
            $this->informations[] = $this->l('You have to select a shop if you want to create a customer.');
        }

        parent::initContent();
    }

    public function initToolbar()
    {
        parent::initToolbar();

        if (!$this->can_add_customer) {
            unset($this->toolbar_btn['new']);
        } elseif (!$this->display && $this->can_import) {
            $this->toolbar_btn['import'] = array(
                'href' => $this->context->link->getAdminLink('AdminImport', true).'&import_type=customers',
                'desc' => $this->l('Import')
            );
        }
    }

    public function getList($id_lang, $orderBy = null, $orderWay = null, $start = 0, $limit = null, $id_lang_shop = null)
    {
        parent::getList($id_lang, $orderBy, $orderWay, $start, $limit, $id_lang_shop);

        if ($this->_list) {
            foreach ($this->_list as &$row) {
                $row['badge_success'] = $row['total_spent'] > 0;
            }
        }
    }


    public function initToolbarTitle()
    {
        parent::initToolbarTitle();

        switch ($this->display) {
            case '':
            case 'list':
                array_pop($this->toolbar_title);
                $this->toolbar_title[] = $this->l('Manage your Customers');
                break;
            case 'view':
                /** @var Customer $customer */
                if (($customer = $this->loadObject(true)) && Validate::isLoadedObject($customer)) {
                    array_pop($this->toolbar_title);
                    $company_or_name = ($customer->is_company ? $customer->company : $customer->firstname.' '.$customer->lastname);
                    $this->toolbar_title[] = sprintf($this->l('%s'), $company_or_name.' (#'.$customer->id.')');
                    //$this->toolbar_title[] = sprintf($this->l('Information about Customer: %s'), Tools::substr($customer->firstname, 0, 1).'. '.$customer->lastname);
                }
                break;
            case 'add':
            case 'edit':
                array_pop($this->toolbar_title);
                /** @var Customer $customer */
                if (($customer = $this->loadObject(true)) && Validate::isLoadedObject($customer)) {
                    $this->toolbar_title[] = sprintf($this->l('Editing Customer: %s'), Tools::substr($customer->firstname, 0, 1).'. '.$customer->lastname);
                } else {
                    $this->toolbar_title[] = $this->l('Creating a new Customer');
                }
                break;
        }

        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }

    public function initPageHeaderToolbar()
    {
        // Override: pulsante "Add new customer" visibile anche in view
        //if (empty($this->display) && $this->can_add_customer) {
        if ($this->can_add_customer) {
            $this->page_header_toolbar_btn['new_customer'] = array(
                'href' => self::$currentIndex.'&addcustomer&token='.$this->token,
                'desc' => $this->l('Add new customer', null, null, false),
                'icon' => 'process-icon-new'
            );
        }

        parent::initPageHeaderToolbar();
    }

    public function initProcess()
    {
        parent::initProcess();

        if (Tools::isSubmit('submitGuestToCustomer') && $this->id_object) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'guest_to_customer';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        } elseif (Tools::isSubmit('changeNewsletterVal') && $this->id_object) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'change_newsletter_val';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        } elseif (Tools::isSubmit('changeOptinVal') && $this->id_object) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'change_optin_val';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        }
        /* OVERRIDE: gestione di active = 2 nella list */
        elseif (Tools::isSubmit('changeActiveVal') && $this->id_object) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'change_active_val';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        }

        // When deleting, first display a form to select the type of deletion
        if ($this->action == 'delete' || $this->action == 'bulkdelete') {
            if (Tools::getValue('deleteMode') == 'real' || Tools::getValue('deleteMode') == 'deleted') {
                $this->delete_mode = Tools::getValue('deleteMode');
            } else {
                $this->action = 'select_delete';
            }
        }

        // override: nella 1.4 era in postProcess (non c'era initProcess), correggere
        if(Tools::getIsset('getPDF')) {
        
            require_once('../classes/html2pdf/html2pdf.class.php');
            
            $id = Tools::getValue('id');
            $tipo = Tools::getValue('tipo');
            
            $content = Customer::exportConversation($id, $tipo);
            
            ob_clean(); ob_end_clean(); 
            $html2pdf = new HTML2PDF('P','A4','it');
            $html2pdf->setTestTdInOnePage(false);
            $html2pdf->WriteHTML($content);
            
            $html2pdf->Output($tipo.'-'.$id.'.pdf', 'D'); 
        }
    }

    public function renderList()
    {
        if ((Tools::isSubmit('submitBulkdelete'.$this->table) || Tools::isSubmit('delete'.$this->table)) && $this->tabAccess['delete'] === '1') {
            $this->tpl_list_vars = array(
                'delete_customer' => true,
                'REQUEST_URI' => $_SERVER['REQUEST_URI'],
                'POST' => $_POST
            );
        }

        /* OVERRIDE */
        // Datatable per le ultime attività
        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'datatable_ready.js');
        $this->addJS(_PS_BO_DEFAULT_THEME_DIR_.'plugins/DataTables/datatables.min.js');
        $this->addCSS(_PS_BO_DEFAULT_THEME_DIR_.'plugins/DataTables/datatables.min.css');

        return parent::renderList();
    }


    // Modifica del cliente selezionato e aggiunta di un nuovo cliente
    public function renderForm()
    {
        /** @var Customer $obj */
        if (!($obj = $this->loadObject(true))) {
            return;
        }

        $genders = Gender::getGenders();
        $list_genders = array();
        foreach ($genders as $key => $gender) {
            /** @var Gender $gender */
            $list_genders[$key]['id'] = 'gender_'.$gender->id;
            $list_genders[$key]['value'] = $gender->id;
            $list_genders[$key]['label'] = $gender->name;
        }

        $years = Tools::dateYears();
        $months = Tools::dateMonths();
        $days = Tools::dateDays();

        $groups = Group::getGroups($this->default_form_language, true);

        $tax_regime = array(
            array(
                'value' => 0,
                'name' => 'Base (Italia: IVA al 22%)'
            ),
            array(
                'value' => 2,
                'name' => 'IVA al 10%'
            ),
            array(
                'value' => 3,
                'name' => 'IVA al 4%'
            ),
            array(
                'value' => 1,
                'name' => 'Esente'
            ),
            array(
                'value' => 4,
                'name' => 'Split Payment con IVA al 22%'
            ),
        );

        $settori = array(
            array(
                'value' => '',
                'name' => '-- Seleziona --'
            ),
            array(
                'value' => 'Call Center',
                'name' => 'Call Center'
            ),
            array(
                'value' => 'Commercio',
                'name' => 'Commercio'
            ),
            array(
                'value' => 'Hotel',
                'name' => 'Hotel'
            ),
            array(
                'value' => 'Industria',
                'name' => 'Industria'
            ),
            array(
                'value' => 'Meccanica',
                'name' => 'Meccanica'
            ),
            array(
                'value' => 'Ristorazione',
                'name' => 'Ristorazione'
            ),
            array(
                'value' => 'Servizi',
                'name' => 'Servizi'
            ),
            array(
                'value' => 'Telecomunicazioni',
                'name' => 'Telecomunicazioni'
            ),
            array(
                'value' => 'Turismo',
                'name' => 'Turismo'
            ),
            array(
                'value' => 'Altro',
                'name' => 'Altro'
            ),
        );

        $canali = array(
            array(
                'value' => '',
                'name' => '-- Seleziona --'
            ),
            array(
                'value' => 'Google',
                'name' => 'Google'
            ),
            array(
                'value' => 'Amazon',
                'name' => 'Amazon'
            ),
            array(
                'value' => 'DEM',
                'name' => 'DEM'
            ),
            array(
                'value' => 'ePrice',
                'name' => 'ePrice'
            ),
            array(
                'value' => 'Facebook',
                'name' => 'Facebook'
            ),
            array(
                'value' => 'Inserzione sul giornale',
                'name' => 'Inserzione sul giornale'
            ),
            array(
                'value' => 'Linkedin',
                'name' => 'Linkedin'
            ),
            array(
                'value' => 'Link diretto',
                'name' => 'Link diretto'
            ),
            array(
                'value' => 'Link su altro sito',
                'name' => 'Link su altro sito'
            ),
            array(
                'value' => 'Altro motore di ricerca (Bing, Yahoo, Duckduckgo ecc.)',
                'name' => 'Altro motore di ricerca (Bing, Yahoo, Duckduckgo ecc.)'
            ),
            array(
                'value' => 'Paid (Adwords, Inserzioni Facebook ecc.)',
                'name' => 'Paid (Adwords, Inserzioni Facebook ecc.)'
            ),
            array(
                'value' => 'Passaparola',
                'name' => 'Passaparola'
            ),
            array(
                'value' => 'Trovaprezzi',
                'name' => 'Trovaprezzi'
            ),
        );

        $employees_number = array(
            array(
                'value' => 'Da 1 a 10',
                'name' => 'Da 1 a 10'
            ),
            array(
                'value' => 'Da 11 a 50',
                'name' => 'Da 11 a 50'
            ),
            array(
                'value' => 'Più di 50',
                'name' => 'Più di 50'
            ),
        );

        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Customer'),
                'icon' => 'icon-user'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Company'),
                    'name' => 'company',
                    'required' => false,
                    'col' => '4'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('First name'),
                    'name' => 'firstname',
                    'required' => true,
                    'col' => '4',
                    'hint' => $this->l('Invalid characters:').' 0-9!&lt;&gt;,;?=+()@#"°{}_$%:'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Last name'),
                    'name' => 'lastname',
                    'required' => true,
                    'col' => '4',
                    'hint' => $this->l('Invalid characters:').' 0-9!&lt;&gt;,;?=+()@#"°{}_$%:'
                ),
                array(
                    'type' => 'text',
                    'prefix' => '<i class="icon-envelope-o"></i>',
                    'label' => $this->l('Email address'),
                    'name' => 'email',
                    'col' => '4',
                    'required' => true,
                    'autocomplete' => false
                ),
                array(
                    'type' => 'text',
                    'prefix' => '<i class="icon-envelope-o"></i>',
                    'label' => $this->l('PEC'),
                    'name' => 'pec',
                    'col' => '4',
                    'autocomplete' => false
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('eSolver Code'),
                    'name' => 'codice_esolver',
                    'col' => '4',
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('VAT Number'),
                    'name' => 'vat_number',
                    'col' => '4',
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Tax Code'),
                    'name' => 'tax_code',
                    'col' => '4',
                    'required' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Telephone'),
                    'name' => 'telefono_principale',
                    'col' => '4',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Mobile Phone'),
                    'name' => 'cellulare_principale',
                    'col' => '4',
                    'required' => false,
                ),/* // correggere: fax è in ps_address
                array(
                    'type' => 'text',
                    'label' => $this->l('Fax'),
                    'name' => 'fax',
                    'col' => '4',
                    'required' => false,
                ),*/
                array(
                    'type' => 'password',
                    'label' => $this->l('New Password'),
                    'name' => 'passwd',
                    'required' => ($obj->id ? false : true),
                    'col' => '4',
                    'hint' => ($obj->id ? $this->l('Leave this field blank if there\'s no change.') :
                        sprintf($this->l('Password should be at least %s characters long.'), Validate::PASSWORD_LENGTH))
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('SDI'),
                    'name' => 'codice_univoco',
                    'col' => '4',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('IPA'),
                    'name' => 'ipa',
                    'col' => '4',
                    'required' => false,
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Tax Regime'),
                    'name' => 'tax_regime',
                    'col' => '4',
                    'required' => false,
                    'options' => array(
                        'query' => $tax_regime,
                        'id' => 'value',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Sector'),
                    'name' => 'settore',
                    'col' => '4',
                    'required' => false,
                    'options' => array(
                        'query' => $settori,
                        'id' => 'value',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Channel'),
                    'name' => 'canale',
                    'col' => '4',
                    'required' => false,
                    'options' => array(
                        'query' => $canali,
                        'id' => 'value',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Employees Number'),
                    'name' => 'employees_number',
                    'col' => '4',
                    'required' => false,
                    'options' => array(
                        'query' => $employees_number,
                        'id' => 'value',
                        'name' => 'name'
                    )
                ),
                array( // correggere: non stampa label ma yes / no (ovunque); con type 'radio' funziona
                    'type' => 'switch',
                    'label' => $this->l('Customer Type'),
                    'name' => 'is_company',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'is_company_on',
                            'value' => 1,
                            'label' => $this->l('Company')
                        ),
                        array(
                            'id' => 'is_company_off',
                            'value' => 0,
                            'label' => $this->l('Individual')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Supplier'),
                    'name' => 'supplier',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'supplier_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'supplier_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Risk Customer'),
                    'name' => 'id_risk',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'risk_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'risk_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ),
                ),
                array(
                    'type' => 'birthday',
                    'label' => $this->l('Birthday'),
                    'name' => 'birthday',
                    'options' => array(
                        'days' => $days,
                        'months' => $months,
                        'years' => $years
                    )
                ),
                array(
                    'type' => 'radio',
                    'label' => $this->l('Social title'),
                    'name' => 'id_gender',
                    'required' => false,
                    'class' => 't',
                    'values' => array(
                        //$list_genders,
                        array(
                            'id' => 'gender_1',
                            'value' => 1,
                            'label' => $this->l('M')
                        ),
                        array(
                            'id' => 'gender_2',
                            'value' => 2,
                            'label' => $this->l('F')
                        ),
                        array(
                            'id' => 'gender_9', // controllare valore;
                            'value' => 9, // controllare valore; aggiungerlo alle tabelle gender e gender_lang
                            'label' => $this->l('Unknown')
                        ),
                    )
                ),
            )
        );

        // Tolgo "active" dal form di add customer (deve essere attivo di default)
        if(Tools::getIsset('updatecustomer')){
            $this->fields_form['input'] = array_merge(
                $this->fields_form['input'],
                array(
                    array(
                        'type' => 'radio',
                        'label' => $this->l('Enabled'),
                        'name' => 'active',
                        'required' => false,
                        'class' => 't',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            ),
                            array(
                                'id' => 'cessata',
                                'value' => 2,
                                'label' => $this->l('Cessata attività')
                            )
                        ),
                        'hint' => $this->l('Enable or disable customer login.')
                    ),
                )
            );
        }

        $this->fields_form['input'] = array_merge(
            $this->fields_form['input'],
            array(
                array(
                    'type' => 'switch',
                    'label' => $this->l('Newsletter'),
                    'name' => 'newsletter',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'newsletter_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'newsletter_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                    'disabled' =>  (bool)!Configuration::get('PS_CUSTOMER_NWSL'),
                    'hint' => $this->l('This customer will receive your newsletter via email.')
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Opt-in'),
                    'name' => 'optin',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'optin_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'optin_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                    'disabled' =>  (bool)!Configuration::get('PS_CUSTOMER_OPTIN'),
                    'hint' => $this->l('This customer will receive your ads via email.')
                ),
            )
        );

        // if we add a customer via fancybox (ajax), it's a customer and he doesn't need to be added to the visitor and guest groups
        if (Tools::isSubmit('addcustomer') && Tools::isSubmit('submitFormAjax')) {
            $visitor_group = Configuration::get('PS_UNIDENTIFIED_GROUP');
            $guest_group = Configuration::get('PS_GUEST_GROUP');
            foreach ($groups as $key => $g) {
                if (in_array($g['id_group'], array($visitor_group, $guest_group))) {
                    unset($groups[$key]);
                }
            }
        }        

        $this->fields_form['input'] = array_merge(
            $this->fields_form['input'],
            array(
                /*
                // selezione multipla del gruppo di accesso ( secondo il precedente portale bisogna tenerne attivo solo uno per cliente perchè lo identifica )
                array(
                    'type' => 'group',
                    'label' => $this->l('Group access'),
                    'name' => 'groupBox',
                    'values' => $groups,
                    'required' => true,
                    'col' => '6',
                    'hint' => $this->l('Select all the groups that you would like to apply to this customer.')
                ),
                */
                array(
                    'type' => 'select',
                    'label' => $this->l('Profile'),
                    'name' => 'id_default_group',
                    'options' => array(
                        'query' => $groups,
                        'id' => 'id_group',
                        'name' => 'name'
                    ),
                    'col' => '4',
                    'hint' => array(
                        $this->l('This group will be the user\'s default group.'),
                        //$this->l('Only the discount for the selected group will be applied to this customer.')
                    )
                )
            )
        );

        // if customer is a guest customer, password hasn't to be there
        if ($obj->id && ($obj->is_guest && $obj->id_default_group == Configuration::get('PS_GUEST_GROUP'))) {
            foreach ($this->fields_form['input'] as $k => $field) {
                if ($field['type'] == 'password') {
                    array_splice($this->fields_form['input'], $k, 1);
                }
            }
        }

        if (Configuration::get('PS_B2B_ENABLE')) {
            $risks = Risk::getRisks();

            $list_risks = array();
            foreach ($risks as $key => $risk) {
                /** @var Risk $risk */
                $list_risks[$key]['id_risk'] = (int)$risk->id;
                $list_risks[$key]['name'] = $risk->name;
            }
            /* // Messo nell'array principale
            $this->fields_form['input'][] = array(
                'type' => 'text',
                'label' => $this->l('Company'),
                'name' => 'company'
            );*/
            $this->fields_form['input'][] = array(
                'type' => 'text',
                'label' => $this->l('SIRET'),
                'name' => 'siret'
            );
            $this->fields_form['input'][] = array(
                'type' => 'text',
                'label' => $this->l('APE'),
                'name' => 'ape'
            );
            $this->fields_form['input'][] = array(
                'type' => 'text',
                'label' => $this->l('Website'),
                'name' => 'website'
            );
            $this->fields_form['input'][] = array(
                'type' => 'text',
                'label' => $this->l('Allowed outstanding amount'),
                'name' => 'outstanding_allow_amount',
                'hint' => $this->l('Valid characters:').' 0-9',
                'suffix' => $this->context->currency->sign
            );
            $this->fields_form['input'][] = array(
                'type' => 'text',
                'label' => $this->l('Maximum number of payment days'),
                'name' => 'max_payment_days',
                'hint' => $this->l('Valid characters:').' 0-9'
            );
            $this->fields_form['input'][] = array(
                'type' => 'select',
                'label' => $this->l('Risk rating'),
                'name' => 'id_risk',
                'required' => false,
                'class' => 't',
                'options' => array(
                    'query' => $list_risks,
                    'id' => 'id_risk',
                    'name' => 'name',
                ),
            );
        }

        $this->fields_form['submit'] = array(
            'title' => $this->l('Save'),
        );

        $birthday = explode('-', $this->getFieldValue($obj, 'birthday'));

        $this->fields_value = array(
            'years' => $this->getFieldValue($obj, 'birthday') ? $birthday[0] : 0,
            'months' => $this->getFieldValue($obj, 'birthday') ? $birthday[1] : 0,
            'days' => $this->getFieldValue($obj, 'birthday') ? $birthday[2] : 0,
        );

        // Added values of object Group
        if (!Validate::isUnsignedId($obj->id)) {
            $customer_groups = array();
        } else {
            $customer_groups = $obj->getGroups();
        }
        $customer_groups_ids = array();
        if (is_array($customer_groups)) {
            foreach ($customer_groups as $customer_group) {
                $customer_groups_ids[] = $customer_group;
            }
        }

        // if empty $carrier_groups_ids : object creation : we set the default groups
        if (empty($customer_groups_ids)) {
            $preselected = array(Configuration::get('PS_UNIDENTIFIED_GROUP'), Configuration::get('PS_GUEST_GROUP'), Configuration::get('PS_CUSTOMER_GROUP'));
            $customer_groups_ids = array_merge($customer_groups_ids, $preselected);
        }

        foreach ($groups as $group) {
            $this->fields_value['groupBox_'.$group['id_group']] =
                Tools::getValue('groupBox_'.$group['id_group'], in_array($group['id_group'], $customer_groups_ids));
        }

        return parent::renderForm();
    }

    public function beforeAdd($customer)
    {
        $customer->id_shop = $this->context->shop->id;
    }

    public function renderKpis()
    {
        $time = time();
        $kpis = array();

        /* The data generation is located in AdminStatsControllerCore */

        $helper = new HelperKpi();
        $helper->id = 'box-gender';
        $helper->icon = 'icon-male';
        $helper->color = 'color1';
        $helper->title = $this->l('Customers', null, null, false);
        $helper->subtitle = $this->l('All Time', null, null, false);
        if (ConfigurationKPI::get('CUSTOMER_MAIN_GENDER', $this->context->language->id) !== false) {
            $helper->value = ConfigurationKPI::get('CUSTOMER_MAIN_GENDER', $this->context->language->id);
        }
        $helper->source = $this->context->link->getAdminLink('AdminStats').'&ajax=1&action=getKpi&kpi=customer_main_gender';
        $helper->refresh = (bool)(ConfigurationKPI::get('CUSTOMER_MAIN_GENDER_EXPIRE', $this->context->language->id) < $time);
        $kpis[] = $helper->generate();

        $helper = new HelperKpi();
        $helper->id = 'box-age';
        $helper->icon = 'icon-calendar';
        $helper->color = 'color2';
        $helper->title = $this->l('Average Age', 'AdminTab', null, false);
        $helper->subtitle = $this->l('All Time', null, null, false);
        if (ConfigurationKPI::get('AVG_CUSTOMER_AGE', $this->context->language->id) !== false) {
            $helper->value = ConfigurationKPI::get('AVG_CUSTOMER_AGE', $this->context->language->id);
        }
        $helper->source = $this->context->link->getAdminLink('AdminStats').'&ajax=1&action=getKpi&kpi=avg_customer_age';
        $helper->refresh = (bool)(ConfigurationKPI::get('AVG_CUSTOMER_AGE_EXPIRE', $this->context->language->id) < $time);
        $kpis[] = $helper->generate();

        $helper = new HelperKpi();
        $helper->id = 'box-orders';
        $helper->icon = 'icon-retweet';
        $helper->color = 'color3';
        $helper->title = $this->l('Orders per Customer', null, null, false);
        $helper->subtitle = $this->l('All Time', null, null, false);
        if (ConfigurationKPI::get('ORDERS_PER_CUSTOMER') !== false) {
            $helper->value = ConfigurationKPI::get('ORDERS_PER_CUSTOMER');
        }
        $helper->source = $this->context->link->getAdminLink('AdminStats').'&ajax=1&action=getKpi&kpi=orders_per_customer';
        $helper->refresh = (bool)(ConfigurationKPI::get('ORDERS_PER_CUSTOMER_EXPIRE') < $time);
        $kpis[] = $helper->generate();

        $helper = new HelperKpi();
        $helper->id = 'box-newsletter';
        $helper->icon = 'icon-envelope';
        $helper->color = 'color4';
        $helper->title = $this->l('Newsletter Registrations', null, null, false);
        $helper->subtitle = $this->l('All Time', null, null, false);
        if (ConfigurationKPI::get('NEWSLETTER_REGISTRATIONS') !== false) {
            $helper->value = ConfigurationKPI::get('NEWSLETTER_REGISTRATIONS');
        }
        $helper->source = $this->context->link->getAdminLink('AdminStats').'&ajax=1&action=getKpi&kpi=newsletter_registrations';
        $helper->refresh = (bool)(ConfigurationKPI::get('NEWSLETTER_REGISTRATIONS_EXPIRE') < $time);
        $kpis[] = $helper->generate();

        $helper = new HelperKpiRow();
        $helper->kpis = $kpis;
        return $helper->generate();
    }

    public function renderView()
    {
        /** @var Customer $customer */
        if (!($customer = $this->loadObject())) {
            return;
        }

        $this->context->customer = $customer;
        $gender = new Gender($customer->id_gender, $this->context->language->id);
        $gender_image = $gender->getImage();

        /* OVERRIDE */

        /* Scelta tab - Sostituisce tab-container di PS 1.4 */
        /* NB: Se nell'URL ci sono più tab_name, prende solo l'ultimo */
        if(Tools::getValue('tab_name'))
            $tab_name = Tools::getValue('tab_name');
        //else
            //$tab_name = 'customerdetails';

        /* Flag utili */
        $stat_escludi_ordinati = false; // true: esclude i prodotti ordinati in $interested (tab "stat")
        $navbar_on = true; // false: non carica la navbar nelle tab view/edit a cui si accede dalla scheda cliente
        $riassunto_on = true; // false: non carica la tab riassuntiva dei dati del cliente
        $attivita_recenti_on = false; // false: non carica la tab riassuntiva delle ultime attività
        
        /* Agente assegnato al cliente (serve per i permessi) */
        $agente_c = Db::getInstance()->getValue('SELECT agente FROM customer_amministrazione WHERE id_customer = '.$customer->id);
        
        /* Variabili utili */
        $mio_id = $this->context->employee->id;
        $mio_nome = Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$mio_id);
        $mio_firstname = Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$mio_id);
        $today = date('d-m-Y');
		$is_supplier = $customer->supplier;

        /* Permessi agenti e impiegati */
        $is_agente = ($this->context->employee->id_profile == 7 ? true : false); // true: l'employee loggato è un agente
        $not_mio_agente = (($this->context->employee->id_profile == 7 && $agente_c != $mio_id) ? true : false); // true: l'employee loggato è un agente e non è l'agente assegnato al cliente
        $check_employee = ($mio_id == 1 || $mio_id == 6 || $mio_id == 22 ? true : false); // true: employee = Ezio, Federico o Carolina (autorizzati a fare azioni speciali)
        $check_programmer = ($mio_id == 6 || $mio_id == 22 ? true : false); // true: employee = Federico o Carolina (autorizzati a fare azioni speciali)
        $check_admin2 = ($this->context->employee->id_profile == 8 ? true : false); // true: l'employee loggato ha profilo administrator2
        $auth_carrelli_tipo = ($customer->id == 44431 && !$check_employee ? false : true); // true: employee autorizzato a vedere i carrelli tipo

        /* Link utili */
		$array_utils = array();
		$array_utils[] = '<option name="" value="-- Seleziona un link utile --" selected="selected">-- Seleziona un link utile --</option>';
		$array_utils[] = '<option name="Preventivi" value="https://www.ezdirect.it/preventivi-quotazioni-miglior-prezzo-cuffie-centralini">Preventivi</option>';
		$array_utils[] = '<option name="Ti richiamiamo noi" value="https://www.ezdirect.it/ti-richiamiamo-noi">Ti richiamiamo noi</option>';
		$array_utils[] = '<option name="Il tuo account" value="https://www.ezdirect.it/autenticazione">Il tuo account</option>';
		$array_utils[] = '<option name="Blog" value="http://www.ezdirect.it/blog/">Blog</option>';
		$array_utils[] = '<option name="Offerte speciali" value="http://www.ezdirect.it/offerte-speciali">Offerte speciali</option>';
		$array_utils[] = '<option name="Assistenza tecnica postvendita" value="http://www.ezdirect.it/contattaci?step=tecnica">Assistenza tecnica postvendita</option>';
		$array_utils[] = '<option name="Assistenza ordini" value="http://www.ezdirect.it/contattaci?step=assistenza-ordini">Assistenza ordini</option>';
		$array_utils[] = '<option name="Assistenza contabilita" value="http://www.ezdirect.it/contattaci?step=contabilita">Assistenza contabilità</option>';
		$array_utils[] = '<option name="Rivenditori" value="https://www.ezdirect.it/autenticazione?cliente=rivenditore">Rivenditori</option>';
		$array_utils[] = '<option name="Condizioni di vendita" value="http://www.ezdirect.it/guide/3-termini-e-condizioni-acquisto-on-line-ezdirect">Condizioni di vendita</option>';
		$array_utils[] = '<option name="Cashback Jabra" value="http://www.ezdirect.it/blog/2017/01/acqistare-al-miglior-prezzo-jabra-rimborso-su-prodotto-comprato/">Cashback Jabra</option>';

		$cmss = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'cms_lang WHERE id_lang = '.$this->context->language->id);
		foreach ($cmss as $cms) {
			$array_utils[] = '<option name="'.$cms['meta_title'].'" value="http://www.ezdirect.it/guide/'.$cms['id_cms'].'-'.$cms['link_rewrite'].'.php">'.$cms['meta_title'].'</option>';
		}
		
		sort($array_utils, SORT_STRING);
		
		$utils_options = '';
		foreach($array_utils as $util)
			$utils_options .= $util;

        /* 
        Nella 1.4 per prendere l'ultimo id inserito nel database veniva usato:
            $link_glob = Db::getInstance()->connect();
            $id = mysqli_insert_id($link_glob);
        -> Sostituito con: 
            Db::getInstance()->Insert_ID() 
        */

        /* FINE OVERRIDE */

        $customer_stats = $customer->getStats();
        $sql = 'SELECT SUM(total_paid_real) FROM '._DB_PREFIX_.'orders WHERE id_customer = %d AND valid = 1';
        if ($total_customer = Db::getInstance()->getValue(sprintf($sql, $customer->id))) {
            $sql = 'SELECT SQL_CALC_FOUND_ROWS COUNT(*) FROM '._DB_PREFIX_.'orders WHERE valid = 1 AND id_customer != '.(int)$customer->id.' GROUP BY id_customer HAVING SUM(total_paid_real) > %d';
            Db::getInstance()->getValue(sprintf($sql, (int)$total_customer));
            $count_better_customers = (int)Db::getInstance()->getValue('SELECT FOUND_ROWS()') + 1;
        } else {
            $count_better_customers = '-';
        }

        $orders = Order::getCustomerOrders($customer->id, true);
        $total_orders = count($orders);
        for ($i = 0; $i < $total_orders; $i++) {
            $orders[$i]['total_paid_real_not_formated'] = $orders[$i]['total_paid_real'];
            $orders[$i]['total_paid_real'] = Tools::displayPrice($orders[$i]['total_paid_real'], new Currency((int)$orders[$i]['id_currency']));
        }

        $messages = CustomerThread::getCustomerMessages((int)$customer->id);

        $total_messages = count($messages);
        for ($i = 0; $i < $total_messages; $i++) {
            $messages[$i]['message'] = substr(strip_tags(html_entity_decode($messages[$i]['message'], ENT_NOQUOTES, 'UTF-8')), 0, 75);
            $messages[$i]['date_add'] = Tools::displayDate($messages[$i]['date_add'], null, true);
            if (isset(self::$meaning_status[$messages[$i]['status']])) {
                $messages[$i]['status'] = self::$meaning_status[$messages[$i]['status']];
            }
        }

        $groups = $customer->getGroups();
        $total_groups = count($groups);
        for ($i = 0; $i < $total_groups; $i++) {
            $group = new Group($groups[$i]);
            $groups[$i] = array();
            $groups[$i]['id_group'] = $group->id;
            $groups[$i]['name'] = $group->name[$this->default_form_language];
        }

        $total_ok = 0;
        $orders_ok = array();
        $orders_ko = array();
        foreach ($orders as $order) {
            if (!isset($order['order_state'])) {
                $order['order_state'] = $this->l('There is no status defined for this order.');
            }

            if ($order['valid']) {
                $orders_ok[] = $order;
                $total_ok += $order['total_paid_real_not_formated']/$order['conversion_rate'];
            } else {
                $orders_ko[] = $order;
            }
        }

        $products = $customer->getBoughtProducts(); // Funzione modificata in override

        // SPOSTATO NELLA TAB CARRELLI
        /*
        $carts = Cart::getCustomerCarts($customer->id);
        $total_carts = count($carts);
        for ($i = 0; $i < $total_carts; $i++) {
            $cart = new Cart((int)$carts[$i]['id_cart']);
            $this->context->cart = $cart;
            $currency = new Currency((int)$carts[$i]['id_currency']);
            $this->context->currency = $currency;
            $summary = $cart->getSummaryDetails();
            $carrier = new Carrier((int)$carts[$i]['id_carrier']);
            $carts[$i]['id_cart'] = sprintf('%06d', $carts[$i]['id_cart']);
            $carts[$i]['date_add'] = Tools::displayDate($carts[$i]['date_add'], null, true);
            $carts[$i]['total_price'] = Tools::displayPrice($summary['total_price'], $currency);
            $carts[$i]['name'] = $carrier->name; // cambiare nome variabile: ps_cart.name è il campo corrispondente all'oggetto del carrello
            $carts[$i]['conv'] = $cart->convertito; // aggiunto convertito alla classe cart
            $carts[$i]['read'] = $cart->visualizzato; // aggiunto visualizzato alla classe cart
            $carts[$i]['quote'] = $cart->preventivo; // aggiunto preventivo alla classe cart
        }
        */

        $this->context->currency = Currency::getDefaultCurrency();

        // Prodotti nel carrello che non sono mai stati ordinati dal cliente
        if($stat_escludi_ordinati){
            $sql = 'SELECT DISTINCT cp.id_product, c.id_cart, c.id_shop, cp.id_shop AS cp_id_shop, cp.date_add
                    FROM '._DB_PREFIX_.'cart_product cp
                    JOIN '._DB_PREFIX_.'cart c ON (c.id_cart = cp.id_cart)
                    JOIN '._DB_PREFIX_.'product p ON (cp.id_product = p.id_product)
                    WHERE c.id_customer = '.(int)$customer->id.'
                        AND NOT EXISTS (
                                SELECT 1
                                FROM '._DB_PREFIX_.'orders o
                                JOIN '._DB_PREFIX_.'order_detail od ON (o.id_order = od.id_order)
                                WHERE product_id = cp.id_product AND o.valid = 1 AND o.id_customer = '.(int)$customer->id.'
                            )';
        }
        else{
            $sql = 'SELECT DISTINCT cp.id_product, c.id_cart, c.id_shop, cp.id_shop AS cp_id_shop, cp.date_add
                    FROM '._DB_PREFIX_.'cart_product cp
                    JOIN '._DB_PREFIX_.'cart c ON (c.id_cart = cp.id_cart)
                    JOIN '._DB_PREFIX_.'product p ON (cp.id_product = p.id_product)
                    WHERE c.id_customer = '.(int)$customer->id;
        }

        $interested = Db::getInstance()->executeS($sql);
        $total_interested = count($interested);
        for ($i = 0; $i < $total_interested; $i++) {
            $product = new Product($interested[$i]['id_product'], false, $this->default_form_language, $interested[$i]['id_shop']);
            if (!Validate::isLoadedObject($product)) {
                continue;
            }
            $interested[$i]['url'] = $this->context->link->getProductLink(
                $product->id,
                $product->link_rewrite,
                Category::getLinkRewrite($product->id_category_default, $this->default_form_language),
                null,
                null,
                $interested[$i]['cp_id_shop']
            );

            if($product->id != 0){
                $interested[$i]['id'] = (int)$product->id;
                $interested[$i]['name'] = Tools::htmlentitiesUTF8($product->name);
            }
            else{
                $resnmex = ("SELECT product_id, product_name FROM "._DB_PREFIX_."order_detail WHERE product_id = ".$prodotto->id);
                $rownmex = Db::getInstance()->getRow($resnmex);
                $interested[$i]['id'] = $rownmex['product_id'];
                $interested[$i]['name'] = Tools::htmlentitiesUTF8($rownmex['product_name']);
            }
            
            // Aggiunto controllo date_add != "0000-00-00"
            $interested[$i]['date_add'] = (substr($interested[$i]['date_add'], 0, 10) != "0000-00-00" ? date("d-m-Y", strtotime($interested[$i]['date_add'])) : 'Unknown');
        }
        // Correggere: Manca controllo se id = 0

        $emails = $customer->getLastEmails();

        $connections = $customer->getLastConnections();
        if (!is_array($connections)) {
            $connections = array();
        }
        $total_connections = count($connections);
        for ($i = 0; $i < $total_connections; $i++) {
            $connections[$i]['http_referer'] = $connections[$i]['http_referer'] ? preg_replace('/^www./', '', parse_url($connections[$i]['http_referer'], PHP_URL_HOST)) : $this->l('Direct link');
        }

        $referrers = Referrer::getReferrers($customer->id);
        $total_referrers = count($referrers);
        for ($i = 0; $i < $total_referrers; $i++) {
            $referrers[$i]['date_add'] = Tools::displayDate($referrers[$i]['date_add'], null, true);
        }

        $customerLanguage = new Language($customer->id_lang);
        $shop = new Shop($customer->id_shop);

        
        $blacklist = Db::getInstance()->getValue('
            SELECT blacklist 
            FROM customer_amministrazione 
            WHERE id_customer = '.$customer->id
        );

        /*
        // Mostra un avviso se il cliente è in blacklist
        if($blacklist == 1)
        {
            echo '
            <script type="text/javascript">
                $(document).ready(function(){
                    swal({
                        type: "error",
                        title: "ATTENZIONE",
                        text: "Questo cliente è in blacklist!",
                        footer: ""
                    })
                });
            </script>
            ';
        }
        */

        $id_gruppo = Customer::getDefaultGroupId($customer->id);
        $gruppo = new Group($id_gruppo);
        $profilo = $gruppo->name[$this->context->language->id];

        /* INIZIO Riassunto dati cliente */
        // CORREGGERE! Aggiungere $numero_ordini (anche nel tpl)
        if($riassunto_on && !$not_mio_agente){

            $company_or_name = ($customer->is_company ? $customer->company : $customer->firstname.' '.$customer->lastname);
            $codici = $customer->id.' / '.$customer->codice_esolver;

            $acquisti_ultimo_anno = Db::getInstance()->getValue('
                SELECT count(id_order) 
                FROM '._DB_PREFIX_.'orders 
                WHERE id_customer = '.$customer->id.' 
                    AND date_add BETWEEN date_sub(curdate(),interval 365 day) 
                    AND date_sub(curdate(),interval -1 day)
            ');
                
            $preventivi_ultimo_anno = Db::getInstance()->getValue('
                SELECT count(id_cart)  
                FROM '._DB_PREFIX_.'cart 
                WHERE id_customer = '.$customer->id.' 
                    AND date_add BETWEEN date_sub(curdate(),interval 365 day) 
                    AND date_sub(curdate(),interval -1 day)
            ');
            
            $ric_prev_ultimo_anno = Db::getInstance()->getValue('
                SELECT count(id_thread)    
                FROM form_prevendita_thread 
                WHERE id_customer = '.$customer->id.' 
                    AND date_add BETWEEN date_sub(curdate(),interval 365 day) 
                    AND date_sub(curdate(),interval -1 day)
            ');
            
            $todo_ultimo_anno = Db::getInstance()->getValue('
                SELECT count(id_action)    
                FROM action_thread 
                WHERE id_customer = '.$customer->id.' 
                    AND date_add BETWEEN date_sub(curdate(),interval 365 day) 
                    AND date_sub(curdate(),interval -1 day)
            ');
            
            $ticket_ultimo_anno = Db::getInstance()->getValue('
                SELECT count(id_customer_thread)   
                FROM '._DB_PREFIX_.'customer_thread 
                WHERE id_customer = '.$customer->id.' 
                    AND date_add BETWEEN date_sub(curdate(),interval 365 day) 
                    AND date_sub(curdate(),interval -1 day)
            ');

            if($acquisti_ultimo_anno >= 3){
                $tipo_profilo = 'Fedele';
                $tipo_profilo_title = 'Almeno tre acquisti nell\'anno';
            }
            else if ($acquisti_ultimo_anno > 0 && $acquisti_ultimo_anno < 3){
                $tipo_profilo = 'Attivo';
                $tipo_profilo_title = 'Almeno un acquisto nell\'anno';
            }  
            else
            {
                if($preventivi_ultimo_anno > 0){
                    $tipo_profilo = 'Prospect';
                    $tipo_profilo_title = 'Non ha ancora acquistato quest\'anno, ma ha un preventivo o un carrello';
                }
                else{
                    if($ric_prev_ultimo_anno > 0 || $todo_ultimo_anno > 0 || $ticket_ultimo_anno > 0){
                        $tipo_profilo = 'Lead';
                        $tipo_profilo_title = 'Non ha acquistato né chiesto preventivi quest\'anno, ma ci ha contattato o ci sono attivit&agrave; in corso';
                    }
                    else{
                        $tipo_profilo = 'Inattivo';
                        $tipo_profilo_title = 'Non acquista da almeno un anno';
                    }  
                }
            }
            
            //$profilo = $groups[0]['name'];
            $tipo_cliente = ($customer->is_company ? 'Azienda' : 'Privato');
            switch ($customer->active) {
                case 0:
                    $status = 'Disattivato';
                    break;
                case 1:
                    $status = 'Attivo';
                    break;
                case 2:
                    $status = 'Cessata attività';
                    break;
                
                default:
                    $status = 'Valore non valido';
                    break;
            }

            $referenti = Db::getInstance()->executeS('
                SELECT id_persona as id, CONCAT(firstname, " ", lastname) as name 
                FROM persone 
                WHERE id_customer = '.$customer->id.'
            ');

            $referente_c = Db::getInstance()->getValue('
                SELECT referente 
                FROM '._DB_PREFIX_.'customer 
                WHERE id_customer = '.$customer->id
            );
			
			if(!$referente_c || $referente_c == 0) 
			{
				$referente_c = Db::getInstance()->getValue('
                    SELECT id_persona as id
                    FROM persone 
                    WHERE id_customer = '.$customer->id.' 
                        AND firstname = "'.$customer->firstname.'" 
                        AND lastname = "'.$customer->lastname.'"
                ');
                // UPDATE DISABILITATO PER ORA
				/*Db::getInstance()->execute('
                    UPDATE '._DB_PREFIX_.'customer 
                    SET referente = '.$referente_c.' 
                    WHERE id_customer = '.$customer->id.'
                ');*/
            }

            /* correggere: migliorabile con le modifiche db fatte? */
            // Unknown se non ci sono indirizzi di fatturazione attivi; gestire il caso o non può mai succedere?
            $provincia = Db::getInstance()->getValue('SELECT id_state FROM '._DB_PREFIX_.'address WHERE fatturazione = 1 AND deleted = 0 AND active = 1 AND id_customer = '.$customer->id); 
            if(!$provincia)
                $prov_text = 'Unknown';
            else
                $prov_text = Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'state WHERE id_state = '.$provincia);
            
            $nazione = Db::getInstance()->getValue('SELECT id_country FROM '._DB_PREFIX_.'address WHERE fatturazione = 1 AND deleted = 0 AND active = 1 AND id_customer = '.$customer->id);
            if(!$nazione)
                $naz_text = 'Unknown';
            else  
                $naz_text = Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'country WHERE id_country = '.$nazione);
            
            $zona = ($prov_text == 'Unknown' ? $naz_text : $naz_text.'-'.$prov_text);
            
            $ordini_totali = Db::getInstance()->getValue('
                SELECT SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) tot 
                FROM '._DB_PREFIX_.'order_detail od
                JOIN '._DB_PREFIX_.'orders o ON o.id_order = od.id_order 
                LEFT JOIN '._DB_PREFIX_.'order_history oh ON oh.id_order_state =6 AND oh.id_order = o.id_order 
                WHERE oh.id_order_state IS NULL  
                    AND (o.valid = 1 OR 
                        (o.valid = 0 AND (o.module = "bankwire" OR o.module = "other_payment"))
                    )
                    AND o.id_customer = '.$customer->id.'
            ');
            if(!$ordini_totali)
                $ordini_totali = 0;
			
			$ordini_ultimi_12_mesi = Db::getInstance()->getValue('
                SELECT SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) tot 
                FROM '._DB_PREFIX_.'order_detail od
                JOIN '._DB_PREFIX_.'orders o ON o.id_order = od.id_order 
                LEFT JOIN '._DB_PREFIX_.'order_history oh ON oh.id_order_state =6 AND oh.id_order = o.id_order 
                WHERE oh.id_order_state IS NULL  
                    AND (o.valid = 1 OR 
                        (o.valid = 0 AND (o.module = "bankwire" OR o.module = "other_payment"))
                    ) 
                    AND o.id_customer = '.$customer->id.' 
                    AND o.date_add BETWEEN date_sub(curdate(),interval 365 day) AND date_sub(curdate(),interval 0 day)
            ');
            if(!$ordini_ultimi_12_mesi)
                $ordini_ultimi_12_mesi = 0;
			
			$ordini_ultimi_6_mesi = Db::getInstance()->getValue('
                SELECT SUM((od.`product_price` - ((od.product_price * od.reduction_percent) / 100)) * od.`product_quantity` / o.conversion_rate) tot 
                FROM '._DB_PREFIX_.'order_detail od
                JOIN '._DB_PREFIX_.'orders o ON o.id_order = od.id_order 
                LEFT JOIN '._DB_PREFIX_.'order_history oh ON oh.id_order_state =6 AND oh.id_order = o.id_order 
                WHERE oh.id_order_state IS NULL  
                    AND (o.valid = 1 OR 
                        (o.valid = 0 AND (o.module = "bankwire" OR o.module = "other_payment"))
                    ) 
                    AND o.id_customer = '.$customer->id.' 
                    AND o.date_add BETWEEN date_sub(curdate(),interval 180 day) AND date_sub(curdate(),interval 0 day)
            ');
            if(!$ordini_ultimi_6_mesi)
                $ordini_ultimi_6_mesi = 0;

            $data_ultimo_acquisto = Db::getInstance()->getValue('
                SELECT data_fattura 
                FROM '._DB_PREFIX_.'fattura 
                WHERE id_customer = '.$customer->id.' 
                ORDER BY data_fattura DESC
            ');

            $impiegati = Db::getInstance()->executeS('SELECT id_employee as id, CONCAT(firstname," ",LEFT(lastname, 1),".") AS name FROM '._DB_PREFIX_.'employee WHERE active = 1 ORDER BY firstname ASC, lastname ASC');
            //$tecnici = Db::getInstance()->executeS('SELECT id_employee as id, CONCAT(firstname," ",LEFT(lastname, 1),".") AS name FROM '._DB_PREFIX_.'employee WHERE id_employee = 4 OR id_employee = 5 OR id_employee = 12 OR id_employee = 17 ORDER BY firstname ASC, lastname ASC');
            // correggere: togliere id_employee = 6 finiti gli employee; la riga sopra è quella giusta
            $tecnici = Db::getInstance()->executeS('SELECT id_employee as id, CONCAT(firstname," ",LEFT(lastname, 1),".") AS name FROM '._DB_PREFIX_.'employee WHERE id_employee = 4 OR id_employee = 5 OR id_employee = 6 ORDER BY firstname ASC, lastname ASC');

            $query_amministrazione = '
                SELECT *
                FROM customer_amministrazione
                WHERE id_customer = '.$customer->id.'
            ';
            $amministrazione_c = Db::getInstance()->getRow($query_amministrazione);

            $query_agente = '
                SELECT id_employee as id, CONCAT(firstname, " ", LEFT(lastname,1), ".") as name
                FROM '._DB_PREFIX_.'employee
                WHERE id_employee = '.$amministrazione_c['agente'].'
            ';
            $agente_c = Db::getInstance()->getRow($query_agente);

            /* // Manca nome agente
            if(!$agente_c || $agente_c == 0) {
                // Se il cliente non ha un agente, considero l'ultimo impiegato che ha fatto azioni su di lui
                // correggere query: perchè doppio select???
                $agente_c = Db::getInstance()->getValue('SELECT id_employee FROM (SELECT id_thread, id_employee FROM form_prevendita_thread WHERE id_customer = '.$customer->id.') au ORDER BY id_thread DESC');
                if(!$agente_c || $agente_c == 0) {	
                    if($customer->id_default_group == 3 || $customer->id_default_group == 5 || $customer->id_default_group == 15 || $customer->id_default_group == 8 || $customer->id_default_group == 19)
                        $agente_c = 1; // Ezio
                    else
                        $agente_c = Db::getInstance()->getValue('SELECT id_employee FROM (SELECT * FROM cart WHERE id_customer = '.$customer->id.') au ORDER BY id_cart DESC');
                }	
            }
            */

            $query_tecnico = '
                SELECT id_employee as id, CONCAT(firstname, " ", LEFT(lastname,1), ".") as name
                FROM '._DB_PREFIX_.'employee
                WHERE id_employee = '.$amministrazione_c['tecnico'].'
            ';
            $tecnico_c = Db::getInstance()->getRow($query_tecnico);
            // Assegnare un tecnico default (quello con + azioni sul cliente) -> basta visualizzarlo, o devo fare update nel db di tutti i clienti?
            // Forse meglio fare update del singolo cliente nel momento in cui entro nell'anagrafica e lo trovo vuoto
            // $tecnico_default = ...; 
            
            /* // Manca nome tecnico            
            if(!$tecnico_c || $tecnico_c == 0) {
                // Se il cliente non ha un tecnico, considero l'ultimo impiegato che ha fatto azioni su di lui
                $tecnico_c = Db::getInstance()->getValue('SELECT id_employee FROM (SELECT * FROM '._DB_PREFIX_.'customer_thread WHERE id_customer = '.$customer->id.') au ORDER BY id_customer_thread DESC');
                if(!$tecnico_c || $tecnico_c == 0) 
                    $tecnico_c = Db::getInstance()->getValue('SELECT action_to FROM (SELECT * FROM action_thread WHERE id_customer = '.$customer->id.') au ORDER BY id_action DESC');
            }
            */


            // RIGA 3

            $r3_visite = Db::getInstance()->getValue('
                SELECT COUNT(conn.id_connections) as Visite
                FROM '._DB_PREFIX_.'connections conn 
                LEFT JOIN '._DB_PREFIX_.'guest g ON conn.id_guest = g.id_guest 
                LEFT JOIN '._DB_PREFIX_.'customer c ON g.id_customer = c.id_customer 
                WHERE g.id_customer = '.$customer->id.'
            ');
            
            $r3_pagine = Db::getInstance()->getValue('
                SELECT
                    COUNT(a.id_connections) as Pagine
                FROM (
                    SELECT DISTINCT conn.id_connections, cp.id_page 
                    FROM '._DB_PREFIX_.'connections conn 
                    LEFT JOIN '._DB_PREFIX_.'guest g ON conn.id_guest = g.id_guest 
                    LEFT JOIN '._DB_PREFIX_.'customer c ON g.id_customer = c.id_customer 
                    LEFT JOIN '._DB_PREFIX_.'connections_page cp ON cp.id_connections = conn.id_connections 
                    WHERE g.id_customer = '.$customer->id.'
                ) a
            ');
            
            $r3_prodotti = Db::getInstance()->executeS('
                SELECT pl.name AS Nome, CONCAT(pr.reference," (", IF(LEFT(c.date_add,10) = CURRENT_DATE(), "Oggi", IF(YEAR(c.date_add) = YEAR(CURRENT_DATE()), DATE_FORMAT(LEFT(c.date_add,10), "%d/%m"), DATE_FORMAT(LEFT(c.date_add,10), "%d/%m/%Y"))),")") AS Codice
                FROM '._DB_PREFIX_.'connections c 
                LEFT JOIN '._DB_PREFIX_.'guest g ON c.id_guest = g.id_guest 
                LEFT JOIN '._DB_PREFIX_.'customer cu ON g.id_customer = cu.id_customer 
                LEFT JOIN '._DB_PREFIX_.'connections_page cp ON cp.id_connections = c.id_connections 
                LEFT JOIN '._DB_PREFIX_.'page p ON p.id_page = cp.id_page 
                LEFT JOIN '._DB_PREFIX_.'page_type pt ON pt.id_page_type = p.id_page_type
                LEFT JOIN '._DB_PREFIX_.'product pr ON pr.id_product = p.id_object
                LEFT JOIN '._DB_PREFIX_.'product_lang pl ON pr.id_product = pl.id_product
                WHERE g.id_customer = '.$customer->id.'
                    AND pt.name = "product.php"
                ORDER BY c.date_add DESC
                LIMIT 1
            ');
            
            // TUTTO IL RESTO FUORI DAL FOREACH PERCHE' LA QUERY RESTITUISCE UNA SOLA ROW
            foreach($r3_prodotti as $r_prodotto){
                $r3_prodotti_c = $r_prodotto['Codice'];
                $r3_prodotti_n = $r_prodotto['Nome'];
            }

            $r3_ch_ricevuta = Db::getInstance()->executeS('
                SELECT LEFT(datetime,16) as data, extfield1, extfield2 
                FROM telefonate 
                WHERE id_customer = '.$customer->id.' 
                    AND calltype = "Inbound" ORDER BY datetime DESC LIMIT 1
            ');
            
            if(count($r3_ch_ricevuta)>0){
                foreach($r3_ch_ricevuta as $ricevuta){
                    $ric_date = $ricevuta['data'];
                    $ric_da = $ricevuta['extfield2'];
                }
            }
            
            $ric_date1 = new DateTime($ric_date);
            
            if($ric_date1->format('Y-m-d') == date("Y-m-d"))
                $ric_date = 'Oggi '.$ric_date1->format('H:i');
            else if($ric_date1->format('Y') == date("Y"))
                $ric_date = $ric_date1->format('d/m');
            else
                $ric_date = $ric_date1->format('d/m/Y');

            $r3_ch_inviata = Db::getInstance()->executeS('
                SELECT LEFT(datetime,16) as data, extfield1, extfield2 
                FROM telefonate
                WHERE id_customer = '.$customer->id.' 
                    AND calltype = "Outbound" ORDER BY datetime DESC LIMIT 1
            ');
            
            if(count($r3_ch_inviata)>0){
                foreach($r3_ch_inviata as $inviata){
                    $inv_date = $inviata['data'];
                    $inv_da = $inviata['extfield1'];
                }
            }
            
            $inv_date1 = new DateTime($inv_date);
            
            if($inv_date1->format('Y-m-d') == date("Y-m-d"))
                $inv_date = 'Oggi '.$inv_date1->format('H:i');
            else if($inv_date1->format('Y') == date("Y"))
                $inv_date = $inv_date1->format('d/m');
            else
                $inv_date = $inv_date1->format('d/m/Y');
            // NEL TPL STAMPA ORA DEL REFRESH PAGINA??????
            $r3_azione = Db::getInstance()->executeS('
                SELECT a.tipo, a.tecnico, a.data 
                FROM (
                    (SELECT "TO DO" as tipo, act.id_action as id, e.firstname as tecnico, act.date_upd as data FROM action_thread act JOIN '._DB_PREFIX_.'employee e on act.action_to = e.id_employee WHERE act.id_customer = '.$customer->id.' and (e.id_employee = 4 OR e.id_employee = 5 OR e.id_employee = 12 OR e.id_employee = 17) ORDER BY act.date_upd DESC)
                    UNION
                    (SELECT "Preventivo" as tipo, ft.id_thread, e.firstname, ft.date_upd FROM form_prevendita_thread ft JOIN '._DB_PREFIX_.'employee e on ft.id_employee = e.id_employee WHERE ft.id_customer = '.$customer->id.' and (e.id_employee = 4 OR e.id_employee = 5 OR e.id_employee = 12 OR e.id_employee = 17) order by ft.date_upd desc)
                    UNION
                    (SELECT "Ticket" as tipo, cm.id_customer_thread, e.firstname, cm.date_add FROM '._DB_PREFIX_.'customer_thread ct INNER JOIN '._DB_PREFIX_.'customer_message cm on ct.id_customer_thread = cm.id_customer_thread INNER JOIN '._DB_PREFIX_.'employee e on cm.id_employee = e.id_employee where cm.id_employee!=0 and ct.id_customer = '.$customer->id.' and (e.id_employee = 4 OR e.id_employee = 5 OR e.id_employee = 12 OR e.id_employee = 17) group by cm.id_customer_thread order by cm.date_add desc)
                    UNION
                    (SELECT "Carrello creato" as tipo, c.id_cart, e.firstname, c.date_add FROM '._DB_PREFIX_.'cart c JOIN '._DB_PREFIX_.'employee e on c.created_by = e.id_employee WHERE c.id_customer = '.$customer->id.' and (e.id_employee = 4 OR e.id_employee = 5 OR e.id_employee = 12 OR e.id_employee = 17) ORDER BY c.date_add desc)
                    UNION
                    (SELECT "Carrello aggiornato" as tipo, c.id_cart, e.firstname, c.date_upd FROM '._DB_PREFIX_.'cart c JOIN '._DB_PREFIX_.'employee e on c.id_employee = e.id_employee WHERE c.created_by = 0 and c.id_customer = '.$customer->id.' and (e.id_employee = 4 OR e.id_employee = 5 OR e.id_employee = 12 OR e.id_employee = 17) ORDER BY c.date_upd desc)
                    ) a
                ORDER BY data desc
                LIMIT 1
            ');
            
            // TUTTO IL RESTO FUORI DAL FOREACH PERCHE' LA QUERY RESTITUISCE UNA SOLA ROW
            foreach($r3_azione as $ultima_azione){
                $r3_azione_tipo = $ultima_azione['tipo'];
                $r3_tecnico = $ultima_azione['tecnico'];
                $r3_data_azione = $ultima_azione['data'];
            }
            
            $azione_date1 = new DateTime($r3_data_azione);
            
            if($azione_date1->format('Y-m-d') == date("Y-m-d"))
                $r3_data_azione = 'Oggi '.$azione_date1->format('H:i');
            else if($azione_date1->format('Y') == date("Y"))
                $r3_data_azione = $azione_date1->format('d/m');
            else
                $r3_data_azione = $azione_date1->format('d/m/Y');
            
            $ultimo_prod = (count($r3_prodotti)>0 ?$r3_prodotti_c : "Nessuno");
            $ultimo_prod_title = $r3_prodotti_n;
            $ultima_ch_ric = (count($r3_ch_ricevuta)>0 ? $ric_da." (".$ric_date.")" : "-");
            $ultima_ch_inv = (count($r3_ch_inviata)>0 ? $inv_da." (".$inv_date.")" : "-");
            $ultima_azione = (count($r3_azione)>0 ? $r3_tecnico." (".$r3_data_azione.")" : "-");

            // Array per view.tpl
            $riassunto = array(
                // Riga 1
                'cliente' => $company_or_name,
                'codici' => $codici,
                'profilo' => $profilo,
                'tipo_profilo' => $tipo_profilo,
                'tipo_profilo_title' => $tipo_profilo_title,
                'tipo_cliente' => $tipo_cliente,
                'status' => $status,
                'registrazione' => Tools::displayDate($customer->date_add, null, true),
                // Riga 2
                'referenti' => $referenti,
                'referente' => $referente_c,
                'zona' => $zona,
                'ord_6' => Tools::displayPrice($ordini_ultimi_6_mesi, $this->context->currency->id),
                'ord_12' => Tools::displayPrice($ordini_ultimi_12_mesi, $this->context->currency->id),
                'ord_tot' => Tools::displayPrice($ordini_totali, $this->context->currency->id),
                'ultimo_acq' => $data_ultimo_acquisto,
                'impiegati' => $impiegati,
                'agente' => $agente_c,
                'tecnici' => $tecnici,
                'tecnico' => $tecnico_c,
                // Riga 3
                'visite' => $r3_visite,
                'pagine_viste' => $r3_pagine,
                'ultimo_prod' => $ultimo_prod,
                'ultimo_prod_title' => $ultimo_prod_title,
                'ultima_ch_ric' => $ultima_ch_ric,
                'ultima_ch_inv' => $ultima_ch_inv,
                'ultima_azione' => $ultima_azione,
            );

        }
        /* FINE Riassunto dati cliente */

        /* Riga pulsanti */

        $azioni_padre_li = array(
            array(
                'link' => '/ezadmin/index.php?controller=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&tab_name=tickets&azione=tickets&viewticket&aprinuovoticket&token='.$this->token,
                'tipo' => 'ticket_li',
                'icona' => 'AdminTools',
                'title' => 'Ticket',
                'nome' => 'Ticket'
            ),
            array(
                'link' => '/ezadmin/index.php?controller=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&tab_name=actions&azione=actions&viewmessage&aprinuovomessaggio&token='.$this->token,
                'tipo' => '',
                'icona' => 'email',
                'title' => 'Messaggio',
                'nome' => 'Messaggio'
            ),
            array(
                'link' => '/ezadmin/index.php?controller=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&tab_name=todo&azione=todo&viewaction&aprinuovaazione&token='.$this->token,
                'tipo' => 'todo_li',
                'icona' => 'return',
                'title' => 'To-Do',
                'nome' => 'To-Do'
            ),
            array(
                'link' => '', // correggere: decidere se farlo in AdminCustomers o no
                'tipo' => 'preventivo_li',
                'icona' => 'charged_ok',
                'title' => 'Preventivo',
                'nome' => 'Preventivo'
            ),
            array(
                'link' => '', // correggere: decidere se farlo in AdminCustomers o no
                'tipo' => '',
                'icona' => 'cart',
                'title' => 'Ordine manuale',
                'nome' => 'Ordine manuale'
            ),
            array(
                'link' => '/ezadmin/index.php?controller=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&tab_name=bdl&azione=bdl_add&token='.$this->token,
                'tipo' => '',
                'icona' => 'prefs',
                'title' => 'Buono di lavoro (BDL)',
                'nome' => 'Buono di lavoro (BDL)'
            ),
        );

        $ticket_li = array(
            array(
                'link' => 'tab_name=tickets&azione=tickets&viewticket&aprinuovoticket&tipo-ticket=2',
                'tipo' => '2',
                'icona' => 'contabilita',
                'title' => 'Contabilita',
                'nome' => 'Contabilit&agrave;'
            ),
            array(
                'link' => 'tab_name=tickets&azione=tickets&viewticket&aprinuovoticket&tipo-ticket=8',
                'tipo' => '8',
                'icona' => 'ordini',
                'title' => 'Ordini',
                'nome' => 'Ordini'
            ),
            array(
                'link' => 'tab_name=tickets&azione=tickets&viewticket&aprinuovoticket&tipo-ticket=4',
                'tipo' => '4',
                'icona' => 'assistenza',
                'title' => 'Assistenza tecnica',
                'nome' => 'Assistenza tecnica'
            ),
            array(
                'link' => 'tab_name=tickets&azione=tickets&viewticket&aprinuovoticket&tipo-ticket=9',
                'tipo' => '9',
                'icona' => 'rma',
                'title' => 'RMA',
                'nome' => 'RMA'
            ),
            array(
                'link' => 'tab_name=tickets&azione=tickets&viewticket&aprinuovoticket&tipo-ticket=3',
                'tipo' => '3',
                'icona' => 'rivenditori',
                'title' => 'Rivenditori',
                'nome' => 'Rivenditori'
            ),
        );

        $todo_li = array(
            array(
                'link' => 'tab_name=todo&azione=todo&viewaction&aprinuovaazione&tipo-todo=Attivita',
                'tipo' => 'Attivita',
                'icona' => 'attivita',
                'title' => 'Attivita',
                'nome' => 'Attivit&agrave;'
            ),
            array(
                'link' => 'tab_name=todo&azione=todo&viewaction&aprinuovaazione&tipo-todo=Telefonata',
                'tipo' => 'Telefonata',
                'icona' => 'telefonata',
                'title' => 'Telefonata',
                'nome' => 'Telefonata'
            ),
            array(
                'link' => 'tab_name=todo&azione=todo&viewaction&aprinuovaazione&tipo-todo=Visita',
                'tipo' => 'Visita',
                'icona' => 'visita',
                'title' => 'Visita',
                'nome' => 'Visita'
            ),
            array(
                'link' => 'tab_name=todo&azione=todo&viewaction&aprinuovaazione&tipo-todo=Caso',
                'tipo' => 'Caso',
                'icona' => 'caso',
                'title' => 'Caso',
                'nome' => 'Caso'
            ),
            array(
                'link' => 'tab_name=todo&azione=todo&viewaction&aprinuovaazione&tipo-todo=Intervento',
                'tipo' => 'Intervento',
                'icona' => 'intervento',
                'title' => 'Intervento',
                'nome' => 'Intervento'
            ),
            array(
                'link' => 'tab_name=todo&azione=todo&viewaction&aprinuovaazione&tipo-todo=Richiesta_RMA',
                'tipo' => 'Richiesta_RMA',
                'icona' => 'richiesta_rma',
                'title' => 'Richiesta RMA',
                'nome' => 'Richiesta RMA'
            ),
        );

        $preventivo_li = array(
            array(
                'link' => '', // non ha né tipo né link ma un onclick che apre un popup
                'tipo' => '', // non ha né tipo né link ma un onclick che apre un popup
                'icona' => 'attivita',
                'title' => 'Telegestione annuale',
                'nome' => 'Telegestione annuale'
            ),
        );

        /* // correggere:
        <li class="tasti-apertura-in"><a href="index.php?tab=AdminCustomers&viewcart&editcart&createnew&provvisorio=1&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4&preventivo=1#modifica-carrello" onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }" id="link-preventivo"><img src="../img/admin/charged_ok.gif" alt="Nuovo preventivo" title="Nuovo preventivo" /> &nbsp;&nbsp;&nbsp;Preventivo</a>';
            
            $orders_telegest = Db::getInstance()->getValue('SELECT count(id_order) FROM orders WHERE id_customer = '.$customer->id);
            if($orders_telegest > 0)
            {
                $orders_telegest_a = Db::getInstance()->executeS('SELECT DISTINCT id_order FROM orders WHERE id_customer = '.$customer->id);
                $otz = 0;
                foreach($orders_telegest_a as $ot)
                {
                    $orders_t_string .= ($otz % 9 == 0 ? '</tr><tr>' : '').'<td><input type="checkbox" class="orders_telegest_c" name="orders_telegest[]" value="'.$ot['id_order'].'" /> '.$ot['id_order'].'&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                    $otz++;
                }	
                echo '
                    <div class="hider" id="hider_telegest_form" style="display:none"></div>
                    <div class="popup_box" id="popup_telegest_form" style="display:none; width:50%; text-align:center; z-index:999999999"><form method="post" action="index.php?tab=AdminCustomers&viewcart&editcart&provvisorio=1&id_customer='.$customer->id.'&amp;copy_to='.$customer->id.'&copia_carrello_conferma=y&id_cart=84366&viewcustomer&token='.$this->token.'&tab-container-1=4&preventivo=1&rif_prev='.$rif.'#modifica-carrello"> Seleziona gli ordini su cui applicare la telegestione: <table><tr>'.$orders_t_string.'</tr></table><br />
                    <input type="checkbox" onchange="$(\'.orders_telegest_c\').not(this).prop(\'checked\', this.checked);" />Seleziona tutti<br /><br />
                    <input type="submit" class="button" value="Conferma" />
                    
                    <button type="button" class="button"  onclick="$(\'#hider_telegest_form\').hide(); $(\'#popup_telegest_form\').hide();">Chiudi</button>
                    
                    </form>
                    </div>
                    <div class="menu-apertura-in" style="position:absolute; left:100px">
                        <ul class="dropdown">
                            <li><a style="color:#000" onclick="$(\'#hider_telegest_form\').show(); $(\'#popup_telegest_form\').show(); "><img src="../img/admin/icons/attivita.gif" alt="Telegestione annuale" title="Telegestione annuale" />&nbsp;&nbsp;&nbsp;Telegestione annuale</a></li>
                        </ul>
                    </div>
                ';
            }
            echo '</li>
        
        <li><a href="index.php?tab=AdminCustomers&viewcart&editcart&createnew&provvisorio=1&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&tab-container-1=4&preventivo=2#modifica-carrello" onclick="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { return true; } else { return false; }" id="link-ordine-manuale"><img src="../img/admin/cart.gif" alt="Nuovo ordine manuale" title="Nuovo ordine manuale" /> &nbsp;&nbsp;&nbsp;Ordine manuale</a></li>
        */

		$mail_persone = Db::getInstance()->executeS("SELECT email FROM persone WHERE id_customer = ".$customer->id." AND email != ''");

        if(Tools::getIsset('viewpreventivo'))
            $preventivo = Db::getInstance()->getRow("SELECT * FROM form_prevendita_thread ft WHERE ft.id_thread = ".Tools::getValue('id_thread')."");
        else
            $preventivo = '';

        $form_prev = Customer::displayFormPreventivo($customer, $preventivo);

        $riga_pulsanti = array(
            'azioni_padre_li' => $azioni_padre_li,
            'todo_li' => $todo_li,
            'ticket_li' => $ticket_li,
            'preventivo_li' => $preventivo_li,
            'mail_persone' => $mail_persone,
            'form_prev' => $form_prev,
        );

        /* // Attivare se il caricamento del cliente Gentech è troppo lento
        if($customer->id == 66355) // Cliente: Gentech (anagrafica pesante)
            $attivita_recenti_on = false;
        */

        /* INIZIO Attività recenti */
        if($attivita_recenti_on && !$not_mio_agente){

            if(!Tools::getValue('tab_name') || $tab_name == 'customerdetails'){
				$sql_ultime_cose = ("
                    SELECT * FROM (SELECT id_thread as id, tipo_richiesta as tipo, tipo_richiesta as tipo_attivita, id_customer as cliente, id_employee as in_carico, status, (CASE WHEN((status = 'open' OR status = 'pending1') AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM form_prevendita_thread WHERE id_customer = ".$customer->id." 
                    UNION 
                    SELECT * FROM (SELECT id_customer_thread as id, ct.id_contact as tipo, cl.name as tipo_attivita, id_customer as cliente, id_employee as in_carico, status, (CASE WHEN((status = 'open' OR status = 'pending1') AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM "._DB_PREFIX_."customer_thread ct JOIN (SELECT * FROM "._DB_PREFIX_."contact_lang WHERE id_lang = ".$this->context->language->id.") cl WHERE id_customer = ".$customer->id." GROUP BY ct.id_customer_thread) ccc 
                    UNION 
                    SELECT id_order as id, 'Ordine' as tipo, 'Ordine' as tipo_attivita, id_customer as cliente, '' as in_carico, (SELECT id_order_state FROM "._DB_PREFIX_."order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM "._DB_PREFIX_."`order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) AS status, (CASE WHEN(((SELECT id_order_state FROM "._DB_PREFIX_."order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM "._DB_PREFIX_."`order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 4 AND (SELECT id_order_state FROM order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM `order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 5 AND (SELECT id_order_state FROM "._DB_PREFIX_."order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM "._DB_PREFIX_."`order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 14 AND (SELECT id_order_state FROM "._DB_PREFIX_."order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM "._DB_PREFIX_."`order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 15 AND (SELECT id_order_state FROM "._DB_PREFIX_."order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM "._DB_PREFIX_."`order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 16 AND (SELECT id_order_state FROM "._DB_PREFIX_."order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM "._DB_PREFIX_."`order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 20) AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM "._DB_PREFIX_."orders WHERE id_customer = ".$customer->id." 
                    UNION 
                    SELECT id_action as id, action_type as tipo, action_type as tipo_attivita, id_customer as cliente, action_to as in_carico, status, (CASE WHEN((status = 'open' OR status = 'pending1') AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) THEN 'SCADUTO' ELSE '' END) as scaduto, date_add, date_upd FROM action_thread WHERE id_customer = ".$customer->id." 
                    UNION 
                    SELECT id_cart as id, 'Carrello' as tipo, 'Carrello' as tipo_attivita, id_customer as cliente, in_carico_a as in_carico, visualizzato AS status, '' as scaduto, date_add, date_upd FROM "._DB_PREFIX_."cart WHERE id_cart NOT IN (SELECT id_cart FROM "._DB_PREFIX_."orders) AND name LIKE '%amazon%' AND id_customer = ".$customer->id." 
                    UNION
                    SELECT id_cart as id, 'Carrello' as tipo, 'Carrello' as tipo_attivita, id_customer as cliente, in_carico_a as in_carico, visualizzato AS status, '' as scaduto, date_add, date_upd FROM carrelli_creati WHERE id_cart NOT IN (SELECT id_cart FROM "._DB_PREFIX_."orders) AND id_customer = ".$customer->id." ORDER BY date_add DESC) x ORDER BY x.date_upd DESC ".(Tools::getIsset('ultime') && Tools::getValue('ultime') == 'vedi_tutte' ? '' : 'LIMIT 10')."
                ");
			}
			else{
				$sql_ultime_cose = ("
                    SELECT * FROM (
                        SELECT id_order as id, 'Ordine' as tipo, 'Ordine' as tipo_attivita, id_customer as cliente, '' as in_carico,
                        (SELECT id_order_state 
                        FROM "._DB_PREFIX_."order_history 
                        WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM "._DB_PREFIX_."`order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)
                        ) AS status, 
                        (CASE WHEN(((SELECT id_order_state FROM "._DB_PREFIX_."order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM "._DB_PREFIX_."`order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 4 AND (SELECT id_order_state FROM "._DB_PREFIX_."order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM "._DB_PREFIX_."`order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 5 AND (SELECT id_order_state FROM "._DB_PREFIX_."order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM "._DB_PREFIX_."`order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 14 AND (SELECT id_order_state FROM "._DB_PREFIX_."order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM "._DB_PREFIX_."`order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 15 AND (SELECT id_order_state FROM "._DB_PREFIX_."order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM "._DB_PREFIX_."`order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 16 AND (SELECT id_order_state FROM "._DB_PREFIX_."order_history WHERE id_order_history = (SELECT MAX(`id_order_history`) FROM "._DB_PREFIX_."`order_history` moh WHERE moh.`id_order` = orders.`id_order` GROUP BY moh.`id_order`)) != 20) AND datediff('".date('Y-m-d H:i:s')."', date_upd) > 3) 
                        THEN 'SCADUTO' ELSE '' END) as scaduto, 
                        date_add, date_upd 
                        FROM "._DB_PREFIX_."orders 
                        WHERE id_customer = ".$customer->id." 
                        UNION 
                        SELECT id_cart as id, 'Carrello' as tipo, 'Carrello' as tipo_attivita, id_customer as cliente, in_carico_a as in_carico, visualizzato AS status, '' as scaduto, date_add, date_upd FROM carrelli_creati WHERE id_customer = ".$customer->id." AND id_cart NOT IN (SELECT id_cart FROM "._DB_PREFIX_."orders) ORDER BY date_add DESC) x ORDER BY x.date_upd DESC ".(Tools::getIsset('ultime') && Tools::getValue('ultime') == 'vedi_tutte' ? '' : 'LIMIT 10')."
                ");
			}
			
			if(strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== false){
				flush();
			}

			if(Tools::getIsset('orderby')){
                $pre_sql_ultime_cose = 'SELECT * FROM (';

				$sql_ultime_cose = $pre_sql_ultime_cose.$sql_ultime_cose;
				$sql_ultime_cose .= ') y ORDER BY y.'.Tools::getValue('orderby').' '.Tools::getValue('orderway');
            }

            $ultime_cose = Db::getInstance()->executeS($sql_ultime_cose);

            // TUTTO DA CORREGGERE E INVIARE A UNA TABLE NEL TPL! SIMILE A AdminEmployeesController
            if(sizeof($ultime_cose) > 0)
			{	
                // CONTROLLARE TUTTO IL JS E ECHO NELLA 1.4 - NON COPIATO QUI
                
				foreach($ultime_cose as $ultima_cosa) {
					if ($ultima_cosa['tipo'] == 7){
						$ctrl_mex = Db::getInstance()->getValue("SELECT COUNT(id_customer_message) FROM "._DB_PREFIX_."customer_thread ct JOIN "._DB_PREFIX_."customer_message cm ON ct.id_customer_thread = cm.id_customer_thread WHERE cm.id_employee = 0 AND ct.id_customer_thread = ".$ultima_cosa['id']."");	
							
						if($ctrl_mex >= 1) {
							
						} else {
                            $ultima_cosa['status'] = ''; 
                            $ultima_cosa['scaduto'] = '';
						}

					}
						
					if($i<5) {
						$includilo = 'yes';
					}
					else {
						if($ultima_cosa['scaduto'] == 'SCADUTO') {
							$includilo = 'yes';
						}
						else {
							$includilo = 'no';
						}
					
					}
					$conv = "--";
					if($includilo == 'yes') {
						$in_carico = $ultima_cosa['in_carico'];
						
						if(is_numeric($in_carico)) {
							$newinc = new Employee($in_carico);
							$in_carico = $newinc->firstname." ".substr($newinc->lastname, 0,1).".";
						}
						
						$tipo = $ultima_cosa['tipo'];
						if ($tipo == 4){
							$sigla = "T"; $tipo = "Ticket assistenza"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=6">';
							$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM "._DB_PREFIX_."customer_message WHERE id_customer_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
						}
						else if ($tipo == 2){
							$sigla = "A"; $tipo = "Ticket contabilita"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=6">';
							$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM "._DB_PREFIX_."customer_message WHERE id_customer_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
						} 
						else if ($tipo == 3){
							$sigla = "V"; $tipo = "Ticket rivenditori"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=6">';
							$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM "._DB_PREFIX_."customer_message WHERE id_customer_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
						} 
						else if ($tipo == 7){
							$sigla = "M"; $tipo = "Messaggio"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewmessage&id_mex='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=7">';
							$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM "._DB_PREFIX_."customer_message WHERE id_customer_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
							
							$ctrl_mex = Db::getInstance()->getValue("SELECT COUNT(id_customer_message) FROM "._DB_PREFIX_."customer_thread ct JOIN customer_message cm ON ct.id_customer_thread = cm.id_customer_thread WHERE cm.id_employee = 0 AND ct.id_customer_thread = ".$ultima_cosa['id']."");	
							
							if($ctrl_mex >= 1) {
								$status = ''; $scaduto = '';
							}
						}
						else if ($tipo == 8){
							$sigla = "D"; $tipo = "Ticket ordini"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=6">';
							$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM "._DB_PREFIX_."customer_message WHERE id_customer_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
						} 
						else if ($tipo == 9){
							$sigla = "S"; $tipo = "RMA"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewticket&id_customer_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=6">';
							$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM "._DB_PREFIX_."customer_message WHERE id_customer_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
						} 
						else if ($tipo == 'tirichiamiamonoi') {	
							$sigla = "R"; $tipo = "Ti richiamiamo noi"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewpreventivo&id_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=7">';
							$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM form_prevendita_message WHERE id_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
						}
						else if ($tipo == 'preventivo') {	
							$sigla = "P"; $tipo = "Preventivo (ric.)"; $ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewpreventivo&id_thread='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=7">';
							$creato_da = Db::getInstance()->getValue("SELECT id_employee FROM form_prevendita_message WHERE id_thread = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
						}
						else {
							if($tipo == 'Carrello') {
								$ultima_mod_imp = Db::getInstance()->getValue('SELECT date_upd FROM carrelli_creati WHERE id_cart = '.$ultima_cosa['id']);
								/*if($ultima_mod_imp == '' || !$ultima_mod_imp)
									$ultima_mod_imp = $ultima_cosa['date_upd'];*/
								
								$ultima_cosa['date_upd'] = $ultima_mod_imp;
				
								$revisioni = Db::getInstance()->getValue("SELECT revisioni FROM "._DB_PREFIX_."cart WHERE id_cart = ".$ultima_cosa['id']."");
								$ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcart&id_cart='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=4">'; $creato_da = Db::getInstance()->getValue("SELECT created_by FROM "._DB_PREFIX_."cart WHERE id_cart = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
								$in_carico = $ultima_cosa['in_carico'];
								
								
								$n_ord_c = Db::getInstance()->getValue("SELECT id_order FROM "._DB_PREFIX_."orders WHERE id_cart = ".$ultima_cosa['id']."");
								if($n_ord_c > 0) 
								{
									$conv = "<img src='https://www.ezdirect.it/img/admin/enabled.gif' alt='Si' title='Si' />";
								}
								else
								{
									$conv = "<img src='https://www.ezdirect.it/img/admin/red_no.gif' alt='No' title='No' />";
									
								}
						
								if(is_numeric($in_carico)) {
									$newinc = new Employee($in_carico);
									$in_carico = $newinc->firstname." ".substr($newinc->lastname, 0,1).".";
								}
							}
							else if ($tipo == 'Ordine') {
								
								$ord_fatt = Db::getInstance()->getRow('SELECT SQL_CACHE id_fattura, tipo FROM '._DB_PREFIX_.'fattura FORCE INDEX (PRIMARY) WHERE rif_ordine LIKE "'.$ultima_cosa['id'].'"');
			
								if($ord_fatt['id_fattura'] > 0) // correggere href
									$link_fatt = '<a href="https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'&id_fattura='.$ord_fatt['id_fattura'].'&tab-container-1=5&viewfattura&tipo='.$ord_fatt['tipo'].'&updatefattura" target="_blank"><img src="../img/admin/enabled.gif" /> Fatt. </a>';
								else
									$link_fatt = '';
								
								$ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&vieworder&id_order='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=4">'; 
								
								$creato_da = Db::getInstance()->getValue("SELECT created_by FROM "._DB_PREFIX_."cart WHERE id_cart = (SELECT id_cart FROM "._DB_PREFIX_."orders WHERE id_order = ".$ultima_cosa['id'].") ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
								
							}
							else if ($tipo != 'Ordine' && $tipo != 'Carrello' && tipo != '') {
								$ahrefcosa = '<a href="'.$currentIndex.'&id_customer='.$customer->id.'&viewaction&id_action='.$ultima_cosa['id'].'&viewcustomer&token='.$this->token.'&tab-container-1=10">'; $creato_da = Db::getInstance()->getValue("SELECT action_from FROM action_thread WHERE id_action = ".$ultima_cosa['id']." ORDER BY date_add ASC"); if($creato_da == 0) { $creato_da = 'Cliente'; } else { $newinc = new Employee($creato_da); $creato_da = $newinc->firstname." ".substr($newinc->lastname, 0,1)."."; }
							}
							else {
								$ahrefcosa = '<a href="#">';
							}
							
							$sigla = "";
						}
						
						$anno = substr($ultima_cosa['date_upd'],2,2);
						
						if($sigla != '') {
							$id_cosa = $sigla.$anno.$ultima_cosa['id'];
						}
						else {
							$id_cosa = $ultima_cosa['id'];
						}
						
						$status = $ultima_cosa['status']; 
						if($status == 'closed') {
							$status = '<img src="../img/admin/status_green.gif" alt="Chiuso" /> Chiuso';
						}
						else if($status == 'pending1' || $status == 'pending2') {
							$status = '<img src="../img/admin/status_orange.gif" alt="In lavorazione" />In lavorazione';
						}
						else if($status == 'open') {
							$status = '<img src="../img/admin/status_red.gif" alt="Aperto" />Aperto';
						}
						else if(is_numeric($status) && $tipo != 'Carrello') {
							
							if($status == 25 || $status == 24 || $status == 27 || $status == 28)
							{
								$status_lav = Db::getInstance()->getValue("SELECT name FROM "._DB_PREFIX_."order_state_lang WHERE id_lang = ".$this->context->language->id." AND id_order_state = ".$status."");
								
								$status = Db::getInstance()->getValue("SELECT name FROM "._DB_PREFIX_."order_state_lang WHERE id_lang = ".$this->context->language->id." AND id_order_state = ".Db::getInstance()->getValue('SELECT id_order_state FROM '._DB_PREFIX_.'order_history WHERE id_order_history = '.Db::getInstance()->getValue('SELECT MAX(id_order_history) FROM '._DB_PREFIX_.'order_history WHERE id_order = '.$ultima_cosa['id'].' AND id_order_state != 24 AND id_order_state != 25 AND id_order_state != 27 AND id_order_state != 28')));
								
								$status .= ' ('.$status_lav.')';
							}
							else
								$status = Db::getInstance()->getValue("SELECT name FROM "._DB_PREFIX_."order_state_lang WHERE id_lang = ".$this->context->language->id." AND id_order_state = ".$status."");
							
						}
						else if(is_numeric($status) && $tipo == 'Carrello') {
							$numero_notifiche =  Db::getInstance()->getValue("SELECT numero_notifiche FROM "._DB_PREFIX_."cart WHERE id_cart = ".$ultima_cosa['id']);
							$status_s = ($status == 1 ? '<img src="../img/admin/enabled.gif" alt="Letto" title="Letto" />Letto' : '<img src="../img/admin/red_no.gif" alt="Non letto" title="Non letto" />Non letto');
							
							if($numero_notifiche > 0)
								$status_s .= '&nbsp;&nbsp;&nbsp;<img src="../img/admin/email-sent.gif" alt="Carrello notificato al cliente" title="Carrello notificato al cliente" />';
							
							$status = $status_s;
							$preventivo = Db::getInstance()->getValue("SELECT preventivo FROM "._DB_PREFIX_."cart WHERE id_cart = ".$ultima_cosa['id']."");
							if($preventivo == 0)
								$status = '--';
							else
								$tipo = 'C.Preventivo';
						}
						
						if($tipo == 'Carrello' || $tipo == 'Preventivo' || $tipo == 'C.Preventivo') {
							$oggetto_uc = Db::getInstance()->getValue("SELECT name FROM "._DB_PREFIX_."cart WHERE id_cart = ".$ultima_cosa['id']."");
							$cartID = new Cart((int)($ultima_cosa['id']));
							$importo_uc = Tools::displayPrice($cartID->getOrderTotal(false),1);
							$is_order = Db::getInstance()->getValue("SELECT id_cart FROM "._DB_PREFIX_."orders WHERE id_cart = ".$ultima_cosa['id']."");
							if($is_order > 0) 
							{
								$rowOrder =  Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'orders WHERE id_cart = '.$ultima_cosa['id']);
								$importo_uc = Tools::displayPrice(($rowOrder['total_products']+($rowOrder['total_shipping']/(($rowOrder['carrier_tax_rate']/100)+1))), 1);
							}
							else
							{
								$importo_uc = Tools::displayPrice($cartID->getOrderTotal(false),1);
							}
							$id_cosa = $id_cosa."-".$revisioni;
						}
						else if($tipo == 'Ordine') {
							$rowOrder =  Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'orders WHERE id_order = '.$ultima_cosa['id']);
							$oggetto_uc = "";

							if($mio_id == 2)
								$importo_uc = Tools::displayPrice($rowOrder['total_paid_real']);
							else
								$importo_uc = Tools::displayPrice(($rowOrder['total_products']+($rowOrder['total_shipping']/(($rowOrder['carrier_tax_rate']/100)+1))), 1);
						}
						else {
							$oggetto_uc = "";
							$importo_uc = "";
						}

                        // CONTROLLARE TUTTO IL JS E ECHO NELLA 1.4 - NON COPIATO QUI
	
					}
                }
                
                // CONTROLLARE TUTTO IL JS E ECHO NELLA 1.4 - NON COPIATO QUI

			}

        }
        /* FINE Attività recenti */

        /* ALERT TICKET - AZIONI - TO DO IN SOSPESO - BLACKLIST */
        $num_customer_note = Db::getInstance()->getValue('SELECT count(*) FROM customer_note WHERE id_customer = '.$customer->id);
        $num_prv = Db::getInstance()->getValue('SELECT count(*) FROM form_prevendita_thread WHERE status = "open" AND id_customer = '.$customer->id);
        $num_tkt = Db::getInstance()->getValue('SELECT count(*) FROM '._DB_PREFIX_.'customer_thread ct LEFT JOIN '._DB_PREFIX_.'customer_message cm ON ct.id_customer_thread = cm.id_customer_thread WHERE (status = "open") AND (id_contact != 7 OR (cm.id_employee = 0 AND ct.id_contact = 7)	) AND id_customer = '.$customer->id);
        $num_act = Db::getInstance()->getValue('SELECT count(*) FROM action_thread WHERE status = "open" AND id_customer = '.$customer->id);
        
        $num_tot = $num_prv + $num_tkt + $num_act;
        
        $href_ticket = 'index.php?controller=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&tab_name=tickets&token='.$this->token;
        $href_azioni = 'index.php?controller=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&tab_name=actions&token='.$this->token;
        $href_todo = 'index.php?controller=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&tab_name=todo&token='.$this->token;
        $href_note = 'index.php?controller=AdminCustomers&id_customer='.$customer->id.'&viewcustomer&token='.$this->token.'#note';
    
        if($num_tot > 0 || $num_customer_note > 0 || $blacklist == 1){
            $alert = 'Attenzione: ';
            if($num_tkt)
                $alert .= '<a href="'.$href_ticket.'" target="_blank">ticket in sospeso</a> - ';
            if($num_prv)
                $alert .= '<a href="'.$href_azioni.'" target="_blank">azioni cliente in sospeso</a> - ';
            if($num_act)
                $alert .= '<a href="'.$href_todo.'" target="_blank">TO DO in sospeso</a> - ';
            if($num_customer_note)
                $alert .= '<a href="'.$href_note.'" target="_blank">note private in anagrafica</a> - ';
            if($blacklist)
                $alert .= 'CLIENTE IN BLACKLIST';
        }

        /* INIZIO Anagrafica (customer details) */
        if((!Tools::getValue('tab_name') || $tab_name == 'customerdetails') && !$not_mio_agente){

            // Salvataggio modifiche (CORREGGERE: mancano dati perchè espande il salvataggio nativo)
            /*if(Tools::getIsset('submitAddcustomer')) 
            {
                $company = Db::getInstance()->getValue('SELECT company FROM customer WHERE id_customer = '.$_POST['id_customer']);
                $gruppo_c = Db::getInstance()->getValue('SELECT id_default_group FROM customer WHERE id_customer = '.$_POST['id_customer']);
                //echo $company."*".$_POST['company'];die();
                
                if($company != $_POST['company'] && $gruppo_c != 5)
                    Db::getInstance()->execute("UPDATE address SET company = '".addslashes($_POST['company'])."' WHERE fatturazione = 1 AND active = 1 AND deleted = 0 AND id_customer = ".$_POST['id_customer']);
                
                Db::getInstance()->execute("UPDATE customer SET tax_regime = ".$_POST['tax_regime']." WHERE id_customer = ".$_POST['id_customer']."");
                Db::getInstance()->execute("UPDATE customer SET pec = '".$_POST['pec']."' WHERE id_customer = ".$_POST['id_customer']."");
                
                $ultimo_contatto_ar = explode(' ',$_POST['ultimo_contatto']);
                $ultimo_contatto_ar2 = explode('-',$ultimo_contatto_ar[0]);
                $ultimo_contatto_i = $ultimo_contatto_ar2[2].'-'.$ultimo_contatto_ar2[1].'-'.$ultimo_contatto_ar2[0];
                
                Db::getInstance()->execute("UPDATE customer SET codice_univoco = '".$_POST['codice_univoco']."', ipa = '".$_POST['ipa']."', settore = '".$_POST['settore']."', canale = '".$_POST['canale']."', categoria = '".addslashes($_POST['categoria'])."', ultimo_contatto = '".$ultimo_contatto_i."'   WHERE id_customer = ".$_POST['id_customer']."");
                
                if($_POST['supplier'] == 1) {
                    Db::getInstance()->execute("UPDATE customer SET supplier = 1 WHERE id_customer = ".$_POST['id_customer']."");
                }
                else {
                    Db::getInstance()->execute("UPDATE customer SET supplier = 0 WHERE id_customer = ".$_POST['id_customer']."");
                }
            }*/
            
            if(Tools::isSubmit('submitCustomerKeyword')) 
            {
                Db::getInstance()->execute("
                    UPDATE "._DB_PREFIX_."customer 
                    SET keyword = '".Tools::getValue('keyword')."' 
                    WHERE id_customer = ".$customer->id
                );

                $customer->keyword = Tools::getValue('keyword');
            }

            // ADD / EDIT NOTE - Spostate in ajax

            /* // DOVE VA? Non sono le note private
            function cancella_nota_attivita(id) {
                $.ajax({
                        url:"ajax.php?cancellanota_attivita=y",
                        type: "POST",
                        data: { 
                        cancella_nota: \'y\',
                        id_note: id
                        },
                        success:function(){
                        alert("Nota cancellata con successo"); 
                        },
                        error: function(xhr,stato,errori){
                            alert("Errore nella cancellazione:"+xhr.status);
                        }
                });
            }
            */

            if($is_agente)
			    $customer_note = Db::getInstance()->executeS('SELECT * FROM customer_note WHERE id_customer = '.$customer->id.' AND id_employee = '.$mio_id);
		    else
			    $customer_note = Db::getInstance()->executeS('SELECT * FROM customer_note WHERE id_customer = '.$customer->id);
        
            foreach($customer_note as &$nota){
                $creato_da = Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$nota['id_employee']);
                $nota['creato_da'] = $creato_da;
                $nota['date_upd'] = Tools::displayDate($nota['date_upd']);
            }

            $gruppi = Group::getGroups($this->default_form_language, true);

        }
        /* FINE Anagrafica (customer details) */
        
        /* INIZIO Indirizzi */
        if($tab_name == 'addresses' && !$not_mio_agente){

            // Inutilizzati (per ora), ma possono essere utili
            $addresses_fatt = $customer->getAddressesFatturazione($this->context->language->id);
            $addresses_cons = $customer->getAddressesConsegna($this->context->language->id);
            
            // L'employee loggato può cancellare un indirizzo solo se ha profile != 8 (Administrator2)
            $cancella_ind = ($check_admin2 ? false : true);

            // Correggere? Controllare se giusto:
            // Si può aggiungere un indirizzo di fatturazione solo se il cliente non ne ha o se è una P.A.
            $aggiungi_fatt = ((!sizeof($addresses_fatt) || $customer->id_default_group == 5) ? true : false);

            // in link delete, cos'è: &cancellind=1 ?
            // devo sempre mettere ps_address.delete = 0 invece che eliminare la riga

            // NUOVO INDIRIZZO: correggere form addresses

            $indirizzi = array(
                'cancella_ind' => $cancella_ind,
                'aggiungi_fatt' => $aggiungi_fatt
            );
        }
        /* FINE Indirizzi */

        $count_persone = Db::getInstance()->getValue("SELECT COUNT(*) FROM persone WHERE id_customer = ".$customer->id);

        /* INIZIO Persone */
        // Se group = 2 (Privati), non ci sono persone associate
        if($tab_name == 'people' && $customer->id_default_group != 2 && !$not_mio_agente){

            if(Tools::getIsset('cancellapersone') && Tools::getValue('cancella_persona') == 'y')
                Db::getInstance()->execute("DELETE FROM persone WHERE id_persona = ".Tools::getValue('id_persona')."");

            if(isset($_POST['cancella_persona']))
                Db::getInstance()->execute("DELETE FROM persone WHERE id_persona = ".$_POST['id_persona']."");
            
            if(isset($_POST['submitpersone'])) {
                foreach ($_POST['persona_nuovo'] as $id_persona => $persona_id) {
                    if($_POST['persona_nuovo'][$id_persona] == 0) {
                        Db::getInstance()->execute("UPDATE persone SET firstname = '".$_POST['persona_firstname'][$id_persona]."', lastname = '".$_POST['persona_lastname'][$id_persona]."', role = '".$_POST['persona_role'][$id_persona]."', phone = '".$_POST['persona_phone'][$id_persona]."', phone_2 = '".$_POST['persona_phone_2'][$id_persona]."', phone_mobile = '".$_POST['persona_phone_mobile'][$id_persona]."', email = '".$_POST['persona_email'][$id_persona]."' WHERE id_persona = ".$id_persona.""); 
                    }
                    else {
                        // Se la riga è vuota, non la inserisco nel database
                        if(!empty($_POST['persona_firstname'][$id_persona]) || !empty($_POST['persona_lastname'][$id_persona]) 
                            || !empty($_POST['persona_role'][$id_persona]) || !empty($_POST['persona_email'][$id_persona]) 
                            || !empty($_POST['persona_phone'][$id_persona]) || !empty($_POST['persona_phone_2'][$id_persona]) || !empty($_POST['persona_phone_mobile'][$id_persona])) 
                        {
                            Db::getInstance()->execute("
                                INSERT INTO persone(
                                    id_persona,
                                    firstname,
                                    lastname,
                                    role,
                                    email,
                                    email_aziendale,
                                    phone,
                                    phone_2,
                                    phone_mobile,
                                    id_customer,
                                    tax_code,
                                    vat_number
                                )
                                VALUES(
                                    NULL,
                                    '".addslashes($_POST['persona_firstname'][$id_persona])."',
                                    '".addslashes($_POST['persona_lastname'][$id_persona])."',
                                    '".addslashes($_POST['persona_role'][$id_persona])."',
                                    '".addslashes($_POST['persona_email'][$id_persona])."',
                                    '".addslashes(trim($customer->email))."',
                                    '".addslashes($_POST['persona_phone'][$id_persona])."',
                                    '".addslashes($_POST['persona_phone_2'][$id_persona])."',
                                    '".addslashes($_POST['persona_phone_mobile'][$id_persona])."',
                                    '".$customer->id."',
                                    '".addslashes(trim($customer->tax_code))."',
                                    '".addslashes(trim($customer->vat_number))."'
                                )
                            ");
                        }
                    }
                }
            }

            $query_persone = '
                SELECT *
                FROM persone
                WHERE id_customer = '.$customer->id.'
            ';

            $for_persone = Db::getInstance()->executeS($query_persone);
            $persone = array();

            foreach ($for_persone as &$persona) {

                $no_phone = 0; // se = 1, telefono in corsivo
                $no_email = 0; // se = 1, mail in corsivo
                
                $customer_phone = Db::getInstance()->getRow("
                    SELECT phone, phone_mobile 
                    FROM "._DB_PREFIX_."address 
                    WHERE active = 1 
                        AND deleted = 0 
                        AND fatturazione = 1
                        AND id_customer = ".$customer->id
                );
                                
                if($customer_phone['phone'] == '')
                    $telefono_cliente = $customer_phone['phone_mobile']; 
                else
                    $telefono_cliente = $customer_phone['phone']; 
                                
                if(empty($persona['phone']) && empty($persona['phone_2']) && empty($persona['phone_mobile'])){
                    $no_phone = 1; 
                    $persona['phone'] = $telefono_cliente;
                }	
                if(empty($persona['email'])){
                    $no_email = 1; 
                    $persona['email'] = $customer->email;
                }

                $persona['no_phone'] = $no_phone;
                $persona['no_email'] = $no_email;

                $persona['messaggio'] = ((!$not_mio_agente) ? true : false); // true: vedo icona invio messaggio
                $persona['cancella'] = ((!$not_mio_agente && !$check_admin2) ? true : false); // true: vedo icona delete persona

                $persone[] = $persona;
            }

        }
        /* FINE Persone */

        /* INIZIO Amministrazione */
        if($tab_name == 'administration' && !$not_mio_agente){

            $modifica_amm = 0;

            $count_amm = Db::getInstance()->getValue("
                SELECT COUNT(id_customer) AS total 
                FROM customer_amministrazione 
                WHERE id_customer = ".$customer->id
            );

            // Salvataggio modifiche
            if(Tools::getIsset('submitAmministrazione')) {

                $modifica_amm = 1;
                
                $dati_fido = Db::getInstance()->getRow("
                    SELECT fido, data_fido 
                    FROM customer_amministrazione
                    WHERE id_customer = ".$customer->id
                );
                
                $fido_a = (float)(str_replace(",", ".", $dati_fido['fido']));
                $fido_i = (float)(str_replace(",", ".", (str_replace('.', '', $_POST['fido']))));
                    
                if($fido_a != $fido_i && $fido_i > 0)
                    $data_fido_new = date('Y-m-d H:i:s');
                
                if(Tools::getValue('data_fido') != '')
                    $data_fido_new = date('Y-m-d H:i:s', strtotime(Tools::getValue('data_fido')));
                
                // CORREGGERE: pagamento_2 va eliminato? fatturato_annuo era stato tolto, va rimesso?
                if($count_amm == 0) {
                    Db::getInstance()->execute("
                        INSERT INTO customer_amministrazione 
                        (id_customer, tipo_soggetto, codice_fornitore, pagamento, pagamento_2, fido, assicurato, data_fido, no_addebito_commissioni, zona, trasporto_a_mezzo, consegna, vettore, id_corriere, trasporto_gratis_da, fermo_deposito, trasporto_assicurato, note_consegna, iban, swift, paypal, iva_agevolata, esportatore_abituale, sconto_extra, per_ordini_da, fino_a, data_rinnovo, data_revoca, motivo_revoca, rebate, fatturato_annuo, blacklist, agente, tecnico) 
                        VALUES ('".$customer->id."', '".Tools::getValue('tipo_soggetto')."', '".Tools::getValue('codice_fornitore')."', '".Tools::getValue('pagamento')."', '".Tools::getValue('pagamento_2')."', '".(str_replace('.', '', Tools::getValue('fido')))."', '".Tools::getValue('assicurato')."', '".$data_fido_new."', '".(Tools::getIsset('no_addebito_commissioni') ? 1 : 0)."', '".Tools::getValue('zona')."', '".Tools::getValue('trasporto_a_mezzo')."', '".Tools::getValue('consegna')."', '".Tools::getValue('vettore')."', '".Tools::getValue('id_corriere')."', '".Tools::getValue('trasporto_gratis_da')."', '".Tools::getValue('fermo_deposito')."', '".Tools::getValue('trasporto_assicurato')."', '".Tools::getValue('note_consegna')."', '".Tools::getValue('iban')."', '".Tools::getValue('swift')."', '".Tools::getValue('paypal')."', '".Tools::getValue('iva_agevolata')."', '".Tools::getValue('esportatore_abituale')."', '".Tools::getValue('sconto_extra')."', '".Tools::getValue('per_ordini_da')."', '".Tools::getValue('fino_a')."',  '".date('Y-m-d H:i:s',strtotime(Tools::getValue('data_rinnovo')))."', '".date('Y-m-d H:i:s',strtotime(Tools::getValue('data_revoca')))."', '".addslashes(Tools::getValue('motivo_revoca'))."', '".Tools::getValue('rebate')."', '".Tools::getValue('fatturato_annuo')."', '".Tools::getValue('blacklist')."', '".Tools::getValue('agente')."', '".Tools::getValue('tecnico')."')
                    ");
                }
                else {
                    Db::getInstance()->execute("
                        UPDATE customer_amministrazione 
                        SET tipo_soggetto = '".$_POST['tipo_soggetto']."', codice_fornitore = '".$_POST['codice_fornitore']."', pagamento = '".$_POST['pagamento']."', pagamento_2 = '".$_POST['pagamento_2']."', fido = '".(str_replace('.', '', $_POST['fido']))."', assicurato = '".$_POST['assicurato']."', data_fido = '".$data_fido_new."', no_addebito_commissioni = ".(Tools::getIsset('no_addebito_commissioni') ? 1 : 0).", zona = '".$_POST['zona']."', trasporto_a_mezzo = '".$_POST['trasporto_a_mezzo']."', consegna = '".$_POST['consegna']."', vettore = '".$_POST['vettore']."', id_corriere = '".$_POST['id_corriere']."', trasporto_gratis_da = '".$_POST['trasporto_gratis_da']."', fermo_deposito = '".$_POST['fermo_deposito']."', trasporto_assicurato = '".$_POST['trasporto_assicurato']."', note_consegna = '".$_POST['note_consegna']."', iban = '".$_POST['iban']."', swift = '".$_POST['swift']."', paypal = '".$_POST['paypal']."', iva_agevolata = '".$_POST['iva_agevolata']."', esportatore_abituale = '".$_POST['esportare_abituale']."', sconto_extra = '".$_POST['sconto_extra']."', per_ordini_da = '".$_POST['per_ordini_da']."', fino_a = '".$_POST['fino_a']."', rebate = '".$_POST['rebate']."', fatturato_annuo = '".$_POST['fatturato_annuo']."', data_rinnovo = '".date('Y-m-d H:i:s', strtotime(Tools::getValue('data_rinnovo')))."', data_revoca = '".date('Y-m-d H:i:s', strtotime(Tools::getValue('data_revoca')))."', motivo_revoca = '".$_POST['motivo_revoca']."',  blacklist = '".$_POST['blacklist']."', agente = '".$_POST['agente']."', tecnico = '".$_POST['tecnico']."' 
                        WHERE id_customer = ".$customer->id
                    );
                }
                
                $blacklist = Db::getInstance()->getValue('
                    SELECT blacklist 
                    FROM customer_amministrazione 
                    WHERE id_customer = '.$customer->id
                );
                
                /* // Mostra un avviso se il cliente è in blacklist
                if($blacklist == 1)
                {
                    echo '<script type="text/javascript">
                        $(document).ready(function(){
                            swal({
                                type: "error",
                                title: "ATTENZIONE",
                                text: "Questo cliente è in blacklist!",
                                footer: ""
                            })
                        });
                    </script>';
                }*/
            }

            // Eseguo la query solo se non l'ho già fatto o se sono state fatte modifiche
            if(!$amministrazione_c || $modifica_amm){
                $query_amministrazione = '
                    SELECT *
                    FROM customer_amministrazione
                    WHERE id_customer = '.$customer->id.'
                ';
                // Ogni cliente dovrebbe avere nessuna o una row in customer_amministrazione (verificare e correggere le eccezioni)
                $amministrazione = Db::getInstance()->getRow($query_amministrazione);
            }
            else
                $amministrazione = $amministrazione_c;

            if(!$agente_c || $modifica_amm){
                $query_agente = '
                    SELECT id_employee as id, CONCAT(firstname, " ", LEFT(lastname,1), ".") as name
                    FROM '._DB_PREFIX_.'employee
                    WHERE id_employee = '.$amministrazione['agente'].'
                ';
                $agente = Db::getInstance()->getRow($query_agente);
            }
            else
                $agente = $agente_c;
            
            if(!$tecnico_c || $modifica_amm){
                $query_tecnico = '
                    SELECT id_employee as id, CONCAT(firstname, " ", LEFT(lastname,1), ".") as name
                    FROM '._DB_PREFIX_.'employee
                    WHERE id_employee = '.$amministrazione['tecnico'].'
                ';
                $tecnico = Db::getInstance()->getRow($query_tecnico);
            }
            else
                $tecnico = $tecnico_c;

            $acquisti_ultimo_anno = Db::getInstance()->getValue('SELECT SUM(imponibile) AS ultimo_anno FROM (SELECT * FROM '._DB_PREFIX_.'fattura WHERE id_customer = '.$customer->id.' AND data_fattura > "'.date("Y").'-01-01 00:00:00" AND data_fattura < "'.date("Y").'-12-31 00:00:00" GROUP BY id_fattura) f');
            if(!$acquisti_ultimo_anno)
                $acquisti_ultimo_anno = 0;

            $acquisti_totali = Db::getInstance()->getValue('SELECT SUM(imponibile) AS totale_acquisti FROM (SELECT * FROM '._DB_PREFIX_.'fattura WHERE id_customer = '.$customer->id.' GROUP BY id_fattura) f');
            if(!$acquisti_totali)
                $acquisti_totali = 0;

            // FIDO RIVENDITORE
			if($customer->id_default_group == 3 || $customer->id_default_group == 15 || $customer->id_default_group == 22){
				
                /*$acquisti_tre_mesi = Db::getInstance()->getValue('SELECT SUM(imponibile) AS tre_mesi FROM (SELECT * FROM '._DB_PREFIX_.'fattura WHERE id_customer = '.$customer->id.' AND data_fattura > (CURRENT_DATE() - INTERVAL 3 MONTH) AND data_fattura < CURRENT_DATE() GROUP BY id_fattura) f');*/
                $acquisti_tre_mesi = Db::getInstance()->getValue('SELECT SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) AS netto FROM '._DB_PREFIX_.'order_detail JOIN '._DB_PREFIX_.'orders ON orders.id_order = order_detail.id_order LEFT JOIN '._DB_PREFIX_.'order_history oh ON oh.id_order_state = 6 AND oh.id_order = orders.id_order WHERE oh.id_order_state IS NULL  AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment"))) AND orders.id_customer = '.$customer->id.' AND orders.date_add > (CURRENT_DATE() - INTERVAL 3 MONTH) AND orders.date_add < CURRENT_DATE()');
                if(!$acquisti_tre_mesi)
                    $acquisti_tre_mesi = 0;

                $acquisti_mille = Db::getInstance()->executeS('SELECT SUM((order_detail.`product_price` - ((order_detail.product_price * order_detail.reduction_percent) / 100)) * order_detail.`product_quantity` / orders.conversion_rate) AS netto FROM '._DB_PREFIX_.'order_detail JOIN '._DB_PREFIX_.'orders ON orders.id_order = order_detail.id_order LEFT JOIN '._DB_PREFIX_.'order_history oh ON oh.id_order_state = 6 AND oh.id_order = orders.id_order WHERE oh.id_order_state IS NULL  AND (orders.valid = 1 OR (orders.valid = 0 AND (orders.module = "bankwire" OR orders.module = "other_payment"))) AND orders.id_customer = '.$customer->id.' AND orders.date_add > (CURRENT_DATE() - INTERVAL 3 MONTH) AND orders.date_add < CURRENT_DATE() GROUP BY orders.id_order');
                
                //$ordine_mille = 'No';
                $ordine_mille = 0;
                
                foreach($acquisti_mille as $mille){
                    if($mille['netto'] >= 1000){
                        //$ordine_mille = 'Sì';
                        $ordine_mille = 1;
                        //$ordine_mille_netto = $mille['netto'];
                        break;
                    }
                }
                
                //if($acquisti_tre_mesi >= 1500 || $ordine_mille == 'Sì')
                if($acquisti_tre_mesi >= 1500 || $ordine_mille == 1)
                    $fido_ok = 1;
                else
                    $fido_ok = 0;
            }

            // PROVA; unset delle variabili, per poi gestirlo con !isset nel tpl
            if($is_agente){
                // Gli agenti non vedono il codice fornitore
                unset($amministrazione['codice_fornitore']);
            }

            $fido = $amministrazione['fido'];
            $fido = str_replace(".","",$fido);
			$fido = (float)(str_replace(",",".",$fido));
			
			$somma_fido = Db::getInstance()->getValue('SELECT SUM(total_paid_real-acconto) FROM '._DB_PREFIX_.'orders WHERE id_customer = '.$customer->id.' AND (payment = "Bonifico 30 gg. fine mese" OR payment = "Bonifico 60 gg. fine mese" OR payment = "Bonifico 90 gg. fine mese" OR payment = "R.B. 30 GG. D.F. F.M." OR payment = "R.B. 60 GG. D.F. F.M." OR payment = "R.B. 90 GG. D.F. F.M." OR payment = "R.B. 30 GG. 5 mese successivo" OR payment = "R.B. 30 GG. 10 mese successivo" OR payment = "R.B. 60 GG. 5 mese successivo" OR payment = "R.B. 60 GG. 10 mese successivo"  OR payment LIKE "%saldo%")  AND id_order NOT IN (SELECT id_order FROM '._DB_PREFIX_.'order_history WHERE id_order_state = 18 OR id_order_state = 32)  AND date_add > "'.$data_fido.'"');

			$somma_fido_32 = Db::getInstance()->getValue('SELECT SUM(total_paid_real-acconto) FROM '._DB_PREFIX_.'orders o JOIN (SELECT id_order_state, id_order FROM '._DB_PREFIX_.'order_history WHERE id_order_state = 32) oh ON o.id_order = oh.id_order WHERE id_customer = '.$customer->id.' AND (payment = "Bonifico 30 gg. fine mese" OR payment = "Bonifico 60 gg. fine mese" OR payment = "Bonifico 90 gg. fine mese" OR payment = "Bonifico 30 gg. 15 mese successivo" OR payment = "R.B. 30 GG. D.F. F.M." OR payment = "R.B. 60 GG. D.F. F.M." OR payment = "R.B. 90 GG. D.F. F.M." OR payment = "R.B. 30 GG. 5 mese successivo" OR payment = "R.B. 30 GG. 10 mese successivo" OR payment = "R.B. 60 GG. 5 mese successivo" OR payment = "R.B. 60 GG. 10 mese successivo"  OR payment LIKE "%saldo%")  AND id_order NOT IN (SELECT id_order FROM '._DB_PREFIX_.'order_history WHERE id_order_state = 18 OR id_order_state = 32)  AND date_add > "'.$data_fido.'"');
			
            $crediti_fido = $fido - $somma_fido + $somma_fido_32;
            
			//$blacklist = $amministrazione['blacklist'];
			if($blacklist == 1)
				$crediti_fido = 0;
			
			if($fido == 0)
				$crediti_fido = '';

            switch ($amministrazione['trasporto_a_mezzo']) {
                case 1:
                    $trasporto_a_mezzo = 'Mittente';
                    break;
                case 2:
                    $trasporto_a_mezzo = 'Destinatario';
                    break;
                case 3:
                    $trasporto_a_mezzo = 'Vettore';
                    break;
                case 4:
                    $trasporto_a_mezzo = 'Ritiro in sede';
                    break;
                
                default:
                    $trasporto_a_mezzo = '?';
                    break;
            }

            // CORREGGERE
            switch ($amministrazione['consegna']) {
                case 1:
                    $consegna = 'Franco'; // togliere o sostituire con ritiro in sede; Se selezionato, Ritiro in sede diventa automaticamente il tipo di trasporto in tutti i preventivi/ordini (manuali e non) del cliente
                    break;
                case 2:
                    $consegna = 'Assegnato (corriere del cliente)'; // aprire un todo per avvisare il cliente a merce pronta e azzerare costi nei prev/ordini
                    break;
                case 3:
                    $consegna = 'Franco Add. in Fattura (ns corriere)';
                    break;
                
                default:
                    $consegna = '?';
                    break;
            }

            switch ($amministrazione['vettore']) {
                case 1043:
                    $vettore = 'GLS National Express s.r.l.';
                    break;
                case 1062:
                    $vettore = 'UPS Italia s.r.l.';
                    break;
                case 1245:
                    $vettore = 'TNT Global Express spa';
                    break;
                case 1269:
                    $vettore = 'SDA Trasporti';
                    break;
                case 1271:
                    $vettore = 'DHL Trasporti';
                    break;
                case 1752:
                    $vettore = 'Ascoli Trasporti';
                    break;
                case 1753:
                    $vettore = 'Poste Trasporti';
                    break;

                default:
                    $vettore = '?';
                    break;
            }
            
            $impiegati_amm = Db::getInstance()->executeS('SELECT id_employee as id, CONCAT(firstname," ",LEFT(lastname, 1),".") AS name FROM '._DB_PREFIX_.'employee WHERE active = 1 ORDER BY firstname ASC, lastname ASC');
            //$tecnici_amm = Db::getInstance()->executeS('SELECT id_employee as id, CONCAT(firstname," ",LEFT(lastname, 1),".") AS name FROM '._DB_PREFIX_.'employee WHERE id_employee = 4 OR id_employee = 5 OR id_employee = 12 OR id_employee = 17 ORDER BY firstname ASC, lastname ASC');
            // correggere: togliere id_employee = 6 finiti gli employee; la riga sopra è quella giusta
            $tecnici_amm = Db::getInstance()->executeS('SELECT id_employee as id, CONCAT(firstname," ",LEFT(lastname, 1),".") AS name FROM '._DB_PREFIX_.'employee WHERE id_employee = 4 OR id_employee = 5 OR id_employee = 6 ORDER BY firstname ASC, lastname ASC');

            $amministrazione['agente_value'] = $agente['id'];
            $amministrazione['tecnico_value'] = $tecnico['id'];
            $amministrazione['agente'] = $agente['name'];
            $amministrazione['tecnico'] = $tecnico['name'];

            $amministrazione['agenti'] = $impiegati_amm;
            $amministrazione['tecnici'] = $tecnici_amm;
            $amministrazione['fatturato_annuo'] = Tools::displayPrice($acquisti_ultimo_anno, $this->context->currency->id);
            $amministrazione['fatturato_totale'] = Tools::displayPrice($acquisti_totali, $this->context->currency->id);
            $amministrazione['acquisti_tre_mesi'] = Tools::displayPrice($acquisti_tre_mesi, $this->context->currency->id);
            $amministrazione['ordine_mille'] = $ordine_mille;
            $amministrazione['fido_ok'] = $fido_ok;
            $amministrazione['fido'] = number_format($fido, 2, ',', '');
            $amministrazione['fido_residuo'] = number_format($crediti_fido, 2, ',', '');
            $amministrazione['trasporto_a_mezzo_value'] = $amministrazione['trasporto_a_mezzo'];
            $amministrazione['consegna_value'] = $amministrazione['consegna'];
            $amministrazione['vettore_value'] = $amministrazione['vettore'];
            $amministrazione['trasporto_a_mezzo'] = $trasporto_a_mezzo;
            $amministrazione['consegna'] = $consegna;
            $amministrazione['vettore'] = $vettore;

            $amministrazione['count_record'] = $count_amm;

        }
        /* FINE Amministrazione */

        $count_ordini = $total_orders; // controllare funzione nella classe

        /* INIZIO Ordini */
        if($tab_name == 'orders' && !$not_mio_agente){

            $ordini = array();

        }
        /* FINE Ordini */
        
        $carts = Cart::getCustomerCarts($customer->id);
        $count_carrelli = count($carts);

        /* INIZIO Carrelli */
        if($tab_name == 'carts' && !$not_mio_agente){

            for ($i = 0; $i < $count_carrelli; $i++) {
                $cart = new Cart((int)$carts[$i]['id_cart']);
                $this->context->cart = $cart;
                $currency = new Currency((int)$carts[$i]['id_currency']);
                $this->context->currency = $currency;
                $summary = $cart->getSummaryDetails();
                $carrier = new Carrier((int)$carts[$i]['id_carrier']);
                $carts[$i]['id_cart'] = sprintf('%06d', $carts[$i]['id_cart']);
                $carts[$i]['date_add'] = Tools::displayDate($carts[$i]['date_add'], null, true);
                $carts[$i]['total_price'] = Tools::displayPrice($summary['total_price'], $currency);
                $carts[$i]['name'] = $carrier->name; // cambiare nome variabile: ps_cart.name è il campo corrispondente all'oggetto del carrello
                $carts[$i]['conv'] = $cart->convertito; // aggiunto convertito alla classe cart
                $carts[$i]['read'] = $cart->visualizzato; // aggiunto visualizzato alla classe cart
                $carts[$i]['quote'] = $cart->preventivo; // aggiunto preventivo alla classe cart
            }

            $carrelli = $carts;

        }
        /* FINE Carrelli */

        $count_fatture = Db::getInstance()->getValue("SELECT COUNT(DISTINCT id_fattura) FROM "._DB_PREFIX_."fattura WHERE id_customer = ".$customer->id);

        /* INIZIO Fatture */
        if($tab_name == 'invoices' && !$is_agente){ // tab-container-1=5

            if($count_fatture)
            {
                // Lista fatture del cliente
                if(!(Tools::getIsset('azione') && Tools::getValue('azione') == 'fattura_view'))
                {
                    $fatture_rows = Db::getInstance()->executeS("
                        SELECT * 
                        FROM "._DB_PREFIX_."fattura 
                        WHERE id_customer = ".$customer->id." 
                        GROUP BY id_fattura
                        ORDER BY data_fattura DESC
                    ");

                    foreach($fatture_rows as &$fattura) {
                        $fattura['tipo'] = ($fattura['tipo'] != "" ? $fattura['tipo'] : 'ezdirect');
                        $fattura['data_fattura'] = date("d/m/Y", strtotime($fattura['data_fattura']));
                        $fattura['imponibile'] = Tools::displayPrice($fattura['imponibile'], $this->context->currency->id);
                        $fattura['totale_fattura'] = Tools::displayPrice($fattura['totale_fattura'], $this->context->currency->id);
                        
                        // Azione INVIA PER MAIL AL CLIENTE
                        // vedi fatture_js.tpl

                        $fattura_products = Db::getInstance()->executeS('
                            SELECT cod_articolo, desc_articolo, qt_ord, qt_sped, prezzo, importo_netto 
                            FROM '._DB_PREFIX_.'fattura 
                            WHERE id_fattura = "'.$fattura['id_fattura'].'" 
                                AND id_customer = '.$fattura['id_customer']
                        );

                        foreach ($fattura_products as &$product) {
                            $id_prodotto = Db::getInstance()->getValue('
                                SELECT id_product 
                                FROM '._DB_PREFIX_.'product 
                                WHERE reference = "'.$product['cod_articolo'].'"
                            ');

                            if($id_prodotto){
                                $product['id'] = $id_prodotto;
                                $product['tooltip'] = Product::showProductTooltip($id_prodotto);
                            }
                            else
                                $product['id'] = 0;

                            $product['qt_sped'] = (int)$product['qt_sped'];
                            $product['prezzo'] = Tools::displayPrice($product['prezzo'], $this->context->currency->id, false);
                            $product['importo_netto'] = Tools::displayPrice($product['importo_netto'], $this->context->currency->id, false);
                        }
                        
                        $fattura['prodotti'] = $fattura_products;
                    }
                }
                else // View fattura
                {
                    // CORREGGERE: SPOSTARE IN UNA FUNZIONE DA USARE SIA PER INVOICES CHE PER CREDIT NOTES
                    $id_customer = $customer->id;
                    $id_fattura = Tools::getValue('id_fattura');
                    // tabella ps_fattura o ps_note_di_credito
                    $tabella_fatture = _DB_PREFIX_.(Tools::getIsset('tipo_fattura') && Tools::getValue('tipo_fattura') == 'nota' ? 'note_di_credito' : 'fattura');

                    /* DATI */

                    $datiFattura = Db::getInstance()->getRow("
                        SELECT * 
                        FROM (
                            SELECT * 
                            FROM ".$tabella_fatture."
                            WHERE id_fattura = '".$id_fattura."' 
                                AND iva_percent >= 0 
                                AND id_customer = ".$id_customer." 
                                ORDER BY num_riga
                        ) t 
                        GROUP BY ".$id_fattura
                    );

                    $tec_ez = ($datiFattura['tipo'] ? $datiFattura['tipo'] : 'ezdirect');
                
                    $numero_righe = Db::getInstance()->getValue("
                        SELECT count(num_riga) AS tot 
                        FROM ".$tabella_fatture." 
                        WHERE id_fattura = '".$id_fattura."' 
                            AND iva_percent >= 0 
                            AND id_customer = ".$id_customer." 
                        ORDER BY num_riga
                    ");
                
                    $datiCliente = Db::getInstance()->getRow("
                        SELECT * 
                        FROM "._DB_PREFIX_."customer 
                        WHERE id_customer = ".$datiFattura['id_customer']
                    );

                    //$customer = new Customer($id_customer); // ok se spostato fuori da AdminCustomers
                    $customerStats = $customer->getStats();

                    // Informazioni sul cliente

                    $tipo_cliente = ($customer->is_company ? 'AZIENDA' : 'PRIVATO');
                    $nome_cliente = ($customer->is_company ? $customer->company : $customer->firstname.' '.$customer->lastname);
                    $data_creazione_account = Tools::displayDate($customer->date_add, null, true);
                    $ordini_totali = $customerStats['nb_orders'];

                    $acquisti_totali = Db::getInstance()->getValue('
                        SELECT SUM(imponibile) AS totale_acquisti 
                        FROM (
                            SELECT * 
                            FROM '.$tabella_fatture.' 
                            WHERE id_customer = '.$customer->id.'
                            GROUP BY id_fattura
                        ) f
                    ');
                    $acquisti_totali = Tools::displayPrice(Tools::ps_round($acquisti_totali, 2), $this->context->currency->id);

                    $info_cliente = array(
                        'tipo_cliente' => $tipo_cliente,
                        'nome_cliente' => $nome_cliente,
                        'data_creazione_account' => $data_creazione_account,
                        'ordini_totali' => $ordini_totali,
                        'acquisti_totali' => $acquisti_totali,
                    );

                    // Dati fattura/nota

                    $tipo = ((Tools::getIsset('tipo_fattura') && Tools::getValue('tipo_fattura') == 'nota') ? 'nota' : 'fattura');
                    $codice_cliente = $datiFattura['codice_cliente'];
                    $piva_cf = $customer->vat_number.' '.$customer->tax_code;
                    $numero_documento = explode('-', $datiFattura['id_fattura']);
                    $numero_documento = $numero_documento[0];
                    $data_documento = date("d/m/Y", strtotime($datiFattura['data_fattura']));
                    $pagamento = $datiFattura['pagamento'];
                    $valuta = 'EURO';
                    $rif_vs_ordine = $datiFattura['rif_vs_ordine'];

                    $dati_fattura = array(
                        'id' => $id_fattura,
                        'tipo' => $tipo,
                        'codice_cliente' => $codice_cliente,
                        'piva_cf' => $piva_cf,
                        'numero_documento' => $numero_documento,
                        'data_documento' => $data_documento,
                        'pagamento' => $pagamento,
                        'valuta' => $valuta,
                        'rif_ordine_sito' => $rif_vs_ordine,
                    );

                    // Indirizzo di fatturazione
                    
                    $dati_fatturazione = $datiFattura['dati_fatturazione'];
                    $dati_fatturazione = explode('--', $dati_fatturazione);

                    $fatturazione = array(
                        'spettle' => $dati_fatturazione[0],
                        'indirizzo' => $dati_fatturazione[1],
                        'cap' => $dati_fatturazione[2],
                        'citta' => $dati_fatturazione[3],
                        'prov' => $dati_fatturazione[4],
                    );

                    // Destinazione

                    $dati_spedizione = $datiFattura['dati_spedizione'];
                    $dati_spedizione = explode('--', $dati_spedizione);

                    $destinazione = array(
                        'spettle' => $dati_spedizione[0],
                        'indirizzo' => $dati_spedizione[1],
                        'cap' => $dati_spedizione[2],
                        'citta' => $dati_spedizione[3],
                        'prov' => $dati_spedizione[4],
                    );
 
                    // Prodotti

                    $prodottiFattura = Db::getInstance()->executeS("
                        SELECT * 
                        FROM ".$tabella_fatture." 
                        WHERE id_fattura = '".$id_fattura."' 
                            AND id_customer = ".$id_customer." 
                        ORDER BY num_riga
                    ");

                    $rif_ordine = explode(' ', $datiFattura['rif_ordine']);
                    $rif_ordine = $rif_ordine[0];

                    $num_prod = 0;
                    $prodotti = array();
                    
                    foreach($prodottiFattura as $rigaFattura) {
                        $num_prod++;
                        
                        if($num_prod > 9999999) {
                            // niente?
                        }
                        else {
                            if(!empty($rigaFattura['desc_articolo']))
                            {
                                $prodotto = array();

                                $id_prodotto = Db::getInstance()->getValue('
                                    SELECT id_product 
                                    FROM '._DB_PREFIX_.'product 
                                    WHERE reference LIKE "%'.$rigaFattura['cod_articolo'].'%"
                                ');

                                // CORREGGERE: manca controllo id > 0

                                $prodotto['id'] = $id_prodotto; // serve per tooltip
                                $prodotto['tooltip'] = Product::showProductTooltip($id_prodotto); // tooltip
                                $prodotto['cod'] = $rigaFattura['cod_articolo'];
                                $prodotto['desc'] = $rigaFattura['desc_articolo'];
                                $prodotto['um'] = ($rigaFattura['qt_sped'] == 0 ? '' : 'N.');
                                $prodotto['qta'] = ($rigaFattura['qt_sped'] == 0 ? '' : $rigaFattura['qt_sped']);
                                $prodotto['rif_ordine'] = $rif_ordine;
                                $prodotto['prezzo'] = ($rigaFattura['qt_sped'] == 0 ? '' : number_format($rigaFattura['prezzo'], 2, ',', ''));
                                $prodotto['sconti'] = ($rigaFattura['sconto1'] != 0 ? number_format($rigaFattura['sconto1'], 2, ',', '') : '');
                                $prodotto['importo'] = ($rigaFattura['qt_sped'] == 0 ? '' : number_format($rigaFattura['importo_netto'], 2, ',', ''));
                                $prodotto['ci'] = ($rigaFattura['qt_sped'] == 0 ? '' : (int)$rigaFattura['iva_percent']);

                                $prodotti[] = $prodotto;
                            }
                        }
                    }

                    // Totali

                    $tot_merce = Tools::displayPrice($datiFattura['imponibile'], $this->context->currency->id);
                    $c_iva = number_format($datiFattura['iva_percent'], 0, ',', ''); // %
                    $imponibile = $tot_merce;
                    $iva = Tools::displayPrice($datiFattura['iva'], $this->context->currency->id);
                    $tot_fattura = Tools::displayPrice($datiFattura['totale_fattura'], $this->context->currency->id);

                    $totali = array(
                        'tot_merce' => $tot_merce,
                        'c_iva' => $c_iva,
                        'imponibile' => $imponibile,
                        'iva' => $iva,
                        'tot_fattura' => $tot_fattura,
                    );

                    // Trasporto

                    $causale_trasporto = $datiFattura['causale_trasporto'];
                    $trasporto_a_cura = 'Vettore';
                    $vettore = $datiFattura['dati_vettore'];
                    $porto = $datiFattura['desc_porto'];
                    $colli = $datiFattura['num_colli'];
                    $peso = number_format($datiFattura['peso'], 2, ',', '');
                    $data_trasporto = $data_documento;

                    $trasporto = array(
                        'causale_trasporto' => $causale_trasporto,
                        'trasporto_a_cura' => $trasporto_a_cura,
                        'vettore' => $vettore,
                        'porto' => $porto,
                        'colli' => $colli,
                        'peso' => $peso,
                        'data_trasporto' => $data_trasporto,
                    );

                    $dati = array(
                        'tec_ez' => $tec_ez,
                        'info_cliente' => $info_cliente,
                        'dati_fattura' => $dati_fattura,
                        'fatturazione' => $fatturazione,
                        'destinazione' => $destinazione,
                        'prodotti' => $prodotti,
                        'totali' => $totali,
                        'trasporto' => $trasporto,
                    );

                    /* STORICO */
                    // Disattivato!
                    
                    /*
                    $id_storico = Db::getInstance()->getValue('
                        SELECT id_riga 
                        FROM '.$tabella_fatture.' 
                        WHERE id_fattura = "'.$id_fattura.'" 
                        ORDER BY id_riga ASC
                    ');
                    
                    $storico_fatt = Db::getInstance()->executeS('
                        SELECT * 
                        FROM storico_attivita 
                        WHERE id_attivita = '.$id_storico.' 
                            AND tipo_attivita = "F" 
                        ORDER BY data_attivita DESC, desc_attivita DESC
                    ');
                    
                    if(sizeof($storico_fatt) > 0) {
                        // table: 
                        // Persona ($storico['id_employee'] == 0 ? 'Cliente' : $name_employee_storico) - Azione ($desc_attivita) - Data ($storico['data_attivita'])
                        // per tutti i campi vale: se $storico['id_employee'] == 0, scritta rossa e in grassetto
                        foreach($storico_fatt as $storico)
                        {
                            if(is_numeric(substr($storico['desc_attivita'], -1))){	
                                $employee = filter_var($storico['desc_attivita'], FILTER_SANITIZE_NUMBER_INT);
                                $name_employee = Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$employee);
                                $desc_attivita = 'Ha messo l\'attivit&agrave; in carico a '.$name_employee;
                            }
                            else
                                $desc_attivita = $storico['desc_attivita'];
                            
                            // $name_employee_storico = Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$storico['id_employee']);
                        }
                    }
                    */
                }
            }
            
            $fatture = array(
                'fatture_rows' => $fatture_rows,
                'dati' => $dati,
                //'storico' => $storico,
            );

        }
        /* FINE Fatture */

        $count_note_di_credito = Db::getInstance()->getValue("SELECT COUNT(DISTINCT id_fattura) FROM "._DB_PREFIX_."note_di_credito WHERE id_customer = ".$customer->id);

        /* INIZIO Note di credito */
        if($tab_name == 'credit_notes' && !$is_agente){ // non presente in PS 1.4
            
            if($count_note_di_credito)
            {
                // Lista note di credito del cliente
                if(!(Tools::getIsset('azione') && Tools::getValue('azione') == 'nota_di_credito_view'))
                {
                    $note_di_credito_rows = Db::getInstance()->executeS("
                        SELECT * 
                        FROM "._DB_PREFIX_."note_di_credito 
                        WHERE id_customer = ".$customer->id." 
                        GROUP BY id_fattura
                        ORDER BY data_fattura DESC
                    ");

                    foreach($note_di_credito_rows as &$nota) {
                        $nota['tipo'] = ($nota['tipo'] != "" ? $nota['tipo'] : 'ezdirect');
                        $nota['data_fattura'] = date("d/m/Y", strtotime($nota['data_fattura']));
                        $nota['imponibile'] = Tools::displayPrice($nota['imponibile'], $this->context->currency->id);
                        $nota['totale_fattura'] = Tools::displayPrice($nota['totale_fattura'], $this->context->currency->id);

                        $nota_products = Db::getInstance()->executeS('
                            SELECT cod_articolo, desc_articolo, qt_ord, qt_sped, prezzo, importo_netto 
                            FROM '._DB_PREFIX_.'note_di_credito 
                            WHERE id_fattura = "'.$nota['id_fattura'].'" 
                                AND id_customer = '.$nota['id_customer']
                        );

                        foreach ($nota_products as &$product) {
                            $id_prodotto = Db::getInstance()->getValue('
                                SELECT id_product 
                                FROM '._DB_PREFIX_.'product 
                                WHERE reference = "'.$product['cod_articolo'].'"
                            ');

                            if($id_prodotto){
                                $product['id'] = $id_prodotto;
                                $product['tooltip'] = Product::showProductTooltip($id_prodotto);
                            }
                            else
                                $product['id'] = 0;
                            
                            $product['qt_sped'] = (int)$product['qt_sped'];
                            $product['prezzo'] = Tools::displayPrice($product['prezzo'], $this->context->currency->id, false);
                            $product['importo_netto'] = Tools::displayPrice($product['importo_netto'], $this->context->currency->id, false);
                        }
                        
                        $nota['prodotti'] = $nota_products;
                    }
                }
                else // View nota di credito
                {
                    // CORREGGERE: SPOSTARE IN UNA FUNZIONE DA USARE SIA PER INVOICES CHE PER CREDIT NOTES
                    $id_customer = $customer->id;
                    $id_fattura = Tools::getValue('id_fattura');
                    // tabella ps_fattura o ps_note_di_credito
                    $tabella_fatture = _DB_PREFIX_.(Tools::getIsset('tipo_fattura') && Tools::getValue('tipo_fattura') == 'nota' ? 'note_di_credito' : 'fattura');

                    /* DATI */

                    $datiFattura = Db::getInstance()->getRow("
                        SELECT * 
                        FROM (
                            SELECT * 
                            FROM ".$tabella_fatture."
                            WHERE id_fattura = '".$id_fattura."' 
                                AND iva_percent >= 0 
                                AND id_customer = ".$id_customer." 
                                ORDER BY num_riga
                        ) t 
                        GROUP BY ".$id_fattura
                    );

                    $tec_ez = ($datiFattura['tipo'] ? $datiFattura['tipo'] : 'ezdirect');
                
                    $numero_righe = Db::getInstance()->getValue("
                        SELECT count(num_riga) AS tot 
                        FROM ".$tabella_fatture." 
                        WHERE id_fattura = '".$id_fattura."' 
                            AND iva_percent >= 0 
                            AND id_customer = ".$id_customer." 
                        ORDER BY num_riga
                    ");
                
                    $datiCliente = Db::getInstance()->getRow("
                        SELECT * 
                        FROM "._DB_PREFIX_."customer 
                        WHERE id_customer = ".$datiFattura['id_customer']
                    );

                    //$customer = new Customer($id_customer); // ok se spostato fuori da AdminCustomers
                    $customerStats = $customer->getStats();

                    // Informazioni sul cliente

                    $tipo_cliente = ($customer->is_company ? 'AZIENDA' : 'PRIVATO');
                    $nome_cliente = ($customer->is_company ? $customer->company : $customer->firstname.' '.$customer->lastname);
                    $data_creazione_account = Tools::displayDate($customer->date_add, null, true);
                    $ordini_totali = $customerStats['nb_orders'];

                    $acquisti_totali = Db::getInstance()->getValue('
                        SELECT SUM(imponibile) AS totale_acquisti 
                        FROM (
                            SELECT * 
                            FROM '.$tabella_fatture.' 
                            WHERE id_customer = '.$customer->id.'
                            GROUP BY id_fattura
                        ) f
                    ');
                    $acquisti_totali = Tools::displayPrice(Tools::ps_round($acquisti_totali, 2), $this->context->currency->id);

                    $info_cliente = array(
                        'tipo_cliente' => $tipo_cliente,
                        'nome_cliente' => $nome_cliente,
                        'data_creazione_account' => $data_creazione_account,
                        'ordini_totali' => $ordini_totali,
                        'acquisti_totali' => $acquisti_totali,
                    );

                    // Dati fattura/nota

                    $tipo = ((Tools::getIsset('tipo_fattura') && Tools::getValue('tipo_fattura') == 'nota') ? 'nota' : 'fattura');
                    $codice_cliente = $datiFattura['codice_cliente'];
                    $piva_cf = $customer->vat_number.' '.$customer->tax_code;
                    $numero_documento = explode('-', $datiFattura['id_fattura']);
                    $numero_documento = $numero_documento[0];
                    $data_documento = date("d/m/Y", strtotime($datiFattura['data_fattura']));
                    $pagamento = $datiFattura['pagamento'];
                    $valuta = 'EURO';
                    $rif_vs_ordine = $datiFattura['rif_vs_ordine'];

                    $dati_fattura = array(
                        'id' => $id_fattura,
                        'tipo' => $tipo,
                        'codice_cliente' => $codice_cliente,
                        'piva_cf' => $piva_cf,
                        'numero_documento' => $numero_documento,
                        'data_documento' => $data_documento,
                        'pagamento' => $pagamento,
                        'valuta' => $valuta,
                        'rif_ordine_sito' => $rif_vs_ordine,
                    );

                    // Indirizzo di fatturazione
                    
                    $dati_fatturazione = $datiFattura['dati_fatturazione'];
                    $dati_fatturazione = explode('--', $dati_fatturazione);

                    $fatturazione = array(
                        'spettle' => $dati_fatturazione[0],
                        'indirizzo' => $dati_fatturazione[1],
                        'cap' => $dati_fatturazione[2],
                        'citta' => $dati_fatturazione[3],
                        'prov' => $dati_fatturazione[4],
                    );

                    // Destinazione

                    $dati_spedizione = $datiFattura['dati_spedizione'];
                    $dati_spedizione = explode('--', $dati_spedizione);

                    $destinazione = array(
                        'spettle' => $dati_spedizione[0],
                        'indirizzo' => $dati_spedizione[1],
                        'cap' => $dati_spedizione[2],
                        'citta' => $dati_spedizione[3],
                        'prov' => $dati_spedizione[4],
                    );
 
                    // Prodotti

                    $prodottiFattura = Db::getInstance()->executeS("
                        SELECT * 
                        FROM ".$tabella_fatture." 
                        WHERE id_fattura = '".$id_fattura."' 
                            AND id_customer = ".$id_customer." 
                        ORDER BY num_riga
                    ");

                    $rif_ordine = explode(' ', $datiFattura['rif_ordine']);
                    $rif_ordine = $rif_ordine[0];

                    $num_prod = 0;
                    $prodotti = array();
                    
                    foreach($prodottiFattura as $rigaFattura) {
                        $num_prod++;
                        
                        if($num_prod > 9999999) {
                            // niente?
                        }
                        else {
                            if(!empty($rigaFattura['desc_articolo']))
                            {
                                $prodotto = array();

                                $id_prodotto = Db::getInstance()->getValue('
                                    SELECT id_product 
                                    FROM '._DB_PREFIX_.'product 
                                    WHERE reference LIKE "%'.$rigaFattura['cod_articolo'].'%"
                                ');

                                $prodotto['id'] = $id_prodotto; // serve per tooltip
                                $prodotto['cod'] = $rigaFattura['cod_articolo']; // tpl: tooltip
                                $prodotto['desc'] = $rigaFattura['desc_articolo'];
                                $prodotto['um'] = ($rigaFattura['qt_sped'] == 0 ? '' : 'N.');
                                $prodotto['qta'] = ($rigaFattura['qt_sped'] == 0 ? '' : $rigaFattura['qt_sped']);
                                $prodotto['rif_ordine'] = $rif_ordine;
                                $prodotto['prezzo'] = ($rigaFattura['qt_sped'] == 0 ? '' : number_format($rigaFattura['prezzo'], 2, ',', ''));
                                $prodotto['sconti'] = ($rigaFattura['sconto1'] != 0 ? number_format($rigaFattura['sconto1'], 2, ',', '') : '');
                                $prodotto['importo'] = ($rigaFattura['qt_sped'] == 0 ? '' : number_format($rigaFattura['importo_netto'], 2, ',', ''));
                                $prodotto['ci'] = ($rigaFattura['qt_sped'] == 0 ? '' : (int)$rigaFattura['iva_percent']);

                                $prodotti[] = $prodotto;
                            }
                        }
                    }

                    // Totali

                    $tot_merce = Tools::displayPrice($datiFattura['imponibile'], $this->context->currency->id);
                    $c_iva = number_format($datiFattura['iva_percent'], 0, ',', ''); // %
                    $imponibile = $tot_merce;
                    $iva = Tools::displayPrice($datiFattura['iva'], $this->context->currency->id);
                    $tot_fattura = Tools::displayPrice($datiFattura['totale_fattura'], $this->context->currency->id);

                    $totali = array(
                        'tot_merce' => $tot_merce,
                        'c_iva' => $c_iva,
                        'imponibile' => $imponibile,
                        'iva' => $iva,
                        'tot_fattura' => $tot_fattura,
                    );

                    // Trasporto

                    $causale_trasporto = $datiFattura['causale_trasporto'];
                    $trasporto_a_cura = 'Vettore';
                    $vettore = $datiFattura['dati_vettore'];
                    $porto = $datiFattura['desc_porto'];
                    $colli = $datiFattura['num_colli'];
                    $peso = number_format($datiFattura['peso'], 2, ',', '');
                    $data_trasporto = $data_documento;

                    $trasporto = array(
                        'causale_trasporto' => $causale_trasporto,
                        'trasporto_a_cura' => $trasporto_a_cura,
                        'vettore' => $vettore,
                        'porto' => $porto,
                        'colli' => $colli,
                        'peso' => $peso,
                        'data_trasporto' => $data_trasporto,
                    );

                    $dati = array(
                        'tec_ez' => $tec_ez,
                        'info_cliente' => $info_cliente,
                        'dati_fattura' => $dati_fattura,
                        'fatturazione' => $fatturazione,
                        'destinazione' => $destinazione,
                        'prodotti' => $prodotti,
                        'totali' => $totali,
                        'trasporto' => $trasporto,
                    );
                }
            }

            $note_di_credito = array(
                'note_di_credito_rows' => $note_di_credito_rows,
                'dati' => $dati,
                //'storico' => $storico,
            );

        }
        /* FINE Note di credito */

        /* INIZIO Cloud */
        if($tab_name == 'cloud' && !$is_agente){ // tab-container-1=17

            /* EZCLOUD */

            /* 
            // il pulsante crea ezcloud è sempre disattivato
            <!-- Vuoi attivare il primo?<br /><br /><form method="post" action="'.$currentIndex."&id_customer=".$customer->id."&viewcustomer&nuovoezcloud&token=".$this->token.'&tab-container-1=17"><input type="submit" class="button" name="crea_ezcloud" value="S&igrave;, voglio attivare un centralino Ezcloud su questo cliente" /></form> -->
            
            if(Tools::getIsset('crea_ezcloud')){
                //Db::getInstance()->execute('INSERT INTO ezcloud (id_ezcloud, id_customer, active, date_add, date_upd) VALUES (NULL, '.$customer->id.', 1, "'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'")');
                
                Tools::redirectAdmin(self::$currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&conf=4&token='.$this->token.'&tab-container-1=17');
            }*/

            // Per ora inutilizzato perchè mi servono solo i centralini attivi
            $count_ezcloud = Db::getInstance()->getValue('
                SELECT count(id_ezcloud) 
                FROM ezcloud 
                WHERE id_customer = '.$customer->id
            );
            
            $count_ezcloud_attivi = Db::getInstance()->getValue('
                SELECT count(id_ezcloud) 
                FROM ezcloud 
                WHERE id_customer = '.$customer->id.'
                    AND active = 1
            ');
            
            if($count_ezcloud_attivi){
                // Considero solo i centralini attivi
                $ezcloud_centralini = Db::getInstance()->executeS('
                    SELECT * 
                    FROM ezcloud 
                    WHERE id_customer = '.$customer->id.'
                        AND active = 1
                ');
                
                foreach($ezcloud_centralini as &$centralino){
                    $centralino['date_add'] = Tools::displayDate($centralino['date_add']);
                    $centralino['prezzo'] = Tools::displayPrice($centralino['prezzo'], $this->context->currency->id);

                    $valutazione = Db::getInstance()->getRow("
                        SELECT id_ezcloud, id_customer, servizio, funzionalita, supporto
                        FROM ezcloud_valutazione 
                        WHERE id_ezcloud = ".$centralino['id_ezcloud']."
                            AND id_customer = ".$customer->id
                    );

                    $centralino['servizio'] = $valutazione['servizio'];
                    $centralino['funzionalita'] = $valutazione['funzionalita'];
                    $centralino['supporto'] = $valutazione['supporto'];
                    $centralino['pagato'] = 2; // PROVA: CORREGGERE!
                }
            }

            /* YEASTAR CLOUD - DA CORREGGERE! PER ORA UGUALI A EZCLOUD */

            // Per ora inutilizzato perchè mi servono solo i centralini attivi
            $count_yeastar_cloud = Db::getInstance()->getValue('
                SELECT count(id_ezcloud) 
                FROM ezcloud 
                WHERE id_customer = '.$customer->id
            );
            
            $count_yeastar_cloud_attivi = Db::getInstance()->getValue('
                SELECT count(id_ezcloud) 
                FROM ezcloud 
                WHERE id_customer = '.$customer->id.'
                    AND active = 1
            ');
            
            if($count_yeastar_cloud_attivi){
                // Considero solo i centralini attivi
                $yeastar_cloud_centralini = Db::getInstance()->executeS('
                    SELECT * 
                    FROM ezcloud 
                    WHERE id_customer = '.$customer->id.'
                        AND active = 1
                ');
                
                foreach($yeastar_cloud_centralini as &$centralino){
                    $centralino['date_add'] = Tools::displayDate($centralino['date_add']);
                    $centralino['prezzo'] = Tools::displayPrice($centralino['prezzo'], $this->context->currency->id);

                    $centralino['pagato'] = 2; // PROVA: CORREGGERE!
                }
            }

            $ezcloud = array(
                'attivi' => $count_ezcloud_attivi,
                'centralini' => $ezcloud_centralini,
            );

            $yeastar_cloud = array(
                'attivi' => $count_yeastar_cloud_attivi,
                'centralini' => $yeastar_cloud_centralini,
            );

        }
        /* FINE Cloud */

        $dirlist_count = array();
        $filelist_count = array();

		$cartella_documenti = Db::getInstance()->getValue("
            SELECT cartella_documenti 
            FROM "._DB_PREFIX_."customer 
            WHERE id_customer = ".$customer->id
        );

        if(empty($cartella_documenti) || !$cartella_documenti || $cartella_documenti == '') {
            $check_cartella_documenti = false;
            $count_directories = 0;
            $count_files = 0;
            $count_documenti = 0;
        }
        else {
            $check_cartella_documenti = true;

            $directory_base = '../documenti-clienti/'.$cartella_documenti;
            $entire_dir = $directory_base; // passaggio inutile
            
            // ???
            if(substr($entire_dir,0,12) != '../documenti') {
                $this->errors[] = Tools::displayError('ERRORE');
                //die();
            }
                
            if($handle = opendir($entire_dir)) {
                while($dir = readdir($handle)) {
                    if(is_dir($entire_dir.'/'.$dir) && $dir != "." && $dir != "..") { 
                        $dirlist_count[] = $dir; 
                    }
                    if(is_file($entire_dir.'/'.$dir)) { 
                        $filelist_count[] = $dir; 
                    }
                }
            }
            
            // Count cartelle (directories) e file nella directory base
            $count_directories = count($dirlist_count);
            $count_files = count($filelist_count);
            $count_documenti = $count_directories + $count_files;
        }

        // DA TESTARE SUL SERVER GIUSTO! Problemi con i permessi
        /* INIZIO Documenti */
        if($tab_name == 'documents' && !$is_agente){ //tab-container-1=12

            // Correggere: testare tutte le azioni!

            // Crea nuova cartella documenti
            if(Tools::getIsset('createdocsdir'))
            {
                $old_umask = umask(0); // correggere: meglio usare chmod?
                
                if($customer->is_company == 1)
                    $nuova_cartella_documenti = $this->sanitize($customer->company." ".$customer->id);
                else
                    $nuova_cartella_documenti = $this->sanitize($customer->firstname." ".$customer->lastname." ".$customer->id);

                Db::getInstance()->execute("
                    UPDATE "._DB_PREFIX_."customer 
                    SET cartella_documenti = '".$nuova_cartella_documenti."' 
                    WHERE id_customer = ".$customer->id
                );

                // Correggere: non crea la cartella
                mkdir('../documenti-clienti/'.$nuova_cartella_documenti, 0777);
                
                umask($old_umask); // correggere: meglio usare chmod?

                // tpl alert tata: Cartella documenti creata con successo!
            }

            // Elimina documenti selezionati
            if(Tools::isSubmit('delete_documento'))
            {
                foreach($_POST['selectdocfile'] as $selectedfile) {
                    if(Tools::getIsset('current_dir'))
                        $entire_dir = Tools::getValue('current_dir');
                    else
                        $entire_dir = $directory_base;
                    
                    if(is_file($entire_dir.'/'.$selectedfile))
                        unlink($entire_dir.'/'.$selectedfile);
                    else if(is_dir($entire_dir.'/'.$selectedfile))
                        $this->deleteDir($entire_dir.'/'.$selectedfile);
                }
            
                // tpl alert tata: Documenti eliminati con successo
                //Tools::redirectAdmin( ... &gotodir='.Tools::getValue('current_dir').'');
            }

            // Sposta o copia file
            if(Tools::isSubmit('move_documento') || Tools::isSubmit('copy_documento'))
            {
                foreach($_POST['selectdocfile'] as $selectedfile){
                    if(Tools::getIsset('current_dir'))
                        $entire_dir = Tools::getValue('current_dir');
                    else
                        $entire_dir = $directory_base;
                    
                    if(Tools::getIsset('move_documento'))
                        rename($entire_dir.'/'.$selectedfile, Tools::getValue('sposta_in')."/".$selectedfile);
                    else if(Tools::getIsset('copy_documento'))
                        copy($entire_dir.'/'.$selectedfile, Tools::getValue('sposta_in')."/".$selectedfile);
                }
            
                // tpl alert tata: Documenti eliminati con successo
                // Tools::redirectAdmin(... &gotodir='.Tools::getValue('current_dir').'');
            }

            // Carica un nuovo file
            if(Tools::isSubmit('carica_file'))
            {
                if(!empty($_FILES['aggiungiFile']))
                {
                    $docfiles = array();
                    $docfdata = $_FILES['aggiungiFile'];

                    if(is_array($docfdata['name'])){
                        $count_docfname = count($docfdata['name']);
                        for($i=0; $i<$count_docfname; ++$i){
                            $docfiles[] = array(
                                'name'    => $docfdata['name'][$i],
                                'tmp_name'=> $docfdata['tmp_name'][$i],
                                'type' => $docfdata['type'][$i],
                                'size' => $docfdata['size'][$i],
                                'error' => $docfdata['error'][$i],
                            );
                        }
                    }
                    else 
                        $docfiles[] = $docfdata;
                                        
                    $attachments = array();
                    
                    foreach($docfiles as $docfile){
                        if (isset($docfiles) AND !empty($docfile['name']) AND $docfile['error'] != 0)
                            $this->_errors[] = Tools::displayError('An error occurred with the file upload.');

                        if (!empty($docfile['name']))
                            rename($docfile['tmp_name'], Tools::getValue('cartella').'/'.$docfile['name']); 
                    }			
                    // tpl alert tata: File aggiunti con successo
                }
            }
            

            if($cartella_documenti && $cartella_documenti != "") {
				
                $dirlist = array();
                $filelist = array();
                $l_sup = false; // false: non mi trovo in una sottocartella e non posso risalire al livello superiore
                              
                if(Tools::getIsset('gotodir')) {
                    $entire_dir = Tools::getValue('gotodir');
                }
                else {
                    $entire_dir = $directory_base;
                }

                // non può succedere se passo in post invece che get, o se passo solo il nome della sottocartella in cui mi trovo
                if(substr($entire_dir,0,12) != '../documenti') {
                    $this->errors[] = Tools::displayError('ERRORE');
                    // die();
                }

                if ($handle = opendir($entire_dir)) {

                    while ($dir = readdir($handle)) {                   
                        if(is_dir($entire_dir.'/'.$dir) && $dir != "." && $dir != "..") { 
                            $dirlist[] = $dir; 
                        }
                        if(is_file($entire_dir.'/'.$dir)) { 
                            $filelist[] = $dir; 
                        }
                    }
                    
                    // Crea nuova sottocartella
                    if(Tools::isSubmit('crea_sottocartella'))
                    {
                        foreach($dirlist as $entry){
                            if($entry == Tools::getValue('nome_cartella')){
                                $this->errors[] = Tools::displayError('Esiste già una cartella con questo nome.');
                                break;
                            }
                        }
                        
                        $old_umask = umask(0);
                        mkdir(Tools::getValue('cartella')."/".Tools::getValue('nome_cartella'),0777);
                        umask($old_umask);
                        // CONTROLLARE URL
                        //Tools::redirectAdmin(self::$currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&tab-name=documents&token='.$this->token.'&gotodir='.Tools::getValue('cartella').'');
                    }

                    // Count cartelle e file nella sottocartella in cui mi trovo
                    $count_sub_cartelle = count($dirlist);
					$count_sub_files = count($filelist);
					$count_docs = $count_sub_cartelle + $count_sub_files;
                        
                    if(Tools::getIsset('gotodir') && Tools::getValue('gotodir') != $directory_base) {
                        $l_sup = true;
                        $l_sup_href = '&gotodir='.(Tools::getIsset('gotodir') ? dirname(Tools::getValue('gotodir')) : $directory_base);
                    }
                    else
                        $l_sup = false;

                    // Lista cartelle
                    foreach($dirlist as $entry) {
                        $pathinfo = pathinfo($entire_dir.'/'.$entry);
                        
                        if($entry == '.' || $entry == '..') { 
                            // niente?
                        }
                        else {
                            $gotodir = '&gotodir='.(Tools::getIsset("gotodir") ? Tools::getValue("gotodir")."/".$entry : $directory_base."/".$entry);
                            $formato = '&laquo;CARTELLA&raquo;';
                            $ultima_modifica = date("d/m/Y H:i:s", filemtime($entire_dir.'/'.$entry));

                            $cartelle[] = array(
                                'gotodir' => $gotodir, // link sottocartelle
                                'nome' => $entry,
                                'dimensioni' => 0,
                                'formato' => $formato,
                                'ultima_modifica' => $ultima_modifica,
                            );
                        }						
                    }
                    
                    // Lista file
                    foreach($filelist as $entry) {
                        if ($entry != "." && $entry != "..") {
                            // PROVA: cambiato con un percorso di prova!!! Quello giusto è quello commentato
                            chmod('/var/www/html/documenti-clienti/'.$entire_dir.'/'.$entry, 0644);
                            // Setto i permessi del file: lettura e scrittura per il proprietario, solo lettura per gli altri
                            //-*+chmod('/var/www/vhosts/ezdirect.it/httpdocs/documenti-clienti/'.$entire_dir.'/'.$entry, 0644);
                            
                            $pathinfo = pathinfo('../documenti-clienti/'.$cartella_documenti.'/'.$entry);
                            
                            $ext = substr($pathinfo['extension'],0);
                            switch ($ext) {
                                case 'txt':
                                    $fileicon = 'icon-file-text-alt';
                                    break;
                                
                                default:
                                    $fileicon = 'icon-file-alt';
                                    break;
                            }
                            
                            $dimensioni = $this->FileSizeConvert(filesize($entire_dir.'/'.$entry)); // funzione in fondo a questo file; spostare in override
                            $formato = $pathinfo['extension'];
                            $ultima_modifica = date("d/m/Y H:i:s", filemtime($entire_dir.'/'.$entry));
                            // Correggere: decommentare finiti i test!
                            //$href = 'https://ezdocs:WRY753MNR43ASD147g!@www.ezdirect.it/documenti-clienti/'.$entire_dir.'/'.$entry;

                            $files[] = array(
                                'href' => $href,
                                'icona' => $fileicon,
                                'nome' => $entry,
                                'dimensioni' => $dimensioni,
                                'formato' => $formato,
                                'ultima_modifica' => $ultima_modifica,
                            );
                        }
                    }

                    $i = 0;
                    $select_dir = array(); // Contiene le option della select per le azioni Sposta e Copia
                    $directories = $this->ListIn($directory_base); // funzione in fondo a questo file; spostare in override

                    // Operazioni sui documenti nella cartella selezionata
                    if($count_docs != 0) {
                    
                        // ELIMINA SELEZIONE (name = 'delete_documento') con conferma "Sei sicuro? I file, le cartelle e il contenuto delle cartelle che hai selezionato, saranno cancellati in modo permanente!!!"
                        
                        // SPOSTA (move_documento) / COPIA (copy_documento): <select name='sposta_in'>

                        // Se non mi trovo nella cartella base del cliente
                        if(Tools::getIsset('gotodir')) {
                            $select_dir[$i]['valore'] = $directory_base;
                            $select_dir[$i]['nome'] = 'Cartella base del cliente';
                            $i++;
                        }

                        foreach($directories as $dir) {
                            if($directory_base."/".$dir == Tools::getValue('gotodir')) {
                                // Non posso selezionare la cartella a cui appartiene già il file
                            }
                            else {
                                $select_dir[$i]['valore'] = $directory_base."/".$dir;
                                $select_dir[$i]['nome'] = $dir;
                                $i++;
                            }
                        }

                        $i = 0;
                    }

                    closedir($handle);
                }
            }

            $documenti = array(
                'l_sup' => $l_sup,
                'l_sup_href' => $l_sup_href,
                'entire_dir' => $entire_dir,
                'directory_base' => $directory_base,
                'cartelle' => $cartelle,
                'files' => $files,
                'directories' => $directories, // forse non serve
                'count_sub_cartelle' => $count_sub_cartelle,
                'count_sub_files' => $count_sub_files,
                'count_docs' => $count_docs,
                'select_dir' => $select_dir,
            );

        } 
        /* FINE Documenti */

        // Per le tab tickets, actions, todo - pulsante "Collega a"
        if(Tools::getIsset('submitCollegaAttivita'))
        {
            $id_thread = substr(Tools::getValue('coll_partenza'), 1);
            switch(substr(Tools::getValue('coll_partenza'), 0, 1))
            {
                case 'T': Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'customer_thread SET riferimento = "'.Tools::getValue('coll_destinazione').'" WHERE id_customer_thread = '.$id_thread); break;
                case 'P': Db::getInstance()->execute('UPDATE form_prevendita_thread SET riferimento = "'.Tools::getValue('coll_destinazione').'" WHERE id_thread = '.$id_thread); break;
                case 'A': Db::getInstance()->execute('UPDATE action_thread SET riferimento = "'.Tools::getValue('coll_destinazione').'" WHERE id_action = '.$id_thread); break;
                case 'L': Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'bdl SET riferimento = "'.Tools::getValue('coll_destinazione').'" WHERE id_bdl = '.$id_thread); break;
                
                default: ''; break;
            }
            
            Customer::Storico($id_thread, substr(Tools::getValue('coll_partenza'), 0, 1), $mio_id, 'Ha collegato ad attivit&agrave; n. '.Customer::trovaSigla(substr(Tools::getValue('coll_destinazione'), 1), substr(Tools::getValue('coll_destinazione'), 0, 1)));
        }

        $employeemess = new Employee($mio_id);
		$impiegati_eza = Db::getInstance()->executeS("SELECT id_employee, firstname, lastname FROM "._DB_PREFIX_."employee WHERE active = 1 ORDER BY firstname ASC, lastname ASC");
        
        // CONTROLLARE: i ticket sono uguali ai messaggi? Allora perchè fare 2 variabili diverse?
        // Controllare se $messages contiene gli stessi record (tranne la condizione id_contact != 7, ma se serve sempre si può aggiungere alla funzione nella classe ed usare $messages per tutto) 
        $count_ticket = Db::getInstance()->getValue("SELECT COUNT(id_customer_thread) FROM "._DB_PREFIX_."customer_thread WHERE id_contact != 7 AND id_customer = ".$customer->id);
        $count_ticket_aperti = Db::getInstance()->getValue("SELECT COUNT(id_customer_thread) FROM "._DB_PREFIX_."customer_thread WHERE id_contact != 7 AND id_customer = ".$customer->id." AND status != 'closed'");
        
        /* INIZIO Ticket */
        if($tab_name == 'tickets' && !$not_mio_agente){ //tab-container-1=6

            // correggere: assegnare una var a Tools::getValue('id_customer_thread') se isset e usare quella

            // Correggere: sono corretti? non posso prenderli anzi dalla tabella ps_employee?
            switch($mio_id) {
                case 1: 
                    $interno = "(201)";
                    break;
                case 2: 
                    $interno = "(202)";
                    break;
                case 3: 
                    $interno = "(204)";
                    break;
                case 4: 
                    $interno = "(205)";
                    break;
                case 5: 
                    $interno = "(203)";
                    break;
                case 6: 
                    $interno = "";
                    break;
                case 7: 
                    $interno = "(207)";
                    break;

                default: 
                    $interno = "";
                    break;
            }

            $gentilecliente = "Gentile cliente, <br />";
		
		    $firma = '
                <br />
                Cordiali saluti / Best regards<br /><br />
                '.($mio_id != 19 ? $employeemess->firstname." ".$employeemess->lastname : 'Staff Ezdirect').'
                <br /><br />
                Ezdirect srl <a href="http://www.ezdirect.it">www.ezdirect.it</a>
                <br /><br />
                Tel +39 0585821163 '.$interno.'<br />
                Fax +39 0585821286
                </p>
            ';

            if(isset($_POST['cambiastatust_listing'])) {
                
                Db::getInstance()->execute("
                    UPDATE "._DB_PREFIX_."customer_thread 
                    SET status = '".Tools::getValue('cambiastatust_listing')."', date_upd = '".date("Y-m-d H:i:s")."' 
                    WHERE id_customer_thread = '".Tools::getValue('id_customer_thread')."'
                ");
                
                switch(Tools::getValue('cambiastatust_listing'))
                {
                    case 'open': $switch_status = 3; break;
                    case 'pending1': $switch_status = 4; break;
                    case 'closed': $switch_status = 5; break;
                    default: $switch_status = 10; break;
                }	
                
                Customer::Storico(Tools::getValue('id_customer_thread'), 'T', $mio_id, $switch_status);
                Tools::redirectAdmin(self::$currentIndex.'&'.$this->identifier.'='.$customer->id.'&viewcustomer&tab_name=tickets&conf=4&token='.$this->token);
            }
            
            if(isset($_POST['cambiaincaricot'])) {
                Customer::cambiaIncaricoVeloce(Tools::getValue('tipo'), Tools::getValue('cambiaincaricot'), Tools::getValue('id_thread'), Tools::getValue('precedente_incarico'), Tools::getValue('id_ticket_univoco'), Tools::getValue('id_customer'));
                Tools::redirectAdmin(self::$currentIndex.'&'.$this->identifier.'='.$customer->id.'&viewcustomer&tab_name=tickets&conf=4&token='.$this->token);
            }

            // Salvataggio panel Generale - correggere: testare RMA!
            if(isset($_POST['submitTicket'])) {
		
				$precedente_status = Db::getInstance()->getValue("SELECT status FROM "._DB_PREFIX_."customer_thread WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
				$impiegato_attuale = Db::getInstance()->getValue("SELECT id_employee FROM "._DB_PREFIX_."customer_thread WHERE id_customer_thread = '".Tools::getValue('id_customer_thread')."'");
				
                switch(Tools::getValue('cambiastatus'))
				{
					case 'open': $switch_status = 3; break;
					case 'pending1': $switch_status = 4; break;
					case 'closed': $switch_status = 5; break;
					default: $switch_status = 10; break;
				}

                // spostare dopo update?
                if($precedente_status != Tools::getValue('cambiastatus'))
					Customer::Storico(Tools::getValue('id_customer_thread'), 'T', $mio_id, $switch_status);
                
                $descrizione_tkt = Tools::getValue('descrizione_tkt');
                if($descrizione_tkt == 'Altro')
                    $descrizione_tkt .= Tools::getValue('descrizione_altro_tkt');

                $guasto_tkt = Tools::getValue('guasto_tkt');
                if($guasto_tkt == 'Altro')
                    $guasto_tkt .= Tools::getValue('guasto_altro_tkt');
                    
                $causa_guasto_tkt = Tools::getValue('causa_guasto_tkt');
                if($causa_guasto_tkt == 'Altro')
                    $causa_guasto_tkt .= Tools::getValue('causa_guasto_altro_tkt');

                // Update subject, note, status, date_upd, descrizione, motivo_chiamata, guasto, causa_guasto, priorita
				Db::getInstance()->execute("UPDATE "._DB_PREFIX_."customer_thread SET subject = '".addslashes(Tools::getValue('ticket_subject'))."', descrizione = '".addslashes($descrizione_tkt)."', motivo_chiamata = '".Tools::getValue('motivo_chiamata_tkt')."', guasto = '".addslashes($guasto_tkt)."', causa_guasto = '".addslashes($causa_guasto_tkt)."', note = '".addslashes(Tools::getValue('ticket_note'))."', status = '".Tools::getValue('cambiastatus')."', priorita = '".Tools::getValue('cambiapriorita')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = '".Tools::getValue('id_customer_thread')."'");
				
                // Update riferimenti prodotto e ordine
				Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'customer_thread SET id_product = "'.Tools::getValue('id_product').'", id_order = "'.Tools::getValue('id_order').'" WHERE id_customer_thread = '.Tools::getValue('id_customer_thread'));
				
                /* NB: L'update delle note private viene fatto in ajax */
				
                // RMA
				if(Tools::getValue('id_contact') == 9)
                {
					Customer::Storico(Tools::getValue('id_customer_thread'), 'T', $mio_id, 'Ha aggiornato le informazioni dell\'RMA');
					
					$chars_per_codice = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
					for ($i = 0, $codice_rma = ''; $i < 5; $i++) {
						$codice_rma .= Tools::substr($chars_per_codice, mt_rand(0, Tools::strlen($chars_per_codice) - 1), 1);
					}
					
					$rma_id = Db::getInstance()->getValue("SELECT codice_rma FROM rma WHERE id_customer_thread = '".Tools::getValue('id_customer_thread')."'");
					if($rma_id == '')
						Db::getInstance()->execute("INSERT INTO rma (id_customer_thread, codice_rma) VALUES (".Tools::getValue('id_customer_thread').", '".$codice_rma."')");

					Db::getInstance()->execute("
                        UPDATE 
                            rma 
                        SET 
                            id_address = '".addslashes(Tools::getValue('indirizzo_rma'))."', 
                            rma_type = '".addslashes(Tools::getValue('rma_type'))."', 
                            rma_shipping = '".addslashes(Tools::getValue('rma_shipping'))."', 
                            arrivoezcli = '".(Tools::getIsset('arrivoezcli') ? 1 : 0)."',
                            arrivocli =  '".date('Y-m-d H:i:s', strtotime(Tools::getValue('arrivocli')))."',
                            a_carico = '".Tools::getValue('a_carico')."',
                            corriere = '".Tools::getValue('corriere')."',
                            richiesto_rma = '".(Tools::getIsset('richiesto_rma') ? 1 : 0)."',
                            data_richiesta = '".date('Y-m-d H:i:s', strtotime(Tools::getValue('data_richiesta')))."',
                            codice_rma_fornitore = '".Tools::getValue('codice_rma_fornitore')."',
                            il_fornitore = '".Tools::getValue('il_fornitore')."',
                            arrivoezfornitore = '".date('Y-m-d H:i:s', strtotime(Tools::getValue('arrivoezfornitore')))."',
                            sostituitoriparato = '".Tools::getValue('sostituitoriparato')."',
                            
                            test = '".Tools::getValue('test')."',
                            test_in_carico_a = '".Tools::getValue('test_in_carico_a')."',
                            problematica_test = '".Tools::getValue('problematica_test')."',
                            urgente = '".(Tools::getIsset('urgente') ? 1 : 0)."',
                            
                            qta = '".addslashes(Tools::getValue('qta'))."', 
                            seriale = '".addslashes(Tools::getValue('seriale'))."', 
                            in_garanzia = '".(Tools::getIsset('in_garanzia') ? 1 : 0)."', 
                            attivita_svolta = '".addslashes(Tools::getValue('attivita_svolta'))."', 
                            altra_attivita = '".addslashes(Tools::getValue('altra_attivita'))."', 
                            costo_materiale = '".str_replace(",", ".", addslashes(Tools::getValue('costo_materiale')))."', 
                            costo_manodopera = '".str_replace(",", ".", addslashes(Tools::getValue('costo_manodopera')))."', 
                            trasporti_qta = '".addslashes(Tools::getValue('trasporti_qta'))."', 
                            manodopera_qta = '".str_replace(",", ".", addslashes(Tools::getValue('manodopera_qta')))."', 
                            totale_trasporti = '".str_replace(",", ".", addslashes(Tools::getValue('totale_trasporti')))."' 
                        WHERE 
                            id_customer_thread = '".Tools::getValue('id_customer_thread')."'
                    ");

					$test_now = Db::getInstance()->getValue('SELECT test FROM rma WHERE id_customer_thread = '.Tools::getValue('id_customer_thread'));
					
					if(Tools::getValue('test') == 1 && $test_now == 0)
					{
						$prodrichiesta = Db::getInstance()->getRow("SELECT reference, name FROM "._DB_PREFIX_."product JOIN "._DB_PREFIX_."product_lang ON product.id_product = product_lang.id_product WHERE id_lang = ".$this->context->language->id." and product.id_product = ".Tools::getValue('id_product').""); 
						
						$action_threadz = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
						$action_threadz++;
						$action_subjectz = "*** RMA N. ".Customer::trovaSigla(Tools::getValue('id_customer_thread'),'ticket')." - TESTARE PRODOTTO *** ";
						$id_employee = Tools::getValue('test_in_carico_a');
						
						$idcust = Db::getInstance()->getValue('SELECT id_customer FROM '._DB_PREFIX_.'customer_thread WHERE id_customer_thread = '.Tools::getValue('id_customer_thread'));
						
						Db::getInstance()-execute("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd, promemoria) VALUES ('$action_threadz', 'Attivita', '".$idcust."', '".addslashes($action_subjectz)."', 0, ".$id_employee.", 'open', 'T".Tools::getValue('id_customer_thread')."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."',1)");   
						Db::getInstance()-execute("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_threadz', ".$idcust.", 99, ".$id_employee.", ".$id_employee.", '', 'RMA N. ".Customer::trovaSigla(Tools::getValue('id_customer_thread'),'ticket')." - TESTARE PRODOTTO ".$prodrichiesta['reference']." (".$prodrichiesta['name']."). Problematica: ".addslashes(Tools::getValue('problematica_test'))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
					}
				}
				
				if($impiegato_attuale != Tools::getValue('assegnaimpiegato'))
                {
					Customer::Storico(Tools::getValue('id_customer_thread'), 'T', $mio_id, 2, Tools::getValue('assegnaimpiegato'));
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."customer_thread SET id_employee = '".Tools::getValue('assegnaimpiegato')."' WHERE id_customer_thread = '".Tools::getValue('id_customer_thread')."'");
					
                    // TEST! CORREGGERE; DECOMMENTARE MAIL GIUSTA
                    $mailimp = 'carolina.carusi@ezdirect.it';
					//$mailimp = Db::getInstance()->getValue("SELECT email FROM "._DB_PREFIX_."employee WHERE id_employee = '".Tools::getValue('assegnaimpiegato')."'");
					$tokenimp = Tools::getAdminToken("AdminCustomers".(int)Tab::getIdFromClassName("AdminCustomers").Tools::getValue('assegnaimpiegato'));
                    $linkimp = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&tab_name=tickets&azione=tickets&viewticket&id_customer_thread=".Tools::getValue('id_customer_thread')."&token=".$tokenimp;
							
					$msgimp = $employeemess->firstname." ".$employeemess->lastname." ti ha assegnato il ticket numero ".Tools::getValue('id_ticket_univoco')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect. <a href='".$linkimp."'>Fai clic qui per aprirlo</a>.";
					
					// Invio una mail di revoca della gestione a $impiegato_attuale, se non è l'employee loggato
                    if($impiegato_attuale != 0 && $impiegato_attuale != $mio_id)
                    {
                        $impiegato_assegnato = (Tools::getValue('assegnaimpiegato') != 0 ? Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.Tools::getValue('assegnaimpiegatom')) : 'Nessuno');
                        // TEST! CORREGGERE; DECOMMENTARE MAIL GIUSTA
                        $mailimprev = 'carolina.carusi@ezdirect.it';
						//$mailimprev = Db::getInstance()->getValue("SELECT email FROM "._DB_PREFIX_."employee WHERE id_employee = '".$impiegato_attuale."'");
						$nomeimprev = Db::getInstance()->getValue("SELECT firstname FROM "._DB_PREFIX_."employee WHERE id_employee = '".$impiegato_attuale."'");
						$msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione del ticket numero ".Tools::getValue('id_ticket_univoco')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect e l'ha passata a ".$impiegato_assegnato;
								
						$params = array(
                            '{msg}' => $msgimprev
                        );
						
                        Mail::Send($this->context->language->id, 'msg_base', Mail::l('Gestione ticket revocata', $this->context->language->id), 
                                $params, $mailimprev, NULL, NULL, NULL, NULL, NULL, _PS_MAIL_DIR_, true);
					}

					$params = array(
                        '{msg}' => $msgimp
                    );
							
					if($_POST['assegnaimpiegato'] != 0 && $_POST['assegnaimpiegato'] != $mio_id) {
						Mail::Send($this->context->language->id, 'msg_base', Mail::l('Ticket assegnato a te su Ezdirect', $this->context->language->id), 
							    $params, $mailimp, NULL, NULL, NULL, NULL, NULL, _PS_MAIL_DIR_, true);
					}
				}
				
                // Correggere: questo andrebbe fatto prima dell'invio delle mail, altrimenti il link potrebbe essere sbagliato (va modificato se è stato fatto convertiTicket)
				$id_contact = Db::getInstance()->getValue('SELECT id_contact FROM '._DB_PREFIX_.'customer_thread WHERE id_customer_thread = '.Tools::getValue('id_customer_thread'));
                
                if($id_contact != Tools::getValue('id_contact')){
					Customer::convertiTicket(Tools::getValue('id_customer_thread'), $id_contact, Tools::getValue('id_contact'));
                    Tools::redirectAdmin(self::$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&tab_name=tickets&conf=4&token='.$this->token);
                }
                else
				    Tools::redirectAdmin(self::$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&tab_name=tickets&azione=tickets&viewticket&id_customer_thread='.Tools::getValue('id_customer_thread').'&conf=4&token='.$this->token);
			
            }

            // Apertura nuovo ticket e invio risposta (panel Cronologia) - Divisi i ticket dai messaggi (nella 1.4 erano insieme)
            if(Tools::isSubmit('submitMessageTicket')) {

                // messaggio vuoto => errore
                if(Tools::getValue('message') == '')
                {
                    $precedente_incarico = Db::getInstance()->getValue("SELECT id_employee FROM "._DB_PREFIX_."customer_thread WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
                    $precedente_status = Db::getInstance()->getValue("SELECT status FROM "._DB_PREFIX_."customer_thread WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");

                    // Update in carico a
                    Customer::cambiaIncaricoVeloce('ticket', Tools::getValue('in_carico_a'), Tools::getValue('id_customer_thread'), $precedente_incarico, Customer::trovaSigla(Tools::getValue('id_customer_thread'),'ticket'), Tools::getValue('id_customer'));
                    
                    if($precedente_status != Tools::getValue('status'))
                    {
                        switch(Tools::getValue('status'))
                        {
                            case 'open': $switch_status = 3; break;
                            case 'pending1': $switch_status = 4; break;
                            case 'closed': $switch_status = 5; break;
                            default: $switch_status = 10; break;
                        }	
                        
                        Customer::Storico(Tools::getValue('id_customer_thread'), 'T', $mio_id, $switch_status);

                        // Update priorita, status, date_upd
                        Db::getInstance()->execute("UPDATE "._DB_PREFIX_."customer_thread SET priorita = '".Tools::getValue('priorita')."', status = '".Tools::getValue('status')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
                    }

                    Tools::redirectAdmin(self::$currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&tab_name=tickets&error=1tk&token='.$this->token);
                }

                // risposta
                if(Tools::getValue('isAnswer') == 1) 
                {
                    $primaqueryct = ("SELECT id_customer_thread, token FROM "._DB_PREFIX_."customer_thread WHERE id_customer = $_POST[id_customer] AND id_customer_thread = $_POST[id_customer_thread]");
                    $primarowct = Db::getInstance()->getRow($primaqueryct);

                    $id_customer_thread = $primarowct['id_customer_thread'];
                    $newtoken = $primarowct['token'];
                    
                    $subj = Mail::l('An answer to your message is available', (int)$ct->id_lang);

				    $precedente_incarico = Db::getInstance()->getValue("SELECT id_employee FROM "._DB_PREFIX_."customer_thread WHERE id_customer_thread = ".Tools::getValue('id_customer_thread'));
                    Customer::cambiaIncaricoVeloce('ticket', Tools::getValue('in_carico_a'), Tools::getValue('id_customer_thread'), $precedente_incarico, Customer::trovaSigla(Tools::getValue('id_customer_thread'),'ticket'), Tools::getValue('id_customer'));
                    
                    $queryinserimentomsg = ("UPDATE "._DB_PREFIX_."customer_thread SET id_employee = '".Tools::getValue('in_carico_a')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = ".$id_customer_thread);
                    Db::getInstance()->execute($queryinserimentomsg);
                }
                // nuovo ticket
                else
                {
                    $queryct = ("SELECT id_customer_thread FROM "._DB_PREFIX_."customer_thread ORDER BY id_customer_thread DESC");
                    $rowct = Db::getInstance()->getRow($queryct);

                    // Correggere: se era stato eliminato un ticket con questo id, cancellarne ogni traccia anche dallo storico
                    $id_customer_thread = $rowct['id_customer_thread']+1;
                    $newtoken = Tools::passwdGen(12);

                    $subj = "Messaggio da parte di Ezdirect";
                                        
                    switch(Tools::getValue('id_contact')) {
                        case 2: $titolomessaggio = "Ticket per amministrazione - contabilita"; break;
                        case 3: $titolomessaggio = "Ticket rivenditori"; break;
                        case 4: $titolomessaggio = "Ticket per assistenza"; break;
                        case 5: $titolomessaggio = "Ticket servizio clienti e ufficio vendite"; break;
                        case 6: $titolomessaggio = "Ticket ti richiamiamo noi"; break;
                        case 7: $titolomessaggio = "Messaggio semplice"; break;
                        case 8: $titolomessaggio = "Ticket per amministrazione - ordini"; break;
                        default: $titolomessaggio = "Messaggio da modulo di contatto"; break;
                    }

                    $cstmr_r = ($customer->is_company ? $customer->company : $customer->firstname." ".$customer->lastname);
                    
                    $subject = "*** RICHIESTA TICKET da ".$cstmr_r." - Tipo: ".$titolomessaggio." ***";

                    // RMA
                    if(Tools::getValue('id_contact') == 9)
                    {
                        $chars_per_codice = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        for ($i = 0, $codice_rma = ''; $i < 5; $i++) {
                            $codice_rma .= Tools::substr($chars_per_codice, mt_rand(0, Tools::strlen($chars_per_codice) - 1), 1);
                        }

                        // Commentato 1.4
                        /*
                        $action_thread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
                        $action_thread++;
                        $action_subject = "*** RMA SU ".($customer->is_company ? $customer->company : $customer->firstname." ".$customer->lastname)." - Tipo richiesta: RMA (Aperto da ".$mio_firstname.") *** ";                        
                        Db::getInstance()->execute("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd) VALUES ('$action_thread', 'Richiesta_RMA', '".$customer->id."', '".addslashes($action_subject)."', 0, 4, 'open', 'T".$id_customer_thread."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");
                        $siglarma = 'S'.substr(date('Y'), 2,2).$id_customer_thread;
                        Db::getInstance()->execute("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, date_add, date_upd) VALUES (NULL, '$action_thread', ".$customer->id.", 0, 4, 4, '', '<strong>*** RMA***</strong><br /><br /><strong>Codice RMA</strong>: ".$codice_rma."<br /><strong>Codice Ticket</strong>: ".$siglarma."<br /><strong>Template RMA</strong>: <a href=\"http://www.ezdirect.it/rma/".$codice_rma.".html\" target=\"_blank\">http://www.ezdirect.it/rma/".$codice_rma.".html</a><br /><strong>Messaggio del cliente</strong>: ".addslashes(Tools::getValue('message'))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");
                        */

                        $id_address_rma = Db::getInstance()->getValue('SELECT id_address FROM '._DB_PREFIX_.'address WHERE id_customer = '.$customer->id.' AND active = 1 AND deleted = 0 AND fatturazione = 1');
                        
                        Db::getInstance()->execute("
                            INSERT INTO rma (id_customer_thread, codice_rma, rma_product, seriale, qta, rma_shipping, rma_type, id_address) 
                            VALUES (".$id_customer_thread.", '".$codice_rma."', '', '', '', '', '', ".$id_address_rma.")
                        ");
                    }

                    // inserimento in ps_customer_thread
                    $queryinserimentomsg = ("INSERT INTO "._DB_PREFIX_."customer_thread (id_customer_thread, id_lang, id_contact, id_customer, id_employee, subject, id_order, id_product, status, email, riferimento, token, date_add, date_upd, note) VALUES ('".$id_customer_thread."', '".$this->context->language->id."',  '".Tools::getValue('id_contact')."', '".Tools::getValue('id_customer')."', '".Tools::getValue('in_carico_a')."', '".addslashes($subject)."', '$_POST[id_order]', '$_POST[id_product]', 'open', '$_POST[customer_email_msg]', '".Tools::getValue('riferimento')."', '".$newtoken."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."', '".addslashes(Tools::getValue('note'))."')");
                    Db::getInstance()->execute($queryinserimentomsg);
                }

                $ct = new CustomerThread($id_customer_thread);
                $cm = new CustomerMessage();
                
                if(Tools::getIsset('chiudi_padre'))
                    Customer::chiudiPadre(Db::getInstance()->getValue('SELECT riferimento FROM '._DB_PREFIX_.'customer_thread WHERE id_customer_thread = '.$id_customer_thread));
                
                if(Tools::getIsset('riferimento'))
                    Db::getInstance()->execute("UPDATE "._DB_PREFIX_."customer_thread SET riferimento = '".Tools::getValue('riferimento')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = ".$id_customer_thread."");

                if(Tools::getIsset('riferimentoo') && Tools::getValue('riferimentoo') != '')
                {
                    if(substr(Tools::getValue('riferimentoo'),0,1) == 'C') {
                        $note_da_copiare = Db::getInstance()->executeS('SELECT * FROM cart_note WHERE id_cart = "'.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo'))).'"');
                    }
                    else if(substr(Tools::getValue('riferimentoo'),0,1) == 'O') {
                        $note_da_copiare = Db::getInstance()->executeS('SELECT * FROM cart_note WHERE id_cart = "'.Db::getInstance()->getValue('SELECT id_cart FROM orders WHERE id_order = '.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo')))).'"');
                    }
                    else { 
                        $note_da_copiare = Db::getInstance()->executeS('SELECT * FROM note_attivita WHERE id_attivita = "'.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo'))).'" AND tipo_attivita = "'.substr(Tools::getValue('riferimentoo'),0,1).'"');
                    }

                    foreach($note_da_copiare as $ndc) {
                        Db::getInstance()->execute('INSERT INTO note_attivita (id_note, id_attivita, tipo_attivita, id_employee, note, date_add, date_upd) VALUES (NULL, '.$id_customer_thread.', "T", "'.$ndc['id_employee'].'","'.$ndc['note'].'", "'.$ndc['date_add'].'","'.$ndc['date_upd'].'")');
                    }
                }

                $getstatus = Tools::getValue('status');

                if(!empty($getstatus)) 
                {
                    $precedente_status = Db::getInstance()->getValue("SELECT status FROM "._DB_PREFIX_."customer_thread WHERE id_customer_thread = ".$id_customer_thread."");
                    if($precedente_status != Tools::getValue('status'))
                    {	
                        switch(Tools::getValue('status'))
                        {
                            case 'open': $switch_status = 3; break;
                            case 'pending1': $switch_status = 4; break;
                            case 'closed': $switch_status = 5; break;
                            default: $switch_status = 10; break;
                        }	
                        
                        Customer::Storico($id_customer_thread, 'T', $mio_id, $switch_status);
                    }
                
                    // Update priorita, status, date_upd
                    Db::getInstance()->execute("UPDATE "._DB_PREFIX_."customer_thread SET priorita = '".Tools::getValue('priorita')."', status = '".Tools::getValue('status')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = ".$id_customer_thread."");
                }
                
                if(Tools::getValue('apri-todo') != '')
                {
                    $action_threadz = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
                    $action_threadz++;
                    $action_subjectz = "*** PROMEMORIA TICKET ".Customer::trovaSigla($id_customer_thread,'ticket')." *** ";
                    $id_employee = Tools::getValue('apri-todo');
                    
                    Db::getInstance()->execute("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, status, riferimento, action_date, date_add, date_upd, promemoria) VALUES ('$action_threadz', 'Attivita', '".$customer->id."', '".addslashes($action_subjectz)."', 0, ".$id_employee.", 'open', 'T".$id_customer_thread."', '".date("Y-m-d H:i:s", strtotime('+1 hours'))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."',1)");   
                    Db::getInstance()->execute("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, file_name, action_message, telefono_cliente, date_add, date_upd) VALUES (NULL, '$action_threadz', ".$customer->id.", 99, ".$id_employee.", ".$id_employee.", '', '<strong>*** PROMEMORIA TICKET  ".Customer::trovaSigla($id_customer_thread,'ticket')." ***</strong><br /><br />".$mio_firstname." ti ha aperto questo todo per ricordarti di gestire il ticket ".Customer::trovaSigla($id_customer_thread,'ticket').". <a href=\"https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".$customer->id."&viewcustomer&tab_name=tickets&azione=tickets&viewticket&id_customer_thread=".$id_customer_thread."&token=".Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').$id_employee)."\" target=\"_blank\">clicca qui per aprirlo</a>', '".Tools::getValue('telefono_cliente')."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
                    Db::getInstance()->execute('INSERT INTO ticket_promemoria (msg, tipo_att, ora, conto, tipo, status) VALUES ('.$action_threadz.', "a", "'.date("Y-m-d H:i:s", strtotime('+1 hours')).'" - INTERVAL 30 MINUTE, "30", "MINUTE", "open")');
                }

                $cm->id_employee = (int)$mio_id;
                $cm->id_customer_thread = $ct->id;
				$cm->message = Tools::htmlentitiesutf8(Tools::getValue('message'));
			    $cm->ip_address = ip2long($_SERVER['REMOTE_ADDR']);

                // allegati
                $files=$_FILES["joinFile"]; // eliminare?

                $files=array();
                $fdata=$_FILES['joinFile'];

                if(is_array($fdata['name'])) {
                    $countf_name = count($fdata['name']);
                    for($i=0;$i<$countf_name;++$i){
                        $files[]=array(
                            'name'    =>$fdata['name'][$i],
                            'tmp_name'=>$fdata['tmp_name'][$i],
                            'type' => $fdata['type'][$i],
                            'size' => $fdata['size'][$i],
                            'error' => $fdata['error'][$i],
                        );
                    }
                }
                else 
                    $files[]=$fdata;
                                    
                $attachments = array();

                foreach($files as $file) 
                {
                    if (isset($files) AND !empty($file['name']) AND $file['error'] != 0)
                        $this->_errors[] = Tools::displayError('An error occurred with the file upload.');
                    
                    if (!empty($file['name'])) {
                        $extension = array('.txt', '.rtf', '.doc', '.docx', '.pdf', '.zip', '.png', '.jpeg', '.gif', '.jpg');
                        $filename = md5(uniqid().substr($file['name'], -5));
                        $fileAttachment['content'] = file_get_contents($file['tmp_name']);
                        $fileAttachment['name'] = $file['name'];
                        $fileAttachment['mime'] = $file['type'];
                    }

                    if (isset($filename) AND rename($file['tmp_name'], _PS_MODULE_DIR_.'../upload/'.$filename)) {
                        $filename = $filename.":::".$file['name'];
                        $file_name .= $filename.";";
                    }
                        
                    $attachments[] = $fileAttachment;
                }
                
                $cm->file_name = $file_name;

                // inserimento in ps_customer_message
                if ($cm->add())
                {
                    $idmessaggio_ticket = Db::getInstance()->Insert_ID();
                    
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."customer_message SET note_private = '".addslashes(Tools::getValue('note_private'))."', in_carico = '".Tools::getValue('in_carico_a')."', email = '".Tools::getValue('customer_email_msg')."' WHERE id_customer_message = ".$idmessaggio_ticket."");

                    $tickettipo = $ct->id_contact;

                    if ($tickettipo == 4){
						$sigla = "T";
					}
					else if ($tickettipo == 2){
						$sigla = "A";
					} 
					else if ($tickettipo == 3){
						$sigla = "V";
					} 
					else if ($tickettipo == 6){
						$sigla = "R";
					}
					else if ($tickettipo == 8){
						$sigla = "D";
					} 
					else if ($tickettipo == 9){
						$sigla = "S";
					}
                    
                    $anno = $ct->date_add;
                    $anno = substr($anno,2,2);
                    
                    $id_thread_ticket = $ct->id;
                    $id_ticket = $sigla.$anno.$id_thread_ticket;
                    $numerounivoco_ticket = $sigla.$anno.$id_thread_ticket.$idmessaggio_ticket;
                    
                    $primo_ticket = Db::getInstance()->getValue("SELECT id_customer_message FROM "._DB_PREFIX_."customer_message WHERE id_customer_thread = ".$id_thread_ticket." ORDER BY id_customer_message ASC");
                    
                    if(!$primo_ticket) {
                        $primo_ticket = $idmessaggio_ticket;
                    }
                    
                    $cfpicr = Db::getInstance()->getValue("SELECT id_customer FROM "._DB_PREFIX_."customer WHERE id_customer = ".$ct->id_customer."");
                    $addretc = Db::getInstance()->getValue("SELECT id_address FROM "._DB_PREFIX_."address WHERE fatturazione = 1 AND active = 1 AND deleted = 0 AND id_customer = ".$ct->id_customer."");
                    
                    $in_carico_a =  Db::getInstance()->getValue("SELECT firstname FROM "._DB_PREFIX_."employee WHERE id_employee = ".Tools::getValue('in_carico_a')."");
                    
                    $clixcsv = new Customer($cfpicr);
                    $addxcsv = new Address($addretc);
                    
                    // inutilizzato? correggere o eliminare
                    // $provinciaxcsv = Db::getInstance()->getRow("SELECT iso_code FROM state WHERE id_state = ".$addxcsv->id_state);
                    
                    $primo_ticket = $sigla.$anno.$primo_ticket;
                    $messaggioclientepercsv = Tools::getValue('message');
                    $messaggioclientepercsv = strip_tags($messaggioclientepercsv);
                    $messaggioclientepercsv = str_replace(";", ",", $messaggioclientepercsv);
                    
                    $messaggioclientepercsv = str_replace(chr(10), " ", $messaggioclientepercsv); //remove carriage returns
                    $messaggioclientepercsv = str_replace(chr(13), " ", $messaggioclientepercsv); //remove carriage returns	
                    
                    $messaggioclientepercsv2 = explode("Cordiali saluti / Best regards", $messaggioclientepercsv);
                    
                    $messaggioclientepercsv = $messaggioclientepercsv2[0];
                    
                    $employeeincaricato = new Employee($mio_id);
                    $employeeincaricato2 = new Employee(Tools::getValue('in_carico_a'));
                    
                    // CORREGGERE: DECOMMENTARE QUANDO FUNZIONANO I FILE
                    /*
                    $ref = rand(0, 99999); 
                    $ref2 = rand(0, 99999);

                    if ($tickettipo == 4){
						$file_csv=fopen("../cms/assistenza_no_contratto_csv/csv-assistenza-output.csv","a+");
						if(Tools::getValue('isAnswer') == 1) {
							$riga_richiesta = "".date('H:i:s d F Y')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;;;;;;;;;;;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;R;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
						}
						else {
							$riga_richiesta = "".date('H:i:s d F Y')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;".$clixcsv->firstname.";".$clixcsv->lastname.";;".$clixcsv->vat_number.";".$clixcsv->tax_code.";".$addxcsv->address1.";".$addxcsv->city.";".$addxcsv->postcode.";".$provincia.";".$clixcsv->email.";".$addxcsv->phone.";".$addxcsv->fax.";".$addxcsv->phone_mobile.";;;;;;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;D;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
						}
					}
					else if ($tickettipo == 2){					
						$file_csv=fopen("../cms/amministrativa_ordini_csv/csv-EZamm-contabilita-output.csv","a+");
						if(Tools::getValue('isAnswer') == 1) {
							$riga_richiesta = "".date('H:i:s d L a')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;;;;;;;".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;Accettata;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;R;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
						}
						else {
							$riga_richiesta = "".date('H:i:s d L a')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$clixcsv->firstname.";".$clixcsv->lastname.";".$addxcsv->address1.";".$addxcsv->postcode.";".$addxcsv->city.";".$provincia.";".$addxcsv->phone.";".$addxcsv->fax.";".$clixcsv->email.";".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;Accettata;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;D;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
						}
					} 
					else if ($tickettipo == 8){					
						$file_csv=fopen("../cms/amministrativa_ordini_csv/csv-EZamm-ordini-output.csv","a+");
						if(Tools::getValue('isAnswer') == 1) {
							$riga_richiesta = "".date('H:i:s d L a')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;;;;;;;".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;Accettata;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;R;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
						}
						else {
							$riga_richiesta = "".date('H:i:s d L a')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$clixcsv->firstname.";".$clixcsv->lastname.";".$addxcsv->address1.";".$addxcsv->postcode.";".$addxcsv->city.";".$provincia.";".$addxcsv->phone.";".$addxcsv->fax.";".$clixcsv->email.";".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;Accettata;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;D;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
						}
					} 
					else if ($tickettipo == 3){
						$data_quotazione = date("d/m/Y H:i:s");

						$file_csv=fopen("../cms/quotazioni/rivenditori.csv","a+");
						if(Tools::getValue('isAnswer') == 1) {
							$riga_richiesta = ";".$data_quotazione.";".date('Ymd')."-".$ref."-".$ref2.";;".$clixcsv->vat_number.";".$clixcsv->tax_code.";".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;;0;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;R;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
						}
						else {
							$riga_richiesta = ";".$data_quotazione.";".date('Ymd')."-".$ref."-".$ref2.";;".$clixcsv->vat_number.";".$clixcsv->tax_code.";".$clixcsv->firstname.";".$clixcsv->lastname.";;;0;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;D;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
						}
					} 
					else if ($tickettipo == 6){
						$file_csv=fopen("../cms/callmeback/csv-EZcall-output.csv","a+");
						if(Tools::getValue('isAnswer') == 1) {
							$riga_richiesta = "".date('H:i:s d F Y')." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;;;;;;;;;".$cfpicr['vat_number'].";".$cfpicr['tax_code'].";".$messaggioclientepercsv.";;Accettata;$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;R;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
						}
					}
                    @fwrite($file_csv,$riga_richiesta);
                    @fclose($file_csv);	
                    */

                    // risposta
                    if(Tools::getValue('isAnswer') == 1) {
                        // correggere: decommentare link giusto (manca contact-form.php)
						$params = array(
							'{reply}' => $gentilecliente.(Tools::getValue('message')),
							'{firma}' => $firma,
							'{id_richiesta}' => "Id ticket: <strong>".$id_ticket."</strong>",
							//'{link}' => $link->getPageLink('contact-form.php', true).'?id_customer_thread='.(int)($ct->id).'&token='.$ct->token
						);
					}
                    // nuovo ticket
					else 
                    {
						Customer::Storico($id_thread_ticket, 'T', $mio_id, 11);

						// correggere: decommentare link giusto (manca contact-form.php)
						$params = array(
							'{reply}' => $gentilecliente."&egrave; stato aperto un ticket dal nostro operatore ".$employeeincaricato->firstname." ".$employeeincaricato->lastname.". Il testo del ticket:<br /><br />".(Tools::getValue('message')),
							'{firma}' => $firma,
							'{id_richiesta}' => "Id ticket: <strong>".$id_ticket."</strong>",
							//'{link}' => $link->getPageLink('contact-form.php', true).'?id_customer_thread='.(int)($ct->id).'&token='.$ct->token
						);

						$tokenimp = Tools::getAdminToken("AdminCustomers".(int)Tab::getIdFromClassName("AdminCustomers").$employeeincaricato->id);
						$linkimp = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&tab_name=tickets&azione=tickets&viewticket&id_customer_thread=".$ct->id."&token=".$tokenimp;	
						
                        if($mailcontatto == 'assistenza@ezdirect.it') {
							$linkmassimo = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&tab_name=tickets&azione=tickets&viewticket&id_customer_thread=".$ct->id."&token=".Tools::getAdminToken("AdminCustomers".(int)Tab::getIdFromClassName("AdminCustomers")."4"); // 4 = Massimo
							$linkpaolo = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&tab_name=tickets&azione=tickets&viewticket&id_customer_thread=".$ct->id."&token=".Tools::getAdminToken("AdminCustomers".(int)Tab::getIdFromClassName("AdminCustomers")."5"); // 5 = Paolo
							$linklorenzo = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&tab_name=tickets&azione=tickets&viewticket&id_customer_thread=".$ct->id."&token=".Tools::getAdminToken("AdminCustomers".(int)Tab::getIdFromClassName("AdminCustomers")."12"); // 12 = Lorenzo
							$linkleonardo = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&tab_name=tickets&azione=tickets&viewticket&id_customer_thread=".$ct->id."&token=".Tools::getAdminToken("AdminCustomers".(int)Tab::getIdFromClassName("AdminCustomers")."17"); // 17 = Leonardo
							$stringrisposta = 'Ticket assistenza<br />
							<a href="'.$linkmassimo.'">Massimo - Per rispondere clicca qui</a><br />
							<a href="'.$linkpaolo.'">Paolo - Per rispondere clicca qui</a><br />
							<a href="'.$linklorenzo.'">Lorenzo - Per rispondere clicca qui</a><br />
							<a href="'.$linkleonardo.'">Leonardo - Per rispondere clicca qui</a><br />
							';
						}
						
						$params2 = array(
                            '{reply}' => "<strong>".$employeeincaricato->firstname." ".$employeeincaricato->lastname."</strong> ha aperto un ticket per conto di un cliente.<br /><br />
                            Il ticket &egrave; in carico a: <strong>".$in_carico_a."</strong><br /><br />
                            L'id del ticket &egrave;: <strong>".$id_ticket."</strong><br /><br />
                            ".($mailcontatto == 'assistenza@ezdirect.it' ? $stringrisposta : "<a href='".$linkimp."'>Per rispondere clicca qui</a>.")." Di seguito il testo del ticket:<br /><br />".(Tools::getValue('message'))."
						");
						
                        $mailcontatto = 'carolina.carusi@ezdirect.it'; // TEST! decommentare quello giusto
						/*$mailcontatto = Db::getInstance()->getValue('
							SELECT email 
							FROM '._DB_PREFIX_.'contact 
							WHERE id_contact = '.$ct->id_contact.'
						');*/
						
						if($is_supplier == 1) {
                            // correggere: sostituire 'msg_base' con 'risposta_fornitore' e il 2° NULL con 'info@ezdirect.it' dopo i test
							Mail::Send($this->context->language->id, 'msg_base', Mail::l('Aperto ticket su Ezdirect', $this->context->language->id), 
                            $params2, $mailcontatto, NULL, NULL, 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true); 
						}
						else {
                            // correggere: sostituire 'msg_base' con 'action' e il 2° NULL con 'nonrispondere@ezdirect.it' dopo i test
							Mail::Send($this->context->language->id, 'msg_base', Mail::l('Aperto ticket per conto del cliente', $this->context->language->id), 
							$params2, $mailcontatto, NULL, NULL, 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true);
						}
					}

					$this->assegna_incarico(Tools::getValue('in_carico_a'), $ct->id, 'ticket', $customer);
                    
                    $in_carico_a =  Db::getInstance()->getValue("
                        SELECT firstname 
                        FROM "._DB_PREFIX_."employee 
                        WHERE id_employee = ".Tools::getValue('in_carico_a')."
                    ");

                    if(empty($in_carico_a)) 
                        $in_carico_a = "Nessuno";

                    $mails_ticket = explode(";", Tools::getValue('customer_email_msg'));
					
					foreach ($mails_ticket as $mail) 
                    {
                        $mail = 'carolina.carusi@ezdirect.it'; // TEST! DECOMMENTARE QUELLO GIUSTO -> eliminare questo e basta
						$mail = trim($mail);
						Customer::findEmailPersona($mail, Tools::getValue('id_customer'));

						if($is_supplier == 1) {
                            // correggere: sostituire 'msg_base' con 'risposta_fornitore' e il 2° NULL con 'info@ezdirect.it' dopo i test
							if (Mail::Send($this->context->language->id, 'msg_base', Mail::l('Messaggio da Ezdirect', $this->context->language->id), 
								$params, $mail, NULL, NULL, 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true))
                            {
								// Commentato 1.4
								//$ct->status = 'closed';
								//$ct->update();
								/*$mail_scrivente = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = ".$mio_id."");
								Mail::Send($this->context->language->id, 'msg_base', Mail::l('Copia messaggio inviato al cliente', $this->context->language->id), 
								$params, $mail_scrivente, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
								_PS_MAIL_DIR_, true);*/

								Customer::Storico($id_thread_ticket, 'T', $mio_id, 7);
                                // Correggere: decommentare quando funzionano i file
								// $log_mail=fopen("../import/log-mail.txt","a+");
								// $riga_log = "MSG | Mail inviata a ".$mail." in data ".date("Y-m-d H:i:s")."\n";
								// @fwrite($log_mail,$riga_log);
								// @fclose($log_mail);
							}
						}
						else {
                            // correggere: sostituire 'msg_base' con 'reply_msg' e il 2° NULL con 'nonrispondere@ezdirect.it' dopo i test
							if (Mail::Send((int)$ct->id_lang, 'msg_base', Mail::l('Messaggio da Ezdirect', (int)$ct->id_lang), 
								$params, $mail, NULL, NULL, 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true))
							{
								// Commentato 1.4
								//$ct->status = 'closed';
								//$ct->update();
								/*$mail_scrivente = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = ".$mio_id."");
								Mail::Send($this->context->language->id, 'simple_msg', Mail::l('Copia ticket inviato al cliente', $this->context->language->id), 
								$params, $mail_scrivente, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
								_PS_MAIL_DIR_, true);*/

								Customer::Storico($id_thread_ticket, 'T', $mio_id, 7);
                                // Correggere: decommentare quando funzionano i file
								// $log_mail=fopen("../import/log-mail.txt","a+");
								// $riga_log = "TKT | Mail inviata a ".$mail." in data ".date("Y-m-d H:i:s")."\n";
								// @fwrite($log_mail,$riga_log);
								// @fclose($log_mail);
							}
						}
					}

					$cc = "";
					$cc_str = "";

					foreach($_POST['conoscenza_t'] as $conoscenza) {
						$cc_str = $cc_str.Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$conoscenza).' ';
					}

					foreach($_POST['conoscenza_t'] as $conoscenza)
                    {
						$tokenimp = Tools::getAdminToken("AdminCustomers".(int)Tab::getIdFromClassName("AdminCustomers").$conoscenza);
						$linkimp = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&tab_name=tickets&azione=tickets&viewticket&id_customer_thread=".$ct->id."&token=".$tokenimp;	
						
                        // correggere: decommentare link giusto (manca contact-form.php)
						$params4 = array(
                            '{reply}' => "<strong>".$employeemess->firstname."</strong> ha inviato una comunicazione di un ticket al cliente e ti ha messo in copia conoscenza. <br /><br />
                                        Id ticket: <strong>".$id_ticket."</strong><br /><br />
                                        Cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id."<br /><br />
                                        Il ticket &egrave; in carico a: <strong>".$in_carico_a."</strong><br /><br />	
                                        Il messaggio &egrave; stato inviato in conoscenza a: ".$cc_str."<br /><br />								
                                        <a href='".$linkimp."'>Per aprire il ticket fai clic qua</a>.",
                            //'{link}' => $link->getPageLink('contact-form.php', true).'?id_customer_thread='.(int)($ct->id).'&token='.$ct->token
                        );
						
						$cc .= $conoscenza.";";

                        $mailconoscenza = 'carolina.carusi@ezdirect.it'; // TEST! DECOMMENTARE QUELLO GIUSTO
						// $mailconoscenza = Db::getInstance()->getValue("SELECT email FROM "._DB_PREFIX_."employee WHERE id_employee = $conoscenza");

						// correggere: sostituire 'msg_base' con 'senzagrafica' e il 2° NULL con 'nonrispondere@ezdirect.it' dopo i test
						Mail::Send((int)$ct->id_lang, 'msg_base', Mail::l('Ticket utente in copia conoscenza', (int)$ct->id_lang), 
						$params4, $mailconoscenza, NULL, NULL, 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true);
					}

					if(!empty($_POST['conoscenza_t'])) {
						Db::getInstance()->executeS("INSERT INTO ticket_cc (msg, type, cc) VALUES (".$idmessaggio_ticket.", 't', '".$cc."')"); 
					}

					Tools::redirectAdmin(self::$currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&tab_name=tickets&conf=4&token='.$this->token);
                }
                else
                    $this->_errors[] = Tools::displayError('An error occurred, your message was not sent. Please contact your system administrator.');
            }

            $action_types = array(
                array(
                    "value" => "4",
                    "name" => "Assistenza tecnica"
                ),
                array(
                    "value" => "2",
                    "name" => "Assistenza amministrativa - contabilit&agrave;"
                ),
                array(
                    "value" => "8",
                    "name" => "Assistenza amministrativa - ordini"
                ),
                array(
                    "value" => "3",
                    "name" => "Rivenditori"
                ),
                array(
                    "value" => "preventivo",
                    "name" => "Richiesta di preventivo"
                ),
                array(
                    "value" => "tirichiamiamonoi",
                    "name" => "Ti richiamiamo noi"
                ),
                array(
                    "value" => "7",
                    "name" => "Messaggio"
                ),
            );

            $ticket_types = array(
                array(
                    "value" => "4",
                    "name" => "Assistenza tecnica"
                ),
                array(
                    "value" => "2",
                    "name" => "Assistenza amministrativa - contabilit&agrave;"
                ),
                array(
                    "value" => "8",
                    "name" => "Assistenza amministrativa - ordini"
                ),
                array(
                    "value" => "9",
                    "name" => "RMA"
                ),
                array(
                    "value" => "3",
                    "name" => "Rivenditori"
                ),
            );

            $precompilati = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'precompilato WHERE active = 1');

            if(Tools::getIsset('viewticket')){

                if(!Tools::getIsset('conf') && !Tools::getIsset('filename'))
					Customer::Storico(Tools::getValue('id_customer_thread'), 'T', $mio_id, 14);

                if(isset($_GET['filename'])) {
                    $filename = $_GET['filename'];
                    
                    if(strpos($filename, ":::")) {
                        $parti = explode(":::", $filename);
                        $nomecodificato = $parti[0];
                        $nomevero = $parti[1];
                    }
                    else {
                        $nomecodificato = $filename;
                        $nomevero = $filename;
                    }
                    
                    if (file_exists(_PS_UPLOAD_DIR_.$nomecodificato)) {
                        self::openUploadedFile();
                    }
                }

                if(Tools::getIsset('id_customer_thread')) {

                    $errore = false;
                    $thread = Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."customer_thread ct WHERE ct.id_customer_thread = ".Tools::getValue('id_customer_thread')." AND ct.id_customer = ".$customer->id."");
                                        
                    if(!$thread && Tools::getValue('id_customer_thread') != '') {
                        $errore = true;
                    }
                    else {

                        $ticket = $thread;

                        // correggere: come devo usarlo? Nella 1.4 c'era una sezione bdl: ($bdl_ticket > 0 || Tools::getIsset('nuovobdl') ? '<li><a href="#ticket-5"><strong>BDL</strong></a></li>' : '')
				        $bdl_ticket = Db::getInstance()->getValue('SELECT id_bdl FROM '._DB_PREFIX_.'bdl WHERE riferimento = "T'.Tools::getValue('id_customer_thread').'"');

                        $incaricato = new Employee($ticket['id_employee']);
                        $in_carico_a_firstname = $incaricato->firstname;

                        $annoticket = substr($ticket['date_add'],2,2);
                        
                        $query_cm = Db::getInstance()->getRow("
                            SELECT date_add, message 
                            FROM "._DB_PREFIX_."customer_message
                            WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')." 
                            ORDER BY date_add DESC
                        ");

                        switch($ticket['status']) {
                            case 'open': 
                                $status_ticket = "<i class='icon-circle rosso' title='Aperto'></i>";
                                $st_tkt = 'Aperto';
                                break;
                            case 'closed': 
                                $status_ticket = "<i class='icon-circle' style='color:green' title='Chiuso'></i>";
                                $st_tkt = 'Chiuso';
                                break;
                            case 'pending1': 
                                $status_ticket = "<i class='icon-circle' style='color:orange' title='In lavorazione'></i>";
                                $st_tkt = 'In lavorazione';
                                break;
                            default: 
                                $status_ticket = "<i class='icon-circle rosso' title='Aperto'></i>";
                                $st_tkt = 'Aperto';
                                break;
                        }
    
                        $status_azione = $status_ticket;
                        $st_azione = $st_tkt;
    
                        switch ($ticket['id_contact']) {
                            case 2:
                                $sigla = 'A';
                                break;
                            case 3:
                                $sigla = 'V';
                                break;
                            case 4:
                                $sigla = 'T';
                                break;
                            case 6:
                                $sigla = 'R';
                                break;
                            case 8:
                                $sigla = 'D';
                                break;
                            case 9:
                                $sigla = 'S';
                                break;
                            
                            default:
                                break;
                        }
    
                        $idticketunivoco = $sigla.$annoticket.$ticket['id_customer_thread'];

                        /* Generale */
                        $oggetto =  $ticket['subject'];
                        if($oggetto == '')
                            $oggetto = $query_cm['message'];

                        $aperto_da = Db::getInstance()->getValue('SELECT id_employee FROM '._DB_PREFIX_.'customer_message WHERE id_customer_thread = '.Tools::getValue('id_customer_thread').' ORDER BY id_customer_message ASC');
                        
                        if($aperto_da != 0)
                            $aperto_da = Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$aperto_da);
                        else
                            $aperto_da = 'Cliente';

                        $data_apertura = Tools::displayDate($ticket['date_add'], (int)($this->context->language->id), true);

                        $dataultimo = $query_cm['date_add'];
                        $data_ultima_com = Tools::displayDate($dataultimo, (int)($this->context->language->id), true);

                        // RMA
                        if($ticket['id_contact'] == 9) {
                            $invoices = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'fattura WHERE id_customer = '.$customer->id.' GROUP BY id_fattura ORDER BY data_fattura');
                            $dati_rma = Db::getInstance()->getRow('SELECT * FROM rma WHERE id_customer_thread = '.$ticket['id_customer_thread']);
                            
                            foreach($invoices as $inv1) {
                                $rma_fatture_options .= '<option name="'.$inv1['id_riga'].'" value="'.$inv1['id_riga'].'" '.($inv1['id_riga'] == $ticket['id_order'] ? 'selected="selected"' : '').'>'.$inv1['id_fattura'] .' del '. date('d/m/Y',strtotime($inv1['data_fattura'])).'</option>';
                            }
                        }

                        if($ticket['riferimento'] != '')
                            $azione_padre = Customer::trovaSiglaLinkPerAlbero(substr($ticket['riferimento'],1), substr($ticket['riferimento'],0,1), '', 'y:T:'.$ticket['id_customer_thread'].':'.$ticket['status']);
                        
                        // Prodotto di riferimento
                        $getoOrders = Db::getInstance()->executeS('
                            SELECT id_order
                            FROM '._DB_PREFIX_.'orders
                            WHERE id_customer = '.(int)$customer->id.' 
                            ORDER BY date_add
                        ');
                            
                        foreach ($getoOrders as $rowo) {
                            $ordero = new Order($rowo['id_order']);
                            $tmp = $ordero->getProducts();

                            foreach ($tmp as $key => $val) {
                                $prodrichiesta = Db::getInstance()->getRow("SELECT reference, name FROM "._DB_PREFIX_."product JOIN "._DB_PREFIX_."product_lang ON product.id_product = product_lang.id_product WHERE id_lang = ".$this->context->language->id." and product.id_product = ".$val['product_id']."");
                                $prodotto_rif_options .= '<option name="'.$val['product_id'].'" value="'.$val['product_id'].'" '.($ticket['id_product'] == $val['product_id'] ? 'selected="selected"' : '').'>'.$prodrichiesta['reference']." - ".$prodrichiesta['name'].'</option>';
                            }
                        }
                
                        if($ticket['id_product'] != 0) { 
                            $prodrichiesta = Db::getInstance()->getRow("SELECT reference, name FROM "._DB_PREFIX_."product JOIN "._DB_PREFIX_."product_lang ON product.id_product = product_lang.id_product WHERE id_lang = ".$this->context->language->id." and product.id_product = ".$ticket['id_product'].""); 
                            $prodotto_rif = $prodrichiesta['reference']." - ".$prodrichiesta['name']; 
                        } 
                        else if ($ticket['id_contact'] == 9 && $ticket['id_product'] == 0) {
                            $prodotto_rif = $dati_rma['rma_product'];
                        }
                        else { 
                            $prodotto_rif = '<em>Nessuno</em>';
                        } 

                        // Assistenza tecnica
                        if($ticket['id_contact'] == 4 ) {
                            $altro_descrizione = (substr($thread['descrizione'],0,5) == 'Altro' ? substr($thread['descrizione'],5,strlen($thread['descrizione'])) : $thread['descrizione']);
                            $altro_guasto = (substr($thread['guasto'],0,5) == 'Altro' ? substr($thread['guasto'],5,strlen($thread['guasto'])) : $thread['guasto']);
                            $altro_causa_guasto = (substr($thread['causa_guasto'],0,5) == 'Altro' ? substr($thread['causa_guasto'],5,strlen($thread['causa_guasto'])) : $thread['causa_guasto']);
                        }

                        // RMA
                        if($ticket['id_contact'] == 9) {
						    $dati_rma['indirizzi_rma'] = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'address a JOIN '._DB_PREFIX_.'state s ON a.id_state = s.id_state WHERE a.active = 1 AND a.deleted = 0 AND a.id_customer='.$customer->id);
						    $dati_rma['impiegati_test'] = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'employee WHERE active = 1 AND id_profile = 4 OR id_profile = 6');

                            $dati_rma['arrivocli'] = ($dati_rma['arrivocli'] == '0000-00-00 00:00:00' || $dati_rma['arrivocli'] == '1970-01-01 01:00:00' ? '' : date('d-m-Y', strtotime($dati_rma['arrivocli'])));
                            $dati_rma['data_richiesta'] = ($dati_rma['data_richiesta'] == '0000-00-00 00:00:00' || $dati_rma['data_richiesta'] == '1970-01-01 01:00:00' ? '' : date('d-m-Y', strtotime($dati_rma['data_richiesta'])));
                            $dati_rma['arrivoezfornitore'] = ($dati_rma['arrivoezfornitore'] == '0000-00-00 00:00:00' || $dati_rma['arrivoezfornitore'] == '1970-01-01 01:00:00' ? '' : date('d-m-Y', strtotime($dati_rma['arrivoezfornitore'])));

                            $dati_rma['totale_rma'] = number_format(($dati_rma['costo_materiale'] + $dati_rma['costo_manodopera'] + $dati_rma['totale_trasporti']),2,",","");
                            $dati_rma['costo_materiale'] = str_replace(".",",",$dati_rma['costo_materiale']);
                            $dati_rma['costo_manodopera'] = str_replace(".",",",$dati_rma['costo_manodopera']);
                            $dati_rma['trasporti_qta'] = str_replace(".",",",$dati_rma['trasporti_qta']);
                            $dati_rma['totale_trasporti'] = str_replace(".",",",$dati_rma['totale_trasporti']);
                        }

                        // Note private
                        $ticket_note = Db::getInstance()->executeS('SELECT * FROM note_attivita WHERE tipo_attivita = "T" AND id_attivita = '.$ticket['id_customer_thread'].'');
                        foreach($ticket_note as &$ticket_nota)
                            $ticket_nota['creato_da'] = Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = "'.$ticket_nota['id_employee'].'"');

                        // Vecchia nota
                        if($ticket['date_add'] < '2017-12-08 00:00:00'){
                            $ticket_old = true;
                            $ticket_nota = Tools::htmlentitiesUTF8($ticket['note']);
                        }
                        else
                            $ticket_old = false;

                        $collega_a = Customer::BuildSelectForLinking($customer->id, $ticket['id_customer_thread'], 'T');

                        $generale = array(
                            'azione' => $ticket,
                            'idunivoco' => $idticketunivoco,
                            'status_azione' => $status_azione,
                            'st_azione' => $st_azione,
                            'oggetto' => $oggetto,
                            'aperto_da' => $aperto_da,
                            'in_carico_a_options' => $impiegati_eza,
                            'in_carico_a_firstname' => $in_carico_a_firstname,
                            'data_apertura' => $data_apertura,
                            'data_ultima_com' => $data_ultima_com,
                            'dati_rma' => $dati_rma,
                            'rma_fatture_options' => $rma_fatture_options,
                            'azione_padre' => $azione_padre,
                            'prodotto_rif_options' => $prodotto_rif_options,
                            'prodotto_rif' => $prodotto_rif,
                            'altro_descrizione' => $altro_descrizione,
                            'altro_guasto' => $altro_guasto,
                            'altro_causa_guasto' => $altro_causa_guasto,
                            'azione_note' => $ticket_note,
                            'ticket_old' => $ticket_old,
                            'ticket_nota' => $ticket_nota,
                            'collega_a' => $collega_a,
                            'errore' => $errore,
                        );
                        /* Fine Generale */

                        /* Cronologia */
                        $messaggit = Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_."customer_message WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')." ORDER BY date_add ASC");
                        $msg_id = 0;

                        foreach($messaggit as &$messaggiot) {

                            if ($messaggiot['id_employee'] == 0) {
                                if($messaggiot['email'] == "" || is_numeric($messaggiot['email']))
                                    $messaggiot['da'] = $customer->firstname." ".$customer->lastname;
                                else
                                    $messaggiot['da'] = "Ticket aperto dallo staff";
                            }
                            else {
                                $employeezt = new Employee($messaggiot['id_employee']);
                                $messaggiot['da'] = $employeezt->firstname." ".substr($employeezt->lastname,0,1).".";

                                if($msg_id == 0)
                                    $messaggiot['da'] .= " - ticket aperto dallo staff";
                            }
                            
                            $messaggiot['data'] = date("d-m-Y H:i:s",strtotime($messaggiot['date_add']));

                            if(!empty($messaggiot['file_name'])) {
                                $allegatit = explode(";",$messaggiot['file_name']);
                                $nall = 1;
                                foreach($allegatit as $allegato) {
                                    if(strpos($allegato, ":::")) {
                                        $parti = explode(":::", $allegato);
                                        $nomeverot = $parti[1];
                                    }
                                    else
                                        $nomeverot = $allegato;
                                    
                                    if($allegato == "") {
                                        // niente
                                    } 
                                    else {
                                        if($nall != 1)
                                            $allegati .= '-';

                                        $allegati .= '<a href="ajax.php?openAllegato=y&id_customer='.$customer->id.'&viewcustomer&tab_name=tickets&azione=tickets&viewticket&id_customer_thread='.$ticket['id_customer_thread'].'&filename='.$allegato.'&token='.$this->token.'"><span style="color:red">'.$nomevero.'</span></a>';
                                        $nall++;
                                    }
                                }
                            }
                            else
                                $allegati = 'Nessun allegato';
                                
                            $messaggiot['allegati'] = $allegati;

                            $messaggiot['message'] = htmlspecialchars_decode($messaggiot['message']);
                            $messaggiot['message_modifica'] = htmlentities($messaggiot['message'], ENT_QUOTES | ENT_SUBSTITUTE, "UTF-8");
                            
                            if($messaggiot['note_private'] != '')
                                $messaggiot['note_private'] = htmlspecialchars_decode(preg_replace('/(<[^>]+) style=".*?"/i', '$1', preg_replace('/(<[^>]+) class=".*?"/i', '$1', $messaggiot['note_private'])));
                            
                            if($messaggiot['email'] != '')
                                $messaggiot['employee_to_name'] = (is_numeric($messaggiot['email']) ? Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$messaggiot['email']) : htmlspecialchars_decode($messaggiot['email']));

                            $messaggiot['in_carico_a_name'] = ($messaggiot['in_carico'] > 0 ? Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$messaggiot['in_carico']) : '--');
                            
                            $cc = Db::getInstance()->getValue("SELECT cc FROM ticket_cc WHERE type = 't' AND msg = ".$messaggiot['id_customer_message']."");
                            if($cc) {
                                $ccs = explode(";",$cc);
                                
                                foreach ($ccs as $conoscenza) {
                                    $imp = Db::getInstance()->getValue("SELECT firstname FROM "._DB_PREFIX_."employee WHERE id_employee = '".$conoscenza."'");
                                    $cc_str .= $imp." ";
                                }
                                
                                $messaggiot['cc'] = $cc_str;
                            }
                            else {
                                $messaggiot['cc'] = 'Nessuno';
                            }

                            // Se non è un RMA, stampo ordine e prodotto di riferimento
                            if($ticket['id_contact'] != 9) {
                                $messaggiot['riferimenti'] = '<strong>Ordine di riferimento</strong>: '.($ticket['id_order'] != 0 ? $ticket['id_order'] : '<em>Nessuno</em>').' - <strong>Prodotto di riferimento</strong>: '; 
                                
                                if($ticket['id_product'] != 0) {
                                    $prodrichiesta = Db::getInstance()->getValue("SELECT name FROM "._DB_PREFIX_."product_lang WHERE id_lang = ".$this->context->language->id." AND id_product = ".$ticket['id_product']."");
                                    $messaggiot['riferimenti'] .= $prodrichiesta;
                                }
                                else
                                    $messaggiot['riferimenti'] .= '<em>Nessuno</em>'; 
                            }

                            $msg_id++;
                        }
                    }                    
                }

                if($customer->is_company == 1)
                    $persone = Db::getInstance()->executeS("SELECT id_persona, firstname, lastname, email FROM persone WHERE id_customer = ".Tools::getValue('id_customer')." AND email != ''");

                if(isset($_GET['id_persona']))
                    $mail_persona = Db::getInstance()->getValue("SELECT email FROM persone WHERE id_persona = ".$_GET['id_persona']);
                else
                    $mail_persona = $customer->email;

                // Correggere: si può migliorare prendendo il telefono della persona selezionata se il campo è vuoto
                if($ticket['phone'] != '')
                    $telefono_cliente = $ticket['phone'];
                else {
                    $customer_phone = Db::getInstance()->getRow("
                        SELECT phone, phone_mobile 
                        FROM "._DB_PREFIX_."address 
                        WHERE active = 1 
                            AND deleted = 0 
                            AND fatturazione = 1
                            AND id_customer = ".$customer->id
                    );

                    if($customer_phone['phone'] == '')
                        $telefono_cliente = $customer_phone['phone_mobile']; 
                    else
                        $telefono_cliente = $customer_phone['phone']; 
                }

                if(isset($_GET['aprinuovoticket'])) {
                    $impez_selected = $mio_id;
                }
                else {
                    $impez_selected = $ticket['id_employee'];
                }

                // Aggiungere form ricerca prodotti come in orders

                $marche = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'manufacturer ORDER BY name ASC');
                $categorie = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'category c JOIN '._DB_PREFIX_.'category_lang cl ON c.id_category = cl.id_category WHERE cl.id_lang = '.$this->context->language->id.' AND c.id_parent = 1 ORDER BY cl.name ASC');
                $fornitori = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'supplier WHERE id_supplier = 8 OR id_supplier = 11 OR id_supplier = 42 ORDER BY name ASC');

                $riferimento = (Tools::getValue('riferimento') ? Tools::getValue('riferimento') : Db::getInstance()->getValue('SELECT riferimento FROM '._DB_PREFIX_.'customer_thread WHERE id_customer_thread = '.$thread['id_customer_thread']));
                $statusPadre = Customer::statusPadre($riferimento);

                $cronologia = array(
                    'azione' => $ticket, // $thread nella 1.4
                    'messaggit' => $messaggit,
                    'impiegati_eza' => $impiegati_eza,
                    'impez_selected' => $impez_selected,
                    'persone' => $persone,
                    'mail_persona' => $mail_persona,
                    'telefono_cliente' => $telefono_cliente,
                    'marche' => $marche,
                    'categorie' => $categorie,
                    'fornitori' => $fornitori,
                    'precompilati' => $precompilati,
                    'riferimento' => $riferimento,
                    'statusPadre' => $statusPadre,
                );
                /* Fine Cronologia */

                // Gerarchia e Storico
                if(Tools::getIsset('id_customer_thread')) {

                    /* Gerarchia */
                    $first = Customer::HierarchyFirst(Tools::getValue('id_customer_thread'), 'T');
                        
                    // Correggere: manca la class tree-menu
                    $tree_ul = '<ul class="tree-menu"><li>'.Customer::trovaSiglaLinkPerAlbero(substr($first, 1), substr($first, 0, 1), '').'';
                    $replace .= str_replace('<ul></ul>', '', Customer::HierarchyFindChildren($first, $children, 'T'.Tools::getValue('id_customer_thread')));
                    $tree_ul .= $replace;
                    $tree_ul .= '</li></ul>';
                    
                    $gerarchia = array(
                        'tree_ul' => $tree_ul,
                    );
                    /* Fine Gerarchia */

                    /* Storico */
                    $storico_ticket = Db::getInstance()->executeS('SELECT * FROM storico_attivita WHERE id_attivita = '.Tools::getValue('id_customer_thread').' AND tipo_attivita = "T" ORDER BY data_attivita DESC, desc_attivita DESC');
    
                    foreach($storico_ticket as &$storico)
                    {
                        if(is_numeric(substr($storico['desc_attivita'], -1)))
                        {	
                            $employee = filter_var($storico['desc_attivita'], FILTER_SANITIZE_NUMBER_INT);
                            $name_employee = Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$employee);
                            $desc_attivita = 'Ha messo l\'attivit&agrave; in carico a '.$name_employee;
                        }
                        else
                            $desc_attivita = $storico['desc_attivita'];
    
                        $storico['azione'] = $desc_attivita;
                        $storico['data_attivita'] = Tools::displayDate($storico['data_attivita'], $this->context->language->id, true);
                        $storico_employee = Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$storico['id_employee']);
    
                        $storico['persona'] = ($storico['id_employee'] == 0 ? 'Cliente' : $storico_employee);
                    }
    
                    $storico = array(
                        'storico_ticket' => $storico_ticket,
                    );
                    /* Fine Storico */
                }

                $ticket_view = array(
                    'generale' => $generale,
                    'cronologia' => $cronologia,
                    'gerarchia' => $gerarchia,
                    'storico' => $storico,
                    /* Variabili usate sia in Generale (view/edit) che in Cronologia (add) */
                    'action_types' => $action_types,
                    'ticket_types' => $ticket_types,
                );
            }
            else
                $ticket_list = Customer::BuildListing($customer->id, 'ticket');

            $specific = 'n';

            $tickets = array(
                'ticket_list' => $ticket_list,
                'ticket_view' => $ticket_view,
                'specific' => $specific,
            );

        }
        /* FINE Ticket */

        $count_preventivi = Db::getInstance()->getValue("SELECT COUNT(id_thread) FROM form_prevendita_thread WHERE id_customer = ".$customer->id);
        $count_preventivi_aperti = Db::getInstance()->getValue("SELECT COUNT(id_thread) FROM form_prevendita_thread WHERE id_customer = ".$customer->id." AND status != 'closed'");
        $count_messaggi = Db::getInstance()->getValue("SELECT COUNT(id_customer_thread) FROM "._DB_PREFIX_."customer_thread WHERE id_contact = 7 AND id_customer = ".$customer->id);
        $messaggi_aperti = Db::getInstance()->executeS("SELECT ct.id_customer_thread FROM "._DB_PREFIX_."customer_message cm JOIN (SELECT * FROM "._DB_PREFIX_."customer_thread WHERE id_customer = ".$customer->id.") ct ON cm.id_customer_thread = ct.id_customer_thread WHERE ct.id_contact = 7 AND cm.id_employee = 0 AND (status = 'open' OR status = 'pending1') GROUP BY ct.id_customer_thread");
        $count_messaggi_aperti = sizeof($messaggi_aperti);

        $count_azioni_cliente = $count_preventivi + $count_messaggi;
        $count_azioni_cliente_aperte = $count_preventivi_aperti + $count_messaggi_aperti;

        /* INIZIO Azioni cliente (messaggi e preventivi / ti richiamiamo noi) */
        if($tab_name == 'actions' && !$not_mio_agente){ //tab-container-1=7

            // Cambia status - Preventivo / Ti richiamiamo noi
            if(isset($_POST['cambiastatusp_listing'])) {

                Db::getInstance()->execute("
                    UPDATE form_prevendita_thread 
                    SET status = '".Tools::getValue('cambiastatusp_listing')."', date_upd = '".date("Y-m-d H:i:s")."' 
                    WHERE id_thread = '".Tools::getValue('id_thread')."'
                ");

                switch(Tools::getValue('cambiastatusp_listing'))
                {
                    case 'open': $switch_status = 3; break;
                    case 'pending1': $switch_status = 4; break;
                    case 'closed': $switch_status = 5; break;
                    default: $switch_status = 10; break;
                }	
                
                Customer::Storico(Tools::getValue('id_thread'), 'P', $mio_id, $switch_status);
                Tools::redirectAdmin(self::$currentIndex.'&'.$this->identifier.'='.$customer->id.'&viewcustomer&tab_name=actions&conf=4&token='.$this->token);
			}

            // Cambia status - Messaggio
            if(isset($_POST['cambiastatust_listing'])) {

                Db::getInstance()->execute("
                    UPDATE "._DB_PREFIX_."customer_thread 
                    SET status = '".Tools::getValue('cambiastatust_listing')."', date_upd = '".date("Y-m-d H:i:s")."' 
                    WHERE id_customer_thread = '".Tools::getValue('id_customer_thread')."'
                ");
                
                switch(Tools::getValue('cambiastatust_listing'))
                {
                    case 'open': $switch_status = 3; break;
                    case 'pending1': $switch_status = 4; break;
                    case 'closed': $switch_status = 5; break;
                    default: $switch_status = 10; break;
                }
                
                Customer::Storico(Tools::getValue('id_customer_thread'), 'T', $mio_id, $switch_status);
                Tools::redirectAdmin(self::$currentIndex.'&'.$this->identifier.'='.$customer->id.'&viewcustomer&tab_name=actions&conf=4&token='.$this->token);
            }
			
            // Cambia in carico - Preventivo / Ti richiamiamo noi
			if(isset($_POST['cambiaincaricop'])) {
				Customer::cambiaIncaricoVeloce(Tools::getValue('tipo'), Tools::getValue('cambiaincaricop'), Tools::getValue('id_thread'), Tools::getValue('precedente_incarico'), Tools::getValue('id_ticket_univoco'), Tools::getValue('id_customer'));				
                Tools::redirectAdmin(self::$currentIndex.'&'.$this->identifier.'='.$customer->id.'&viewcustomer&tab_name=actions&conf=4&token='.$this->token);
			}

            // Cambia in carico - Messaggio
            if(isset($_POST['cambiaincaricot'])) {
                Customer::cambiaIncaricoVeloce(Tools::getValue('tipo'), Tools::getValue('cambiaincaricot'), Tools::getValue('id_thread'), Tools::getValue('precedente_incarico'), Tools::getValue('id_ticket_univoco'), Tools::getValue('id_customer'));
                Tools::redirectAdmin(self::$currentIndex.'&'.$this->identifier.'='.$customer->id.'&viewcustomer&tab_name=actions&conf=4&token='.$this->token);
            }

            // Salvataggio panel Generale - Preventivo / Ti richiamiamo noi -> TESTARE!
            if(isset($_POST['submitPreventivo'])) {

                $precedente_status = Db::getInstance()->getValue("SELECT status FROM form_prevendita_thread WHERE id_thread = ".Tools::getValue('id_thread'));
                $impiegato_attuale = Db::getInstance()->getValue("SELECT id_employee FROM form_prevendita_thread WHERE id_thread = ".Tools::getValue('id_thread'));
                
                switch(Tools::getValue('cambiastatusp'))
                {
                    case 'open': $switch_status = 3; break;
                    case 'pending1': $switch_status = 4; break;
                    case 'closed': $switch_status = 5; break;
                    default: $switch_status = 10; break;
                }

                // spostare dopo update?
                if($precedente_status != Tools::getValue('cambiastatusp'))
                    Customer::Storico(Tools::getValue('id_thread'), 'P', $mio_id, $switch_status);
                
                // Update subject, note, status, date_upd
                Db::getInstance()->execute("UPDATE form_prevendita_thread SET subject = '".addslashes(Tools::getValue('preventivo_subject'))."', note = '".addslashes(Tools::getValue('preventivo_note'))."', status = '".Tools::getValue('cambiastatusp')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_thread = '".Tools::getValue('id_thread')."'");

                /* NB: L'update delle note private viene fatto in ajax */
                
                if($impiegato_attuale != Tools::getValue('assegnaimpiegatop')) 
                {
                    $tipo_richiesta_p = Db::getInstance()->getValue("SELECT tipo_richiesta FROM form_prevendita_thread WHERE id_thread = '".Tools::getValue('id_thread')."'");
                    $tipo_richiesta_string = ($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo');

                    Customer::Storico(Tools::getValue('id_thread'), 'P', $mio_id, 2, Tools::getValue('assegnaimpiegatop'));
                    Db::getInstance()->execute("UPDATE form_prevendita_thread SET id_employee = '".Tools::getValue('assegnaimpiegatop')."' WHERE id_thread = '".Tools::getValue('id_thread')."'");
                    
                    // TEST! CORREGGERE; DECOMMENTARE MAIL GIUSTA
                    $mailimp = 'carolina.carusi@ezdirect.it';
                    //$mailimp = Db::getInstance()->getValue("SELECT email FROM "._DB_PREFIX_."employee WHERE id_employee = '".Tools::getValue('assegnaimpiegatop')."'");
                    $tokenimp = Tools::getAdminToken("AdminCustomers".(int)Tab::getIdFromClassName("AdminCustomers").Tools::getValue('assegnaimpiegatop'));
                    $linkimp = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&tab_name=actions&azione=actions&viewpreventivo&id_thread=".Tools::getValue('id_thread')."&token=".$tokenimp;

                    $msgimp = $employeemess->firstname." ".$employeemess->lastname." ti ha assegnato il ".$tipo_richiesta_string." numero ".Tools::getValue('id_preventivo_univoco')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect. <a href='".$linkimp."'>Fai clic qui per aprirlo</a>.";
                    
                    // Invio una mail di revoca della gestione a $impiegato_attuale, se non è l'employee loggato
                    if($impiegato_attuale != 0 && $impiegato_attuale != $mio_id)
                    {
                        $impiegato_assegnato = (Tools::getValue('assegnaimpiegatop') != 0 ? Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.Tools::getValue('assegnaimpiegatom')) : 'Nessuno');
                        // TEST! CORREGGERE; DECOMMENTARE MAIL GIUSTA
                        $mailimprev = 'carolina.carusi@ezdirect.it';
                        //$mailimprev = Db::getInstance()->getValue("SELECT email FROM "._DB_PREFIX_."employee WHERE id_employee = '".$impiegato_attuale."'");
                        $nomeimprev = Db::getInstance()->getValue("SELECT firstname FROM "._DB_PREFIX_."employee WHERE id_employee = '".$impiegato_attuale."'");
                        $msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione del ".$tipo_richiesta_string." numero ".Tools::getValue('id_preventivo_univoco')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect e l'ha passata a ".$impiegato_assegnato;
                                
                        $params = array(
                            '{msg}' => $msgimprev
                        );

                        Mail::Send($this->context->language->id, 'msg_base', Mail::l('Gestione '.$tipo_richiesta_string.' revocata', $this->context->language->id), 
                                $params, $mailimprev, NULL, NULL, NULL, NULL, NULL, _PS_MAIL_DIR_, true);
                    }
                            
                    $params = array(
                        '{msg}' => $msgimp
                    );
                            
                    Mail::Send($this->context->language->id, 'msg_base', Mail::l(''.ucfirst($tipo_richiesta_string).' assegnato a te su Ezdirect', $this->context->language->id), 
                            $params, $mailimp, NULL, NULL, NULL, NULL, NULL, _PS_MAIL_DIR_, true);
                }
                
                // Correggere: questo andrebbe fatto prima dell'invio delle mail, altrimenti il link potrebbe essere sbagliato (va modificato se è stato fatto convertiTicket)
                $id_contact = Db::getInstance()->getValue('SELECT tipo_richiesta FROM form_prevendita_thread WHERE id_thread = '.Tools::getValue('id_thread'));
                
                if($id_contact != Tools::getValue('id_contact')){
                    Customer::convertiTicket(Tools::getValue('id_thread'), $id_contact, Tools::getValue('id_contact'));
                    Tools::redirectAdmin(self::$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&tab_name=actions&conf=4&token='.$this->token);
                }
                else
                    Tools::redirectAdmin(self::$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&tab_name=actions&azione=actions&viewpreventivo&id_thread='.Tools::getValue('id_thread').'&conf=4&token='.$this->token);
                    
            }

            // Salvataggio panel Generale - Messaggio -> TESTARE!
            if(Tools::getIsset('submitMex')) {

                $precedente_status = Db::getInstance()->getValue("SELECT status FROM "._DB_PREFIX_."customer_thread WHERE id_customer_thread = ".Tools::getValue('id_mex'));
                $impiegato_attuale = Db::getInstance()->getValue("SELECT id_employee FROM "._DB_PREFIX_."customer_thread WHERE id_customer_thread = ".Tools::getValue('id_mex'));
                
                // Update subject, note, status, date_upd
                Db::getInstance()->execute("UPDATE "._DB_PREFIX_."customer_thread SET subject = '".addslashes(Tools::getValue('mex_subject'))."', note = '".addslashes(Tools::getValue('mex_note'))."', status = '".addslashes(Tools::getValue('cambiastatusm'))."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = '".Tools::getValue('id_mex')."'");
                
                switch(Tools::getValue('cambiastatusm'))
                {
                    case 'open': $switch_status = 3; break;
                    case 'pending1': $switch_status = 4; break;
                    case 'closed': $switch_status = 5; break;
                    default: $switch_status = 10; break;
                }	
                
                if($precedente_status != Tools::getValue('cambiastatusm'))
                    Customer::Storico(Tools::getValue('id_mex'), 'T', $mio_id, $switch_status);
                
                if($impiegato_attuale != Tools::getValue('assegnaimpiegatom')) 
                {
                    Customer::Storico(Tools::getValue('id_mex'), 'T', $mio_id, 2, Tools::getValue('assegnaimpiegatom'));
                    Db::getInstance()->execute("UPDATE "._DB_PREFIX_."customer_thread SET id_employee = '".Tools::getValue('assegnaimpiegatom')."' WHERE id_customer_thread = '".Tools::getValue('id_mex')."'");
                    
                    // TEST! CORREGGERE; DECOMMENTARE MAIL GIUSTA
                    $mailimp = 'carolina.carusi@ezdirect.it';
                    //$mailimp = Db::getInstance()->getValue("SELECT email FROM "._DB_PREFIX_."employee WHERE id_employee = '".Tools::getValue('assegnaimpiegatom')."'");
                    $tokenimp = Tools::getAdminToken("AdminCustomers".(int)Tab::getIdFromClassName("AdminCustomers").Tools::getValue('assegnaimpiegatom'));
                    $linkimp = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&tab_name=actions&azione=actions&viewmessage&id_mex=".Tools::getValue('id_mex')."&token=".$tokenimp;

                    $msgimp = $employeemess->firstname." ".$employeemess->lastname." ti ha assegnato il messaggio numero ".Tools::getValue('id_messaggio_univoco')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect. <a href='".$linkimp."'>Fai clic qui per aprirlo</a>.";
                                    
                    // Invio una mail di revoca della gestione a $impiegato_attuale, se non è l'employee loggato
                    if($impiegato_attuale != 0 && $impiegato_attuale != $mio_id)
                    {
                        $impiegato_assegnato = (Tools::getValue('assegnaimpiegatom') != 0 ? Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.Tools::getValue('assegnaimpiegatom')) : 'Nessuno');
                        // TEST! CORREGGERE; DECOMMENTARE MAIL GIUSTA
                        $mailimprev = 'carolina.carusi@ezdirect.it';
                        //$mailimprev = Db::getInstance()->getValue("SELECT email FROM "._DB_PREFIX_."employee WHERE id_employee = '".$impiegato_attuale."'");
                        $nomeimprev = Db::getInstance()->getValue("SELECT firstname FROM "._DB_PREFIX_."employee WHERE id_employee = '".Tools::getValue('assegnaimpiegatom')."'");
                        $msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione del messaggio numero ".Tools::getValue('id_messaggio_univoco')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect e l'ha passata a ".$impiegato_assegnato;
                                        
                        $params = array(
                            '{link}' => $linkimp,
                            '{firma}' => '',
                            '{msg}' => $m
                        );
                        
                        Mail::Send($this->context->language->id, 'msg_base', Mail::l('Gestione messaggio revocata', $this->context->language->id), 
                                $params, $mailimprev, NULL, NULL, NULL, NULL, NULL, _PS_MAIL_DIR_, true);		
                    }
                                    
                    $params = array(
                        '{link}' => $linkimp,
                        '{firma}' => '',
                        '{msg}' => $msgimp
                    );
                                    
                    if(Tools::getValue('assegnaimpiegatom') != 0 && Tools::getValue('assegnaimpiegatom') != $mio_id) {
                        Mail::Send($this->context->language->id, 'msg_base', Mail::l('Messaggio assegnato a te su Ezdirect', $this->context->language->id), 
                            $params, $mailimp, NULL, NULL, NULL, NULL, NULL, _PS_MAIL_DIR_, true);
                    }
                }

                // Correggere: questo andrebbe fatto prima dell'invio delle mail, altrimenti il link potrebbe essere sbagliato (va modificato se è stato fatto convertiTicket)
                $id_contact = Db::getInstance()->getValue('SELECT id_contact FROM '._DB_PREFIX_.'customer_thread WHERE id_customer_thread = '.Tools::getValue('id_mex'));

                if($id_contact != Tools::getValue('id_contact')){
                    Customer::convertiTicket(Tools::getValue('id_mex'), $id_contact, Tools::getValue('id_contact'));
                    Tools::redirectAdmin(self::$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&tab_name=actions&conf=4&token='.$this->token);
                }
                else
                    Tools::redirectAdmin(self::$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&tab_name=actions&azione=actions&viewmessage&id_mex='.Tools::getValue('id_mex').'&conf=4&token='.$this->token);                
           
            }

            // Invio nuovo messaggio e risposta (panel Cronologia) - Divisi i ticket dai messaggi (nella 1.4 erano insieme)
            if(Tools::isSubmit('submitMessage')) {

                // messaggio vuoto => errore
                if(Tools::getValue('messaggio') == '') 
                {
                    $precedente_incarico = Db::getInstance()->getValue("SELECT id_employee FROM "._DB_PREFIX_."customer_thread WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
                    $precedente_status = Db::getInstance()->getValue("SELECT status FROM "._DB_PREFIX_."customer_thread WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
            
                    // Update in carico a
                    Customer::cambiaIncaricoVeloce('messaggio', Tools::getValue('in_carico_a_m'), Tools::getValue('id_customer_thread'), $precedente_incarico, Customer::trovaSigla(Tools::getValue('id_customer_thread'),'messaggio'), Tools::getValue('id_customer'));
                    
                    if($precedente_status != Tools::getValue('statusm'))
                    {	
                        switch(Tools::getValue('statusm'))
                        {
                            case 'open': $switch_status = 3; break;
                            case 'pending1': $switch_status = 4; break;
                            case 'closed': $switch_status = 5; break;
                            default: $switch_status = 10; break;
                        }	
                        
                        Customer::Storico(Tools::getValue('id_customer_thread'), 'T', $mio_id, $switch_status);

                        // Update status, date_upd
                        Db::getInstance()->execute("UPDATE "._DB_PREFIX_."customer_thread SET status = '".Tools::getValue('statusm')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = ".Tools::getValue('id_customer_thread')."");
                    }

                    Tools::redirectAdmin(self::$currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&tab_name=actions&error=1ms&token='.$this->token);
                }

                // risposta
                if(Tools::getValue('isAnswer') == 1) 
                {
                    $primaqueryct = ("SELECT id_customer_thread, token FROM "._DB_PREFIX_."customer_thread WHERE id_customer = $_POST[id_customer] AND id_customer_thread = $_POST[id_customer_thread]");
                    $primarowct = Db::getInstance()->getRow($primaqueryct);

                    $id_customer_thread = $primarowct['id_customer_thread'];
                    $newtoken = $primarowct['token'];
                    
                    $subj = Mail::l('An answer to your message is available', (int)$ct->id_lang);
                    
                    $precedente_status = Db::getInstance()->getValue("SELECT status FROM "._DB_PREFIX_."customer_thread WHERE id_customer_thread = ".$id_customer_thread."");
                    if($precedente_status != Tools::getValue('statusm'))
                    {	
                        switch(Tools::getValue('statusm'))
                        {
                            case 'open': $switch_status = 3; break;
                            case 'pending1': $switch_status = 4; break;
                            case 'closed': $switch_status = 5; break;
                            default: $switch_status = 10; break;
                        }	
                        
                        Customer::Storico($id_customer_thread, 'T', $mio_id, $switch_status);
                    }
                    
                    $queryinserimentomsg = ("UPDATE "._DB_PREFIX_."customer_thread SET status = '".Tools::getValue('statusm')."', date_upd = '".date("Y-m-d H:i:s")."', id_employee = '".Tools::getValue('in_carico_a_m')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = ".$id_customer_thread."");
                    Db::getInstance()->execute($queryinserimentomsg);
                }
                // nuovo messaggio
                else
                {
                    $queryct = ("SELECT id_customer_thread FROM "._DB_PREFIX_."customer_thread ORDER BY id_customer_thread DESC");
                    $rowct = Db::getInstance()->getRow($queryct);

                    $id_customer_thread = $rowct['id_customer_thread']+1;
                    $newtoken = Tools::passwdGen(12);

                    $subj = "Messaggio da parte di Ezdirect";
                    
                    switch(Tools::getValue('id_contact')) {
                        case 2: $titolomessaggio = "Ticket per amministrazione - contabilita"; break;
                        case 3: $titolomessaggio = "Ticket rivenditori"; break;
                        case 4: $titolomessaggio = "Ticket per assistenza"; break;
                        case 5: $titolomessaggio = "Ticket servizio clienti e ufficio vendite"; break;
                        case 6: $titolomessaggio = "Ticket ti richiamiamo noi"; break;
                        case 7: $titolomessaggio = "Messaggio semplice"; break;
                        case 8: $titolomessaggio = "Ticket per amministrazione - ordini"; break;
                        default: $titolomessaggio = "Messaggio da modulo di contatto"; break;
                    }

                    $cstmr_r = ($customer->is_company ? $customer->company : $customer->firstname." ".$customer->lastname);
                    
                    $subject = "*** MESSAGGIO da ".$cstmr_r." ***";

                    // inserimento in ps_customer_thread
                    $queryinserimentomsg = ("INSERT INTO "._DB_PREFIX_."customer_thread (id_customer_thread, id_lang, id_contact, id_customer, id_employee, subject, id_order, id_product, status, email, riferimento, token, date_add, date_upd, note) VALUES ('".$id_customer_thread."', '".$this->context->language->id."',  '7', '".Tools::getValue('id_customer')."', '".$mio_id."', '".addslashes($subject)."', '$_POST[id_order]', '$_POST[id_product]', 'open', '$_POST[customer_email_msg_m]', '".Tools::getValue('riferimento')."', '".$newtoken."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."', '".addslashes(Tools::getValue('note'))."')");
                    Db::getInstance()->execute($queryinserimentomsg);
                }

                $ct = new CustomerThread($id_customer_thread);
                $cm = new CustomerMessage();
                
                if(Tools::getIsset('chiudi_padre'))
                    Customer::chiudiPadre(Db::getInstance()->getValue('SELECT riferimento FROM '._DB_PREFIX_.'customer_thread WHERE id_customer_thread = '.$id_customer_thread));
                
                if(Tools::getIsset('riferimento'))
                    Db::getInstance()->execute("UPDATE "._DB_PREFIX_."customer_thread SET riferimento = '".Tools::getValue('riferimento')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = ".$id_customer_thread."");

                if(Tools::getIsset('riferimentoo') && Tools::getValue('riferimentoo') != '')
                {
                    if(substr(Tools::getValue('riferimentoo'),0,1) == 'C') {
                        $note_da_copiare = Db::getInstance()->executeS('SELECT * FROM cart_note WHERE id_cart = "'.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo'))).'"');
                    }
                    else if(substr(Tools::getValue('riferimentoo'),0,1) == 'O') {
                        $note_da_copiare = Db::getInstance()->executeS('SELECT * FROM cart_note WHERE id_cart = "'.Db::getInstance()->getValue('SELECT id_cart FROM orders WHERE id_order = '.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo')))).'"');
                    }
                    else { 
                        $note_da_copiare = Db::getInstance()->executeS('SELECT * FROM note_attivita WHERE id_attivita = "'.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo'))).'" AND tipo_attivita = "'.substr(Tools::getValue('riferimentoo'),0,1).'"');
                    }

                    foreach($note_da_copiare as $ndc) {
                        Db::getInstance()->execute('INSERT INTO note_attivita (id_note, id_attivita, tipo_attivita, id_employee, note, date_add, date_upd) VALUES (NULL, '.$id_customer_thread.', "T", "'.$ndc['id_employee'].'","'.$ndc['note'].'", "'.$ndc['date_add'].'","'.$ndc['date_upd'].'")');
                    }
                }

                $getstatus = Tools::getValue('status');

                if(!empty($getstatus)) 
                {
                    $precedente_status = Db::getInstance()->getValue("SELECT status FROM "._DB_PREFIX_."customer_thread WHERE id_customer_thread = ".$id_customer_thread."");
                    if($precedente_status != Tools::getValue('status'))
                    {	
                        switch(Tools::getValue('status'))
                        {
                            case 'open': $switch_status = 3; break;
                            case 'pending1': $switch_status = 4; break;
                            case 'closed': $switch_status = 5; break;
                            default: $switch_status = 10; break;
                        }	
                        
                        Customer::Storico($id_customer_thread, 'T', $mio_id, $switch_status);
            
                    }
                
                    Db::getInstance()->execute("UPDATE "._DB_PREFIX_."customer_thread SET status = '".Tools::getValue('status')."', priorita = '".Tools::getValue('priorita')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_customer_thread = ".$id_customer_thread."");
                }

                $cm->id_employee = (int)$mio_id;
                $cm->id_customer_thread = $ct->id;
                $cm->message = Tools::htmlentitiesutf8(Tools::getValue('messaggio'));
                $cm->ip_address = ip2long($_SERVER['REMOTE_ADDR']);

                // allegati
                $files=$_FILES["joinFile"]; // eliminare?

                $files=array();
                $fdata=$_FILES['joinFile'];

                if(is_array($fdata['name'])) {
                    $countf_name = count($fdata['name']);
                    for($i=0;$i<$countf_name;++$i){
                        $files[]=array(
                            'name'    =>$fdata['name'][$i],
                            'tmp_name'=>$fdata['tmp_name'][$i],
                            'type' => $fdata['type'][$i],
                            'size' => $fdata['size'][$i],
                            'error' => $fdata['error'][$i],
                        );
                    }
                }
                else 
                    $files[]=$fdata;
                                    
                $attachments = array();
                
                if(Tools::getIsset('invio_con_bdl_pdf')) {
                    require_once('../classes/html2pdf/html2pdf.class.php'); // correggere
                    $content = $this->generatePDFBDL(Tools::getValue('invio_con_bdl_pdf'), 'ezdirect');
                    
                    $html2pdf = new HTML2PDF('P','A4','it');
                    $html2pdf->WriteHTML($content);
                    
                    $pdfdoc = $html2pdf->Output('', true);

                    $fileAttachment1['content'] = $pdfdoc;
                    $fileAttachment1['name'] = Tools::getValue('invio_con_bdl_pdf').'-buono-di-lavoro.pdf';
                    $fileAttachment1['mime'] = 'application/x-download';
                    
                    $filename = "BDL".Tools::getValue('invio_con_bdl_pdf').":::".Tools::getValue('invio_con_bdl_pdf').'-buono-di-lavoro.pdf';
                    $file_name .= $filename.";";
                
                    $attachments[] = $fileAttachment1;
                }
                
                foreach($files as $file) 
                {
                    if (isset($files) AND !empty($file['name']) AND $file['error'] != 0)
                        $this->_errors[] = Tools::displayError('An error occurred with the file upload.');
                    
                    if (!empty($file['name'])) {
                        $extension = array('.txt', '.rtf', '.doc', '.docx', '.pdf', '.zip', '.png', '.jpeg', '.gif', '.jpg');
                        $filename = md5(uniqid().substr($file['name'], -5));
                        $fileAttachment['content'] = file_get_contents($file['tmp_name']);
                        $fileAttachment['name'] = $file['name'];
                        $fileAttachment['mime'] = $file['type'];
                    }

                    if (isset($filename) AND rename($file['tmp_name'], _PS_MODULE_DIR_.'../upload/'.$filename)) {
                        $filename = $filename.":::".$file['name'];
                        $file_name .= $filename.";";
                    }
                        
                    $attachments[] = $fileAttachment;
                }
                
                $cm->file_name = $file_name;	
                
                // inserimento in ps_customer_message
                if ($cm->add())
                {
                    $idmessaggio_ticket = Db::getInstance()->Insert_ID();

                    Db::getInstance()->execute("UPDATE "._DB_PREFIX_."customer_message SET note_private = '".addslashes(Tools::getValue('note_private'))."', in_carico = '".Tools::getValue('in_carico_a_m')."', email = '".Tools::getValue('customer_email_msg_m')."' WHERE id_customer_message = ".$idmessaggio_ticket."");

                    $sigla = "M";
                    
                    $anno = $ct->date_add;
                    $anno = substr($anno,2,2);
                    
                    $id_thread_ticket = $ct->id;
                    $id_ticket = $sigla.$anno.$id_thread_ticket;
                    $numerounivoco_ticket = $sigla.$anno.$id_thread_ticket.$idmessaggio_ticket;
                    
                    $primo_ticket = Db::getInstance()->getValue("SELECT id_customer_message FROM "._DB_PREFIX_."customer_message WHERE id_customer_thread = ".$id_thread_ticket." ORDER BY id_customer_message ASC");
                    
                    if(!$primo_ticket) {
                        $primo_ticket = $idmessaggio_ticket;
                    }
                    
                    $cfpicr = Db::getInstance()->getValue("SELECT id_customer FROM "._DB_PREFIX_."customer WHERE id_customer = ".$ct->id_customer."");
                    $addretc = Db::getInstance()->getValue("SELECT id_address FROM "._DB_PREFIX_."address WHERE fatturazione = 1 AND active = 1 AND deleted = 0 AND id_customer = ".$ct->id_customer."");
                    
                    $in_carico_a =  Db::getInstance()->getValue("SELECT firstname FROM "._DB_PREFIX_."employee WHERE id_employee = ".Tools::getValue('in_carico_a_m')."");
                    
                    $clixcsv = new Customer($cfpicr);
                    $addxcsv = new Address($addretc);
                    
                    // inutilizzato? correggere o eliminare
                    // $provinciaxcsv = Db::getInstance()->getRow("SELECT iso_code FROM state WHERE id_state = ".$addxcsv->id_state);
                    
                    $primo_ticket = $sigla.$anno.$primo_ticket;
                    $messaggioclientepercsv = Tools::getValue('message');
                    $messaggioclientepercsv = strip_tags($messaggioclientepercsv);
                    $messaggioclientepercsv = str_replace(";", ",", $messaggioclientepercsv);
                    
                    $messaggioclientepercsv = str_replace(chr(10), " ", $messaggioclientepercsv); //remove carriage returns
                    $messaggioclientepercsv = str_replace(chr(13), " ", $messaggioclientepercsv); //remove carriage returns	
                    
                    $messaggioclientepercsv2 = explode("Cordiali saluti / Best regards", $messaggioclientepercsv);
                    
                    $messaggioclientepercsv = $messaggioclientepercsv2[0];
                    
                    $employeeincaricato = new Employee($mio_id);
                    $employeeincaricato2 = new Employee(Tools::getValue('in_carico_a_m'));

                    // CORREGGERE: DECOMMENTARE QUANDO FUNZIONANO I FILE
                    /*
                    $ref = rand(0, 99999); 
                    $ref2 = rand(0, 99999);
                    
                    $file_csv=fopen("../cms/quotazioni/messaggi.csv","a+");
                    if(Tools::getValue('isAnswer') == 1) {
                        $riga_richiesta = "".date("d/m/Y H:i:s")." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;;;;;;;".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;Accettata;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;R;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
                    }
                    else {
                        $riga_richiesta = "".date("d/m/Y H:i:s")." GMT+1;".date('Ymd')."-".$ref."-".$ref2.";;;".$clixcsv->firstname.";".$clixcsv->lastname.";".$addxcsv->address1.";".$addxcsv->postcode.";".$addxcsv->city.";".$provincia.";".$addxcsv->phone.";".$addxcsv->fax.";".$clixcsv->email.";".$clixcsv->vat_number.";".$clixcsv->tax_code.";;;;Accettata;".$messaggioclientepercsv.";$idmessaggio_ticket;$id_thread_ticket;$numerounivoco_ticket;$primo_ticket;postvendita;D;$id_ticket;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
                    }
                    @fwrite($file_csv,$riga_richiesta);
                    @fclose($file_csv);	
                    */
                    
                    // risposta
                    if(Tools::getValue('isAnswer') == 1) {
                        // correggere: decommentare link giusto (manca contact-form.php)
                        $params = array(
                            '{reply}' => (Tools::getIsset('mail_solo_testo') ? strip_tags($gentilecliente.Tools::getValue('messaggio')) : $gentilecliente.Tools::getValue('messaggio')),
                            '{msg}' => (Tools::getIsset('mail_solo_testo') ? strip_tags($gentilecliente.Tools::getValue('messaggio')) : $gentilecliente.Tools::getValue('messaggio')),
                            '{firma}' => $firma,
                            '{id_richiesta}' => "Id richiesta: <strong>".$id_ticket."</strong>",
                            //'{link}' => $link->getPageLink('contact-form.php', true).'?id_customer_thread='.(int)($ct->id).'&token='.$ct->token
                        );
                    }
                    // nuovo messaggio
                    else {
                        // correggere: decommentare link giusto (manca contact-form.php)
                        $params = array(
                            '{reply}' => (Tools::getIsset('mail_solo_testo') ? strip_tags($gentilecliente.Tools::getValue('messaggio')) : $gentilecliente.Tools::getValue('messaggio')),
                            '{msg}' => (Tools::getIsset('mail_solo_testo') ? strip_tags($gentilecliente.Tools::getValue('messaggio')) : $gentilecliente.Tools::getValue('messaggio')),
                            '{firma}' => $firma,
                            '{id_richiesta}' => "Id richiesta: <strong>".$id_ticket."</strong>",
                            //'{link}' => $link->getPageLink('contact-form.php', true).'?id_customer_thread='.(int)($ct->id).'&token='.$ct->token
                        );
                    }
                    
                    $in_carico_a =  Db::getInstance()->getValue("
                        SELECT firstname 
                        FROM "._DB_PREFIX_."employee 
                        WHERE id_employee = ".Tools::getValue('in_carico_a_m')."
                    ");

                    if(empty($in_carico_a)) 
                        $in_carico_a = "Nessuno";
            
                    if(Tools::getValue('isMail') == 1) {
                        $mails_message = explode(";", Tools::getValue('customer_email_msg_m'));
                    
                        foreach ($mails_message as $mail)
                        {
                            $mail = 'carolina.carusi@ezdirect.it'; // TEST! DECOMMENTARE QUELLO GIUSTO
                            //$mail = trim($mail);
                            Customer::findEmailPersona($mail, Tools::getValue('id_customer'));

                            if($is_supplier == 1) 
                            {
                                if (Mail::Send($this->context->language->id, 'risposta_fornitore', Mail::l('Messaggio da Ezdirect', $this->context->language->id), 
                                    $params, $mail, NULL, 'info@ezdirect.it', 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true)) 
                                {
                                    // Commentato 1.4
                                    /*
                                    $ct->status = 'closed';
                                    $ct->update();
                                    $mail_scrivente = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = ".$mio_id."");
                                    
                                    Mail::Send($this->context->language->id, 'msg_base', Mail::l('Copia messaggio inviato al cliente', $this->context->language->id), 
                                    $params, $mail_scrivente, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
                                    _PS_MAIL_DIR_, true);
                                    */

                                    Customer::Storico($id_thread_ticket, 'T', $mio_id, 7);
                                    // CORREGGERE: DECOMMENTARE QUANDO FUNZIONANO I FILE
                                    // $log_mail = fopen("../import/log-mail.txt","a+");
                                    // $riga_log = "MSG | Mail inviata a ".$mail." in data ".date("Y-m-d H:i:s")."\n";
                                    // @fwrite($log_mail,$riga_log);
                                    // @fclose($log_mail);
                                }
                            }
                            else 
                            {
                                // correggere: sostituire 'msg_base' con 'simple_msg' e il 2° NULL con 'nonrispondere@ezdirect.it' dopo i test
                                if (Mail::Send($this->context->language->id, 'msg_base', Mail::l('Messaggio da Ezdirect', $this->context->language->id), 
                                    $params, $mail, NULL, NULL, 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true))
                                {
                                    // Commentato 1.4
                                    /*
                                    $ct->status = 'closed';
                                    $ct->update();
                                    $mail_scrivente = Db::getInstance()->getValue("SELECT email FROM employee WHERE id_employee = ".$mio_id."");
                                    
                                    Mail::Send($this->context->language->id, 'simple_msg', Mail::l('Copia messaggio inviato al cliente', $this->context->language->id), 
                                    $params, $mail_scrivente, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
                                    _PS_MAIL_DIR_, true);
                                    */
                                    
                                    Customer::Storico($id_thread_ticket, 'T', $mio_id, 7);
                                    // CORREGGERE: DECOMMENTARE QUANDO FUNZIONANO I FILE
                                    // $log_mail = fopen("../import/log-mail.txt","a+");
                                    // $riga_log = "MSG | Mail inviata a ".$mail." in data ".date("Y-m-d H:i:s")."\n";
                                    // @fwrite($log_mail,$riga_log);
                                    // @fclose($log_mail);
                                }
                            }
                        }
                        
                        $cc = "";
                        $cc_str = "";

                        foreach($_POST['conoscenza_m'] as $conoscenza) {
                            $cc_str = $cc_str.Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$conoscenza).' ';
                        }
                            
                        foreach($_POST['conoscenza_m'] as $conoscenza) 
                        {
                            $tokenimp = Tools::getAdminToken("AdminCustomers".(int)Tab::getIdFromClassName("AdminCustomers").$conoscenza);
                            $linkimp = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&tab_name=actions&azione=actions&viewmessage&id_mex=".$ct->id."&token=".$tokenimp;	
                            
                            // correggere: decommentare link giusto (manca contact-form.php)
                            $params4 = array(
                                '{reply}' => "<strong>".$employeemess->firstname."</strong> ha inviato una comunicazione al cliente e ti ha messo in copia conoscenza. <br /><br />
                                Id messaggio: <strong>".$id_ticket."</strong><br /><br />
                                Cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id."<br /><br />
                                Il messaggio &egrave; in carico a: <strong>".$in_carico_a."</strong><br /><br />	
                                ".($cc_str != '' ? "Il messaggio &egrave; stato inviato in conoscenza a: ".$cc_str."<br /><br />" : "")."
                                <a href='".$linkimp."'>Per aprire il messaggio fai clic qua</a>.",
                                //'{link}' => $link->getPageLink('contact-form.php', true).'?id_customer_thread='.(int)($ct->id).'&token='.$ct->token
                            );
                            
                            $cc .= $conoscenza.";";

                            $mailconoscenza = 'carolina.carusi@ezdirect.it'; // TEST! DECOMMENTARE QUELLO GIUSTO
                            //$mailconoscenza = Db::getInstance()->getValue("SELECT email FROM "._DB_PREFIX_."employee WHERE id_employee = ".$conoscenza."");
                            
                            Mail::Send($this->context->language->id, 'senzagrafica', Mail::l('Messaggio cliente in copia conoscenza', $this->context->language->id), 
                            $params4, $mailconoscenza, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
                            _PS_MAIL_DIR_, true);
                        }
                        
                        if(!empty($_POST['conoscenza_m'])) {
                            Db::getInstance()->execute("INSERT INTO ticket_cc (msg, type, cc) VALUES (".$idmessaggio_ticket.", 't', '".$cc."')");
                        }
                    }

                    Tools::redirectAdmin(self::$currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&tab_name=actions&conf=4&token='.$this->token);
                }
                else
                    $this->_errors[] = Tools::displayError('An error occurred, your message was not sent. Please contact your system administrator.');
            }

            $action_types = array(
                array(
                    "value" => "4",
                    "name" => "Assistenza tecnica"
                ),
                array(
                    "value" => "2",
                    "name" => "Assistenza amministrativa - contabilit&agrave;"
                ),
                array(
                    "value" => "8",
                    "name" => "Assistenza amministrativa - ordini"
                ),
                array(
                    "value" => "3",
                    "name" => "Rivenditori"
                ),
                array(
                    "value" => "preventivo",
                    "name" => "Richiesta di preventivo"
                ),
                array(
                    "value" => "tirichiamiamonoi",
                    "name" => "Ti richiamiamo noi"
                ),
                array(
                    "value" => "7",
                    "name" => "Messaggio"
                ),
            );

            $precompilati = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'precompilato WHERE active = 1');

            if(Tools::getIsset('viewpreventivo')){

                if(!Tools::getIsset('conf') && !Tools::getIsset('filename'))
					Customer::Storico(Tools::getValue('id_thread'), 'P', $mio_id, 14);

                if(isset($_GET['filename'])){
                    $filename = $_GET['filename'];
                    
                    if(strpos($filename, ":::")) {
                        $parti = explode(":::", $filename);
                        $nomecodificato = $parti[0];
                        $nomevero = $parti[1];
                    }
                    else {
                        $nomecodificato = $filename;
                        $nomevero = $filename;
                    }
                    
                    if (file_exists(_PS_UPLOAD_DIR_.$nomecodificato)) {
                        self::openUploadedFile();
                    }
                }

                if(isset($_GET['reinvia'])){
                    Customer::Storico(Tools::getValue('id_thread'), 'P', $mio_id, 12);
					
                    $values = Db::getInstance()->getRow("SELECT * FROM form_prevendita_message fm JOIN form_prevendita_thread ft ON ft.id_thread = fm.id_thread WHERE fm.id_message = '".Tools::getValue('reinvia')."'");
                
                    $anno = date('Y');
                    $anno = substr($anno, 2,2);
                    $sigla = "P";
                    $idrichiesta = $sigla.$anno.$values['id_thread'];

                    // correggere: decommentare link giusto (manca modulo formprevendita)
                    $params = array(
                        '{reply}' => $gentilecliente.$values['message'],
                        '{firma}' => $firma,
                        '{id_richiesta}' => "Id richiesta: <strong>".$idrichiesta."</strong>",
                        //'{link}' => $link->getPageLink('modules/formprevendita/form.php', true).'?id_thread='.$values['id_thread'].'&token='.$values['token']
                        '{link}' => 'prova_link'
                    );

                    $attachments = array();
                        
                    $files = explode(";", $values['file_name']);

                    foreach($files as $file) {
                        $parti = explode(":::", $file);
                        $fileAttachment['content'] = file_get_contents(_PS_MODULE_DIR_.'../upload/'.$parti[0]);
                        $fileAttachment['name'] = $parti[1];	
                        $extensions = array('.txt' => 'text/plain', '.rtf' => 'application/rtf', '.doc' => 'application/msword', '.docx'=> 'application/msword',
                        '.pdf' => 'application/pdf', '.zip' => 'multipart/x-zip', '.png' => 'image/png', '.jpeg' => 'image/jpeg', '.gif' => 'image/gif', '.jpg' => 'image/jpeg');

                        $extension = '';
                        foreach ($extensions AS $key => $val)
                        if (substr($parti[1], -4) == $key OR substr($parti[1], -5) == $key)
                        {
                            $extension = $val;
                            break;
                        }
                        $fileAttachment['mime'] = $extension;
                        $attachments[] = $fileAttachment;			
                    }	

                    Customer::findEmailPersona($values['email'], $customer->id);

                    if($is_supplier == 1) {
                        // correggere: sostituire 'msg_base' con 'risposta_fornitore' e il 2° NULL con 'info@ezdirect.it' e 'carolina.carusi@ezdirect.it' con $values['email'] dopo test
                        if (Mail::Send($this->context->language->id, 'msg_base', Mail::l('Messaggio da Ezdirect', $this->context->language->id), 
                            $params, 'carolina.carusi@ezdirect.it', NULL, NULL, 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true)){

                            Customer::Storico($values['id_thread'], 'P', $mio_id, 7);
                            
                            // Commentato 1.4
                            //$ct->status = 'closed';
                            //$ct->update();

                            $mail_scrivente = Db::getInstance()->getValue("
                                SELECT email 
                                FROM "._DB_PREFIX_."employee 
                                WHERE id_employee = ".$mio_id."
                            ");

                            // Decommentare quando funzionano i file
                            //$log_mail = fopen("../import/log-mail.txt","a+");
                            $riga_log = "PRV | Mail inviata a ".$values['email']." in data ".date("Y-m-d H:i:s")."\n";
                            //@fwrite($log_mail,$riga_log);
                            //@fclose($log_mail);
                        }
                    }
                    else {
                        // correggere: sostituire 'msg_base' con 'reply_msg' e il 2° NULL con 'nonrispondere@ezdirect.it' e 'carolina.carusi@ezdirect.it' con $values['email'] dopo test
                        if (Mail::Send((int)$this->context->language->id, 'msg_base', Mail::l('Messaggio da Ezdirect', (int)$this->context->language->id), 
                            $params, 'carolina.carusi@ezdirect.it', NULL, NULL, 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true)) {

                            Customer::Storico($values['id_thread'], 'P', $mio_id, 7);

                            // Commentato 1.4
                            //$ct->status = 'closed';
                            //$ct->update();

                            $mail_scrivente = Db::getInstance()->getValue("
                                SELECT email 
                                FROM "._DB_PREFIX_."employee 
                                WHERE id_employee = ".$mio_id."
                            ");
                            
                            // Commentato 1.4
                            /*Mail::Send($this->context->language->id, 'simple_msg', Mail::l('Copia preventivo inviato al cliente', 5), 
                                $params, $mail_scrivente, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
                                _PS_MAIL_DIR_, true);*/
                                
                            // Decommentare quando funzionano i file
                            //$log_mail = fopen("../import/log-mail.txt","a+");
                            $riga_log = "PRV | Mail inviata a ".$values['email']." in data ".date("Y-m-d H:i:s")."\n";
                            //@fwrite($log_mail,$riga_log);
                            //@fclose($log_mail);
                        }
                    }
                    
                    Tools::redirectAdmin(self::$currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&tab_name=actions&conf=4&token='.$this->token);
                }

                if(Tools::getIsset('id_thread')) {

                    $errore = false;
                    $thread = Db::getInstance()->getRow("SELECT * FROM form_prevendita_thread ct WHERE ct.id_thread = ".Tools::getValue('id_thread')." AND ct.id_customer = ".$customer->id."");
                    
                    if(!$thread && Tools::getValue('id_thread') != '') {
                        $errore = true;
                    }
                    else {

                        $preventivo = Db::getInstance()->getRow("SELECT * FROM form_prevendita_thread ft WHERE ft.id_thread = ".Tools::getValue('id_thread')."");
                        
                        $tipo_richiesta_p = $preventivo['tipo_richiesta']; // preventivo o tirichiamiamonoi
                        $action_type = $tipo_richiesta_p;

                        $incaricato = new Employee($preventivo['id_employee']);
                        $in_carico_a_firstname = $incaricato->firstname;

                        $annoprev = substr($preventivo['date_add'],2,2);
                        
                        $query_fpm = Db::getInstance()->getRow("
                            SELECT date_add, message 
                            FROM form_prevendita_message
                            WHERE id_thread = ".Tools::getValue('id_thread')." 
                            ORDER BY date_add DESC
                        ");

                        switch($preventivo['status']) {
                            case 'open': 
                                $status_preventivo = "<i class='icon-circle rosso' title='Aperto'></i>";
                                $st_prev = 'Aperto';
                                break;
                            case 'closed': 
                                $status_preventivo = "<i class='icon-circle' style='color:green' title='Chiuso'></i>";
                                $st_prev = 'Chiuso';
                                break;
                            case 'pending1': 
                                $status_preventivo = "<i class='icon-circle' style='color:orange' title='In lavorazione'></i>";
                                $st_prev = 'In lavorazione';
                                break;
                            default: 
                                $status_preventivo = "<i class='icon-circle rosso' title='Aperto'></i>";
                                $st_prev = 'Aperto';
                                break;
                        }
    
                        $status_azione = $status_preventivo;
                        $st_azione = $st_prev;
    
                        switch ($preventivo['tipo_richiesta']) {
                            case 'preventivo':
                                $sigla = 'P';
                                break;
                            case 'tirichiamiamonoi':
                                $sigla = 'R';
                                break;
                            
                            default:
                                break;
                        }
    
                        $idprevunivoco = $sigla.$annoprev.$preventivo['id_thread'];

                        /* Generale */
                        $oggetto =  $preventivo['subject'];                        
                        if($oggetto == '')
                            $oggetto = $query_fpm['message'];

                        $aperto_da = Db::getInstance()->getValue('SELECT id_employee FROM form_prevendita_message WHERE id_thread = '.Tools::getValue('id_thread').' ORDER BY id_message ASC');
						
						if($aperto_da != 0)
							$aperto_da = Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$aperto_da);
						else
							$aperto_da = 'Cliente';

                        $data_apertura = Tools::displayDate($preventivo['date_add'], (int)($this->context->language->id), true);

                        $dataultimo = $query_fpm['date_add'];
                        $data_ultima_com = Tools::displayDate($dataultimo, (int)($this->context->language->id), true);

                        if(is_numeric($preventivo['category']))
							$cp_interest = Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'product_lang WHERE id_lang = '.$this->context->language->id.' AND id_product = '.$preventivo['category'].'');
						else
							$cp_interest = $preventivo['category'];

                        if($preventivo['riferimento'] != '')
                            $azione_padre = Customer::trovaSiglaLinkPerAlbero(substr($preventivo['riferimento'],1), substr($preventivo['riferimento'],0,1), '', 'y:P:'.$preventivo['id_thread'].':'.$preventivo['status']);
                        
                        // Note private
						$preventivo_note = Db::getInstance()->executeS('SELECT * FROM note_attivita WHERE tipo_attivita = "P" AND id_attivita = '.$preventivo['id_thread'].'');
                        foreach($preventivo_note as &$preventivo_nota)
                            $preventivo_nota['creato_da'] = Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$preventivo_nota['id_employee']);

                        // Vecchia nota
                        if($preventivo['date_add'] < '2017-12-08 00:00:00'){
                            $preventivo_old = true;
                            $preventivo_nota = Tools::htmlentitiesUTF8($preventivo['note']);
                        }
                        else
                            $preventivo_old = false;

                        $collega_a = Customer::BuildSelectForLinking($customer->id, $preventivo['id_thread'], 'P');

                        $generale = array(
                            'azione' => $preventivo,
                            'tipo_richiesta_p' => $tipo_richiesta_p,
                            'idunivoco' => $idprevunivoco,
                            'status_azione' => $status_azione,
                            'st_azione' => $st_azione,
                            'oggetto' => $oggetto,
                            'aperto_da' => $aperto_da,
                            'in_carico_a_options' => $impiegati_eza,
                            'in_carico_a_firstname' => $in_carico_a_firstname,
                            'data_apertura' => $data_apertura,
                            'data_ultima_com' => $data_ultima_com,
                            'cp_interest' => $cp_interest,
                            'azione_padre' => $azione_padre,
                            'azione_note' => $preventivo_note,
                            'preventivo_old' => $preventivo_old,
                            'preventivo_nota' => $preventivo_nota,
                            'collega_a' => $collega_a,
                            'errore' => $errore,
                        );
                        /* Fine Generale */

                        /* Cronologia */
					    $messaggip = Db::getInstance()->executeS("SELECT * FROM form_prevendita_message fm WHERE fm.id_thread = ".Tools::getValue('id_thread')."");
                        
                        foreach($messaggip as &$messaggiop) {

                            if ($messaggiop['id_employee'] == 0) 
                                $messaggiop['da'] = $customer->firstname." ".$customer->lastname;
                            else {
                                $employeezp = new Employee($messaggiop['id_employee']);
                                $messaggiop['da'] = $employeezp->firstname." ".substr($employeezp->lastname,0,1).".";
                            }
                                
                            $messaggiop['data'] = date("d-m-Y H:i:s",strtotime($messaggiop['date_add']));

                            if(!empty($messaggiop['file_name'])) {
                                $allegatip = explode(";",$messaggiop['file_name']);
                                $nallm = 1;
                                foreach($allegatip as $allegatop) {
                                    if(strpos($allegatop, ":::")) {
                                        $parti = explode(":::", $allegatop);
                                        $nomeverop = $parti[1];
                                    }
                                    else
                                        $nomeverop = $allegatop;
                                    
                                    if($allegatop == "") {
                                        // niente
                                    } 
                                    else {
                                        if($nallm != 1)
                                            $allegati .= '-';

                                        $allegati .= '<a href="ajax.php?openAllegato=y&id_customer='.$customer->id.'&viewcustomer&tab_name=actions&azione=actions&viewpreventivo&id_thread='.$preventivo['id_thread'].'&filename='.$allegatop.'&token='.$this->token.'"><span style="color:red">'.$nomeverop.'</span></a>';
                                        $nallm++;
                                    }
                                }
                            }
                            else
                                $allegati = 'Nessun allegato';
                                
                            $messaggiop['allegati'] = $allegati;

                            $messaggiop['message'] = htmlspecialchars_decode($messaggiop['message']);
                            $messaggiop['message_modifica'] = htmlentities($messaggiop['message'], ENT_QUOTES | ENT_SUBSTITUTE, "UTF-8");
                            
                            if($messaggiop['note_private'] != '')
                                $messaggiop['note_private'] = htmlspecialchars_decode(preg_replace('/(<[^>]+) style=".*?"/i', '$1', preg_replace('/(<[^>]+) class=".*?"/i', '$1', $messaggiop['note_private'])));
                            
                            if($messaggiop['email'] != '')
                                $messaggiop['employee_to_name'] = (is_numeric($messaggiop['email']) ? Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$messaggiop['email']) : htmlspecialchars_decode($messaggiop['email']));

                            $messaggiop['in_carico_a_name'] = ($messaggiop['in_carico'] > 0 ? Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$messaggiop['in_carico']) : '--');
                            
                            $cc = Db::getInstance()->getValue("SELECT cc FROM ticket_cc WHERE type = 'p' AND msg = ".$messaggiop['id_message']."");
                            if($cc) {
                                $ccs = explode(";",$cc);
                                
                                foreach ($ccs as $conoscenza) {
                                    $imp = Db::getInstance()->getValue("SELECT firstname FROM "._DB_PREFIX_."employee WHERE id_employee = '".$conoscenza."'");
                                    $cc_str .= $imp." ";
                                }
                                
                                $messaggiop['cc'] = $cc_str;
                            }
                            else {
                                $messaggiop['cc'] = 'Nessuno';
                            }

                            if($check_employee) {
                                $inoltra_count = 0;
                                $inoltra_string = '';

                                foreach ($impiegati_eza as $impez) {
                                    $inoltra_count++;                                
                                    $inoltra_string .= "<input type='checkbox' class='inoltrap' name='inoltrap[]' id='inoltra_".$impez['id_employee']."' value='".$impez['id_employee']."' /> ".$impez['firstname']." &nbsp;&nbsp;&nbsp;&nbsp;";
                                    if($inoltra_count == 8)
                                        $inoltra_string .= '<br />';
                                }

                                $inoltra_string .= '<br /><span id="inoltrap_response">';
                                $messaggiop['inoltra_string'] = $inoltra_string;
                            }
                        }

                        if(Tools::isSubmit('submitPreventivoReply')) {

                            $tipo_richiesta_p = Db::getInstance()->getValue("
                                SELECT tipo_richiesta 
                                FROM form_prevendita_thread 
                                WHERE id_thread = ".$preventivo['id_thread']."
                            ");

                            $files = array();
                            $fdata = $_FILES['joinFile'];
                            if(is_array($fdata['name'])){
                                $count_name = count($fdata['name']);
                                for($i=0;$i<$count_name;++$i){
                                    $files[] = array(
                                        'name'    =>$fdata['name'][$i],
                                        'tmp_name'=>$fdata['tmp_name'][$i],
                                        'type' => $fdata['type'][$i],
                                        'size' => $fdata['size'][$i],
                                        'error' => $fdata['error'][$i],
                                    );
                                }
                            }
                            else 
                                $files[] = $fdata;
                                                
                            $attachments = array();
                            
                            foreach($files as $file) 
                            {
                                if (isset($files) AND !empty($file['name']) AND $file['error'] != 0)
                                    $this->_errors[] = Tools::displayError('An error occurred with the file upload.');
                                    
                                if (!empty($file['name']))
                                {
                                        $extension = array('.txt', '.rtf', '.doc', '.docx', '.pdf', '.zip', '.png', '.jpeg', '.gif', '.jpg');
                                        $filename = md5(uniqid().substr($file['name'], -5));
                                        $fileAttachment['content'] = file_get_contents($file['tmp_name']);
                                        $fileAttachment['name'] = $file['name'];
                                        $fileAttachment['mime'] = $file['type'];
                                }
                                    
                                if (isset($filename) AND rename($file['tmp_name'], _PS_MODULE_DIR_.'../upload/'.$filename)) {
                                    $filename = $filename.":::".$file['name'];
                                    $file_name .= $filename.";";
                                }
                                
                                $attachments[] = $fileAttachment;
                            }
                                
                            if(Tools::getValue('messaggiop') == '') 
                            {
                                $precedente_status = Db::getInstance()->getValue("SELECT status FROM form_prevendita_thread WHERE id_thread = ".$preventivo['id_thread']."");
                                if($precedente_status != Tools::getValue('status'))
                                {	
                                    switch(Tools::getValue('status'))
                                    {
                                        case 'open': $switch_status = 3; break;
                                        case 'pending1': $switch_status = 4; break;
                                        case 'closed': $switch_status = 5; break;
                                        default: $switch_status = 10; break;
                                    }
                                    
                                    Customer::Storico($preventivo['id_thread'], 'P', $mio_id, $switch_status);
                                }
                                
                                $precedente_incarico = Db::getInstance()->getValue("SELECT id_employee FROM form_prevendita_thread WHERE id_thread = ".$preventivo['id_thread']."");
                                
                                if($precedente_incarico != 0) 
                                {
                                    $employee_pi = Db::getInstance()->getRow("SELECT email, firstname FROM "._DB_PREFIX_."employee WHERE id_employee = '".$precedente_incarico."'");
                                    $mailimprev = 'carolina.carusi@ezdirect.it'; // prova!
                                    //$mailimprev = $employee_pi['email']; // correggere: decommentare
                                    $nomeimprev = $employee_pi['firstname'];
                                    
                                    $employeeinc = Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.Tools::getValue('in_carico_a_p'));
                                    
                                    $msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione del ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." numero ".Tools::getValue('id_preventivo_univoco')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect e l'ha passata a ".$employeeinc.".";
                                            
                                    $params = array(
                                        '{msg}' => $msgimprev
                                    );
                                    
                                    // Invio una mail di revoca della gestione a $precedente_incarico, se non è l'employee loggato
                                    if($precedente_incarico != $mio_id)
                                    {
                                        Mail::Send($this->context->language->id, 'msg_base', Mail::l('Gestione '.($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo').' revocata', $this->context->language->id), 
                                            $params, $mailimprev, NULL, NULL, NULL, NULL, NULL, _PS_MAIL_DIR_, true);
                                    }
                                }

                                if($precedente_incarico != Tools::getValue('in_carico_a_p'))
                                    Customer::Storico($preventivo['id_thread'], 'P', $mio_id, 2, Tools::getValue('in_carico_a_p'));
                        
                                // Update status, id_employee, date_upd
                                Db::getInstance()->execute("UPDATE form_prevendita_thread SET status = '".Tools::getValue('status')."', id_employee = '".Tools::getValue('in_carico_a_p')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_thread = ".$preventivo['id_thread']."");

                                $this->assegna_incarico(Tools::getValue('in_carico_a_p'), $preventivo['id_thread'], 'preventivo', $customer);

                                Tools::redirectAdmin(self::$currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&tab_name=actions&error=1pv&token='.$this->token);
                            }
                            
                            // Correggere: Viene utilizzato? Non mi sembra
                            if(Tools::getValue('nuovopreventivo') == 1) 
                            {
                                $ultimothread = Db::getInstance()->getValue("SELECT id_thread FROM form_prevendita_thread ORDER BY id_thread DESC");
                                $thread = $ultimothread+1;
                                
                                $addresscli = Db::getInstance()->getValue("SELECT id_address FROM address WHERE fatturazione = 1 AND active = 1 AND deleted = 0 AND id_customer = ".$customer->id."");
                                
                                $addresscliente = new Address($addresscli);
                                $token = Tools::passwdGen(12);
                                
                                $cstmr = Db::getInstance()->getRow("SELECT company, firstname, lastname, is_company FROM customer WHERE id_customer = ".(int)($customer->id)."");
                                    
                                    
                                if($cstmr['is_company'] == 1) { $cstmr_r = $cstmr['company']; } else { $cstmr_r = $cstmr['firstname']." ".$cstmr['lastname']; }
                                
                                $subject = "*** RICHIESTA COMMERCIALE da ".$cstmr_r." - Tipo richiesta: Preventivo ***";
                                
                                
                                Db::getInstance()->ExecuteS("INSERT INTO form_prevendita_thread (id_thread, tipo_richiesta, id_customer, id_employee, subject, tax_code, vat_number, firstname, lastname, company, address1, postcode, state, country, email, phone, token, status, category, date_add, date_upd, note) VALUES ('$thread', 'preventivo', '".$customer->id."', '".Tools::getValue('in_carico_a_p')."', '".addslashes($subject)."', '".addslashes($customer->tax_code)."', '".addslashes($customer->vat_number)."', '".addslashes($customer->firstname)."', '".addslashes($customer->lastname)."', '".addslashes($customer->company)."', '".addslashes($addresscliente->address1)."', '".addslashes($addresscliente->postcode)."', '".addslashes($addresscliente->id_state)."', '".addslashes($addresscliente->id_country)."', '".addslashes($customer->email)."', '".addslashes($addresscliente->phone)."', '$token', '".addslashes(Tools::getValue('status'))."', '', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."', '".Tools::getValue('note')."')");   
                                
                                Db::getInstance()->ExecuteS("INSERT INTO form_prevendita_message (id_message, id_thread, id_customer, id_employee, in_carico, file_name, message, note_private, date_add, date_upd) VALUES (NULL, '$thread', ".$customer->id.", '".$mio_id."', '".Tools::getValue('in_carico_a_p')."', '".addslashes($file_name)."', '".addslashes(Tools::getValue('messaggiop'))."', '".addslashes(Tools::getValue('note_private'))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
                                
                                Customer::Storico($thread, 'P', $mio_id, 13);
                                                                
                                $sigla = 'P';
                                $idmessaggio = Db::getInstance()->Insert_ID();
                            }
                            else 
                            {
                                $token = $preventivo['token'];
                                $thread = $preventivo['id_thread'];
                                
                                $precedente_status = Db::getInstance()->getValue("SELECT status FROM form_prevendita_thread WHERE id_thread = ".$thread."");
                                if($precedente_status != Tools::getValue('status'))
                                {	
                                    switch(Tools::getValue('status'))
                                    {
                                        case 'open': $switch_status = 3; break;
                                        case 'pending1': $switch_status = 4; break;
                                        case 'closed': $switch_status = 5; break;
                                        default: $switch_status = 10; break;
                                    }	
                                    
                                    Customer::Storico($thread, 'P', $mio_id, $switch_status);
                                }
                                
                                $precedente_incarico = Db::getInstance()->getValue("SELECT id_employee FROM form_prevendita_thread WHERE id_thread = ".$thread."");
                                
                                // Correggere: esegue la funzione anche se i valori sono uguali!
                                if($precedente_incarico != Tools::getValue('in_carico_a_p'))
                                    Customer::Storico($thread, 'P', $mio_id, 2, Tools::getValue('in_carico_a_p'));
                            
                                Db::getInstance()->execute("UPDATE form_prevendita_thread SET  status = '".addslashes(Tools::getValue('status'))."', date_upd = '".date("Y-m-d H:i:s")."', id_employee = '".Tools::getValue('in_carico_a_p')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_thread = '".$thread."'");  
                                Db::getInstance()->execute("INSERT INTO form_prevendita_message (id_message, id_thread, id_customer, id_employee, in_carico, file_name, message, note_private, date_add, date_upd) VALUES (NULL, '".$thread."', ".$customer->id.", '".$mio_id."', '".Tools::getValue('in_carico_a_p')."','".addslashes($file_name)."', '".addslashes(Tools::getValue('messaggiop'))."', '".addslashes(Tools::getValue('note_private'))."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
                                                                
                                $idmessaggio = Db::getInstance()->Insert_ID();
                            }
                            
                            if(Tools::getIsset('chiudi_padre'))
                                Customer::chiudiPadre(Db::getInstance()->getValue('SELECT riferimento FROM form_prevendita_thread WHERE id_thread = '.$preventivo['id_thread']));
                            
                            if(Tools::getIsset('riferimentoo') && Tools::getValue('riferimentoo') != '')
                            {
                                if(substr(Tools::getValue('riferimentoo'),0,1) == 'C')
                                    $note_da_copiare = Db::getInstance()->executeS('SELECT * FROM cart_note WHERE id_cart = "'.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo'))).'"');
                                else if(substr(Tools::getValue('riferimentoo'),0,1) == 'O')
                                    $note_da_copiare = Db::getInstance()->executeS('SELECT * FROM cart_note WHERE id_cart = "'.Db::getInstance()->getValue('SELECT id_cart FROM '._DB_PREFIX_.'orders WHERE id_order = '.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo')))).'"');
                                else
                                    $note_da_copiare = Db::getInstance()->executeS('SELECT * FROM note_attivita WHERE id_attivita = "'.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo'))).'" AND tipo_attivita = "'.substr(Tools::getValue('riferimentoo'),0,1).'"');
                                
                                foreach($note_da_copiare as $ndc)
                                    Db::getInstance()->execute('INSERT INTO note_attivita (id_note, id_attivita, tipo_attivita, id_employee, note, date_add, date_upd) VALUES (NULL, '.$preventivo['id_thread'].', "P", "'.$ndc['id_employee'].'","'.$ndc['note'].'", "'.$ndc['date_add'].'","'.$ndc['date_upd'].'")');
                            }	
                            
                            Db::getInstance()->execute("UPDATE form_prevendita_message SET email = '".Tools::getValue('customer_email_msg_p')."' WHERE id_message = ".$idmessaggio."");
                        
                            if($preventivo['tipo_richiesta'] == 'preventivo')
                                $sigla = 'P';
                            else if($preventivo['tipo_richiesta'] == 'tirichiamiamonoi')
                                $sigla = 'R';
                            
                            $anno = date('Y');
                            $anno = substr($anno, 2, 2);
                            $numerounivoco = $sigla.$anno.$preventivo['id_thread'].$idmessaggio;
                                
                            $primoticket = Db::getInstance()->getValue("SELECT id_message FROM form_prevendita_message WHERE id_thread = ".$preventivo['id_thread']." ORDER BY id_message ASC");
                                    
                            if(!$primoticket)
                                $primoticket = $idmessaggio;

                            $primoticket = $sigla.$anno.$preventivo['id_thread'].$primoticket;
                            
                            // CORREGGERE: Decommentare quando funzionano i file
                            //$file_csv = fopen("../cms/quotazioni/servizio-clienti.csv","a+");
                            
                            $data_quotazione = date("d/m/Y H:i:s");
                            
                            $categoria = $preventivo['category'];
                            
                            if(is_numeric($categoria)) {
                                $intcat = "Prodotto di interesse";
                                $categoria =  Db::getInstance()->getValue("SELECT name FROM "._DB_PREFIX_."product_lang WHERE id_product = '$categoria' AND id_lang = ".$this->context->language->id);
                            }
                            else {
                                $intcat = "Categoria di interesse";
                                $categoria = $preventivo['category'];
                            }

                            $in_carico_a =  Db::getInstance()->getValue("SELECT firstname FROM "._DB_PREFIX_."employee WHERE id_employee = ".Tools::getValue('in_carico_a_p')."");
                            if(empty($in_carico_a))
                                $in_carico_a = "Nessuno";
                            
                            $messaggioclientepercsv = Tools::getValue('messaggiop');
                            $messaggioclientepercsv = strip_tags($messaggioclientepercsv);
                            $messaggioclientepercsv = str_replace(";", ",", $messaggioclientepercsv);
                                
                            $messaggioclientepercsv = str_replace(chr(10), " ", $messaggioclientepercsv); //remove carriage returns
                            $messaggioclientepercsv = str_replace(chr(13), " ", $messaggioclientepercsv); //remove carriage returns	
                            
                            $employeeincaricato = new Employee($mio_id);
                            $employeeincaricato2 = new Employee($preventivo['id_employee']);

                            if(Tools::getValue('nuovopreventivo') == 1)
                                $idrichiesta = $sigla.$anno.$thread;
                            else
                                $idrichiesta = $sigla.$anno.$preventivo['id_thread'];

                            $riga_richiesta = $categoria.";".$data_quotazione.";;;$preventivo[vat_number];$preventivo[tax_code];".$employeeincaricato->firstname.";".$employeeincaricato->lastname.";;;;;;;;".$messaggioclientepercsv.";$idmessaggio;$preventivo[id_thread];$numerounivoco;$primoticket;prevendita;R;$idrichiesta;".Tools::getValue('status').";".$employeeincaricato2->firstname." ".$employeeincaricato2->lastname."\n";
                            
                            if($tipo_richiesta_p == 'preventivo') {
                                //@fwrite($file_csv,$riga_richiesta); // Decommentare quando funzionano i file
                            }
                                    
                            //@fclose($file_csv); // Decommentare quando funzionano i file
                            
                            // correggere: decommentare link giusto (manca modulo formprevendita)
                            $params = array(
                                '{reply}' => $gentilecliente.(Tools::getValue('messaggiop')),
                                '{firma}' => $firma,
                                '{id_richiesta}' => "Id richiesta: <strong>".$idrichiesta."</strong>",
                                //'{link}' => $link->getPageLink('modules/formprevendita/form.php', true).'?id_thread='.$thread.'&token='.$token
                                '{link}' => 'prova_link'
                            );
                            
                            $params2 = array(
                            '{reply}' => $employeeincaricato->firstname." ".$employeeincaricato->lastname." ha aperto un ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." per conto di un cliente. L'id del preventivo &egrave;: ".$idrichiesta."");
                            
                            $params3 = array(
                            '{reply}' => $employeeincaricato->firstname." ".$employeeincaricato->lastname." ti ha assegnato un ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." su Ezdirect. L'id del ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." &egrave;: ".$idrichiesta." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.")");
                            
                            $mails_preventivi = explode(";", Tools::getValue('customer_email_msg_p'));
                                    
                            foreach ($mails_preventivi as $mail) {
                                $mail = trim($mail);
                                Customer::findEmailPersona($mail, Tools::getValue('id_customer'));

                                if (Mail::Send((int)$this->context->language->id, 'reply_msg', Mail::l('Messaggio da Ezdirect', (int)$this->context->language->id), 
                                    $params, $mail, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, _PS_MAIL_DIR_, true)) {

                                    Customer::Storico($preventivo['id_thread'], 'P', $mio_id, 7);

                                    // Commentato 1.4
                                    // $ct->status = 'closed';
                                    // $ct->update();

                                    $mail_scrivente = Db::getInstance()->getValue("
                                        SELECT email 
                                        FROM "._DB_PREFIX_."employee 
                                        WHERE id_employee = ".$mio_id."
                                    ");
                                    
                                    // Commentato 1.4
                                    /*Mail::Send($this->context->language->id, 'simple_msg', Mail::l('Copia preventivo inviato al cliente', 5), 
                                        $params, $mail_scrivente, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
                                        _PS_MAIL_DIR_, true);*/

                                    Customer::Storico(Tools::getValue('id_thread'), 'P', $mio_id, 7);
                                    
                                    // CORREGGERE: decommentare quando funzionano i file
                                    /*$log_mail = fopen("../import/log-mail.txt","a+");
                                    $riga_log = "PRV | Mail inviata a ".$mail." in data ".date("Y-m-d H:i:s")."\n";
                                    @fwrite($log_mail,$riga_log);
                                    @fclose($log_mail);*/
                                }
                            }
                            
                            $cc = "";
                            $cc_str = "";

                            foreach($_POST['conoscenza_p'] as $conoscenza) {
                                $cc_str = $cc_str.Db::getInstance()->getValue('
                                    SELECT firstname 
                                    FROM '._DB_PREFIX_.'employee 
                                    WHERE id_employee = '.$conoscenza).' 
                                ';
                            }
                                
                            foreach($_POST['conoscenza_p'] as $conoscenza) 
                            {
                                $cc .= $conoscenza.";";
                                echo $conoscenza."<br />";
                                
                                $mailconoscenza = Db::getInstance()->getValue('
                                    SELECT email 
                                    FROM '._DB_PREFIX_.'employee 
                                    WHERE id_employee = '.$conoscenza.'
                                ');
                                
                                $tokenimp = Tools::getAdminToken("AdminCustomers".(int)Tab::getIdFromClassName("AdminCustomers").$conoscenza);
                                        
                                $linkimp = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&tab_name=actions&azione=actions&viewpreventivo&id_thread=".$thread."&token=".$tokenimp;
                                    
                                // correggere: decommentare link giusto (manca modulo formprevendita)
                                $params4 = array(
                                    '{reply}' => "<strong>".$employeemess->firstname."</strong> ha inviato un ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." a un cliente e ti ha messo in copia conoscenza.
                                    <br /><br />
                                    L'ID del ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." &egrave;: <strong>".$idrichiesta."</strong><br /><br />
                                    Il ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." &egrave; in carico a: <strong>".$in_carico_a."</strong><br /><br />
                                    Il messaggio &egrave; stato inviato in conoscenza a: ".$cc_str."<br /><br />
                                    <a href='".$linkimp."'>Clicca qui per aprire il ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')."</a>. ",
                                    '{firma}' => $firma,
                                    '{id_richiesta}' => "Id richiesta: <strong>".$idrichiesta."</strong>",
                                    //'{link}' => $link->getPageLink('modules/formprevendita/form.php', true).'?id_thread='.$thread.'&token='.$token
                                    '{link}' => 'prova_link'
                                );
                                        
                                Mail::Send((int)$this->context->language->id, 'senzagrafica', Mail::l(($tipo_richiesta_p == 'tirichiamiamonoi' ? 'Ti richiamiamo noi' : 'Preventivo').' utente in copia conoscenza', (int)$this->context->language->id), 
                                $params4, $mailconoscenza, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
                                _PS_MAIL_DIR_, true);
                            }
                            
                            if(!empty($_POST['conoscenza_p']))
                                Db::getInstance()->execute("
                                    INSERT INTO ticket_cc (msg, type, cc) 
                                    VALUES (".$idmessaggio.", 'p', '".$cc."')
                                ");
                        
                            /*Mail::Send((int)$this->context->language->id, 'action', Mail::l('Aperto un preventivo per conto del cliente', (int)$this->context->language->id), 
                                        $params2, "ezio.delgiudice@ezdirect.it", NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
                                        _PS_MAIL_DIR_, true);*/
                            
                            if(Tools::getValue('in_carico_a_p') != 0) {

                                if(Tools::getValue('in_carico_a_p') != $preventivo['id_employee'] && Tools::getValue('in_carico_a_p') != $mio_id) 
                                {
                                    $mail_incarico_p = Db::getInstance()->getValue('
                                        SELECT email 
                                        FROM '._DB_PREFIX_.'employee 
                                        WHERE id_employee = '.Tools::getValue('in_carico_a_p').'
                                    ');

                                    $tokenimp = Tools::getAdminToken("AdminCustomers".(int)Tab::getIdFromClassName("AdminCustomers").Tools::getValue('in_carico_a_p'));
                                    $linkimp = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&tab_name=actions&azione=actions&viewpreventivo&id_thread=".$thread."&token=".$tokenimp;
                                    
                                    $params3 = array(
                                    '{reply}' => $employeeincaricato->firstname." ".$employeeincaricato->lastname." ti ha assegnato un ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." su Ezdirect. L'id del ".($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo')." &egrave;: ".$idrichiesta." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.")<br /><br />
                                    Il messaggio &egrave; stato inviato in conoscenza a: ".$cc_str."<br /><br />
                                    <a href='".$linkimp."'>Clicca qui per rispondere</a>.
                                    ");
                                    
                                    // correggere: sostituire 'msg_base' con 'action' dopo test
                                    Mail::Send($this->context->language->id, 'msg_base', Mail::l(''.($tipo_richiesta_p == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo').' cliente assegnato a te su Ezdirect', $this->context->language->id), 
                                    $params3, $mail_incarico_p, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
                                    _PS_MAIL_DIR_, true);
                                }
                            
                                Tools::redirectAdmin(self::$currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&tab_name=actions&conf=4&token='.$this->token);
                            }
                            else {
                                Tools::redirectAdmin(self::$currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&tab_name=actions&azione=actions&viewpreventivo&id_thread='.$thread.'&preventivoreply&conf=4&token='.$this->token);
                            }
                        }
                    }

                    if(!Tools::isSubmit('submitPreventivoReply')) {

                        if (isset($_GET['preventivoreply'])) {
                            $this->confirmations[] = 'Risposta inviata con successo'; // correggere: test, se non funziona mettere nel tpl
                        }
                        else if (!isset($_GET['id_mex']) && (!isset($_GET['preventivoreply']) || isset($_GET['aprinuovopreventivo']))) {

                            // Correggere: dove setto aprinuovopreventivo? Esiste ancora?
                            if(isset($_GET['aprinuovopreventivo'])) {
                                // tpl <h2>Invia preventivo al cliente</h2>';
                                // tpl <input type="hidden" name="nuovopreventivo" value="1" />
                            }

                            // Correggere: ricominciano le variabili di cronologia ma devono essere tolte da isset(id_thread) se esiste davvero aprinuovopreventivo!

                            if($customer->is_company == 1){
                                $persone = Db::getInstance()->executeS("SELECT id_persona, firstname, lastname, email FROM persone WHERE id_customer = ".$customer->id." AND email != ''");
								$persona_preventivo = Db::getInstance()->getValue('SELECT id_persona FROM persone WHERE firstname = "'.$preventivo['firstname'].'" AND lastname = "'.$preventivo['lastname'].'" AND email = "'.$preventivo['email'].'"');
                            }

                            $customer_phone = Db::getInstance()->getRow("
                                SELECT phone, phone_mobile 
                                FROM "._DB_PREFIX_."address 
                                WHERE active = 1 
                                    AND deleted = 0 
                                    AND fatturazione = 1
                                    AND id_customer = ".$customer->id
                            );

                            if($customer_phone['phone'] == '')
                                $telefono_cliente = $customer_phone['phone_mobile']; 
                            else
                                $telefono_cliente = $customer_phone['phone']; 

                            if($preventivo['phone'] != '')
                                $telefono_cliente = $preventivo['phone']; 

                            if(isset($_GET['aprinuovopreventivo'])) {
                                $impez_selected = $mio_id;
                            }
                            else {
                                $impez_selected = $preventivo['id_employee'];
                            }

                            // Aggiungere form ricerca prodotti come in orders

                            $marche = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'manufacturer ORDER BY name ASC');
                            $categorie = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'category c JOIN '._DB_PREFIX_.'category_lang cl ON c.id_category = cl.id_category WHERE cl.id_lang = '.$this->context->language->id.' AND c.id_parent = 1 ORDER BY cl.name ASC');
                            $fornitori = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'supplier WHERE id_supplier = 8 OR id_supplier = 11 OR id_supplier = 42 ORDER BY name ASC');

                            $riferimento = (Tools::getValue('riferimento') ? Tools::getValue('riferimento') : Db::getInstance()->getValue('SELECT riferimento FROM form_prevendita_thread WHERE id_thread = '.$thread['id_thread']));
                            $statusPadre = Customer::statusPadre($riferimento);
                        }
                    }
                    
                    // correggere: togliere da isset(id_thread) se esiste ancora aprinuovopreventivo
                    $cronologia = array(
                        'azione' => $preventivo,
                        'thread' => $thread,
                        'messaggip' => $messaggip,
                        'impiegati_eza' => $impiegati_eza,
                        'impez_selected' => $impez_selected,
                        'persone' => $persone,
                        'telefono_cliente' => $telefono_cliente,
                        'marche' => $marche,
                        'categorie' => $categorie,
                        'fornitori' => $fornitori,
                        'precompilati' => $precompilati,
                        'riferimento' => $riferimento,
                        'statusPadre' => $statusPadre,
                        'cp_interest' => $cp_interest,
                    );
                    /* Fine Cronologia */

                    /* Gerarchia */
                    $first = Customer::HierarchyFirst(Tools::getValue('id_thread'), 'P');
                        
                    // Correggere: manca la class tree-menu
                    $tree_ul = '<ul class="tree-menu"><li>'.Customer::trovaSiglaLinkPerAlbero(substr($first, 1), substr($first, 0, 1), '').'';
                    $replace .= str_replace('<ul></ul>', '', Customer::HierarchyFindChildren($first, $children, 'P'.Tools::getValue('id_thread')));
                    $tree_ul .= $replace;
                    $tree_ul .= '</li></ul>';
                    
                    $gerarchia = array(
                        'tree_ul' => $tree_ul,
                    );
                    /* Fine Gerarchia */

                    /* Storico */
                    $storico_ticket = Db::getInstance()->executeS('SELECT * FROM storico_attivita WHERE id_attivita = '.Tools::getValue('id_thread').' AND tipo_attivita = "P" ORDER BY data_attivita DESC, desc_attivita DESC');
    
                    foreach($storico_ticket as &$storico)
                    {
                        if(is_numeric(substr($storico['desc_attivita'], -1)))
                        {	
                            $employee = filter_var($storico['desc_attivita'], FILTER_SANITIZE_NUMBER_INT);
                            $name_employee = Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$employee);
                            $desc_attivita = 'Ha messo l\'attivit&agrave; in carico a '.$name_employee;
                        }
                        else
                            $desc_attivita = $storico['desc_attivita'];
    
                        $storico['azione'] = $desc_attivita;
                        $storico['data_attivita'] = Tools::displayDate($storico['data_attivita'], $this->context->language->id, true);
                        $storico_employee = Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$storico['id_employee']);
    
                        $storico['persona'] = ($storico['id_employee'] == 0 ? 'Cliente' : $storico_employee);
                    }
    
                    $storico = array(
                        'storico_ticket' => $storico_ticket,
                    );
                    /* Fine Storico */
                }

                $azioni_view = array(
                    'generale' => $generale,
                    'cronologia' => $cronologia,
                    'gerarchia' => $gerarchia,
                    'storico' => $storico,
                    /* Variabili usate sia in Generale (view/edit) che in Cronologia (add) */
                    'action_types' => $action_types,
                    'action_type' => $action_type,
                );
            }
            else if(Tools::getIsset('viewmessage')){
                
                if(!Tools::getIsset('conf') && !Tools::getIsset('filename'))
                    Customer::Storico(Tools::getValue('id_mex'), 'T', $mio_id, 14);
                
                if(isset($_GET['filename'])){
                    
                    $filename = $_GET['filename'];
                    
                    if(strpos($filename, ":::")) {
                        $parti = explode(":::", $filename);
                        $nomecodificato = $parti[0];
                        $nomevero = $parti[1];
                    }
                    else {
                        $nomecodificato = $filename;
                        $nomevero = $filename;
                    }
                    
                    if (file_exists(_PS_UPLOAD_DIR_.$nomecodificato))
                    self::openUploadedFile();
                }

                $action_type = 7; // Messaggio
                
                $allega_bdl = '';
                if(Tools::getIsset('allega_bdl'))
                    $allega_bdl = Tools::getValue('allega_bdl');

                if(Tools::getIsset('id_mex')) { // correggere: finire di mettere in generale e cronologia le var per il tpl
                    
                    $mex = Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."customer_thread ct WHERE ct.id_customer_thread = ".Tools::getValue('id_mex')."");

                    $id_incmex = Db::getInstance()->getValue("SELECT id_employee FROM "._DB_PREFIX_."customer_message WHERE id_customer_thread = ".$mex['id_customer_thread']."");
					$incmex = new Employee($id_incmex);
                    $in_carico_a_firstname = $incmex->firstname;
					
					$annomex = substr($mex['date_add'],2,2);
					$siglam = "M";
					$idmexunivoco = $siglam.$annomex.$mex['id_customer_thread'];
						
					switch($mex['status']) {
						case 'open': 
                            $status_mex = "<i class='icon-circle rosso' title='Aperto'></i>";
                            $st_mex = 'Aperto';
                            break;
						case 'closed': 
                            $status_mex = "<i class='icon-circle' style='color:green' title='Chiuso'></i>";
                            $st_mex = 'Chiuso';
                            break;
						case 'pending1': 
                            $status_mex = "<i class='icon-circle' style='color:orange' title='In lavorazione'></i>";
                            $st_mex = 'In lavorazione';
                            break;
						default: 
                            $status_mex = "<i class='icon-circle rosso' title='Aperto'></i>";
                            $st_mex = 'Aperto';
                            break;
					}

                    $status_azione = $status_mex;
                    $st_azione = $st_mex;

                    // Numero di messaggi del cliente nel thread
					$ctrl_mex = Db::getInstance()->getValue("SELECT COUNT(id_customer_message) FROM "._DB_PREFIX_."customer_thread ct JOIN "._DB_PREFIX_."customer_message cm ON ct.id_customer_thread = cm.id_customer_thread WHERE cm.id_employee = 0 AND ct.id_customer_thread = ".$mex['id_customer_thread']."");

                    /* Generale */
                    $oggetto =  $mex['subject'];  
                    if($oggetto == '')
                        $oggetto = Db::getInstance()->getValue("SELECT message FROM "._DB_PREFIX_."customer_message WHERE id_customer_thread = ".Tools::getValue('id_mex')." ORDER BY date_add ASC");
                    
                    $oggetto = strip_tags(htmlspecialchars_decode($oggetto));
                    
                    $aperto_da = Db::getInstance()->getValue('SELECT id_employee FROM '._DB_PREFIX_.'customer_message WHERE id_customer_thread = '.Tools::getValue('id_mex').' ORDER BY id_customer_message ASC');
                
                    if($aperto_da != 0)
                        $aperto_da = Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$aperto_da);
                    else
                        $aperto_da = 'Cliente';
                    
                    $data_apertura = Tools::displayDate($mex['date_add'], (int)($this->context->language->id), true);

                    $dataultimo = Db::getInstance()->getValue("SELECT date_add FROM "._DB_PREFIX_."customer_message WHERE id_customer_thread = ".$_GET['id_mex']." ORDER BY date_add DESC");
                    $data_ultima_com = Tools::displayDate($dataultimo, (int)($this->context->language->id), true);
                    
                    if($mex['riferimento'] != '')
                        $azione_padre = Customer::trovaSiglaLinkPerAlbero(substr($mex['riferimento'],1), substr($mex['riferimento'],0,1), '','y:T:'.$mex['id_customer_thread'].':'.$mex['status']);

                    // Note private
                    $messaggio_note = Db::getInstance()->executeS('SELECT * FROM note_attivita WHERE tipo_attivita = "T" AND id_attivita = '.$mex['id_customer_thread'].'');
                    foreach($messaggio_note as &$messaggio_nota)
                        $messaggio_nota['creato_da'] = Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$messaggio_nota['id_employee']);

                    // Vecchia nota del messaggio
                    if($mex['date_add'] < '2017-12-08 00:00:00'){
                        $mex_old = true;
                        $mex_nota = Tools::htmlentitiesUTF8($mex['note']);
                    }
                    else
                        $mex_old = false;

                    $collega_a = Customer::BuildSelectForLinking($customer->id, $mex['id_customer_thread'], 'T');

                    $generale = array(
                        'azione' => $mex,
                        'idunivoco' => $idmexunivoco,
                        'ctrl_mex' => $ctrl_mex,
                        'status_azione' => $status_azione,
                        'st_azione' => $st_azione,
                        'oggetto' => $oggetto,
                        'aperto_da' => $aperto_da,
                        'in_carico_a_options' => $impiegati_eza,
                        'in_carico_a_firstname' => $in_carico_a_firstname,
                        'data_apertura' => $data_apertura,
                        'data_ultima_com' => $data_ultima_com,
                        'azione_padre' => $azione_padre,
                        'azione_note' => $messaggio_note,
                        'mex_old' => $mex_old,
                        'mex_nota' => $mex_nota,
                        'collega_a' => $collega_a,
                    );
                    /* Fine Generale */
        
                    /* Cronologia */
                    $messaggixt = Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_."customer_message cm WHERE cm.id_customer_thread = ".Tools::getValue('id_mex')."");
                    
                    foreach($messaggixt as &$messaggioxt) {

                        if ($messaggioxt['id_employee'] == 0) 
                            $messaggioxt['da'] = $customer->firstname." ".$customer->lastname;
                        else {
                            $employeezxt = new Employee($messaggioxt['id_employee']);
                            $messaggioxt['da'] = $employeezxt->firstname." ".substr($employeezxt->lastname,0,1).".";
                        }
                            
                        $messaggioxt['data'] = date("d-m-Y H:i:s",strtotime($messaggioxt['date_add']));

                        if(!empty($messaggioxt['file_name'])) {
                            $allegatim = explode(";",$messaggioxt['file_name']);
                            $nallm = 1;
                            foreach($allegatim as $allegatom) {
                                if(strpos($allegatom, ":::")) {
                                    $parti = explode(":::", $allegatom);
                                    $nomeverom = $parti[1];
                                }
                                else
                                    $nomeverom = $allegatom;
                                
                                if($allegatom == "") {
                                    // niente
                                } 
                                else {
                                    if($nallm != 1)
                                        $allegati .= '-';

                                    $allegati .= '<a href="'.(substr($allegatom,0,3) == 'BDL' ? 'ajax.php?openAllegato=y&id_customer='.$customer->id.'&viewcustomer&tab_name=bdl&azione=bdl_edit&id_bdl='.str_replace('BDL','',$parti[0]).'&getPDFBDL=ezdirect&token='.$this->token : 'ajax.php?openAllegato=y&id_customer='.$customer->id.'&viewcustomer&tab_name=actions&azione=actions&viewmessage&id_mex='.$mex['id_customer_thread'].'&filename='.$allegatom.'&token='.$this->token).'"><span style="color:red">'.$nomeverom.'</span></a>';
                                    $nallm++;
                                }
                            }
                        }
                        else
                            $allegati = 'Nessun allegato';
                            
                        $messaggioxt['allegati'] = $allegati;

                        $messaggioxt['message'] = htmlspecialchars_decode($messaggioxt['message']);
                        $messaggioxt['message_modifica'] = htmlentities($messaggioxt['message'], ENT_QUOTES | ENT_SUBSTITUTE, "UTF-8");
                        
                        if($messaggioxt['note_private'] != '')
                            $messaggioxt['note_private'] = htmlspecialchars_decode(preg_replace('/(<[^>]+) style=".*?"/i', '$1', preg_replace('/(<[^>]+) class=".*?"/i', '$1', $messaggioxt['note_private'])));
                        
                        if($messaggioxt['email'] == '') { 
                            $messaggioxt['in_carico_a_name'] = ($messaggioxt['in_carico'] > 0 ? Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$messaggioxt['in_carico']) : '--');
                        } 
                        else {
                            $messaggioxt['employee_to_name'] = (is_numeric($messaggioxt['email']) ? Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$messaggioxt['email']) : htmlspecialchars_decode($messaggioxt['email']));
                            $messaggioxt['in_carico_a_name'] = ($messaggioxt['in_carico'] > 0 ? Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$messaggioxt['in_carico']) : '--');
                        }
                        
                        $cc = Db::getInstance()->getValue("SELECT cc FROM ticket_cc WHERE type = 't' AND msg = ".$messaggioxt['id_customer_message']."");
                        if($cc) {
                            $ccs = explode(";",$cc);
                            
                            foreach ($ccs as $conoscenza) {
                                $imp = Db::getInstance()->getValue("SELECT firstname FROM "._DB_PREFIX_."employee WHERE id_employee = '".$conoscenza."'");
                                $cc_str .= $imp." ";
                            }
                            
                            $messaggioxt['cc'] = $cc_str;
                        }
                        else {
                            $messaggioxt['cc'] = 'Nessuno';
                        }
                    }
                }

                if($customer->is_company == 1)
                    $persone = Db::getInstance()->executeS("SELECT id_persona, firstname, lastname, email FROM persone WHERE id_customer = ".Tools::getValue('id_customer')." AND email != ''");

                if(isset($_GET['id_persona']))
                    $mail_persona = Db::getInstance()->getValue("SELECT email FROM persone WHERE id_persona = ".$_GET['id_persona']);
                else
                    $mail_persona = $customer->email;

                // Correggere: si può migliorare prendendo il telefono della persona selezionata se il campo è vuoto
                if($mex['phone'] != '')
                    $telefono_cliente = $mex['phone'];
                else {
                    $customer_phone = Db::getInstance()->getRow("
                        SELECT phone, phone_mobile 
                        FROM "._DB_PREFIX_."address 
                        WHERE active = 1 
                            AND deleted = 0 
                            AND fatturazione = 1
                            AND id_customer = ".$customer->id
                    );

                    if($customer_phone['phone'] == '')
                        $telefono_cliente = $customer_phone['phone_mobile']; 
                    else
                        $telefono_cliente = $customer_phone['phone']; 
                }

                if(isset($_GET['aprinuovomessaggio'])) {
                    $impez_selected = $mio_id;
                }
                else {
                    $impez_selected = $mex['id_employee'];
                }

                // Aggiungere form ricerca prodotti come in orders

                $marche = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'manufacturer ORDER BY name ASC');
                $categorie = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'category c JOIN '._DB_PREFIX_.'category_lang cl ON c.id_category = cl.id_category WHERE cl.id_lang = '.$this->context->language->id.' AND c.id_parent = 1 ORDER BY cl.name ASC');
                $fornitori = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'supplier WHERE id_supplier = 8 OR id_supplier = 11 OR id_supplier = 42 ORDER BY name ASC');

                $riferimento = (Tools::getValue('riferimento') ? Tools::getValue('riferimento') : Db::getInstance()->getValue('SELECT riferimento FROM '._DB_PREFIX_.'customer_thread WHERE id_customer_thread = '.$mex['id_customer_thread']));
				$statusPadre = Customer::statusPadre($riferimento);

                $thread = $mex['id_customer_thread'];

                $cronologia = array(
                    'azione' => $mex,
                    'thread' => $thread,
                    'messaggixt' => $messaggixt,
                    'impiegati_eza' => $impiegati_eza,
                    'impez_selected' => $impez_selected,
                    'persone' => $persone,
                    'mail_persona' => $mail_persona,
                    'telefono_cliente' => $telefono_cliente,
                    'marche' => $marche,
                    'categorie' => $categorie,
                    'fornitori' => $fornitori,
                    'precompilati' => $precompilati,
                    'riferimento' => $riferimento,
                    'statusPadre' => $statusPadre,
                    'allega_bdl' => $allega_bdl,
                );
                /* Fine Cronologia */

                // Gerarchia e Storico
                if(Tools::getIsset('id_mex')){

                    /* Gerarchia */
                    $first = Customer::HierarchyFirst(Tools::getValue('id_mex'), 'T');
                        
                    // Correggere: manca la class tree-menu
                    $tree_ul = '<ul class="tree-menu"><li>'.Customer::trovaSiglaLinkPerAlbero(substr($first, 1), substr($first, 0, 1), '').'';
                    $replace .= str_replace('<ul></ul>', '', Customer::HierarchyFindChildren($first, $children, 'T'.Tools::getValue('id_mex')));
                    $tree_ul .= $replace;
                    $tree_ul .= '</li></ul>';
                    
                    $gerarchia = array(
                        'tree_ul' => $tree_ul,
                    );
                    /* Fine Gerarchia */
    
                    /* Storico */
                    $storico_ticket = Db::getInstance()->executeS('SELECT * FROM storico_attivita WHERE id_attivita = '.Tools::getValue('id_mex').' AND tipo_attivita = "T" ORDER BY data_attivita DESC, desc_attivita DESC');
    
                    foreach($storico_ticket as &$storico)
                    {
                        if(is_numeric(substr($storico['desc_attivita'], -1)))
                        {	
                            $employee = filter_var($storico['desc_attivita'], FILTER_SANITIZE_NUMBER_INT);
                            $name_employee = Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$employee);
                            $desc_attivita = 'Ha messo l\'attivit&agrave; in carico a '.$name_employee;
                        }
                        else
                            $desc_attivita = $storico['desc_attivita'];
    
                        $storico['azione'] = $desc_attivita;
                        $storico['data_attivita'] = Tools::displayDate($storico['data_attivita'], $this->context->language->id, true);
                        $storico_employee = Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$storico['id_employee']);
    
                        $storico['persona'] = ($storico['id_employee'] == 0 ? 'Cliente' : $storico_employee);
                    }
    
                    $storico = array(
                        'storico_ticket' => $storico_ticket,
                    );
                    /* Fine Storico */ 
                }

                $azioni_view = array(
                    'generale' => $generale,
                    'cronologia' => $cronologia,
                    'gerarchia' => $gerarchia,
                    'storico' => $storico,
                    /* Variabili usate sia in Generale (view/edit) che in Cronologia (add) */
                    'action_types' => $action_types,
                    'action_type' => $action_type,
                );
            }
            else {
                $azioni_list = Customer::BuildListing($customer->id, 'preventivo'); // I messaggi sono inclusi
            }

            $specific = 'n';

            $azioni_cliente = array(
                'azioni_list' => $azioni_list,
                'azioni_view' => $azioni_view,
                'specific' => $specific,
            );

        }
        /* FINE Azioni cliente */

        // Nella 1.4 si chiamavano $azioni e $azioniaperti
        $count_todo_aperti = Db::getInstance()->getValue("SELECT COUNT(act.id_action) FROM action_thread act WHERE act.id_customer = ".$customer->id." AND act.status != 'closed'");
        $count_todo = Db::getInstance()->getValue("SELECT COUNT(id_action) FROM action_thread WHERE id_customer = ".$customer->id);

        /* INIZIO TO DO */
        if($tab_name == 'todo' && !$not_mio_agente){ //tab-container-1=10

            // TESTARE!
            if(isset($_POST['cambiastatusa_listing'])) {
                $subject = Db::getInstance()->getValue('SELECT subject FROM action_thread WHERE id_action = '.Tools::getValue('id_action'));
                $current_action_status = Db::getInstance()->getValue('SELECT status FROM action_thread WHERE id_action = '.Tools::getValue('id_action'));
    
                if(substr($subject, 0, 20) == '*** VERIFICA TECNICA')
                {	
                    if(Tools::getValue('cambiastatusa_listing') == 'closed' && $current_action_status != 'closed')
                    {
                        Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$mio_id.', '.substr($subject, -6, 5).', 25, "'.date('Y-m-d H:i:s').'")');
                        // Product::RecuperaCSV(substr($subject, -6, 5));
                    }
                    else if((Tools::getValue('cambiastatusa_listing') == 'open' || Tools::getValue('cambiastatusa_listing') == 'pending1')  && $current_action_status != 'closed')
                    {
                        Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$mio_id.', '.substr($subject, -6, 5).', 24, "'.date('Y-m-d H:i:s').'")');
                        // Product::RecuperaCSV(substr($subject, -6, 5));
                    }
                }
                else if(substr($subject, 0, 10) == '*** RMA N.')
                {	
                    if(Tools::getValue('cambiastatusa_listing') == 'closed' && $current_action_status != 'closed')
                    {
                        Db::getInstance()->execute('UPDATE rma SET test = 0 WHERE id_customer_thread = '.substr(Db::getInstance()->getValue('SELECT riferimento FROM action_thread WHERE id_action = '.Tools::getValue('id_action')),1));
                    }
                }
                
                Db::getInstance()->execute("UPDATE action_thread SET status = '".Tools::getValue('cambiastatusa_listing')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_action = '".addslashes(Tools::getValue('id_action'))."'");
                
                switch(Tools::getValue('cambiastatusa_listing'))
                {
                    case 'open': $switch_status = 3; break;
                    case 'pending1': $switch_status = 4; break;
                    case 'closed': $switch_status = 5; break;
                    default: $switch_status = 10; break;
                }	
                
                Customer::Storico(Tools::getValue('id_action'), 'A', $mio_id, $switch_status);
                Customer::chiudiOrdineDaAzione(Tools::getValue('id_action'));
                Tools::redirectAdmin(self::$currentIndex.'&'.$this->identifier.'='.$customer->id.'&viewcustomer&tab_name=todo&conf=4&token='.$this->token);
            }

            if(isset($_POST['cambiaincaricoa'])) {
                Customer::cambiaIncaricoVeloce(Tools::getValue('tipo'), Tools::getValue('cambiaincaricoa'), Tools::getValue('id_thread'), Tools::getValue('precedente_incarico'), Tools::getValue('id_ticket_univoco'), Tools::getValue('id_customer'));
                Tools::redirectAdmin(self::$currentIndex.'&'.$this->identifier.'='.$customer->id.'&viewcustomer&tab_name=todo&conf=4&token='.$this->token);
            }    
            
            // CORREGGERE: DECOMMENTARE TUTTI I RECUPERACSV DEL CODICE DOPO CAMBIO SERVER
            if(isset($_POST['submitAzione'])) {
                $action_date_array = explode("-",Tools::getValue('action_date'));
                $action_date = $action_date_array[2]."-".$action_date_array[1]."-".$action_date_array[0]." ".Tools::getValue('action_hours').":".Tools::getValue('action_minutes').":00";
                
                if(strpos(Tools::getValue('action_date'),'0000') !== false) {
                    $action_date = Db::getInstance()->getValue("SELECT date_action FROM action_thread WHERE id_action = ".Tools::getValue('id_action')."");
                }
                
                $precedente_status = Db::getInstance()->getValue("SELECT status FROM action_thread WHERE id_action = ".Tools::getValue('id_action')."");
                if($precedente_status != Tools::getValue('cambiastatusa'))
                {
                    switch(Tools::getValue('cambiastatusa'))
                    {
                        case 'open': $switch_status = 3; break;
                        case 'pending1': $switch_status = 4; break;
                        case 'closed': $switch_status = 5; break;
                        default: $switch_status = 10; break;
                    }	
                    
                    Customer::Storico(Tools::getValue('id_action'), 'A', $mio_id, $switch_status);
                }

                $subject = Db::getInstance()->getValue('SELECT subject FROM action_thread WHERE id_action = "'.Tools::getValue('id_action').'"');
                $current_action_status = Db::getInstance()->getValue('SELECT status FROM action_thread WHERE id_action = "'.Tools::getValue('id_action').'"');
                
                if(substr($subject,0,20) == '*** VERIFICA TECNICA')
                {	
                    if(Tools::getValue('cambiastatusa') == 'closed' && $current_action_status != 'closed')
                    {
                        Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$mio_id.', '.substr($subject, -6, 5).', 25, "'.date('Y-m-d H:i:s').'")');
                        // Product::RecuperaCSV(substr($subject, -6, 5));
                    }
                    else if((Tools::getValue('cambiastatusa') == 'open' || Tools::getValue('cambiastatusa') == 'pending1') && $current_action_status == 'closed')
                    {
                        Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$mio_id.', '.substr($subject, -6, 5).', 24, "'.date('Y-m-d H:i:s').'")');
                        // Product::RecuperaCSV(substr($subject, -6, 5));
                    }
                }
                else if(substr($subject,0,10) == '*** RMA N.')
                {	
                    if(Tools::getValue('cambiastatusa_listing') == 'closed'  && $current_action_status != 'closed')
                    {
                        Db::getInstance()->execute('UPDATE rma SET test = 0 WHERE id_customer_thread = '.substr(Db::getInstance()->getValue('SELECT riferimento FROM action_thread WHERE id_action = '.Tools::getValue('id_action')),1));
                    }
                }

                Db::getInstance()->execute("UPDATE action_thread SET subject = '".addslashes(Tools::getValue('azione_subject'))."', action_type = '".addslashes(Tools::getValue('action_type'))."', action_date = '".$action_date."', status = '".Tools::getValue('cambiastatusa')."', note = '".addslashes(Tools::getValue('action_note'))."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_action = '".addslashes(Tools::getValue('id_action'))."'");
                
                // Attivare solo se vengono introdotte le note private in aprinuovaazione (in quel caso non si possono lasciare in ajax perchè id_action non esiste ancora)
                /*foreach ($_POST['note_nuova'] as $id_nota => $nota_id) {
                                    
                    if($_POST['note_nuova'][$id_nota] == 0) {
                        $testo_nota = Db::getInstance()->getValue('SELECT note FROM note_attivita WHERE id_note = '.$id_nota.'');
                        if($testo_nota != $_POST['note_nota'][$id_nota])
                            Db::getInstance()->execute("UPDATE note_attivita SET note = '".addslashes($_POST['note_nota'][$id_nota])."', date_upd = '".date("Y-m-d H:i:s")."', id_employee = '".$mio_id."' WHERE id_note = ".$id_nota.""); 
                    }
                    else {
                        if(addslashes($_POST['note_nota'][$id_nota]) != '')
                        {
                            Db::getInstance()->execute("INSERT INTO note_attivita
                            (id_note,
                            id_attivita,
                            tipo_attivita,
                            id_employee,
                            note,
                            date_add,
                            date_upd)
                            
                            VALUES (
                            NULL,
                            '".Tools::getValue('id_action')."',
                            'A',
                            '".$mio_id."',
                            '".addslashes($_POST['note_nota'][$id_nota])."',
                            '".date("Y-m-d H:i:s")."',
                            '".date("Y-m-d H:i:s")."'
                            )");
                        }
                    }
                }*/
                
                foreach ($_POST['promemoria_nuovo'] as $id_promemoria=>$promemoria_id) {
                    Db::getInstance()->execute('INSERT INTO ticket_promemoria (msg, tipo_att, ora, conto, tipo, status) VALUES ('.Tools::getValue('id_action').', "a", "'.$action_date.'" - INTERVAL '.$_POST['action_minuti'][$id_promemoria].' '.$_POST['action_tipo_promemoria'][$id_promemoria] .', "'.$_POST['action_minuti'][$id_promemoria].'", "'.$_POST['action_tipo_promemoria'][$id_promemoria].'", "open")');
                }
                
                Customer::chiudiOrdineDaAzione(Tools::getValue('id_action'));
                
                $impiegato_attuale = Db::getInstance()->getValue("SELECT action_to FROM action_thread WHERE id_action = '".Tools::getValue('id_action')."'");
                
                if($impiegato_attuale != Tools::getValue('assegnaimpiegatoa')) 
                {
                    Customer::Storico(Tools::getValue('id_action'), 'A', $mio_id, 2, Tools::getValue('assegnaimpiegatoa'));
                    Db::getInstance()->execute("UPDATE action_thread SET action_to = '".Tools::getValue('assegnaimpiegatoa')."' WHERE id_action = '".Tools::getValue('id_action')."'");
                                    
                    // TEST! CORREGGERE; DECOMMENTARE MAIL GIUSTA
                    $mailimp = 'carolina.carusi@ezdirect.it';
                    //$mailimp = Db::getInstance()->getValue("SELECT email FROM "._DB_PREFIX_."employee WHERE id_employee = '".Tools::getValue('assegnaimpiegatoa')."'");

                    $tokenimp = Tools::getAdminToken("AdminCustomers".(int)Tab::getIdFromClassName("AdminCustomers").Tools::getValue('assegnaimpiegatoa'));
                    $linkimp = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&tab_name=todo&azione=todo&viewaction&id_action=".Tools::getValue('id_action')."&token=".$tokenimp;
        
                    $msgimp = $employeemess->firstname." ".$employeemess->lastname." ti ha assegnato l'azione numero ".Tools::getValue('id_action')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect. <a href='".$linkimp."'>Fai clic qui per aprirla</a>.";
                                    
                    if($impiegato_attuale != 0 && $impiegato_attuale != $mio_id) 
                    {
                        $impiegato_assegnato = (Tools::getValue('assegnaimpiegatoa') != 0 ? Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.Tools::getValue('assegnaimpiegatom')) : 'Nessuno');
                        // TEST! CORREGGERE; DECOMMENTARE MAIL GIUSTA
                        $mailimprev = 'carolina.carusi@ezdirect.it';
                        //$mailimprev = Db::getInstance()->getValue("SELECT email FROM "._DB_PREFIX_."employee WHERE id_employee = '".$impiegato_attuale."'");
                        $nomeimprev = Db::getInstance()->getValue("SELECT firstname FROM "._DB_PREFIX_."employee WHERE id_employee = '".$impiegato_attuale."'");
                        $msgimprev = $employeemess->firstname." ".$employeemess->lastname." ti ha revocato la gestione dell'azione numero ".Tools::getValue('id_action')." (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname." ".$customer->lastname)." - ID ".$customer->id.") su Ezdirect e l'ha passata a ".$impiegato_assegnato;
                                        
                        $params = array(
                            '{link}' => $linkimp,
                            '{firma}' => '',
                            '{msg}' => $msgimprev
                        );
                        		
                        Mail::Send($this->context->language->id, 'msg_base', Mail::l('Gestione azione revocata', $this->context->language->id), 
                            $params, $mailimprev, NULL, NULL, NULL, NULL, NULL, _PS_MAIL_DIR_, true);
                    }
                                    
                    $params = array(
                        '{link}' => $linkimp,
                        '{firma}' => '',
                        '{msg}' => $msgimp
                    );
                                    
                    if($_POST['assegnaimpiegatoa'] != 0 && $_POST['assegnaimpiegatoa'] != $mio_id) {
                        Mail::Send($this->context->language->id, 'msg_base', Mail::l('Azione assegnata a te su Ezdirect', $this->context->language->id), 
                                $params, $mailimp, NULL, NULL, NULL, NULL, NULL, _PS_MAIL_DIR_, true);
                    }
                }
                
                Tools::redirectAdmin(self::$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&tab_name=todo&azione=todo&viewaction&id_action='.Tools::getValue('id_action').'&conf=4&token='.$this->token);
            }

            $precompilati = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'precompilato WHERE active = 1');

            if(Tools::getIsset('viewaction')) {

                /*$fta = Db::getInstance()->getValue('SELECT id_attivita FROM storico_attivita WHERE tipo_num = 1 AND id_attivita = '.Tools::getValue('id_action'));
                if((!$fta || $fta <= 0) && $_GET['id_action'] > 12210)*/

                if(!Tools::getIsset('conf') && !Tools::getIsset('filename'))
                    Customer::Storico(Tools::getValue('id_action'), 'A', $mio_id, 14);
            
                if(isset($_GET['filename'])) {
                    $filename = $_GET['filename'];
                    
                    if(strpos($filename, ":::")) {
                        $parti = explode(":::", $filename);
                        $nomecodificato = $parti[0];
                        $nomevero = $parti[1];
                    }
                    else {
                        $nomecodificato = $filename;
                        $nomevero = $filename;
                    }
                    
                    if (file_exists(_PS_UPLOAD_DIR_.$nomecodificato)) {
                        self::openUploadedFile();
                    }
                }

                $action_types = array(
                    array(
                        "value" => "Attivita",
                        "name" => "Attivit&agrave;"
                    ),
                    array(
                        "value" => "Telefonata",
                        "name" => "Telefonata"
                    ),
                    array(
                        "value" => "Visita",
                        "name" => "Visita"
                    ),
                    array(
                        "value" => "Caso",
                        "name" => "Caso"
                    ),
                    array(
                        "value" => "Intervento",
                        "name" => "Intervento"
                    ),
                    array(
                        "value" => "Richiesta_RMA",
                        "name" => "Richiesta RMA"
                    ),
                    array(
                        "value" => "TODO Amazon",
                        "name" => "TODO Amazon"
                    ),
                    array(
                        "value" => "TODO Social",
                        "name" => "TODO Social"
                    ),
                    array(
                        "value" => "TODO Sendinblue",
                        "name" => "TODO Sendinblue"
                    ),
                );

                if(Tools::getIsset('id_action')){

                    $errore = false;
                    $azione = Db::getInstance()->getRow("SELECT * FROM action_thread at WHERE at.id_action = ".Tools::getValue('id_action')." AND at.id_customer = ".$customer->id);
                    
                    /* Generale */
                    if(!$azione && Tools::getValue('id_action') != '') {
                        $errore = true;
                    }
                    else {
                        // correggere: mettere tutto il resto di generale ecc... (tutto quello con id_action settato) nell'else per ottimizzare
                    }
                    
                    $incaricato = new Employee($azione['action_to']);
                    $in_carico_a_firstname = $incaricato->firstname;
                
                    switch($azione['status']) {
                        case 'open': 
                            $status_azione = "<i class='icon-circle rosso' title='Aperto' /></i>"; 
                            $st_azione = 'Aperto'; 
                            break;
                        case 'closed': 
                            $status_azione = "<i class='icon-circle' style='color:green' title='Chiuso' /></i>"; 
                            $st_azione = 'Chiuso'; 
                            break;
                        case 'pending1': 
                            $status_azione = "<i class='icon-circle' style='color:orange' title='In lavorazione' /></i>"; 
                            $st_azione = 'In lavorazione'; 
                            break;
                        default: 
                            $status_azione = "<i class='icon-circle rosso' title='Aperto' /></i>"; 
                            $st_azione = 'Aperto'; 
                            break;
                    }
            
                    $oggetto = Db::getInstance()->getValue("SELECT subject FROM action_thread WHERE id_action = ".$azione['id_action']." ORDER BY date_add ASC");
                    if($oggetto == '') {
                        $oggetto = Db::getInstance()->getValue("SELECT action_message FROM action_message FORCE INDEX (PRIMARY) WHERE id_action = ".$azione['id_action']." ORDER BY date_add ASC");
                    }

                    $oggetto_readonly = (substr($oggetto, 0, 20) == '*** VERIFICA TECNICA' ? true : false);
					
                    $aperto_da_id = Db::getInstance()->getValue('SELECT action_m_from FROM action_message WHERE id_action = '.$azione['id_action'].' ORDER BY id_action_message ASC');
                    if(!$aperto_da_id) // or = 99? correggere
                        $aperto_da = 'Sistema';
                    else
                        $aperto_da = Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$aperto_da_id);
                    
                    $data_apertura = Tools::displayDate($azione['date_add'], (int)($this->context->language->id), true);

                    $dataultimo = Db::getInstance()->getValue("SELECT date_add FROM action_message WHERE id_action = ".$azione['id_action']." ORDER BY date_add DESC");
					$data_ultima_com = Tools::displayDate($dataultimo, (int)($this->context->language->id), true);

                    if($azione['riferimento'] != '')
                        $azione_padre = Customer::trovaSiglaLinkPerAlbero(substr($azione['riferimento'], 1), substr($azione['riferimento'], 0, 1), '', 'y:A:'.$azione['id_action'].':'.$azione['status']);
                    
                    $data_azione = ($azione['action_date'] == '0000-00-00 00:00:00' ? 'Nessuna data' : date("d-m-Y", strtotime($azione['action_date'])));
                    if($data_azione != 'Nessuna data'){
                        $data_azione_ore = date('H',strtotime($azione['action_date']));
                        $data_azione_min = date('i',strtotime($azione['action_date']));
                    }

                    $promemoria_az = Db::getInstance()->executeS('SELECT * FROM ticket_promemoria WHERE tipo_att = "a" AND msg = '.$azione['id_action']);
                    $i_p_a = 1;
                    foreach($promemoria_az as $p_a)
                    {	
                        switch($p_a['tipo']) {
                            case 'DAY': $tipo_pm = 'giorni'; break;
                            case 'HOUR': $tipo_pm = 'ore'; break;
                            case 'MINUTE': $tipo_pm = 'minuti'; break;
                            default: $tipo_pm = ''; break;
                        }	
                        $tr_promemoria .= '<tr id="pm_'.$i_p_a.'"><td>'.$i_p_a.') '.$p_a['conto'].' '.$tipo_pm.' prima (data: '.Tools::displayDate($p_a['ora'], $this->context->language->id, true).')</td><td><a href="javascript:void(0)" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { deletePromemoria('.$p_a['msg'].', \''.$p_a['tipo_att'].'\', \''.$p_a['ora'].'\', '.$i_p_a.'); } else { return false; }"><img src="../img/admin/delete.gif" alt="Elimina" title="Elimina" /></a></td></tr>';
                        $i_p_a++;
                    }

                    $azione_note = Db::getInstance()->executeS('SELECT * FROM note_attivita WHERE tipo_attivita = "A" AND id_attivita = '.$azione['id_action']);
                    foreach($azione_note as &$azione_nota)
                        $azione_nota['creato_da'] = Db::getInstance()->getValue('SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$azione_nota['id_employee']);

                    $collega_a = Customer::BuildSelectForLinking($customer->id, $azione['id_action'], 'A');

                    $generale = array(
                        'azione' => $azione,
                        'status_azione' => $status_azione,
                        'st_azione' => $st_azione,
                        'oggetto' => $oggetto,
                        'oggetto_readonly' => $oggetto_readonly,
                        'aperto_da' => $aperto_da,
                        'in_carico_a_options' => $impiegati_eza,
                        'in_carico_a_firstname' => $in_carico_a_firstname,
                        'data_apertura' => $data_apertura,
                        'data_ultima_com' => $data_ultima_com,
                        'azione_padre' => $azione_padre,
                        'data_azione' => $data_azione,
                        'data_azione_ore' => $data_azione_ore,
                        'data_azione_min' => $data_azione_min,
                        'tr_promemoria' => $tr_promemoria,
                        'azione_note' => $azione_note,
                        'collega_a' => $collega_a,
                        'errore' => $errore,
                    );
                    /* Fine Generale */
                }

                /* Cronologia */
                $messaggia = Db::getInstance()->executeS("SELECT * FROM action_message am WHERE am.id_action = ".Tools::getValue('id_action')."");

                foreach($messaggia as &$messaggioa) {
                    $employee_from = new Employee($messaggioa['action_m_from']);
                    $employee_to = new Employee($messaggioa['action_m_to']);

                    $messaggioa['da'] = $employee_from->firstname." ".substr($employee_from->lastname, 0, 1).".";
                    $messaggioa['data'] = date("d-m-Y H:i:s", strtotime($messaggioa['date_add']));

                    if(!empty($messaggioa['file_name'])) {
                        $allegatia = explode(";", $messaggioa['file_name']);
                        $nalla = 1;
                        foreach($allegatia as $allegatoa) {
                            if(strpos($allegatoa, ":::")) {
                                $parti = explode(":::", $allegatoa);
                                $nomeveroa = $parti[1];
                            }
                            else {
                                $nomeveroa = $allegatoa;
                            }

                            if($allegatoa == "") { } 
                            else {
                                if($nalla == 1) { } 
                                else 
                                    $allegati .= " - ";

                                $allegati .= '<a href="ajax.php?openAllegato=y&id_customer='.$customer->id.'&viewcustomer&tab_name=todo&azione=todo&viewaction&id_action='.$azione['id_action'].'&filename='.$allegatoa.'&token='.$this->token.'"><span style="color:red">'.$nomeveroa.'</span></a>';
                                $nallp++;
                            }
                        }
                    }
                    else 
                        $allegati = 'Nessun allegato';

                    $messaggioa['allegati'] = $allegati;
                    
                    $messaggioa['employee_to_name'] = $employee_to->firstname;
                    $messaggioa['in_carico_a_name'] = ($messaggioa['in_carico'] > 0 ? Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$messaggioa['in_carico']) : '--');
                    
                    $cc = Db::getInstance()->getValue("SELECT cc FROM ticket_cc WHERE type = 'a' AND msg = ".$messaggioa['id_action_message']);
                    if($cc){
                        $ccs = explode(";", $cc);
                        
                        foreach ($ccs as $conoscenza) {
                            $imp = Db::getInstance()->getValue("SELECT firstname FROM "._DB_PREFIX_."employee WHERE id_employee = '".$conoscenza."'");
                            $cc_str .= $imp." ";
                        }
                        
                        $messaggioa['cc'] = $cc_str;
                    }
                    else
                        $messaggioa['cc'] = 'Nessuno';
                }

                if (Tools::isSubmit('submitActionReply')) 
                {
                    $files = array();
                    $fdata = $_FILES['joinFile'];

                    if(is_array($fdata['name'])){
                        $count_name2 = count($fdata['name']);
                        for($i=0; $i<$count_name2; ++$i){
                            $files[] = array(
                                'name'    =>$fdata['name'][$i],
                                'tmp_name'=>$fdata['tmp_name'][$i],
                                'type' => $fdata['type'][$i],
                                'size' => $fdata['size'][$i],
                                'error' => $fdata['error'][$i],
                                
                            );
                        }
                    }
                    else 
                        $files[] = $fdata;
                                        
                    $attachments = array();
                    
                    foreach($files as $file) {
                        if (isset($files) AND !empty($file['name']) AND $file['error'] != 0)
                            $this->_errors[] = Tools::displayError('An error occurred with the file upload.');
                            
                        if (!empty($file['name'])) {
                            $extension = array('.txt', '.rtf', '.doc', '.docx', '.pdf', '.zip', '.png', '.jpeg', '.gif', '.jpg');
                            $filename = md5(uniqid().substr($file['name'], -5));
                            $fileAttachment['content'] = file_get_contents($file['tmp_name']);
                            $fileAttachment['name'] = $file['name'];
                            $fileAttachment['mime'] = $file['type'];
                        }
                        
                        if (isset($filename) AND rename($file['tmp_name'], _PS_MODULE_DIR_.'../upload/'.$filename)) {
                            $filename = $filename.":::".$file['name'];
                            $file_name .= $filename.";";
                        }
                        
                        $attachments[] = $fileAttachment;
                    }

                    if(Tools::getValue('nuovaazione') == 1) 
                    {
                        $ultimothread = Db::getInstance()->getValue("SELECT id_action FROM action_thread ORDER BY id_action DESC");
                        $thread = $ultimothread+1;
                                                    
                        $cstmr_r = ($customer->is_company ? $customer->company : $customer->firstname." ".$customer->lastname);
                            
                        $subject = "*** AZIONE su cliente ".$cstmr_r." - Tipo azione: ".Tools::getValue('action_type')." ***";
                        
                        $action_date_array = explode("-",Tools::getValue('action_date'));
                        $action_date = $action_date_array[2]."-".$action_date_array[1]."-".$action_date_array[0]." ".Tools::getValue('action_hours').":".Tools::getValue('action_minutes').":00";
                        
                        foreach ($_POST['promemoria_nuovo'] as $id_promemoria=>$promemoria_id) { 
                            Db::getInstance()->execute('INSERT INTO ticket_promemoria (msg, tipo_att, ora, conto, tipo, status) VALUES ('.$thread.', "a", "'.$action_date.'" - INTERVAL '.$_POST['action_minuti'][$id_promemoria].' '.$_POST['action_tipo_promemoria'][$id_promemoria] .', "'.$_POST['action_minuti'][$id_promemoria].'", "'.$_POST['action_tipo_promemoria'][$id_promemoria].'", "open")');
                        }
                        
                        if(strpos(Tools::getValue('action_date'), '0000') !== false) {
                            $action_date = date("Y-m-d H:i:s");
                        }

                        Customer::Storico($thread, 'A', $mio_id, 13);
                        
                        Db::getInstance()->execute("INSERT INTO action_thread (id_action, action_type, id_customer, subject, action_from, action_to, action_date, status, riferimento, date_add, date_upd, note) VALUES ('$thread', '".Tools::getValue('action_type')."', '".$customer->id."', '".addslashes($subject)."', '".$mio_id."', '".Tools::getValue('in_carico_a_a')."', '".$action_date."', '".addslashes(Tools::getValue('statusa'))."', '".Tools::getValue('riferimento')."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."', '".addslashes(Tools::getValue('action_note'))."')");   
                        Db::getInstance()->execute("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, action_message, file_name, telefono_cliente, date_add, date_upd) VALUES (NULL, '$thread', ".$customer->id.", '".$mio_id."', '".Tools::getValue('in_carico_a_a')."', '".Tools::getValue('in_carico_a_a')."', '".addslashes(Tools::getValue('messaggioa'))."', '".addslashes($file_name)."', '".Tools::getValue('telefono_cliente')."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')");  
                        
                        if(Tools::getIsset('riferimentoo') && Tools::getValue('riferimentoo') != '')
                        {	
                            if(substr(Tools::getValue('riferimentoo'),0,1) == 'C')
                                $note_da_copiare = Db::getInstance()->executeS('SELECT * FROM cart_note WHERE id_cart = "'.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo'))).'"');
                            else if(substr(Tools::getValue('riferimentoo'),0,1) == 'O')
                                $note_da_copiare = Db::getInstance()->executeS('SELECT * FROM cart_note WHERE id_cart = "'.Db::getInstance()->getValue('SELECT id_cart FROM orders WHERE id_order = '.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo')))).'"');
                            else
                                $note_da_copiare = Db::getInstance()->executeS('SELECT * FROM note_attivita WHERE id_attivita = "'.substr(Tools::getValue('riferimentoo'),1,strlen(Tools::getValue('riferimentoo'))).'" AND tipo_attivita = "'.substr(Tools::getValue('riferimentoo'),0,1).'"');

                            foreach($note_da_copiare as $ndc)
                                Db::getInstance()->execute('INSERT INTO note_attivita (id_note, id_attivita, tipo_attivita, id_employee, note, date_add, date_upd) VALUES (NULL, '.$thread.', "A", "'.$ndc['id_employee'].'","'.$ndc['note'].'", "'.$ndc['date_add'].'","'.$ndc['date_upd'].'")');
                        }
                        
                        $idmessaggio = Db::getInstance()->Insert_ID();
                    }
                    else 
                    {
                        $thread = $azione['id_action'];
                        $current_action_status = Db::getInstance()->getValue('SELECT status FROM action_thread WHERE id_action = "'.$thread.'"');
                        $precedente_status = Db::getInstance()->getValue("SELECT status FROM action_thread WHERE id_action = '".$azione['id_action']."'");
                        if($precedente_status != Tools::getValue('statusa'))
                        {	
                            switch(Tools::getValue('statusa'))
                            {
                                case 'open': $switch_status = 3; break;
                                case 'pending1': $switch_status = 4; break;
                                case 'closed': $switch_status = 5; break;
                                default: $switch_status = 10; break;
                            }	
                            
                            Customer::Storico($azione['id_action'], 'A', $mio_id, $switch_status);
                        }
                        
                        $precedente_incarico = Db::getInstance()->getValue("SELECT action_to FROM action_thread WHERE id_action = ".$azione['id_action']);
                        
                        if($precedente_incarico != Tools::getValue('in_carico_a_a'))
                            Customer::Storico($azione['id_action'], 'A', $mio_id, 2, Tools::getValue('in_carico_a_a'));
                    
                        Db::getInstance()->execute("UPDATE action_thread SET status = '".addslashes(Tools::getValue('statusa'))."', date_upd = '".date("Y-m-d H:i:s")."', action_to = '".Tools::getValue('in_carico_a_a')."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_action = '".$azione['id_action']."'");  
                    
                        Customer::chiudiOrdineDaAzione($azione['id_action']);
                        
                        if(isset($_POST['modifica_messaggio_azione_id'])) {
                            Customer::Storico($azione['id_action'], 'A', $mio_id, 16);
                            Db::getInstance()->execute("UPDATE action_message SET action_message = '".addslashes(Tools::getValue('messaggioa'))."' WHERE id_action_message = ".$_POST['modifica_messaggio_azione_id']); 
                            
                            $idmessaggio = $_POST['modifica_messaggio_azione_id'];
                        }
                        else {
                            Customer::Storico($azione['id_action'], 'A', $mio_id, 15);
                            Db::getInstance()->execute("INSERT INTO action_message (id_action_message, id_action, id_customer, action_m_from, action_m_to, in_carico, action_message, file_name, telefono_cliente, date_add, date_upd) VALUES (NULL, '$thread', ".$customer->id.", '".$mio_id."', '".Tools::getValue('in_carico_a_a')."', '".Tools::getValue('in_carico_a_a')."', '".addslashes(Tools::getValue('messaggioa'))."', '".addslashes($file_name)."', '".Tools::getValue('telefono_cliente')."', '".date("Y-m-d H:i:s")."',  '".date("Y-m-d H:i:s")."')"); 
                            
                            $idmessaggio = Db::getInstance()->Insert_ID();
                            $subject = Db::getInstance()->getValue('SELECT subject FROM action_thread WHERE id_action = '.$thread);
                            
                            if(substr($subject,0,20) == '*** VERIFICA TECNICA')
                            {	
                                $id_cart = Db::getInstance()->getValue('SELECT id_cart FROM '._DB_PREFIX_.'orders WHERE id_order = '.substr($subject, -6, 5));
                                $note_private = Db::getInstance()->getValue("SELECT note_private FROM "._DB_PREFIX_."cart WHERE id_cart = ".$id_cart);
                                $note_private .= addslashes(Tools::getValue('messaggioa'));
                                
                                if((Tools::getValue('cambiastatusa') == 'closed' || Tools::getValue('statusa') == 'closed' )  && $current_action_status != 'closed')
                                {
                                    Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$mio_id.', '.substr($subject, -6, 5).', 25, "'.date('Y-m-d H:i:s').'")');
                                    // Product::RecuperaCSV(substr($subject, -6, 5));
                                }
                                else if((Tools::getValue('cambiastatusa') == 'open' || Tools::getValue('statusa') == 'open' || Tools::getValue('cambiastatusa') == 'pending1' || Tools::getValue('statusa') == 'pending1') && $current_action_status == 'closed')
                                {
                                    Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'order_history (id_order_history, id_employee, id_order, id_order_state, date_add) VALUES (NULL, '.$mio_id.', '.substr($subject, -6, 5).', 24, "'.date('Y-m-d H:i:s').'")');
                                    // Product::RecuperaCSV(substr($subject, -6, 5));
                                }
                                
                                Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'cart SET note_private = "'.addslashes($note_private).'" WHERE id_cart = '.$id_cart);
                                Db::getInstance()->execute('UPDATE carrelli_creati SET note_private = "'.addslashes($note_private).'" WHERE id_cart = '.$id_cart);
                            }
                            else if(substr($subject,0,10) == '*** RMA N.')
                            {	
                                if(Tools::getValue('cambiastatusa') == 'closed'  && $current_action_status != 'closed')
                                    Db::getInstance()->execute('UPDATE rma SET test = 0 WHERE id_customer_thread = '.substr(Db::getInstance()->getValue('SELECT riferimento FROM action_thread WHERE id_action = '.Tools::getValue('id_action')),1));
                            }
                        }
                    }

                    if(Tools::getIsset('chiudi_padre'))
                        Customer::chiudiPadre(Db::getInstance()->getValue('SELECT riferimento FROM action_thread WHERE id_action = '.$thread));
                    
                    // CORREGGERE: manca file
                    //$file_csv = fopen("../cms/quotazioni/azioni.csv","a+");
                    
                    $in_carico_a =  Db::getInstance()->getValue("SELECT firstname FROM "._DB_PREFIX_."employee WHERE id_employee = ".Tools::getValue('in_carico_a_a')."");
                    $prima_azione = Db::getInstance()->getValue("SELECT id_action_message FROM action_message WHERE id_action = ".$thread." ORDER BY id_action_message ASC");
                        
                    if(!$prima_azione)
                        $prima_azione = $idmessaggio;
                    
                    $messaggiopercsv = Tools::getValue('messaggioa');
                    $messaggiopercsv = strip_tags($messaggiopercsv);
                    $messaggiopercsv = str_replace(";", ",", $messaggiopercsv);
                        
                    $messaggiopercsv = str_replace(chr(10), " ", $messaggiopercsv); //remove carriage returns
                    $messaggiopercsv = str_replace(chr(13), " ", $messaggiopercsv); //remove carriage returns	
                    $ref = rand(0, 99999); 
                    $ref2 = rand(0, 99999);
                    
                    $riga_richiesta = "".date("d/m/Y H:i:s").";".date('Ymd')."-".$ref."-".$ref2.";".$in_carico_a.";".Tools::getValue('action_type').";".Tools::getValue('statusa').";".$messaggiopercsv.";".$thread.";".$idmessaggio.";".$prima_azione.";".$customer->id."\n";

                    $subj_per_ctrl = Db::getInstance()->getValue("SELECT subject FROM action_thread WHERE id_action = ".$thread);
                    $pos = strpos($subj_per_ctrl,"ti richiamiamo noi");
                    
                    //if (strlen(stristr($subj_per_ctrl,"ti richiamiamo noi"))>0) // Commentato 1.4
                        //@fwrite($file_csv,$riga_richiesta); // CORREGGERE: manca file; decommentare quando ci sarà
                    
                    //@fclose($file_csv); // CORREGGERE: manca file

                    if((Tools::getValue('messaggioa')) == '')
                        $testo_azione = "Il testo di questa azione &egrave; vuoto.";
                    else
                        $testo_azione = "Testo azione: ".(Tools::getValue('messaggioa'));
                    
                    $cliente = ($customer->is_company ? $customer->company : $customer->firstname." ".$customer->lastname);
                    
                    $cc = "";
                    $cc_str = "";

                    foreach($_POST['conoscenza_a'] as $conoscenza) {
                        $cc_str = $cc_str.Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$conoscenza).'';
                    }
                    
                    foreach($_POST['conoscenza_a'] as $conoscenza) 
                    {
                        $cc .= $conoscenza.";";
                        $tokenimp = Tools::getAdminToken("AdminCustomers".(int)Tab::getIdFromClassName("AdminCustomers").$conoscenza);
                        $linkimp = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&tab_name=todo&azione=todo&viewaction&id_action=".$thread."&token=".$tokenimp;			
                                            
                        if((Tools::getValue('messaggioa')) == '')
                            $testo_azione = "Il testo di questa azione &egrave; vuoto.";
                        else
                            $testo_azione = "Testo azione: ".(Tools::getValue('messaggioa'));
                                    
                        $params = array(
                        '{reply}' => "Ti &egrave; stata inviata una azione in copia conoscenza su Ezdirect.<br /><br />
                        Azione aperta da: <strong>".$employeemess->firstname."</strong><br /><br />
                        Azione in carico a: <strong>".$in_carico_a."</strong><br /><br />
                        Tipo azione: <strong>".Tools::getValue('action_type')."</strong><br /><br />".$testo_azione."<br /><br />
                        ".($cc_str != '' ? "Il messaggio &egrave; stato inviato in conoscenza a: ".$cc_str."<br /><br />" : "")."
                        Per aprire questa azione, <a href='".$linkimp."'>clicca su questo link</a>.");
                    
                        // TEST! DECOMMENTARE
                        //$mailconoscenza = Db::getInstance()->getValue("SELECT email FROM "._DB_PREFIX_."employee WHERE id_employee = ".$conoscenza);
                        $mailconoscenza = "carolina.carusi@ezdirect.it";
                        
                        // TEST template; sostituire 'msg_base' con 'senzagrafica'
                        Mail::Send((int)$this->context->language->id, 'msg_base', Mail::l('Azione in copia conoscenza', (int)$this->context->language->id), 
                        $params, $mailconoscenza, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
                        _PS_MAIL_DIR_, true);
                    }

                    $tokenimp = Tools::getAdminToken("AdminCustomers".(int)Tab::getIdFromClassName("AdminCustomers").Tools::getValue('in_carico_a_a'));
                    $linkimp = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".Tools::getValue('id_customer')."&viewcustomer&tab_name=todo&azione=todo&viewaction&id_action=".$thread."&token=".$tokenimp;			

                    $params = array(
                    '{reply}' => "Ti &egrave; stata inviata una azione su Ezdirect.<br /><br />Azione aperta da: <strong>".$employeemess->firstname."</strong><br /><br />Cliente: <strong>".$cliente."</strong><br /><br />Azione in carico a: <strong>".$in_carico_a."</strong><br /><br />Tipo azione:<strong>".Tools::getValue('action_type')."</strong><br /><br />".$testo_azione."<br /><br />".($cc_str != '' ? "Il messaggio &egrave; stato inviato in conoscenza a: ".$cc_str."<br /><br />" : "")."Per leggere questa azione o per rispondere, <a href='".$linkimp."'>clicca su questo link</a>.");
                    
                    if(!empty($_POST['conoscenza_a'])) {
                        Db::getInstance()->execute("INSERT INTO ticket_cc (msg, type, cc) VALUES (".$idmessaggio.", 'a', '".$cc."')"); 
                    }
                    
                    if(Tools::getValue('in_carico_a_a') != 0) {
                        // TEST! DECOMMENTARE
                        //$mail_incarico_a = Db::getInstance()->getValue('SELECT email FROM '._DB_PREFIX_.'employee WHERE id_employee = '.Tools::getValue('in_carico_a_a'));
                        $mail_incarico_a = "carolina.carusi@ezdirect.it";
                        if(Tools::getValue('in_carico_a_a') == $mio_id) {
                            // niente
                        }
                        else { // TEST template; sostituire 'msg_base' con 'action'
                            Mail::Send($this->context->language->id, 'msg_base', Mail::l('Azione assegnata a te su Ezdirect', $this->context->language->id), 
                            $params, $mail_incarico_a, NULL, 'nonrispondere@ezdirect.it', 'Ezdirect', $attachments, NULL, 
                            _PS_MAIL_DIR_, true);
                        }
                    }
                    
                    Tools::redirectAdmin(self::$currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&tab_name=todo&conf=4&token='.$this->token);
                }
                else 
                {
                    if(isset($_GET['actionreply'])) { // CORREGGERE: dove viene settato?
                        // tpl (correggere link): Risposta inviata con successo.<br /> <a class="button" href="'.$currentIndex.'&id_customer='.$customer->id.'&viewcustomer&viewaction&id_action='.$azione['id_action'].'&token='.$this->token.'&tab-container-1=10"><strong>Clicca qui se vuoi rispondere nuovamente.</strong></a><br /><br />
                    } 
                    else if(isset($_GET['actionreply2'])) { // CORREGGERE: dove viene settato?
                        // tpl: Azione aperta con successo. <br /><br />
                    }
                    else if(!isset($_GET['actionreply'])) {
                        // Rispondi a questa azione
                        if(isset($_POST['modifica_messaggio_azione'])) {
                            if(Tools::isSubmit('cancella_messaggio_submit'))
                            {
                                $id_action = Db::getInstance()->getValue('SELECT id_action FROM action_message WHERE id_action_message = '.Tools::getValue('modifica_messaggio_azione_id'));
                                Db::getInstance()->getInstance()->execute('DELETE FROM action_message WHERE id_action_message = '.Tools::getValue('modifica_messaggio_azione_id'));
                                Tools::redirectAdmin(self::$currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&tab_name=todo&azione=todo&viewaction&id_action='.$id_action.'&conf=4&token='.$this->token);
                            }
                        }
                    }

                    if(isset($_GET['aprinuovaazione'])) {
                        $impez_selected = $mio_id;
                    }
                    else {
                        $impez_selected = $azione['action_to'];
                    }

                    // Tel. cliente
                    $telefoni_cliente = Db::getInstance()->executeS('
                        SELECT phone AS tel FROM '._DB_PREFIX_.'address WHERE id_customer = '.$customer->id.' AND deleted = 0 AND active = 1
                        UNION
                        SELECT phone_mobile AS tel FROM '._DB_PREFIX_.'address WHERE id_customer = '.$customer->id.' AND deleted = 0 AND active = 1
                        UNION
                        SELECT phone AS tel FROM persone WHERE id_customer = '.$customer->id.'
                        UNION
                        SELECT phone_2 AS tel FROM persone WHERE id_customer = '.$customer->id.'
                        UNION
                        SELECT phone_mobile AS tel FROM persone WHERE id_customer = '.$customer->id.'
                    ');

                    $telefoni_options = '';
                        
                    $attuale_tel = Db::getInstance()->getValue('SELECT telefono_cliente FROM action_message WHERE id_action = '.Tools::getValue('id_action').' ORDER BY id_action_message DESC');
                    $i_tels = 0;

                    foreach($telefoni_cliente as $tel)
                    {
                        if($i_tels == 0 && $tel['tel'] != '')
                            $primo_tel = $tel['tel'];
                        
                        if($tel['tel'] != '')
                        {	
                            $telefoni_options .= '<option value="'.$tel['tel'].'" '.($tel['tel'] == $attuale_tel ? ' selected="selected"' : '').'>'.$tel['tel'].'</option>';
                            $i_tels++;
                        }	
                    }

                    if($attuale_tel == '')
                        $attuale_tel = $primo_tel;

                    $marche = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'manufacturer ORDER BY name ASC');
                    $categorie = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'category c JOIN '._DB_PREFIX_.'category_lang cl ON c.id_category = cl.id_category WHERE cl.id_lang = '.$this->context->language->id.' AND c.id_parent = 1 ORDER BY cl.name ASC');
                    $fornitori = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'supplier WHERE id_supplier = 8 OR id_supplier = 11 OR id_supplier = 42 ORDER BY name ASC');

                    $riferimento = (Tools::getValue('riferimento') ? Tools::getValue('riferimento') : Db::getInstance()->getValue('SELECT riferimento FROM action_thread WHERE id_action = '.$azione['id_action']));
                    $statusPadre = Customer::statusPadre($riferimento);
                }

                $cronologia = array(
                    'azione' => $azione,
                    'thread' => $thread,
                    'messaggia' => $messaggia,
                    'impiegati_eza' => $impiegati_eza,
                    'impez_selected' => $impez_selected,
                    'telefoni_options' => $telefoni_options,
                    'attuale_tel' => $attuale_tel,
                    'marche' => $marche,
                    'categorie' => $categorie,
                    'fornitori' => $fornitori,
                    'precompilati' => $precompilati,
                    'riferimento' => $riferimento,
                    'statusPadre' => $statusPadre,
                );
                /* Fine Cronologia */

                if(Tools::getIsset('id_action')){

                    /* Gerarchia */
                    $first = Customer::HierarchyFirst($azione['id_action'], 'A');
                    
                    // Correggere: manca la class tree-menu
                    $tree_ul = '<ul class="tree-menu"><li>'.Customer::trovaSiglaLinkPerAlbero(substr($first, 1), substr($first, 0, 1), '').'';
                    $replace .= str_replace('<ul></ul>', '', Customer::HierarchyFindChildren($first, $children, 'A'.$azione['id_action']));
                    $tree_ul .= $replace;
                    $tree_ul .= '</li></ul>';
                    
                    $gerarchia = array(
                        'tree_ul' => $tree_ul,
                    );
                    /* Fine Gerarchia */

                    /* Storico */
                    $storico_ticket = Db::getInstance()->executeS('SELECT * FROM storico_attivita WHERE id_attivita = '.$azione['id_action'].' AND tipo_attivita = "A" ORDER BY data_attivita DESC, desc_attivita DESC');

                    foreach($storico_ticket as &$storico)
                    {
                        if(is_numeric(substr($storico['desc_attivita'], -1)))
                        {	
                            $employee = filter_var($storico['desc_attivita'], FILTER_SANITIZE_NUMBER_INT);
                            $name_employee = Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$employee);
                            $desc_attivita = 'Ha messo l\'attivit&agrave; in carico a '.$name_employee;
                        }
                        else
                            $desc_attivita = $storico['desc_attivita'];

                        $storico['azione'] = $desc_attivita;
                        $storico['data_attivita'] = Tools::displayDate($storico['data_attivita'], $this->context->language->id, true);
                        $storico_employee = Db::getInstance()->getValue('SELECT concat(firstname, " ", LEFT(lastname,1),".") FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$storico['id_employee']);

                        $storico['persona'] = ($storico['id_employee'] == 0 ? 'Cliente' : $storico_employee);
                    }

                    $storico = array(
                        'storico_ticket' => $storico_ticket,
                    );
                    /* Fine Storico */  
                }

                $todo_view = array(
                    'generale' => $generale,
                    'cronologia' => $cronologia,
                    'gerarchia' => $gerarchia,
                    'storico' => $storico,
                    /* Variabili usate sia in Generale (view/edit) che in Cronologia (add) */
                    'action_types' => $action_types,
                );
            }
            else
                $todo_list = Customer::BuildListing($customer->id, 'to-do');

            $specific = 'n';

            $todo = array(
                'todo_list' => $todo_list,
                'todo_view' => $todo_view, // Variabile usata in add/view/edit
                'specific' => $specific,
            );

        }
        /* FINE TO DO */

        /* INIZIO Stat */
        if($tab_name == 'stats' && !$is_agente){ // tab-container-1=8

            $interesse = $customer->getInteresse();
            
            foreach ($interesse as &$intx) {
                /* Sostituisce la funzione Category::getCatNameById(); */
                $category = new Category($intx['id_category_default'], $this->context->language->id);
                $intx['category'] = $category->name;
            }

            // Correggere: Prende un prodotto a caso del primo/ultimo ordine, che prodotto dovrei far vedere?
            $primo_acquisto = $customer->ultimoAcquistoPrimoAcquisto("primo");
            $ultimo_acquisto = $customer->ultimoAcquistoPrimoAcquisto("ultimo");

            $acquisti_per_anno = $customer->getOrdersByYear();

            foreach($acquisti_per_anno as &$acquisti) {
                $acquisti['totale'] = Tools::displayPrice($acquisti['totale'], $this->context->currency->id);
                $acquisti['totale_senza_iva'] = Tools::displayPrice($acquisti['totale_senza_iva'], $this->context->currency->id);
            }

            foreach ($orders AS $order)
                if ($order['valid']) {
                    $totalOKconiva += $order['total_paid_real'];
                    $totalOKsenzaiva += $order['total_products'];
                }

            $totale_ok_con_iva = Tools::displayPrice($totalOKconiva, $this->context->currency->id);
            $totale_ok_senza_iva = Tools::displayPrice($totalOKsenzaiva, $this->context->currency->id);

            $prodotti_ordinati = $products;
            foreach($prodotti_ordinati as &$product){
                $manufacturer = new Manufacturer($product['id_manufacturer'], $this->context->language->id);
                $category = new Category($product['id_category_default'], $this->context->language->id);
                $product['manufacturer'] = $manufacturer->name;
                $product['category'] = $category->name;
                $product['total'] = $customer->getTotalBoughtProducts($product['product_id'], $customer->id);
            }

            $stat = array(
                'interesse' => $interesse, 
                'primo_acquisto' => $primo_acquisto,
                'ultimo_acquisto' => $ultimo_acquisto,
                'acquisti_per_anno' => $acquisti_per_anno,
                'totale_ok_con_iva' => $totale_ok_con_iva,
                'totale_ok_senza_iva' => $totale_ok_senza_iva,
                'prodotti_ordinati' => $prodotti_ordinati, // = $products
                /*'prodotti_nel_carrello' => $prodotti_nel_carrello, // = $interested
                'ultime_connessioni' => $ultime_connessioni, // = $connections
                'referrers' => $referrers
                */
            );

        }
        /* FINE Stat */

        $count_contratti = Db::getInstance()->getValue("SELECT COUNT(id_contratto) FROM "._DB_PREFIX_."contratto_assistenza WHERE id_customer = ".$customer->id);
        $count_contratti_attivi = Db::getInstance()->getValue("SELECT COUNT(*) FROM "._DB_PREFIX_."contratto_assistenza WHERE id_customer = ".$customer->id." AND status = 0");

        /* INIZIO Contratti */
        if($tab_name == 'contracts' && !$is_agente){ // tab-container-1=13

            // DELETE
            if(Tools::getIsset('azione') && Tools::getValue('azione') == 'contratto_delete') {
                Db::getInstance()->executeS('DELETE FROM '._DB_PREFIX_.'contratto_assistenza WHERE id_contratto = '.Tools::getValue('id_contratto'));
                Db::getInstance()->executeS('DELETE FROM '._DB_PREFIX_.'contratto_assistenza_prodotti WHERE id_contratto = '.Tools::getValue('id_contratto'));
                
                Tools::redirectAdmin(self::$currentIndex.'&'.$this->identifier.'='.$customer->id.'&viewcustomer&tab_name=contracts&conf=4&token='.$this->token);
            }

            // ADD E EDIT
            if(Tools::getIsset('azione') && (Tools::getValue('azione') == 'contratto_add' || Tools::getValue('azione') == 'contratto_edit')) {

                if(Tools::getValue('azione') == 'contratto_edit')
                    $edit = 1;
                else
                    $edit = 0;

                $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'contratti.js'); // correggere: da finire!

                // Salvataggio
                if(Tools::getValue('submitted') == 'y')
                {
                    if(Tools::getIsset('blocco_amministrativo'))
                        $blocco_amministrativo = 1;
                    else
                        $blocco_amministrativo = 0;

                    $data_registrazione = date("Y-m-d H:i:s", strtotime(Tools::getValue('data_registrazione')));
                    $data_inizio = date("Y-m-d H:i:s", strtotime(Tools::getValue('data_inizio')));
                    $data_fine = date("Y-m-d H:i:s", strtotime(Tools::getValue('data_fine'))); // correggere: manca un valore di default in caso non fosse valorizzato
                    
                    if($edit){
                        $dati_contratto = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'contratto_assistenza WHERE id_contratto = '.Tools::getValue('id_contratto'));
                        
                        switch($storico_contratto['tipo']) {
                            case 1: $tipo_s = 'Base'; break;
                            case 2: $tipo_s = 'Top'; break;
                            case 3: $tipo_s = 'Gold'; break;
                            case 4: $tipo_s = 'Teleassistenza'; break;
                            case 5: $tipo_s = 'Noleggio'; break;
                            default: $tipo_s = 'Base'; break;
                        }
                        
                        switch($storico_contratto['status']) {
                            case 0: $status_s = 'Attivo'; break;
                            case 1: $status_s = 'Sospeso'; break;
                            case 2: $status_s = 'Cancellato'; break;
                            case 3: $status_s = 'Disdetto'; break;
                            case 4: $status_s = 'Scaduto'; break;
                            default: $status_s = 'Attivo'; break;
                        }
                        
                        $storico_contratto = '<table class=\'table\'><tr><th>Rif. fattura</th><th>Rif. ordine</th><th>Rif. noleggio</th><th>Tipo</th><th>Status</th><th>Prezzo</th><th>Pagamento</th><th>CIG</th><th>CUP</th><th>IPA</th><th>Data reg.</th><th>Inizio</th><th>Fine</th></tr><tr><td>'.$dati_contratto['rif_fattura'].'</td><td>'.$dati_contratto['rif_ordine'].'</td><td>'.$dati_contratto['rif_noleggio'].'</td><td>'.$tipo_s.'</td><td>'.$status_s.'</td><td>'.$dati_contratto['prezzo'].'</td><td>'.$dati_contratto['pagamento'].'</td><td>'.$dati_contratto['cig'].'</td><td>'.$dati_contratto['cup'].'</td><td>'.$dati_contratto['univoco'].'</td><td>'.date('d/m/Y',strtotime($dati_contratto['data_registrazione'])).'</td><td>'.date('d/m/Y',strtotime($dati_contratto['data_inizio'])).'</td><td>'.date('d/m/Y',strtotime($dati_contratto['data_fine'])).'</td></tr></table>';
                        
                        // CORREGGERE: non inserisce $storico_contratto ma sempre 0!
                        Customer::Storico(Tools::getValue('id_contratto'), 'CO', $mio_id, $storico_contratto);
                        
                        Db::getInstance()->execute("UPDATE "._DB_PREFIX_."contratto_assistenza SET status = '".Tools::getValue('status')."', tipo = '".Tools::getValue('tipo')."', data_registrazione = '".$data_registrazione."', data_inizio = '".$data_inizio."', data_fine = '".$data_fine."', indirizzo = '".Tools::getValue('indirizzo_contratto')."',  cig = '".Tools::getValue('cig')."',  cup = '".Tools::getValue('cup')."', univoco = '".Tools::getValue('univoco')."', prezzo = '".str_replace(",",".",Tools::getValue('totale_importo'))."',  periodicita = '".Tools::getValue('periodicita')."', competenza_dal = '".Tools::getValue('competenza_dal')."', competenza_al = '".Tools::getValue('competenza_al')."', cadenza = '".Tools::getValue('cadenza')."', pagamento = '".Tools::getValue('pagamento')."', blocco_amministrativo = '".$blocco_amministrativo."', rif_fattura = '".Tools::getValue('rif_fattura')."', pagato = '".(Tools::getIsset('pagato') ? 1 : 0)."', fatturato = '".(Tools::getIsset('fatturato') ? 1 : 0)."', rif_noleggio = '".Tools::getValue('rif_noleggio')."', rif_ordine = '".Tools::getValue('rif_ordine')."', note_private = '".addslashes(Tools::getValue('note_private'))."', date_upd = '".date("Y-m-d H:i:s")."' WHERE id_contratto = '".Tools::getValue('id_contratto')."'");
                        Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."contratto_assistenza_prodotti WHERE id_contratto = '".Tools::getValue('id_contratto')."'");
                    }
                    else {
                        // Eliminato da ps_customer; correggere: eliminarlo anche da ps_contratto_assistenza
                        //$codice_spring = Db::getInstance()->getValue("SELECT codice_spring FROM customer WHERE id_customer = ".Tools::getValue('id_customer'));
                        Db::getInstance()->execute("INSERT INTO "._DB_PREFIX_."contratto_assistenza (id_contratto, id_customer, status, tipo, descrizione, data_registrazione, data_inizio, data_fine, cig, cup, univoco, indirizzo, prezzo, periodicita, competenza_dal, competenza_al, cadenza, pagamento, blocco_amministrativo, rif_fattura, rif_noleggio, rif_ordine, note_private, date_add, date_upd) VALUES ('".Tools::getValue('id_contratto')."', '".Tools::getValue('id_customer')."', '".Tools::getValue('status')."', '".Tools::getValue('tipo')."', '', '".$data_registrazione."', '".$data_inizio."', '".$data_fine."', '".Tools::getValue('cig')."', '".Tools::getValue('cup')."', '".Tools::getValue('univoco')."', '".Tools::getValue('indirizzo_contratto')."', '".str_replace(",",".",Tools::getValue('totale_importo'))."', '".Tools::getValue('periodicita')."', '".$data_inizio."', '".$data_fine."', '".Tools::getValue('cadenza')."', '".Tools::getValue('pagamento')."', '".$blocco_amministrativo."', '".Tools::getValue('rif_fattura')."', '".Tools::getValue('rif_noleggio')."', '".Tools::getValue('rif_ordine')."', '".addslashes(Tools::getValue('note_private'))."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')");
                    }
                                        
                    foreach($_POST['prodotto'] as $id_product_g => $id_product) {
                        $codice_prodotto = Db::getInstance()->getValue("SELECT reference FROM "._DB_PREFIX_."product WHERE id_product = ".$id_product_g);
                        $descrizione_prodotto = Db::getInstance()->getValue("SELECT name FROM "._DB_PREFIX_."product_lang WHERE id_lang = ".$this->context->language->id." AND id_product = ".$id_product_g);

                        Db::getInstance()->execute("INSERT INTO "._DB_PREFIX_."contratto_assistenza_prodotti (id_contratto, id_product, codice_prodotto, descrizione_prodotto, quantita, prezzo, seriale, note, indirizzo, date_add, date_upd) VALUES ('".Tools::getValue('id_contratto')."', ".$id_product_g.", '".$codice_prodotto."', '".$_POST['prodotto_descrizione'][$id_product_g]."', '".$_POST['quantita'][$id_product_g]."', '".str_replace(",",".",$_POST['prezzo_prodotto'][$id_product_g])."', '".$_POST['seriale_prodotto'][$id_product_g]."', '', '".$_POST['indirizzo_prodotto'][$id_product_g]."', '".date("Y-m-d H:i:s")."', '".date("Y-m-d H:i:s")."')");
                    }
                    
                    if((Tools::getIsset('invio_contabilita') && Tools::getValue('invio_contabilita') == 1 && !Tools::getIsset('fatturato')) || Tools::getValue('rinnova_contratto_ok') == 1)
                    {
                        if(Tools::getValue('rinnova_contratto_ok') == 1)
                            Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'contratto_assistenza SET status = 0 WHERE id_contratto = '.Tools::getValue('id_contratto'));
                        
                        $contratto = Db::getInstance()->getRow("
                            SELECT c.id_customer, c.firstname, c.lastname, c.company, c.is_company, c.email, co.data_fine, co.id_contratto, co.status, co.tipo, co.descrizione, co.prezzo 
                            FROM "._DB_PREFIX_."customer c
                            JOIN "._DB_PREFIX_."contratto_assistenza co ON co.id_customer = c.id_customer
                            WHERE co.id_contratto = '".Tools::getValue('id_contratto')."'
                        ");

                        $indirizzo = Db::getInstance()->getValue('SELECT id_address FROM '._DB_PREFIX_.'address WHERE id_customer = '.$contratto['id_customer'].' AND active = 1 AND deleted = 0 AND fatturazione = 1');
                        
                        if(Tools::getValue('rinnova_contratto_ok') == 1)
                        {	
                            Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'cart (id_cart, id_lang, id_currency, id_customer, id_address_invoice, id_address_delivery, name, cig, cup, ipa, validita, date_add, date_upd, created_by,  id_employee, in_carico_a, preventivo, note) VALUES (NULL, '.$this->context->language->id.', 1, "'.$contratto['id_customer'].'", "'.$indirizzo.'", "'.$indirizzo.'", "Rinnovo contratto", "'.$_POST['cig'].'", "'.$_POST['cup'].'", "'.$_POST['univoco'].'", "'.$contratto['data_fine'].'", "'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'", 1,1,1,1, "'.$nota_ta.'")');

                            $id_cart = Db::getInstance()->Insert_ID();
                            
                            $prodotti_per_carrello = Db::getInstance()->executeS('SELECT id_product, prezzo, quantita FROM '._DB_PREFIX_.'contratto_assistenza_prodotti WHERE id_contratto = '.$contratto['id_contratto']);
                            
                            $prezzo_prod = $contratto['prezzo'];
                            $prezzo_acquisto = (($prezzo_prod/100) * 60);
                            
                            Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'cart_product (id_cart, id_product, quantity, price, free, prezzo_acquisto, date_add) VALUES ('.$id_cart.', 336198, "1", "'.$prezzo_prod.'", "0", "'.$prezzo_acquisto.'",  "'.date('Y-m-d H:i:s').'")');
                            
                            $id_cart_ta = $id_cart;
                            // Correggere: modulo da installare? Decommentare ovunque quando funziona
                            /*
                            include("../modules/pss_clearcarts/pss_clearcarts.php");
                            include("../modules/pss_clearcarts/AdminPssClearCarts.php");
                            $apcc = new AdminPssClearCarts();
                            $apcc->orderThisCart($id_cart_ta);
                            */

                            $order = Db::getInstance()->getValue('SELECT id_order FROM '._DB_PREFIX_.'orders WHERE id_cart = '.$id_cart_ta);
                        }
                        else
                        {
                            if(!$edit){
                                $contratto['id_customer'] = Tools::getValue('id_customer');
                                $contratto['id_contratto'] = Tools::getValue('id_contratto');
                            }
                            
                            Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'cart (id_cart, id_lang, id_currency, id_customer, id_address_invoice, id_address_delivery, name, cig, cup, ipa, validita, date_add, date_upd, created_by,  id_employee, in_carico_a, preventivo, note) VALUES (NULL, '.$this->context->language->id.', 1, "'.$contratto['id_customer'].'", "'.$indirizzo.'", "'.$indirizzo.'", "Contratto N. '.$contratto['id_contratto'].'", "'.$_POST['cig'].'", "'.$_POST['cup'].'", "'.$_POST['univoco'].'", "'.$contratto['data_fine'].'", "'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'", 1,1,1,1, "'.$nota_ta.'")');

                            $id_cart = Db::getInstance()->Insert_ID();
                            
                            $prodotti_per_carrello = Db::getInstance()->executeS('SELECT id_product, prezzo, quantita FROM contratto_assistenza_prodotti WHERE id_contratto = '.$contratto['id_contratto'].'');
                            
                            // Commentato 1.4
                            //$prezzo_prod = $contratto['prezzo'];
                            //$prezzo_acquisto = (($prezzo_prod/100) * 60);
                            
                            foreach($prodotti_per_carrello as $p)
                                Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'cart_product (id_cart, id_product, quantity, price, free, prezzo_acquisto, date_add) VALUES ('.$id_cart.', '.$p['id_product'].', "'.$p['quantita'].'", "'.$p['prezzo'].'", "0", "0",  "'.date('Y-m-d H:i:s').'")') ;

                            $id_cart_ta = $id_cart;

                            include("../modules/pss_clearcarts/pss_clearcarts.php"); // serve o posso commentarlo?
                            include("../modules/pss_clearcarts/AdminPssClearCarts.php"); // serve o posso commentarlo?
                            // Commentato 1.4
                            /*
                            $apcc = new AdminPssClearCarts();
                            $apcc->orderThisCart($id_cart_ta);
                            $order = Db::getInstance()->getValue('SELECT id_order FROM orders WHERE id_cart = '.$id_cart_ta.'');
                            */
                        }

                        $token_employee = "2"; // Barbara

                        // $currentIndex = "http://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers";
                        
                        $subject = "Nuovo contratto da controllare per fatturazione";
                        $message = "
                            <html>
                                <head>
                                    <title>Nuovo contratto da controllare per fatturazione</title>
                                </head>
                                <body>
                                    Controllare <a href='".self::$currentIndex."&id_customer=".(int)$customer->id."&viewcustomer&tab_name=contracts&azione=contratto_edit&id_contratto=".Tools::getValue('id_contratto')."&token=".Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$token_employee)."'>contratto n. ".Tools::getValue('id_contratto')."</a> (ordine n. ".$order.") per fatturazione (cliente: ".($customer->is_company == 1 ? $customer->company : $customer->firstname.' '.$customer->lastname).")
                                </body>
                            </html>
                        ";
                                
                        $headers  = 'MIME-Version: 1.0' . "\n";
                        $headers .= 'Content-Type: text/html' ."\n";
                        $headers .= 'From: "Ezdirect" <info@ezdirect.it>' . "\n";
                        //mail("barbara.giorgini@ezdirect.it", $subject, $message, $headers); // disabilitato
                        //mail("federico.giannini@mail.com", $subject, $message, $headers); // abilitare x prove
                        mail("carolina.carusi@ezdirect.it", $subject, $message, $headers); // abilitato x prove
                        
                        Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'contratto_assistenza SET invio_contabilita = 1 WHERE id_contratto = '.Tools::getValue('id_contratto'));
                    }
                    
                    Tools::redirectAdmin(self::$currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&tab_name=contracts&azione=contratto_view&id_contratto='.Tools::getValue('id_contratto').'&conf=4&token='.$this->token);
                }
                else
                {
                    if($edit) {
                        $contratto = Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."contratto_assistenza WHERE id_contratto = ".Tools::getValue('id_contratto'));
                        //$prodotti = Db::getInstance()->executeS("SELECT COUNT(cap.id_product) AS conto, cap.id_product as product_id, cap.quantita, cap.prezzo, cap.indirizzo, cap.seriale, p.reference, cap.descrizione_prodotto as name FROM "._DB_PREFIX_."contratto_assistenza_prodotti cap JOIN "._DB_PREFIX_."contratto_assistenza ca ON ca.id_contratto = cap.id_contratto JOIN "._DB_PREFIX_."product p ON cap.id_product = p.id_product JOIN "._DB_PREFIX_."product_lang pl ON cap.id_product = pl.id_product WHERE cap.id_contratto = '".Tools::getValue('id_contratto')."' AND pl.id_lang = ".$this->context->language->id." GROUP BY cap.id_product");
                        // PROVA! quello commentato è quello giusto
                        $prodotti = Db::getInstance()->executeS("SELECT id_product as product_id, QUANTITA, PREZZO, SERIALE, descrizione_prodotto as name FROM "._DB_PREFIX_."contratto_assistenza_prodotti WHERE id_contratto = ".Tools::getValue('id_contratto'));
                        $storico_contratto = Db::getInstance()->executeS('SELECT * FROM storico_attivita WHERE id_attivita = '.$contratto['id_contratto'].' AND tipo_attivita = "CO"');
    
                        foreach($storico_contratto as &$storico)
                            $storico['data_attivita'] = Tools::displayDate($storico['data_attivita'], $this->context->language->id);

                        // Correggere: cosa stampo se periodicita = 0?
                        $contratto['rata'] = str_replace(".", ",", $contratto['prezzo'] / $contratto['periodicita']);

                        $contratto['data_registrazione'] = Tools::displayDate($contratto['data_registrazione'], $this->context->language->id);
                        $contratto['data_inizio'] = Tools::displayDate($contratto['data_inizio'], $this->context->language->id);
                        $contratto['data_fine'] = Tools::displayDate($contratto['data_fine'], $this->context->language->id);
                        $contratto['note_private'] = Tools::htmlentitiesUTF8($contratto['note_private']);
                    }
                    else {
                        $id_contratto = Db::getInstance()->getValue("SELECT id_contratto FROM "._DB_PREFIX_."contratto_assistenza ORDER BY id_contratto DESC");
                        $id_contratto++;
                        $prodotti = Db::getInstance()->executeS("SELECT COUNT(od.product_id) AS conto, od.product_id, p.reference, pl.name, f.qt_ord FROM "._DB_PREFIX_."order_detail od JOIN "._DB_PREFIX_."orders o ON od.id_order = o.id_order JOIN "._DB_PREFIX_."product p ON od.product_id = p.id_product JOIN "._DB_PREFIX_."product_lang pl ON od.product_id = pl.id_product JOIN "._DB_PREFIX_."fattura f ON p.reference = f.cod_articolo WHERE o.id_customer = ".$customer->id." AND p.reference IN (SELECT cod_articolo FROM "._DB_PREFIX_."fattura WHERE id_customer = ".$customer->id.") AND pl.id_lang = ".$this->context->language->id." GROUP BY od.product_id");
                        $oggi = date('d-m-Y');
                    }

					$indirizzi = Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_."address WHERE id_customer = ".$customer->id." AND deleted = 0 AND active = 1 ORDER BY id_address ASC");
                    $contratto['indirizzi'] = $indirizzi;

                    // form ricerca prodotti: aggiungere quello pronto in orders

                    $marche = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'manufacturer ORDER BY name ASC');
                    $categorie = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'category c JOIN '._DB_PREFIX_.'category_lang cl ON c.id_category = cl.id_category WHERE cl.id_lang = '.$this->context->language->id.' AND c.id_parent = 1 ORDER BY cl.name ASC');
                    $fornitori = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'supplier WHERE id_supplier = 8 OR id_supplier = 11 OR id_supplier = 42 ORDER BY name ASC');
					
                    if(sizeof($prodotti) > 0) 
                    {
                        foreach($prodotti as &$prodotto) {
                            $prodotto['tooltip'] = Product::showProductTooltip($prodotto['product_id']);
                            $prodotto['prezzo_format'] = number_format(($prodotto['prezzo']), 2, ',', '');
                        }
                    }

                    $contratto['prezzo_format'] = number_format(($contratto['prezzo']), 2, ',', '');

                    if($edit) {
                        $contratti_edit = array(
                            'dati' => $contratto,
                            'prodotti' => $prodotti,
                            'storico' => $storico_contratto,
                            'marche' => $marche,
                            'categorie' => $categorie,
                            'fornitori' => $fornitori,
                        );
                    }
                    else {
                        $contratti_add = array(
                            'id_contratto' => $id_contratto,
                            'prodotti' => $prodotti,
                            'oggi' => $oggi,
                            'indirizzi' => $indirizzi,
                            'marche' => $marche,
                            'categorie' => $categorie,
                            'fornitori' => $fornitori,
                        );
                    }
                }
            }
            // VIEW
            else if(Tools::getIsset('azione') && Tools::getValue('azione') == 'contratto_view') {

                $contratto = Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."contratto_assistenza WHERE id_contratto = ".Tools::getValue('id_contratto'));
                
                switch($contratto['tipo']) { case 1: $tipo_contratto = 'Base'; break; case 2: $tipo_contratto = 'Top'; break; case 3: $tipo_contratto = 'Gold'; break; default: $tipo_contratto = 'Altro'; }
                switch($contratto['status']) { case 0: $status_contratto = 'Attivo'; break; case 1: $status_contratto = 'Sospeso'; break; case 2: $status_contratto = 'Cancellato'; break;  case 3: $status_contratto = 'Disdetto da cliente'; break; case 4: $status_contratto = 'Scaduto'; break; default: $status_contratto = 'Altro'; }

                $contratto['tipo_contratto'] = $tipo_contratto;
                $contratto['status_contratto'] = $status_contratto;
                $contratto['importo'] = Tools::displayPrice($contratto['prezzo'], $this->context->currency->id);

                $contratto['competenza_dal'] = ($contratto['competenza_dal'] != '0000-00-00' ? date("d-m-Y", strtotime($contratto['competenza_dal'])) : '');
                $contratto['competenza_al'] = ($contratto['competenza_al'] != '0000-00-00' ? date("d-m-Y", strtotime($contratto['competenza_al'])) : '');

                // correggere: cosa stampo se periodicita = 0?
                $contratto['rata'] = Tools::displayPrice($contratto['prezzo'] / $contratto['periodicita'], $this->context->currency->id);
                
                $contratto['data_registrazione'] = Tools::displayDate($contratto['data_registrazione'], $this->context->language->id);
                $contratto['data_inizio'] = Tools::displayDate($contratto['data_inizio'], $this->context->language->id);
                $contratto['data_fine'] = Tools::displayDate($contratto['data_fine'], $this->context->language->id);

                if(is_numeric($contratto['indirizzo'])) {
                    $indirizzo_row = Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."address WHERE id_address = ".$contratto['indirizzo']);
                    $indirizzo = $indirizzo_row['address1']." - ".$indirizzo_row['postcode']." ".$indirizzo_row['city']." (".Db::getInstance()->getValue("SELECT iso_code FROM "._DB_PREFIX_."state WHERE id_state = ".$indirizzo_row['id_state']).")";
                }
                else {
                    $indirizzo = $contratto['indirizzo'];
                }

                $contratto['sede'] = $indirizzo;

                $prodotti = Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_."contratto_assistenza_prodotti WHERE id_contratto = ".Tools::getValue('id_contratto'));
                
                foreach($prodotti as &$prodotto) 
                {
                    $prodotto['tooltip'] = Product::showProductTooltip($prodotto['id_product']);
                    $prodotto['prezzo_format'] = Tools::displayPrice($prodotto['prezzo'], $this->context->currency->id);

                    if($prodotto['indirizzo'] == '') {
                        $indirizzo1 = explode("Canone Assistenza",$prodotto['note']);
                        $indirizzo2 = explode(" : ",$indirizzo1[0]);
                        $indirizzo = $indirizzo2[1];
                    }
                    else {
                        if(is_numeric($contratto['indirizzo'])) {
                            $indirizzo_row = Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."address WHERE id_address = ".$prodotto['indirizzo']);
                            $indirizzo = $indirizzo_row['address1']." - ".$indirizzo_row['postcode']." ".$indirizzo_row['city']." (".Db::getInstance()->getValue("SELECT iso_code FROM "._DB_PREFIX_."state WHERE id_state = ".$indirizzo_row['id_state']).")";
                        }
                        else {
                            $indirizzo = $prodotto['indirizzo'];
                        }
                    }

                    $prodotto['indirizzo'] = $indirizzo;
                }

                $storico_contratto = Db::getInstance()->executeS('SELECT * FROM storico_attivita WHERE id_attivita = '.$contratto['id_contratto'].' AND tipo_attivita = "CO"');
                
                foreach($storico_contratto as &$storico)
                    $storico['data_attivita'] = Tools::displayDate($storico['data_attivita'], $this->context->language->id);

                $contratti_view = array(
                    'dati' => $contratto,
                    'storico' => $storico_contratto,
                    'prodotti' => $prodotti,
                );
            }
            // VIEW TABELLA
            else {
                if($count_contratti) 
                {
                    $contratti_list = Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_."contratto_assistenza WHERE id_customer = ".$customer->id."");
                    
                    foreach($contratti_list as &$contratto) 
                    {
                        switch($contratto['tipo']) { case 1: $tipo_contratto = 'Base'; break; case 2: $tipo_contratto = 'Top'; break; case 3: $tipo_contratto = 'Gold'; break; default: $tipo_contratto = 'Altro'; }
                        switch($contratto['status']) { case 0: $status_contratto = 'Attivo'; break; case 1: $status_contratto = 'Sospeso'; break; case 2: $status_contratto = 'Cancellato'; break; case 3: $status_contratto = 'Disdetto da cliente'; break; case 4: $status_contratto = 'Scaduto'; break; default: $status_contratto = 'Altro'; }
                        
                        if(is_numeric($contratto['indirizzo'])) {
                            $indirizzo_row = Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."address WHERE id_address = ".$contratto['indirizzo']);
                            $indirizzo = $indirizzo_row['address1']." - ".$indirizzo_row['postcode']." ".$indirizzo_row['city']." (".Db::getInstance()->getValue("SELECT iso_code FROM "._DB_PREFIX_."state WHERE id_state = ".$indirizzo_row['id_state']).")";
                        }
                        else {
                            $indirizzo = $contratto['indirizzo'];
                        }

                        $contratto['tipo_contratto'] = $tipo_contratto;
                        $contratto['status_contratto'] = $status_contratto;
                        $contratto['inizia_il'] = Tools::displayDate($contratto['data_inizio'], (int)($this->context->language->id), false);
                        $contratto['scade_il'] = Tools::displayDate($contratto['data_fine'], (int)($this->context->language->id), false);
                        $contratto['importo'] = Tools::displayPrice($contratto['prezzo'], $this->context->currency->id);
                        $contratto['sede'] = $indirizzo;
                    }
                }
            }

            $contratti = array(
                'contratti_list' => $contratti_list,
                'contratti_view' => $contratti_view,
                'contratti_edit' => $contratti_edit,
                'contratti_add' => $contratti_add,
            );

        }
        /* FINE Contratti */
        
        $bdl_aperti = Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_."bdl a WHERE id_customer = ".$customer->id." AND a.id_bdl > 384 AND nessun_addebito = 0 AND ( (a.rif_ordine = 0 AND a.invio_controllo = 1 AND a.invio_contabilita = 0) OR (a.rif_ordine = 0 AND a.invio_controllo = 0) ) GROUP BY a.id_bdl ORDER BY a.id_bdl");
        $count_bdl_aperti = sizeof($bdl_aperti);
        $count_bdl_totali = Db::getInstance()->getValue("SELECT count(id_bdl) FROM "._DB_PREFIX_."bdl WHERE id_customer = ".$customer->id);	
     
        /* INIZIO BDL */
        if($tab_name == 'bdl' && !$is_agente){ // tab-container-1=14
            
            // Genera PDF
            if(Tools::getIsset('azione') && (Tools::getValue('azione') == 'bdl_pdf_tecnotel' || Tools::getValue('azione') == 'bdl_pdf_ezdirect')) {
                
                require_once('../classes/html2pdf/html2pdf.class.php');
                
                if(Tools::getIsset('id_bdl') && Tools::getValue('id_bdl') == 'vuoto')
                    $content = BDL::generatePDFBDL(Tools::getValue('id_bdl'), Tools::getValue('azione'), 'y'); // correggere value azione
                else
                    $content = BDL::generatePDFBDL(Tools::getValue('id_bdl'), Tools::getValue('azione')); // correggere value azione
                
                ob_clean();
                $html2pdf = new HTML2PDF('P','A4','it');
                $html2pdf->WriteHTML($content);
                
                ob_start();
                ob_end_clean();

                $html2pdf->Output(Tools::getValue('id_bdl').'-buono-di-lavoro.pdf', 'D');
            }
            
            // DELETE
            if(Tools::getIsset('azione') && Tools::getValue('azione') == 'bdl_delete') {
                
                /* // usando la classe (provare):
                $bdl = new BDL((int)Tools::getValue('bdl_delete'));
                $bdl->delete();
                */
                // Sostituire con classe BDL
                Db::getInstance()->execute("
                    DELETE 
                    FROM "._DB_PREFIX_."bdl 
                    WHERE id_bdl = ".Tools::getValue('bdl_delete')."
                ");

                Tools::redirectAdmin(self::$currentIndex.'&'.$this->identifier.'='.$customer->id.'&viewcustomer&tab_name=bdl&conf=4&token='.$this->token);
            }
            
            // ADD E EDIT
            if(Tools::getIsset('azione') && (Tools::getValue('azione') == 'bdl_add' || Tools::getValue('azione') == 'bdl_edit')) {

                // Se è stato premuto il tasto submit del form, salvo i dati
                if(Tools::isSubmit('bdl_save')) {

                    $ora_richiesta_array = explode(":",Tools::getValue('ora_richiesta'));
                    $ora_richiesta = sprintf("%02d", $ora_richiesta_array[0]).":".(!$ora_richiesta_array[1] ? '00' : $ora_richiesta_array[1]);
                    $ora_effettuato_array = explode(":",Tools::getValue('ora_effettuato'));
                    $ora_effettuato = sprintf("%02d", $ora_effettuato_array[0]).":".(!$ora_effettuato_array[1] ? '00' : $ora_effettuato_array[1]);
                    
                    $data_richiesta = date("Y-m-d H:i:s", strtotime(Tools::getValue('data_richiesta')." ".$ora_richiesta));
                    $data_effettuato = date("Y-m-d H:i:s", strtotime(Tools::getValue('data_effettuato')." ".$ora_effettuato));

                    // VARIABILI SPOSTATE FUORI DALL'IF - PROVARE!
                    /** */
                    $phone = Tools::getValue('bdl_phone');
                    if($phone == 'Altro')
                        $phone .= Tools::getValue('bdl_phone_altro');	
                
                    $guasto = Tools::getValue('guasto');
                    if($guasto == 'Altro')
                        $guasto .= Tools::getValue('guasto_altro');
                        
                    $causa_guasto = Tools::getValue('causa_guasto');
                    if($causa_guasto == 'Altro')
                        $causa_guasto .= Tools::getValue('causa_guasto_altro');
                    
                    $descrizione_bdl = Tools::getValue('descrizione');
                    if($descrizione_bdl == 'Altro')
                        $descrizione_bdl .= Tools::getValue('descrizione_altro');
                        
                    $impianto_ubicazione = Tools::getValue('impianto_ubicazione');
                    if(!is_numeric($impianto_ubicazione))
                        $impianto_ubicazione = Tools::getValue('impianto_ubicazione_manuale');

                    $indirizzo_bdl = Tools::getValue('indirizzo_bdl');
                    $richiesto_da = Tools::getValue('richiesto_da');
                    $effettuato_da = Tools::getValue('effettuato_da');
                    $contratto_assistenza = Tools::getValue('contratto_assistenza');
                    $manutenzione_ordinaria = (Tools::getIsset('manutenzione_ordinaria') ? 1 : 0);
                    $manutenzione_straordinaria = (Tools::getIsset('manutenzione_straordinaria') ? 1 : 0);
                    $motivo_chiamata = Tools::getValue('motivo_chiamata');
                    $invio_controllo = (Tools::getIsset('invio_controllo') ? 1 : 0);
                    $invio_contabilita = (Tools::getIsset('invio_contabilita') ? 1 : 0);
                    $intervento_svolto = Tools::getValue('intervento_svolto');
                    $note_bdl = Tools::getValue('note_bdl');
                    $note_private_bdl = Tools::getValue('note_private_bdl');
                    $rif_ordine = Tools::getValue('rif_ordine');
                    $pagato = (Tools::getIsset('pagato') ? 1 : 0);
                    $fatturato = (Tools::getIsset('fatturato') ? 1 : 0);
                    $nessun_addebito = (Tools::getIsset('nessun_addebito') ? 1 : 0);

                    if(Tools::getValue('azione') == 'bdl_edit') {
                        $id_bdl = Tools::getValue('id_bdl');
                        
                        Db::getInstance()->execute("
                            UPDATE "._DB_PREFIX_."bdl 
                            SET id_address = '".$indirizzo_bdl."', 
                                phone = '".$phone."', 
                                data_richiesta = '".$data_richiesta."', 
                                richiesto_da = '".$richiesto_da."',
                                data_effettuato = '".$data_effettuato."', 
                                effettuato_da = '".$effettuato_da."', 
                                impianto_ubicazione = '".$impianto_ubicazione."', 
                                contratto_assistenza = '".$contratto_assistenza."', 
                                manutenzione_ordinaria = '".$manutenzione_ordinaria."', 
                                manutenzione_straordinaria = '".$manutenzione_straordinaria."', 
                                motivo_chiamata = '".addslashes($motivo_chiamata)."', 
                                guasto = '".addslashes($guasto)."', 
                                causa_guasto = '".addslashes($causa_guasto)."', 
                                descrizione = '".addslashes($descrizione_bdl)."', 
                                invio_controllo = '".$invio_controllo."', 
                                invio_contabilita = '".$invio_contabilita."', 
                                intervento_svolto = '".addslashes($intervento_svolto)."', 
                                note = '".addslashes($note_bdl)."', 
                                note_private = '".addslashes($note_private_bdl)."', 
                                rif_ordine = '".addslashes($rif_ordine)."', 
                                date_upd = '".date('Y-m-d H:i:s')."', 
                                pagato = '".$pagato."', 
                                fatturato = '".$fatturato."', 
                                nessun_addebito = '".$nessun_addebito."' 
                            WHERE id_bdl = '".$id_bdl."'
                        ");

                        foreach ($_POST['note_nuova'] as $id_nota=>$nota_id) {
                            // NEL TPL:
                            // echo $_POST['note_nota'][$id_nota].'<br />';
                        
                            if($_POST['note_nuova'][$id_nota] == 0) {
                                $testo_nota = Db::getInstance()->getValue('
                                    SELECT note FROM note_attivita WHERE id_note = '.$id_nota
                                );
                                if($testo_nota != $_POST['note_nota'][$id_nota])
                                    Db::getInstance()->execute("
                                        UPDATE note_attivita 
                                        SET note = '".addslashes($_POST['note_nota'][$id_nota])."', 
                                            date_upd = '".date("Y-m-d H:i:s")."', 
                                            id_employee = '".$mio_id."' 
                                        WHERE id_note = ".$id_nota
                                    ); 
                            }
                            else {
                                if(addslashes($_POST['note_nota'][$id_nota]) != '') {
                                    Db::getInstance()->execute("
                                        INSERT INTO note_attivita (
                                            id_note,
                                            id_attivita,
                                            tipo_attivita,
                                            id_employee,
                                            note,
                                            date_add,
                                            date_upd
                                        )
                                        VALUES (
                                            NULL,
                                            '".Tools::getValue('id_bdl')."',
                                            'B',
                                            '".$mio_id."',
                                            '".addslashes($_POST['note_nota'][$id_nota])."',
                                            '".date("Y-m-d H:i:s")."',
                                            '".date("Y-m-d H:i:s")."'
                                        )
                                    ");
                                }
                            }
                        }               
                    }
                    else {
                        Db::getInstance()->execute("
                            INSERT INTO "._DB_PREFIX_."bdl (
                                id_bdl, 
                                id_customer, 
                                id_address, 
                                phone, 
                                data_richiesta, 
                                richiesto_da, 
                                data_effettuato, 
                                effettuato_da, 
                                impianto_ubicazione, 
                                contratto_assistenza, 
                                manutenzione_ordinaria, 
                                manutenzione_straordinaria, 
                                motivo_chiamata, 
                                guasto, 
                                causa_guasto, 
                                descrizione, 
                                intervento_svolto, 
                                prodotti, 
                                dettaglio_costi, 
                                note, 
                                note_private, 
                                rif_ordine, 
                                pagato, 
                                fatturato, 
                                nessun_addebito, 
                                invio_controllo, 
                                invio_contabilita, 
                                date_add, 
                                date_upd 
                                ".(Tools::getIsset('riferimento') ? ', riferimento' : '')."
                            ) 
                            VALUES (
                                NULL, 
                                '".$customer->id."', 
                                '".$indirizzo_bdl."', 
                                '".$phone."', 
                                '".$data_richiesta."', 
                                '".$richiesto_da."', 
                                '".$data_effettuato."', 
                                '".$effettuato_da."', 
                                '".addslashes($impianto_ubicazione)."', 
                                '".$contratto_assistenza."', 
                                '".$manutenzione_ordinaria."', 
                                '".$manutenzione_straordinaria."', 
                                '".addslashes($motivo_chiamata)."', 
                                '".addslashes($guasto)."', 
                                '".addslashes($causa_guasto)."', 
                                '".addslashes($descrizione_bdl)."', 
                                '".addslashes($intervento_svolto)."', 
                                '', 
                                '', 
                                '".addslashes($note_bdl)."', 
                                '".addslashes($note_private_bdl)."', 
                                '".addslashes($rif_ordine)."', 
                                '".$pagato."', 
                                '".$fatturato."', 
                                '".$nessun_addebito."', 
                                '".$invio_controllo."', 
                                '".$invio_contabilita."', 
                                '".date("Y-m-d H:i:s")."', 
                                '".date("Y-m-d H:i:s")."' 
                                ".(Tools::getIsset('riferimento') ? ", '".Tools::getValue('riferimento')."'" : '')."
                            )
                        ");
                    }
                    
                    $string_bdl_order = '';

                    if(Tools::getValue('azione') == 'bdl_add')
                        $id_bdl = Db::getInstance()->Insert_ID();
                    else
                        $id_bdl = Tools::getValue('id_bdl');
                    
                    $identificatore_univoco_cliente = $customer->id;
                    $partita_iva_cliente = $customer->vat_number;
                    $codice_fiscale =  $customer->tax_code;
                    
                    $id_bdl_cart = Db::getInstance()->getValue('SELECT id_cart FROM '._DB_PREFIX_.'cart ORDER BY id_cart DESC');
                    $id_bdl_cart++;
                    
                    $dettaglio_bdl['chilometri_percorsi_tipo'] = str_replace(",",".",$_POST['chilometri_percorsi_tipo']);
                    $dettaglio_bdl['chilometri_percorsi_qta'] = str_replace(",",".",$_POST['chilometri_percorsi_qta']);
                    $dettaglio_bdl['chilometri_percorsi_sconto'] = str_replace(",",".",$_POST['chilometri_percorsi_sconto']);
                    $dettaglio_bdl['chilometri_percorsi_unitario'] = str_replace(",",".",$_POST['chilometri_percorsi_unitario']);
                    
                    if($dettaglio_bdl['chilometri_percorsi_unitario'] > 0) {
                        $product_bdl_data = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'product p JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product = pl.id_product WHERE id_lang = '.$this->context->language->id.' AND p.id_product = (SELECT id_product FROM '._DB_PREFIX_.'product WHERE reference = "'.$dettaglio_bdl['chilometri_percorsi_tipo'].'")');
                        $string_bdl_order .= 'INSERT INTO '._DB_PREFIX_.'cart_product (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, sconto_extra, prezzo_acquisto, no_acq, id_specific_price, sort_order, bundle, date_add) VALUES ('.$id_bdl_cart.', '.$product_bdl_data['id_product'].', 0,'.$dettaglio_bdl['chilometri_percorsi_qta'].','.($dettaglio_bdl['chilometri_percorsi_unitario']-(($dettaglio_bdl['chilometri_percorsi_unitario']*$dettaglio_bdl['chilometri_percorsi_sconto'])/100)).',0,"'.$product_bdl_data['name'].'",0,'.$dettaglio_bdl['chilometri_percorsi_sconto'].','.$product_bdl_data['wholesale_price'].',0,0,0,0,"'.date('Y-m-d H:i:s').'");|; ';
                    }

                    $dettaglio_bdl['diritto_chiamata_tipo'] = str_replace(",",".",$_POST['diritto_chiamata_tipo']);
                    $dettaglio_bdl['diritto_chiamata_qta'] = str_replace(",",".",$_POST['diritto_chiamata_qta']);
                    $dettaglio_bdl['diritto_chiamata_sconto'] = str_replace(",",".",$_POST['diritto_chiamata_sconto']);
                    $dettaglio_bdl['diritto_chiamata_unitario'] = str_replace(",",".",$_POST['diritto_chiamata_unitario']);
                    
                    if($dettaglio_bdl['diritto_chiamata_unitario'] > 0) {
                        $product_bdl_data = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'product p JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product = pl.id_product WHERE id_lang = '.$this->context->language->id.' AND p.id_product = (SELECT id_product FROM '._DB_PREFIX_.'product WHERE reference = "'.$dettaglio_bdl['diritto_chiamata_tipo'].'")');
                        $string_bdl_order .= 'INSERT INTO '._DB_PREFIX_.'cart_product (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, sconto_extra, prezzo_acquisto, no_acq, id_specific_price, sort_order, bundle, date_add) VALUES ('.$id_bdl_cart.', '.$product_bdl_data['id_product'].', 0,'.$dettaglio_bdl['diritto_chiamata_qta'].','.($dettaglio_bdl['diritto_chiamata_unitario']-(($dettaglio_bdl['diritto_chiamata_unitario']*$dettaglio_bdl['diritto_chiamata_sconto'])/100)).',0,"'.$product_bdl_data['name'].'",0,'.$dettaglio_bdl['diritto_chiamata_sconto'].','.$product_bdl_data['wholesale_price'].',0,0,0,0,"'.date('Y-m-d H:i:s').'");|; ';
                    }
                    
                    $dettaglio_bdl['costo_manodopera_tipo'] = str_replace(",",".",$_POST['costo_manodopera_tipo']);
                    $dettaglio_bdl['costo_manodopera_qta'] = str_replace(",",".",$_POST['costo_manodopera_qta']);
                    $dettaglio_bdl['costo_manodopera_sconto'] = str_replace(",",".",$_POST['costo_manodopera_sconto']);
                    $dettaglio_bdl['costo_manodopera_unitario'] = str_replace(",",".",$_POST['costo_manodopera_unitario']);
                    
                    if($dettaglio_bdl['costo_manodopera_unitario']) {
                        $product_bdl_data = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'product p JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product = pl.id_product WHERE id_lang = '.$this->context->language->id.' AND p.id_product = (SELECT id_product FROM '._DB_PREFIX_.'product WHERE reference = "'.$dettaglio_bdl['costo_manodopera_tipo'].'")');
                        $string_bdl_order .= 'INSERT INTO '._DB_PREFIX_.'cart_product (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, sconto_extra, prezzo_acquisto, no_acq, id_specific_price, sort_order, bundle, date_add) VALUES ('.$id_bdl_cart.', '.$product_bdl_data['id_product'].', 0,'.$dettaglio_bdl['costo_manodopera_qta'].','.($dettaglio_bdl['costo_manodopera_unitario']-(($dettaglio_bdl['costo_manodopera_unitario']*$dettaglio_bdl['costo_manodopera_sconto'])/100)).',0,"'.$product_bdl_data['name'].'",0,'.$dettaglio_bdl['costo_manodopera_sconto'].','.$product_bdl_data['wholesale_price'].',0,0,0,0,"'.date('Y-m-d H:i:s').'");|; ';
                    }
                    
                    $dettaglio_bdl['num_varie'] = $_POST['num_varie'];
                    
                    $num_varie = $_POST['num_varie']; 
                    
                    $totale_bdl_u = 0;
                    $totale_bdl_u += (($dettaglio_bdl['diritto_chiamata_unitario']-(($dettaglio_bdl['diritto_chiamata_unitario']*$dettaglio_bdl['diritto_chiamata_sconto'])/100))*$dettaglio_bdl['diritto_chiamata_qta'])+(($dettaglio_bdl['chilometri_percorsi_unitario']-(($dettaglio_bdl['chilometri_percorsi_unitario']*$dettaglio_bdl['chilometri_percorsi_sconto'])/100))*$dettaglio_bdl['chilometri_percorsi_qta'])+(($dettaglio_bdl['costo_manodopera_unitario']-(($dettaglio_bdl['costo_manodopera_unitario']*$dettaglio_bdl['costo_manodopera_sconto'])/100))*$dettaglio_bdl['costo_manodopera_qta']);
                    
                    for($i = 0; $i<=$_POST['num_varie']; $i++) {

                        $dettaglio_bdl['ricambi_e_varie_'.$i.'_qta'] = str_replace(",",".",$_POST['ricambi_e_varie_'.$i.'_qta']);
                        $dettaglio_bdl['ricambi_e_varie_'.$i.'_unitario'] = str_replace(",",".",$_POST['ricambi_e_varie_'.$i.'_unitario']);
                        $dettaglio_bdl['ricambi_e_varie_'.$i.'_sconto'] = str_replace(",",".",$_POST['ricambi_e_varie_'.$i.'_sconto']);
                        $dettaglio_bdl['ricambi_e_varie_'.$i.'_descrizione'] = str_replace(",",".",$_POST['ricambi_e_varie_'.$i.'_descrizione']);
                        $dettaglio_bdl['ricambi_e_varie_'.$i.'_tipo'] = $_POST['ricambi_e_varie_'.$i.'_tipo'];
                        $totale_bdl_u += ($dettaglio_bdl['ricambi_e_varie_'.$i.'_unitario']-(($dettaglio_bdl['ricambi_e_varie_'.$i.'_unitario']*$dettaglio_bdl['ricambi_e_varie_'.$i.'_sconto'])/100))*$dettaglio_bdl['ricambi_e_varie_'.$i.'_qta'];
                        
                        $prezzo_ricambi_e_varie = str_replace(".",",",$prezzo_ricambi_e_varie);
                        
                        $product_bdl_data = Db::getInstance()->getRow('
                            SELECT * 
                            FROM '._DB_PREFIX_.'product p 
                            JOIN '._DB_PREFIX_.'product_lang pl 
                                ON p.id_product = pl.id_product 
                            WHERE id_lang = '.$this->context->language->id.' 
                                AND p.id_product = (
                                    SELECT id_product 
                                    FROM '._DB_PREFIX_.'product 
                                    WHERE reference = "'.$dettaglio_bdl['ricambi_e_varie_'.$i.'_tipo'].'"
                                )
                        ');
                    
                        if($product_bdl_data['id_product'] > 0) {
                            $string_bdl_order .= 'INSERT INTO '._DB_PREFIX_.'cart_product (id_cart, id_product, id_product_attribute, quantity, price, free, name, sc_qta, sconto_extra, prezzo_acquisto, no_acq, id_specific_price, sort_order, bundle, date_add) VALUES ('.$id_bdl_cart.', '.$product_bdl_data['id_product'].', 0,'.$dettaglio_bdl['ricambi_e_varie_'.$i.'_qta'].','.($dettaglio_bdl['ricambi_e_varie_'.$i.'_unitario']-(($dettaglio_bdl['ricambi_e_varie_'.$i.'_unitario']*$dettaglio_bdl['ricambi_e_varie_'.$i.'_sconto'])/100)).',0,"'.$product_bdl_data['name'].'",0,'.$dettaglio_bdl['ricambi_e_varie_'.$i.'_sconto'].','.$product_bdl_data['wholesale_price'].',0,0,0,0,"'.date('Y-m-d H:i:s').'");|; ';
                        }
                    }
                    
                    if(Tools::getValue('rif_ordine') > 0) {
                        $dettaglio_bdl = '';
                        Db::getInstance()->execute('
                            UPDATE '._DB_PREFIX_.'bdl 
                            SET importo = 0 
                            WHERE id_bdl = '.$id_bdl
                        );
                    }
                    else {
                        $dettaglio_bdl = serialize($dettaglio_bdl);
                        Db::getInstance()->execute('
                            UPDATE '._DB_PREFIX_.'bdl 
                            SET importo = '.$totale_bdl_u.' 
                            WHERE id_bdl = '.$id_bdl
                        );
                    }

                    Db::getInstance()->execute("
                        UPDATE "._DB_PREFIX_."bdl 
                        SET dettaglio_costi = '".$dettaglio_bdl."' 
                        WHERE id_bdl = ".$id_bdl
                    );

                    /*
                    if(Tools::getIsset('invio_controllo') && Tools::getValue('invio_controllo') == 1) {
                        // niente?
                    }
                    */
                    
                    if(Tools::getValue('rif_ordine') > 0) {
                        Db::getInstance()->execute('
                            UPDATE '._DB_PREFIX_.'bdl 
                            SET invio_controllo = 2, invio_contabilita = 2 
                            WHERE id_bdl = '.$id_bdl
                        );
                    }
                    
                    if(Tools::getValue('contratto_assistenza') == 1 && Tools::getIsset('manutenzione_ordinaria')) {
                        Db::getInstance()->execute('
                            UPDATE '._DB_PREFIX_.'bdl 
                            SET pagato = 2, fatturato = 2, invio_controllo = 2, invio_contabilita = 2 
                            WHERE id_bdl = '.$id_bdl
                        );
                    }

                    if(Tools::getIsset('invio_contabilita') && !Tools::getIsset('nessun_addebito') && Tools::getValue('invio_contabilita') == 1 && (Tools::getValue('rif_ordine') == 0 || empty($_POST['rif_ordine'])) && $totale_bdl_u != 0 && !Tools::getIsset('fatturato')) {
                        $note_bdl = 'BDL '.$id_bdl.' del '.date("d/m/Y", strtotime(Db::getInstance()->getValue('SELECT data_effettuato FROM '._DB_PREFIX_.'bdl WHERE id_bdl = '.$id_bdl)));
                        
                        $bdl_dati = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'bdl WHERE id_bdl = '.$id_bdl);
                        
                        $note_bdl .= '<br /><br /><strong>Ticket:</strong> '.Customer::trovaSigla(substr($bdl_dati['riferimento'],1), substr($bdl_dati['riferimento'],0,1));
                        $note_bdl .= '<br /><br /><strong>Descrizione intervento:</strong> '.$bdl_dati['descrizione'].'';
                        $note_bdl .= '<br /><br /><strong>Motivo chiamata:</strong> '.$bdl_dati['motivo_chiamata'].'';
                        $note_bdl .= '<br /><br /><strong>Guasto:</strong> '.$bdl_dati['guasto'].'';
                        $note_bdl .= '<br /><br /><strong>Causa guasto riscontrata:</strong> '.$bdl_dati['causa_guasto'].'';
                        
                        /* Inizio creazione carrello e trasformazione in ordine */
                        /* // decommentare dopo aver testato il resto
                        $indirizzo = Db::getInstance()->getValue('
                            SELECT id_address 
                            FROM '._DB_PREFIX_.'address 
                            WHERE active = 1 
                                AND deleted = 0 
                                AND fatturazione = 1
                                AND id_customer = '.$customer->id
                        );
                        
                        $bdl_order_products = explode(";|;",$string_bdl_order);
                    
                        $payment_default = Db::getInstance()->getValue('
                            SELECT pagamento 
                            FROM customer_amministrazione
                            WHERE id_customer = '.$id_customer
                        );
                        
                        if($payment_default == '')
                            $payment_default = 'Bonifico';
                    
                        $transport_bdl = Db::getInstance()->getValue('
                            SELECT id_carrier 
                            FROM '._DB_PREFIX_.'carrier 
                            WHERE name LIKE "%grat%" 
                                AND deleted = 0
                        ');
                        
                        Db::getInstance()->execute('
                            INSERT INTO '._DB_PREFIX_.'cart (id_cart, riferimento, id_carrier, id_lang, id_currency, id_customer, id_address_invoice, id_address_delivery, name, cig, cup, ipa, validita, date_add, date_upd, created_by,  id_employee, in_carico_a, preventivo, transport, payment, note, rif_ordine) 
                            VALUES ('.$id_bdl_cart.', "BDL'.$id_bdl.'", '.$transport_bdl.', '.$this->context->language->id.', 1, "'.$customer->id.'", "'.$indirizzo.'", "'.$indirizzo.'", "BDL n. '.$id_bdl.'", "", "", "", "", "'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'", '.$mio_id.','.$mio_id.','.$mio_id.',1, "'.$transport_bdl.':0", "'.$payment_default.'", "'.$note_bdl.'", "BDL'.$id_bdl.'")
                        ');
                        
                        foreach($bdl_order_products as $b) {
                            // Sostituire con execute()?
                            Db::getInstance()->executeS($b);
                        }
                        // CONTROLLARE SE IL MODULO ESISTE, HA CAMBIATO NOME O VA MODIFICATO
                        include("../modules/pss_clearcarts/pss_clearcarts.php");
                        include("../modules/pss_clearcarts/AdminPssClearCarts.php");
                        $apcc = new AdminPssClearCarts();
                        
                        $apcc->orderThisCart($id_bdl_cart);
                        //$order = Db::getInstance()->getValue('SELECT id_order FROM '._DB_PREFIX_.'orders WHERE id_cart = '.$id_cart_ta.'');
                        */
                        /* Fine creazione carrello e trasformazione in ordine */
                    
                        $subject = "Nuovo BDL da fatturare";
        
                        $message = "<html>
                        <head>
                            <title>Nuovo BDL da fatturare</title>
                        </head>
                        <body>
                        ".$this->generatePDFBDL($id_bdl)."
                        </body>
                        </html>";
                                
                        $headers  = 'MIME-Version: 1.0' . "\n";
                        $headers .= 'Content-Type: text/html' ."\n";
                        $headers .= 'From: "Ezdirect" <info@ezdirect.it>' . "\n";
                        // CORREGGERE DOPO AVER FATTO PROVE: mail a Barbara
                        // mail("barbara.giorgini@ezdirect.it", $subject, $message, $headers);
                        // PROVA!!!!!!!
                        mail("carolina.carusi@ezdirect.it", $subject, $message, $headers);
                    }

                    // ??? correggere; forse è solo per usare la vecchia funzione nei ticket
                    if(Tools::getIsset('viewticket'))
                        Tools::redirectAdmin(self::$currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&viewticket&id_customer_thread='.Tools::getValue('id_customer_thread').'&token='.$this->token.'&tab_name=tickets&conf=4&tab-container-ticket=5#ticket-5');
                    else
                        Tools::redirectAdmin(self::$currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&tab_name=bdl&conf=4&token='.$this->token);
                }
                // Visualizzo i dati nel form (vedi correzioni, trasforma in funzione)
                else {
                    $buoni = array();

                    $bdl_ticket = 0; // lasciare così finchè non è pronta la funzione in customer.php, poi passarlo come parametro
                    $edit = ((Tools::getValue('azione') == 'bdl_edit' || $bdl_ticket > 0) ? true : false); // true = modifica, false = inserimento
                    
                    // DA QUI IN POI SPOSTARE TUTTO NELLA FUNZIONE, CHE VERRA' USATA ANCHE PER I TICKET

                    if($edit){
                        if($bdl_ticket > 0)
                            $edit_bdl = $bdl_ticket;
                        else
                            $edit_bdl = Tools::getValue('id_bdl');

                        $bdl = Db::getInstance()->getRow("
                            SELECT * 
                            FROM "._DB_PREFIX_."bdl 
                            WHERE id_bdl = ".$edit_bdl
                        );

                        // Invia BDL via mail
                        //$bdl_mail_href = $currentIndex.'&id_customer='.(int)$customer->id.'&viewcustomer&tab_name=bdl&viewmessage&aprinuovomessaggio&allega_bdl='.$edit_bdl.'&token='.$this->token;
                    }

                    $dati_ticket = Db::getInstance()->getRow('
                        SELECT id_customer_thread, descrizione, motivo_chiamata, causa_guasto, guasto
                        FROM '._DB_PREFIX_.'customer_thread 
                        WHERE id_customer_thread = '.Tools::getValue('id_customer_thread')
                    );
                    
                    if($dati_ticket['id_customer_thread'] > 0){					
                        if($bdl['descrizione'] == '')
                            $bdl['descrizione'] = $dati_ticket['descrizione'];
                        
                        if($bdl['motivo_chiamata'] == '')
                            $bdl['motivo_chiamata'] = $dati_ticket['motivo_chiamata'];
                        
                        if($bdl['causa_guasto'] == '')
                            $bdl['causa_guasto'] = $dati_ticket['causa_guasto'];
                        
                        if($bdl['guasto'] == '')
                            $bdl['guasto'] = $dati_ticket['guasto'];
                    }
                    else
                        $dati_ticket['id_customer_thread'] = 0;

                    /* vedi dove inserire nel tpl:
                    // se Tools::getIsset('riferimento'): Azione padre: Tools::getValue('riferimento')
                    // seguito da: ($bdl['riferimento'] != '' ? '- <strong>Azione padre</strong>: '.Customer::trovaSiglaLinkPerAlbero(substr($bdl['riferimento'],1), substr($bdl['riferimento'],0,1), $bdl['id_bdl']) : '');

                    // pulsanti submit (vedi link in customer.php)
                    */

                    $indirizzi_bdl = Db::getInstance()->executeS('
                        SELECT id_address, address1, postcode, city, iso_code
                        FROM '._DB_PREFIX_.'address a 
                        JOIN '._DB_PREFIX_.'state s ON a.id_state = s.id_state 
                        WHERE a.active = 1 
                            AND a.deleted = 0 
                            AND a.id_customer='.$customer->id
                    ); 

                    $indirizzo_fatt = Db::getInstance()->getRow('
                        SELECT id_address, address1, postcode, city, iso_code
                        FROM '._DB_PREFIX_.'address a 
                        JOIN '._DB_PREFIX_.'state s ON a.id_state = s.id_state 
                        WHERE a.active = 1 
                            AND a.deleted = 0 
                            AND a.fatturazione = 1 
                            AND a.id_customer='.$customer->id
                    );

                    $telefoni_cliente = Db::getInstance()->executeS('
                        SELECT phone, phone_mobile 
                        FROM '._DB_PREFIX_.'address 
                        WHERE id_customer = '.$customer->id
                    );

                    // Richiesto da:
                    $persone = Db::getInstance()->executeS("
                        SELECT id_persona, firstname, lastname 
                        FROM persone 
                        WHERE id_customer = ".$customer->id
                    );

                    // Effettuato da:
                    $impiegati = array();
                    $i = 0;

                    $employees = Employee::getEmployees();
                    foreach($employees as $employee){
                        // Massimo, Paolo, Lorenzo, Leonardo, Dorinel
                        if($employee['id_employee'] == 4 || $employee['id_employee'] == 5 || $employee['id_employee'] == 12 || $employee['id_employee'] == 17 || $employee['id_employee'] == 26){
                            $impiegati[$i]['id'] = $employee['id_employee'];
                            $impiegati[$i]['name'] = $employee['firstname'].' '.$employee['lastname'][0].'.';
                            $i++;
                        }
                    }

                    $i = 0;

                    $rif_ordini = Db::getInstance()->executeS('
                        SELECT * 
                        FROM '._DB_PREFIX_.'orders 
                        WHERE id_customer = '.$customer->id
                    );

                    $ordini = array();
                    $i = 0;

                    foreach($rif_ordini as $ordine){
                        $ordini[$i]['id'] = $ordine['id_order'];
                        $ordini[$i]['stringa'] = $ordine['id_order'].' del '.Tools::displayDate($ordine['date_add']).' ('.Tools::displayPrice($ordine['total_products'], $this->context->currency->id).')';
                        $i++;
                    }

                    $i = 0;
                    
                    $marche = Db::getInstance()->executeS('
                        SELECT id_manufacturer as id, name
                        FROM '._DB_PREFIX_.'manufacturer 
                        ORDER BY name ASC
                    ');

                    $categorie = Db::getInstance()->executeS('
                        SELECT c.id_category as id, cl.name 
                        FROM '._DB_PREFIX_.'category c 
                        JOIN '._DB_PREFIX_.'category_lang cl ON c.id_category = cl.id_category 
                        WHERE cl.id_lang = '.$this->context->language->id.' 
                            AND c.id_parent = 1 
                            ORDER BY cl.name ASC
                    ');
                    
                    $fornitori = Db::getInstance()->executeS('
                        SELECT id_supplier as id, name
                        FROM '._DB_PREFIX_.'supplier 
                        WHERE id_supplier = 8
                            OR id_supplier = 11
                            OR id_supplier = 42
                        ORDER BY name ASC
                    ');
                    
                    // PROVA
                    /*$fornitori = Db::getInstance()->executeS('
                        SELECT id_supplier as id, name
                        FROM '._DB_PREFIX_.'supplier
                        ORDER BY name ASC
                    ');*/

                    $num_varie = 0;
                    $totale_bdl = 0;

                    if($edit){
                        // Dettaglio costi
                        $dettaglio_costi = Db::getInstance()->getValue("
                            SELECT dettaglio_costi 
                            FROM ".DB_PREFIX_."bdl 
                            WHERE id_bdl = ".$edit_bdl
                        );
                        $dettaglio_costi = unserialize($dettaglio_costi); // non funziona

                        $num_varie = $dettaglio_costi['num_varie'];
                        if(!$num_varie)
                            $num_varie = 0;
                        /*
                        for($i=0; $i<=$num_varie; $i++) {
                            if(!(empty($dettaglio_costi['ricambi_e_varie_'.$i.'_descrizione']) && $dettaglio_costi['ricambi_e_varie_'.$i.'_unitario'] == 0)) {
                                // tpl + funzioni js
                            }
                        }
                        */

                        $totale_bdl += (($dettaglio_costi['diritto_chiamata_unitario']-(($dettaglio_costi['diritto_chiamata_unitario']*$dettaglio_costi['diritto_chiamata_sconto'])/100))*$dettaglio_costi['diritto_chiamata_qta'])+(($dettaglio_costi['chilometri_percorsi_unitario']-(($dettaglio_costi['chilometri_percorsi_unitario']*$dettaglio_costi['chilometri_percorsi_sconto'])/100))*$dettaglio_costi['chilometri_percorsi_qta'])+(($dettaglio_costi['costo_manodopera_unitario']-(($dettaglio_costi['costo_manodopera_unitario']*$dettaglio_costi['costo_manodopera_sconto'])/100))*$dettaglio_costi['costo_manodopera_qta']);
                        
                        for($i=0; $i<=$num_varie; $i++) {
                            $totale_bdl += ($dettaglio_costi['ricambi_e_varie_'.$i.'_unitario']-(($dettaglio_costi['ricambi_e_varie_'.$i.'_unitario']*$dettaglio_costi['ricambi_e_varie_'.$i.'_sconto'])/100))*$dettaglio_costi['ricambi_e_varie_'.$i.'_qta'];
                        }
                        
                        // Note private
                        $note_attivita = Db::getInstance()->executeS('
                            SELECT * 
                            FROM note_attivita 
                            WHERE tipo_attivita = "B" 
                                AND id_attivita = '.$edit_bdl
                        );
                    }

                    $buono = array();

                    if($edit){
                        $buono = $bdl;
                        //$buono['bdl_mail_href'] = $bdl_mail_href;
                        $buono['ora_richiesta'] = date('H:i', strtotime($buono['data_richiesta']));
                        $buono['ora_effettuato'] = date('H:i', strtotime($buono['data_effettuato']));
                        $buono['data_richiesta'] = date('Y-m-d', strtotime($buono['data_richiesta']));
                        $buono['data_effettuato'] = date('Y-m-d', strtotime($buono['data_effettuato']));

                        $buono['note_attivita'] = $note_attivita;
                        $buono['dettaglio_costi'] = $dettaglio_costi;

                        $buono['collega_bdl'] = Customer::BuildSelectForLinking($customer->id, $buono['id_bdl'], 'L');
                    }
                    
                    $buono['ticket'] = $dati_ticket['id_customer_thread'];
                    $buono['indirizzi_bdl'] = $indirizzi_bdl;
                    $buono['indirizzo_fatt'] = $indirizzo_fatt;
                    $buono['telefoni'] = $telefoni_cliente;
                    $buono['persone'] = $persone;
                    $buono['impiegati'] = $impiegati;
                    $buono['ordini'] = $ordini;
                    $buono['marche'] = $marche;
                    //$buono['serie'] = $serie;
                    $buono['categorie'] = $categorie;
                    $buono['fornitori'] = $fornitori;
                    $buono['num_varie'] = $num_varie;
                    $buono['totale_bdl'] = $totale_bdl;

                    $buoni = $buono;
                }
            }
            // VIEW TABELLA
            else {
                if($count_bdl_totali > 0) {
                    $buoni = array();

                    $bdl = Db::getInstance()->executeS("
                        SELECT * 
                        FROM "._DB_PREFIX_."bdl 
                        WHERE id_customer = ".$customer->id." 
                        ORDER BY id_bdl DESC
                    ");

                    foreach($bdl as $buono) {
                    
                        $status_bdl = Db::getInstance()->getValue('
                            SELECT (
                                CASE WHEN a.rif_ordine > 0
                                THEN "closed"
                                WHEN a.nessun_addebito = 1
                                THEN "closed"
                                WHEN a.id_bdl < 384
                                THEN "closed"
                                WHEN a.rif_ordine = 0 AND a.invio_controllo = 1 AND a.invio_contabilita = 0
                                THEN "pending1"
                                WHEN a.rif_ordine = 0 AND a.invio_controllo = 1 AND a.invio_contabilita = 1
                                THEN "closed"
                                WHEN a.invio_controllo = 2 AND a.invio_contabilita = 2
                                THEN "closed"
                                WHEN a.rif_ordine = 0 AND a.invio_controllo = 0
                                THEN "open"
                                ELSE "open"
                                END
                            ) stato 
                            FROM '._DB_PREFIX_.'bdl a 
                            WHERE id_bdl = '.$buono['id_bdl']
                        );
                        
                        switch($status_bdl){
                            case 'open': 
                                $buono['status'] = 'Aperto'; 
                                $buono['status_style'] = 'class="badge badge-danger" style="font-weight:bold; border-radius:0px;"'; 
                                break;
                            case 'closed': 
                                $buono['status'] = 'Chiuso'; 
                                $buono['status_style'] = 'class="badge badge-success" style="font-weight:bold; border-radius:0px;"'; 
                                break;
                            case 'pending1': 
                                $buono['status'] = 'Lavor.'; 
                                $buono['status_style'] = 'class="badge badge-warning" style="font-weight:bold; border-radius:0px;"'; 
                                break;
                            default: 
                                $buono['status'] = ''; 
                                $buono['status_style'] = ''; 
                                break;
                        }
                        
                        $buono['data_bdl'] = Tools::displayDate($buono['data_richiesta']);
                        $buono['importo'] = Tools::displayPrice($buono['importo']);
                        $buono['fatto_da'] = Db::getInstance()->getValue('
                            SELECT CONCAT(firstname," ",LEFT(lastname, 1),".") 
                            FROM '._DB_PREFIX_.'employee 
                            WHERE id_employee = '.$buono['effettuato_da']
                        );
                        
                        $buoni[] = $buono;                        
                    }
                }
            }

            $bdl = array(
                'buoni' => $buoni,
            );
        }
        /* FINE BDL */

        /* INIZIO Mail */
        if($tab_name == 'mail' && !$is_agente){ // tab-container-1=15
            
            // Connessione al database mail
            $connection = mysqli_connect("localhost", "ezdml", "as5HJ786A/&4g1x");
            mysqli_select_db($connection,"ez_ml");
            
            $result_mails_ricevute = mysqli_query($connection, 'SELECT * FROM mail WHERE type = "received" AND id_customer = '.$customer->id.' ORDER BY data DESC');
            // TEST con db prestashop (Attenzione! La table mail è stata aggiunta per prova, ma esiste anche ps_mail, nativa di PS 1.6)
            //$mails_ricevute = Db::getInstance()->executeS('SELECT * FROM mail WHERE type = "received" AND id_customer = '.$customer->id.' ORDER BY data DESC');
    
            $mails_ricevute = array();
            while($row_mails = mysqli_fetch_array($result_mails_ricevute)){
                $mails_ricevute[] = $row_mails;
            }

            foreach ($mails_ricevute as &$ricevuta) {
                $ricevuta['data'] = date('d/m/Y H:i:s', strtotime($ricevuta['data']));
            }
            
            $result_mails_inviate = mysqli_query($connection, 'SELECT * FROM mail WHERE type = "sent" AND id_customer = '.$customer->id.' ORDER BY data DESC');
            // TEST con db prestashop (Attenzione! La table mail è stata aggiunta per prova, ma esiste anche ps_mail, nativa di PS 1.6)
            //$mails_inviate = Db::getInstance()->executeS('SELECT * FROM mail WHERE type = "sent" AND id_customer = '.$customer->id.' ORDER BY data DESC');
    
            $mails_inviate = array();
            while($row_mails_inviate = mysqli_fetch_array($result_mails_inviate)){
                $mails_inviate[] = $row_mails_inviate;
            }

            foreach ($mails_inviate as &$inviata) {
                $inviata['data'] = date('d/m/Y H:i:s', strtotime($inviata['data']));
            }

            // Ristabilisco la connessione con il database iniziale
            $connection = mysqli_connect(_DB_SERVER_, _DB_USER_, _DB_PASSWD_);
			mysqli_select_db($connection,_DB_NAME_);

            $mail = array(
                'ricevute' => $mails_ricevute,
                'inviate' => $mails_inviate,
            );

        }
        /* FINE Mail */

		$count_telefonate_ricevute = Db::getInstance()->getValue('SELECT COUNT(*) FROM telefonate WHERE id_customer = '.$customer->id.' AND calltype = "Inbound" ORDER BY datetime DESC');
        $count_telefonate_inviate = Db::getInstance()->getValue('SELECT COUNT(*) FROM telefonate WHERE id_customer = '.$customer->id.' AND calltype = "Outbound" ORDER BY datetime DESC');	

        /* INIZIO Telefonate */
        if($tab_name == 'calls' && !$is_agente){ // tab-container-1=16

            $ricevute = Db::getInstance()->executeS('
                SELECT id_telefonata as id, LEFT(datetime,16) as data, src as da, dst as a, billable as durata, disposition as stato 
                FROM telefonate 
                WHERE id_customer = '.$customer->id.' 
                    AND calltype = "Inbound" ORDER BY datetime DESC
            ');

            foreach ($ricevute as &$ricevuta) {
                $ricevuta['durata'] = gmdate("H:i:s", $ricevuta['durata']);
            }

            $inviate = Db::getInstance()->executeS('
                SELECT id_telefonata as id, LEFT(datetime,16) as data, src as da, dst as a, billable as durata, disposition as stato
                FROM telefonate
                WHERE id_customer = '.$customer->id.' 
                    AND calltype = "Outbound" ORDER BY datetime DESC
            ');

            foreach ($inviate as &$inviata) {
                $inviata['durata'] = gmdate("H:i:s", $inviata['durata']);
            }

            $stats_mese = Db::getInstance()->executeS('
                SELECT SUM(durata_ricevute) AS durata_ricevute, SUM(durata_inviate) AS durata_inviate, SUM(tel_ricevute) AS totale_ricevute, SUM(tel_inviate) AS totale_inviate, year(datetime) AS anno, month(datetime) AS mese, CONCAT(MONTHNAME(datetime), " ",YEAR(datetime)) AS mese_anno
                FROM (
                    SELECT datetime, id_customer, calltype, IF(calltype LIKE "%inbound%", billable, NULL) AS durata_ricevute, IF(calltype LIKE "%outbound%", billable, NULL) AS durata_inviate,
                        IF(calltype LIKE "%inbound%", 1, NULL) AS tel_ricevute, IF(calltype LIKE "%outbound%", 1, NULL) AS tel_inviate, COUNT(id_telefonata) AS conto 
                        FROM telefonate 
                        WHERE id_customer = '.$customer->id.' 
                        GROUP BY id_telefonata
                ) x 
                GROUP BY year(datetime), month(datetime)
            ');
            
            if(count($stats_mese) > 0){
                $totale_ricevute = 0;
                $totale_inviate = 0;
                $durata_ricevute = 0;
                $durata_inviate = 0;

                $totale_ricevute_reparto = 0;
                $totale_inviate_reparto = 0;
                $durata_ricevute_reparto = 0;
                $durata_inviate_reparto = 0;

                foreach($stats_mese as $telefonate_mese){
                    $totale_ricevute += $telefonate_mese['totale_ricevute'];
                    $totale_inviate +=  $telefonate_mese['totale_inviate'];
                    $durata_ricevute += $telefonate_mese['durata_ricevute'];
                    $durata_inviate +=  $telefonate_mese['durata_inviate'];
                }

                $durata_ricevute = gmdate("H:i:s", $durata_ricevute);
                $durata_inviate = gmdate("H:i:s", $durata_inviate);

                $stats_totali = array(
                    'totale_ricevute' => $totale_ricevute,
                    'totale_inviate' =>  $totale_inviate,
                    'durata_ricevute' => $durata_ricevute,
                    'durata_inviate' =>  $durata_inviate,
                );

                $stats_mese_reparto = Db::getInstance()->executeS('
                    SELECT reparto, SUM(durata_ricevute) durata_ricevute, SUM(durata_inviate) durata_inviate, SUM(tel_ricevute) totale_ricevute, SUM(tel_inviate) totale_inviate
                    FROM (
                        SELECT id_telefonata, id_customer, calltype, IF(calltype LIKE "%inbound%", billable,NULL) AS durata_ricevute, IF(calltype LIKE "%outbound%", billable,NULL) AS durata_inviate,
                        IF(calltype LIKE "%inbound%", 1,NULL) AS tel_ricevute, IF(calltype LIKE "%outbound%", 1,NULL) AS tel_inviate,
                        (CASE WHEN calltype LIKE "%inbound%" THEN dst ELSE src END) reparto,
                        COUNT(id_telefonata) conto 
                        FROM telefonate 
                        WHERE id_customer = '.$customer->id.' 
                        GROUP BY id_telefonata
                    ) x 
                    GROUP BY reparto
                ');
                
                foreach($stats_mese_reparto as $telefonate_reparto){
                    $totale_ricevute_reparto += $telefonate_reparto['totale_ricevute'];
                    $totale_inviate_reparto +=  $telefonate_reparto['totale_inviate'];
                    $durata_ricevute_reparto += $telefonate_reparto['durata_ricevute'];
                    $durata_inviate_reparto +=  $telefonate_reparto['durata_inviate'];
                }

                $durata_inviate_reparto = gmdate("H:i:s", $durata_inviate_reparto);
                $durata_ricevute_reparto = gmdate("H:i:s", $durata_ricevute_reparto);

                $stats_totali_reparto = array(
                    'totale_ricevute' => $totale_ricevute_reparto,
                    'totale_inviate' =>  $totale_inviate_reparto,
                    'durata_ricevute' => $durata_ricevute_reparto,
                    'durata_inviate' =>  $durata_inviate_reparto,
                );

                foreach ($stats_mese as &$s_mese) {
                    $s_mese['durata_ricevute'] = gmdate("H:i:s", $s_mese['durata_ricevute']);
                    $s_mese['durata_inviate'] = gmdate("H:i:s", $s_mese['durata_inviate']);
                }

                foreach ($stats_mese_reparto as &$s_mese_reparto) {
                    $s_mese_reparto['durata_ricevute'] = gmdate("H:i:s", $s_mese_reparto['durata_ricevute']);
                    $s_mese_reparto['durata_inviate'] = gmdate("H:i:s", $s_mese_reparto['durata_inviate']);
                }
            }

            $telefonate = array(
                'ricevute' => $ricevute,
                'inviate' => $inviate,
                'stats_mese' => $stats_mese,
                'stats_mese_reparto' => $stats_mese_reparto,
                'stats_totali' => $stats_totali,
                'stats_totali_reparto' => $stats_totali_reparto,
            );

        }
        /* FINE Telefonate */

        $this->tpl_view_vars = array(
            'customer' => $customer,
            'gender' => $gender,
            'gender_image' => $gender_image,
            // General information of the customer
            'registration_date' => Tools::displayDate($customer->date_add, null, true),
            'customer_stats' => $customer_stats,
            'last_visit' => Tools::displayDate($customer_stats['last_visit'], null, true),
            'count_better_customers' => $count_better_customers,
            'shop_is_feature_active' => Shop::isFeatureActive(),
            'name_shop' => $shop->name,
            'customer_birthday' => Tools::displayDate($customer->birthday),
            'last_update' => Tools::displayDate($customer->date_upd, null, true),
            'customer_exists' => Customer::customerExists($customer->email),
            'id_lang' => $customer->id_lang,
            'customerLanguage' => $customerLanguage,
            // Add a Private note
            // modificato nome variabile (aggiunto _ps) per distinguerla da customer_note personalizzata
            'customer_note_ps' => Tools::htmlentitiesUTF8($customer->note),
            // Messages
            'messages' => $messages,
            // Groups
            'groups' => $groups,
            // Orders
            'orders' => $orders,
            'orders_ok' => $orders_ok,
            'orders_ko' => $orders_ko,
            'total_ok' => Tools::displayPrice($total_ok, $this->context->currency->id),
            // Products
            'products' => $products,
            // Addresses
            //'addresses' => $customer->getAddresses($this->default_form_language),
            // Discounts
            'discounts' => CartRule::getCustomerCartRules($this->default_form_language, $customer->id, false, false),
            // Carts
            'carts' => $carts,
            // Interested
            'interested' => $interested,
            // Emails
            'emails' => $emails,
            // Connections
            'connections' => $connections,
            // Referrers
            'referrers' => $referrers,

            /* INIZIO override! */
            'navbar_on' => $navbar_on,
            'riassunto_on' => $riassunto_on,
            'riassunto' => $riassunto,
            'riga_pulsanti' => $riga_pulsanti,
            'alert' => $alert,
            'addresses' => $customer->getAddressesFattCons($this->default_form_language),
            'indirizzi' => $indirizzi,
            'addresses_fatt' => $addresses_fatt,
            'addresses_cons' => $addresses_cons,
            'gruppi' => $gruppi,
            'count_persone' => $count_persone,
            'persone' => $persone,
            'amministrazione' => $amministrazione,
            'count_ordini' => $count_ordini,
            'ordini' => $ordini,
            'count_carrelli' => $count_carrelli,
            'carrelli' => $carrelli,
            'count_fatture' => $count_fatture,
            'fatture' => $fatture,
            'count_note_di_credito' => $count_note_di_credito,
            'note_di_credito' => $note_di_credito,
            'ezcloud' => $ezcloud,
            'yeastar_cloud' => $yeastar_cloud,
            'count_documenti_dir' => $count_directories,
            'count_documenti_files' => $count_files,
            'count_documenti' => $count_documenti,
            'check_cartella_documenti' => $check_cartella_documenti,
            'documenti' => $documenti,
            'count_ticket_aperti' => $count_ticket_aperti,
            'count_ticket' => $count_ticket,
            'tickets' => $tickets,
            'count_azioni_cliente_aperte' => $count_azioni_cliente_aperte,
            'count_azioni_cliente' => $count_azioni_cliente,
            'azioni_cliente' => $azioni_cliente,
            'count_todo_aperti' => $count_todo_aperti,
            'count_todo' => $count_todo,
            'todo' => $todo,
            'stat' => $stat,
            'count_contratti_attivi' => $count_contratti_attivi,
            'count_contratti' => $count_contratti,
            'contratti' => $contratti,
            'count_bdl_aperti' => $count_bdl_aperti,
            'count_bdl_totali' => $count_bdl_totali,
            'bdl' => $bdl,
            'mail' => $mail,
            'count_telefonate_ricevute' => $count_telefonate_ricevute,
            'count_telefonate_inviate' => $count_telefonate_inviate,
            'telefonate' => $telefonate,
            'customer_note' => $customer_note,
            //'punti_fedelta' => $punti_fedelta, // modulo?
            'is_agente' => $is_agente,
            'not_mio_agente' => $not_mio_agente,
            'auth_carrelli_tipo' => $auth_carrelli_tipo,
            'check_employee' => $check_employee,
            'check_programmer' => $check_programmer,
            'check_admin2' => $check_admin2,
            'mio_id' => $mio_id,
            'mio_nome' => $mio_nome,
            'mio_firstname' => $mio_firstname,
            'today' => $today,
            'utils_options' => $utils_options,
            'tab_name' => $tab_name,
            /* FINE override! */

            'show_toolbar' => true
        );

        $this->addJqueryUI('ui.datepicker');
        
        $this->addjQueryPlugin(array(
            'autocomplete',
            'select2',
        ));

        $this->addJS(_PS_JS_DIR_.'jquery/plugins/select2/select2_locale_'.$this->context->language->iso_code.'.js');
        
        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'select2_ready.js');
        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'tooltip_prodotto.js');
        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'override_customers.js');
        $this->addCSS(_PS_BO_DEFAULT_THEME_CSS_DIR_.'overrides.css');
        
        // $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'tooltipster.bundle.min.js');
        // $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'tooltipster.main.min.js');

        // $this->addCSS(_PS_BO_DEFAULT_THEME_CSS_DIR_.'tooltipster.bundle.min.css');
        // $this->addCSS(_PS_BO_DEFAULT_THEME_CSS_DIR_.'tooltipster.main.min.css');

        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'datatable_ready.js');
        $this->addJS(_PS_BO_DEFAULT_THEME_DIR_.'plugins/DataTables/datatables.min.js');
        $this->addCSS(_PS_BO_DEFAULT_THEME_DIR_.'plugins/DataTables/datatables.min.css');

        $this->addJS(_PS_BO_DEFAULT_THEME_DIR_.'plugins/TaTa/dist/tata.js');
        $this->addJS(_PS_BO_DEFAULT_THEME_DIR_.'plugins/listree/listree.umd.min.js');
        $this->addCSS(_PS_BO_DEFAULT_THEME_DIR_.'plugins/listree/listree.min.css');

        // autoload rich text editor (tiny mce)
        $this->tpl_view_vars['tinymce'] = true;
        $iso = $this->context->language->iso_code;
        $this->tpl_view_vars['iso'] = file_exists(_PS_CORE_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en';
        $this->tpl_view_vars['path_css'] = _THEME_CSS_DIR_;
        $this->tpl_view_vars['ad'] = __PS_BASE_URI__.basename(_PS_ADMIN_DIR_);

        $this->addJS(array(
            _PS_JS_DIR_.'tiny_mce/tiny_mce.js',
            _PS_JS_DIR_.'admin/tinymce.inc.js',
        ));

        return parent::renderView();
    }

    public function processDelete()
    {
        $this->_setDeletedMode();
        parent::processDelete();
    }

    protected function _setDeletedMode()
    {
        if ($this->delete_mode == 'real') {
            $this->deleted = false;
        } elseif ($this->delete_mode == 'deleted') {
            $this->deleted = true;
        } else {
            $this->errors[] = Tools::displayError('Unknown delete mode:').' '.$this->deleted;
            return;
        }
    }

    protected function processBulkDelete()
    {
        $this->_setDeletedMode();
        parent::processBulkDelete();
    }

    public function processAdd()
    {
        if (Tools::getValue('submitFormAjax')) {
            $this->redirect_after = false;
        }
        // Check that the new email is not already in use
        $customer_email = strval(Tools::getValue('email'));
        $customer = new Customer();
        if (Validate::isEmail($customer_email)) {
            $customer->getByEmail($customer_email);
        }
        if ($customer->id) {
            $this->errors[] = Tools::displayError('An account already exists for this email address:').' '.$customer_email;
            $this->display = 'edit';
            return $customer;
        } elseif (trim(Tools::getValue('passwd')) == '') {
            $this->validateRules();
            $this->errors[] = Tools::displayError('Password can not be empty.');
            $this->display = 'edit';
        } elseif ($customer = parent::processAdd()) {
            $this->context->smarty->assign('new_customer', $customer);
            return $customer;
        }
        return false;
    }

    public function processUpdate()
    {
        if (Validate::isLoadedObject($this->object)) {
            $customer_email = strval(Tools::getValue('email'));

            // check if e-mail already used
            if ($customer_email != $this->object->email) {
                $customer = new Customer();
                if (Validate::isEmail($customer_email)) {
                    $customer->getByEmail($customer_email);
                }
                if (($customer->id) && ($customer->id != (int)$this->object->id)) {
                    $this->errors[] = Tools::displayError('An account already exists for this email address:').' '.$customer_email;
                }
            }

            return parent::processUpdate();
        } else {
            $this->errors[] = Tools::displayError('An error occurred while loading the object.').'
				<b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
        }
    }

    public function processSave()
    {
        // Check that default group is selected
        /*if (!is_array(Tools::getValue('groupBox')) || !in_array(Tools::getValue('id_default_group'), Tools::getValue('groupBox'))) {
            $this->errors[] = Tools::displayError('A default customer group must be selected in group box.');
        }*/

        // Check the requires fields which are settings in the BO
        $customer = new Customer();
        $this->errors = array_merge($this->errors, $customer->validateFieldsRequiredDatabase());

        return parent::processSave();
    }

    protected function afterDelete($object, $old_id)
    {
        $customer = new Customer($old_id);
        $addresses = $customer->getAddresses($this->default_form_language);
        foreach ($addresses as $k => $v) {
            $address = new Address($v['id_address']);
            $address->id_customer = $object->id;
            $address->save();
        }
        return true;
    }
    /**
     * Transform a guest account into a registered customer account
     */
    public function processGuestToCustomer()
    {
        $customer = new Customer((int)Tools::getValue('id_customer'));
        if (!Validate::isLoadedObject($customer)) {
            $this->errors[] = Tools::displayError('This customer does not exist.');
        }
        if (Customer::customerExists($customer->email)) {
            $this->errors[] = Tools::displayError('This customer already exists as a non-guest.');
        } elseif ($customer->transformToCustomer(Tools::getValue('id_lang', $this->context->language->id))) {
            if ($id_order = (int)Tools::getValue('id_order')) {
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminOrders').'&id_order='.$id_order.'&vieworder&conf=3');
            } else {
                Tools::redirectAdmin(self::$currentIndex.'&'.$this->identifier.'='.$customer->id.'&viewcustomer&conf=3&token='.$this->token);
            }
        } else {
            $this->errors[] = Tools::displayError('An error occurred while updating customer information.');
        }
    }


    /* OVERRIDE: gestione di active = 2 nella list */
    public function processChangeActiveVal()
    {
        $customer = new Customer($this->id_object);
        if (!Validate::isLoadedObject($customer)) {
            $this->errors[] = Tools::displayError('An error occurred while updating customer information.');
        }
        $customer->active = (($customer->active == 0 || $customer->active == 2) ? 1 : 0);
        if (!$customer->update()) {
            $this->errors[] = Tools::displayError('An error occurred while updating customer information.');
        }
        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    }
    
    public function printActiveIcon($value, $customer)
    {
        return '<a class="list-action-enable '.($value ? 'action-enabled' : 'action-disabled').'" href="index.php?'.htmlspecialchars('tab=AdminCustomers&id_customer='
            .(int)$customer['id_customer'].'&changeActiveVal&token='.Tools::getAdminTokenLite('AdminCustomers')).'">
				'.($value == 1 ? '<i class="icon-check"></i>' : ($value == 0 ? '<i class="icon-remove"></i>' : '<i class="icon-remove" style="color:orange"></i>')).
            '</a>';
    }
    /* FINE OVERRIDE */


    /**
     * Toggle the newsletter flag
     */
    public function processChangeNewsletterVal()
    {
        $customer = new Customer($this->id_object);
        if (!Validate::isLoadedObject($customer)) {
            $this->errors[] = Tools::displayError('An error occurred while updating customer information.');
        }
        $customer->newsletter = $customer->newsletter ? 0 : 1;
        if (!$customer->update()) {
            $this->errors[] = Tools::displayError('An error occurred while updating customer information.');
        }
        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    }

    /**
     * Toggle newsletter optin flag
     */
    public function processChangeOptinVal()
    {
        $customer = new Customer($this->id_object);
        if (!Validate::isLoadedObject($customer)) {
            $this->errors[] = Tools::displayError('An error occurred while updating customer information.');
        }
        $customer->optin = $customer->optin ? 0 : 1;
        if (!$customer->update()) {
            $this->errors[] = Tools::displayError('An error occurred while updating customer information.');
        }
        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    }

    public function printNewsIcon($value, $customer)
    {
        return '<a class="list-action-enable '.($value ? 'action-enabled' : 'action-disabled').'" href="index.php?'.htmlspecialchars('tab=AdminCustomers&id_customer='
            .(int)$customer['id_customer'].'&changeNewsletterVal&token='.Tools::getAdminTokenLite('AdminCustomers')).'">
				'.($value ? '<i class="icon-check"></i>' : '<i class="icon-remove"></i>').
            '</a>';
    }

    public function printOptinIcon($value, $customer)
    {
        return '<a class="list-action-enable '.($value ? 'action-enabled' : 'action-disabled').'" href="index.php?'.htmlspecialchars('tab=AdminCustomers&id_customer='
            .(int)$customer['id_customer'].'&changeOptinVal&token='.Tools::getAdminTokenLite('AdminCustomers')).'">
				'.($value ? '<i class="icon-check"></i>' : '<i class="icon-remove"></i>').
            '</a>';
    }

    /**
     * @param string $token
     * @param int $id
     * @param string $name
     * @return mixed
     */
    public function displayDeleteLink($token = null, $id, $name = null)
    {
        $tpl = $this->createTemplate('helpers/list/list_action_delete.tpl');

        $customer = new Customer($id);
        $name = $customer->lastname.' '.$customer->firstname;
        $name = '\n\n'.$this->l('Name:', 'helper').' '.$name;

        $tpl->assign(array(
            'href' => self::$currentIndex.'&'.$this->identifier.'='.$id.'&delete'.$this->table.'&token='.($token != null ? $token : $this->token),
            'confirm' => $this->l('Delete the selected item?').$name,
            'action' => $this->l('Delete'),
            'id' => $id,
        ));

        return $tpl->fetch();
    }

    /**
     * add to $this->content the result of Customer::SearchByName
     * (encoded in json)
     *
     * @return void
     */
    public function ajaxProcessSearchCustomers()
    {
        $searches = explode(' ', Tools::getValue('customer_search'));
        $customers = array();
        $searches = array_unique($searches);
        foreach ($searches as $search) {
            if (!empty($search) && $results = Customer::searchByName($search, 50)) {
                foreach ($results as $result) {
                    if ($result['active']) {
                        $customers[$result['id_customer']] = $result;
                    }
                }
            }
        }

        if (count($customers)) {
            $to_return = array(
                'customers' => $customers,
                'found' => true
            );
        } else {
            $to_return = array('found' => false);
        }

        $this->content = Tools::jsonEncode($to_return);
    }

    /**
     * Uodate the customer note
     *
     * @return void
     */
    public function ajaxProcessUpdateCustomerNote()
    {
        if ($this->tabAccess['edit'] === '1') {
            $note = Tools::htmlentitiesDecodeUTF8(Tools::getValue('note'));
            $customer = new Customer((int)Tools::getValue('id_customer'));
            if (!Validate::isLoadedObject($customer)) {
                die('error:update');
            }
            if (!empty($note) && !Validate::isCleanHtml($note)) {
                die('error:validation');
            }
            $customer->note = $note;
            if (!$customer->update()) {
                die('error:update');
            }
            die('ok');
        }
    }

    // override; aggiunto l'argomento $customerz
    public function assegna_incarico($in_carico, $idct, $tipo, $customerz = '') 
    {
        $context = Context::getContext();

        $employeeincaricato = new Employee($context->employee->id);

        // correggere link e verificare il resto
        if($tipo != 'preventivo' && $tipo != 'tirichiamiamonoi') { 

            $ct = new CustomerThread($idct);
            
            if($customerz == '')
                $customerz = new Customer($ct->id_customer);

            $tickettipo = $ct->id_contact;
            
            if($tickettipo == 4){
                $sigla = "T";
            }
            else if ($tickettipo == 2){
                $sigla = "A";
            } 
            else if ($tickettipo == 3){
                $sigla = "V";
            } 
            else if ($tickettipo == 6){
                $sigla = "R";
            }
            else if ($tickettipo == 7){
                $sigla = "M";
            }
            else if ($tickettipo == 8){
                $sigla = "D";
            } 
            else if ($tickettipo == 9){
                $sigla = "S";
            }

            $anno = $ct->date_add;         
            $anno = substr($anno,2,2);

            $id_thread_ticket = $ct->id;
            $id_ticket = $sigla.$anno.$id_thread_ticket;

            if($in_carico != 0 && $in_carico != $context->employee->id && $in_carico != $ct->id_employee)
            {
                // correggere: prova, decommentare quello giusto
                $mail_incarico = 'carolina.carusi@ezdirect.it';
                //$mail_incarico = Db::getInstance()->getValue('SELECT email FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$in_carico.'');
                $tokenimp = Tools::getAdminToken("AdminCustomers".(int)Tab::getIdFromClassName("AdminCustomers").$in_carico);
                $linkimp = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".$ct->id_customer."&viewcustomer&tab_name=tickets&azione=tickets&viewticket&id_customer_thread=".$ct->id."&token=".$tokenimp;    
                
                if($tickettipo == 7) {
                    $linkimp = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".$ct->id_customer."&viewcustomer&tab_name=actions&azione=actions&viewmessage&id_mex=".$ct->id."&token=".$tokenimp;   
                    $params3 = array(
                        '{reply}' => $employeeincaricato->firstname." ".$employeeincaricato->lastname." ti ha assegnato un messaggio su Ezdirect. L'id del messaggio &egrave;: <strong>".$id_ticket."</strong> (cliente: ".($customerz->is_company == 1 ? $customerz->company : $customerz->firstname." ".$customerz->lastname)." - ID ".$customerz->id.")
                                    <br /><br />
                                    <a href='".$linkimp."'>Per rispondere al messaggio, clicca qua</a>.                                     
                    ");
                    // CORREGGERE: Sostituire 'msg_base' con 'action' dopo test e il 2° NULL con 'nonrispondere@ezdirect.it'
                    Mail::Send($context->language->id, 'msg_base', Mail::l('Messaggio cliente assegnato a te su Ezdirect', $context->language->id), 
                    $params3, $mail_incarico, NULL, NULL, 'Ezdirect', '', NULL, _PS_MAIL_DIR_, true);
                }
                else {
                    $params3 = array(
                        '{reply}' => $employeeincaricato->firstname." ".$employeeincaricato->lastname." ti ha assegnato un ticket su Ezdirect. L'id del ticket &egrave;: <strong>".$id_ticket."</strong> (cliente: ".($customerz->is_company == 1 ? $customerz->company : $customerz->firstname." ".$customerz->lastname)." - ID ".$customerz->id.")
                                    <br /><br />
                                    <a href='".$linkimp."'>Per rispondere al ticket, clicca qua</a>.                                        
                    ");
                    // CORREGGERE: Sostituire 'msg_base' con 'action' dopo test e il 2° NULL con 'nonrispondere@ezdirect.it'
                    Mail::Send($context->language->id, 'msg_base', Mail::l('Ticket cliente assegnato a te su Ezdirect', $context->language->id), 
                            $params3, $mail_incarico, NULL, NULL, 'Ezdirect', '', NULL, _PS_MAIL_DIR_, true);
                }
            }
        }
        else {
            $id_ticket = Customer::trovaSigla($idct, $tipo);

            if($customerz == ''){
                $rowz = Db::getInstance()->getValue("SELECT id_customer FROM form_prevendita_thread WHERE id_thread = ".$idct."");
                $customerz = new Customer($rowz['id_customer']);
            }

            if($in_carico != 0 && $in_carico != $context->employee->id) 
            {
                // correggere: prova, decommentare quello giusto
                $mail_incarico = 'carolina.carusi@ezdirect.it';
                //$mail_incarico = Db::getInstance()->getValue('SELECT email FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$in_carico.'');
                $tokenimp = Tools::getAdminToken("AdminCustomers".(int)Tab::getIdFromClassName("AdminCustomers").$in_carico);
                // Correggere: prima nel link c'era &viewticket&id_customer_thread; secondo me è sbagliato e l'ho corretto con azioni cliente
                $linkimp = "https://www.ezdirect.it/ezadmin/index.php?controller=AdminCustomers&id_customer=".$rowz['id_customer']."&viewcustomer&tab_name=actions&azione=actions&viewpreventivo&id_thread=".$idct."&token=".$tokenimp;  
                
                $tipo_richiesta_p = ($tipo == 'tirichiamiamonoi' ? 'ti richiamiamo noi' : 'preventivo');
                
                $params3 = array(
                    '{reply}' => $employeeincaricato->firstname." ".$employeeincaricato->lastname." ti ha assegnato un ".$tipo_richiesta_p." su Ezdirect. L'id del ".$tipo_richiesta_p." &egrave;: <strong>".$id_ticket."</strong> (cliente: ".($customerz->is_company == 1 ? $customerz->company : $customerz->firstname." ".$customerz->lastname)." - ID ".$customerz->id.")
                                <br /><br />
                                <a href='".$linkimp."'>Per rispondere al ".$tipo_richiesta_p.", clicca qua</a>.                                       
                ");
                // CORREGGERE: Sostituire 'msg_base' con 'action' dopo test e il 2° NULL con 'nonrispondere@ezdirect.it'
                Mail::Send($context->language->id, 'msg_base', Mail::l(''.ucfirst($tipo_richiesta_p).' cliente assegnato a te su Ezdirect', $context->language->id), 
                        $params3, $mail_incarico, NULL, NULL, 'Ezdirect', '', NULL, _PS_MAIL_DIR_, true);
            }     
        }
    }

    // override; correggere
    private function openUploadedFile()
    {
        $filename = $_GET['filename'];
        
        if(strpos($filename, ":::")) {
            $parti = explode(":::", $filename);
            $nomecodificato = $parti[0];
            $nomevero = $parti[1];
        }
        else {
            $nomecodificato = $filename;
            $nomevero = $filename;
        }

        $extensions = array('.txt' => 'text/plain', '.rtf' => 'application/rtf', '.doc' => 'application/msword', '.docx'=> 'application/msword',
        '.pdf' => 'application/pdf', '.zip' => 'multipart/x-zip', '.png' => 'image/png', '.jpeg' => 'image/jpeg', '.gif' => 'image/gif', '.jpg' => 'image/jpeg');

        $extension = '';
        foreach ($extensions AS $key => $val) {
            if(substr($nomevero, -4) == $key OR substr($nomevero, -5) == $key){
                $extension = $val;
                break;
            }
        }

        // spostare echo nel tpl o eliminarlo?
        //echo $nomecodificato."<br />".$nomevero;

        ob_end_clean();
        header('Content-Type: '.$extension);
        header('Content-Disposition:attachment;filename="'.$nomevero.'"');
        readfile(_PS_UPLOAD_DIR_.$nomecodificato);
        die; // probabilmente sbagliato
    }

    // override; funzione per tab Documenti
    public function FileSizeConvert($bytes) 
    {
        $bytes = floatval($bytes);
        $arBytes = array(
            0 => array(
                "UNIT" => "TB",
                "VALUE" => pow(1024, 4)
            ),
            1 => array(
                "UNIT" => "GB",
                "VALUE" => pow(1024, 3)
            ),
            2 => array(
                "UNIT" => "MB",
                "VALUE" => pow(1024, 2)
            ),
            3 => array(
                "UNIT" => "KB",
                "VALUE" => 1024
            ),
            4 => array(
                "UNIT" => "B",
                "VALUE" => 1
            ),
        );

        foreach($arBytes as $arItem)
        {
            if($bytes >= $arItem["VALUE"]) {
                $result = $bytes / $arItem["VALUE"];
                $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
                break;
            }
        }
        return $result;
    }
	
    // override; funzione per tab Documenti
    public static function deleteDir($dirPath) 
    {
		if (!is_dir($dirPath))
            throw new InvalidArgumentException("$dirPath must be a directory");
            
		if (substr($dirPath, strlen($dirPath) - 1, 1) != '/')
            $dirPath .= '/';
            
        $files = glob($dirPath . '*', GLOB_MARK);
        
        foreach($files as $file) 
        {
			if(is_dir($file))
				self::deleteDir($file);
            else 
                unlink($file);
        }
        
		rmdir($dirPath);
	}

    // override; funzione per tab Documenti
    public static function ListIn($dir, $prefix = '') 
    {
		$dir = rtrim($dir, '\\/');
		$result = array();

		$h = opendir($dir);
        while (($f = readdir($h)) !== false)
        {
		  if($f !== '.' and $f !== '..') {
			if(is_dir("$dir/$f")) {
			  $result[] = $prefix.$f;
			  $result = array_merge($result, self::ListIn("$dir/$f", "$prefix$f/"));
            }
		  }
        }
        
		closedir($h);

	    return $result;
    }

    // override; funzione per tab Documenti
    public static function sanitize($string, $force_lowercase = true, $anal = false) 
    {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
                       "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
                       "â€”", "â€“", ",", "<", ".", ">", "/", "?");

        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean;

        return ($force_lowercase ? (function_exists('mb_strtolower') ? mb_strtolower($clean, 'UTF-8') : strtolower($clean)) : $clean);
    }

}
