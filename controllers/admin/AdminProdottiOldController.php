<?php

// Potrei togliere Core e trasformare in override
class AdminProdottiOldControllerCore extends AdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'product';
        $this->lang = false;
        $this->explicitSelect = true;

        $this->allow_export = true;

        $this->addRowAction('edit');

        $this->context = Context::getContext();

        $this->_select = 'pl.name as prodotto, m.name as costruttore';

        $this->_join = '
            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (a.`id_product` = pl.`id_product`)
            LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (a.`id_manufacturer` = m.`id_manufacturer`)
        ';

        $this->_where = '
            AND pl.id_lang = '.(int)$this->context->language->id.'
            AND a.active = 2
        ';

        $this->_group = 'GROUP BY a.id_product';

        $this->_orderBy = 'reference';
        $this->_orderWay = 'ASC';

        $this->_use_found_rows = true;

        $this->fields_list = array(
            'id_product' => array( // necessario per action edit
                'title' => $this->l('ID'),
                'align' => 'text-center',
                'class' => 'fixed-width-sm'
            ),
            'reference' => array(
                'title' => $this->l('Codice'),
                'align' => 'text-center',
                //'callback' => 'creaTooltip',
                'remove_onclick' => true,
            ),
            'supplier_reference' => array(
                'title' => $this->l('Codice SKU'),
                'align' => 'text-center',
            ),
            'ean13' => array(
                'title' => $this->l('EAN'),
                'align' => 'text-center',
            ),
            'costruttore' => array(
                'title' => $this->l('Costruttore'),
                'align' => 'text-center',
                'filter_key' => 'm!name',
            ),
            'prodotto' => array(
                'title' => $this->l('Prodotto'),
                'align' => 'text-center',
                'filter_key' => 'pl!name',
            ),
            'stock_quantity' => array(
                'title' => $this->l('Mag. EZ'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs'
            ),
        );

        $this->redirect_after = ' '; // necessario per function redirect()
        
        parent::__construct();
    }

    public function renderList()
    {
        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'tooltip_prodotto.js');

        return parent::renderList();
    }

    public function initToolbarTitle()
    {
        parent::initToolbarTitle();

        array_pop($this->toolbar_title);
        $this->toolbar_title[] = sprintf($this->l('Prodotti OLD'));
        
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }
    
    public function initToolbar()
    {
        parent::initToolbar();

        unset($this->toolbar_btn['new']); // Toglie il pulsante ADD NEW dalla toolbar
    }

    // Se action = edit, redirect a edit prodotto
    protected function redirect()
    {
        if(Tools::getIsset('id_product') && Tools::getIsset('updateproduct')){
            $this->currentIndex = 'AdminProducts';
            $this->token = Tools::getAdminTokenLite('AdminProducts');
            $this->redirect_after = '/ezadmin/index.php?controller='.$this->currentIndex.'&id_product='.Tools::getValue('id_product').'&updateproduct&token='.$this->token;
            Tools::redirectAdmin($this->redirect_after);
        }
    }

    /*public function creaTooltip($value, $tr)
	{
        $tokenProducts = Tools::getAdminTokenLite('AdminProducts');
        $href_prodotto = '/ezadmin/index.php?controller=AdminProducts&id_product='.$tr['id_product'].'&updateproduct&token='.$tokenProducts;
        return '<a class="tooltip_prodotto" title="'.Product::showProductTooltip($tr['id_product']).'" href="'.$href_prodotto.'" target="_blank">'.$value.'</a>';
    }*/
}