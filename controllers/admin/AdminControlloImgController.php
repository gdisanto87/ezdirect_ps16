<?php

class AdminControlloImgControllerCore extends AdminController
{
    public function __construct()
    {
        $this->displayWarning('Warning: Page under construction!');
        $this->displayInformation('Eliminata tabella immagini_provvisoria; sono cambiate le dim default? => Correggere nomi colonne! Fare orderby');

        $this->bootstrap = true;
        $this->table = 'image';
        $this->lang = false;
        $this->explicitSelect = true;

        $this->context = Context::getContext();

        $this->_select = '
            p.reference AS codice, pl.name AS nomeprod
        ';
		
        $this->_join = '
            LEFT JOIN `'._DB_PREFIX_.'product` p ON (a.id_product = p.id_product)
            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (a.id_product = pl.id_product)
        ';

        $this->_where = '
            AND pl.id_lang = '.(int)$this->context->language->id
        ;

        //$this->_orderBy = 'id_image';
        //$this->_orderWay = 'DESC';

        $this->use_found_rows = true;

        $this->fields_list = array(
            'id_image' => array(
                'title' => $this->l('ID Img'),  
                'align' => 'left',
                'search' => false,
                'remove_onclick' => true,
            ),
            'nomeprod' => array(
                'title' => $this->l('Nome prodotto'),  
                'align' => 'left',
                'search' => false,
                'remove_onclick' => true,
            ),
            'codice' => array(
                'title' => $this->l('Codice prodotto'),  
                'align' => 'left',
                'search' => false,
                'remove_onclick' => true,
            ),
            'cover' => array(
                'title' => $this->l('Default'),  
                'align' => 'left',
                'search' => false,
                'callback' => 'printDefault',
                'remove_onclick' => true,
            ),
            'anteprima' => array(
                'title' => $this->l('Anteprima'),  
                'align' => 'left',
                'search' => false,
                'callback' => 'printImg',
                'filter_key' => 'id_image',
                'remove_onclick' => true,
            ),
            'dimensioniOrig' => array(
                'title' => $this->l('Dimensioni orig.'),  
                'align' => 'left',
                'search' => false,
                'callback' => 'getImgSize',
                'filter_key' => 'id_image',
                'remove_onclick' => true,
            ),
            'pesoOrig' => array(
                'title' => $this->l('Peso orig.'),  
                'align' => 'left',
                'search' => false,
                'callback' => 'getImgWeight',
                'filter_key' => 'id_image',
                'remove_onclick' => true,
            ),
            'pesoTrec' => array(
                'title' => $this->l('Peso 300x300'),  
                'align' => 'left',
                'search' => false,
                'callback' => 'getImgWeightT',
                'filter_key' => 'id_image',
                'remove_onclick' => true,
            ),
            'pesoSeic' => array(
                'title' => $this->l('Peso 600x600'),  
                'align' => 'left',
                'search' => false,
                'callback' => 'getImgWeightS',
                'filter_key' => 'id_image',
                'remove_onclick' => true,
            ),
            'pesoList' => array(
                'title' => $this->l('Peso 110x110'),  
                'align' => 'left',
                'search' => false,
                'callback' => 'getImgWeightL',
                'filter_key' => 'id_image',
                'remove_onclick' => true,
            ),
            'pesoPic' => array(
                'title' => $this->l('Peso 45x45'),  
                'align' => 'left',
                'search' => false,
                'callback' => 'getImgWeightP',
                'filter_key' => 'id_image',
                'remove_onclick' => true,
            ),
        );
        
        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Controllo Peso Immagini'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }

    public function initToolbar()
    {
        parent::initToolbar();

        unset($this->toolbar_btn['new']); // Toglie il pulsante ADD NEW dalla toolbar
    }

    // Default
    public function printDefault($value)
    {
        // Correggere: al posto di 'No' stampa '--'
        $print = ($value == 1 ? '<strong>Sì</strong>' : 'No');
        return '<span>'.$print.'</span>';
    }

    // Anteprima
    public function printImg($value)
    {
        $img = new Image($value);
        $image_url = _PS_BASE_URL_._THEME_PROD_DIR_.$img->getExistingImgPath()."-small_default.jpg";

        return '<a><img src='.$image_url.' /></a>';
    }

    // dimensioniOrig
    public function getImgSize($value)
    {
        $img = new Image($value);
        $image_url = _PS_BASE_URL_._THEME_PROD_DIR_.$img->getExistingImgPath().".jpg";
		list($imwidth, $imheight) = getimagesize($image_url);

        return '<span>'.$imwidth.' x '.$imheight.' pixel</span>';
    }

    // pesoOrig
    public function getImgWeight($value)
    {
        $img = new Image($value);
        $image_url = _PS_ROOT_DIR_._THEME_PROD_DIR_.$img->getExistingImgPath().".jpg";

        clearstatcache();
        $fileimg_size_normal = round((filesize($image_url)/1024), 2);
        
        return '<span>'.$fileimg_size_normal.' kb</span>';
    }

    // pesoTrec
    public function getImgWeightT($value)
    {
        $img = new Image($value);
        $image_url = _PS_ROOT_DIR_._THEME_PROD_DIR_.$img->getExistingImgPath()."-large_default.jpg";
        
        clearstatcache();
        $fileimg_size_large = round((filesize($image_url)/1024), 2);
        
        return '<span>'.$fileimg_size_large.' kb</span>';
    }

    // pesoSeic
    public function getImgWeightS($value)
    {
        $img = new Image($value);
        $image_url = _PS_ROOT_DIR_._THEME_PROD_DIR_.$img->getExistingImgPath()."-thickbox_default.jpg";
        
        clearstatcache();
		$fileimg_size_box = round((filesize($image_url)/1024), 2);
        
        return '<span>'.$fileimg_size_box.' kb</span>';
    }

    // pesoList
    public function getImgWeightL($value)
    {
        $img = new Image($value);
        $image_url = _PS_ROOT_DIR_._THEME_PROD_DIR_.$img->getExistingImgPath()."-medium_default.jpg";

        clearstatcache();
		$fileimg_size_medium = round((filesize($image_url)/1024), 2);
        
        return '<span>'.$fileimg_size_medium.' kb</span>';
    }

    // pesoPic
    public function getImgWeightP($value)
    {
        $img = new Image($value);
        $image_url = _PS_ROOT_DIR_._THEME_PROD_DIR_.$img->getExistingImgPath()."-small_default.jpg";
        
        clearstatcache();
		$fileimg_size_small = round((filesize($image_url)/1024), 2);
        
        return '<span>'.$fileimg_size_small.' kb</span>';
    }
}