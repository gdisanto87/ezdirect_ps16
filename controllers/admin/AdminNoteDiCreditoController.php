<?php

class AdminNoteDiCreditoControllerCore extends AdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
		$this->table = 'note_di_credito';
        $this->lang = false;
        $this->explicitSelect = true;

        $this->context = Context::getContext();

		$this->_select = '
			(CASE c.is_company
			WHEN 0
			THEN CONCAT(c.firstname," ",c.lastname)
			WHEN 1
			THEN c.company
			ELSE
			CONCAT(c.firstname," ",c.lastname)
			END
			) AS customer,

            c.company,
            c.id_default_group,
            gl.name as gruppo
        ';
        
		$this->_join = 'LEFT JOIN `'._DB_PREFIX_.'customer` c ON (c.`id_customer` = a.`id_customer`)';
		$this->_join .= 'LEFT JOIN `'._DB_PREFIX_.'group_lang` gl ON (c.`id_default_group` = gl.`id_group` AND gl.id_lang = '.(int)$this->context->language->id.')';
        
        $this->_group = 'GROUP BY a.id_storico, a.id_fattura';
        
        $this->_orderBy = 'data_fattura';
        $this->_orderWay = 'DESC';

        $this->_use_found_rows = true;

		$tipiArray = array(
            'Nota credito (immediata)' => 'Nota credito (immediata)'
        );

        $gruppi = Group::getGroups($this->context->language->id);
        $groupArray = array();
        foreach($gruppi as $gruppo){
            $groupArray = $groupArray + array($gruppo['id_group'] => $gruppo['name']);
        }
	 	
 		$this->fields_list = array(
            'id_fattura' => array(
                'title' => $this->l('ID Nota'),  
                'align' => 'center',
                'callback' => 'printLink',
                'remove_onclick' => true
            ),
            'id_storico' => array(
                'title' => $this->l('ID Storico'), 
                'align' => 'center',
                'remove_onclick' => true
            ),
            'tipo_fattura' => array(
                'title' => $this->l('Tipo'),
                'type' => 'select', 
                'list' => $tipiArray, 
                'align' => 'center', 
                'tmpTableFilter' => true,
                'filter_key' => 'tipo_fattura',
                'remove_onclick' => true
            ),
            'rif_ordine' => array(
                'title' => $this->l('ID Ordine'), 
                'align' => 'center',
            ),
            'id_customer' => array(
                'title' => $this->l('ID Cliente'), 
                'align' => 'center',
                'filter_key' => 'a!id_customer',
                'remove_onclick' => true
            ),
            'customer' => array(
                'title' => $this->l('Customer'), 
                'filter_key' => 'customer',
                'tmpTableFilter' => true,
                'remove_onclick' => true
            ), // Correggere: se uso la stringa gruppo (gl.name) e filtro per Tipo, errore "Duplicate column name 'gruppo'"
            'gruppoz' => array(
                'title' => $this->l('Group'),
                'type' => 'select',
                'list' => $groupArray,
                'align' => 'center',
                'filter_key' => 'c!id_default_group',
                'remove_onclick' => true
            ),
            'data_fattura' => array(
                'title' => $this->l('Data nota'), 
                'type' => 'date', 
                'align' => 'center',
                'remove_onclick' => true
            )
        );
        
        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Note di Credito'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }
    
    public function initToolbar()
    {
        parent::initToolbar();

        unset($this->toolbar_btn['new']); // Toglie il pulsante ADD NEW dalla toolbar
    }

    public function printLink($value)
    {
        $id_customer = Db::getInstance()->getValue('SELECT id_customer FROM '._DB_PREFIX_.'note_di_credito WHERE id_fattura = "'.$value.'"');
        $tipo = Db::getInstance()->getValue('SELECT tipo FROM '._DB_PREFIX_.'note_di_credito WHERE id_fattura = "'.$value.'"');
        return '<a title="Vedi nota di credito" href="/ezadmin/index.php?controller=AdminCustomers&id_customer='.$id_customer.'&viewcustomer&tab_name=invoices&id_fattura='.$value.'&azione=fattura_view&tipo='.$tipo.'&tipo_fattura=nota&token='.Tools::getAdminTokenLite('AdminCustomers').'" target="_blank">'.$value.'</a>';
    }
}