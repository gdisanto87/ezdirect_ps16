<?php

class AdminBDLControllerCore extends AdminController
{
    public function __construct()
    {
        $this->displayInformation('Installare dipendenze html2pdf');

        $this->bootstrap = true;
        $this->table = 'bdl';
        $this->className = 'BDL';
        $this->lang = false;
        $this->explicitSelect = true;

        $this->allow_export = true;

        $this->context = Context::getContext();
        $this->context->currency = Currency::getDefaultCurrency();

        $this->_select = '
            (CASE c.is_company
            WHEN 1
            THEN c.company
            ELSE
            CONCAT(c.firstname," ",c.lastname)
            END
            ) 
            cliente,
		
            (CASE WHEN a.rif_ordine > 0
            THEN "closed"
            WHEN a.id_bdl < 384
            THEN "closed"
            WHEN a.rif_ordine = 0 AND a.invio_controllo = 1 AND a.invio_contabilita = 0
            THEN "pending1"
            WHEN a.rif_ordine = 0 AND a.invio_controllo = 1 AND a.invio_contabilita = 1
            THEN "closed"
            WHEN a.invio_controllo = 2 AND a.invio_contabilita = 2
            THEN "closed"
            WHEN a.rif_ordine = 0 AND a.invio_controllo = 0
            THEN "open"
            WHEN a.nessun_addebito = 1
            THEN "closed"
            ELSE "open"
            END
            ) 
            stato,

            (SELECT CONCAT(firstname," ",LEFT(lastname, 1),".")  
            FROM '._DB_PREFIX_.'employee 
            WHERE id_employee = a.effettuato_da) 
            impiegato,
		
            (CASE c.is_company
            WHEN 1
            THEN c.vat_number
            WHEN 0
            THEN c.tax_code
            ELSE c.tax_code
            END
            ) 
            piva_cf
        ';

        $this->_join = 'JOIN '._DB_PREFIX_.'customer c ON a.id_customer = c.id_customer';

        if($this->context->employee->id == 2) {
			$this->_where = ' AND a.fatturato = 0 AND a.invio_contabilita > 0 ';
		}

		$this->_where .= ' AND a.nessun_addebito = 0 ';
        $this->_group = 'GROUP BY a.id_bdl';
        
        $this->_use_found_rows = true;
		
		$statusArray = array(
			'open' => $this->l('Aperto'),
			'closed' => $this->l('Chiuso'),
			'pending1' => $this->l('Check')
        );
        
        $the_employees = Db::getInstance()->executeS('
            SELECT `id_employee`, `firstname`, `lastname`
            FROM `'._DB_PREFIX_.'employee`
            WHERE `active` = 1
            ORDER BY `firstname` ASC
        ');

        $employees_list[0] = 'Nessuno';
        foreach ($the_employees as $employee) {
            $employees_list[$employee['id_employee']] = $this->findEmployee($employee['id_employee']);
        }

        $this->fields_list = array(
            'id_bdl' => array(
                'title' => $this->l('ID BDL'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
                'orderby' => true
            ),
            'id_customer' => array(
                'title' => $this->l('ID Customer'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
                'filter_key' => 'a!id_customer',
            ),
            'cliente' => array(
                'title' => $this->l('Customer'),
                'align' => 'text-center',
                'filter_key' => 'cliente',
                'filter_type' => 'string',
                'tmpTableFilter' => true,
            ),
            'piva_cf' => array(
                'title' => $this->l('PI/CF'),
                'align' => 'text-center',
                'tmpTableFilter' => true,
            ),
            'rif_ordine' => array(
                'title' => $this->l('Rif. Ordine'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
            ),
            'importo' => array(
                'title' => $this->l('Importo'),
                'align' => 'text-right',
                'class' => 'fixed-width-xs',
                'prefix' => '<strong>',
                'suffix' => '</strong>',
                'callback' => 'format_prezzo'
            ),
            'pagato' => array(
                'title' => $this->l('Pagato?'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
                'orderby' => false,
                'type' => 'select',
                'list' => array(1 => 'Sì', 0 => 'No'),
                'filter_key' => 'a!pagato',
                'callback' => 'printPagatoIcon',
            ),
            'stato' => array(
                'title' => $this->l('Status'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
                'hint' => 'ROSSO: in lavorazione. GIALLO: attività completata, in attesa di check amministrativo. VERDE: chiuso e verificato.',
                'type' => 'select',
                'list' => $statusArray,
                'filter_key' => 'stato',
                'filter_type' => 'string',
                'tmpTableFilter' => true,
                'callback' => 'statusStyle'
            ),
            'fatturato' => array(
                'title' => $this->l('Fatturato?'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
                'type' => 'select',
                'list' => array(1 => 'Sì', 0 => 'No'),
                'orderby' => false,
                'filter_key' => 'a!fatturato',
                'callback' => 'printFatturatoIcon',
            ),
            'impiegato' => array(
                'title' => $this->l('Fatto da'),
                'align' => 'text-center',
                'type' => 'select',
                'list' => $employees_list,
                'filter_key' => 'a!effettuato_da'
            ),
            'date_add' => array(
                'title' => $this->l('Data Inserimento'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
                'type' => 'date',
                'callback' => 'format_data',
                'filter_key' => 'a!date_add'
            ),
        );

        $this->redirect_after = ' '; // necessario per function redirect()

        $this->addRowAction('view');
        $this->addRowAction('edit');

        // Ezio, Federico e Carolina possono cancellare i BDL; meglio fare un controllo su profile (se è in comune)
        if($this->context->employee->id == 1 || $this->context->employee->id == 6 || $this->context->employee->id == 22) {
			$this->addRowAction('delete');
        }
        
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?'),
                'icon' => 'icon-trash'
            )
        );
        
        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Buoni di Lavoro'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }
    
    public function initToolbar()
    {
        parent::initToolbar();

        unset($this->toolbar_btn['new']); // Toglie il pulsante ADD NEW dalla toolbar
    }

    protected function redirect()
    {
        // Redirect all'anagrafica del cliente se action = view
        if(Tools::getIsset('id_bdl') && Tools::getIsset('viewbdl')){
            $id_customer = Db::getInstance()->getValue('SELECT id_customer FROM '._DB_PREFIX_.'bdl WHERE id_bdl = '.Tools::getValue('id_bdl'));
            $this->currentIndex = 'AdminCustomers';
            $this->token = Tools::getAdminTokenLite('AdminCustomers');
            $this->redirect_after = '/ezadmin/index.php?controller='.$this->currentIndex.'&id_customer='.$id_customer.'&viewcustomer&tab_name=bdl&token='.$this->token;
            Tools::redirectAdmin($this->redirect_after);
        }
        // action = edit
        else if(Tools::getIsset('id_bdl') && Tools::getIsset('updatebdl')){
            $id_customer = Db::getInstance()->getValue('SELECT id_customer FROM '._DB_PREFIX_.'bdl WHERE id_bdl = '.Tools::getValue('id_bdl'));
            $this->currentIndex = 'AdminCustomers';
            $this->token = Tools::getAdminTokenLite('AdminCustomers');
            $this->redirect_after = '/ezadmin/index.php?controller='.$this->currentIndex.'&id_customer='.$id_customer.'&viewcustomer&tab_name=bdl&azione=bdl_edit&id_bdl='.Tools::getValue('id_bdl').'&token='.$this->token;
            Tools::redirectAdmin($this->redirect_after);
        }
        // action = delete
        else if(Tools::getIsset('id_bdl') && Tools::getIsset('deletebdl')){
            Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'bdl WHERE id_bdl = '.Tools::getValue('id_bdl'));            
            Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
        }
    }

    public function renderList()
    {
        if(Tools::getIsset('azione') && Tools::getValue('azione') == 'bdl_pdf_ezdirect'){

            $content = BDL::generatePDFBDL('vuoto', 'ezdirect', 'y');

            require_once('../classes/html2pdf/Html2Pdf.php');
            ob_clean();

            $html2pdf = new Html2Pdf('P','A4','it');
            $html2pdf->writeHTML($content);
            
            ob_start();
            ob_end_clean();

            $html2pdf->output('vuoto-buono-di-lavoro.pdf', 'D');
        }
        else if(Tools::getIsset('azione') && Tools::getValue('azione') == 'bdl_pdf_tecnotel'){

            $content = BDL::generatePDFBDL('vuoto', 'tecnotel', 'y');

            require_once('../classes/html2pdf/Html2Pdf.php');
            ob_clean();

            $html2pdf = new Html2Pdf('P','A4','it');
            $html2pdf->writeHTML($content);
                        
            ob_start();
            ob_end_clean();
            $html2pdf->output('vuoto-buono-di-lavoro.pdf', 'D');            
        }

        return parent::renderList();
    }

    // spostare in una classe / file di utilità ed usarla dichiarandola una sola volta
    // potrebbe non serve più una volta cambiata la lingua di PS in italiano
    public function format_data($value)
	{
        $data = new DateTime($value);
        $data = $data->format('d/m/Y');

        return '<span>'.$data.'</span>';
    }

    // spostare in una classe / file di utilità ed usarla dichiarandola una sola volta
    public function format_prezzo($value)
	{
        // CON SIMBOLO EURO E . PER I DECIMALI
        /* $prezzo = (float)(str_replace(",",".",$value));
        $prezzo = Tools::displayPrice($prezzo, $this->context->currency); */

        $prezzo = number_format($value, 2, ',', '.');

        return '<span>'.$prezzo.'</span>';
    }

    public function statusStyle($value)
    {
        return ($value == 'closed' ? '<span class="badge badge-success squadrato thick">Chiuso</span>' : 
                ($value == 'open' ? '<span class="badge badge-danger squadrato thick">Aperto</span>' : 
                '<span class="badge badge-warning squadrato thick">Check</span>')
        );
    }

    public function processChangePagatoVal()
    {
        $bdl = new BDL($this->id_object);
        if (!Validate::isLoadedObject($bdl)) {
            $this->errors[] = Tools::displayError('An error occurred while updating bdl information.');
        }
        $bdl->pagato = ($bdl->pagato ? 0 : 1);
        if (!$bdl->update()) {
            $this->errors[] = Tools::displayError('An error occurred while updating bdl information.');
        }
        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    }

    public function processChangeFatturatoVal()
    {
        $bdl = new BDL($this->id_object);
        if (!Validate::isLoadedObject($bdl)) {
            $this->errors[] = Tools::displayError('An error occurred while updating bdl information.');
        }
        $bdl->fatturato = ($bdl->fatturato ? 0 : 1);
        if (!$bdl->update()) {
            $this->errors[] = Tools::displayError('An error occurred while updating bdl information.');
        }
        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    }

    public function printPagatoIcon($value, $bdl)
    {
        return '<a class="list-action-enable '.($value ? 'action-enabled' : 'action-disabled').'" href="index.php?'.htmlspecialchars('tab=AdminBDL&id_bdl='
            .(int)$bdl['id_bdl'].'&changePagatoVal&token='.Tools::getAdminTokenLite('AdminBDL')).'">
				'.($value ? '<i class="icon-check"></i>' : '<i class="icon-remove"></i>').
            '</a>';
    }

    public function printFatturatoIcon($value, $bdl)
    {
        return '<a class="list-action-enable '.($value ? 'action-enabled' : 'action-disabled').'" href="index.php?'.htmlspecialchars('tab=AdminBDL&id_bdl='
            .(int)$bdl['id_bdl'].'&changeFatturatoVal&token='.Tools::getAdminTokenLite('AdminBDL')).'">
				'.($value ? '<i class="icon-check"></i>' : '<i class="icon-remove"></i>').
            '</a>';
    }

    public function initProcess()
    {
        parent::initProcess();

        if (Tools::isSubmit('changePagatoVal') && $this->id_object) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'change_pagato_val';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        } elseif (Tools::isSubmit('changeFatturatoVal') && $this->id_object) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'change_fatturato_val';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        }
    }

    // Esiste già in AdminCarts, spostare in una classe / file di utilità ed usarla dichiarandola una sola volta
    public function findEmployee($value)
    {
        if($value){
            $employee = Db::getInstance()->getRow('SELECT firstname, lastname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$value);

            $firstname = $employee['firstname'];
            $lastname = $employee['lastname'][0]; // Iniziale del cognome
            $name = $firstname.' '.$lastname.'.';
        }
        else
            $name = '-';
        
        return $name;
    }
}