<?php

class AdminTuttiPrezziAcqSpecialiControllerCore extends AdminController
{
    public function __construct()
    {
        $this->displayWarning('Correggere azione edit: il tasto salva non disabilita gli input e le date diventano undefined (ma vengono salvate correttamente)');

        $this->bootstrap = true;
        $this->table = 'specific_price_wholesale';
        $this->lang = false;
        $this->explicitSelect = true;

        $this->allow_export = true;

        $this->context = Context::getContext();
        $this->context->currency = Currency::getDefaultCurrency();

        $this->_select = '
            a.id_specific_price AS id_specific_price_wholesale, p.id_product AS id_prodotto, p.reference, p.stock_quantity, pl.name AS nome_prodotto, m.name AS costruttore,
            a.id_specific_price AS id_edit
		';
		
        $this->_join = '
            JOIN '._DB_PREFIX_.'product p ON p.id_product = a.id_product
            JOIN '._DB_PREFIX_.'product_lang pl ON pl.id_product = a.id_product
            JOIN '._DB_PREFIX_.'manufacturer m ON m.id_manufacturer = p.id_manufacturer 
        ';

        $this->_where = '
            AND pl.id_lang = '.$this->context->language->id.'
		';

        $this->_defaultOrderBy = 'a.id_specific_price';
        $this->_orderBy = 'a.id_specific_price';
        $this->_orderWay = 'ASC';
        
        $this->_use_found_rows = false;

        $this->fields_list = array(
            'id_specific_price_wholesale' => array(
                'title' => $this->l('ID Prezzo'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
                'filter_key' => 'a!id_specific_price',
                'remove_onclick' => true,
            ),
            'reference' => array(
                'title' => $this->l('Cod. Prodotto'),
                'align' => 'text-center',
                'callback' => 'creaTooltip',
                'remove_onclick' => true,
            ),
            'nome_prodotto' => array(
                'title' => $this->l('Desc. Prodotto'),
                'align' => 'text-left',
                'remove_onclick' => true,
            ),
            'costruttore' => array(
                'title' => $this->l('Costruttore'),
                'align' => 'text-left',
                'remove_onclick' => true,
            ),
            'stock_quantity' => array(
                'title' => $this->l('Mag. EZ'),
                'align' => 'text-left',
                'remove_onclick' => true,
            ),
            'pieces' => array(
                'title' => $this->l('Qt. disp.'),
                'align' => 'text-left',
                'callback' => 'coloraPieces',
                'remove_onclick' => true,
            ),
            'reduction_1' => array(
                'title' => $this->l('Sconto 1 (%)'),
                'align' => 'text-right',
                'class' => 'mod',
                'callback' => 'format_sconto_1',
                'remove_onclick' => true,
            ),
            'reduction_2' => array(
                'title' => $this->l('Sconto 2 (%)'),
                'align' => 'text-right',
                'class' => 'mod',
                'callback' => 'format_sconto_2',
                'remove_onclick' => true,
            ),
            'reduction_3' => array(
                'title' => $this->l('Sconto 3 (%)'),
                'align' => 'text-right',
                'class' => 'mod',
                'callback' => 'format_sconto_3',
                'remove_onclick' => true,
            ),
            'wholesale_price' => array(
                'title' => $this->l('Prezzo Speciale'),
                'align' => 'text-right',
                'class' => 'mod',
                'callback' => 'format_prezzo',
                'prefix' => '<b>',
                'suffix' => '</b>',
                'filter_key' => 'a!wholesale_price',
                'remove_onclick' => true,
            ),
            'from' => array(
                'title' => $this->l('Partenza'),
                'align' => 'text-right',
                'class' => 'mod mod_data_p',
                'type' => 'datetime',
                'callback' => 'format_data',
                'remove_onclick' => true,
            ),
            'to' => array(
                'title' => $this->l('Fine'),
                'align' => 'text-right', 
                'class' => 'mod mod_data_f',
                'type' => 'datetime',
                'filter_key' => 'a!to',
                'callback' => 'format_data_to',
                'remove_onclick' => true,
            ),
            'id_edit' => array(
                'title' => $this->l('Edit'),
                'align' => 'text-right', 
                'class' => 'edit',
                'orderby' => false,
                'search' => false,
                'callback' => 'crea_pulsante_edit',
                'remove_onclick' => true,
            ),
        );

        $this->addRowAction('delete');

        $this->bulk_actions = array( // Queste azioni si trovano in AdminController (processBulk...)
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?'),
                'icon' => 'icon-trash'
            ),
            'allunga' => array(
                'text' => $this->l('Allunga fino a fine mese (da oggi) i prezzi speciali flaggati'),
                'confirm' => $this->l('Apply changes to selected items?'),
                'icon' => 'icon-double-angle-right'
            ),
            'allunga_successivo' => array(
                'text' => $this->l('Allunga fino a fine mese SUCCESSIVO (da oggi) i prezzi speciali flaggati'),
                'confirm' => $this->l('Apply changes to selected items?'),
                'icon' => 'icon-arrow-right'
            )
        );
        
        parent::__construct();
    }

    public function renderList()
    {
        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'tooltip_prodotto.js');
        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'edit_acq_speciali.js'); // correggere
        $this->addJS(_PS_BO_DEFAULT_THEME_DIR_.'plugins/TaTa/dist/tata.js');

        return parent::renderList();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Tutti i prezzi speciali d\'acquisto'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
	}

    public function initToolbar()
    {
        parent::initToolbar();

        unset($this->toolbar_btn['new']); // Toglie il pulsante ADD NEW dalla toolbar
    }

    public function format_data($value)
	{
        if($value == '0000-00-00 00:00:00' || $value == '1970-01-01 00:00:00')
            $value = date('Y-01-01 00:00:00');

        $data = new DateTime($value);
        $data = $data->format('d/m/Y H:i');

        return '<span>'.$data.'</span>';
    }

    public function format_prezzo($value, $tr)
	{
        // CON SIMBOLO EURO E . PER I DECIMALI
        /* $prezzo = (float)(str_replace(",",".",$value));
        $prezzo = Tools::displayPrice($prezzo, $this->context->currency); */

        $prezzo = number_format($value, 2, ',', '.');

        return '<input type="text" id="new_special_price_'.$tr['id_specific_price_wholesale'].'" name="new_special_price_'.$tr['id_specific_price_wholesale'].'" value="'.$prezzo.'" readonly />';
        // return '<span>'.$prezzo.'</span>';
    }

    public function format_sconto_1($value, $tr)
	{
        $sconto = number_format($value, 2, ',', '.');

        return '<input type="text" id="sconto_1_'.$tr['id_specific_price_wholesale'].'" name="sconto_1_'.$tr['id_specific_price_wholesale'].'" value="'.$sconto.'" readonly />';
        // return '<span>'.$sconto.' %</span>';
    }

    public function format_sconto_2($value, $tr)
	{
        $sconto = number_format($value, 2, ',', '.');

        return '<input type="text" id="sconto_2_'.$tr['id_specific_price_wholesale'].'" name="sconto_2_'.$tr['id_specific_price_wholesale'].'" value="'.$sconto.'" readonly />';
        // return '<span>'.$sconto.' %</span>';
    }

    public function format_sconto_3($value, $tr)
	{
        $sconto = number_format($value, 2, ',', '.');

        return '<input type="text" id="sconto_3_'.$tr['id_specific_price_wholesale'].'" name="sconto_3_'.$tr['id_specific_price_wholesale'].'" value="'.$sconto.'" readonly />';
        // return '<span>'.$sconto.' %</span>';
    }

    public function creaTooltip($value, $tr)
	{
        $tokenProducts = Tools::getAdminTokenLite('AdminProducts');
        $href_prodotto = '/ezadmin/index.php?controller=AdminProducts&id_product='.$tr['id_prodotto'].'&updateproduct&token='.$tokenProducts;
        return '<a class="tooltip_prodotto" title="'.Product::showProductTooltip($tr['id_prodotto']).'" href="'.$href_prodotto.'" target="_blank">'.$value.'</a>';
    }

    public function coloraPieces($value)
	{
        if(($value == 0 && $value != ''))
            return '<span class="thick rosso">'.$value.'</span>';
        else
            return $value;
    }

    public function format_data_to($value)
	{
        if($value == '0000-00-00 00:00:00' || $value == '1970-01-01 00:00:00')
            $value = date('Y-01-01 00:00:00');

        $data = new DateTime($value);
        $data = $data->format('d/m/Y H:i');

        if($value < date('Y-m-d H:i:s'))
            return '<span class="thick rosso">'.$data.'</span>';
        else
            return '<span>'.$data.'</span>';
    }

    public function crea_pulsante_edit($value)
    {
        return '<a class="edit" title="Edit"><i class="icon-edit"></i></a><a class="add" title="Save"><i class="icon-save"></i></a>';
    }
}