<?php

class AdminMarketingClientiPreventiviControllerCore extends AdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->lang = false;

        $this->context = Context::getContext();

        $this->display = 'view';

        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Strumento marketing per i preventivi'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }

    public function renderView()
    {
        $this->displayInformation('Verificare funzionamento date');

        if(isset($_POST['cercaiclienti'])) 
        {
            if(!empty($_POST['arco_dal'])) {
                // $data_dal_a = explode("-", $_POST['arco_dal']);
                // $data_dal = $data_dal_a[2]."-".$data_dal_a[1]."-".$data_dal_a[0]; 
                
                $arco_dal = 'AND o.date_add > "'.$data_dal.' 00:00:00"';
                $striscia_arco_dal = "Da: <strong>".$_POST['arco_dal']."</strong> ";
            }
            else {
                $striscia_arco_dal = "Da: <strong>inizio della storia del sito</strong> ";
            }
            
            if(isset($_POST['striscia_arco_dal'])) {
                $striscia_arco_dal = $_POST['striscia_arco_dal'];
            }
        
            $riepilogo_html .= $striscia_arco_dal;
            
            if(!empty($_POST['arco_al'])) {
                // $data_al_a = explode("-", $_POST['arco_al']);
                // $data_al = $data_al_a[2]."-".$data_al_a[1]."-".$data_al_a[0]; 

                $arco_al = 'AND o.date_add < "'.$data_al.' 00:00:00"';
                $striscia_arco_al = "a: <strong>".$_POST['arco_al']."</strong> ";
            }
            else {
                $striscia_arco_al = "a: <strong>oggi</strong> ";
            
            }
            
            $riepilogo_html .= $striscia_arco_al;
            $riepilogo_html .= "<br />";

            $striscia_gruppi = "Gruppi clienti: ";

            if(Tools::getIsset('clienti')) { $striscia_gruppi .= "<strong>Clienti</strong> - "; }
            if(Tools::getIsset('rivenditori')) { $striscia_gruppi .= "<strong>Rivenditori</strong> - "; }
            if(Tools::getIsset('distributori')) { $striscia_gruppi .= "<strong>Distributori</strong> - "; }

            $riepilogo_html .= $striscia_gruppi;
            $riepilogo_html .= "<br />";
            
            $striscia_tipo = "Tipo clienti: ";
            if(Tools::getIsset('aziende')) { $striscia_tipo .= "<strong>Aziende</strong> - "; }
            if(Tools::getIsset('privati')) { $striscia_tipo .= "<strong>Privati</strong> - "; }
        
            $riepilogo_html .= $striscia_tipo;
            $riepilogo_html .= "<br />";
            
            $orderby = "";
            
            switch($_POST['cercaiclienti']) 
            {
                case 'idcustomerasc': $orderby = "ORDER BY c.id_customer ASC"; break;
                case 'idcustomerdesc': $orderby = "ORDER BY c.id_customer DESC"; break;
                case 'lastnameasc': $orderby = "ORDER BY c.lastname ASC"; break;
                case 'lastnamedesc': $orderby = "ORDER BY c.lastname DESC"; break;
                case 'companyasc': $orderby = "ORDER BY c.company ASC"; break;
                case 'companydesc': $orderby = "ORDER BY c.company DESC"; break;
                case 'clienteasc': $orderby = "ORDER BY cliente ASC"; break;
                case 'clientedesc': $orderby = "ORDER BY cliente DESC"; break;
                case 'iscompanyasc': $orderby = "ORDER BY tipo_cliente ASC"; break;
                case 'iscompanydesc': $orderby = "ORDER BY tipo_cliente DESC"; break;
                case 'groupasc': $orderby = "ORDER BY gruppo_cliente ASC"; break;
                case 'groupdesc': $orderby = "ORDER BY gruppo_cliente DESC"; break;
                case 'stateasc': $orderby = "ORDER BY s.iso_code ASC"; break;
                case 'statedesc': $orderby = "ORDER BY s.iso_code DESC"; break;
                case 'emailasc': $orderby = "ORDER BY c.email ASC"; break;
                case 'emaildesc': $orderby = "ORDER BY c.email DESC"; break;
                case 'telefonoasc': $orderby = "ORDER BY a.phone ASC"; break;
                case 'telefonodesc': $orderby = "ORDER BY a.phone DESC"; break;
                case 'cellulareasc': $orderby = "ORDER BY a.phone_mobile ASC"; break;
                case 'cellularedesc': $orderby = "ORDER BY a.phone_mobile DESC"; break;
                default: $orderby = "ORDER BY c.id_default_group, c.id_customer ASC"; break;
            }
            
            if(Tools::getIsset('querysql')) {
                $query = stripslashes($_POST['querysql']);
            }
            else {
                $query = '
                    SELECT c.id_customer, c.is_company, c.id_default_group, 
                    (CASE WHEN c.is_company = 0 THEN CONCAT(c.lastname," ",c.firstname) WHEN c.is_company = 1 THEN (CASE WHEN c.company IS NULL OR c.company = "" THEN CONCAT(c.lastname," ",c.firstname) ELSE c.company END) END) cliente, 
                    c.firstname, c.lastname, c.company, c.email, 
                    (CASE c.is_company WHEN 1 THEN "Azienda" ELSE "Privato" END) tipo_cliente, (CASE c.id_default_group WHEN 1 THEN "Clienti web" WHEN 3 THEN "Rivenditori" WHEN 12 THEN "Distributori" ELSE "Altro" END) gruppo_cliente, 
                    a.address1, a.postcode, a.city, a.phone, a.phone_mobile, a.fax, s.iso_code 
                    FROM '._DB_PREFIX_.'customer c 
                        JOIN '._DB_PREFIX_.'address a ON c.id_customer = a.id_customer 
                        JOIN '._DB_PREFIX_.'state s ON a.id_state = s.id_state 
                    WHERE a.active = 1 AND a.fatturazione = 1 AND a.deleted = 0 
                        AND (c.is_company = 9
                        '.(Tools::getIsset('aziende') ? 'OR c.is_company = 1 ' : '').'
                        '.(Tools::getIsset('privati') ? 'OR c.is_company = 0 ' : '').' 
                        )
                        AND (c.id_default_group = 99999
                        '.(Tools::getIsset('clienti') ? 'OR c.id_default_group = 1 ' : '').'
                        '.(Tools::getIsset('rivenditori') ? 'OR c.id_default_group = 3 ' : '').' 
                        '.(Tools::getIsset('distributori') ? 'OR c.id_default_group = 12' : '').' 
                        )
                        AND c.id_customer IN (SELECT cart.id_customer FROM '._DB_PREFIX_.'cart LEFT JOIN '._DB_PREFIX_.'orders ON cart.id_cart = orders.id_cart WHERE cart.id_employee != 0
                        '.(!empty($_POST['arco_dal']) ? 'AND cart.date_add > "'.date("Y-m-d H:i:s", strtotime(Tools::getValue('arco_dal')." 00:00:00")).'" ' : '').' 
                        '.(!empty($_POST['arco_al'])  ? 'AND cart.date_add < "'.date("Y-m-d H:i:s", strtotime(Tools::getValue('arco_al')." 23:59:59")).'" ' : '').'
                        AND orders.id_order IS NULL)
                ';
            }
            
            $query_da_eseguire = $query." ".$orderby;
        
            $resultsclienti = Db::getInstance()->executeS(stripslashes($query_da_eseguire));
            $i = 1;
            
            $riepilogo_html .= "<form method='post' action=''>";
            $riepilogo_html .= "<table class='table'><thead class='persist-header'>";
            $riepilogo_html .= "<tr>";
            $riepilogo_html .= "<th style='width:30px'>N.</th>";
            $riepilogo_html .= "<th style='width:45px'>Id cliente 
            <br />
            <input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)' value='idcustomerasc' /></a>   
            <input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)' value='idcustomerdesc' /></a>   
            </th>";
            $riepilogo_html .= "<th style='width:150px'>Cliente
            <br />
            <input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)' value='clienteasc' /></a>   
            <input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)' value='clientedesc' /></a> 
            </th>";
            $riepilogo_html .= "<th style='width:50px'>Tipo
            <br />
            <input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)' value='iscompanyasc' /></a>   
            <input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)' value='iscompanydesc' /></a> 
            </th>";
            $riepilogo_html .= "<th style='width:50px'>Gruppo
            <br />
            <input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)' value='groupasc' /></a>   
            <input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)' value='groupdesc' /></a> 
            </th>";
            $riepilogo_html .= "<th style='width:30px'>Provincia
            <br />
            <input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)' value='stateasc' /></a>   
            <input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)' value='statedesc' /></a> 
            </th>";
            $riepilogo_html .= "<th style='width:80px'>Email
            <br />
            <input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)' value='emailasc' /></a>   
            <input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)' value='emaildesc' /></a> 
            </th>
            ";
            $riepilogo_html .= "<th style='width:80px'>Telefono
            <br />
            <input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)' value='telefonoasc' /></a>   
            <input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)' value='telefonodesc' /></a> 
            </th>
            ";
            $riepilogo_html .= "<th style='width:80px'>Cellulare
            <br />
            <input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)' value='cellulareasc' /></a>   
            <input type='submit' name='cercaiclienti' style='width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)' value='cellularedesc' /></a> 
            </th>
            ";
            $riepilogo_html .= "<th>Vai</th>";
            $riepilogo_html .= "</tr></thead>";
            
            foreach ($resultsclienti as $row) {
                $riepilogo_html .= "<tr>";
                $riepilogo_html .= "<td style='width:30px'>".$i."</td>";
                $riepilogo_html .= "<td>".$row['id_customer']."</td>";
                $riepilogo_html .= "<td>".$row['cliente']."</td>";
                $riepilogo_html .= "<td>".$row['tipo_cliente']."</td>";
                $riepilogo_html .= "<td>".$row['gruppo_cliente']."</td>";
                $riepilogo_html .= "<td style='width:30px'>".$row['iso_code']."</td>";
                $riepilogo_html .= "<td>".$row['email']."</td>";
                $riepilogo_html .= "<td>".$row['phone']."</td>";
                $riepilogo_html .= "<td>".$row['phone_mobile']."</td>";	
                $riepilogo_html .= "<td><a href='index.php?controller=AdminCustomers&id_customer=".$row['id_customer']."&viewcustomer&token=".$tokenCustomers."' target='_blank'><img src='../img/admin/details.gif' alt='Apri cliente' title='Apri cliente' /></a></td>";
                $riepilogo_html .= "</tr>";
                $i++;                
            }
            
            $riepilogo_html .= "</table>";
            $riepilogo_html .= "
            <input type='hidden' name='num' value='".$i."' />		
            <input type='hidden' name='orderby' value='".$orderby."' />	
            <input type='hidden' name='striscia_arco_dal' value='".strip_tags($striscia_arco_dal)."' />	
            <input type='hidden' name='striscia_arco_al' value='".strip_tags($striscia_arco_al)."' />	
            <input type='hidden' name='striscia_gruppi' value='".strip_tags($striscia_gruppi)."' />	
            <input type='hidden' name='striscia_tipo' value='".strip_tags($striscia_tipo)."' />	
            ";
            $riepilogo_html .= "<input type='hidden' name='querysql' value='".(isset($_POST['querysql']) ? stripslashes($_POST['querysql']) : $query)."' />	";
            
            $riepilogo_html .= "<input type='submit' value='Esporta i dati in formato Excel' name='esportaexcel' class='button' />";
            $riepilogo_html .= "</form>";

            $this->tpl_view_vars = array(
                'riepilogo_html' => $riepilogo_html, // Variabile che sostituisce gli echo finchè non viene portato tutto in bootstrap
            );
        }
        else if(isset($_POST['esportaexcel']))
        {
            // da fare, vedi 1.4
        }
        else {
            // $this->tpl_view_vars = array();
        }

        return parent::renderView();
    }
}
