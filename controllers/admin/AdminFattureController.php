<?php

class AdminFattureControllerCore extends AdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
		$this->table = 'fattura';
        $this->lang = false;
        $this->explicitSelect = true;

        $this->context = Context::getContext();

		$this->_select = '
			(CASE c.is_company
			WHEN 0
			THEN CONCAT(c.firstname," ",c.lastname)
			WHEN 1
			THEN c.company
			ELSE
			CONCAT(c.firstname," ",c.lastname)
			END
			) AS customer,

            c.company,
            c.id_default_group,
            gl.name as gruppo
        ';
		
		$this->_join = 'LEFT JOIN `'._DB_PREFIX_.'customer` c ON (c.`id_customer` = a.`id_customer`)
                        LEFT JOIN `'._DB_PREFIX_.'group_lang` gl ON (c.`id_default_group` = gl.`id_group` AND gl.id_lang = '.(int)$this->context->language->id.')
        ';
        
        // Il tipo 'Tutto tranne estero' è una combinazione di altri tipi. Uso questo metodo per non modificare la funzione processFilter di AdminController.
        if(Tools::getIsset('fatturaFilter_tipo_fattura') && Tools::getValue('fatturaFilter_tipo_fattura') == 'Tutto tranne estero')
            $this->_tmpTableFilter .= ' AND (tipo_fattura = "Fattura cliente (accompagnatoria)" OR tipo_fattura = "Fattura cliente (immediata)" OR tipo_fattura = "Fattura cliente (differita)") OR 1';

        $this->_group = 'GROUP BY a.id_storico, a.id_fattura';
        
        $this->_orderBy = 'data_fattura';
        $this->_orderWay = 'DESC';

        $this->_use_found_rows = true;

		$tipiArray = array(
            'Tutto tranne estero' => 'Tutto tranne estero', 
            'Fattura cliente (accompagnatoria)' => 'Fattura cliente (accompagnatoria)', 
            'Fattura cliente (immediata)' => 'Fattura cliente (immediata)', 
            'Fattura cliente (differita)' => 'Fattura cliente (differita)', 
            'FATTURA ACCOMPAGNATORIA EZ' => 'FATTURA ACCOMPAGNATORIA EZ',
            'Fattura cli. estero accompagnatoria' => 'Fattura cli. estero accompagnatoria',
            'Fat.acc.clienti privati Germania' => 'Fat.acc.clienti privati Germania',
        );

        $gruppi = Group::getGroups($this->context->language->id);
        $groupArray = array();
        foreach($gruppi as $gruppo){
            $groupArray = $groupArray + array($gruppo['id_group'] => $gruppo['name']);
        }
	 	
 		$this->fields_list = array(
            'id_fattura' => array(
                'title' => $this->l('ID Fattura'),  
                'align' => 'center'
            ),
            'id_storico' => array(
                'title' => $this->l('ID Storico'), 
                'align' => 'center'
            ),
            'tipo_fattura' => array(
                'title' => $this->l('Tipo'),
                'type' => 'select', 
                'list' => $tipiArray, 
                'align' => 'center', 
                'tmpTableFilter' => true, 
                'filter_key' => 'tipo_fattura' 
            ),
            'rif_ordine' => array(
                'title' => $this->l('ID Ordine'), 
                'align' => 'center',
            ),
            'id_customer' => array(
                'title' => $this->l('ID Cliente'), 
                'align' => 'center',
                'filter_key' => 'a!id_customer',
            ),
            'customer' => array(
                'title' => $this->l('Customer'), 
                'filter_key' => 'customer', 
                'tmpTableFilter' => true
            ), // Correggere: se uso la stringa gruppo (gl.name) e filtro per Tipo, errore "Duplicate column name 'gruppo'"
            'gruppoz' => array(
                'title' => $this->l('Group'),
                'type' => 'select',
                'list' => $groupArray,
                'align' => 'center',
                'filter_key' => 'c!id_default_group',
            ),
            'data_fattura' => array(
                'title' => $this->l('Data fattura'), 
                'type' => 'date', 
                'align' => 'center',
            )
        );
        
        $this->redirect_after = ' '; // necessario per function redirect()

        $this->addRowAction('view');
        
        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Fatture'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }
    
    public function initToolbar()
    {
        parent::initToolbar();

        unset($this->toolbar_btn['new']); // Toglie il pulsante ADD NEW dalla toolbar
    }

    protected function redirect()
    {
        // Redirect alla fattura in customers se action = view
        if(Tools::getIsset('id_fattura') && Tools::getIsset('viewfattura')){
            $id_customer = Db::getInstance()->getValue('SELECT id_customer FROM '._DB_PREFIX_.'fattura WHERE id_fattura = "'.Tools::getValue('id_fattura').'"');
            $tipo = Db::getInstance()->getValue('SELECT tipo FROM '._DB_PREFIX_.'fattura WHERE id_fattura = "'.Tools::getValue('id_fattura').'"');
            $this->currentIndex = 'AdminCustomers';
            $this->token = Tools::getAdminTokenLite('AdminCustomers');
            $this->redirect_after = '/ezadmin/index.php?controller='.$this->currentIndex.'&id_customer='.$id_customer.'&viewcustomer&tab_name=invoices&id_fattura='.Tools::getValue('id_fattura').'&azione=fattura_view&tipo='.$tipo.'&tipo_fattura=fattura&token='.$this->token;
            Tools::redirectAdmin($this->redirect_after);
        }
    }
}