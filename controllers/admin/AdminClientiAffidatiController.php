<?php

// Potrei togliere Core e trasformare in override
class AdminClientiAffidatiControllerCore extends AdminController
{
    protected $_defaultOrderBy = 'ca!fino_a';
    protected $_defaultOrderWay = 'DESC';

    public function __construct()
    {
        $this->displayWarning("Correggere calcolo residuo");
        $this->bootstrap = true;
        $this->table = 'customer';
        $this->lang = false;
        $this->explicitSelect = true;

        $this->allow_export = true;

        $this->context = Context::getContext();
        $this->context->currency = Currency::getDefaultCurrency();

        $this->_select = '
            (CASE a.is_company
            WHEN 0
            THEN CONCAT(a.firstname," ",a.lastname)
            WHEN 1
            THEN a.company
            ELSE
            CONCAT(a.firstname," ",a.lastname)
            END
            ) AS cliente
        ';

        $this->_join = '
            LEFT JOIN customer_amministrazione ca ON (a.`id_customer` = ca.`id_customer`)
        ';

        $this->_where = '
            AND ca.`fido` != ""
        ';
        
        $this->_group = '
            GROUP BY a.`id_customer`
        ';
        
        $this->_use_found_rows = true;

        $this->fields_list = array(
            'id_customer' => array(
                'title' => $this->l('ID CRM'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
            ),
            'codice_esolver' => array(
                'title' => $this->l('eSolver Code'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
                'filter_key' => 'a!codice_esolver'
            ),
            'cliente' => array(
                'title' => $this->l('Customer'),
                'align' => 'text-left',
                'tmpTableFilter' => true,
            ),
            'fido' => array(
                'title' => $this->l('Fido'),
                'align' => 'text-right',
                'class' => 'fixed-width-md',
                'callback' => 'format_prezzo',
                'filter_key' => 'ca!fido'
            ),
            'speso' => array(
                'title' => $this->l('Speso'),
                'align' => 'text-right',
                'class' => 'fixed-width-md',
                'search' => false,
                'orderby' => false,
                'callback' => 'calcola_speso',
                'filter_key' => 'ca!id_customer'
            ),
            'residuo' => array(
                'title' => $this->l('Residuo'),
                'align' => 'text-right',
                'class' => 'fixed-width-md',
                'search' => false,
                'orderby' => false,
                'callback' => 'calcola_residuo',
                'filter_key' => 'ca!id_customer'
            ),
            'data_fido' => array(
                'title' => $this->l('Data registrazione fido'),
                'align' => 'text-right',
                'type' => 'date',
                'callback' => 'format_data',
            ),
            'fino_a' => array(
                'title' => $this->l('Scadenza'),
                'align' => 'text-right',
                'type' => 'date',
                'callback' => 'format_data',
            ),
        );

        $this->redirect_after = ' '; // necessario per function redirect()

        $this->addRowAction('view');
        
        parent::__construct();
    }

    public function initToolbar()
    {
        parent::initToolbar();

        unset($this->toolbar_btn['new']); // Toglie il pulsante ADD NEW dalla toolbar
    }

    // Redirect all'anagrafica del cliente se action = view
    protected function redirect()
    {
        if(Tools::getIsset('id_customer') && Tools::getIsset('viewcustomer')){
            $this->currentIndex = 'AdminCustomers';
            $this->token = Tools::getAdminTokenLite('AdminCustomers');
            $this->redirect_after = '/ezadmin/index.php?controller='.$this->currentIndex.'&id_customer='.Tools::getValue('id_customer').'&viewcustomer&tab_name=administration&token='.$this->token;
            Tools::redirectAdmin($this->redirect_after);
        }
    }

    public function format_data($value)
	{
        $data = new DateTime($value);
        $data = $data->format('d/m/Y');

        return '<span>'.$data.'</span>';
    }

    public function format_prezzo($value)
	{
        // CON SIMBOLO EURO E . PER I DECIMALI
        /* $prezzo = (float)(str_replace(",",".",$value));
        $prezzo = Tools::displayPrice($prezzo, $this->context->currency); */

        $prezzo = number_format($value, 2, ',', '.');

        return '<span>'.$prezzo.'</span>';
    }

    public function calcola_speso($value)
	{
        $data_fido = Db::getInstance()->getValue('SELECT data_fido FROM customer_amministrazione WHERE id_customer = '.$value);

		$somma_fido = Db::getInstance()->getValue('SELECT SUM(total_paid_real) FROM '._DB_PREFIX_.'orders WHERE id_customer = '.$value.' AND (payment = "Bonifico 30 gg. fine mese" OR payment = "Bonifico 60 gg. fine mese" OR payment = "Bonifico 90 gg. fine mese" OR payment = "Bonifico 30 gg. 15 mese successivo" OR payment = "R.B. 30 GG. D.F. F.M." OR payment = "R.B. 60 GG. D.F. F.M." OR payment = "R.B. 90 GG. D.F. F.M."  OR payment = "R.B. 30 GG. 5 mese successivo" OR payment = "R.B. 30 GG. 10 mese successivo" OR payment = "R.B. 60 GG. 5 mese successivo" OR payment = "R.B. 60 GG. 10 mese successivo" OR payment LIKE "%saldo%") AND id_order NOT IN (SELECT id_order FROM '._DB_PREFIX_.'order_history WHERE id_order_state = 18 OR id_order_state = 32) AND date_add > "'.$data_fido.'"');
        if(!$somma_fido)
            $somma_fido = 0;
        
		$somma_fido_32 = Db::getInstance()->getValue('SELECT SUM(total_paid_real) FROM '._DB_PREFIX_.'orders o JOIN (SELECT id_order_state, id_order FROM '._DB_PREFIX_.'order_history WHERE id_order_state = 32) oh ON o.id_order = oh.id_order WHERE id_customer = '.$value.' AND (payment = "Bonifico 30 gg. fine mese" OR payment = "Bonifico 60 gg. fine mese" OR payment = "Bonifico 90 gg. fine mese" OR payment = "Bonifico 30 gg. 15 mese successivo" OR payment = "R.B. 30 GG. D.F. F.M." OR payment = "R.B. 60 GG. D.F. F.M." OR payment = "R.B. 90 GG. D.F. F.M."  OR payment = "R.B. 30 GG. 5 mese successivo" OR payment = "R.B. 30 GG. 10 mese successivo" OR payment = "R.B. 60 GG. 5 mese successivo" OR payment = "R.B. 60 GG. 10 mese successivo" OR payment LIKE "%saldo%") AND o.id_order NOT IN (SELECT id_order FROM '._DB_PREFIX_.'order_history WHERE id_order_state = 18 OR id_order_state = 32) AND date_add > "'.$data_fido.'"');	
        if(!$somma_fido_32)
            $somma_fido_32 = 0;

        $speso = $somma_fido + $somma_fido_32;

        // CON SIMBOLO EURO E . PER I DECIMALI
        /* $speso = Tools::displayPrice($speso, $this->context->currency); */

        $speso = number_format($speso, 2, ',', '.');

        return '<span>'.$speso.'</span>';
    }

    public function calcola_residuo($value)
	{			
        $ca = Db::getInstance()->getRow('SELECT fido, data_fido FROM customer_amministrazione WHERE id_customer = '.$value);
        
        $fido = $ca['fido'];
        if(!$fido)
            $fido = 0;
        else
            $fido = (float)(str_replace(",",".",$fido));

        $data_fido = $ca['data_fido'];
        
		$somma_fido = Db::getInstance()->getValue('SELECT SUM(total_paid_real) FROM '._DB_PREFIX_.'orders WHERE id_customer = '.$value.' AND (payment = "Bonifico 30 gg. fine mese" OR payment = "Bonifico 60 gg. fine mese" OR payment = "Bonifico 90 gg. fine mese" OR payment = "Bonifico 30 gg. 15 mese successivo"  OR payment = "R.B. 30 GG. D.F. F.M." OR payment = "R.B. 60 GG. D.F. F.M." OR payment = "R.B. 90 GG. D.F. F.M." OR payment = "R.B. 30 GG. 5 mese successivo" OR payment = "R.B. 30 GG. 10 mese successivo" OR payment = "R.B. 60 GG. 5 mese successivo" OR payment = "R.B. 60 GG. 10 mese successivo" OR payment LIKE "%saldo%")  AND id_order NOT IN (SELECT id_order FROM '._DB_PREFIX_.'order_history WHERE id_order_state = 18 OR id_order_state = 32) AND date_add > "'.$data_fido.'"');
        if(!$somma_fido)
            $somma_fido = 0;
        
		$somma_fido_32 = Db::getInstance()->getValue('SELECT SUM(total_paid_real) FROM '._DB_PREFIX_.'orders o JOIN (SELECT id_order_state, id_order FROM '._DB_PREFIX_.'order_history WHERE id_order_state = 32) oh ON o.id_order = oh.id_order WHERE id_customer = '.$value.' AND (payment = "Bonifico 30 gg. fine mese" OR payment = "Bonifico 60 gg. fine mese" OR payment = "Bonifico 90 gg. fine mese" OR payment = "Bonifico 30 gg. 15 mese successivo" OR payment = "R.B. 30 GG. D.F. F.M." OR payment = "R.B. 60 GG. D.F. F.M." OR payment = "R.B. 90 GG. D.F. F.M."  OR payment = "R.B. 30 GG. 5 mese successivo" OR payment = "R.B. 30 GG. 10 mese successivo" OR payment = "R.B. 60 GG. 5 mese successivo" OR payment = "R.B. 60 GG. 10 mese successivo" OR payment LIKE "%saldo%") AND o.id_order NOT IN (SELECT id_order FROM '._DB_PREFIX_.'order_history WHERE id_order_state = 18 OR id_order_state = 32) AND date_add > "'.$data_fido.'"');	
        if(!$somma_fido_32)
            $somma_fido_32 = 0;

        // Nella 1.4 mancano le parentesi dopo il - (controllare quale è giusto)
        // stato 32 = pagamento differito ok
        $crediti_fido = $fido - ($somma_fido + $somma_fido_32);

        $soglia = 100; // prova: correggere valore
        $colore = ($crediti_fido < 0 ? 'color: red' : ($crediti_fido < $soglia ? 'color: orange': ''));

        // CON SIMBOLO EURO E . PER I DECIMALI
        /* $this->context->currency = Currency::getDefaultCurrency();
        $residuo = Tools::displayPrice($crediti_fido, $this->context->currency); */

        $residuo = number_format($crediti_fido, 2, ',', '.');

        return '<span class="thick" style="'.$colore.'">'.$residuo.'</span>';
    }
}