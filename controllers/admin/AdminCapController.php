<?php

class AdminCapControllerCore extends AdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->lang = false;

        $this->context = Context::getContext();

        $this->display = 'view';

        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Aggiornamento CAP'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }

    public function renderView()
    {
        if(Tools::getIsset('cercacap')){

            $caps = Db::getInstance()->executeS('
                SELECT * 
                FROM cap 
                WHERE comune LIKE "%'.Tools::getValue('cap_ricerca').'%" 
                    OR cap LIKE "%'.Tools::getValue('cap_ricerca').'%"
            ');

        }

        // Fatto submit normale al posto di submit ajax (vedi AdminCAP 1.4)
        if(Tools::getIsset('salva_cap')){

            $cap_modificato = Tools::getValue('cap_id');

            Db::getInstance()->execute('
                UPDATE cap
                SET comune = "'.Tools::getValue('comune').'",
                    cap = "'.Tools::getValue('cap').'"
                WHERE id = '.$cap_modificato
            );
            
            $caps = Db::getInstance()->executeS('
                SELECT * 
                FROM cap 
                WHERE comune LIKE "%'.Tools::getValue('ricerca_precedente').'%" 
                    OR cap LIKE "%'.Tools::getValue('ricerca_precedente').'%"
            ');

        }

        $this->tpl_view_vars = array(
            'caps' => $caps,
            'cap_modificato' => $cap_modificato,
        );
		
        return parent::renderView();
    }
}