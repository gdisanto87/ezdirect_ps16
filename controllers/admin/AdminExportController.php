<?php 

class AdminExportControllerCore extends AdminController
{
    public function __construct()
    {
        $this->displayWarning('Correggere i file inclusi e fare comparaprezzi');

        $this->bootstrap = true;
        $this->lang = false;

        $this->context = Context::getContext();

        $this->display = 'view';

        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Strumenti di esportazione'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }

    public function renderView()
    {
        if(Tools::getValue('tab_name'))
			$tab_name = Tools::getValue('tab_name');
		//else
            //$tab_name = 'catalogo';

        // Esporta catalogo
        if(!Tools::getValue('tab_name') || $tab_name == 'catalogo'){

            $categorie = array(
                array(
                    'id' => 'tutto-il-catalogo',
                    'name' => 'Tutto il catalogo',
                    'tempo' => ''
                ),
                array(
                    'id' => 'audioconferenze',
                    'name' => 'Audioconferenze',
                    'tempo' => '(15 secondi ca.)'
                ),
                array(
                    'id' => 'videoconferenza',
                    'name' => 'Videoconferenza',
                    'tempo' => '(15 secondi ca.)'
                ),
                array(
                    'id' => 'cuffie',
                    'name' => 'Cuffie',
                    'tempo' => '(2 minuti ca.)'
                ),
                array(
                    'id' => 'fissi',
                    'name' => 'Telefoni Fissi',
                    'tempo' => '(1 minuto ca.)'
                ),
                array(
                    'id' => 'cordless',
                    'name' => 'Telefoni Cordless',
                    'tempo' => '(40 secondi ca.)'
                ),
                array(
                    'id' => 'audioguide',
                    'name' => 'Audioguide',
                    'tempo' => '(5 secondi ca.)'
                ),
                array(
                    'id' => 'ricetrasmittenti',
                    'name' => 'Ricetrasmittenti',
                    'tempo' => '(5 secondi ca.)'
                ),
                array(
                    'id' => 'registratori',
                    'name' => 'Registratori',
                    'tempo' => '(5 secondi ca.)'
                ),
                array(
                    'id' => 'gsm',
                    'name' => 'GSM/UMTS',
                    'tempo' => '(10 secondi ca.)'
                ),
                array(
                    'id' => 'gateway',
                    'name' => 'Gateway VoIP',
                    'tempo' => '(5 secondi ca.)'
                ),
                array(
                    'id' => 'centralini',
                    'name' => 'Centralini',
                    'tempo' => '(2 minuti e 30 secondi ca.)'
                ),
                array(
                    'id' => 'citofoni',
                    'name' => 'Citofoni',
                    'tempo' => '(5 secondi ca.)'
                ),
                array(
                    'id' => 'ipcamera',
                    'name' => 'IP Camera',
                    'tempo' => '(5 secondi ca.)'
                ),
                array(
                    'id' => 'antirumore',
                    'name' => 'Cuffie Antirumore',
                    'tempo' => '(5 secondi ca.)'
                ),
                array(
                    'id' => 'varie',
                    'name' => 'Varie',
                    'tempo' => '(30 secondi ca.)'
                ),
                array(
                    'id' => 'listino-interno',
                    'name' => 'Listino interno',
                    'tempo' => '(40 secondi ca.)'
                ),
            );

            $costruttori = Db::getInstance()->executeS('
                SELECT id_manufacturer, name 
                FROM '._DB_PREFIX_.'manufacturer 
                ORDER BY name
            ');

            $check_cat = false;

            if(Tools::getIsset('azione') && Tools::getValue('azione') == 'esporta_xls_categoria'){
                foreach($categorie as $cat){
                    if(Tools::getIsset('categoria_'.$cat['id']) && Tools::getValue('categoria_'.$cat['id']) == $cat['id']){
                        include('esportazione-catalogo/esportazione-'.$cat['id'].'.php');
                        $check_cat = true;
                        break;
                    }
                }
            }
            else if(Tools::getIsset('azione') && Tools::getValue('azione') == 'esporta_listino_categoria'){
                $check_cat = true;
                include("esportazione-catalogo/esportazione-rapida.php");
            }
            else if(Tools::getIsset('azione') && Tools::getValue('azione') == 'esporta_xls_costruttore'){
                include('esportazione-catalogo/esportazione-per-costruttore.php');
            }
            else if(Tools::getIsset('azione') && Tools::getValue('azione') == 'esporta_listino_costruttore'){
                include("esportazione-catalogo/esportazione-rapida-costruttori.php");
            }

            $catalogo = array(
                'categorie' => $categorie,
                'costruttori' => $costruttori,
                'check_cat' => $check_cat, // false => Questa categoria non è ancora pronta
            );
        }

        // Importa catalogo
        if($tab_name == 'import_catalogo'){

            if(Tools::getIsset('azione') && Tools::getValue('azione') == 'importa_file'){

                $check_file = false;
                
                if($_FILES['filexls']['size'] != 0)
                {
                    $tmp = $_FILES['filexls']['tmp_name']; // cartella temporanea 
                    $path = $_SERVER["DOCUMENT_ROOT"];
                    $dir = "$path"."/ezadmin/importazione-catalogo/catalogo_xls/"; // percorso della cartella
                    $filexls = $_FILES['filexls']['name'];
                    $dirfile = $dir.$filexls;
                    move_uploaded_file($tmp, $dirfile);

                    if(Tools::getIsset('aggiorna') && Tools::getValue('aggiorna') == 'solo_prezzi'){
                        include("importazione-catalogo/aggiorna_solo_prezzi.php"); // testare!
                    }
                    else {
                        // Tipologie di import
                        $import = array(
                            'audioconferenze',
                            'videoconferenza',
                            'cuffie',
                            'fissi',
                            'cordless',
                            'audioguide',
                            'ricetrasmittenti',
                            'registratori',
                            'gsm',
                            'gateway',
                            'centralini',
                            'citofoni',
                            'ipcamera',
                            'antirumore',
                            'varie',
                        );

                        /* // In PHP 8:
                        if(str_contains($str, $import)){ 
                        }
                        */

                        foreach($import as $substr){
                            if(strpos($filexls, $substr) !== false){
                                include('importazione-catalogo/import-'.$substr.'.php'); // correggere i file e testare!
                                $check_file = true;
                                break;
                            }
                        }
                    }
                }
            }

            $imp_catalogo = array(
                'check_file' => $check_file, // false => File non riconosciuto
            );
        }
        
        // Esportatore comparaprezzi
        if($tab_name == 'comparaprezzi'){

        }

        // Esportazione Mailup
        if($tab_name == 'mailup'){

            if(Tools::getIsset('azione')){
                /* CORREGGERE: manca datepicker nel tpl */
                if(!empty($_POST['arco_dal'])){
                    $data_dal_a = explode("-", $_POST['arco_dal']);
                    $data_dal = $data_dal_a[2]."-".$data_dal_a[1]."-".$data_dal_a[0]; 
                }
                else{
                    $data_dal = date('Y-m-d 00:00:00',strtotime("-30 days"));
                }

                if(!empty($_POST['arco_al'])){
                    $data_al_a = explode("-", $_POST['arco_al']);
                    $data_al = $data_al_a[2]."-".$data_al_a[1]."-".$data_al_a[0]; 
                }
                else{
                    $data_al = date('Y-m-d 00:00:00',strtotime("now"));
                }
                /** */
                
                // CORREGGERE: I VALUE DI AZIONE DEVONO ESSERE COME NELLA 1.4 O I FILE POSSONO ESSERE RINOMINATI?
                if(Tools::isSubmit('azione') && Tools::getValue('azione') == 'aziende_tutti'){
                    $query = "SELECT c.firstname, c.lastname, c.company, c.email, a.address1, a.city, a.postcode, a.phone FROM "._DB_PREFIX_."customer c JOIN "._DB_PREFIX_."address a ON c.id_customer = a.id_customer WHERE a.active = 1 AND a.deleted = 0 AND a.fatturazione = 1 AND c.is_company = 1 AND c.ex_rivenditore = 0 AND c.id_default_group != 15 AND c.id_default_group != 3 AND email NOT LIKE '%amazon%' AND c.date_add > '".$data_dal."' AND c.date_add < '".$data_al."'";
                } 
                else if(Tools::isSubmit('azione') && Tools::getValue('azione') == 'aziende_gdpr'){
                    $query = "SELECT c.firstname, c.lastname, c.company, c.email, a.address1, a.city, a.postcode, a.phone FROM "._DB_PREFIX_."customer c JOIN "._DB_PREFIX_."address a ON c.id_customer = a.id_customer WHERE a.active = 1 AND a.deleted = 0 AND a.fatturazione = 1 AND c.is_company = 1 AND c.newsletter = 1 AND c.ex_rivenditore = 0 AND (c.id_default_group != 15 AND c.id_default_group != 3) AND email NOT LIKE '%amazon%' AND c.date_add > '".$data_dal."' AND c.date_add < '".$data_al."'";
                }
                else if(Tools::isSubmit('azione') && Tools::getValue('azione') == 'privati_tutti'){
                    $query = "SELECT c.firstname, c.lastname, c.company, c.email, a.address1, a.city, a.postcode, a.phone FROM "._DB_PREFIX_."customer c JOIN "._DB_PREFIX_."address a ON c.id_customer = a.id_customer WHERE a.active = 1 AND a.deleted = 0 AND a.fatturazione = 1 AND c.is_company = 0 AND email NOT LIKE '%amazon%' AND c.date_add > '".$data_dal."' AND c.date_add < '".$data_al."'";
                }
                else if(Tools::isSubmit('azione') && Tools::getValue('azione') == 'privati_gdpr'){
                    $query = "SELECT c.firstname, c.lastname, c.company, c.email, a.address1, a.city, a.postcode, a.phone FROM "._DB_PREFIX_."customer c JOIN "._DB_PREFIX_."address a ON c.id_customer = a.id_customer WHERE a.active = 1 AND a.deleted = 0 AND a.fatturazione = 1 AND c.is_company = 0 AND c.newsletter = 1 AND email NOT LIKE '%amazon%' AND c.date_add > '".$data_dal."' AND c.date_add < '".$data_al."'";
                }
                else if(Tools::isSubmit('azione') && Tools::getValue('azione') == 'rivenditori_1_tutti'){
                    $query = "SELECT c.firstname, c.lastname, c.company, c.email, a.address1, a.city, a.postcode, a.phone FROM "._DB_PREFIX_."customer c JOIN "._DB_PREFIX_."address a ON c.id_customer = a.id_customer WHERE a.active = 1 AND a.deleted = 0 AND a.fatturazione = 1 AND c.is_company = 1 AND (c.id_default_group = 3) AND email NOT LIKE '%amazon%' AND c.date_add > '".$data_dal."' AND c.date_add < '".$data_al."'";
                }
                else if(Tools::isSubmit('azione') && Tools::getValue('azione') == 'rivenditori_1_gdpr'){
                    $query = "SELECT c.firstname, c.lastname, c.company, c.email, a.address1, a.city, a.postcode, a.phone FROM "._DB_PREFIX_."customer c JOIN "._DB_PREFIX_."address a ON c.id_customer = a.id_customer WHERE a.active = 1 AND a.deleted = 0 AND a.fatturazione = 1 AND c.is_company = 1 AND c.newsletter = 1 AND (c.id_default_group = 3) AND email NOT LIKE '%amazon%' AND c.date_add > '".$data_dal."' AND c.date_add < '".$data_al."'";
                }
                else if(Tools::isSubmit('azione') && Tools::getValue('azione') == 'richiesta_rivenditori_tutti'){
                    $query = "SELECT c.firstname, c.lastname, c.company, c.email, a.address1, a.city, a.postcode, a.phone FROM "._DB_PREFIX_."customer c JOIN "._DB_PREFIX_."address a ON c.id_customer = a.id_customer WHERE a.active = 1 AND a.deleted = 0 AND a.fatturazione = 1 AND c.is_company = 1 AND (c.id_default_group = 15) AND email NOT LIKE '%amazon%' AND c.date_add > '".$data_dal."' AND c.date_add < '".$data_al."'";
                }
                else if(Tools::isSubmit('azione') && Tools::getValue('azione') == 'richiesta_rivenditori_gdpr'){
                    $query = "SELECT c.firstname, c.lastname, c.company, c.email, a.address1, a.city, a.postcode, a.phone FROM "._DB_PREFIX_."customer c JOIN "._DB_PREFIX_."address a ON c.id_customer = a.id_customer WHERE a.active = 1 AND a.deleted = 0 AND a.fatturazione = 1 AND c.is_company = 1 AND c.newsletter = 1 AND (c.id_default_group = 15) AND email NOT LIKE '%amazon%' AND c.date_add > '".$data_dal."' AND c.date_add < '".$data_al."'";
                }
                else if(Tools::isSubmit('azione') && Tools::getValue('azione') == 'ex_rivenditori_tutti'){
                    $query = "SELECT c.firstname, c.lastname, c.company, c.email, a.address1, a.city, a.postcode, a.phone FROM "._DB_PREFIX_."customer c JOIN "._DB_PREFIX_."address a ON c.id_customer = a.id_customer WHERE a.active = 1 AND a.deleted = 0 AND a.fatturazione = 1 AND c.is_company = 1 AND c.ex_rivenditore = 1 AND email NOT LIKE '%amazon%' AND c.date_add > '".$data_dal."' AND c.date_add < '".$data_al."'";
                }
                else if(Tools::isSubmit('azione') && Tools::getValue('azione') == 'ex_rivenditori_gdpr'){
                    $query = "SELECT c.firstname, c.lastname, c.company, c.email, a.address1, a.city, a.postcode, a.phone FROM "._DB_PREFIX_."customer c JOIN "._DB_PREFIX_."address a ON c.id_customer = a.id_customer WHERE a.active = 1 AND a.deleted = 0 AND a.fatturazione = 1 AND c.is_company = 1 AND c.newsletter = 1 AND c.ex_rivenditore = 1 AND email NOT LIKE '%amazon%' AND c.date_add > '".$data_dal."' AND c.date_add < '".$data_al."'";
                }
                
                $res = Db::getInstance()->executeS($query);
                
                $file = '';
                foreach($res as $row){
                    $file .= $row['firstname'].';'.$row['lastname'].';'.$row['company'].';'.$row['email'].';'.str_replace(';',',',$row['address1']).';'.$row['city'].';'.$row['postcode'].';'.$row['phone']."\n";
                }
                
                $data = date('d-m-Y',strtotime("now"));
                
                // CORREGGERE: TEST SU SERVER DI PROVA 46.101.235.11
                $file_csv_mailup_stream = fopen("esportazione-catalogo/catalogo_xls/".str_replace(' ', '', Tools::getValue('azione'))."-$data-mailup.csv","w+");
                @fwrite($file_csv_mailup_stream, $file);
                @fclose("esportazione-catalogo/catalogo_xls/".str_replace(' ', '', Tools::getValue('azione'))."-$data-mailup.csv");
        
                header("Location: http://46.101.235.11/ezadmin/esportazione-catalogo/catalogo_xls/".str_replace(' ', '', Tools::getValue('azione'))."-$data-mailup.csv");
                
                // decommentare
                /*$file_csv_mailup_stream = fopen("/var/www/vhosts/ezdirect.it/httpdocs/ezadmin/esportazione-catalogo/catalogo_xls/".str_replace(' ', '', Tools::getValue('azione'))."-$data-mailup.csv","w+");
                @fwrite($file_csv_mailup_stream, $file);
                @fclose("/var/www/vhosts/ezdirect.it/httpdocs/ezadmin/esportazione-catalogo/catalogo_xls/".str_replace(' ', '', Tools::getValue('azione'))."-$data-mailup.csv");
        
                header("Location: http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/".str_replace(' ', '', Tools::getValue('azione'))."-$data-mailup.csv");
                */
            }

        }

        // Esporta listini
        if($tab_name == 'listini'){

            if(Tools::getIsset('azione') && Tools::getValue('azione') == 'esporta_xls') {
                include("esportazione-catalogo/esportazione-listini.php"); // CORREGGERE: Salva un file excel vuoto (vengono settati solo i titoli delle colonne)
            }
            else {
                // js: function message, eliminabile?
                
                // id_customer 44431 = carrelli tipo
                $carrelli = Db::getInstance()->executeS('
                    SELECT id_cart as id, name
                    FROM '._DB_PREFIX_.'cart 
                    WHERE id_customer = 44431 
                        AND name != "" 
                    ORDER BY name
                ');

                $listini = array(
                    'carrelli' => $carrelli,
                );
            }

        }

        $this->tpl_view_vars = array(
            'catalogo' => $catalogo,
            'imp_catalogo' => $imp_catalogo,
            'comparaprezzi' => $comparaprezzi,
            'mailup' => $mailup,
            'listini' => $listini,
            'tab_name' => $tab_name,
        );

        return parent::renderView();
    }
}
