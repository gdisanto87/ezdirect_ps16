<?php

class AdminOreFerieImpiegatiControllerCore extends AdminController
{
	public function __construct()
	{		
        $this->bootstrap = true;
        $this->lang = false;

        $this->context = Context::getContext();

        if(Tools::getValue('tab_name'))
			$tab_name = Tools::getValue('tab_name');

        $this->display = 'view';
        
        parent::__construct();
    }

	public function initToolbarTitle()
	{
        parent::initToolbarTitle();

        if(Tools::getValue('tab_name'))
			$tab_name = Tools::getValue('tab_name');

        array_pop($this->toolbar_title);
        if($tab_name == 'ore')
            $this->toolbar_title[] = sprintf($this->l('Ore impiegati'));
        else if($tab_name == 'ferie')
            $this->toolbar_title[] = sprintf($this->l('Ferie impiegati'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
	}
	
	public function renderView()
	{				
        $is_agente = ($this->context->employee->id_profile == 7 ? true : false); // true: l'employee loggato è un agente
        $check_employee = (($cookie->id_employee == 1 || $cookie->id_employee == 2 || $cookie->id_employee == 6 || $cookie->id_employee == 22) ? true : false);
        
		$tab_name = Tools::getValue('tab_name');
		
		$check_employee = true; // per ora metto true
		
        // Ore
		if($tab_name == 'ore' && !$is_agente && $check_employee){

            // nel tpl:
            // if(!$check_employee) die('Non hai accesso a questa sezione');
            
            // include lo script: <script src="handsontable.full.js"></script>

            /* lista mesi: selected se (Tools::getIsset('month-ore') && Tools::getValue('month-ore') == MESE SELEZIONATO) oppure se (!Tools::getIsset('month-ore') && date('n') == MESE SELEZIONATO) */
            /* lista anni dal 2014 al 2020: selected se (Tools::getIsset('year-ore') && Tools::getValue('year-ore') == ANNO SELEZIONATO) */
            /* anno 2021: selected come gli altri + selected se (!Tools::getIsset('year-ore')) */
            
            if(Tools::getIsset('year-ore'))
                $year = Tools::getValue('year-ore');
            else
                $year = date("Y");
                
            if(Tools::getIsset('month-ore'))
                $month = Tools::getValue('month-ore');
            else
                $month = date("n");
              		  
            // correggere per il tpl (vedi $mesi)
            switch($month)
            {
                case 1: $month_name = 'Gennaio'; break; 
                case 2: $month_name = 'Febbraio'; break; 
                case 3: $month_name = 'Marzo'; break; 
                case 4: $month_name = 'Aprile'; break; 
                case 5: $month_name = 'Maggio'; break; 
                case 6: $month_name = 'Giugno'; break; 
                case 7: $month_name = 'Luglio'; break; 
                case 8: $month_name = 'Agosto'; break; 
                case 9: $month_name = 'Settembre'; break; 
                case 10: $month_name = 'Ottobre'; break; 
                case 11: $month_name = 'Novembre'; break; 
                case 12: $month_name = 'Dicembre'; break; 
                default: ''; break; 
            }

            // var nuova, eliminare se non serve, altrimenti passare al tpl
            $mesi = array(
                1 => 'Gennaio',
                2 => 'Febbraio',
                3 => 'Marzo',
                4 => 'Aprile',
                5 => 'Maggio',
                6 => 'Giugno',
                7 => 'Luglio',
                8 => 'Agosto',
                9 => 'Settembre',
                10 => 'Ottobre',
                11 => 'Novembre',
                12 => 'Dicembre',
            );
            
            $days_number = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $days_sequence = '';
            $days_sequence_empty = ',';

            for($i = 1; $i<=$days_number; $i++){
                $days_sequence .= $i.",";
                $days_sequence_empty .= ",";
            }

            $days_sequence = substr($days_sequence, 0, -1);

            $employees = Db::getInstance()->ExecuteS('
                SELECT id_employee, firstname, lastname
                FROM `'._DB_PREFIX_.'employee`
                WHERE id_employee != 6 AND id_employee != 22 AND id_employee != 11 AND id_employee != 15  AND id_employee != 16 AND id_employee != 20 AND id_employee != 3 AND id_employee != 18  AND id_employee != 21 AND id_employee != 23 AND id_employee != 25
                ORDER BY (CASE id_employee WHEN 1 THEN 0 WHEN 2 THEN 1 WHEN 7 THEN 2 WHEN 4 THEN 3 WHEN 3 THEN 4 WHEN 5 THEN 6 WHEN 13 THEN 7 WHEN 12 THEN 8 WHEN 14 THEN 9 WHEN 17 THEN 10 WHEN 18 THEN 11 WHEN 19 THEN 12 WHEN 22 THEN 13 WHEN 24 THEN 14 END)
            ');
            
            $rowHeaders = "";
			$rowImpiegati = "";
			
			foreach ($employees as $employee) {
				if($employee['id_employee'] == 6 || (($month > 9 && $year >= 2018 || $year >= 2019) && ($employee['id_employee'] == 3 || $employee['id_employee'] == 18))) {
                    // niente
                }
				else {
                    $rowHeaders .= '"ID'.$employee['id_employee'].'",';
                    
                    $exists = Db::getInstance()->getValue('
                        SELECT count(valori_ore) 
                        FROM ore 
                        WHERE id_employee = '.$employee['id_employee'].' 
                            AND month = '.$month.' 
                            AND year = '.$year
                    );

                    if($exists > 0) {
                        $valori_ore = Db::getInstance()->getValue('
                            SELECT valori_ore 
                            FROM ore 
                            WHERE id_employee = '.$employee['id_employee'].' 
                                AND month = '.$month.' 
                                AND year = '.$year
                        );

                        $valori_ore = preg_replace('/\*/', '","', $valori_ore);
                        $valori_ore = '"'.$valori_ore.'"';

                        $rowImpiegati .= preg_replace('/"0"/', '""','["'.$employee['lastname'].' '.$employee['firstname'].'",'.$valori_ore.',],');
                    }
                    else {
                        $rowImpiegati .= '["'.$employee['lastname'].' '.$employee['firstname'].'"'.$days_sequence_empty.'],';
                    }
				}
            }
            
			$rowHeaders = substr($rowHeaders, 0, -1);
            $rowImpiegati = substr($rowImpiegati, 0, -1);
			
            $k = 0;
            $data_to_save = "";
            $data_to_save_all = "var data_to_save_all = ";
            
            foreach($employees as $employee)
            {
                if($employee['id_employee'] == 6 || ($month > 9 && $year >= 2018 && ($employee['id_employee'] == 3 || $employee['id_employee'] == 18))) {
                    // niente	
                }
                else {
                    $data_to_save .= 'var data_to_save_'.$employee['id_employee'].' = "'.$employee['id_employee'].'|'.$month.'|'.$year.'|" + ';
                   
                    for($i = 1; $i<=$days_number; $i++)
                        $data_to_save .= 'data['.$k.']['.$i.']+"*"+';
                
                    $data_to_save = substr($data_to_save, 0, -5);
                    $data_to_save .= "+\":::\";\n";
                    $data_to_save_all .= 'data_to_save_'.$employee['id_employee'].' + ';
                    $k++;
                }
            }

            $data_to_save_all .= '"";';
            
            // tutte le variabili precedenti finiscono in una funzione js / ajax
            // tutte le celle della tabella sono modificabili
            // nel tpl: pulsanti salva e esporta in excel

			$ore = array( // inserire qui variabili per tpl
			);
        }
        
        // Ferie
		if($tab_name == 'ferie' && !$is_agente){

            /* ANNO 2019 */

            $year = 2019;
            
            $employees = Db::getInstance()->ExecuteS('
                SELECT id_employee, firstname, lastname
                FROM `'._DB_PREFIX_.'employee`
                WHERE id_employee != 6 AND id_employee != 22 AND id_employee != 11 AND id_employee != 1 AND id_employee != 16 AND id_employee != 18 AND id_employee != 20 AND id_employee != 3 AND id_employee != 21 AND id_employee != 23 AND id_employee != 25
                ORDER BY (CASE id_employee WHEN 1 THEN 0 WHEN 2 THEN 1 WHEN 7 THEN 2 WHEN 4 THEN 3 WHEN 3 THEN 4 WHEN 5 THEN 6 WHEN 13 THEN 7 WHEN 12 THEN 8 WHEN 14 THEN 9 WHEN 17 THEN 10 WHEN 18 THEN 11 WHEN 19 THEN 12 WHEN 22 THEN 13 WHEN 24 THEN 14 END)
            ');

            $ore_totali = array();
            $ferie_godute = array();
            $ferie_pianificate = array();
            $ferie_residue = array();
            $permessi_goduti = array();
            $permessi_pianificati = array();
            $permessi_residui = array();
            $certificati = array();
            
            // Situazione al 30 aprile 2019
            $permessi_residui[2] = 176;
            $permessi_residui[7] = 185;
            $permessi_residui[4] = 154;
            $permessi_residui[5] = 101;
            $permessi_residui[13] = 44;
            $permessi_residui[12] = 90;
            $permessi_residui[14] = 43;
            $permessi_residui[17] = 28;
            $permessi_residui[19] = 24;
            
            $ferie_residue[2] = 34;
            $ferie_residue[7] = 22;
            $ferie_residue[4] = 30;
            $ferie_residue[5] = 31;
            $ferie_residue[13] = 22;
            $ferie_residue[12] = 27;
            $ferie_residue[14] = 23;
            $ferie_residue[17] = 23;
            $ferie_residue[19] = 19;

            $limit[2] = 8; 
            $limit[7] = 8; 
            $limit[4] = 8; 
            $limit[3] = 8; 
            $limit[5] = 8; 
            $limit[13] = 5; 
            $limit[12] = 8;  
            $limit[14] = 8; 
            $limit[17] = 8; 
            $limit[19] = 8;
			
            foreach ($employees as $employee)
            {
				
                if($year < 2019 || ($year == 2019 && $month < 4))
                    $limit[19] = 6;
                else
                    $limit[19] = 8;
            
                if($employee['id_employee'] == 6 || ($month > 9 && $year >= 2018 && ($employee['id_employee'] == 3 || $employee['id_employee'] == 18))) {
                    // niente
                }
                else 
                {
					
                    $months = Db::getInstance()->executeS('
                        SELECT month, valori_ore 
                        FROM ore 
                        WHERE id_employee = '.$employee['id_employee'].' 
                            AND month > 4 
                            AND year = '.$year
                    );
                    foreach($months as $month) {
                        $valori_ore = $month['valori_ore'];
                        $valori_ore_array = explode("*", $valori_ore);

                        $iv = 1;
                        
                        foreach($valori_ore_array as $v) {
                            if(is_numeric($v)) {
                                $ore_totali[$employee['id_employee']] += $v;
                                
                                if($v < $limit[$employee['id_employee']] && $v != 0) {
                                    $permesso = $limit[$employee['id_employee']]-($v);
                                    
                                    if(strtotime($year.'-'. sprintf("%02d", $month['month']).'-'. sprintf("%02d", $iv).' 23:59:59') < strtotime(date('Y-m-d H:i:s')))
                                        $permessi_goduti[$employee['id_employee']] += $permesso;
                                    else
                                        $permessi_pianificati[$employee['id_employee']] += $permesso;
                                    
                                    $permessi_residui[$employee['id_employee']] -= $permesso;
                                }
                            }
                            else {
                                if($v == 'F'){
                                    if(strtotime($year.'-'. sprintf("%02d", $month['month']).'-'. sprintf("%02d", $iv).' 23:59:59') < strtotime(date('Y-m-d H:i:s')))
                                        $ferie_godute[$employee['id_employee']] += 1;
                                    else
                                        $ferie_pianificate[$employee['id_employee']] += 1;
                                    
                                    $ferie_residue[$employee['id_employee']] -= 1;
                                }
                                else if($v == 'C'){
                                    $certificati[$employee['id_employee']] += 1;
                                }
                                else {
                                    // niente
                                }
                            }
                            $iv++;
                        }

                        if(is_numeric($ferie_godute[$employee['id_employee']]) && floor($ferie_godute[$employee['id_employee']]) != $ferie_godute[$employee['id_employee']])
                        {
                            $ferie_godute[$employee['id_employee']] -= 0.5;
                            $ferie_residue[$employee['id_employee']] += 0.5;
                            //$permessi_goduti[$employee['id_employee']] += 4;
                            //$permessi_residui[$employee['id_employee']] -= 4;
                        }
                        
                        if(is_numeric($ferie_pianificate[$employee['id_employee']]) && floor($ferie_pianificate[$employee['id_employee']]) != $ferie_pianificate[$employee['id_employee']])
                        {
                            $ferie_pianificate[$employee['id_employee']] -= 0.5;
                            $ferie_residue[$employee['id_employee']] += 0.5;
                            //$permessi_pianificati[$employee['id_employee']] += 4;
                            //$permessi_residui[$employee['id_employee']] -= 4;
                        }
                    }

                    if($ferie_godute[$employee['id_employee']] == '') 
                        $ferie_godute[$employee['id_employee']] = 0;
                    if($ferie_pianificate[$employee['id_employee']] == '') 
                        $ferie_pianificate[$employee['id_employee']] = 0;
                    if($ferie_residue[$employee['id_employee']] == '') 
                        $ferie_residue[$employee['id_employee']] = 0;
                        
                    if($permessi_goduti[$employee['id_employee']] == '') 
                        $permessi_goduti[$employee['id_employee']] = 0;
                    if($permessi_pianificati[$employee['id_employee']] == '') 
                        $permessi_pianificati[$employee['id_employee']] = 0;
                    if($permessi_residui[$employee['id_employee']] == '') 
                        $permessi_residui[$employee['id_employee']] = 0;
                    
                    $nome = $employee['lastname'].' '.$employee['firstname'];
                    $ore_totali = $ore_totali[$employee['id_employee']];
                    $ferie_godute = $ferie_godute[$employee['id_employee']];
                    $ferie_pianificate = $ferie_pianificate[$employee['id_employee']];
                    $ferie_residue = $ferie_residue[$employee['id_employee']];
                    $permessi_goduti = $permessi_goduti[$employee['id_employee']];
                    $permessi_pianificati = $permessi_pianificati[$employee['id_employee']];
                    $permessi_residui = $permessi_residui[$employee['id_employee']];
                    $certificati = $certificati[$employee['id_employee']];
                    
                    $impiegato = array(
                        'nome' => $nome,
                        'ore_totali' => $ore_totali,
                        'ferie_godute' => $ferie_godute,
                        'ferie_pianificate' => $ferie_pianificate,
                        'ferie_residue' => $ferie_residue,
                        'permessi_goduti' => $permessi_goduti,
                        'permessi_pianificati' => $permessi_pianificati,
                        'permessi_residui' => $permessi_residui,
                        'certificati' => $certificati,
                    );

                    $impiegati[] = $impiegato;
                }
            }

            $ferie = array(
                'impiegati' => $impiegati,
            );
        }

        $this->tpl_view_vars = array(
            'ore' => $ore,
            'ferie' => $ferie,
			'is_agente' => $is_agente,
			'check_employee' => $check_employee,
			'impiegati' => $impiegati,
			'tab_name' => $tab_name,
			'month' => $month,
			'month_name' => $month_name,
			'year' => $year,
			'rowImpiegati' => $rowImpiegati,
			'rowHeaders' => $rowHeaders,
			'days_sequence' => $days_sequence,
			'data_to_save' => $data_to_save,
			'data_to_save_all' => $data_to_save_all
            //'show_toolbar' => true,
			
        );

        return parent::renderView();
    }
}