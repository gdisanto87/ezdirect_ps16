<?php

class AdminCorrezioniControllerCore extends AdminController
{
    public function __construct()
    {
		$this->displayInformation('Manca tabella bundle');

        $this->bootstrap = true;
        $this->lang = false;

        $this->context = Context::getContext();

		$this->display = 'view';

        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Correzioni Ortografiche'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }

    public function renderView()
    {
		// Update db e creazione file log
        if(Tools::getIsset('correggi'))
        {
            $da_correggere = Tools::getValue('da_correggere');
            $correzione = Tools::getValue('correzione');
            
			$log_file = "Correzioni effettuate\n".$correzione." al posto di ".$da_correggere.". Correzioni effettuate in data ".date('d/m/Y')." alle ore ".date('H:i:s.')."\nDettaglio correzioni eseguite:\n\n\nNumero occorrenze;Tipo dato;Prod./Cat./art.;Codice prodotto\n";
			
			$products = Db::getInstance()->executeS('
                SELECT * 
                FROM '._DB_PREFIX_.'product_lang 
                WHERE id_lang = '.(int)$this->context->language->id
            );

            $matches = array();
        
			foreach($products as $product){
                
				$cod_prodotto = Db::getInstance()->getValue('
                    SELECT reference 
                    FROM '._DB_PREFIX_.'product 
                    WHERE id_product = '.$product['id_product']
                );
                
                if(preg_match('/ '.$da_correggere.' /', $product['description_short'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $product["description_short"],$matches).";descrizione breve;".$product["name"].";".$cod_prodotto."\n";
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product_lang SET description_short = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $product['description_short']))."' WHERE id_product = ".$product['id_product']);
				}
				
				if(preg_match('/ '.$da_correggere.' /', $product['description'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $product["description"],$matches)." ;descrizione;".$product["name"].";".$cod_prodotto."\n";
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product_lang SET description = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $product['description']))."' WHERE id_product = ".$product['id_product']);
				}
				
				if(preg_match('/ '.$da_correggere.' /', $product['meta_description'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $product["meta_description"],$matches)." ;meta description;".$product["name"].";".$cod_prodotto."\n";
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product_lang SET meta_description = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $product['meta_description']))."' WHERE id_product = ".$product['id_product']);
				}
				
				if(preg_match('/ '.$da_correggere.' /', $product['meta_title'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $product["meta_title"],$matches)." ;title tag;".$product["name"].";".$cod_prodotto."\n";
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product_lang SET meta_title = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $product['meta_title']))."' WHERE id_product = ".$product['id_product']);
				}
				
				if(preg_match('/ '.$da_correggere.' /', $product['title'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $product["title"],$matches)." ;titolo;".$product["name"].";".$cod_prodotto."\n";
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product_lang SET title = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $product['title']))."' WHERE id_product = ".$product['id_product']);
				}
				
				if(preg_match('/ '.$da_correggere.' /', $product['name'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $product["name"],$matches)." ;nome;".$product["name"].";".$cod_prodotto."\n";
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product_lang SET name = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $product['name']))."' WHERE id_product = ".$product['id_product']);
				}
				
				if(preg_match('/ '.$da_correggere.' /', $product['meta_keywords'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $product["meta_keywords"],$matches).";keywords;".$product["name"].";".$cod_prodotto."\n";
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product_lang SET meta_keywords = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $product['meta_keywords']))."' WHERE id_product = ".$product['id_product']);
				}
			}
			
			$log_file .= "\n";

            $categories = Db::getInstance()->executeS('
                SELECT * 
                FROM '._DB_PREFIX_.'category_lang 
                WHERE id_lang = '.(int)$this->context->language->id
            );

			foreach($categories as $category) {
				if(preg_match('/ '.$da_correggere.' /', $category['meta_description'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $category["meta_description"],$matches)." ;meta description;".$category["name"]."\n";
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."category_lang SET meta_description = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $category['meta_description']))."' WHERE id_category = ".$category['id_category']);
				}
				
				if(preg_match('/ '.$da_correggere.' /', $category['description'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $category["description"],$matches)." ;descrizione;".$category["name"]."\n";
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."category_lang SET description = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $category['description']))."' WHERE id_category = ".$category['id_category']);
				}
				
				if(preg_match('/ '.$da_correggere.' /', $category['meta_title'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $category["meta_title"],$matches)." ;title tag;".$category["name"]."\n";
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."category_lang SET meta_title = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $category['meta_title']))."' WHERE id_category = ".$category['id_category']);
				}
				
				if(preg_match('/ '.$da_correggere.' /', $category['title'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $category["title"],$matches)." ;titolo;".$category["name"]."\n";
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."category_lang SET title = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $category['title']))."' WHERE id_category = ".$category['id_category']);
				}
				
				if(preg_match('/ '.$da_correggere.' /', $category['name'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $category["name"],$matches)." ;nome;".$category["name"]."\n";
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."category_lang SET name = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $category['name']))."' WHERE id_category = ".$category['id_category']);
				}
				
				if(preg_match('/ '.$da_correggere.' /', $category['meta_keywords'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $category["meta_keywords"],$matches)." ;keywords;".$category["name"]."\n";
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."category_lang SET meta_keywords = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $category['meta_keywords']))."' WHERE id_category = ".$category['id_category']);
				}
			}
			
			$log_file .= "\n";
			
            $cmss = Db::getInstance()->executeS('
                SELECT * 
                FROM '._DB_PREFIX_.'cms_lang 
                WHERE id_lang = '.(int)$this->context->language->id
            );

			foreach($cmss as $cms) {
				if(preg_match('/ '.$da_correggere.' /', $cms['meta_description'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $cms["meta_description"],$matches)." ; meta description;".$cms["meta_title"]."\n";
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cms_lang SET meta_description = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $cms['meta_description']))."' WHERE id_cms = ".$cms['id_cms']);
				}
				
				if(preg_match('/ '.$da_correggere.' /', $cms['content'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $cms["content"],$matches)." ;contenuto;".$cms["meta_title"]."\n";
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cms_lang SET content = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $cms['content']))."' WHERE id_cms = ".$cms['id_cms']);
				}
				
				if(preg_match('/ '.$da_correggere.' /', $cms['meta_title'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $cms["meta_title"],$matches)." ; title tag;".$cms["meta_title"]."\n";
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cms_lang SET meta_title = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $cms['meta_title']))."' WHERE id_cms = ".$cms['id_cms']);
				}
				
				if(preg_match('/ '.$da_correggere.' /', $cms['meta_keywords'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $cms["meta_keywords"],$matches)." ; title tag;".$cms["meta_title"]."\n";
					Db::getInstance()->execute("UPDATE "._DB_PREFIX_."cms_lang SET meta_keywords = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $cms['meta_keywords']))."' WHERE id_cms = ".$cms['id_cms']);
				}
			}
			
			$log_file .= "\n";
            
            // Correggere: aggiungere tabella bundle (scaricare anzi modulo per bundle?)
			/*$bundles = Db::getInstance()->executeS('
                SELECT * FROM bundle
            ');

			foreach($bundles as $bundle) {
				if(preg_match('/ '.$da_correggere.' /', $bundle['bundle_name'])) {
					$log_file.= preg_match_all("/ ".$da_correggere." /", $bundle["bundle_name"],$matches).";nome;".$bundle["bundle_name"].";".$bundle["bundle_ref"]."\n";
					Db::getInstance()->execute("UPDATE bundle SET bundle_name = '".addslashes(preg_replace('/ '.$da_correggere.' /', ' '.$correzione.' ', $bundle['bundle_name']))."' WHERE id_bundle = ".$bundle['id_bundle']);
				}
			}*/

			$log_file = htmlentities($log_file, ENT_QUOTES);
		}
		// Download file log
		else if(Tools::getIsset('confermalog'))
        {
			ob_end_clean();
			header('Content-type: text/csv');
			header("Content-Type: application/force-download");
			header('Content-Disposition: attachment; filename="correzioni.csv"');
			header("Connection: close");
			print Tools::getValue('log_file');
			exit();
		}

        $this->tpl_view_vars = array(
            'log_file' => $log_file,
        );

        return parent::renderView();
    }
}
