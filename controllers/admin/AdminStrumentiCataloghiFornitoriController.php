<?php

// Potrei togliere Core e trasformare in override
class AdminStrumentiCataloghiFornitoriControllerCore extends AdminController
{
    public function __construct()
    {
        $this->displayWarning('WARNING: non finire, verrà eliminato!');
        
        $this->bootstrap = true;
        $this->lang = false;

        $this->context = Context::getContext();
        $this->context->currency = Currency::getDefaultCurrency();

        $this->display = 'view';

        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Strumenti cataloghi fornitori'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }
    
    public function renderView()
    {
        $id_allnet = 11;
        $id_intracom = 95;

        if(Tools::getIsset('azione') && Tools::getValue('azione') == 'pronti_da_caricare_allnet')
        {
            // redirect a "https://ezdirectftp:nhfy57HGr32@www.ezdirect.it/docs/disponibilita/pronti-da-caricare.xls"
        }
        else if(Tools::getIsset('azione') && Tools::getValue('azione') == 'usciti_di_produzione_allnet')
        {
            if(isset($_POST['del']))
            {
                foreach($_POST['delete_sp'] as $del) 
                {
                    if(isset($del))
                    {
                        $prod = new Product($del);
                        $prod->delete();
                    }	
                }
                // tpl o conf
                //echo "<div class='conf'>Prodotti cancellati con successo</div>";
            }
            else if(isset($_POST['old']))
            {
                foreach($_POST['delete_sp'] as $del) 
                {
                    if(isset($del))
                    {
                        Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'product SET active = 2 WHERE id_product = '.$del);
                    }	
                }
                // tpl o conf
                //echo "<div class='conf'>Prodotti messi in old con successo</div>";
            }
            else if(isset($_POST['off']))
            {
                foreach($_POST['delete_sp'] as $del) 
                {
                    if(isset($del))
                    {
                        Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'product SET active = 0 WHERE id_product = '.$del);
                    }	
                }
                // tpl o conf
                //echo "<div class='conf'>Prodotti messi offline con successo</div>";
            }
            
            // tpl:
            /*
            echo '<h1>Catalogo Allnet - Usciti di produzione</h1>';
            echo 'Da questa pagina puoi vedere quali prodotti inseriti su Ezdirect sono usciti di produzione dal catalogo Allnet. Criteri: vengono mostrati prodotti offline e online (NO old), che non hanno il flag "fuori produzione" e che non compaiono nel file di Allnet.<br /><br />';
            echo '<strong>ULTIMO AGGIORNAMENTO: '.date('d/m/Y H:i:s', filemtime("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/allnet/ezdirectNEW.csv")).'</strong><br /><br />';
            */
            ini_set('display_errors', 'off');
            ini_set("memory_limit","892M");
            set_time_limit(36000);

            function in_array_r($needle, $haystack, $strict = false) {
                foreach ($haystack as $item) {
                    if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
                        return true;
                    }
                }
                return false;
            }

            $id = 1;
            $dispzero = 0;
            $array = array();
            $array_vecchi = array();
            $i = 0;

            // ripetuto 2 volte?
            ini_set("memory_limit","892M");
            set_time_limit(3600);
            
            // + js tablesorter
            
            // tabella tpl
            /*
            echo '<form method="post" action="'.$currentIndex.'&token='.$this->token.'&azione=usciti_di_produzione_allnet">
            <table class="table" id="usciti_allnet"><thead><tr><th>Codice Ezdirect</th><th>Codice SKU</th><th>Descrizione</th><th>Costruttore</th><th>Listino</th><th>Acquisto</th><th>Vendita</th><th>Att.?</th><th>Data usc.</th><th></th></tr></thead><tbody>';
            */

            $id = 2;

            $array_vecchi = Db::getInstance()->executeS('
                SELECT SQL_CACHE p.id_product, p.active, p.reference, p.supplier_reference, p.listino, p.price, p.wholesale_price, pl.name as nome, m.name as costruttore, p.date_add, p.data_uscita_produzione 
                FROM '._DB_PREFIX_.'product p 
                    JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product = pl.id_product 
                    JOIN '._DB_PREFIX_.'manufacturer m ON p.id_manufacturer = m.id_manufacturer 
                WHERE pl.id_lang = '.(int)$this->context->language->id.' 
                    AND p.fuori_produzione = 0 
                    AND p.id_supplier = '.$id_allnet.' 
                    AND p.data_uscita_produzione > "0000-00-00"
            ');
            
            foreach($array_vecchi as $dati)
            {                    
                //$tokenProducts = Tools::getAdminToken('AdminCatalogExFeatures'.(int)Tab::getIdFromClassName('AdminCatalogExFeatures').(int)$cookie->id_employee);
                
                // righe tabella:
                //<tr><td><span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($dati["id_product"]).'"><a href="index.php?tab=AdminCatalogExFeatures&id_product='.$dati["id_product"].'&updateproduct&token='.$tokenProducts.'" target="_blank">'.$dati['reference'].'</td><td>'.$dati['supplier_reference'].'</td><td>'.$dati['nome'].'</td><td>'.$dati['costruttore'].'</td><td style="text-align:right">'.number_format($dati['listino'],2,",","").'</td><td style="text-align:right">'.number_format($dati['wholesale_price'],2,",","").'</td><td style="text-align:right">'.number_format($dati['price'],2,",","").'</td><td>'.($dati['active']).'</td><td>'.date('d/m/Y',strtotime($dati['data_uscita_produzione'])).'</td><td><input type="checkbox" name="delete_sp[]" value="'.$dati['id_product'].'" /> </td></tr>';
                
                $id++;
            }
            
            /* pulsanti nel tpl:
            <input class="button" type="submit" name="off" value="Metti offline i prodotti selezionati" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" />
            <input class="button" type="submit" name="old" value="Metti in old i prodotti selezionati" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" />
            <input class="button" type="submit" name="del" value="Cancella i prodotti selezionati" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" />
            */
        }
        else if(Tools::getIsset('azione') && Tools::getValue('azione') == 'nuovi_prodotti_allnet')
        {
            // tpl:
            /*
            echo '<h1>Catalogo Allnet - Ultimi 100 prodotti inseriti</h1>';
					
            echo 'Da questa pagina puoi vedere quali sono gli ultimi 100 prodotti inseriti dal catalogo Allnet, con relativa data di inserimento.<br /><br />';
            
            echo '<strong>ULTIMO AGGIORNAMENTO: '.date('d/m/Y H:i:s', filemtime("/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/allnet/ezdirectNEW.csv")).'</strong><br /><br />
            
            <a class="button" style="display:block; text-align:center; width:450px" href="https://ezdirectftp:nhfy57HGr32@www.ezdirect.it/docs/disponibilita/pronti-da-caricare.xls"><strong>ALLNET: scarica file con prodotti pronti da caricare</strong></a><br /><br />';
            
            echo '<table class="table" id="usciti_allnet"><thead><tr><th>Codice Ezdirect</th><th>Codice SKU</th><th>Descrizione</th><th>Costruttore</th><th>Listino</th><th>Acquisto</th><th>Vendita</th><th>Att.?</th><th>Data ins.</th></tr></thead><tbody>';
            */
            $array_nuovi = Db::getInstance()->executeS('
                SELECT p.id_product, p.active, p.reference, p.supplier_reference, p.listino, p.price, p.wholesale_price, pl.name as nome, m.name as costruttore, p.date_add 
                FROM '._DB_PREFIX_.'product p 
                    JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product = pl.id_product 
                    JOIN '._DB_PREFIX_.'manufacturer m ON p.id_manufacturer = m.id_manufacturer 
                WHERE p.active < 1 
                    AND p.id_supplier = '.$id_allnet.' 
                GROUP BY p.id_product 
                ORDER BY p.date_add DESC 
                LIMIT 100
            ');
            
            foreach($array_nuovi as $dati)
            {
                //$tokenProducts = Tools::getAdminToken('AdminCatalogExFeatures'.(int)Tab::getIdFromClassName('AdminCatalogExFeatures').(int)$cookie->id_employee);
                
                Db::getInstance()->execute('
                    UPDATE '._DB_PREFIX_.'product 
                    SET supplier_quantity = 0, arrivo_quantity = 0 
                    WHERE id_product = '.$id_product
                );
                
                // righe tabella:
                //echo '<tr><td><span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($dati["id_product"]).'"><a href="index.php?tab=AdminCatalogExFeatures&id_product='.$dati["id_product"].'&updateproduct&token='.$tokenProducts.'" target="_blank">'.$dati['reference'].'</td><td>'.$dati['supplier_reference'].'</td><td>'.$dati['nome'].'</td><td>'.$dati['costruttore'].'</td><td style="text-align:right">'.number_format($dati['listino'],2,",","").'</td><td style="text-align:right">'.number_format($dati['wholesale_price'],2,",","").'</td><td style="text-align:right">'.number_format($dati['price'],2,",","").'</td><td>'.($dati['active']).'</td><td>'.date('d/m/Y',strtotime($dati['date_add'])).'</td></tr>'; 
            }
        }
        else if(Tools::getIsset('azione') && Tools::getValue('azione') == 'usciti_di_produzione_intracom')
        {
            // COPIARE usciti_di_produzione_allnet (prima correggerlo)

            // differenze:
            // sostituire la stringa "Allnet" con "Intracom" 
            // url file ultimo aggiornamento: /var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/ezdirect-intracom.csv

            if(isset($_POST['del']))
            {
                foreach($_POST['delete_sp'] as $del) 
                {
                    if(isset($del))
                    {
                        $prod = new Product($del);
                        $prod->delete();
                    }	
                }
                // tpl o conf
                //echo "<div class='conf'>Prodotti cancellati con successo</div>";
            }
            else if(isset($_POST['old']))
            {
                foreach($_POST['delete_sp'] as $del) 
                {
                    if(isset($del))
                    {
                        Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'product SET active = 2 WHERE id_product = '.$del);
                    }	
                }
                // tpl o conf
                //echo "<div class='conf'>Prodotti messi in old con successo</div>";
            }
            else if(isset($_POST['off']))
            {
                foreach($_POST['delete_sp'] as $del) 
                {
                    if(isset($del))
                    {
                        Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'product SET active = 0 WHERE id_product = '.$del);
                    }	
                }
                // tpl o conf
                //echo "<div class='conf'>Prodotti messi offline con successo</div>";
            }
            
            // cambiato id_supplier:
			$array_vecchi = Db::getInstance()->executeS('
                SELECT SQL_CACHE p.id_product, p.active, p.reference, p.supplier_reference, p.listino, p.price, p.wholesale_price, pl.name as nome, m.name as costruttore, p.date_add, p.data_uscita_produzione 
                FROM '._DB_PREFIX_.'product p 
                    JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product = pl.id_product 
                    JOIN '._DB_PREFIX_.'manufacturer m ON p.id_manufacturer = m.id_manufacturer 
                WHERE pl.id_lang = '.(int)$this->context->language->id.'
                    AND p.fuori_produzione = 0 
                    AND p.id_supplier = '.$id_intracom.'
                    AND p.data_uscita_produzione > "0000-00-00"
            ');
        }
        else if(Tools::getIsset('azione') && Tools::getValue('azione') == 'nuovi_prodotti_intracom')
        {
            // come nuovi prodotti allnet tranne:
            // ULTIMO AGGIORNAMENTO ha file "/var/www/vhosts/ezdirect.it/httpdocs/docs/disponibilita/ezdirect-intracom.csv"
            // <th>Codice Ezdirect</th><th>Codice SKU</th><th>Descrizione</th><th>Costruttore</th><th>Listino</th><th>Acquisto</th><th>Vendita</th><th>Att.?</th><th>Data ins.</th>

            $array_nuovi = Db::getInstance()->executeS('
                SELECT p.id_product, p.active, p.reference, p.supplier_reference, p.listino, p.price, p.wholesale_price, pl.name as nome, m.name as costruttore, p.date_add 
                FROM '._DB_PREFIX_.'product p 
                    JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product = pl.id_product 
                    JOIN '._DB_PREFIX_.'manufacturer m ON p.id_manufacturer = m.id_manufacturer 
                WHERE p.active < 2 
                    AND p.id_supplier = '.$id_intracom.'
                GROUP BY p.id_product 
                ORDER BY p.date_add DESC 
                LIMIT 100
            ');
            
            foreach($array_nuovi as $dati)
            {
                //$tokenProducts = Tools::getAdminToken('AdminCatalogExFeatures'.(int)Tab::getIdFromClassName('AdminCatalogExFeatures').(int)$cookie->id_employee);
                
                Db::getInstance()->execute('
                    UPDATE '._DB_PREFIX_.'product 
                    SET supplier_quantity = 0, arrivo_quantity = 0 
                    WHERE id_product = '.$id_product
                );
                
                // <td><span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($dati["id_product"]).'"><a href="index.php?tab=AdminCatalogExFeatures&id_product='.$dati["id_product"].'&updateproduct&token='.$tokenProducts.'" target="_blank">'.$dati['reference'].'</td><td>'.$dati['supplier_reference'].'</td><td>'.$dati['nome'].'</td><td>'.$dati['costruttore'].'</td><td style="text-align:right">'.number_format($dati['listino'],2,",","").'</td><td style="text-align:right">'.number_format($dati['wholesale_price'],2,",","").'</td><td style="text-align:right">'.number_format($dati['price'],2,",","").'</td><td>'.($dati['active']).'</td><td>'.date('d/m/Y',strtotime($dati['date_add'])).'</td>    
            }
        }

        $this->tpl_view_vars = array(
            'vecchi' => $array_vecchi,
            'nuovi' => $array_nuovi,
        );

        return parent::renderView();
    }
}