<?php

class AdminDuplicatiControllerCore extends AdminController
{
	public function __construct()
	{		
        $this->bootstrap = true;
        $this->lang = false;

        $this->context = Context::getContext();

        $this->display = 'view';
        
        parent::__construct();
    }

	public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Duplicati'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
	}
	
	public function renderView()
	{
		if(Tools::getValue('tab_name'))
			$tab_name = Tools::getValue('tab_name');
		//else
			//$tab_name = 'piva_dup';
				
        $is_agente = ($this->context->employee->id_profile == 7 ? true : false); // true: l'employee loggato è un agente

		$tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)Tab::getIdFromClassName('AdminCustomers').(int)$this->context->employee->id);

		// Partite IVA doppie
		if((!Tools::getValue('tab_name') || $tab_name == 'piva_dup') && !$is_agente){

			$doppi = Db::getInstance()->executeS("
				SELECT vat_number, COUNT(vat_number) AS c
				FROM "._DB_PREFIX_."customer 
				WHERE is_company = 1 
					AND vat_number != '' 
					AND vat_number != '00000000000' 
					AND vat_number != '1' 
					AND vat_number != '-' 
				GROUP BY vat_number 
				HAVING c > 1
			");

			foreach($doppi as $doppio) 
			{
				$clienti_doppi = Db::getInstance()->executeS("
					SELECT DISTINCT c.id_customer, c.firstname, c.email, c.lastname, c.company, c.vat_number
					FROM "._DB_PREFIX_."customer c 
					WHERE c.vat_number = '".$doppio['vat_number']."' 
					ORDER BY c.id_customer DESC
				");

				foreach($clienti_doppi as &$c){
					$href = '/ezadmin/index.php?controller=AdminCustomers&id_customer='.$c['id_customer'].'&viewcustomer&token='.$tokenCustomers;
					$c['href'] = $href;
				}

				$vat_number = $doppio['vat_number'];

				$piva_doppie[] = array(
					'vat_number' => $vat_number,
					'clienti_doppi' => $clienti_doppi,
				);
				
			}

			$partite_iva = array(
				'duplicati' => $piva_doppie,
			);
		}

		// Codici fiscali doppi
		if($tab_name == 'cf_dup' && !$is_agente){

			$doppi_cf = Db::getInstance()->executeS("
				SELECT tax_code, COUNT(tax_code) c 
				FROM "._DB_PREFIX_."customer 
				WHERE is_company = 0 
					AND tax_code != '' 
					AND tax_code != '0' 
					AND tax_code != '0000000000000000' 
					AND tax_code != '1' 
					AND tax_code != '-' 
				GROUP BY tax_code 
				HAVING c > 1
			");

			foreach($doppi_cf as $doppio_cf) 
			{
				$clienti_doppi_cf = Db::getInstance()->executeS("
					SELECT DISTINCT c.id_customer, c.firstname, c.email, c.lastname, c.company, c.tax_code
					FROM "._DB_PREFIX_."customer c 
					WHERE c.tax_code = '".$doppio_cf['tax_code']."' 
					ORDER BY c.id_customer DESC
				");

				foreach($clienti_doppi_cf as &$c){
					$href = '/ezadmin/index.php?controller=AdminCustomers&id_customer='.$c['id_customer'].'&viewcustomer&token='.$tokenCustomers;
					$c['href'] = $href;
				}

				$tax_code = $doppio_cf['tax_code'];

				$cf_doppi[] = array(
					'tax_code' => $tax_code,
					'clienti_doppi' => $clienti_doppi_cf,
				);
			}

			$codici_fiscali = array(
				'duplicati' => $cf_doppi,
			);
		}

		// Deduplica anagrafica
		if($tab_name == 'deduplica' && !$is_agente){

			if(Tools::getIsset('azione') && Tools::getValue('azione') == 'deduplica'){
				if(is_numeric(Tools::getValue('partenza')) && is_numeric(Tools::getValue('arrivo')))
				{
					Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'orders SET id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza'));
					Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'cart SET id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza'));
					// Correggere: eliminare la tabella carrelli_creati?
					Db::getInstance()->execute('UPDATE carrelli_creati SET id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza'));
					Db::getInstance()->execute('UPDATE fattura SET id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza'));
					Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'address SET id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza').' and active = 1 and fatturazione = 0 and deleted = 0');
					Db::getInstance()->execute('UPDATE action_thread SET id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza'));
					Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'customer_thread SET id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza'));
					Db::getInstance()->execute('UPDATE form_prevendita_thread SET id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza'));
					Db::getInstance()->execute('UPDATE form_prevendita_message SET id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza'));
					Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'contratto_assistenza SET id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza'));
					Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'bdl SET id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza'));
					Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'guest SET id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza'));
					Db::getInstance()->execute('UPDATE persone SET id_customer = '.Tools::getValue('arrivo').' where id_customer = '.Tools::getValue('partenza'));
					
					$customer_p = new Customer(Tools::getValue('partenza'));
					$customer_a = new Customer(Tools::getValue('arrivo'));

					$nome_p = ($customer_p->is_company ? $customer_p->company : $customer_p->firstname.' '.$customer_p->lastname);
					$nome_a = ($customer_a->is_company ? $customer_a->company : $customer_a->firstname.' '.$customer_a->lastname);

					$stringa_1 = 'dati ID '.Tools::getValue('partenza').' ('.$nome_p.') spostati su ID '.Tools::getValue('arrivo').' ('.$nome_a.').';
					
					if(Tools::getIsset('cancella_partenza')){
						$customer = new Customer(Tools::getValue('partenza'));
						$customer->delete();
						$stringa_2 = 'Anagrafica ID '.Tools::getValue('partenza').' ('.$nome_p.') cancellata';
					}
					else
						$stringa_2 = '';

					$this->confirmations[] = "Operazione eseguita con successo: ".$stringa_1." ".$stringa_2." ";
				}
				else{
					$this->errors[] = Tools::displayError('I campi ID devono essere numerici.');
				}
			}
		}

        $this->tpl_view_vars = array(
			'partite_iva' => $partite_iva,
			'codici_fiscali' => $codici_fiscali,
			'is_agente' => $is_agente,
			'tab_name' => $tab_name,
            //'show_toolbar' => true
        );

        return parent::renderView();
    }
}