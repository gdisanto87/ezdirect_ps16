<?php

class AdminTuttiPrezziSpecialiControllerCore extends AdminController
{
    public function __construct()
    {
        $this->displayWarning('Correggere azione edit: il tasto salva non disabilita gli input e le date diventano undefined (ma vengono salvate correttamente)');

        $this->bootstrap = true;
        $this->table = 'specific_price';
        $this->className = 'SpecificPrice';
        $this->lang = false;
        $this->explicitSelect = true;

        $this->allow_export = true;

        $this->context = Context::getContext();
        $this->context->currency = Currency::getDefaultCurrency();

        $this->_select = '
            p.id_product AS id_prodotto, a.id_product AS id_prodotto_2, p.reference, p.stock_quantity, pl.name AS nome_prodotto, m.name AS costruttore,
            a.reduction_type, a.reduction,
            IF(a.reduction_type = "amount", a.price - a.reduction, a.price - (a.price * a.reduction)) as prezzo_speciale,
            a.id_specific_price AS id_edit
		';
		
        $this->_join = '
            JOIN '._DB_PREFIX_.'product p ON p.id_product = a.id_product
            JOIN '._DB_PREFIX_.'product_lang pl ON pl.id_product = a.id_product
            JOIN '._DB_PREFIX_.'manufacturer m ON m.id_manufacturer = p.id_manufacturer 
            LEFT OUTER JOIN '._DB_PREFIX_.'specific_price_pieces spp ON spp.id_specific_price = a.id_specific_price
        ';

        $this->_where = '
            AND pl.id_lang = '.$this->context->language->id.'
            AND a.specific_price_name != "sc_qta_1" 
            AND a.specific_price_name != "sc_qta_2" 
            AND a.specific_price_name != "sc_qta_3" 
            AND a.specific_price_name != "sc_riv_1" 
            AND a.specific_price_name != "sc_riv_2" 
            AND a.specific_price_name != "sc_riv_3"
		';

        $this->_defaultOrderBy = 'a.id_specific_price';
        $this->_orderBy = 'a.id_specific_price';
        $this->_orderWay = 'ASC';
        
        $this->_use_found_rows = false;

        $this->fields_list = array(
            'id_specific_price' => array(
                'title' => $this->l('ID Prezzo'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
                'remove_onclick' => true,
            ),
            'reference' => array(
                'title' => $this->l('Cod. Prodotto'),
                'align' => 'text-center',
                'callback' => 'creaTooltip',
                'remove_onclick' => true,
            ),
            'nome_prodotto' => array(
                'title' => $this->l('Desc. Prodotto'),
                'align' => 'text-left',
                'remove_onclick' => true,
            ),
            'costruttore' => array(
                'title' => $this->l('Costruttore'),
                'align' => 'text-left',
                'remove_onclick' => true,
            ),
            'stock_quantity' => array(
                'title' => $this->l('Mag. EZ'),
                'align' => 'text-left',
                'remove_onclick' => true,
            ),/* // Correggere: chiedere a Federico la differenza tra stock e pieces; dobbiamo usare pieces
            'stock' => array(
                'title' => $this->l('Qt. disp.'),
                'align' => 'text-left',
                'callback' => 'coloraPieces',
                'remove_onclick' => true,
            ),*/
            'pieces' => array(
                'title' => $this->l('Qt. disp.'),
                'align' => 'text-left',
                'callback' => 'coloraPieces',
                'remove_onclick' => true,
            ),
            'id_prodotto' => array(
                'title' => $this->l('Miglior p. CLI'),
                'align' => 'text-right',
                'callback' => 'calcolaMigliorPrezzoCli',
                'orderby' => false, // correggere: disattivati perchè filtrano per id_product
                'search' => false, // correggere: disattivati perchè filtrano per id_product
                'remove_onclick' => true,
            ),
            'id_prodotto_2' => array(
                'title' => $this->l('Miglior p. RIV'),
                'align' => 'text-right',
                'callback' => 'calcolaMigliorPrezzoRiv',
                'orderby' => false, // correggere: disattivati perchè filtrano per id_product
                'search' => false, // correggere: disattivati perchè filtrano per id_product
                'remove_onclick' => true,
            ),
            'prezzo_speciale' => array( // correggere: non funziona la ricerca
                'title' => $this->l('Prezzo Speciale'),
                'align' => 'text-right',
                'class' => 'mod',
                'callback' => 'calcolaPrezzoSpeciale',
                'prefix' => '<b>',
                'suffix' => '</b>',
                'tmpTableFilter' => true,
                'remove_onclick' => true,
            ),
            'from' => array(
                'title' => $this->l('Partenza'),
                'align' => 'text-right',
                'class' => 'mod mod_data_p',
                'type' => 'datetime',
                'callback' => 'format_data',
                'remove_onclick' => true,
            ),
            'to' => array(
                'title' => $this->l('Fine'),
                'align' => 'text-right', 
                'class' => 'mod mod_data_f',
                'type' => 'datetime',
                'filter_key' => 'a!to',
                'callback' => 'format_data_to',
                'remove_onclick' => true,
            ),
            'id_edit' => array(
                'title' => $this->l('Edit'),
                'align' => 'text-right', 
                'class' => 'edit',
                'orderby' => false,
                'search' => false,
                'callback' => 'crea_pulsante_edit',
                'remove_onclick' => true,
            ),
        );

        $this->addRowAction('delete');

        $this->bulk_actions = array( // Queste azioni si trovano in AdminController (processBulk...)
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?'),
                'icon' => 'icon-trash'
            ),
            'allunga' => array(
                'text' => $this->l('Allunga fino a fine mese (da oggi) i prezzi speciali flaggati'),
                'confirm' => $this->l('Apply changes to selected items?'),
                'icon' => 'icon-double-angle-right'
            ),
            'allunga_successivo' => array(
                'text' => $this->l('Allunga fino a fine mese SUCCESSIVO (da oggi) i prezzi speciali flaggati'),
                'confirm' => $this->l('Apply changes to selected items?'),
                'icon' => 'icon-arrow-right'
            )
        );
        
        parent::__construct();
    }

    public function renderList()
    {
        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'tooltip_prodotto.js');
        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'edit_speciali.js'); // correggere
        $this->addJS(_PS_BO_DEFAULT_THEME_DIR_.'plugins/TaTa/dist/tata.js');

        return parent::renderList();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Tutti i prezzi speciali di vendita'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
	}

    public function initToolbar()
    {
        parent::initToolbar();

        unset($this->toolbar_btn['new']); // Toglie il pulsante ADD NEW dalla toolbar
    }

    public function format_data($value)
	{
        if($value == '0000-00-00 00:00:00' || $value == '1970-01-01 00:00:00')
            $value = date('Y-01-01 00:00:00');

        $data = new DateTime($value);
        $data = $data->format('d/m/Y H:i');

        return '<span>'.$data.'</span>';
    }

    public function format_prezzo($value)
	{
        // CON SIMBOLO EURO E . PER I DECIMALI
        /* $prezzo = (float)(str_replace(",",".",$value));
        $prezzo = Tools::displayPrice($prezzo, $this->context->currency); */

        $prezzo = number_format($value, 2, ',', '.');

        return '<span>'.$prezzo.'</span>';
    }

    public function format_sconto($value)
	{
        $sconto = number_format($value, 2, ',', '.');

        return '<span>'.$sconto.' %</span>';
    }

    public function creaTooltip($value, $tr)
	{
        $tokenProducts = Tools::getAdminTokenLite('AdminProducts');
        $href_prodotto = '/ezadmin/index.php?controller=AdminProducts&id_product='.$tr['id_prodotto'].'&updateproduct&token='.$tokenProducts;
        return '<a class="tooltip_prodotto" title="'.Product::showProductTooltip($tr['id_prodotto']).'" href="'.$href_prodotto.'" target="_blank">'.$value.'</a>';
    }

    public function coloraPieces($value)
	{
        if(($value == 0 && $value != ''))
            return '<span class="thick rosso">'.$value.'</span>';
        else
            return $value;
    }

    public function format_data_to($value)
	{
        if($value == '0000-00-00 00:00:00' || $value == '1970-01-01 00:00:00')
            $value = date('Y-01-01 00:00:00');

        $data = new DateTime($value);
        $data = $data->format('d/m/Y H:i');

        if($value < date('Y-m-d H:i:s'))
            return '<span class="thick rosso">'.$data.'</span>';
        else
            return '<span>'.$data.'</span>';
    }

    public function calcolaMigliorPrezzoCli($value)
	{
        $prezzo = Product::trovaMigliorPrezzo($value, 1, 9999999, 'n');
        $prezzo = number_format($prezzo, 2, ',', '.');
        return $prezzo;
    }

    public function calcolaMigliorPrezzoRiv($value)
	{
        $prezzo = Product::trovaMigliorPrezzo($value, 3, 9999999, 'n');
        $prezzo = number_format($prezzo, 2, ',', '.');
        return $prezzo;
    }

    public function calcolaPrezzoSpeciale($value, $tr)
	{
        $prezzo_speciale = number_format($value, 2, ',', '.');

        return '<input type="text" id="new_special_price_'.$tr['id_specific_price'].'" name="new_special_price_'.$tr['id_specific_price'].'" value="'.$prezzo_speciale.'" readonly />';
        // return '<span>'.$prezzo_speciale.'</span>';
    }

    public function crea_pulsante_edit($value)
    {
        return '<a class="edit" title="Edit"><i class="icon-edit"></i></a><a class="add" title="Save"><i class="icon-save"></i></a>';
    }
}