<?php

class AdminMarketingTelefonateControllerCore extends AdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->lang = false;

        $this->context = Context::getContext();

        $this->display = 'view';

        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Analisi Telefonate'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }

    public function renderView()
    {
        $this->displayInformation('Verificare funzionamento date');

        if(isset($_POST['cercaiclienti'])) 
        {
            $prodotti = "";
            $categorie_di_default = "";
            $macrocategorie = "";
            $macrocategorie_di_default = "";
            $categorie = "";
            $marchi = "";
            
            if(Tools::getValue('modalita') == 1)
            {
                $strisciamodalita = 'Ricerca eseguita sulle chiamate in uscita';		
                $modalita = 1;
            }
            else if(Tools::getValue('modalita') == 2)
            {
                $strisciamodalita = 'Ricerca eseguita sia sulle chiamate in ingresso sia sulle chiamate in uscita';		
                $modalita = 2;
            }	
            else
            {
                $strisciamodalita = 'Ricerca eseguita sulle chiamate in ingresso';			
                $modalita = 0;
            }
            $riepilogo_html .= "<strong>Riepilogo</strong><br /><br />";
            
            if(!empty($_POST['arco_dal'])) {
                // $data_dal_a = explode("-", $_POST['arco_dal']);
                // $data_dal = $data_dal_a[2]."-".$data_dal_a[1]."-".$data_dal_a[0]; 
                
                if($modalita == 1 || $modalita == 2)
                    $arco_dal = 'AND c.date_add > "'.$_POST['arco_dal'].' 00:00:00"';
                else
                    $arco_dal = 'AND t.datetime > "'.$_POST['arco_dal'].' 00:00:00"';
                
                $striscia_arco_dal = "Da: <strong>".$_POST['arco_dal']."</strong> ";
            }
            else {
                $striscia_arco_dal = "Da: <strong>inizio della storia del sito</strong> ";
            }
            
            if(isset($_POST['striscia_arco_dal'])) {
                $striscia_arco_dal = $_POST['striscia_arco_dal'];
            }
        
            $riepilogo_html .= $striscia_arco_dal;
            
            if(!empty($_POST['arco_al'])) {
                // $data_al_a = explode("-", $_POST['arco_al']);
                // $data_al = $data_al_a[2]."-".$data_al_a[1]."-".$data_al_a[0]; 
                
                if($modalita == 1 || $modalita == 2)
                    $arco_al = 'AND c.date_add < "'.$_POST["arco_al"].' 00:00:00"';
                else
                    $arco_al = 'AND t.datetime < "'.$_POST["arco_al"].' 00:00:00"';
            
                $striscia_arco_al = "a: <strong>".$_POST['arco_al']."</strong> ";
            }
            else {
                $striscia_arco_al = "a: <strong>oggi</strong> ";
            }
            
            if(isset($_POST['striscia_arco_al'])) {
                $striscia_arco_al = $_POST['striscia_arco_al'];
            }
        
            $riepilogo_html .= $striscia_arco_al."<br />";
                    
            $strisciatelefonate = '';
            $numero_telefonate = '';
            if(Tools::getValue('numero_telefonate') != 0)
            {
                switch(Tools::getValue('numero_telefonate'))
                {
                    case '1_10': 
                        $numero_telefonate = ' AND numero_telefonate >= 1 AND numero_telefonate <= 10 '; 
                        $strisciatelefonate = ' che hanno effettuato da 1 a 10 telefonate'; 
                        break;
                    case '11_30': 
                        $numero_telefonate = ' AND numero_telefonate >= 11 AND numero_telefonate <= 30 '; 
                        $strisciatelefonate = ' che hanno effettuato da 11 a 30 telefonate'; 
                        break;
                    case '31_50': 
                        $numero_telefonate = ' AND numero_telefonate >= 31 AND numero_telefonate <= 50 '; 
                        $strisciatelefonate = ' che hanno effettuato da 31 a 50 telefonate'; 
                        break;
                    case '50p': 
                        $numero_telefonate = ' AND numero_telefonate > 50 '; 
                        $strisciatelefonate = ' che hanno effettuato pi&ugrave; di 50 telefonate'; 
                        break;
                }
            }	
            
            $strisciadurata = '';
            $durata = '';
            
            if(Tools::getValue('durata') != 0)
            {
                switch(Tools::getValue('durata'))
                {
                    case '1_15': 
                        $durata = ' AND durata >= 60 AND durata <= 900 '; 
                        $strisciadurata = ' che hanno effettuato chiamate per una durata complessiva compresa tra 1 e 15 minuti'; 
                        break;
                    case '16_30': 
                        $durata = ' AND durata >= 901 AND durata <= 1800 '; 
                        $strisciadurata = ' che hanno effettuato chiamate per una durata complessiva compresa tra 16 e 30 minuti'; 
                        break;
                    case '31_60': 
                        $durata = ' AND durata >= 1801 AND durata <= 3600 '; 
                        $strisciadurata = ' che hanno effettuato chiamate per una durata complessiva compresa tra 31 e 60 minuti'; 
                        break;
                    case '60p': 
                        $durata = ' AND durata > 3600 '; 
                        $strisciadurata = ' che hanno effettuato chiamate per una durata complessiva superiore a un\'ora'; 
                        break;
                }
            }	
            
            $strisciareparto = '';
            $reparto = '';
            
            if(Tools::getValue('reparto') != 0)
            {
                switch(Tools::getValue('reparto'))
                {
                    case '6700': 
                        $reparto = ' AND dst LIKE "%6700%" '; 
                        $strisciareparto = ' che hanno effettuato chiamate verso il reparto ordini'; 
                        break;
                    case '6701': 
                        $reparto = ' AND (dst LIKE "%6701%" OR dst LIKE "%6706%") '; 
                        $strisciareparto = ' che hanno effettuato chiamate verso il reparto commerciale'; 
                        break;
                    case '6702': 
                        $reparto = ' AND dst LIKE "%6702%" '; 
                        $strisciareparto = ' che hanno effettuato chiamate verso il reparto assistenza'; 
                        break;
                    case '6703': 
                        $reparto = ' AND dst LIKE "%6703%" '; 
                        $strisciareparto = ' che hanno effettuato chiamate verso il reparto amministrazione'; 
                        break;
                    case '6704': 
                        $reparto = ' AND (dst LIKE "%6704%" OR dst LIKE "%6706%") '; 
                        $strisciareparto = ' che hanno effettuato chiamate in entrata'; 
                        break;
                }
            }	
            
            $strisciainterno = '';
            $interno = '';
            
            if(Tools::getValue('interno') != 0)
            {
                if($modalita == 1)
                    $interno = ' AND src LIKE "%'.Tools::getValue('interno').'%"';
                else if($modalita == 0)
                    $interno = ' AND dst LIKE "%'.Tools::getValue('interno').'%"';
                else if($modalita == 2)
                    $interno = ' AND (src LIKE "%'.Tools::getValue('interno').'%" OR dst LIKE "%'.Tools::getValue('interno').'%")';
                
                switch(Tools::getValue('interno'))
                {
                    case '201': $strisciainterno .= 'chiamate su interno Ezio'; break;
                    case '202': $strisciainterno .= 'chiamate su interno Barbara'; break;
                    case '203': $strisciainterno .= 'chiamate su interno Paolo'; break;
                    case '205': $strisciainterno .= 'chiamate su interno Massimo'; break;
                    case '206': $strisciainterno .= 'chiamate su interno Magazzino'; break;
                    case '207': $strisciainterno .= 'chiamate su interno Matteo'; break;
                    case '208': $strisciainterno .= 'chiamate su interno Valentina'; break;
                    case '209': $strisciainterno .= 'chiamate su interno Federico'; break;
                    case '210': $strisciainterno .= 'chiamate su interno Sala Riunioni'; break;
                    case '211': $strisciainterno .= 'chiamate su interno Ingresso'; break;
                    case '212': $strisciainterno .= 'chiamate su interno Laboratorio'; break;
                    case '214': $strisciainterno .= 'chiamate su interno Laboratorio 1'; break;
                    case '215': $strisciainterno .= 'chiamate su interno Laboratorio 2'; break;
                    case '216': $strisciainterno .= 'chiamate su interno Laboratorio 3'; break;
                    case '217': $strisciainterno .= 'chiamate su interno Laboratorio 4'; break;
                    case '218': $strisciainterno .= 'chiamate su interno Amministrazione'; break;
                    case '219': $strisciainterno .= 'chiamate su interno Lorenzo'; break;
                }
            }	
            
            $strisciaprivati = '';
            $strisciagruppi = '';
            
            $gruppi = 'AND (';
            foreach($_POST['group'] as $key => $value)
            {	
                $gruppi .= 'c.id_default_group = '.$key.' OR ';
                $strisciagruppi .= ' '.Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'group_lang WHERE id_group = '.$key.' AND id_lang = '.$this->context->language->id).',';
            }	
            $gruppi	.= 'c.id_default_group = 99999999)';
            $strisciagruppi = 'In questi gruppi:'.substr($strisciagruppi, 0, -1);	
                
            if($_POST['escludi_privati'] == 1) {
            
                $strisciaprivati = "<strong>Privati esclusi</strong>";
            }
            else {
                $strisciaprivati = "<strong>Aziende e privati inclusi</strong>";
            }
            
            if(Tools::getValue('inattivita') > 0)
                $strisciainattivita = 'Che non acquistano da '.Tools::getValue('inattivita').' mesi';
            
            $riepilogo_html .= $strisciaprivati.'<br />'.$strisciagruppi.'<br />'.($strisciatelefonate != '' ? $strisciatelefonate.'<br />' : '').($strisciadurata != '' ? $strisciadurata.'<br />' : '').($strisciareparto != '' ? $strisciareparto.'<br />' : '').($strisciainterno != '' ? $strisciainterno.'<br />' : '').'<strong>'.$strisciamodalita.'</strong><br />';
            
            $anno_corrente = date("Y");
            
            $where = $reparto.$interno;
            
            $from = 'FROM '._DB_PREFIX_.'customer c JOIN telefonate t ON c.id_customer = t.id_customer JOIN '._DB_PREFIX_.'address a ON c.id_customer = a.id_customer
                    JOIN '._DB_PREFIX_.'state s ON a.id_state = s.id_state';
            
            if($modalita == 1) {
                $where .= ' AND calltype = "Outbound" '; 
            }	
            else if($modalita == 0) {
                $where .= ' AND calltype = "Inbound" '; 
            }
            else {
                $where .= ' AND (calltype = "Inbound" OR calltype = "Outbound") '; 
            }		
            
            if(isset($_POST['querysql'])) {
                $sql = $_POST['querysql'];
            }
            else 
            {
                $sql = "
                    SELECT * 
                    FROM (
                        SELECT count(id_telefonata) numero_telefonate, sum(billable) durata, 
                        IF(calltype LIKE '%inbound%', SUM(billable),NULL) AS durata_ricevute, IF(calltype LIKE '%outbound%', SUM(billable),NULL) AS durata_inviate,
                        IF(calltype LIKE '%inbound%',count(id_telefonata),NULL) AS totale_ricevute, IF(calltype LIKE '%outbound%', count(id_telefonata),NULL) AS totale_inviate, 
                        c.id_customer, c.firstname, c.lastname, gr.name gruppo, s.iso_code, c.email, c.id_default_group, c.is_company, c.company, a.phone, a.phone_mobile, a.fax, fat.totale_acquisti, fa.ultimo_anno,
                        c.firstname AS nome_mailup, c.lastname AS cognome_mailup, c.company AS azienda_mailup, c.email AS email_mailup, REPLACE(a.address1,',',' ') AS indirizzo_mailup, a.city AS citta_mailup, a.postcode AS cap_mailup, a.phone AS telefono_mailup, a.phone_mobile AS cellulare_mailup,
                        primo.data_fattura primo_acquisto, ultimo.data_fattura ultimo_acquisto, (CASE WHEN c.is_company = 0 THEN CONCAT(c.lastname,\" \",c.firstname) WHEN c.is_company = 1 THEN (CASE WHEN c.company IS NULL OR c.company = \"\" THEN CONCAT(c.lastname,\" \",c.firstname) ELSE c.company END) END) cliente
                    
                        ".$from."
                        LEFT JOIN (SELECT * FROM "._DB_PREFIX_."fattura GROUP BY id_customer ORDER BY data_fattura ASC ) primo ON primo.id_customer = c.id_customer
                        LEFT JOIN (SELECT id_customer, MAX(data_fattura) data_fattura FROM "._DB_PREFIX_."fattura GROUP BY id_customer ORDER BY data_fattura DESC) ultimo ON ultimo.id_customer = c.id_customer
                        LEFT JOIN (SELECT * FROM "._DB_PREFIX_."group_lang WHERE id_lang = ".$this->context->language->id.") gr ON c.id_default_group = gr.id_group
                        LEFT JOIN (SELECT SUM(fprv.totale_fattura) AS totale_acquisti, fprv.id_customer FROM (SELECT totale_fattura, id_customer FROM "._DB_PREFIX_."fattura GROUP BY id_fattura) fprv GROUP BY fprv.id_customer) fat ON fat.id_customer = c.id_customer
                        LEFT JOIN (SELECT SUM(totale_fattura) AS ultimo_anno, id_customer FROM (SELECT totale_fattura, id_customer FROM "._DB_PREFIX_."fattura WHERE data_fattura > '".$anno_corrente."-01-01 00:00:00' AND data_fattura < '".$anno_corrente."-12-31 00:00:00' GROUP BY id_fattura) faprv GROUP BY faprv.id_customer) fa ON fa.id_customer = c.id_customer
                        ".(Tools::getIsset('prev_richiesto') ? 'JOIN (SELECT * FROM form_prevendita_thread WHERE tipo_richiesta = "preventivo") fp ON fp.id_customer = c.id_customer' : '')."
                        ".(Tools::getIsset('ric_richiesto') ? 'JOIN (SELECT * FROM form_prevendita_thread WHERE tipo_richiesta = "tirichiamiamonoi") fp2 ON fp2.id_customer = c.id_customer' : '')."
                        ".(Tools::getIsset('prev_ricevuto')  ? 'JOIN "._DB_PREFIX_."cart ca ON ca.id_customer = c.id_customer' : '')."
                    
                        WHERE a.fatturazione =1
                            AND a.deleted =0
                            AND a.active =1 
                            ".(Tools::getValue('escludi_privati') == 1 ? 'AND c.is_company = 1' : '')."
                            ".(Tools::getValue('inattivita') > 0 ? 'AND DATE_FORMAT(ultimo.data_fattura, "%Y-%m-%d %H:%i") < DATE_FORMAT(NOW() - INTERVAL '.Tools::getValue('inattivita').' MONTH, "%Y-%m-%d %H:%i")' : '')."
                            ".$gruppi."
                            ".$arco_dal."
                            ".$arco_al."
                            ".$where." 
                    
                        GROUP BY c.id_customer
                    ) cz 
                    WHERE cz.id_customer > 0 ".$numero_telefonate.$durata." 
                    GROUP BY cz.id_customer
                ";
            }

            $numeroclienti = Db::getInstance()->NumRows($sql);
            
            $riepilogo_html .= "<form method='post' action=''>";

            //$riepilogo_html .= "<!-- <input type='submit' value='Esporta i dati in formato Excel' name='esportaexcel' class='button' /> -->";
            $riepilogo_html .= "<input type='submit' value='Vedi solo le mail in formato TXT per invio' name='esportamailtxt' class='button' />
            <input type='submit' value='Esporta lista in formato CSV per Mailup' name='esportamailup' class='button' />
            <input type='hidden' name='querysql' value='".htmlentities((isset($_POST['querysql']) ? $_POST['querysql'] : $sql),ENT_QUOTES)."' />	
            <input type='hidden' name='query_from' value='".htmlentities((isset($_POST['query_from']) ? $_POST['query_from'] : $from),ENT_QUOTES)."' />	
            <input type='hidden' name='query_where' value='".htmlentities((isset($_POST['query_where']) ? $_POST['query_where'] : $where),ENT_QUOTES)."' />						
            <input type='hidden' name='prodottiinclusi' value='".$prodottiinclusi."' />		
            <input type='hidden' name='strisciaprodotti' value='".$strisciaprodotti."' />		
            <input type='hidden' name='categorieincluse' value='".$categorieincluse."' />	
            <input type='hidden' name='macrocategorieincluse' value='".$macrocategorieincluse."' />						
            <input type='hidden' name='strisciacat' value='".$strisciacat."' />	
            <input type='hidden' name='marchiinclusi' value='".$marchiinclusi."' />		
            <input type='hidden' name='strisciamarchi' value='".$strisciamarchi."' />	
            <input type='hidden' name='striscia_escludi_privati' value='".$striscia_escludi_privati."' />				
            <input type='hidden' name='striscia_arco_dal' value='".$striscia_arco_dal."' />	
            <input type='hidden' name='striscia_arco_al' value='".$striscia_arco_al."' />	
            <input type='hidden' name='strisciaprivati' value='".$strisciaprivati."' />	
            <input type='hidden' name='strisciainattivita' value='".$strisciainattivita."' />	
            <input type='hidden' name='strisciagruppi' value='".$strisciagruppi."' />	
            <input type='hidden' name='strisciaprevric' value='".$strisciaprevric."' />	
            <input type='hidden' name='strisciaprevriv' value='".$strisciaprevriv."' />	
            <input type='hidden' name='strisciaricric' value='".$strisciaricric."' />
            ";
            
            $sql.= $orderby;
        
            $resultsclienti = Db::getInstance()->ExecuteS(stripslashes($sql));
            $tot_acquisti = 0;
            
            $num = count($resultsclienti);
            foreach($resultsclienti as $r)
                $tot_acquisti += $r['totale_acquisti'];
        
            $totale_chiamate = 0;
            $totale_durata = 0;
            $totale_ricevute = 0;
            $totale_inviate = 0;
            $durata_ricevute = 0;
            $durata_inviate = 0;
        
            $riepilogo_html .= "<input type='hidden' name='num' value='".$num."' />		
            <input type='hidden' name='tot_acquisti' value='".$tot_acquisti."' />";	
            $riepilogo_html .= "<table class='table'><tr><td>Totale clienti trovati</td><td><strong>$num</strong></td></tr>";
            $riepilogo_html .= "<tr><td>Totale acquisti</td><td><strong>".Tools::displayPrice($tot_acquisti,1)."</strong></td></tr></table><br />";
            $riepilogo_html .= "<div id='table-container'><table class='table tablesorter' id='table-clienti'><thead>";
            $riepilogo_html .= "<tr>";
            $riepilogo_html .= "<th style='width:30px'>N.</th>";
            $riepilogo_html .= "<th style='width:45px'>Id cliente 
            </th>";
            $riepilogo_html .= "<th style='width:150px'>Cliente
            </th>";
            $riepilogo_html .= "<th style='width:50px'>Tipo
            </th>";
            $riepilogo_html .= "<th style='width:50px'>Gruppo
            </th>";
            $riepilogo_html .= "<th style='width:30px'>Provincia
            </th>";
            $riepilogo_html .= "<th style='width:80px'>Totale acq. in euro
            </th>";
            $riepilogo_html .= "<th style='width:80px'>Totale acq. ".$anno_corrente."
            </th>";
            
            if($modalita == 2)
            {
                $riepilogo_html .= "<th style='width:80px'>Tot. ricevute</th>";
                $riepilogo_html .= "<th style='width:80px'>Dur. ricevute</th>";
                $riepilogo_html .= "<th style='width:80px'>Tot. inviate</th>";
                $riepilogo_html .= "<th style='width:80px'>Dur. inviate</th>";
            }
            else
            {
                $riepilogo_html .= "<th style='width:80px'>Tot.</th>";
                $riepilogo_html .= "<th style='width:80px'>Durata complessiva</th>";
            }

            //$riepilogo_html .= "<th>Mesi inattivit&agrave;</th>";
            
            $riepilogo_html .= "<th>Vai</th></tr></thead><tbody>";
            
            $i = 1;
            
            foreach ($resultsclienti as $row) 
            {
                if($row['totale_ricevute'] == '')
                    $row['totale_ricevute'] = 0;
                
                if($row['totale_inviate'] == '')
                    $row['totale_inviate'] = 0;
                
                if($row['is_company'] == 1) {
                    $tipo = 'A';
                }
                else {
                    $tipo = 'P';
                }

                // $mesi_inattivita = $this->datediff("M", $row['ultimo_acquisto'], date("Y-m-d H:i:s"));
                
                $tokenCustomers = Tools::getAdminToken('AdminCustomers'.(int)(Tab::getIdFromClassName('AdminCustomers')).(int)($this->context->employee->id));
                
                $riepilogo_html .= "<tr>";
                $riepilogo_html .= "<td style='width:30px'>".$i."</td>";
                $riepilogo_html .= "<td>".$row['id_customer']."</td>";
                $riepilogo_html .= "<td>".$row['cliente']."</td>";
                $riepilogo_html .= "<td>".$tipo."</td>";
                $riepilogo_html .= "<td>".$row['gruppo']."</td>";
                $riepilogo_html .= "<td style='width:30px'>".$row['iso_code']."</td>";
                
                $riepilogo_html .= "<td style='text-align:right'>".Tools::displayPrice($row['totale_acquisti'],1)."</td>";
                
                $riepilogo_html .= "<td style='text-align:right'>".Tools::displayPrice($row['ultimo_anno'],1)."</td>";
                
                if($modalita == 2)
                {
                    $riepilogo_html .= "<td style='text-align:right'>".$row['totale_ricevute']."</td>";
                    $riepilogo_html .= "<td style='text-align:right'>".gmdate("H:i:s", $row['durata_ricevute'])."</td>";
                    $riepilogo_html .= "<td style='text-align:right'>".$row['totale_inviate']."</td>";
                    $riepilogo_html .= "<td style='text-align:right'>".gmdate("H:i:s", $row['durata_inviate'])."</td>";
                }
                else
                {
                    $riepilogo_html .= "<td style='text-align:right'>".$row['numero_telefonate']."</td>";
                    $riepilogo_html .= "<td style='text-align:right'>".gmdate("H:i:s", $row['durata'])."</td>";
                }
                // $riepilogo_html .= "<td>".$mesi_inattivita."</td>";
                
                $riepilogo_html .= "<td><a href='index.php?controller=AdminCustomers&id_customer=".$row['id_customer']."&viewcustomer&token=".$tokenCustomers."' target='_blank'><img src='../img/admin/details.gif' alt='Apri cliente' title='Apri cliente' /></a></td>";
                
                $totale_chiamate += $row['numero_telefonate'];
                $totale_durata += $row['durata'];
                $totale_ricevute += $row['totale_ricevute'];
                $durata_ricevute += $row['durata_ricevute'];
                $totale_inviate += $row['totale_inviate'];
                $durata_inviate += $row['durata_inviate'];
                
                $riepilogo_html .= "</tr>";
                
                $i++;
            }
            
            $riepilogo_html .= "</tbody><tfoot><tr><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th>".($modalita == 2 ? '<th>'.$totale_ricevute.'</th><th>'.gmdate("H:i:s", $durata_ricevute).'</th><th>'.$totale_inviate.'</th><th>'.gmdate("H:i:s", $durata_inviate).'</th>' : "<th>".$totale_chiamate."</th><th>".gmdate("H:i:s", $totale_durata)."</th>")."<th></th></tr></tfoot></table></div><div id='bottom_anchor'></div></form>";

            $this->tpl_view_vars = array(
                'modalita' => $modalita,
                'riepilogo_html' => $riepilogo_html, // Variabile che sostituisce gli echo finchè non viene portato tutto in bootstrap
            );
        }
        else if(isset($_POST['esportamailup'])) // correggere
        {
            $resultsclienti = Db::getInstance()->executeS(html_entity_decode($_POST['querysql']));
            $mails = '';

            foreach ($resultsclienti as $row) {
                if(isset($_POST['esportaaziende']) && $row['is_company'] == 0) {}
                else if(isset($_POST['esportaprivati']) && $row['is_company'] == 1) {}
                else if(isset($_POST['esportaclienti']) && ($row['id_default_group'] == 3 || $row['id_default_group'] != 15)) {}
                else if(isset($_POST['esportarivenditori']) && ($row['id_default_group'] != 3 && $row['id_default_group'] != 15)) {}
                else {
                    if(isset($_POST['escludimarketplace']) && ($row['id_default_group'] == 16 || $row['id_default_group'] == 17)) {}
                    else {
                        $mails.= $row['nome_mailup'].";".$row['cognome_mailup'].";".$row['azienda_mailup'].";".$row['email_mailup'].";".$row['indirizzo_mailup'].";".$row['citta_mailup'].";".$row['cap_mailup'].";".$row['telefono_mailup'].";".$row['cellulare_mailup']."\n";
                    }
                }	
            }
            /* // Decommentare quando avremo il server ezdirect
            $file_csv_mailup_stream=fopen("/var/www/vhosts/ezdirect.it/httpdocs/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-clienti-$data-mailup.csv","w+");
            @fwrite($file_csv_mailup_stream,$mails);
            @fclose("/var/www/vhosts/ezdirect.it/httpdocs/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-clienti-$data-mailup.csv");
    
            header("Location: http://www.ezdirect.it/ezadmin/esportazione-catalogo/catalogo_xls/prodotti-clienti-$data-mailup.csv");*/
        }
        else if(isset($_POST['esportamailtxt']))
        {
            $resultsclienti = Db::getInstance()->executeS(html_entity_decode($_POST['querysql']));
            $mails = '';

            foreach ($resultsclienti as $row) {
                if(isset($_POST['esportaaziende']) && $row['is_company'] == 0) {}
                else if(isset($_POST['esportaprivati']) && $row['is_company'] == 1) {}
                else if(isset($_POST['esportaclienti']) && ($row['id_default_group'] == 3 || $row['id_default_group'] != 15)) {}
                else if(isset($_POST['esportamarketplace']) && ($row['id_default_group'] != 3 && $row['id_default_group'] != 15)) {}
                else {
                    if(isset($_POST['escludirivenditori']) && ($row['id_default_group'] == 16 || $row['id_default_group'] == 17)) {}
                    else {
                        $mails.= $row['email']."; ";
                    }
                }
            }
    
            $esportamailtxt_html = $mails;
            
            // Commentato 1.4
            /*$filename = "mail-clienti.txt";
            
            $newf = fopen($filename, 'w');
            fwrite($newf, $mails);
            fclose($newf);
            header("Content-Description: File Transfer");
            header("Content-Length: ". filesize("$filename").";");
            header("Content-Disposition: attachment; filename=$filename");
            header("Content-Type: application/octet-stream; "); 
            header("Content-Transfer-Encoding: binary");
            readfile($filename);*/

            $this->tpl_view_vars = array(
                'esportamailtxt_html' => $esportamailtxt_html,
            );
        }
        /*else if(isset($_POST['esportaexcel'])){}*/ // da fare, ma nella 1.4 il pulsante è commentato
        else {
            $gruppi_clienti = Db::getInstance()->executeS('SELECT id_group, name FROM '._DB_PREFIX_.'group_lang WHERE id_lang = '.$this->context->language->id.' AND (id_group != 11 AND id_group != 14 AND id_group != 4)');
            
            // Le altre option sono inizializzate nel tpl

            $this->tpl_view_vars = array(
                'gruppi_clienti' => $gruppi_clienti,
            );
        }

        return parent::renderView();
    }
}
