<?php

// Potrei togliere Core e trasformare in override
class AdminForecastStockControllerCore extends AdminController
{
    public function __construct()
    {
        $this->displayInformation('Manca esportazione in excel');
        
        $this->bootstrap = true;
        $this->lang = false;

        $this->context = Context::getContext();

        $this->display = 'view';

        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Forecast Stock'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }
    
    public function renderView()
    {
		$forecast_registrato = Db::getInstance()->getValue('
            SELECT periodo 
            FROM forecast 
            WHERE id_product = 11111
        ');

        $forecast_registrato_2 = round($forecast_registrato, 2);;

        if(Tools::getIsset('azione') && Tools::getValue('azione') == 'vai-forecast')
        {
            // Variabile per capire in che view mi trovo
            // view lista prodotti
            $forecast = 1;

			if(empty($_POST['per_marchio']))
                $marchi .= "p.id_manufacturer > 0 OR ";
            else {
                foreach($_POST['per_marchio'] as $marchio) {
                    if($marchio == 'tutto')
                        $marchi .= "p.id_manufacturer > 0 OR ";
                    else
                        $marchi .= "p.id_manufacturer = ".$marchio." OR ";
                }
            }
			
			if(empty($_POST['per_fornitore']))
                $fornitori .= "p.id_supplier > 0 OR ";
            else {
                foreach($_POST['per_fornitore'] as $fornitore) {
                    if($fornitore == 'tutti_fornitori')
                        $fornitori .= "p.id_supplier > 0 OR ";
                    else
                        $fornitori .= "p.id_supplier = ".$fornitore." OR ";
                }
            }

            $query = '
                SELECT p.*, pl.name as nome_prodotto, m.name as costruttore, s.name as fornitore 
                FROM '._DB_PREFIX_.'product p 
                    JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product = pl.id_product 
                    JOIN '._DB_PREFIX_.'manufacturer m on p.id_manufacturer = m.id_manufacturer 
                    JOIN '._DB_PREFIX_.'supplier s ON s.id_supplier = p.id_supplier 
                WHERE pl.id_lang = '.(int)$this->context->language->id.'
                    AND ('.$marchi.' p.id_manufacturer = 9999999999) 
                    AND ('.$fornitori.' p.id_supplier = 999999999) 
                    AND p.quantity < 99999 
                GROUP BY p.id_product
            ';
            // spostare nel caso export:
			// $query = Tools::getValue('query');
			
            $products = Db::getInstance()->executeS($query);

			foreach($products as &$p)
			{
				$venduto30 = Db::getInstance()->getValue('SELECT SUM(product_quantity) from '._DB_PREFIX_.'orders o join '._DB_PREFIX_.'order_detail od on o.id_order = od.id_order where od.product_reference = "'.$p['reference'].'" and o.date_add BETWEEN NOW() - INTERVAL 30 DAY AND NOW()'); 
				$venduto60 = Db::getInstance()->getValue('SELECT SUM(product_quantity) from '._DB_PREFIX_.'orders o join '._DB_PREFIX_.'order_detail od on o.id_order = od.id_order where od.product_reference = "'.$p['reference'].'" and o.date_add BETWEEN NOW() - INTERVAL 60 DAY AND NOW()'); 
				$venduto90 = Db::getInstance()->getValue('SELECT SUM(product_quantity) from '._DB_PREFIX_.'orders o join '._DB_PREFIX_.'order_detail od on o.id_order = od.id_order where od.product_reference = "'.$p['reference'].'" and o.date_add BETWEEN NOW() - INTERVAL 90 DAY AND NOW()'); 
				$venduto180 = Db::getInstance()->getValue('SELECT SUM(product_quantity) from '._DB_PREFIX_.'orders o join '._DB_PREFIX_.'order_detail od on o.id_order = od.id_order where od.product_reference = "'.$p['reference'].'" and o.date_add BETWEEN NOW() - INTERVAL 180 DAY AND NOW()'); 
				$venduto365 = Db::getInstance()->getValue('SELECT SUM(product_quantity) from '._DB_PREFIX_.'orders o join '._DB_PREFIX_.'order_detail od on o.id_order = od.id_order where od.product_reference = "'.$p['reference'].'" and o.date_add BETWEEN NOW() - INTERVAL 365 DAY AND NOW()'); 
                
                // CORREGGERE: non conviene fare anzi if nelle query?
                if($venduto30 <= 0) $venduto30 = 0;
                if($venduto60 <= 0) $venduto60 = 0;
                if($venduto90 <= 0) $venduto90 = 0;
                if($venduto180 <= 0) $venduto180 = 0;
                if($venduto365 <= 0) $venduto365 = 0;

				$media30 = $venduto30/1;
				$media60 = $venduto60/2;
				$media90 = $venduto90/3;
				$media180 = $venduto180/6;
				$media365 = $venduto365/6;
				$media_tot = ($media30 + $media60 + $media90 + $media180)/4;
				
				$media30 = (round($media30,2));
				$media60 = (round($media60,2));
				$media90 = (round($media90,2));
				$media180 = (round($media180,2));
				$media365 = (round($media365,2));
				
                $previsione_raw = $media_tot - ($media_tot * 0.1);
                
				$qta_ord_clienti = Db::getInstance()->getValue('
                    SELECT SUM(product_quantity - (REPLACE(cons,"0_",""))) 
                    FROM '._DB_PREFIX_.'order_detail od 
                        JOIN '._DB_PREFIX_.'orders o ON o.id_order = od.id_order
				        JOIN '._DB_PREFIX_.'cart c ON c.id_cart = o.id_cart
                        JOIN '._DB_PREFIX_.'order_history oh ON (
                            oh.id_order_history = (
                                SELECT MAX(id_order_history) 
                                FROM '._DB_PREFIX_.'order_history moh 
                                WHERE moh.id_order_state != 18 
                                    AND moh.id_order_state != 27 
                                    AND moh.id_order_state != 28 
                                    AND moh.id_order = od.id_order 
                                GROUP BY moh.id_order
                                )
                            ) 
                        JOIN '._DB_PREFIX_.'order_state os ON os.id_order_state = oh.id_order_state 
                    WHERE c.name NOT LIKE "%prime%" 
                        AND od.product_id = '.$p['id_product'].' 
                        AND os.id_order_state != 4 
                        AND os.id_order_state != 5 
                        AND os.id_order_state != 6 
                        AND os.id_order_state != 7 
                        AND os.id_order_state != 8 
                        AND os.id_order_state != 14 
                        AND os.id_order_state != 16 
                        AND os.id_order_state != 20 
                        AND os.id_order_state != 26 
                        AND os.id_order_state != 29 
                        AND os.id_order_state != 31 
                        AND os.id_order_state != 32 
                        AND os.id_order_state != 35 
                    GROUP BY od.product_id
                ');

                $netto = $p['stock_quantity'] - $qta_ord_clienti;

                /* // commentato 1.4
                if($previsione_raw > 3){	
					$previsione = round($previsione_raw,0);
					$previsione_raw = round($previsione_raw,0);
				}	
				else
                    $previsione = number_format(round($previsione_raw, 2),2,",","");
                */
					
                $previsione = round($previsione_raw, 0, PHP_ROUND_HALF_UP);
                
                $forecast_registrato = round($forecast_registrato, 0);
                
				if($forecast_registrato > 0) {
					$calcolo = $previsione_raw * $forecast_registrato;
					$ord_tot = $netto + $p['ordinato_quantity'] - $calcolo;
					
					if($ord_tot > 0)
						$ord_tot = 0;
					
					$ord_tot = 0 - $ord_tot; // ????????
				}
				else {
					$calcolo = '';
					$ord_tot = '';
				}
				
				if($calcolo > 3)
					$calcolo_round = round($calcolo, 0);
				else
					$calcolo_round = number_format($calcolo, 2, ",", ".");

				$ord_round = round($ord_tot, 0, PHP_ROUND_HALF_UP);

                $p['venduto30'] = $venduto30;
				$p['venduto60'] = $venduto60;
				$p['venduto90'] = $venduto90;
				$p['venduto180'] = $venduto180;
                $p['venduto365'] = $venduto365;
                
                $p['previsione'] = $previsione;
                $p['previsione_raw'] = $previsione_raw;

                $p['ord_mese'] = $previsione_raw; // ?????????
                $p['forecast'] = $calcolo_round; // ?????????

				$p['netto'] = $netto;
				$p['ord'] = $p['ordinato_quantity'];
				$p['ord_netto'] = $netto + $p['ordinato_quantity'];
                $p['ord_periodo'] = $ord_round;

                $p['tooltip'] = Product::showProductTooltip($p['id_product']);
            }
            
            // if(Tools::getIsset('esportaexcel'))
            // Tools::getIsset('azione') && Tools::getValue('azione') == 'esportaexcel'
            // esportaexcel: vedi tutto in ForecastStock 1.4 e mettilo in un file a parte

            $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'override_forecast_stock.js');
            $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'tooltip_prodotto.js');

            $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'datatable_ready.js');
            $this->addJS(_PS_BO_DEFAULT_THEME_DIR_.'plugins/DataTables/datatables.min.js');
            $this->addCSS(_PS_BO_DEFAULT_THEME_DIR_.'plugins/DataTables/datatables.min.css');

            $this->addCSS(_PS_BO_DEFAULT_THEME_CSS_DIR_.'overrides.css');
        }
        // View iniziale (form di ricerca)
        else
        {
            $forecast = 0;
            
            $marchi = Db::getInstance()->ExecuteS("
                SELECT id_manufacturer as id, name 
                FROM "._DB_PREFIX_."manufacturer 
                ORDER BY name
            ");
            
            $fornitori = Db::getInstance()->ExecuteS("
                SELECT id_supplier as id, name 
                FROM "._DB_PREFIX_."supplier 
                WHERE name != '' 
                ORDER BY name
            ");

            $this->addjQueryPlugin(array(
                'select2'
            ));

            $this->addJS(_PS_JS_DIR_.'jquery/plugins/select2/select2_locale_'.$this->context->language->iso_code.'.js');
            $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'select2_ready.js');
        }        

        $this->tpl_view_vars = array(
            'marchi' => $marchi,
            'fornitori' => $fornitori,
            'forecast' => $forecast,
            'forecast_registrato' => $forecast_registrato,
            'forecast_registrato_2' => $forecast_registrato_2,
            'prodotti' => $products,
        );

        return parent::renderView();
    }
}