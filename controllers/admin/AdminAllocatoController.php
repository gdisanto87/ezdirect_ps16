<?php

class AdminAllocatoControllerCore extends AdminController
{
    public function __construct()
    {
        $this->displayInformation('Correggere select di pronto; trovare un modo per mantenere i parametri in get dopo aver selezionato un filtro di default');
        
        $this->bootstrap = true;
        $this->table = 'product';
        $this->lang = false;
        $this->explicitSelect = true;

        $this->allow_export = true;

        $this->context = Context::getContext();

        $this->_select = 'osl.name as stato, od.product_reference, od.id_order_detail, o.id_order as ordine, o.payment as pagamento, o.id_customer, o.verificato, a.stock_quantity as magazzino, pl.name AS nome_prodotto, a.supplier_quantity as allnet, a.esprinet_quantity as esprinet, a.itancia_quantity as itancia, a.ordinato_quantity as qt_ordinato, a.quantity as totale, m.name as costruttore, od.product_quantity, o.date_add as data_ordine, os.id_order_state,';
        $this->_select .= '
            (CASE c.is_company
            WHEN 0
            THEN CONCAT(c.firstname," ",c.lastname)
            WHEN 1
            THEN c.company
            ELSE
            CONCAT(c.firstname," ",c.lastname)
            END
            ) AS cliente,
        ';
        // Astec: sostituisce il filtro assistenza tecnica
        $this->_select .= '
            IF(od.product_reference LIKE "%astec%" OR od.product_reference LIKE "%teleg%" OR od.product_reference LIKE "%inst%", "Sì", "No") as astec,
        ';
        // Calcolo ordinati (Qta. Ord.)
        $this->_select .= '
            IF(os.id_order_state = 15, 
                IF(o.id_order IN (
                    SELECT rif_ordine FROM `'._DB_PREFIX_.'fattura` WHERE TRIM(cod_articolo) = TRIM(a.reference)
                    ), 
                    od.product_quantity - (SELECT SUM(qt_sped) FROM `'._DB_PREFIX_.'fattura` WHERE TRIM(cod_articolo) = TRIM(a.reference) AND rif_ordine = o.id_order), 
                    (SELECT (od3.product_quantity - (REPLACE(od3.cons,"0_",""))) 
                    FROM `'._DB_PREFIX_.'order_detail` od3 
                    JOIN `'._DB_PREFIX_.'orders` o3 ON o3.id_order = od3.id_order 
                    WHERE od3.product_reference = TRIM(a.reference)
                        AND o3.id_order = o.id_order)
                ),
                od.product_quantity
            ) as ordinati,
        ';
        // Calcolo Qt. x cli.: se stock_quantity >= 0, è il minore tra stock_quantity e ordinati (cioè tra Mag. EZ e Qta. Ord.); altrimenti, 'XXX'
        $this->_select .= '
            IF(a.stock_quantity >= 0, 
                LEAST(
                    a.stock_quantity,
                    IF(os.id_order_state = 15, 
                        IF(o.id_order IN (
                            SELECT rif_ordine FROM `'._DB_PREFIX_.'fattura` WHERE TRIM(cod_articolo) = TRIM(a.reference)
                            ), 
                            od.product_quantity - (SELECT SUM(qt_sped) FROM `'._DB_PREFIX_.'fattura` WHERE TRIM(cod_articolo) = TRIM(a.reference) AND rif_ordine = o.id_order), 
                            (SELECT (od4.product_quantity - (REPLACE(od4.cons,"0_",""))) 
                            FROM `'._DB_PREFIX_.'order_detail` od4 
                            JOIN `'._DB_PREFIX_.'orders` o4 ON o4.id_order = od4.id_order 
                            WHERE od4.product_reference = TRIM(a.reference)
                                AND o4.id_order = o.id_order)
                        ),
                        od.product_quantity
                    )
                ),
                "XXX"
            ) as disponibili,
        ';
        // Calcolo Pronto: se Qt. x cli. >= Qta. Ord., pronto = 1 (l'ordine è pronto per la spedizione); altrimenti, pronto = 0
        $this->_select .= '
            IF(a.stock_quantity >= 0, 
                IF(
                    LEAST(
                        a.stock_quantity,
                        IF(os.id_order_state = 15, 
                            IF(o.id_order IN (
                                SELECT rif_ordine FROM `'._DB_PREFIX_.'fattura` WHERE TRIM(cod_articolo) = TRIM(a.reference)
                                ), 
                                od.product_quantity - (SELECT SUM(qt_sped) FROM `'._DB_PREFIX_.'fattura` WHERE TRIM(cod_articolo) = TRIM(a.reference) AND rif_ordine = o.id_order), 
                                (SELECT (od4.product_quantity - (REPLACE(od4.cons,"0_",""))) 
                                FROM `'._DB_PREFIX_.'order_detail` od4 
                                JOIN `'._DB_PREFIX_.'orders` o4 ON o4.id_order = od4.id_order 
                                WHERE od4.product_reference = TRIM(a.reference)
                                    AND o4.id_order = o.id_order)
                            ),
                            od.product_quantity
                        )
                    ) >= IF(os.id_order_state = 15, 
                            IF(o.id_order IN (
                                SELECT rif_ordine FROM `'._DB_PREFIX_.'fattura` WHERE TRIM(cod_articolo) = TRIM(a.reference)
                                ), 
                                od.product_quantity - (SELECT SUM(qt_sped) FROM `'._DB_PREFIX_.'fattura` WHERE TRIM(cod_articolo) = TRIM(a.reference) AND rif_ordine = o.id_order), 
                                (SELECT (od3.product_quantity - (REPLACE(od3.cons,"0_",""))) 
                                FROM `'._DB_PREFIX_.'order_detail` od3 
                                JOIN `'._DB_PREFIX_.'orders` o3 ON o3.id_order = od3.id_order 
                                WHERE od3.product_reference = TRIM(a.reference)
                                    AND o3.id_order = o.id_order)
                            ),
                            od.product_quantity
                        ), 
                    1, 
                    0
                ),
                0
            ) as pronto
        ';
        
        $this->_join = '
            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (a.`id_product` = pl.`id_product`)
            LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (a.`id_manufacturer` = m.`id_manufacturer`)
            LEFT JOIN `'._DB_PREFIX_.'order_detail` od ON (od.product_id = a.id_product)
            JOIN `'._DB_PREFIX_.'orders` o ON (o.id_order = od.id_order) 
            JOIN `'._DB_PREFIX_.'cart` ca ON (o.id_cart = ca.id_cart) 
            JOIN `'._DB_PREFIX_.'customer` c ON (o.id_customer = c.id_customer) 
            JOIN `'._DB_PREFIX_.'order_history` oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM `'._DB_PREFIX_.'order_history` moh WHERE moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) 
            JOIN `'._DB_PREFIX_.'order_state` os ON (os.id_order_state = oh.id_order_state) 
            LEFT JOIN `'._DB_PREFIX_.'order_state_lang` osl ON (os.id_order_state = osl.id_order_state) 
        ';

        $this->_where = '
            AND pl.id_lang = '.(int)$this->context->language->id.'
            AND osl.id_lang = '.(int)$this->context->language->id.'
            AND ca.riferimento NOT LIKE "%BDL%"
            AND os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16 AND os.id_order_state != 20 AND os.id_order_state != 26 AND os.id_order_state != 29 AND os.id_order_state != 30 AND os.id_order_state != 31 AND os.id_order_state != 35 
        ';

        // Filtri (pulsanti in list header)
        $this->_where .= '
            '.(Tools::getIsset('parziali') && Tools::getValue('parziali') == 's' ? ' AND os.id_order_state = 15 ' : '').'
            '.(Tools::getIsset('prime') && Tools::getValue('prime') == 's' ? ' AND o.id_cart IN (SELECT id_cart FROM `'._DB_PREFIX_.'cart` WHERE name LIKE "%AMAZON PRIME%") ' : '').'
        ';

        // Prendo solo gli ordini parziali o non fatturati, non verificati e con qta. ord. > 0
        $this->_where .= '
            AND (
                os.id_order_state = 15
                OR o.id_order NOT IN (
                    SELECT rif_ordine FROM `'._DB_PREFIX_.'fattura` WHERE TRIM(cod_articolo) = TRIM(a.reference)
                )
            )
            AND o.id_order NOT IN (
                SELECT id_order 
                FROM `'._DB_PREFIX_.'order_history`
                WHERE (id_order_state = 4 OR id_order_state = 5 OR id_order_state = 6 OR id_order_state = 35 OR id_order_state = 7 OR id_order_state = 8 OR id_order_state = 14 OR id_order_state = 16 OR id_order_state = 20 OR id_order_state = 26 OR id_order_state = 29 OR id_order_state = 31)
            )
            AND IF(os.id_order_state = 15, 
                IF(o.id_order IN (
                    SELECT rif_ordine FROM `'._DB_PREFIX_.'fattura` WHERE TRIM(cod_articolo) = TRIM(a.reference)
                    ), 
                    od.product_quantity - (SELECT SUM(qt_sped) FROM `'._DB_PREFIX_.'fattura` WHERE TRIM(cod_articolo) = TRIM(a.reference) AND rif_ordine = o.id_order), 
                    (SELECT (od3.product_quantity - (REPLACE(od3.cons,"0_",""))) 
                    FROM `'._DB_PREFIX_.'order_detail` od3 
                    JOIN `'._DB_PREFIX_.'orders` o3 ON o3.id_order = od3.id_order 
                    WHERE od3.product_reference = TRIM(a.reference)
                        AND o3.id_order = o.id_order)
                ),
                od.product_quantity
            ) > 0
        ';

        $this->_orderBy = 'o.id_order';
        $this->_orderWay = 'ASC';

        $prodotti = Db::getInstance()->executeS('
            SELECT pl.name, CONCAT(p.reference, " - ", pl.name) as stringa_nome
            FROM '._DB_PREFIX_.'product p
            JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product = pl.id_product 
            WHERE pl.id_lang = '.$this->context->language->id.'
            ORDER BY name ASC
        ');

        $productsList = array();
        foreach($prodotti as $prodotto){
            $productsList = $productsList + array($prodotto['name'] => $prodotto['stringa_nome']);
        }

        $this->fields_list = array(
            'reference' => array(
                'title' => $this->l('Codice'), 
                'align' => 'left',
                'filter_key' => 'a!reference',
                'callback' => 'printCodice',
                'remove_onclick' => true
            ),
            'nome_prodotto' => array(
                'title' => $this->l('Prodotto'),
                'align' => 'left', 
                'type' => 'select',
                'list' => $productsList,
                'select2' => true, // parametro aggiunto in helpers/list/list_header.tpl
                'filter_key' => 'pl!name',
                'callback' => 'cleanName',
                'remove_onclick' => true
            ),
            'costruttore' => array(
                'title' => $this->l('Costruttore'),
                'align' => 'left', 
                'filter_key' => 'm!name',
                'remove_onclick' => true
            ),
            'ordinati' => array(
                'title' => $this->l('Qta. Ord.'),
                'align' => 'center',
                'tmpTableFilter' => true,
                'remove_onclick' => true
            ),
            'cliente' => array(
                'title' => $this->l('Cliente'),
                'align' => 'left',
                'tmpTableFilter' => true,
                'remove_onclick' => true
            ),
            'ordine' => array(
                'title' => $this->l('ID Ordine'),
                'align' => 'center',
                'filter_key' => 'o!id_order',
                'callback' => 'printOrdine',
                'remove_onclick' => true
            ),
            'stato' => array(
                'title' => $this->l('Stato'),
                'align' => 'left',
                'filter_key' => 'osl!name',
                'callback' => 'printStato',
                'remove_onclick' => true
            ),
            'pagamento' => array(
                'title' => $this->l('Pagamento'),
                'align' => 'left',
                'filter_key' => 'o!payment',
                'remove_onclick' => true
            ), 
            'disponibili' => array(
                'title' => $this->l('Qt. x cli.'),
                'align' => 'center',
                'tmpTableFilter' => true,
                'remove_onclick' => true
            ),
            'magazzino' => array(
                'title' => $this->l('Mag. EZ'),
                'align' => 'center',
                'filter_key' => 'a!stock_quantity',
                'remove_onclick' => true
            ),
            'astec' => array(
                'title' => $this->l('Astec'),
                'align' => 'center',
                'type' => 'select',
                'list' => array('Sì' => 'Sì', 'No' => 'No'),
                'filter_key' => 'astec',
                'tmpTableFilter' => true,
                'hint' => 'Sì = assistenza tecnica, telegestione/teleassistenza o installazione',
                'remove_onclick' => true
            ), 
            // Correggere: non funziona filter_key per la select (seleziona 'pronto' as pronto anche se non dovrebbe)
            'pronto' => array(
                'title' => $this->l('Pronto?'),
                'align' => 'center',
                //'type' => 'select',
                //'list' => array(1 => 'Sì', 0 => 'No'),
                //'filter_key' => 'pronto',
                'callback' => 'printPronto',
                'callback_color' => 'coloraPronto',
                'tmpTableFilter' => true,
                'remove_onclick' => true
            ),
        );

        $this->_use_found_rows = true;

        // Attivo l'attributo 'color' di tr per usare callback_color
        $this->colorOnBackground = true;

        parent::__construct();
    }

    public function renderList()
    {
        $this->addjQueryPlugin(array(
            'autocomplete',
            'select2',
        ));

        $this->addJS(_PS_JS_DIR_.'jquery/plugins/select2/select2_locale_'.$this->context->language->iso_code.'.js');
        
        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'select2_ready.js');

        return parent::renderList();
    }

    public function initToolbarTitle()
    {
        parent::initToolbarTitle();

        array_pop($this->toolbar_title);
        $this->toolbar_title[] = sprintf($this->l('Allocato'));
        
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }
    
    public function initToolbar()
    {
        parent::initToolbar();

        unset($this->toolbar_btn['new']); // Toglie il pulsante ADD NEW dalla toolbar
    }

    public function cleanName($value)
    {
        return preg_replace('/"/', "", $value);  
    }

    // Restituisce la differenza tra due date (di default in giorni)
    public function dateDifference($date_1, $date_2, $differenceFormat = '%a')
    {
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);
       
        $interval = date_diff($datetime1, $datetime2);
       
        return $interval->format($differenceFormat);
       
    }

    public function printCodice($value)
    {
        $id_product = Db::getInstance()->getValue('SELECT id_product FROM '._DB_PREFIX_.'product WHERE reference="'.$value.'"');
        $tooltip = Product::showProductTooltip($id_product);
        return '<a class="tooltip_prodotto" title="'.$tooltip.'" href="/ezadmin/index.php?controller=AdminProducts&id_product='.$id_product.'&updateproduct&token='.Tools::getAdminTokenLite('AdminProducts').'" target="_blank">'.$value.'</a>';
    }

    public function printOrdine($value, $tr)
    {
        $diff = $this->dateDifference($tr['data_ordine'], date('Y-m-d H:i:s'));

        if($diff > 2)
            $morethan2 = 1;
        else
            $morethan2 = 0;

        $stringa = '<a href="/ezadmin/index.php?controller=AdminOrders&id_order='.$value.'&vieworder&token='.Tools::getAdminTokenLite('AdminOrders').'" target="_blank">'.$value.'</a>';

        // In grassetto e più grande se l'ordine aspetta la spedizione da almeno due giorni
        return ($morethan2 == 1 ? '<strong style="font-size:120%">'.$stringa.'</strong>' : $stringa);
    }

    public function printStato($value, $tr)
    {
        $id_order = $tr['ordine'];

        $ast = Db::getInstance()->getRow('
            SELECT am.id_action, am.id_action_message, a.status, am.action_message, a.action_to, a.date_upd 
            FROM action_message am 
                JOIN action_thread a ON am.id_action = a.id_action 
            WHERE (a.status = "closed" OR a.status = "pending1" OR a.status = "pending2") 
                AND (am.action_message LIKE "%<strong>*** ASSISTENZA TECNICA ***</strong><br /><br />%" AND am.action_message LIKE "%'.$id_order.')%") 
            GROUP BY a.id_action
        ');

        $ast_status = '';
        if($ast['id_action'] > 0)	
        {
            $employee = Db::getInstance()->getValue('SELECT firstname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$ast['action_to']);
            if($ast['status'] == 'closed')
                $ast_status = '<em>Status assistenza tecnica</em>: <strong>chiuso</strong>';
            else if($ast['status'] == 'open')
                $ast_status = '<em>Status assistenza tecnica</em>: <strong>aperto</strong>';
            else
                $ast_status = '<em>Status assistenza tecnica</em>: <strong>in lavorazione</strong>';
            
            $ast_status .= '<br /><em>In carico a</em>: <strong>'.$employee.'</strong>';
        }

        return $value.' '.$ast_status;
    }

    public function printPronto($value)
    {
        return ($value ? '<i class="icon-check" style="color:green;"></i>' : '');
    }

    // Se l'ordine è pronto per la spedizione, coloro la riga
    public function coloraPronto($value)
    {
        return $value;
    }
}
