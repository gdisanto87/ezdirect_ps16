<?php
/*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @property Product $object
 */
class AdminProductsControllerCore extends AdminController
{
    /** @var int Max image size for upload
     * As of 1.5 it is recommended to not set a limit to max image size
     */
    protected $max_file_size = null;
    protected $max_image_size = null;

    protected $_category;
    /**
     * @var string name of the tab to display
     */
    protected $tab_display;
    protected $tab_display_module;

    /**
     * The order in the array decides the order in the list of tab. If an element's value is a number, it will be preloaded.
     * The tabs are preloaded from the smallest to the highest number.
     * @var array Product tabs.
     */
    protected $available_tabs = array();

    protected $default_tab = 'Informations';

    protected $available_tabs_lang = array();

    protected $position_identifier = 'id_product';

    protected $submitted_tabs;

    protected $id_current_category;

    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'product';
        $this->className = 'Product';
        $this->lang = true;
        $this->explicitSelect = true;
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'icon' => 'icon-trash',
                'confirm' => $this->l('Delete selected items?')
            )
        );
        if (!Tools::getValue('id_product')) {
            $this->multishop_context_group = false;
        }

        parent::__construct();

        $this->imageType = 'jpg';
        $this->_defaultOrderBy = 'position';
        $this->max_file_size = (int)(Configuration::get('PS_LIMIT_UPLOAD_FILE_VALUE') * 1000000);
        $this->max_image_size = (int)Configuration::get('PS_PRODUCT_PICTURE_MAX_SIZE');
        $this->allow_export = true;

        /* Override: aggiunte nuove tabs */

        // @since 1.5 : translations for tabs
        // Questo genera la lista a sinistra
        $this->available_tabs_lang = array(
            'Informations' => $this->l('Information'),
            'Pack' => $this->l('Pack'),
            'VirtualProduct' => $this->l('Virtual Product'),
            'Prices' => $this->l('Prices'),
            'Seo' => $this->l('SEO'),
            'Associations' => $this->l('Associations'),
            'Shipping' => $this->l('Shipping'),
            'Attachments' => $this->l('Attachments'),
            //'Suppliers' => $this->l('Suppliers'),
            'Warehouses' => $this->l('Warehouses'),
            'Bundles' => $this->l('Bundles'), // Kit
        );

        if(!Tools::getIsset('addproduct') && Tools::getIsset('updateproduct')) {
            $this->available_tabs_lang = array_merge($this->available_tabs_lang, array(
                'Combinations' => $this->l('Combinations'),
                'Quantities' => $this->l('Quantities'),
                'Images' => $this->l('Images'),
                'Features' => $this->l('Features'),
                'Customization' => $this->l('Customization'),
                'Storico' => $this->l('Storico'),
                'Esolver' => $this->l('eSolver'),
            ));
        }

        // Questo da' priorità alle singole tab laterali sx
        $this->available_tabs = array('Warehouses' => 14);
        if ($this->context->shop->getContext() != Shop::CONTEXT_GROUP) {
            $this->available_tabs = array_merge($this->available_tabs, array(
                'Informations' => 0,
                'Pack' => 7,
                'VirtualProduct' => 8,
                'Prices' => 1,
                'Seo' => 2,
                'Associations' => 3,
                'Shipping' => 4,
                'Attachments' => 12,
                //'Suppliers' => 13,
                'Bundles' => 15, // Kit
            ));
            
            if(!Tools::getIsset('addproduct') && Tools::getIsset('updateproduct')) {
                $this->available_tabs = array_merge($this->available_tabs, array(
                    'Combinations' => 5,
                    'Quantities' => 6,
                    'Images' => 9,
                    'Features' => 10,
                    'Customization' => 11,
                    'Storico' => 17, 
                    'Esolver' => 16,
                ));
            }
        }

        // Sort the tabs that need to be preloaded by their priority number
        asort($this->available_tabs, SORT_NUMERIC);

        /* Adding tab if modules are hooked */
        $modules_list = Hook::getHookModuleExecList('displayAdminProductsExtra');
        if (is_array($modules_list) && count($modules_list) > 0) {
            foreach ($modules_list as $m) {
                $this->available_tabs['Module'.ucfirst($m['module'])] = 23;
                $this->available_tabs_lang['Module'.ucfirst($m['module'])] = Module::getModuleName($m['module']);
            }
        }

        if (Tools::getValue('reset_filter_category')) {
            $this->context->cookie->id_category_products_filter = false;
        }
        if (Shop::isFeatureActive() && $this->context->cookie->id_category_products_filter) {
            $category = new Category((int)$this->context->cookie->id_category_products_filter);
            if (!$category->inShop()) {
                $this->context->cookie->id_category_products_filter = false;
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminProducts'));
            }
        }
        /* Join categories table */
        if ($id_category = (int)Tools::getValue('productFilter_cl!name')) {
            $this->_category = new Category((int)$id_category);
            $_POST['productFilter_cl!name'] = $this->_category->name[$this->context->language->id];
        } else {
            if ($id_category = (int)Tools::getValue('id_category')) {
                $this->id_current_category = $id_category;
                $this->context->cookie->id_category_products_filter = $id_category;
            } elseif ($id_category = $this->context->cookie->id_category_products_filter) {
                $this->id_current_category = $id_category;
            }
            if ($this->id_current_category) {
                $this->_category = new Category((int)$this->id_current_category);
            } else {
                $this->_category = new Category();
            }
        }

        $join_category = false;
        if (Validate::isLoadedObject($this->_category) && empty($this->_filter)) {
            $join_category = true;
        }

        $this->_join .= '
		LEFT JOIN `'._DB_PREFIX_.'stock_available` sav ON (sav.`id_product` = a.`id_product` AND sav.`id_product_attribute` = 0
		'.StockAvailable::addSqlShopRestriction(null, null, 'sav').') ';

        $alias = 'sa';
        $alias_image = 'image_shop';

        $id_shop = Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP? (int)$this->context->shop->id : 'a.id_shop_default';
        $this->_join .= ' JOIN `'._DB_PREFIX_.'product_shop` sa ON (a.`id_product` = sa.`id_product` AND sa.id_shop = '.$id_shop.')
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON ('.$alias.'.`id_category_default` = cl.`id_category` AND b.`id_lang` = cl.`id_lang` AND cl.id_shop = '.$id_shop.')
				LEFT JOIN `'._DB_PREFIX_.'shop` shop ON (shop.id_shop = '.$id_shop.')
				LEFT JOIN `'._DB_PREFIX_.'image_shop` image_shop ON (image_shop.`id_product` = a.`id_product` AND image_shop.`cover` = 1 AND image_shop.id_shop = '.$id_shop.')
				LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_image` = image_shop.`id_image`)
				LEFT JOIN `'._DB_PREFIX_.'product_download` pd ON (pd.`id_product` = a.`id_product` AND pd.`active` = 1)';

        $this->_select .= 'shop.`name` AS `shopname`, a.`id_shop_default`, ';
        $this->_select .= $alias_image.'.`id_image` AS `id_image`, cl.`name` AS `name_category`, '.$alias.'.`price`, 0 AS `price_final`, a.`is_virtual`, pd.`nb_downloadable`, sav.`quantity` AS `sav_quantity`, '.$alias.'.`active`, IF(sav.`quantity`<=0, 1, 0) AS `badge_danger`';

        if ($join_category) {
            $this->_join .= ' INNER JOIN `'._DB_PREFIX_.'category_product` cp ON (cp.`id_product` = a.`id_product` AND cp.`id_category` = '.(int)$this->_category->id.') ';
            $this->_select .= ' , cp.`position`, ';
        }
        $this->_use_found_rows = false;
        $this->_group = '';

        /* OVERRIDE */
        $this->_where = ' AND a.active != 2 ';
        /* FINE OVERRIDE */

        $this->fields_list = array();
        $this->fields_list['id_product'] = array(
            'title' => $this->l('ID'),
            'align' => 'center',
            'class' => 'fixed-width-xs',
            'type' => 'int'
        );
        $this->fields_list['image'] = array(
            'title' => $this->l('Image'),
            'align' => 'center',
            'image' => 'p',
            'orderby' => false,
            'filter' => false,
            'search' => false
        );
        $this->fields_list['name'] = array(
            'title' => $this->l('Name'),
            'filter_key' => 'b!name'
        );
        $this->fields_list['reference'] = array(
            'title' => $this->l('Reference'),
            'align' => 'left',
        );

        if (Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_SHOP) {
            $this->fields_list['shopname'] = array(
                'title' => $this->l('Default shop'),
                'filter_key' => 'shop!name',
            );
        } else {
            $this->fields_list['name_category'] = array(
                'title' => $this->l('Category'),
                'filter_key' => 'cl!name',
            );
        }
        $this->fields_list['price'] = array(
            'title' => $this->l('Base price'),
            'type' => 'price',
            'align' => 'text-right',
            'filter_key' => 'a!price'
        );
        /*$this->fields_list['price_final'] = array(
            'title' => $this->l('Final price'),
            'type' => 'price',
            'align' => 'text-right',
            'havingFilter' => true,
            'orderby' => false,
            'search' => false
        );*/
        /* OVERRIDE */
        $this->fields_list['listino'] = array(
            'title' => $this->l('Listino'),
            'type' => 'price',
            'align' => 'text-right',
            'search' => false
        );
        $this->fields_list['wholesale_price'] = array(
            'title' => $this->l('Acquisto'),
            'type' => 'price',
            'align' => 'text-right',
            'filter_key' => 'a!wholesale_price',
            'search' => false
        );
        $this->fields_list['stock_quantity'] = array(
            'title' => $this->l('EZ'),
            'align' => 'text-right',
            'hint' => 'Prodotti disponibili a magazzino Ezdirect',
        );
        $this->fields_list['ordinato_quantity'] = array(
            'title' => $this->l('OEZ'),
            'align' => 'text-right',
            'hint' => 'Prodotti ordinati al fornitore',
        );
        $this->fields_list['supplier_quantity'] = array(
            'title' => $this->l('AN'),
            'align' => 'text-right',
            'hint' => 'Prodotti disponibili su Allnet',
        );
        $this->fields_list['arrivo_quantity'] = array(
            'title' => $this->l('AAN'),
            'align' => 'text-right',
            'hint' => 'Prodotti in arrivo su Allnet',
        );
        $this->fields_list['condition'] = array(
            'title' => $this->l('Cond.'),
            'align' => 'text-right',
            'filter_key' => 'a!condition',
        );
        /* FINE OVERRIDE */

        /*if (Configuration::get('PS_STOCK_MANAGEMENT')) {
            $this->fields_list['sav_quantity'] = array(
                'title' => $this->l('Quantity'),
                'type' => 'int',
                'align' => 'text-right',
                'filter_key' => 'sav!quantity',
                'orderby' => true,
                'badge_danger' => true,
                //'hint' => $this->l('This is the quantity available in the current shop/group.'),
            );
        }*/

        $this->fields_list['active'] = array(
            'title' => $this->l('Status'),
            'active' => 'status',
            'filter_key' => $alias.'!active',
            'align' => 'text-center',
            'type' => 'bool',
            'class' => 'fixed-width-sm',
            'orderby' => false
        );

        if ($join_category && (int)$this->id_current_category) {
            $this->fields_list['position'] = array(
                'title' => $this->l('Position'),
                'filter_key' => 'cp!position',
                'align' => 'center',
                'position' => 'position'
            );
        }
    }

    public static function getQuantities($echo, $tr)
    {
        if ((int)$tr['is_virtual'] == 1 && $tr['nb_downloadable'] == 0) {
            return '&infin;';
        } else {
            return $echo;
        } 
    }

    public function setMedia()
    {
        parent::setMedia();

        $bo_theme = ((Validate::isLoadedObject($this->context->employee)
            && $this->context->employee->bo_theme) ? $this->context->employee->bo_theme : 'default');

        if (!file_exists(_PS_BO_ALL_THEMES_DIR_.$bo_theme.DIRECTORY_SEPARATOR
            .'template')) {
            $bo_theme = 'default';
        }

        $this->addJs(__PS_BASE_URI__.$this->admin_webpath.'/themes/'.$bo_theme.'/js/jquery.iframe-transport.js');
        $this->addJs(__PS_BASE_URI__.$this->admin_webpath.'/themes/'.$bo_theme.'/js/jquery.fileupload.js');
        $this->addJs(__PS_BASE_URI__.$this->admin_webpath.'/themes/'.$bo_theme.'/js/jquery.fileupload-process.js');
        $this->addJs(__PS_BASE_URI__.$this->admin_webpath.'/themes/'.$bo_theme.'/js/jquery.fileupload-validate.js');
        $this->addJs(__PS_BASE_URI__.'js/vendor/spin.js');
        $this->addJs(__PS_BASE_URI__.'js/vendor/ladda.js');
    }

    protected function _cleanMetaKeywords($keywords)
    {
        if (!empty($keywords) && $keywords != '') {
            $out = array();
            $words = explode(',', $keywords);
            foreach ($words as $word_item) {
                $word_item = trim($word_item);
                if (!empty($word_item) && $word_item != '') {
                    $out[] = $word_item;
                }
            }
            return ((count($out) > 0) ? implode(',', $out) : '');
        } else {
            return '';
        }
    }

    /**
     * @param Product|ObjectModel $object
     * @param string              $table
     */
    protected function copyFromPost(&$object, $table)
    {
        parent::copyFromPost($object, $table);
        if (get_class($object) != 'Product') {
            return;
        }

        /* Additional fields */
        foreach (Language::getIDs(false) as $id_lang) {
            if (isset($_POST['meta_keywords_'.$id_lang])) {
                $_POST['meta_keywords_'.$id_lang] = $this->_cleanMetaKeywords(Tools::strtolower($_POST['meta_keywords_'.$id_lang]));
                // preg_replace('/ *,? +,* /', ',', strtolower($_POST['meta_keywords_'.$id_lang]));
                $object->meta_keywords[$id_lang] = $_POST['meta_keywords_'.$id_lang];
            }
        }
        $_POST['width'] = empty($_POST['width']) ? '0' : str_replace(',', '.', $_POST['width']);
        $_POST['height'] = empty($_POST['height']) ? '0' : str_replace(',', '.', $_POST['height']);
        $_POST['depth'] = empty($_POST['depth']) ? '0' : str_replace(',', '.', $_POST['depth']);
        $_POST['weight'] = empty($_POST['weight']) ? '0' : str_replace(',', '.', $_POST['weight']);

        if (Tools::getIsset('unit_price') != null) {
            $object->unit_price = str_replace(',', '.', Tools::getValue('unit_price'));
        }
        if (Tools::getIsset('ecotax') != null) {
            $object->ecotax = str_replace(',', '.', Tools::getValue('ecotax'));
        }

        if ($this->isTabSubmitted('Informations')) {
            if ($this->checkMultishopBox('available_for_order', $this->context)) {
                $object->available_for_order = (int)Tools::getValue('available_for_order');
            }

            if ($this->checkMultishopBox('show_price', $this->context)) {
                $object->show_price = $object->available_for_order ? 1 : (int)Tools::getValue('show_price');
            }

            if ($this->checkMultishopBox('online_only', $this->context)) {
                $object->online_only = (int)Tools::getValue('online_only');
            }
        }
        if ($this->isTabSubmitted('Prices')) {
            $object->on_sale = (int)Tools::getValue('on_sale');
        }

        $object->fuori_produzione = (int)Tools::getValue('fuori_produzione');
		$object->acquisto_in_dollari = (int)Tools::getValue('acquisto_in_dollari');
		$object->blocco_prezzi = (int)Tools::getValue('blocco_prezzi');
		$object->login_for_offer = (int)Tools::getValue('login_for_offer');
		$object->noindex = (int)Tools::getValue('noindex');
		$object->prodotto_con_canone = (int)Tools::getValue('prodotto_con_canone');
		$object->ottieni_miglior_prezzo_flag = (int)Tools::getValue('ottieni_miglior_prezzo_flag');
    }

    public function checkMultishopBox($field, $context = null)
    {
        static $checkbox = null;
        static $shop_context = null;

        if ($context == null && $shop_context == null) {
            $context = Context::getContext();
        }

        if ($shop_context == null) {
            $shop_context = $context->shop->getContext();
        }

        if ($checkbox == null) {
            $checkbox = Tools::getValue('multishop_check', array());
        }

        if ($shop_context == Shop::CONTEXT_SHOP) {
            return true;
        }

        if (isset($checkbox[$field]) && $checkbox[$field] == 1) {
            return true;
        }

        return false;
    }

    public function getList($id_lang, $orderBy = null, $orderWay = null, $start = 0, $limit = null, $id_lang_shop = null)
    {
        $orderByPriceFinal = (empty($orderBy) ? ($this->context->cookie->__get($this->table.'Orderby') ? $this->context->cookie->__get($this->table.'Orderby') : 'id_'.$this->table) : $orderBy);
        $orderWayPriceFinal = (empty($orderWay) ? ($this->context->cookie->__get($this->table.'Orderway') ? $this->context->cookie->__get($this->table.'Orderby') : 'ASC') : $orderWay);
        if ($orderByPriceFinal == 'price_final') {
            $orderBy = 'id_'.$this->table;
            $orderWay = 'ASC';
        }
        parent::getList($id_lang, $orderBy, $orderWay, $start, $limit, $this->context->shop->id);

        /* update product quantity with attributes ...*/
        $nb = count($this->_list);
        if ($this->_list) {
            $context = $this->context->cloneContext();
            $context->shop = clone($context->shop);
            /* update product final price */
            for ($i = 0; $i < $nb; $i++) {
                if (Context::getContext()->shop->getContext() != Shop::CONTEXT_SHOP) {
                    $context->shop = new Shop((int)$this->_list[$i]['id_shop_default']);
                }

                // convert price with the currency from context
                $this->_list[$i]['price'] = Tools::convertPrice($this->_list[$i]['price'], $this->context->currency, true, $this->context);
                $this->_list[$i]['price_tmp'] = Product::getPriceStatic($this->_list[$i]['id_product'], true, null,
                    (int)Configuration::get('PS_PRICE_DISPLAY_PRECISION'), null, false, true, 1, true, null, null, null, $nothing, true, true,
                    $context);
            }
        }

        if ($orderByPriceFinal == 'price_final') {
            if (strtolower($orderWayPriceFinal) == 'desc') {
                uasort($this->_list, 'cmpPriceDesc');
            } else {
                uasort($this->_list, 'cmpPriceAsc');
            }
        }
        for ($i = 0; $this->_list && $i < $nb; $i++) {
            $this->_list[$i]['price_final'] = $this->_list[$i]['price_tmp'];
            unset($this->_list[$i]['price_tmp']);
        }
    }

    protected function loadObject($opt = false)
    {
        $result = parent::loadObject($opt);
        if ($result && Validate::isLoadedObject($this->object)) {
            if (Shop::getContext() == Shop::CONTEXT_SHOP && Shop::isFeatureActive() && !$this->object->isAssociatedToShop()) {
                $default_product = new Product((int)$this->object->id, false, null, (int)$this->object->id_shop_default);
                $def = ObjectModel::getDefinition($this->object);
                foreach ($def['fields'] as $field_name => $row) {
                    if (is_array($default_product->$field_name)) {
                        foreach ($default_product->$field_name as $key => $value) {
                            $this->object->{$field_name}[$key] = $value;
                        }
                    } else {
                        $this->object->$field_name = $default_product->$field_name;
                    }
                }
            }
            $this->object->loadStockData();
        }
        return $result;
    }

    public function ajaxProcessGetCategoryTree()
    {
        $category = Tools::getValue('category', Category::getRootCategory()->id);
        $full_tree = Tools::getValue('fullTree', 0);
        $use_check_box = Tools::getValue('useCheckBox', 1);
        $selected = Tools::getValue('selected', array());
        $id_tree = Tools::getValue('type');
        $input_name = str_replace(array('[', ']'), '', Tools::getValue('inputName', null));

        $tree = new HelperTreeCategories('subtree_associated_categories');
        $tree->setTemplate('subtree_associated_categories.tpl')
            ->setUseCheckBox($use_check_box)
            ->setUseSearch(true)
            ->setIdTree($id_tree)
            ->setSelectedCategories($selected)
            ->setFullTree($full_tree)
            ->setChildrenOnly(true)
            ->setNoJS(true)
            ->setRootCategory($category);

        if ($input_name) {
            $tree->setInputName($input_name);
        }

        die($tree->render());
    }

    public function ajaxProcessGetCountriesOptions()
    {
        if (!$res = Country::getCountriesByIdShop((int)Tools::getValue('id_shop'), (int)$this->context->language->id)) {
            return;
        }

        $tpl = $this->createTemplate('specific_prices_shop_update.tpl');
        $tpl->assign(array(
            'option_list' => $res,
            'key_id' => 'id_country',
            'key_value' => 'name'
            )
        );

        $this->content = $tpl->fetch();
    }

    public function ajaxProcessGetCurrenciesOptions()
    {
        if (!$res = Currency::getCurrenciesByIdShop((int)Tools::getValue('id_shop'))) {
            return;
        }

        $tpl = $this->createTemplate('specific_prices_shop_update.tpl');
        $tpl->assign(array(
            'option_list' => $res,
            'key_id' => 'id_currency',
            'key_value' => 'name'
            )
        );

        $this->content = $tpl->fetch();
    }

    public function ajaxProcessGetGroupsOptions()
    {
        if (!$res = Group::getGroups((int)$this->context->language->id, (int)Tools::getValue('id_shop'))) {
            return;
        }

        $tpl = $this->createTemplate('specific_prices_shop_update.tpl');
        $tpl->assign(array(
            'option_list' => $res,
            'key_id' => 'id_group',
            'key_value' => 'name'
            )
        );

        $this->content = $tpl->fetch();
    }

    public function processDeleteVirtualProduct()
    {
        if (!($id_product_download = ProductDownload::getIdFromIdProduct((int)Tools::getValue('id_product')))) {
            $this->errors[] = Tools::displayError('Cannot retrieve file');
        } else {
            $product_download = new ProductDownload((int)$id_product_download);

            if (!$product_download->deleteFile((int)$id_product_download)) {
                $this->errors[] = Tools::displayError('Cannot delete file');
            } else {
                $this->redirect_after = self::$currentIndex.'&id_product='.(int)Tools::getValue('id_product').'&updateproduct&key_tab=VirtualProduct&conf=1&token='.$this->token;
            }
        }

        $this->display = 'edit';
        $this->tab_display = 'VirtualProduct';
    }

    public function ajaxProcessAddAttachment()
    {
        if ($this->tabAccess['edit'] === '0') {
            return die(Tools::jsonEncode(array('error' => $this->l('You do not have the right permission'))));
        }
        if (isset($_FILES['attachment_file'])) {
            if ((int)$_FILES['attachment_file']['error'] === 1) {
                $_FILES['attachment_file']['error'] = array();

                $max_upload = (int)ini_get('upload_max_filesize');
                $max_post = (int)ini_get('post_max_size');
                $upload_mb = min($max_upload, $max_post);
                $_FILES['attachment_file']['error'][] = sprintf(
                    $this->l('File %1$s exceeds the size allowed by the server. The limit is set to %2$d MB.'),
                    '<b>'.$_FILES['attachment_file']['name'].'</b> ',
                    '<b>'.$upload_mb.'</b>'
                );
            }

            $_FILES['attachment_file']['error'] = array();

            $is_attachment_name_valid = false;
            $attachment_names = Tools::getValue('attachment_name');
            $attachment_descriptions = Tools::getValue('attachment_description');

            if (!isset($attachment_names) || !$attachment_names) {
                $attachment_names = array();
            }

            if (!isset($attachment_descriptions) || !$attachment_descriptions) {
                $attachment_descriptions = array();
            }

            foreach ($attachment_names as $lang => $name) {
                $language = Language::getLanguage((int)$lang);

                if (Tools::strlen($name) > 0) {
                    $is_attachment_name_valid = true;
                }

                if (!Validate::isGenericName($name)) { // Ci sono caratteri non validi nel nome del file allegato
                    $_FILES['attachment_file']['error'][] = sprintf(Tools::displayError('Invalid name for %s language'), $language['name']);
                } elseif (Tools::strlen($name) > 32) { // Nome allegato troppo lungo
                    $_FILES['attachment_file']['error'][] = sprintf(Tools::displayError('The name for %1s language is too long (%2d chars max).'), $language['name'], 32);
                }
            }

            foreach ($attachment_descriptions as $lang => $description) {
                $language = Language::getLanguage((int)$lang);

                if (!Validate::isCleanHtml($description)) { // Ci sono caratteri non validi nella descrizione del file allegato
                    $_FILES['attachment_file']['error'][] = sprintf(Tools::displayError('Invalid description for %s language'), $language['name']);
                }
            }

            if (!$is_attachment_name_valid) { // Nome allegato non valido o non inserito
                $_FILES['attachment_file']['error'][] = Tools::displayError('An attachment name is required.');
            }

            if (empty($_FILES['attachment_file']['error'])) {
                if (is_uploaded_file($_FILES['attachment_file']['tmp_name'])) {
                    if ($_FILES['attachment_file']['size'] > (Configuration::get('PS_ATTACHMENT_MAXIMUM_SIZE') * 1024 * 1024)) {
                        $_FILES['attachment_file']['error'][] = sprintf( // File allegato troppo grande
                            $this->l('The file is too large. Maximum size allowed is: %1$d kB. The file you are trying to upload is %2$d kB.'),
                            (Configuration::get('PS_ATTACHMENT_MAXIMUM_SIZE') * 1024),
                            number_format(($_FILES['attachment_file']['size'] / 1024), 2, '.', '')
                        );
                    } else {
                        do {
                            $uniqid = sha1(microtime());
                        } while (file_exists(_PS_DOWNLOAD_DIR_.$uniqid));
                        if (!copy($_FILES['attachment_file']['tmp_name'], _PS_DOWNLOAD_DIR_.$uniqid)) {
                            $_FILES['attachment_file']['error'][] = $this->l('File copy failed');
                        }
                        @unlink($_FILES['attachment_file']['tmp_name']);
                    }
                } else {
                    $_FILES['attachment_file']['error'][] = Tools::displayError('The file is missing.');
                }

                if (empty($_FILES['attachment_file']['error']) && isset($uniqid)) {
                    $attachment = new Attachment();

                    foreach ($attachment_names as $lang => $name) {
                        $attachment->name[(int)$lang] = $name;
                    }

                    foreach ($attachment_descriptions as $lang => $description) {
                        $attachment->description[(int)$lang] = $description;
                    }

                    $attachment->file = $uniqid;
                    $attachment->mime = $_FILES['attachment_file']['type'];
                    $attachment->file_name = $_FILES['attachment_file']['name'];

                    if (empty($attachment->mime) || Tools::strlen($attachment->mime) > 128) {
                        $_FILES['attachment_file']['error'][] = Tools::displayError('Invalid file extension');
                    }
                    if (!Validate::isGenericName($attachment->file_name)) {
                        $_FILES['attachment_file']['error'][] = Tools::displayError('Invalid file name');
                    }
                    if (Tools::strlen($attachment->file_name) > 128) {
                        $_FILES['attachment_file']['error'][] = Tools::displayError('The file name is too long.');
                    }
                    if (empty($this->errors)) {
                        $res = $attachment->add();
                        if (!$res) {
                            $_FILES['attachment_file']['error'][] = Tools::displayError('This attachment was unable to be loaded into the database.');
                        } else {
                            $_FILES['attachment_file']['id_attachment'] = $attachment->id;
                            $_FILES['attachment_file']['filename'] = $attachment->name[$this->context->employee->id_lang];
                            $id_product = (int)Tools::getValue($this->identifier);

                            /* OVERRIDE -> controllare se va implementato o no! */
                            /*$array_degli_allegati = array();
							$rowalleg = Db::getInstance()->getRow("SELECT id_attachment FROM "._DB_PREFIX_."product_attachment WHERE id_product = ".$id_product);
							$array_degli_allegati[] = $rowalleg['id_attachment'];
							$array_degli_allegati[] = $id_attachment;
                            Attachment::attachToProduct($id_product, $array_degli_allegati);*/
                            /* FINE OVERRIDE */

                            $res = $attachment->attachProduct($id_product);
                            if (!$res) {
                                $_FILES['attachment_file']['error'][] = Tools::displayError('We were unable to associate this attachment to a product.');
                            }
                        }
                    } else {
                        $_FILES['attachment_file']['error'][] = Tools::displayError('Invalid file');
                    }
                }
            }

            die(Tools::jsonEncode($_FILES));
        }
    }


    /**
     * Attach an existing attachment to the product
     *
     * @return void
     */
    public function processAttachments()
    {
        if ($id = (int)Tools::getValue($this->identifier)) {
            $attachments = trim(Tools::getValue('arrayAttachments'), ',');
            $attachments = explode(',', $attachments);
            if (!Attachment::attachToProduct($id, $attachments)) {
                $this->errors[] = Tools::displayError('An error occurred while saving product attachments.');
            }
        }
    }

    public function processDuplicate()
    {
        if (Validate::isLoadedObject($product = new Product((int)Tools::getValue('id_product')))) {
            $id_product_old = $product->id;
            if (empty($product->price) && Shop::getContext() == Shop::CONTEXT_GROUP) {
                $shops = ShopGroup::getShopsFromGroup(Shop::getContextShopGroupID());
                foreach ($shops as $shop) {
                    if ($product->isAssociatedToShop($shop['id_shop'])) {
                        $product_price = new Product($id_product_old, false, null, $shop['id_shop']);
                        $product->price = $product_price->price;
                    }
                }
            }
            unset($product->id);
            unset($product->id_product);
            $product->indexed = 0;
            $product->active = 0;

            /* OVERRIDE */
            $product->price = 0;
            $product->listino = 0;
            $product->wholesale_price = 0;
            $product->sconto_acquisto_1 = 0;
            $product->sconto_acquisto_2 = 0;
            $product->sconto_acquisto_3 = 0;
            $product->scontolistinovendita = 0;
            $product->reference = "";
            $product->supplier_reference = '';
            $product->ean13 = '';
            $product->description_amazon = '';
            $product->asin = '';
            $product->upc = '';
            $product->quantity = 0;
            $product->stock_quantity = 0;
            $product->arrivo_quantity = 0;
            $product->supplier_quantity = 0;
            $product->esprinet_quantity = 0;
            $product->attiva_quantity = 0;
            $product->ingram_quantity = 0;
            $product->itancia_quantity = 0;
            $product->scorta_minima = 0;
            $product->scorta_minima_amazon = 0;
            $product->ordinato_quantity = 0;
            $product->impegnato_quantity = 0;

            $product->data_arrivo = '0000-00-00';
            $product->data_arrivo_ordinato = '';
            $product->data_arrivo_esprinet = '0000-00-00';
            $product->data_arrivo_intracom = '0000-00-00';
            $product->data_arrivo_techdata = '0000-00-00';
            /* FINE OVERRIDE */

            if ($product->add()
            && Category::duplicateProductCategories($id_product_old, $product->id)
            && Product::duplicateSuppliers($id_product_old, $product->id)
            && ($combination_images = Product::duplicateAttributes($id_product_old, $product->id)) !== false
            && GroupReduction::duplicateReduction($id_product_old, $product->id)
            && Product::duplicateAccessories($id_product_old, $product->id)
            && Product::duplicateFeatures($id_product_old, $product->id)
            && Pack::duplicate($id_product_old, $product->id)
            && Product::duplicateCustomizationFields($id_product_old, $product->id)
            && Product::duplicateTags($id_product_old, $product->id)
            && Product::duplicateDownload($id_product_old, $product->id)) {

                // Override
                Product::duplicateComparaprezzi($id_product_old, $product->id);

                if ($product->hasAttributes()) {
                    Product::updateDefaultAttribute($product->id);
                } else {
                    //Product::duplicateSpecificPrices($id_product_old, $product->id);
                }

                if (!Tools::getValue('noimage') && !Image::duplicateProductImages($id_product_old, $product->id, $combination_images)) {
                    $this->errors[] = Tools::displayError('An error occurred while copying images.');
                } else {
                    Hook::exec('actionProductAdd', array('id_product' => (int)$product->id, 'product' => $product));
                    if (in_array($product->visibility, array('both', 'search')) && Configuration::get('PS_SEARCH_INDEXATION')) {
                        Search::indexation(false, $product->id);
                    }
                    $this->redirect_after = self::$currentIndex.(Tools::getIsset('id_category') ? '&id_category='.(int)Tools::getValue('id_category') : '').'&conf=19&token='.$this->token;
                }
            } else {
                $this->errors[] = Tools::displayError('An error occurred while creating an object.');
            }
        }
    }

    public function processDelete()
    {
        if (Validate::isLoadedObject($object = $this->loadObject()) && isset($this->fieldImageSettings)) {
            /** @var Product $object */
            // check if request at least one object with noZeroObject
            if (isset($object->noZeroObject) && count($taxes = call_user_func(array($this->className, $object->noZeroObject))) <= 1) {
                $this->errors[] = Tools::displayError('You need at least one object.').' <b>'.$this->table.'</b><br />'.Tools::displayError('You cannot delete all of the items.');
            } else {
                /*
                 * @since 1.5.0
                 * It is NOT possible to delete a product if there are currently:
                 * - physical stock for this product
                 * - supply order(s) for this product
                 */
                if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && $object->advanced_stock_management) {
                    $stock_manager = StockManagerFactory::getManager();
                    $physical_quantity = $stock_manager->getProductPhysicalQuantities($object->id, 0);
                    $real_quantity = $stock_manager->getProductRealQuantities($object->id, 0);
                    if ($physical_quantity > 0 || $real_quantity > $physical_quantity) {
                        $this->errors[] = Tools::displayError('You cannot delete this product because there is physical stock left.');
                    }
                }

                if (!count($this->errors)) {
                    if ($object->delete()) {
                        $id_category = (int)Tools::getValue('id_category');
                        $category_url = empty($id_category) ? '' : '&id_category='.(int)$id_category;
                        PrestaShopLogger::addLog(sprintf($this->l('%s deletion', 'AdminTab', false, false), $this->className), 1, null, $this->className, (int)$object->id, true, (int)$this->context->employee->id);
                        $this->redirect_after = self::$currentIndex.'&conf=1&token='.$this->token.$category_url;
                    } else {
                        $this->errors[] = Tools::displayError('An error occurred during deletion.');
                    }
                }
            }
        } else {
            $this->errors[] = Tools::displayError('An error occurred while deleting the object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
        }
    }

    public function processImage()
    {
        $id_image = (int)Tools::getValue('id_image');
        $image = new Image((int)$id_image);
        if (Validate::isLoadedObject($image)) {
            /* Update product image/legend */
            // @todo : move in processEditProductImage
            if (Tools::getIsset('editImage')) {
                if ($image->cover) {
                    $_POST['cover'] = 1;
                }

                $_POST['id_image'] = $image->id;
            } elseif (Tools::getIsset('coverImage')) {
                /* Choose product cover image */
                Image::deleteCover($image->id_product);
                $image->cover = 1;
                if (!$image->update()) {
                    $this->errors[] = Tools::displayError('You cannot change the product\'s cover image.');
                } else {
                    $productId = (int)Tools::getValue('id_product');
                    @unlink(_PS_TMP_IMG_DIR_.'product_'.$productId.'.jpg');
                    @unlink(_PS_TMP_IMG_DIR_.'product_mini_'.$productId.'_'.$this->context->shop->id.'.jpg');
                    $this->redirect_after = self::$currentIndex.'&id_product='.$image->id_product.'&id_category='.(Tools::getIsset('id_category') ? '&id_category='.(int)Tools::getValue('id_category') : '').'&action=Images&addproduct'.'&token='.$this->token;
                }
            } elseif (Tools::getIsset('imgPosition') && Tools::getIsset('imgDirection')) {
                /* Choose product image position */
                $image->updatePosition(Tools::getValue('imgDirection'), Tools::getValue('imgPosition'));
                $this->redirect_after = self::$currentIndex.'&id_product='.$image->id_product.'&id_category='.(Tools::getIsset('id_category') ? '&id_category='.(int)Tools::getValue('id_category') : '').'&add'.$this->table.'&action=Images&token='.$this->token;
            }
        } else {
            $this->errors[] = Tools::displayError('The image could not be found. ');
        }
    }

    protected function processBulkDelete()
    {
        if ($this->tabAccess['delete'] === '1') {
            if (is_array($this->boxes) && !empty($this->boxes)) {
                $object = new $this->className();

                if (isset($object->noZeroObject) &&
                    // Check if all object will be deleted
                    (count(call_user_func(array($this->className, $object->noZeroObject))) <= 1 || count($_POST[$this->table.'Box']) == count(call_user_func(array($this->className, $object->noZeroObject))))) {
                    $this->errors[] = Tools::displayError('You need at least one object.').' <b>'.$this->table.'</b><br />'.Tools::displayError('You cannot delete all of the items.');
                } else {
                    $success = 1;
                    $products = Tools::getValue($this->table.'Box');
                    if (is_array($products) && ($count = count($products))) {
                        // Deleting products can be quite long on a cheap server. Let's say 1.5 seconds by product (I've seen it!).
                        if (intval(ini_get('max_execution_time')) < round($count * 1.5)) {
                            ini_set('max_execution_time', round($count * 1.5));
                        }

                        if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT')) {
                            $stock_manager = StockManagerFactory::getManager();
                        }

                        foreach ($products as $id_product) {
                            $product = new Product((int)$id_product);
                            /*
                             * @since 1.5.0
                             * It is NOT possible to delete a product if there are currently:
                             * - physical stock for this product
                             * - supply order(s) for this product
                             */
                            if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && $product->advanced_stock_management) {
                                $physical_quantity = $stock_manager->getProductPhysicalQuantities($product->id, 0);
                                $real_quantity = $stock_manager->getProductRealQuantities($product->id, 0);
                                if ($physical_quantity > 0 || $real_quantity > $physical_quantity) {
                                    $this->errors[] = sprintf(Tools::displayError('You cannot delete the product #%d because there is physical stock left.'), $product->id);
                                }
                            }
                            if (!count($this->errors)) {
                                if ($product->delete()) {
                                    PrestaShopLogger::addLog(sprintf($this->l('%s deletion', 'AdminTab', false, false), $this->className), 1, null, $this->className, (int)$product->id, true, (int)$this->context->employee->id);
                                } else {
                                    $success = false;
                                }
                            } else {
                                $success = 0;
                            }
                        }
                    }

                    if ($success) {
                        $id_category = (int)Tools::getValue('id_category');
                        $category_url = empty($id_category) ? '' : '&id_category='.(int)$id_category;
                        $this->redirect_after = self::$currentIndex.'&conf=2&token='.$this->token.$category_url;
                    } else {
                        $this->errors[] = Tools::displayError('An error occurred while deleting this selection.');
                    }
                }
            } else {
                $this->errors[] = Tools::displayError('You must select at least one element to delete.');
            }
        } else {
            $this->errors[] = Tools::displayError('You do not have permission to delete this.');
        }
    }

    public function processProductAttribute()
    {
        // Don't process if the combination fields have not been submitted
        if (!Combination::isFeatureActive() || !Tools::getValue('attribute_combination_list')) {
            return;
        }

        if (Validate::isLoadedObject($product = $this->object)) {
            if ($this->isProductFieldUpdated('attribute_price') && (!Tools::getIsset('attribute_price') || Tools::getIsset('attribute_price') == null)) {
                $this->errors[] = Tools::displayError('The price attribute is required.');
            }
            if (!Tools::getIsset('attribute_combination_list') || Tools::isEmpty(Tools::getValue('attribute_combination_list'))) {
                $this->errors[] = Tools::displayError('You must add at least one attribute.');
            }

            $array_checks = array(
                'reference' => 'isReference',
                'supplier_reference' => 'isReference',
                'location' => 'isReference',
                'ean13' => 'isEan13',
                'upc' => 'isUpc',
                'wholesale_price' => 'isPrice',
                'price' => 'isPrice',
                'ecotax' => 'isPrice',
                'quantity' => 'isInt',
                'weight' => 'isUnsignedFloat',
                'unit_price_impact' => 'isPrice',
                'default_on' => 'isBool',
                'minimal_quantity' => 'isUnsignedInt',
                'available_date' => 'isDateFormat'
            );
            foreach ($array_checks as $property => $check) {
                if (Tools::getValue('attribute_'.$property) !== false && !call_user_func(array('Validate', $check), Tools::getValue('attribute_'.$property))) {
                    $this->errors[] = sprintf(Tools::displayError('Field %s is not valid'), $property);
                }
            }

            if (!count($this->errors)) {
                if (!isset($_POST['attribute_wholesale_price'])) {
                    $_POST['attribute_wholesale_price'] = 0;
                }
                if (!isset($_POST['attribute_price_impact'])) {
                    $_POST['attribute_price_impact'] = 0;
                }
                if (!isset($_POST['attribute_weight_impact'])) {
                    $_POST['attribute_weight_impact'] = 0;
                }
                if (!isset($_POST['attribute_ecotax'])) {
                    $_POST['attribute_ecotax'] = 0;
                }
                if (Tools::getValue('attribute_default')) {
                    $product->deleteDefaultAttributes();
                }

                // Change existing one
                if (($id_product_attribute = (int)Tools::getValue('id_product_attribute')) || ($id_product_attribute = $product->productAttributeExists(Tools::getValue('attribute_combination_list'), false, null, true, true))) {
                    if ($this->tabAccess['edit'] === '1') {
                        if ($this->isProductFieldUpdated('available_date_attribute') && (Tools::getValue('available_date_attribute') != '' &&!Validate::isDateFormat(Tools::getValue('available_date_attribute')))) {
                            $this->errors[] = Tools::displayError('Invalid date format.');
                        } else {
                            $product->updateAttribute((int)$id_product_attribute,
                                $this->isProductFieldUpdated('attribute_wholesale_price') ? Tools::getValue('attribute_wholesale_price') : null,
                                $this->isProductFieldUpdated('attribute_price_impact') ? Tools::getValue('attribute_price') * Tools::getValue('attribute_price_impact') : null,
                                $this->isProductFieldUpdated('attribute_weight_impact') ? Tools::getValue('attribute_weight') * Tools::getValue('attribute_weight_impact') : null,
                                $this->isProductFieldUpdated('attribute_unit_impact') ? Tools::getValue('attribute_unity') * Tools::getValue('attribute_unit_impact') : null,
                                $this->isProductFieldUpdated('attribute_ecotax') ? Tools::getValue('attribute_ecotax') : null,
                                Tools::getValue('id_image_attr'),
                                Tools::getValue('attribute_reference'),
                                Tools::getValue('attribute_ean13'),
                                $this->isProductFieldUpdated('attribute_default') ? Tools::getValue('attribute_default') : null,
                                Tools::getValue('attribute_location'),
                                Tools::getValue('attribute_upc'),
                                $this->isProductFieldUpdated('attribute_minimal_quantity') ? Tools::getValue('attribute_minimal_quantity') : null,
                                $this->isProductFieldUpdated('available_date_attribute') ? Tools::getValue('available_date_attribute') : null, false);
                            StockAvailable::setProductDependsOnStock((int)$product->id, $product->depends_on_stock, null, (int)$id_product_attribute);
                            StockAvailable::setProductOutOfStock((int)$product->id, $product->out_of_stock, null, (int)$id_product_attribute);
                        }
                    } else {
                        $this->errors[] = Tools::displayError('You do not have permission to add this.');
                    }
                }
                // Add new
                else {
                    if ($this->tabAccess['add'] === '1') {
                        if ($product->productAttributeExists(Tools::getValue('attribute_combination_list'))) {
                            $this->errors[] = Tools::displayError('This combination already exists.');
                        } else {
                            $id_product_attribute = $product->addCombinationEntity(
                                Tools::getValue('attribute_wholesale_price'),
                                Tools::getValue('attribute_price') * Tools::getValue('attribute_price_impact'),
                                Tools::getValue('attribute_weight') * Tools::getValue('attribute_weight_impact'),
                                Tools::getValue('attribute_unity') * Tools::getValue('attribute_unit_impact'),
                                Tools::getValue('attribute_ecotax'),
                                0,
                                Tools::getValue('id_image_attr'),
                                Tools::getValue('attribute_reference'),
                                null,
                                Tools::getValue('attribute_ean13'),
                                Tools::getValue('attribute_default'),
                                Tools::getValue('attribute_location'),
                                Tools::getValue('attribute_upc'),
                                Tools::getValue('attribute_minimal_quantity'),
                                array(),
                                Tools::getValue('available_date_attribute')
                            );
                            StockAvailable::setProductDependsOnStock((int)$product->id, $product->depends_on_stock, null, (int)$id_product_attribute);
                            StockAvailable::setProductOutOfStock((int)$product->id, $product->out_of_stock, null, (int)$id_product_attribute);
                        }
                    } else {
                        $this->errors[] = Tools::displayError('You do not have permission to').'<hr>'.Tools::displayError('edit here.');
                    }
                }
                if (!count($this->errors)) {
                    $combination = new Combination((int)$id_product_attribute);
                    $combination->setAttributes(Tools::getValue('attribute_combination_list'));

                    // images could be deleted before
                    $id_images = Tools::getValue('id_image_attr');
                    if (!empty($id_images)) {
                        $combination->setImages($id_images);
                    }

                    $product->checkDefaultAttributes();
                    if (Tools::getValue('attribute_default')) {
                        Product::updateDefaultAttribute((int)$product->id);
                        if (isset($id_product_attribute)) {
                            $product->cache_default_attribute = (int)$id_product_attribute;
                        }

                        if ($available_date = Tools::getValue('available_date_attribute')) {
                            $product->setAvailableDate($available_date);
                        } else {
                            $product->setAvailableDate();
                        }
                    }
                }
            }
        }
    }

    public function processFeatures()
    {
        if (!Feature::isFeatureActive()) {
            return;
        }

        if (Validate::isLoadedObject($product = new Product((int)Tools::getValue('id_product')))) {
            // delete all objects
            $product->deleteFeatures();

            // add new objects
            $languages = Language::getLanguages(false);
            foreach ($_POST as $key => $val) {
                if (preg_match('/^feature_([0-9]+)_value/i', $key, $match)) {
                    if ($val) {
                        $product->addFeaturesToDB($match[1], $val);
                    } else {
                        if ($default_value = $this->checkFeatures($languages, $match[1])) {
                            $id_value = $product->addFeaturesToDB($match[1], 0, 1);
                            foreach ($languages as $language) {
                                if ($cust = Tools::getValue('custom_'.$match[1].'_'.(int)$language['id_lang'])) {
                                    $product->addFeaturesCustomToDB($id_value, (int)$language['id_lang'], $cust);
                                } else {
                                    $product->addFeaturesCustomToDB($id_value, (int)$language['id_lang'], $default_value);
                                }
                            }
                        }
                    }
                }
            }
        } else {
            $this->errors[] = Tools::displayError('A product must be created before adding features.');
        }
    }

    /**
     * This function is never called at the moment (specific prices cannot be edited)
     */
    public function processPricesModification()
    {
        $id_specific_prices = Tools::getValue('spm_id_specific_price');
        $id_combinations = Tools::getValue('spm_id_product_attribute');
        $id_shops = Tools::getValue('spm_id_shop');
        $id_currencies = Tools::getValue('spm_id_currency');
        $id_countries = Tools::getValue('spm_id_country');
        $id_groups = Tools::getValue('spm_id_group');
        $id_customers = Tools::getValue('spm_id_customer');
        $prices = Tools::getValue('spm_price');
        $from_quantities = Tools::getValue('spm_from_quantity');
        $reductions = Tools::getValue('spm_reduction');
        $reduction_types = Tools::getValue('spm_reduction_type');
        $froms = Tools::getValue('spm_from');
        $tos = Tools::getValue('spm_to');

        foreach ($id_specific_prices as $key => $id_specific_price) {
            if ($reduction_types[$key] == 'percentage' && ((float)$reductions[$key] <= 0 || (float)$reductions[$key] > 100)) {
                $this->errors[] = Tools::displayError('Submitted reduction value (0-100) is out-of-range');
            } elseif ($this->_validateSpecificPrice($id_shops[$key], $id_currencies[$key], $id_countries[$key], $id_groups[$key], $id_customers[$key], $prices[$key], $from_quantities[$key], $reductions[$key], $reduction_types[$key], $froms[$key], $tos[$key], $id_combinations[$key])) {
                $specific_price = new SpecificPrice((int)($id_specific_price));
                $specific_price->id_shop = (int)$id_shops[$key];
                $specific_price->id_product_attribute = (int)$id_combinations[$key];
                $specific_price->id_currency = (int)($id_currencies[$key]);
                $specific_price->id_country = (int)($id_countries[$key]);
                $specific_price->id_group = (int)($id_groups[$key]);
                $specific_price->id_customer = (int)$id_customers[$key];
                $specific_price->price = (float)($prices[$key]);
                $specific_price->from_quantity = (int)($from_quantities[$key]);
                $specific_price->reduction = (float)($reduction_types[$key] == 'percentage' ? ($reductions[$key] / 100) : $reductions[$key]);
                $specific_price->reduction_type = !$reductions[$key] ? 'amount' : $reduction_types[$key];
                $specific_price->from = !$froms[$key] ? '0000-00-00 00:00:00' : $froms[$key];
                $specific_price->to = !$tos[$key] ? '0000-00-00 00:00:00' : $tos[$key];
                if (!$specific_price->update()) {
                    $this->errors[] = Tools::displayError('An error occurred while updating the specific price.');
                }
            }
        }
        if (!count($this->errors)) {
            $this->redirect_after = self::$currentIndex.'&id_product='.(int)(Tools::getValue('id_product')).(Tools::getIsset('id_category') ? '&id_category='.(int)Tools::getValue('id_category') : '').'&update'.$this->table.'&action=Prices&token='.$this->token;
        }
    }

    public function processPriceAddition()
    {
        // Check if a specific price has been submitted
        if (!Tools::getIsset('submitPriceAddition')) {
            return;
        }

		$sp_type = Tools::getValue('sp_type');
		
        $id_product = Tools::getValue('id_product');
        $id_product_attribute = Tools::getValue('sp_id_product_attribute');
        $id_shop = Tools::getValue('sp_id_shop');
        $id_currency = Tools::getValue('sp_id_currency');
        $id_country = Tools::getValue('sp_id_country');
        $id_group = Tools::getValue('sp_id_group');
        $id_customer = Tools::getValue('sp_id_customer');
        $sp_price = Tools::getValue('sp_price');
        $from_quantity = Tools::getValue('sp_from_quantity');
        $reduction = (float)(Tools::getValue('sp_reduction'));
        $reduction_tax = Tools::getValue('sp_reduction_tax');
        $reduction_type = !$reduction ? 'amount' : Tools::getValue('sp_reduction_type');
        $reduction_type = $reduction_type == '-' ? 'amount' : $reduction_type;
        $from = Tools::getValue('sp_from');
        if (!$from) {
            $from = '0000-00-00 00:00:00';
        }
        $to = Tools::getValue('sp_to');
        if (!$to) {
            $to = '0000-00-00 00:00:00';
        }
		
		if($sp_type == 'vendita')
		{
			
			if (($sp_price == '-1') && ((float)$reduction == '0')) {
				$this->errors[] = Tools::displayError('No reduction value has been submitted');
			}  elseif ($to != '0000-00-00 00:00:00' && strtotime($to) < strtotime($from)) {
				$this->errors[] = Tools::displayError('Invalid date range');
			} elseif ($reduction_type == 'percentage' && ((float)$reduction <= 0 || (float)$reduction > 100)) {
				$this->errors[] = Tools::displayError('Submitted reduction value (0-100) is out-of-range');
			} elseif ($this->_validateSpecificPrice($id_shop, $id_currency, $id_country, $id_group, $id_customer, $sp_price, $from_quantity, $reduction, $reduction_type, $from, $to, $id_product_attribute)) {
				$specificPrice = new SpecificPrice();
				$specificPrice->id_product = (int)$id_product;
				$specificPrice->id_product_attribute = (int)$id_product_attribute;
				$specificPrice->id_shop = (int)$id_shop;
				$specificPrice->id_currency = (int)($id_currency);
				$specificPrice->id_country = (int)($id_country);
				$specificPrice->id_group = (int)($id_group);
				$specificPrice->id_customer = (int)$id_customer;
				$specificPrice->price = (float)($sp_price);
				$specificPrice->from_quantity = (int)($from_quantity);
				$specificPrice->reduction = (float)($reduction_type == 'percentage' ? $reduction / 100 : $reduction);
				$specificPrice->reduction_tax = $reduction_tax;
				$specificPrice->reduction_type = $reduction_type;
				$specificPrice->from = $from;
				$specificPrice->to = $to;
				if (!$specificPrice->add()) {
					$this->errors[] = Tools::displayError('An error occurred while updating the specific price.');
				}
			}
		}
		else if($sp_type == 'acquisto')
		{
			$wholesalee = Db::getInstance()->getValue('select listino from '._DB_PREFIX_.'product WHERE id_product = '.$id_product) - ( Db::getInstance()->getValue('select listino from '._DB_PREFIX_.'product WHERE id_product = '.$id_product) * ($reduction/100));
			
			Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'specific_price_wholesale VALUES (NULL, '.$id_product.', '.Db::getInstance()->getValue('select id_supplier from '._DB_PREFIX_.'product WHERE id_product = '.$id_product).', "'.Db::getInstance()->getValue('select listino from '._DB_PREFIX_.'product WHERE id_product = '.$id_product).'", '.$reduction.', "", "", "'.$wholesale.'", "'.$from.'", "'.$to.'", "'.$from_quantity.'")');
		}
    }

    public function ajaxProcessDeleteSpecificPrice()
    {
        if ($this->tabAccess['delete'] === '1') {
            $id_specific_price = (int)Tools::getValue('id_specific_price');
            if (!$id_specific_price || !Validate::isUnsignedId($id_specific_price)) {
                $error = Tools::displayError('The specific price ID is invalid.');
            } else {
                $specificPrice = new SpecificPrice((int)$id_specific_price);
                if (!$specificPrice->delete()) {
                    $error = Tools::displayError('An error occurred while attempting to delete the specific price.');
                }
            }
        } else {
            $error = Tools::displayError('You do not have permission to delete this.');
        }

        if (isset($error)) {
            $json = array(
                'status' => 'error',
                'message'=> $error
            );
        } else {
            $json = array(
                'status' => 'ok',
                'message'=> $this->_conf[1]
            );
        }

        die(Tools::jsonEncode($json));
    }
	
	public function ajaxProcessDeleteSpecificWholesale()
    {
        if ($this->tabAccess['delete'] === '1') {
            $id_specific_price = (int)Tools::getValue('id_specific_price');
            if (!$id_specific_price || !Validate::isUnsignedId($id_specific_price)) {
                $error = Tools::displayError('The specific price ID is invalid.');
            } else {
                Db::getInstance()->execute('delete from '._DB_PREFIX_.'specific_price_wholesale where id_specific_price = '.$id_specific_price);
                
            }
        } else {
            $error = Tools::displayError('You do not have permission to delete this.');
        }

        if (isset($error)) {
            $json = array(
                'status' => 'error',
                'message'=> $error
            );
        } else {
            $json = array(
                'status' => 'ok',
                'message'=> $this->_conf[1]
            );
        }

        die(Tools::jsonEncode($json));
    }

    public function processSpecificPricePriorities()
    {
        if (!($obj = $this->loadObject())) {
            return;
        }
        if (!$priorities = Tools::getValue('specificPricePriority')) {
            $this->errors[] = Tools::displayError('Please specify priorities.');
        } elseif (Tools::isSubmit('specificPricePriorityToAll')) {
            if (!SpecificPrice::setPriorities($priorities)) {
                $this->errors[] = Tools::displayError('An error occurred while updating priorities.');
            } else {
                $this->confirmations[] = $this->l('The price rule has successfully updated');
            }
        } elseif (!SpecificPrice::setSpecificPriority((int)$obj->id, $priorities)) {
            $this->errors[] = Tools::displayError('An error occurred while setting priorities.');
        }
    }

    public function processCustomizationConfiguration()
    {
        $product = $this->object;
        // Get the number of existing customization fields ($product->text_fields is the updated value, not the existing value)
        $current_customization = $product->getCustomizationFieldIds();
        $files_count = 0;
        $text_count = 0;
        if (is_array($current_customization)) {
            foreach ($current_customization as $field) {
                if ($field['type'] == 1) {
                    $text_count++;
                } else {
                    $files_count++;
                }
            }
        }

        if (!$product->createLabels((int)$product->uploadable_files - $files_count, (int)$product->text_fields - $text_count)) {
            $this->errors[] = Tools::displayError('An error occurred while creating customization fields.');
        }
        if (!count($this->errors) && !$product->updateLabels()) {
            $this->errors[] = Tools::displayError('An error occurred while updating customization fields.');
        }
        $product->customizable = ($product->uploadable_files > 0 || $product->text_fields > 0) ? 1 : 0;
        if (($product->uploadable_files != $files_count || $product->text_fields != $text_count) && !count($this->errors) && !$product->update()) {
            $this->errors[] = Tools::displayError('An error occurred while updating the custom configuration.');
        }
    }

    public function processProductCustomization()
    {
        if (Validate::isLoadedObject($product = new Product((int)Tools::getValue('id_product')))) {
            foreach ($_POST as $field => $value) {
                if (strncmp($field, 'label_', 6) == 0 && !Validate::isLabel($value)) {
                    $this->errors[] = Tools::displayError('The label fields defined are invalid.');
                }
            }
            if (empty($this->errors) && !$product->updateLabels()) {
                $this->errors[] = Tools::displayError('An error occurred while updating customization fields.');
            }
            if (empty($this->errors)) {
                $this->confirmations[] = $this->l('Update successful');
            }
        } else {
            $this->errors[] = Tools::displayError('A product must be created before adding customization.');
        }
    }

    /**
     * Overrides parent for custom redirect link
     */
    public function processPosition()
    {
        /** @var Product $object */
        if (!Validate::isLoadedObject($object = $this->loadObject())) {
            $this->errors[] = Tools::displayError('An error occurred while updating the status for an object.').
                ' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
        } elseif (!$object->updatePosition((int)Tools::getValue('way'), (int)Tools::getValue('position'))) {
            $this->errors[] = Tools::displayError('Failed to update the position.');
        } else {
            $category = new Category((int)Tools::getValue('id_category'));
            if (Validate::isLoadedObject($category)) {
                Hook::exec('actionCategoryUpdate', array('category' => $category));
            }
            $this->redirect_after = self::$currentIndex.'&'.$this->table.'Orderby=position&'.$this->table.'Orderway=asc&action=Customization&conf=5'.(($id_category = (Tools::getIsset('id_category') ? (int)Tools::getValue('id_category') : '')) ? ('&id_category='.$id_category) : '').'&token='.Tools::getAdminTokenLite('AdminProducts');
        }
    }

    public function processSalvaTutto()
    {
        $this->salvatutto();
    }

    public function processAccodaEsolver()
    {
        ini_set("memory_limit", "892M");
        Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'product SET date_upd = "'.date('Y-m-d H:i:s').'" WHERE id_product = '.Tools::getValue('id_product'));
        
        Product::anagraficheArticoliEsolver();
    }

    public function initProcess()
    {
        if (Tools::isSubmit('submitAddproductAndStay') || Tools::isSubmit('submitAddproduct') || Tools::isSubmit('salvatutto')) {
            $this->id_object = (int)Tools::getValue('id_product');
            $this->object = new Product($this->id_object);

            if ($this->isTabSubmitted('Informations') && $this->object->is_virtual && (int)Tools::getValue('type_product') != 2) {
                if ($id_product_download = (int)ProductDownload::getIdFromIdProduct($this->id_object)) {
                    $product_download = new ProductDownload($id_product_download);
                    if (!$product_download->deleteFile($id_product_download)) {
                        $this->errors[] = Tools::displayError('Cannot delete file');
                    }
                }
            }
        }

        if(Tools::isSubmit('salvatutto')) { // Override
            // correggere: inserire permessi di add e edit?
            $this->action = 'salvaTutto';
		}
        elseif(Tools::isSubmit('accoda_esolver')) { // Override
            // correggere: inserire permessi di add e edit?
            $this->action = 'accodaEsolver';
		}
        // Delete a product in the download folder
        else if (Tools::getValue('deleteVirtualProduct')) {
            if ($this->tabAccess['delete'] === '1') {
                $this->action = 'deleteVirtualProduct';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to delete this.');
            }
        }
        // Product preview
        elseif (Tools::isSubmit('submitAddProductAndPreview')) {
            $this->display = 'edit';
            $this->action = 'save';
            if (Tools::getValue('id_product')) {
                $this->id_object = Tools::getValue('id_product');
                $this->object = new Product((int)Tools::getValue('id_product'));
            }
        } elseif (Tools::isSubmit('submitAttachments')) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'attachments';
                $this->tab_display = 'attachments';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        }
        // Product duplication
        elseif (Tools::getIsset('duplicate'.$this->table)) {
            if ($this->tabAccess['add'] === '1') {
                $this->action = 'duplicate';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to add this.');
            }
        }
        // Product images management
        elseif (Tools::getValue('id_image') && Tools::getValue('ajax')) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'image';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        }
        // Product attributes management
        elseif (Tools::isSubmit('submitProductAttribute')) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'productAttribute';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        }
        // Product features management
        elseif (Tools::isSubmit('submitFeatures') || Tools::isSubmit('submitFeaturesAndStay')) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'features';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        }
        // Product specific prices management NEVER USED
        elseif (Tools::isSubmit('submitPricesModification')) {
            if ($this->tabAccess['add'] === '1') {
                $this->action = 'pricesModification';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to add this.');
            }
        } elseif (Tools::isSubmit('deleteSpecificPrice')) {
            if ($this->tabAccess['delete'] === '1') {
                $this->action = 'deleteSpecificPrice';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to delete this.');
            }
		} elseif (Tools::isSubmit('deleteSpecificWholesale')) {
            if ($this->tabAccess['delete'] === '1') {
                $this->action = 'deleteSpecificWholesale';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to delete this.');
            }	
        } elseif (Tools::isSubmit('submitSpecificPricePriorities')) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'specificPricePriorities';
                $this->tab_display = 'prices';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        }
        // Customization management
        elseif (Tools::isSubmit('submitCustomizationConfiguration')) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'customizationConfiguration';
                $this->tab_display = 'customization';
                $this->display = 'edit';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        } elseif (Tools::isSubmit('submitProductCustomization')) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'productCustomization';
                $this->tab_display = 'customization';
                $this->display = 'edit';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        } elseif (Tools::isSubmit('id_product')) {
            $post_max_size = Tools::getMaxUploadSize(Configuration::get('PS_LIMIT_UPLOAD_FILE_VALUE') * 1024 * 1024);
            if ($post_max_size && isset($_SERVER['CONTENT_LENGTH']) && $_SERVER['CONTENT_LENGTH'] && $_SERVER['CONTENT_LENGTH'] > $post_max_size) {
                $this->errors[] = sprintf(Tools::displayError('The uploaded file exceeds the "Maximum size for a downloadable product" set in preferences (%1$dMB) or the post_max_size/ directive in php.ini (%2$dMB).'), number_format((Configuration::get('PS_LIMIT_UPLOAD_FILE_VALUE'))), ($post_max_size / 1024 / 1024));
            }
        }

        if (!$this->action) {
            parent::initProcess();
        } else {
            $this->id_object = (int)Tools::getValue($this->identifier);
        }

        if (isset($this->available_tabs[Tools::getValue('key_tab')])) {
            $this->tab_display = Tools::getValue('key_tab');
        }

        // Set tab to display if not decided already
        if (!$this->tab_display && $this->action) {
            if (in_array($this->action, array_keys($this->available_tabs))) {
                $this->tab_display = $this->action;
            }
        }

        // And if still not set, use default
        if (!$this->tab_display) {
            if (in_array($this->default_tab, $this->available_tabs)) {
                $this->tab_display = $this->default_tab;
            } else {
                $this->tab_display = key($this->available_tabs);
            }
        }
    }

    /**
     * postProcess handle every checks before saving products information
     *
     * @return void
     */
    public function postProcess()
    {
        if (!$this->redirect_after) {
            parent::postProcess();
        }

        if ($this->display == 'edit' || $this->display == 'add') {
            $this->addJqueryUI(array(
                'ui.core',
                'ui.widget'
            ));

            $this->addjQueryPlugin(array(
                'autocomplete',
                'tablednd',
                'thickbox',
                'ajaxfileupload',
                'date',
                'tagify',
                'select2',
                'validate'
            ));

            $this->addJS(array(
                _PS_JS_DIR_.'admin/products.js',
                _PS_JS_DIR_.'admin/attributes.js',
                _PS_JS_DIR_.'admin/price.js',
                _PS_JS_DIR_.'tiny_mce/tiny_mce.js',
                _PS_JS_DIR_.'admin/tinymce.inc.js',
                _PS_JS_DIR_.'admin/dnd.js',
                _PS_JS_DIR_.'jquery/ui/jquery.ui.progressbar.min.js',
                _PS_JS_DIR_.'vendor/spin.js',
                _PS_JS_DIR_.'vendor/ladda.js'
            ));

            $this->addJS(_PS_JS_DIR_.'jquery/plugins/select2/select2_locale_'.$this->context->language->iso_code.'.js');
            $this->addJS(_PS_JS_DIR_.'jquery/plugins/validate/localization/messages_'.$this->context->language->iso_code.'.js');

            $this->addCSS(array(
                _PS_JS_DIR_.'jquery/plugins/timepicker/jquery-ui-timepicker-addon.css'
            ));

            // Override
            $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'select2_ready.js');
        }
    }

    public function ajaxProcessDeleteProductAttribute()
    {
        if (!Combination::isFeatureActive()) {
            return;
        }

        if ($this->tabAccess['delete'] === '1') {
            $id_product = (int)Tools::getValue('id_product');
            $id_product_attribute = (int)Tools::getValue('id_product_attribute');

            if ($id_product && Validate::isUnsignedId($id_product) && Validate::isLoadedObject($product = new Product($id_product))) {
                if (($depends_on_stock = StockAvailable::dependsOnStock($id_product)) && StockAvailable::getQuantityAvailableByProduct($id_product, $id_product_attribute)) {
                    $json = array(
                        'status' => 'error',
                        'message'=> $this->l('It is not possible to delete a combination while it still has some quantities in the Advanced Stock Management. You must delete its stock first.')
                    );
                } else {
                    $product->deleteAttributeCombination((int)$id_product_attribute);
                    $product->checkDefaultAttributes();
                    Tools::clearColorListCache((int)$product->id);
                    if (!$product->hasAttributes()) {
                        $product->cache_default_attribute = 0;
                        $product->update();
                    } else {
                        Product::updateDefaultAttribute($id_product);
                    }

                    if ($depends_on_stock && !Stock::deleteStockByIds($id_product, $id_product_attribute)) {
                        $json = array(
                            'status' => 'error',
                            'message'=> $this->l('Error while deleting the stock')
                        );
                    } else {
                        $json = array(
                            'status' => 'ok',
                            'message'=> $this->_conf[1],
                            'id_product_attribute' => (int)$id_product_attribute
                        );
                    }
                }
            } else {
                $json = array(
                    'status' => 'error',
                    'message'=> $this->l('You cannot delete this attribute.')
                );
            }
        } else {
            $json = array(
                'status' => 'error',
                'message'=> $this->l('You do not have permission to delete this.')
            );
        }

        die(Tools::jsonEncode($json));
    }

    public function ajaxProcessDefaultProductAttribute()
    {
        if ($this->tabAccess['edit'] === '1') {
            if (!Combination::isFeatureActive()) {
                return;
            }

            if (Validate::isLoadedObject($product = new Product((int)Tools::getValue('id_product')))) {
                $product->deleteDefaultAttributes();
                $product->setDefaultAttribute((int)Tools::getValue('id_product_attribute'));
                $json = array(
                    'status' => 'ok',
                    'message'=> $this->_conf[4]
                );
            } else {
                $json = array(
                    'status' => 'error',
                    'message'=> $this->l('You cannot make this the default attribute.')
                );
            }

            die(Tools::jsonEncode($json));
        }
    }

    public function ajaxProcessEditProductAttribute()
    {
        if ($this->tabAccess['edit'] === '1') {
            $id_product = (int)Tools::getValue('id_product');
            $id_product_attribute = (int)Tools::getValue('id_product_attribute');
            if ($id_product && Validate::isUnsignedId($id_product) && Validate::isLoadedObject($product = new Product((int)$id_product))) {
                $combinations = $product->getAttributeCombinationsById($id_product_attribute, $this->context->language->id);
                foreach ($combinations as $key => $combination) {
                    $combinations[$key]['attributes'][] = array($combination['group_name'], $combination['attribute_name'], $combination['id_attribute']);
                }

                die(Tools::jsonEncode($combinations));
            }
        }
    }

    public function ajaxPreProcess()
    {
        if (Tools::getIsset('update'.$this->table) && Tools::getIsset('id_'.$this->table)) {
            $this->display = 'edit';
            $this->action = Tools::getValue('action');
        }
    }

    public function ajaxProcessUpdateProductImageShopAsso()
    {
        $id_product = Tools::getValue('id_product');
        if (($id_image = Tools::getValue('id_image')) && ($id_shop = (int)Tools::getValue('id_shop'))) {
            if (Tools::getValue('active') == 'true') {
                $res = Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'image_shop (`id_product`, `id_image`, `id_shop`, `cover`) VALUES('.(int)$id_product.', '.(int)$id_image.', '.(int)$id_shop.', NULL)');
            } else {
                $res = Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'image_shop WHERE `id_image` = '.(int)$id_image.' AND `id_shop` = '.(int)$id_shop);
            }
        }

        // Clean covers in image table
        $count_cover_image = Db::getInstance()->getValue('
			SELECT COUNT(*) FROM '._DB_PREFIX_.'image i
			INNER JOIN '._DB_PREFIX_.'image_shop ish ON (i.id_image = ish.id_image AND ish.id_shop = '.(int)$id_shop.')
			WHERE i.cover = 1 AND i.`id_product` = '.(int)$id_product);

        if (!$id_image) {
            $id_image = Db::getInstance()->getValue('
                SELECT i.`id_image` FROM '._DB_PREFIX_.'image i
                INNER JOIN '._DB_PREFIX_.'image_shop ish ON (i.id_image = ish.id_image AND ish.id_shop = '.(int)$id_shop.')
                WHERE i.`id_product` = '.(int)$id_product);
        }

        if ($count_cover_image < 1) {
            Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'image i SET i.cover = 1 WHERE i.id_image = '.(int)$id_image.' AND i.`id_product` = '.(int)$id_product.' LIMIT 1');
        }

        // Clean covers in image_shop table
        $count_cover_image_shop = Db::getInstance()->getValue('
			SELECT COUNT(*)
			FROM '._DB_PREFIX_.'image_shop ish
			WHERE ish.`id_product` = '.(int)$id_product.' AND ish.id_shop = '.(int)$id_shop.' AND ish.cover = 1');

        if ($count_cover_image_shop < 1) {
            Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'image_shop ish SET ish.cover = 1 WHERE ish.id_image = '.(int)$id_image.' AND ish.`id_product` = '.(int)$id_product.' AND ish.id_shop =  '.(int)$id_shop.' LIMIT 1');
        }

        if ($res) {
            $this->jsonConfirmation($this->_conf[27]);
        } else {
            $this->jsonError(Tools::displayError('An error occurred while attempting to associate this image with your shop. '));
        }
    }

    public function ajaxProcessUpdateImagePosition()
    {
        if ($this->tabAccess['edit'] === '0') {
            return die(Tools::jsonEncode(array('error' => $this->l('You do not have the right permission'))));
        }
        $res = false;
        if ($json = Tools::getValue('json')) {
            $res = true;
            $json = stripslashes($json);
            $images = Tools::jsonDecode($json, true);
            foreach ($images as $id => $position) {
                $img = new Image((int)$id);
                $img->position = (int)$position;
                $res &= $img->update();
            }
        }
        if ($res) {
            $this->jsonConfirmation($this->_conf[25]);
        } else {
            $this->jsonError(Tools::displayError('An error occurred while attempting to move this picture.'));
        }
    }

    public function ajaxProcessUpdateCover()
    {
        if ($this->tabAccess['edit'] === '0') {
            return die(Tools::jsonEncode(array('error' => $this->l('You do not have the right permission'))));
        }
        Image::deleteCover((int)Tools::getValue('id_product'));
        $img = new Image((int)Tools::getValue('id_image'));
        $img->cover = 1;

        @unlink(_PS_TMP_IMG_DIR_.'product_'.(int)$img->id_product.'.jpg');
        @unlink(_PS_TMP_IMG_DIR_.'product_mini_'.(int)$img->id_product.'_'.$this->context->shop->id.'.jpg');

        if ($img->update()) {
            $this->jsonConfirmation($this->_conf[26]);
        } else {
            $this->jsonError(Tools::displayError('An error occurred while attempting to update the cover picture.'));
        }
    }

    public function ajaxProcessDeleteProductImage()
    {
        $this->display = 'content';
        $res = true;
        /* Delete product image */
        $image = new Image((int)Tools::getValue('id_image'));
        $this->content['id'] = $image->id;
        $res &= $image->delete();
        // if deleted image was the cover, change it to the first one
        if (!Image::getCover($image->id_product)) {
            $res &= Db::getInstance()->execute('
			UPDATE `'._DB_PREFIX_.'image_shop` image_shop
			SET image_shop.`cover` = 1
			WHERE image_shop.`id_product` = '.(int)$image->id_product.'
			AND id_shop='.(int)$this->context->shop->id.' LIMIT 1');
        }

        if (!Image::getGlobalCover($image->id_product)) {
            $res &= Db::getInstance()->execute('
			UPDATE `'._DB_PREFIX_.'image` i
			SET i.`cover` = 1
			WHERE i.`id_product` = '.(int)$image->id_product.' LIMIT 1');
        }

        if (file_exists(_PS_TMP_IMG_DIR_.'product_'.$image->id_product.'.jpg')) {
            $res &= @unlink(_PS_TMP_IMG_DIR_.'product_'.$image->id_product.'.jpg');
        }
        if (file_exists(_PS_TMP_IMG_DIR_.'product_mini_'.$image->id_product.'_'.$this->context->shop->id.'.jpg')) {
            $res &= @unlink(_PS_TMP_IMG_DIR_.'product_mini_'.$image->id_product.'_'.$this->context->shop->id.'.jpg');
        }

        if ($res) {
            $this->jsonConfirmation($this->_conf[7]);
        } else {
            $this->jsonError(Tools::displayError('An error occurred while attempting to delete the product image.'));
        }
    }

    protected function _validateSpecificPrice($id_shop, $id_currency, $id_country, $id_group, $id_customer, $price, $from_quantity, $reduction, $reduction_type, $from, $to, $id_combination = 0)
    {
        if (!Validate::isUnsignedId($id_shop) || !Validate::isUnsignedId($id_currency) || !Validate::isUnsignedId($id_country) || !Validate::isUnsignedId($id_group) || !Validate::isUnsignedId($id_customer)) {
            $this->errors[] = Tools::displayError('Wrong IDs');
        } elseif ((!isset($price) && !isset($reduction)) || (isset($price) && !Validate::isNegativePrice($price)) || (isset($reduction) && !Validate::isPrice($reduction))) {
            $this->errors[] = Tools::displayError('Invalid price/discount amount');
        } elseif (!Validate::isUnsignedInt($from_quantity)) {
            $this->errors[] = Tools::displayError('Invalid quantity');
        } elseif ($reduction && !Validate::isReductionType($reduction_type)) {
            $this->errors[] = Tools::displayError('Please select a discount type (amount or percentage).');
        } elseif ($from && $to && (!Validate::isDateFormat($from) || !Validate::isDateFormat($to))) {
            $this->errors[] = Tools::displayError('The from/to date is invalid.');
        } elseif (SpecificPrice::exists((int)$this->object->id, $id_combination, $id_shop, $id_group, $id_country, $id_currency, $id_customer, $from_quantity, $from, $to, false)) {
            $this->errors[] = Tools::displayError('A specific price already exists for these parameters.');
        } else {
            return true;
        }
        return false;
    }

    /* Checking customs feature */
    protected function checkFeatures($languages, $feature_id)
    {
        $rules = call_user_func(array('FeatureValue', 'getValidationRules'), 'FeatureValue');
        $feature = Feature::getFeature((int)Configuration::get('PS_LANG_DEFAULT'), $feature_id);

        foreach ($languages as $language) {
            if ($val = Tools::getValue('custom_'.$feature_id.'_'.$language['id_lang'])) {
                $current_language = new Language($language['id_lang']);
                if (Tools::strlen($val) > $rules['sizeLang']['value']) {
                    $this->errors[] = sprintf(
                        Tools::displayError('The name for feature %1$s is too long in %2$s.'),
                        ' <b>'.$feature['name'].'</b>',
                        $current_language->name
                    );
                } elseif (!call_user_func(array('Validate', $rules['validateLang']['value']), $val)) {
                    $this->errors[] = sprintf(
                        Tools::displayError('A valid name required for feature. %1$s in %2$s.'),
                        ' <b>'.$feature['name'].'</b>',
                        $current_language->name
                    );
                }
                if (count($this->errors)) {
                    return 0;
                }
                // Getting default language
                if ($language['id_lang'] == Configuration::get('PS_LANG_DEFAULT')) {
                    return $val;
                }
            }
        }
        return 0;
    }

    /**
     * Add or update a product image
     *
     * @param Product $product Product object to add image
     * @param string  $method
     *
     * @return int|false
     */
    public function addProductImage($product, $method = 'auto')
    {
        /* Updating an existing product image */
        if ($id_image = (int)Tools::getValue('id_image')) {
            $image = new Image((int)$id_image);
            if (!Validate::isLoadedObject($image)) {
                $this->errors[] = Tools::displayError('An error occurred while loading the object image.');
            } else {
                if (($cover = Tools::getValue('cover')) == 1) {
                    Image::deleteCover($product->id);
                }
                $image->cover = $cover;
                $this->validateRules('Image');
                $this->copyFromPost($image, 'image');
                if (count($this->errors) || !$image->update()) {
                    $this->errors[] = Tools::displayError('An error occurred while updating the image.');
                } elseif (isset($_FILES['image_product']['tmp_name']) && $_FILES['image_product']['tmp_name'] != null) {
                    $this->copyImage($product->id, $image->id, $method);
                }
            }
        }
        if (isset($image) && Validate::isLoadedObject($image) && !file_exists(_PS_PROD_IMG_DIR_.$image->getExistingImgPath().'.'.$image->image_format)) {
            $image->delete();
        }
        if (count($this->errors)) {
            return false;
        }
        @unlink(_PS_TMP_IMG_DIR_.'product_'.$product->id.'.jpg');
        @unlink(_PS_TMP_IMG_DIR_.'product_mini_'.$product->id.'_'.$this->context->shop->id.'.jpg');
        return ((isset($id_image) && is_int($id_image) && $id_image) ? $id_image : false);
    }

    /**
     * Copy a product image
     *
     * @param int    $id_product Product Id for product image filename
     * @param int    $id_image   Image Id for product image filename
     * @param string $method
     *
     * @return void|false
     * @throws PrestaShopException
     */
    public function copyImage($id_product, $id_image, $method = 'auto')
    {
        if (!isset($_FILES['image_product']['tmp_name'])) {
            return false;
        }
        if ($error = ImageManager::validateUpload($_FILES['image_product'])) {
            $this->errors[] = $error;
        } else {
            $image = new Image($id_image);

            if (!$new_path = $image->getPathForCreation()) {
                $this->errors[] = Tools::displayError('An error occurred while attempting to create a new folder.');
            }
            if (!($tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS')) || !move_uploaded_file($_FILES['image_product']['tmp_name'], $tmpName)) {
                $this->errors[] = Tools::displayError('An error occurred during the image upload process.');
            } elseif (!ImageManager::resize($tmpName, $new_path.'.'.$image->image_format)) {
                $this->errors[] = Tools::displayError('An error occurred while copying the image.');
            } elseif ($method == 'auto') {
                $imagesTypes = ImageType::getImagesTypes('products');
                foreach ($imagesTypes as $k => $image_type) {
                    if (!ImageManager::resize($tmpName, $new_path.'-'.stripslashes($image_type['name']).'.'.$image->image_format, $image_type['width'], $image_type['height'], $image->image_format)) {
                        $this->errors[] = Tools::displayError('An error occurred while copying this image:').' '.stripslashes($image_type['name']);
                    }
                }
            }

            @unlink($tmpName);
            Hook::exec('actionWatermark', array('id_image' => $id_image, 'id_product' => $id_product));
        }
    }

    protected function updateAssoShop($id_object)
    {
        //override AdminController::updateAssoShop() specifically for products because shop association is set with the context in ObjectModel
        return;
    }

    public function processAdd()
    {
        $this->checkProduct();

        if (!empty($this->errors)) {
            $this->display = 'add';
            return false;
        }

        $this->object = new $this->className();
        $this->_removeTaxFromEcotax();
        $this->copyFromPost($this->object, $this->table);
        if ($this->object->add()) {
            PrestaShopLogger::addLog(sprintf($this->l('%s addition', 'AdminTab', false, false), $this->className), 1, null, $this->className, (int)$this->object->id, true, (int)$this->context->employee->id);
            $this->addCarriers($this->object);
            $this->updateAccessories($this->object);
            $this->updateAlternatives($object); // Override
            $this->updatePackItems($this->object);
            $this->updateDownloadProduct($this->object);

            if (Configuration::get('PS_FORCE_ASM_NEW_PRODUCT') && Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && $this->object->getType() != Product::PTYPE_VIRTUAL) {
                $this->object->advanced_stock_management = 1;
                $this->object->save();
                $id_shops = Shop::getContextListShopID();
                foreach ($id_shops as $id_shop) {
                    StockAvailable::setProductDependsOnStock($this->object->id, true, (int)$id_shop, 0);
                }
            }

            if (empty($this->errors)) {
                $languages = Language::getLanguages(false);
                if ($this->isProductFieldUpdated('category_box') && !$this->object->updateCategories(Tools::getValue('categoryBox'))) {
                    $this->errors[] = Tools::displayError('An error occurred while linking the object.').' <b>'.$this->table.'</b> '.Tools::displayError('To categories');
                } elseif (!$this->updateTags($languages, $this->object)) {
                    $this->errors[] = Tools::displayError('An error occurred while adding tags.');
                } else {
                    Hook::exec('actionProductAdd', array('id_product' => (int)$this->object->id, 'product' => $this->object));
                    if (in_array($this->object->visibility, array('both', 'search')) && Configuration::get('PS_SEARCH_INDEXATION')) {
                        Search::indexation(false, $this->object->id);
                    }
                }

                if (Configuration::get('PS_DEFAULT_WAREHOUSE_NEW_PRODUCT') != 0 && Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT')) {
                    $warehouse_location_entity = new WarehouseProductLocation();
                    $warehouse_location_entity->id_product = $this->object->id;
                    $warehouse_location_entity->id_product_attribute = 0;
                    $warehouse_location_entity->id_warehouse = Configuration::get('PS_DEFAULT_WAREHOUSE_NEW_PRODUCT');
                    $warehouse_location_entity->location = pSQL('');
                    $warehouse_location_entity->save();
                }

                // Apply groups reductions
                $this->object->setGroupReduction();

                // Save and preview
                if (Tools::isSubmit('submitAddProductAndPreview')) {
                    $this->redirect_after = $this->getPreviewUrl($this->object);
                }

                $this->aggiornaCoseNonPredefinite($this->object); // Override

                // Save and stay on same form
                if ($this->display == 'edit') {
                    $this->redirect_after = self::$currentIndex.'&id_product='.(int)$this->object->id
                        .(Tools::getIsset('id_category') ? '&id_category='.(int)Tools::getValue('id_category') : '')
                        .'&updateproduct&conf=3&key_tab='.Tools::safeOutput(Tools::getValue('key_tab')).'&token='.$this->token;
                } else {
                    // Default behavior (save and back)
                    $this->redirect_after = self::$currentIndex
                        .(Tools::getIsset('id_category') ? '&id_category='.(int)Tools::getValue('id_category') : '')
                        .'&conf=3&token='.$this->token;
                }
            } else {
                $this->object->delete();
                // if errors : stay on edit page
                $this->display = 'edit';
            }
        } else {
            $this->errors[] = Tools::displayError('An error occurred while creating an object.').' <b>'.$this->table.'</b>';
        }

        return $this->object;
    }

    protected function isTabSubmitted($tab_name)
    {
        if (!is_array($this->submitted_tabs)) {
            $this->submitted_tabs = Tools::getValue('submitted_tabs');
        }

        if (is_array($this->submitted_tabs) && in_array($tab_name, $this->submitted_tabs)) {
            return true;
        }

        return false;
    }

    public function processStatus()
    {
        $this->loadObject(true);
        if (!Validate::isLoadedObject($this->object)) {
            return false;
        }
        if (($error = $this->object->validateFields(false, true)) !== true) {
            $this->errors[] = $error;
        }
        if (($error = $this->object->validateFieldsLang(false, true)) !== true) {
            $this->errors[] = $error;
        }

        if (count($this->errors)) {
            return false;
        }

        $res = parent::processStatus();

        $query = trim(Tools::getValue('bo_query'));
        $searchType = (int)Tools::getValue('bo_search_type');

        if ($query) {
            $this->redirect_after = preg_replace('/[\?|&](bo_query|bo_search_type)=([^&]*)/i', '', $this->redirect_after);
            $this->redirect_after .= '&bo_query='.$query.'&bo_search_type='.$searchType;
        }

        return $res;
    }

    public function processUpdate()
    {
        $existing_product = $this->object;

        $this->checkProduct();

        if (!empty($this->errors)) {
            $this->display = 'edit';
            return false;
        }

        $id = (int)Tools::getValue('id_'.$this->table);
        /* Update an existing product */
        if (isset($id) && !empty($id)) {
            /** @var Product $object */
            $object = new $this->className((int)$id);
            $this->object = $object;

            if (Validate::isLoadedObject($object)) {
                $this->_removeTaxFromEcotax();
                $product_type_before = $object->getType();
                $this->copyFromPost($object, $this->table);
                $object->indexed = 0;

                if (Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_SHOP) {
                    $object->setFieldsToUpdate((array)Tools::getValue('multishop_check', array()));
                }

                // Duplicate combinations if not associated to shop
                if ($this->context->shop->getContext() == Shop::CONTEXT_SHOP && !$object->isAssociatedToShop()) {
                    $is_associated_to_shop = false;
                    $combinations = Product::getProductAttributesIds($object->id);
                    if ($combinations) {
                        foreach ($combinations as $id_combination) {
                            $combination = new Combination((int)$id_combination['id_product_attribute']);
                            $default_combination = new Combination((int)$id_combination['id_product_attribute'], null, (int)$this->object->id_shop_default);

                            $def = ObjectModel::getDefinition($default_combination);
                            foreach ($def['fields'] as $field_name => $row) {
                                $combination->$field_name = ObjectModel::formatValue($default_combination->$field_name, $def['fields'][$field_name]['type']);
                            }

                            $combination->save();
                        }
                    }
                } else {
                    $is_associated_to_shop = true;
                }

                if ($object->update()) {
                    // If the product doesn't exist in the current shop but exists in another shop
                    if (Shop::getContext() == Shop::CONTEXT_SHOP && !$existing_product->isAssociatedToShop($this->context->shop->id)) {
                        $out_of_stock = StockAvailable::outOfStock($existing_product->id, $existing_product->id_shop_default);
                        $depends_on_stock = StockAvailable::dependsOnStock($existing_product->id, $existing_product->id_shop_default);
                        StockAvailable::setProductOutOfStock((int)$this->object->id, $out_of_stock, $this->context->shop->id);
                        StockAvailable::setProductDependsOnStock((int)$this->object->id, $depends_on_stock, $this->context->shop->id);
                    }

                    PrestaShopLogger::addLog(sprintf($this->l('%s modification', 'AdminTab', false, false), $this->className), 1, null, $this->className, (int)$this->object->id, true, (int)$this->context->employee->id);
                    if (in_array($this->context->shop->getContext(), array(Shop::CONTEXT_SHOP, Shop::CONTEXT_ALL))) {
                        if ($this->isTabSubmitted('Shipping')) {
                            $this->addCarriers();
                        }
                        if ($this->isTabSubmitted('Associations')) {
                            $this->updateAccessories($object);
                            $this->updateAlternatives($object); // Override
                        }
                        if ($this->isTabSubmitted('Suppliers')) {
                            $this->processSuppliers();
                        }
                        if ($this->isTabSubmitted('Features')) {
                            $this->processFeatures();
                        }
                        if ($this->isTabSubmitted('Combinations')) {
                            $this->processProductAttribute();
                        }
                        if ($this->isTabSubmitted('Prices')) {
                            $this->processPriceAddition();
                            $this->processSpecificPricePriorities();
                        }
                        if ($this->isTabSubmitted('Customization')) {
                            $this->processCustomizationConfiguration();
                        }
                        if ($this->isTabSubmitted('Attachments')) {
                            $this->processAttachments();
                        }
                        if ($this->isTabSubmitted('Images')) {
                            $this->processImageLegends();
                        }

                        $this->updatePackItems($object);
                        // Disallow avanced stock management if the product become a pack
                        if ($product_type_before == Product::PTYPE_SIMPLE && $object->getType() == Product::PTYPE_PACK) {
                            StockAvailable::setProductDependsOnStock((int)$object->id, false);
                        }
                        $this->updateDownloadProduct($object, 1);
                        $this->updateTags(Language::getLanguages(false), $object);

                        $this->aggiornaCoseNonPredefinite($object); // Override

                        if ($this->isProductFieldUpdated('category_box') && !$object->updateCategories(Tools::getValue('categoryBox'))) {
                            $this->errors[] = Tools::displayError('An error occurred while linking the object.').' <b>'.$this->table.'</b> '.Tools::displayError('To categories');
                        }
                    }

                    if ($this->isTabSubmitted('Warehouses')) {
                        $this->processWarehouses();
                    }
                    if (empty($this->errors)) {
                        if (in_array($object->visibility, array('both', 'search')) && Configuration::get('PS_SEARCH_INDEXATION')) {
                            Search::indexation(false, $object->id);
                        }

                        // Save and preview
                        if (Tools::isSubmit('submitAddProductAndPreview')) {
                            $this->redirect_after = $this->getPreviewUrl($object);
                        } else {
                            $page = (int)Tools::getValue('page');
                            // Save and stay on same form
                            if ($this->display == 'edit') {
                                $this->confirmations[] = $this->l('Update successful');
                                $this->redirect_after = self::$currentIndex.'&id_product='.(int)$this->object->id
                                    .(Tools::getIsset('id_category') ? '&id_category='.(int)Tools::getValue('id_category') : '')
                                    .'&updateproduct&conf=4&key_tab='.Tools::safeOutput(Tools::getValue('key_tab')).($page > 1 ? '&page='.(int)$page : '').'&token='.$this->token;
                            } else {
                                // Default behavior (save and back)
                                $this->redirect_after = self::$currentIndex.(Tools::getIsset('id_category') ? '&id_category='.(int)Tools::getValue('id_category') : '').'&conf=4'.($page > 1 ? '&submitFilterproduct='.(int)$page : '').'&token='.$this->token;
                            }
                        }
                    }
                    // if errors : stay on edit page
                    else {
                        $this->display = 'edit';
                    }
                } else {
                    if (!$is_associated_to_shop && $combinations) {
                        foreach ($combinations as $id_combination) {
                            $combination = new Combination((int)$id_combination['id_product_attribute']);
                            $combination->delete();
                        }
                    }
                    $this->errors[] = Tools::displayError('An error occurred while updating an object.').' <b>'.$this->table.'</b> ('.Db::getInstance()->getMsgError().')';
                }
            } else {
                $this->errors[] = Tools::displayError('An error occurred while updating an object.').' <b>'.$this->table.'</b> ('.Tools::displayError('The object cannot be loaded. ').')';
            }
            return $object;
        }
    }

    /**
     * Check that a saved product is valid
     */
    public function checkProduct()
    {
        $className = 'Product';
        // @todo : the call_user_func seems to contains only statics values (className = 'Product')
        $rules = call_user_func(array($this->className, 'getValidationRules'), $this->className);
        $default_language = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $languages = Language::getLanguages(false);

        // Check required fields
        foreach ($rules['required'] as $field) {
            if (!$this->isProductFieldUpdated($field)) {
                continue;
            }

            if (($value = Tools::getValue($field)) == false && $value != '0') {

                if(Tools::getIsset('sku_provvisorio') && $field == 'reference') { } //  Override
                else {
                    if (Tools::getValue('id_'.$this->table) && $field == 'passwd') {
                        continue;
                    }
                    $this->errors[] = sprintf(
                        Tools::displayError('The %s field is required.'),
                        call_user_func(array($className, 'displayFieldName'), $field, $className)
                    );
                }
            }
        }

        // Check multilingual required fields
        foreach ($rules['requiredLang'] as $fieldLang) {
            if ($this->isProductFieldUpdated($fieldLang, $default_language->id) && !Tools::getValue($fieldLang.'_'.$default_language->id)) {
                $this->errors[] = sprintf(
                    Tools::displayError('This %1$s field is required at least in %2$s'),
                    call_user_func(array($className, 'displayFieldName'), $fieldLang, $className),
                    $default_language->name
                );
            }
        }

        // Check fields sizes
        foreach ($rules['size'] as $field => $maxLength) {
            if ($this->isProductFieldUpdated($field) && ($value = Tools::getValue($field)) && Tools::strlen($value) > $maxLength) {
                $this->errors[] = sprintf(
                    Tools::displayError('The %1$s field is too long (%2$d chars max).'),
                    call_user_func(array($className, 'displayFieldName'), $field, $className),
                    $maxLength
                );
            }
        }

        if (Tools::getIsset('description_short') && $this->isProductFieldUpdated('description_short')) {
            $saveShort = Tools::getValue('description_short');
            $_POST['description_short'] = strip_tags(Tools::getValue('description_short'));
        }

        // Check description short size without html
        $limit = (int)Configuration::get('PS_PRODUCT_SHORT_DESC_LIMIT');
        if ($limit <= 0) {
            $limit = 400;
        }
        foreach ($languages as $language) {
            if ($this->isProductFieldUpdated('description_short', $language['id_lang']) && ($value = Tools::getValue('description_short_'.$language['id_lang']))) {
                if (Tools::strlen(strip_tags($value)) > $limit) {
                    $this->errors[] = sprintf(
                        Tools::displayError('This %1$s field (%2$s) is too long: %3$d chars max (current count %4$d).'),
                        call_user_func(array($className, 'displayFieldName'), 'description_short'),
                        $language['name'],
                        $limit,
                        Tools::strlen(strip_tags($value))
                    );
                }
            }
        }

        // Check multilingual fields sizes
        foreach ($rules['sizeLang'] as $fieldLang => $maxLength) {
            foreach ($languages as $language) {
                $value = Tools::getValue($fieldLang.'_'.$language['id_lang']);
                if ($value && Tools::strlen($value) > $maxLength) {
                    $this->errors[] = sprintf(
                        Tools::displayError('The %1$s field is too long (%2$d chars max).'),
                        call_user_func(array($className, 'displayFieldName'), $fieldLang, $className),
                        $maxLength
                    );
                }
            }
        }

        if ($this->isProductFieldUpdated('description_short') && isset($_POST['description_short'])) {
            $_POST['description_short'] = $saveShort;
        }

        // Check fields validity
        foreach ($rules['validate'] as $field => $function) {
            if ($this->isProductFieldUpdated($field) && ($value = Tools::getValue($field))) {
                $res = true;
                if (Tools::strtolower($function) == 'iscleanhtml') {
                    if (!Validate::$function($value, (int)Configuration::get('PS_ALLOW_HTML_IFRAME'))) {
                        $res = false;
                    }
                } elseif (!Validate::$function($value)) {
                    $res = false;
                }

                if (!$res) {
                    $this->errors[] = sprintf(
                        Tools::displayError('The %s field is invalid.'),
                        call_user_func(array($className, 'displayFieldName'), $field, $className)
                    );
                }
            }
        }
        // Check multilingual fields validity
        foreach ($rules['validateLang'] as $fieldLang => $function) {
            foreach ($languages as $language) {
                if ($this->isProductFieldUpdated($fieldLang, $language['id_lang']) && ($value = Tools::getValue($fieldLang.'_'.$language['id_lang']))) {
                    if (!Validate::$function($value, (int)Configuration::get('PS_ALLOW_HTML_IFRAME'))) {
                        $this->errors[] = sprintf(
                            Tools::displayError('The %1$s field (%2$s) is invalid.'),
                            call_user_func(array($className, 'displayFieldName'), $fieldLang, $className),
                            $language['name']
                        );
                    }
                }
            }
        }

        // Categories
       /* if ($this->isProductFieldUpdated('id_category_default') && (!Tools::isSubmit('categoryBox') || !count(Tools::getValue('categoryBox')))) {
            $this->errors[] = $this->l('Products must be in at least one category.');
        }
*/
        if ($this->isProductFieldUpdated('id_category_default') && (!is_array(Tools::getValue('categoryBox')) || !in_array(Tools::getValue('id_category_default'), Tools::getValue('categoryBox')))) {
            $this->errors[] = $this->l('This product must be in the default category.');
        }

        // Tags
        foreach ($languages as $language) {
            if ($value = Tools::getValue('tags_'.$language['id_lang'])) {
                //if (!Validate::isTagsList($value)) {
                if (!Validate::isTagsListModificata($value)) {
                    $this->errors[] = sprintf(
                        Tools::displayError('The tags list (%s) is invalid.'),
                        $language['name']
                    );
                }
            }
        }
    }

    /**
     * Check if a field is edited (if the checkbox is checked)
     * This method will do something only for multishop with a context all / group
     *
     * @param string $field Name of field
     * @param int $id_lang
     * @return bool
     */
    protected function isProductFieldUpdated($field, $id_lang = null)
    {
        // Cache this condition to improve performances
        static $is_activated = null;
        if (is_null($is_activated)) {
            $is_activated = Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_SHOP && $this->id_object;
        }

        if (!$is_activated) {
            return true;
        }

        $def = ObjectModel::getDefinition($this->object);
        if (!$this->object->isMultiShopField($field) && is_null($id_lang) && isset($def['fields'][$field])) {
            return true;
        }

        if (is_null($id_lang)) {
            return !empty($_POST['multishop_check'][$field]);
        } else {
            return !empty($_POST['multishop_check'][$field][$id_lang]);
        }
    }

    protected function _removeTaxFromEcotax()
    {
        if ($ecotax = Tools::getValue('ecotax')) {
            $_POST['ecotax'] = Tools::ps_round($ecotax / (1 + Tax::getProductEcotaxRate() / 100), 6);
        }
    }

    protected function _applyTaxToEcotax($product)
    {
        if ($product->ecotax) {
            $product->ecotax = Tools::ps_round($product->ecotax * (1 + Tax::getProductEcotaxRate() / 100), 2);
        }
    }

    /**
     * Update product download
     *
     * @param Product $product
     * @param int     $edit
     *
     * @return bool
     */
    public function updateDownloadProduct($product, $edit = 0)
    {
        if ((int)Tools::getValue('is_virtual_file') == 1) {
            if (isset($_FILES['virtual_product_file_uploader']) && $_FILES['virtual_product_file_uploader']['size'] > 0) {
                $virtual_product_filename = ProductDownload::getNewFilename();
                $helper = new HelperUploader('virtual_product_file_uploader');
                $helper->setPostMaxSize(Tools::getOctets(ini_get('upload_max_filesize')))
                    ->setSavePath(_PS_DOWNLOAD_DIR_)->upload($_FILES['virtual_product_file_uploader'], $virtual_product_filename);
            } else {
                $virtual_product_filename = Tools::getValue('virtual_product_filename', ProductDownload::getNewFilename());
            }

            $product->setDefaultAttribute(0);//reset cache_default_attribute
            if (Tools::getValue('virtual_product_expiration_date') && !Validate::isDate(Tools::getValue('virtual_product_expiration_date'))) {
                if (!Tools::getValue('virtual_product_expiration_date')) {
                    $this->errors[] = Tools::displayError('The expiration-date attribute is required.');
                    return false;
                }
            }

            // Trick's
            if ($edit == 1) {
                $id_product_download = (int)ProductDownload::getIdFromIdProduct((int)$product->id, false);
                if (!$id_product_download) {
                    $id_product_download = (int)Tools::getValue('virtual_product_id');
                }
            } else {
                $id_product_download = Tools::getValue('virtual_product_id');
            }

            $is_shareable = Tools::getValue('virtual_product_is_shareable');
            $virtual_product_name = Tools::getValue('virtual_product_name');
            $virtual_product_nb_days = Tools::getValue('virtual_product_nb_days');
            $virtual_product_nb_downloable = Tools::getValue('virtual_product_nb_downloable');
            $virtual_product_expiration_date = Tools::getValue('virtual_product_expiration_date');

            $download = new ProductDownload((int)$id_product_download);
            $download->id_product = (int)$product->id;
            $download->display_filename = $virtual_product_name;
            $download->filename = $virtual_product_filename;
            $download->date_add = date('Y-m-d H:i:s');
            $download->date_expiration = $virtual_product_expiration_date ? $virtual_product_expiration_date.' 23:59:59' : '';
            $download->nb_days_accessible = (int)$virtual_product_nb_days;
            $download->nb_downloadable = (int)$virtual_product_nb_downloable;
            $download->active = 1;
            $download->is_shareable = (int)$is_shareable;
            if ($download->save()) {
                return true;
            }
        } else {
            /* unactive download product if checkbox not checked */
            if ($edit == 1) {
                $id_product_download = (int)ProductDownload::getIdFromIdProduct((int)$product->id);
                if (!$id_product_download) {
                    $id_product_download = (int)Tools::getValue('virtual_product_id');
                }
            } else {
                $id_product_download = ProductDownload::getIdFromIdProduct($product->id);
            }

            /* OVERRIDE */
            // Necessario? Non basta product_download->delete ?
            Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'product_download WHERE id_product = '.$product->id);

            if (!empty($id_product_download)) {
                //return $product_download->delete();
                $product_download->delete();
            }
            /* FINE OVERRIDE */

            /*if (!empty($id_product_download)) {
                $product_download = new ProductDownload((int)$id_product_download);
                $product_download->date_expiration = date('Y-m-d H:i:s', time() - 1);
                $product_download->active = 0;
                return $product_download->save();
            }*/
        }
        return false;
    }

    /**
     * Update product accessories
     *
     * @param object $product Product
     */
    public function updateAccessories($product)
    {
        $product->deleteAccessories();
        if ($accessories = Tools::getValue('inputAccessories')) {
            $accessories_id = array_unique(explode('-', $accessories));
            if (count($accessories_id)) {
                array_pop($accessories_id);
                $product->changeAccessories($accessories_id);
            }
        }
    }

    // Override; Update product alternatives - come Accessories
    public function updateAlternatives($product)
    {
        //$product->deleteAlternatives();
        if ($alternatives = Tools::getValue('inputAlternatives')) {
            $alternatives_id = array_unique(explode('-', $alternatives));
            if (count($alternatives_id)) {
                array_pop($alternatives_id);
                $product->changeAlternatives($alternatives_id);
            }
        }
    }

    /**
     * Update product tags
     *
     * @param array $languages Array languages
     * @param object $product Product
     * @return bool Update result
     */
    public function updateTags($languages, $product)
    {
        $tag_success = true;
        /* Reset all tags for THIS product */
        if (!Tag::deleteTagsForProduct((int)$product->id)) {
            $this->errors[] = Tools::displayError('An error occurred while attempting to delete previous tags.');
        }
        /* Assign tags to this product */
        foreach ($languages as $language) {
            // CORREGGERE: se lang = ita ok questo, altrimenti Product::addTagAuto($product->id, 5); chiedere a federico e correggere la funzione nella classe
            if ($value = Tools::getValue('tags_'.$language['id_lang'])) {
                $tag_success &= Tag::addTags($language['id_lang'], (int)$product->id, $value);
            }
        }

        if (!$tag_success) {
            $this->errors[] = Tools::displayError('An error occurred while adding tags.');
        }

        return $tag_success;
    }

    public function initContent($token = null)
    {
        if ($this->display == 'edit' || $this->display == 'add') {
            $this->fields_form = array();

            // Check if Module
            if (substr($this->tab_display, 0, 6) == 'Module') {
                $this->tab_display_module = strtolower(substr($this->tab_display, 6, Tools::strlen($this->tab_display) - 6));
                $this->tab_display = 'Modules';
            }
            if (method_exists($this, 'initForm'.$this->tab_display)) {
                $this->tpl_form = strtolower($this->tab_display).'.tpl';
            }

            if ($this->ajax) {
                $this->content_only = true;
            } else {
                $product_tabs = array();

                // tab_display defines which tab to display first
                if (!method_exists($this, 'initForm'.$this->tab_display)) {
                    $this->tab_display = $this->default_tab;
                }

                $advanced_stock_management_active = Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT');
                foreach ($this->available_tabs as $product_tab => $value) {
                    // if it's the warehouses tab and advanced stock management is disabled, continue
                    if ($advanced_stock_management_active == 0 && $product_tab == 'Warehouses') {
                        continue;
                    }

                    $product_tabs[$product_tab] = array(
                        'id' => $product_tab,
                        'selected' => (strtolower($product_tab) == strtolower($this->tab_display) || (isset($this->tab_display_module) && 'module'.$this->tab_display_module == Tools::strtolower($product_tab))),
                        'name' => $this->available_tabs_lang[$product_tab],
                        'href' => $this->context->link->getAdminLink('AdminProducts').'&id_product='.(int)Tools::getValue('id_product').'&action='.$product_tab,
                    );
                }
                $this->tpl_form_vars['product_tabs'] = $product_tabs;

                /* OVERRIDE */

                if(Tools::getIsset('id_product')){
                    $scadenze_speciali_vendita = Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_."specific_price sp WHERE id_product = ".Tools::getValue('id_product')." AND sp.to != '0000-00-00 00:00:00' AND sp.to < '".date('Y-m-d H:i:s')."'");
                    $scadenze_speciali_acquisto = Db::getInstance()->executeS("SELECT * FROM "._DB_PREFIX_."specific_price_wholesale spw WHERE id_product = ".Tools::getValue('id_product')." AND spw.to != '0000-00-00 00:00:00' AND spw.to < '".date('Y-m-d H:i:s')."'");
    
                    foreach($scadenze_speciali_vendita as $scadenza_vendita) {
                        $this->errors[] = Tools::displayError("ATTENZIONE: scaduto in data ".date("d/m/Y", strtotime($scadenza_vendita['to']))." prezzo speciale VENDITA di euro ".($scadenza_vendita['reduction_type'] == 'amount' ? str_replace(".",",",($scadenza_vendita['price'] - $scadenza_vendita['reduction'])) : str_replace(".",",",($scadenza_vendita['price'] - ($scadenza_vendita['price']*$scadenza_vendita['reduction'])))).". Consulta il tab Prezzi per informazioni.");
                    }
    
                    foreach($scadenze_speciali_acquisto as $scadenza_acquisto) {
                        $this->errors[] = Tools::displayError("ATTENZIONE: scaduto in data ".date("d/m/Y", strtotime($scadenza_acquisto['to']))." prezzo speciale ACQUISTO di euro ".str_replace(".",",",$scadenza_acquisto['wholesale_price']).". Consulta il tab Prezzi per informazioni.");
                    }
                }

                /* FINE OVERRIDE */
            }
        } else {
            if ($id_category = (int)$this->id_current_category) {
                self::$currentIndex .= '&id_category='.(int)$this->id_current_category;
            }

            // If products from all categories are displayed, we don't want to use sorting by position
            if (!$id_category) {
                $this->_defaultOrderBy = $this->identifier;
                if ($this->context->cookie->{$this->table.'Orderby'} == 'position') {
                    unset($this->context->cookie->{$this->table.'Orderby'});
                    unset($this->context->cookie->{$this->table.'Orderway'});
                }
            }
            if (!$id_category) {
                $id_category = Configuration::get('PS_ROOT_CATEGORY');
            }
            $this->tpl_list_vars['is_category_filter'] = (bool)$this->id_current_category;

            // Generate category selection tree
            $tree = new HelperTreeCategories('categories-tree', $this->l('Filter by category'));
            $tree->setAttribute('is_category_filter', (bool)$this->id_current_category)
                ->setAttribute('base_url', preg_replace('#&id_category=[0-9]*#', '', self::$currentIndex).'&token='.$this->token)
                ->setInputName('id-category')
                ->setRootCategory(Category::getRootCategory()->id)
                ->setSelectedCategories(array((int)$id_category));
            $this->tpl_list_vars['category_tree'] = $tree->render();

            // used to build the new url when changing category
            $this->tpl_list_vars['base_url'] = preg_replace('#&id_category=[0-9]*#', '', self::$currentIndex).'&token='.$this->token;
        }
        // @todo module free
        $this->tpl_form_vars['vat_number'] = file_exists(_PS_MODULE_DIR_.'vatnumber/ajax.php');

        parent::initContent();
    }

    public function renderKpis()
    {
        $time = time();
        $kpis = array();

        /* The data generation is located in AdminStatsControllerCore */

        if (Configuration::get('PS_STOCK_MANAGEMENT')) {
            $helper = new HelperKpi();
            $helper->id = 'box-products-stock';
            $helper->icon = 'icon-archive';
            $helper->color = 'color1';
            $helper->title = $this->l('Out of stock items', null, null, false);
            if (ConfigurationKPI::get('PERCENT_PRODUCT_OUT_OF_STOCK') !== false) {
                $helper->value = ConfigurationKPI::get('PERCENT_PRODUCT_OUT_OF_STOCK');
            }
            $helper->source = $this->context->link->getAdminLink('AdminStats').'&ajax=1&action=getKpi&kpi=percent_product_out_of_stock';
            $helper->tooltip = $this->l('X% of your products for sale are out of stock.', null, null, false);
            $helper->refresh = (bool)(ConfigurationKPI::get('PERCENT_PRODUCT_OUT_OF_STOCK_EXPIRE') < $time);
            $helper->href = Context::getContext()->link->getAdminLink('AdminProducts').'&productFilter_sav!quantity=0&productFilter_active=1&submitFilterproduct=1';
            $kpis[] = $helper->generate();
        }

        $helper = new HelperKpi();
        $helper->id = 'box-avg-gross-margin';
        $helper->icon = 'icon-tags';
        $helper->color = 'color2';
        $helper->title = $this->l('Average Gross Margin %', null, null, false);
        if (ConfigurationKPI::get('PRODUCT_AVG_GROSS_MARGIN') !== false) {
            $helper->value = ConfigurationKPI::get('PRODUCT_AVG_GROSS_MARGIN');
        }
        $helper->source = $this->context->link->getAdminLink('AdminStats').'&ajax=1&action=getKpi&kpi=product_avg_gross_margin';
        $helper->tooltip = $this->l('Gross margin expressed in percentage assesses how cost-effectively you sell your goods. Out of $100, you will retain $X to cover profit and expenses.', null, null, false);
        $helper->refresh = (bool)(ConfigurationKPI::get('PRODUCT_AVG_GROSS_MARGIN_EXPIRE') < $time);
        $kpis[] = $helper->generate();

        $helper = new HelperKpi();
        $helper->id = 'box-8020-sales-catalog';
        $helper->icon = 'icon-beaker';
        $helper->color = 'color3';
        $helper->title = $this->l('Purchased references', null, null, false);
        $helper->subtitle = $this->l('30 days', null, null, false);
        if (ConfigurationKPI::get('8020_SALES_CATALOG') !== false) {
            $helper->value = ConfigurationKPI::get('8020_SALES_CATALOG');
        }
        $helper->source = $this->context->link->getAdminLink('AdminStats').'&ajax=1&action=getKpi&kpi=8020_sales_catalog';
        $helper->tooltip = $this->l('X% of your references have been purchased for the past 30 days', null, null, false);
        $helper->refresh = (bool)(ConfigurationKPI::get('8020_SALES_CATALOG_EXPIRE') < $time);
        if (Module::isInstalled('statsbestproducts')) {
            $helper->href = Context::getContext()->link->getAdminLink('AdminStats').'&module=statsbestproducts&datepickerFrom='.date('Y-m-d', strtotime('-30 days')).'&datepickerTo='.date('Y-m-d');
        }
        $kpis[] = $helper->generate();

        $helper = new HelperKpi();
        $helper->id = 'box-disabled-products';
        $helper->icon = 'icon-off';
        $helper->color = 'color4';
        $helper->href = $this->context->link->getAdminLink('AdminProducts');
        $helper->title = $this->l('Disabled Products', null, null, false);
        if (ConfigurationKPI::get('DISABLED_PRODUCTS') !== false) {
            $helper->value = ConfigurationKPI::get('DISABLED_PRODUCTS');
        }
        $helper->source = $this->context->link->getAdminLink('AdminStats').'&ajax=1&action=getKpi&kpi=disabled_products';
        $helper->refresh = (bool)(ConfigurationKPI::get('DISABLED_PRODUCTS_EXPIRE') < $time);
        $helper->tooltip = $this->l('X% of your products are disabled and not visible to your customers', null, null, false);
        $helper->href = Context::getContext()->link->getAdminLink('AdminProducts').'&productFilter_active=0&submitFilterproduct=1';
        $kpis[] = $helper->generate();

        $helper = new HelperKpiRow();
        $helper->kpis = $kpis;
        return $helper->generate();
    }

    public function renderList()
    {
        // test: inserisce tutti i prodotti in ps_product_shop
        /*$prodotti = Db::getInstance()->executeS('SELECT id_product, id_category_default, price, wholesale_price, active FROM ps_product WHERE id_product NOT IN (SELECT s.id_product FROM ps_product_shop s)');
        foreach($prodotti as $p){
            Db::getInstance()->execute('
                INSERT INTO ps_product_shop (id_product, id_shop, id_category_default, id_tax_rules_group, price, wholesale_price, active) 
                VALUES ('.$p['id_product'].', 1, '.$p['id_category_default'].', 1, '.$p['price'].', '.$p['wholesale_price'].', '.$p['active'].')
            ');
        }*/

        $this->addRowAction('edit');
        $this->addRowAction('preview');
        $this->addRowAction('duplicate');
        $this->addRowAction('delete');
        return parent::renderList();
    }

    public function ajaxProcessProductManufacturers()
    {
        $manufacturers = Manufacturer::getManufacturers(false, 0, true, false, false, false, true);
        $jsonArray = array();

        if ($manufacturers) {
            foreach ($manufacturers as $manufacturer) {
                $tmp = array("optionValue" => $manufacturer['id_manufacturer'], "optionDisplay" => htmlspecialchars(trim($manufacturer['name'])));
                $jsonArray[] = Tools::jsonEncode($tmp);
            }
        }

        die('['.implode(',', $jsonArray).']');
    }

    /**
     * Build a categories tree
     *
     * @param       $id_obj
     * @param array $indexedCategories   Array with categories where product is indexed (in order to check checkbox)
     * @param array $categories          Categories to list
     * @param       $current
     * @param null  $id_category         Current category ID
     * @param null  $id_category_default
     * @param array $has_suite
     *
     * @return string
     */
    public static function recurseCategoryForInclude($id_obj, $indexedCategories, $categories, $current, $id_category = null, $id_category_default = null, $has_suite = array())
    {
        global $done;
        static $irow;
        $content = '';

        if (!$id_category) {
            $id_category = (int)Configuration::get('PS_ROOT_CATEGORY');
        }

        if (!isset($done[$current['infos']['id_parent']])) {
            $done[$current['infos']['id_parent']] = 0;
        }
        $done[$current['infos']['id_parent']] += 1;

        $todo = count($categories[$current['infos']['id_parent']]);
        $doneC = $done[$current['infos']['id_parent']];

        $level = $current['infos']['level_depth'] + 1;

        $content .= '
		<tr class="'.($irow++ % 2 ? 'alt_row' : '').'">
			<td>
				<input type="checkbox" name="categoryBox[]" class="categoryBox'.($id_category_default == $id_category ? ' id_category_default' : '').'" id="categoryBox_'.$id_category.'" value="'.$id_category.'"'.((in_array($id_category, $indexedCategories) || ((int)(Tools::getValue('id_category')) == $id_category && !(int)($id_obj))) ? ' checked="checked"' : '').' />
			</td>
			<td>
				'.$id_category.'
			</td>
			<td>';
        for ($i = 2; $i < $level; $i++) {
            $content .= '<img src="../img/admin/lvl_'.$has_suite[$i - 2].'.gif" alt="" />';
        }
        $content .= '<img src="../img/admin/'.($level == 1 ? 'lv1.gif' : 'lv2_'.($todo == $doneC ? 'f' : 'b').'.gif').'" alt="" /> &nbsp;
			<label for="categoryBox_'.$id_category.'" class="t">'.stripslashes($current['infos']['name']).'</label></td>
		</tr>';

        if ($level > 1) {
            $has_suite[] = ($todo == $doneC ? 0 : 1);
        }
        if (isset($categories[$id_category])) {
            foreach ($categories[$id_category] as $key => $row) {
                if ($key != 'infos') {
                    $content .= AdminProductsController::recurseCategoryForInclude($id_obj, $indexedCategories, $categories, $categories[$id_category][$key], $key, $id_category_default, $has_suite);
                }
            }
        }
        return $content;
    }

    protected function _displayDraftWarning($active)
    {
        $content = '<div class="warn draft" style="'.($active ? 'display:none' : '').'">
				<span>'.$this->l('Your product will be saved as a draft.').'</span>
				<a href="#" class="btn btn-default pull-right" onclick="submitAddProductAndPreview()" ><i class="icon-external-link-sign"></i> '.$this->l('Save and preview').'</a>
				<input type="hidden" name="fakeSubmitAddProductAndPreview" id="fakeSubmitAddProductAndPreview" />
	 		</div>';
        $this->tpl_form_vars['draft_warning'] = $content;
    }

    public function initPageHeaderToolbar()
    {
        if (empty($this->display)) {
            $this->page_header_toolbar_btn['new_product'] = array(
                    'href' => self::$currentIndex.'&addproduct&token='.$this->token,
                    'desc' => $this->l('Add new product', null, null, false),
                    'icon' => 'process-icon-new'
                );
        }
        if ($this->display == 'edit') {
            if (($product = $this->loadObject(true)) && $product->isAssociatedToShop()) {
                // adding button for preview this product
                if ($url_preview = $this->getPreviewUrl($product)) {
                    $this->page_header_toolbar_btn['preview'] = array(
                        'short' => $this->l('Preview', null, null, false),
                        'href' => $url_preview,
                        'desc' => $this->l('Preview', null, null, false),
                        'target' => true,
                        'class' => 'previewUrl'
                    );
                }

                $js = (bool)Image::getImages($this->context->language->id, (int)$product->id) ?
                    'confirm_link(\'\', \''.$this->l('This will copy the images too. If you wish to proceed, click "Yes". If not, click "No".', null, true, false).'\', \''.$this->l('Yes', null, true, false).'\', \''.$this->l('No', null, true, false).'\', \''.$this->context->link->getAdminLink('AdminProducts', true).'&id_product='.(int)$product->id.'&duplicateproduct'.'\', \''.$this->context->link->getAdminLink('AdminProducts', true).'&id_product='.(int)$product->id.'&duplicateproduct&noimage=1'.'\')'
                    :
                    'document.location = \''.$this->context->link->getAdminLink('AdminProducts', true).'&id_product='.(int)$product->id.'&duplicateproduct&noimage=1'.'\'';

                // adding button for duplicate this product
                if ($this->tabAccess['add']) {
                    $this->page_header_toolbar_btn['duplicate'] = array(
                        'short' => $this->l('Duplicate', null, null, false),
                        'desc' => $this->l('Duplicate', null, null, false),
                        'confirm' => 1,
                        'js' => $js
                    );
                }

                // adding button for preview this product statistics
                if (file_exists(_PS_MODULE_DIR_.'statsproduct/statsproduct.php')) {
                    $this->page_header_toolbar_btn['stats'] = array(
                    'short' => $this->l('Statistics', null, null, false),
                    'href' => $this->context->link->getAdminLink('AdminStats').'&module=statsproduct&id_product='.(int)$product->id,
                    'desc' => $this->l('Product sales', null, null, false),
                );
                }

                // adding button for delete this product
                if ($this->tabAccess['delete']) {
                    $this->page_header_toolbar_btn['delete'] = array(
                        'short' => $this->l('Delete', null, null, false),
                        'href' => $this->context->link->getAdminLink('AdminProducts').'&id_product='.(int)$product->id.'&deleteproduct',
                        'desc' => $this->l('Delete this product', null, null, false),
                        'confirm' => 1,
                        'js' => 'if (confirm(\''.$this->l('Delete product?', null, true, false).'\')){return true;}else{event.preventDefault();}'
                    );
                }
            }
        }
        parent::initPageHeaderToolbar();
    }

    public function initToolbar()
    {
        parent::initToolbar();
        if ($this->display == 'edit' || $this->display == 'add') {
            $this->toolbar_btn['save'] = array(
                'short' => 'Save',
                'href' => '#',
                'desc' => $this->l('Save'),
            );

            $this->toolbar_btn['save-and-stay'] = array(
                'short' => 'SaveAndStay',
                'href' => '#',
                'desc' => $this->l('Save and stay'),
            );

            // adding button for adding a new combination in Combination tab
            $this->toolbar_btn['newCombination'] = array(
                'short' => 'New combination',
                'desc' => $this->l('New combination'),
                'class' => 'toolbar-new'
            );
        } elseif ($this->can_import) {
            $this->toolbar_btn['import'] = array(
                'href' => $this->context->link->getAdminLink('AdminImport', true).'&import_type=products',
                'desc' => $this->l('Import')
            );
        }

        $this->context->smarty->assign('toolbar_scroll', 1);
        $this->context->smarty->assign('show_toolbar', 1);
        $this->context->smarty->assign('toolbar_btn', $this->toolbar_btn);
    }

    /**
     * renderForm contains all necessary initialization needed for all tabs
     *
     * @return string|void
     * @throws PrestaShopException
     */
    public function renderForm()
    {
        // This nice code (irony) is here to store the product name, because the row after will erase product name in multishop context
        $this->product_name = $this->object->name[$this->context->language->id];

        if (!method_exists($this, 'initForm'.$this->tab_display)) {
            return;
        }

        $product = $this->object;

        // Product for multishop
        $this->context->smarty->assign('bullet_common_field', '');
        if (Shop::isFeatureActive() && $this->display == 'edit') {
            if (Shop::getContext() != Shop::CONTEXT_SHOP) {
                $this->context->smarty->assign(array(
                    'display_multishop_checkboxes' => true,
                    'multishop_check' => Tools::getValue('multishop_check'),
                ));
            }

            if (Shop::getContext() != Shop::CONTEXT_ALL) {
                $this->context->smarty->assign('bullet_common_field', '<i class="icon-circle text-orange"></i>');
                $this->context->smarty->assign('display_common_field', true);
            }
        }

        $this->tpl_form_vars['tabs_preloaded'] = $this->available_tabs;

        $this->tpl_form_vars['product_type'] = (int)Tools::getValue('type_product', $product->getType());

        $this->getLanguages();

        $this->tpl_form_vars['id_lang_default'] = Configuration::get('PS_LANG_DEFAULT');

        $this->tpl_form_vars['currentIndex'] = self::$currentIndex;
        $this->tpl_form_vars['display_multishop_checkboxes'] = (Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_SHOP && $this->display == 'edit');
        $this->fields_form = array('');

        $this->tpl_form_vars['token'] = $this->token;
        $this->tpl_form_vars['combinationImagesJs'] = $this->getCombinationImagesJs();
        $this->tpl_form_vars['PS_ALLOW_ACCENTED_CHARS_URL'] = (int)Configuration::get('PS_ALLOW_ACCENTED_CHARS_URL');
        $this->tpl_form_vars['post_data'] = Tools::jsonEncode($_POST);
        $this->tpl_form_vars['save_error'] = !empty($this->errors);
        $this->tpl_form_vars['mod_evasive'] = Tools::apacheModExists('evasive');
        $this->tpl_form_vars['mod_security'] = Tools::apacheModExists('security');
        $this->tpl_form_vars['ps_force_friendly_product'] = Configuration::get('PS_FORCE_FRIENDLY_PRODUCT');

        // autoload rich text editor (tiny mce)
        $this->tpl_form_vars['tinymce'] = true;
        $iso = $this->context->language->iso_code;
        $this->tpl_form_vars['iso'] = file_exists(_PS_CORE_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en';
        $this->tpl_form_vars['path_css'] = _THEME_CSS_DIR_;
        $this->tpl_form_vars['ad'] = __PS_BASE_URI__.basename(_PS_ADMIN_DIR_);

        if (Validate::isLoadedObject(($this->object))) {
            $id_product = (int)$this->object->id;
        } else {
            $id_product = (int)Tools::getvalue('id_product');
        }

        $page = (int)Tools::getValue('page');

        $this->tpl_form_vars['form_action'] = $this->context->link->getAdminLink('AdminProducts').'&'.($id_product ? 'updateproduct&id_product='.(int)$id_product : 'addproduct').($page > 1 ? '&page='.(int)$page : '');
        $this->tpl_form_vars['id_product'] = $id_product;

        // Transform configuration option 'upload_max_filesize' in octets
        $upload_max_filesize = Tools::getOctets(ini_get('upload_max_filesize'));

        // Transform configuration option 'upload_max_filesize' in MegaOctets
        $upload_max_filesize = ($upload_max_filesize / 1024) / 1024;

        $this->tpl_form_vars['upload_max_filesize'] = $upload_max_filesize;
        $this->tpl_form_vars['country_display_tax_label'] = $this->context->country->display_tax_label;
        $this->tpl_form_vars['has_combinations'] = $this->object->hasAttributes();
        $this->product_exists_in_shop = true;

        if ($this->display == 'edit' && Validate::isLoadedObject($product) && Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP && !$product->isAssociatedToShop($this->context->shop->id)) {
            $this->product_exists_in_shop = false;
            if ($this->tab_display == 'Informations') {
                $this->displayWarning($this->l('Warning: The product does not exist in this shop'));
            }

            $default_product = new Product();
            $definition = ObjectModel::getDefinition($product);
            foreach ($definition['fields'] as $field_name => $field) {
                if (isset($field['shop']) && $field['shop']) {
                    $product->$field_name = ObjectModel::formatValue($default_product->$field_name, $field['type']);
                }
            }
        }

        // let's calculate this once for all
        if (!Validate::isLoadedObject($this->object) && Tools::getValue('id_product')) {
            $this->errors[] = 'Unable to load object';
        } else {
            $this->_displayDraftWarning($this->object->active);

            // if there was an error while saving, we don't want to lose posted data
            if (!empty($this->errors)) {
                $this->copyFromPost($this->object, $this->table);
            }

            $this->initPack($this->object);
            $this->{'initForm'.$this->tab_display}($this->object);
            $this->tpl_form_vars['product'] = $this->object;

            if ($this->ajax) {
                if (!isset($this->tpl_form_vars['custom_form'])) {
                    throw new PrestaShopException('custom_form empty for action '.$this->tab_display);
                } else {
                    return $this->tpl_form_vars['custom_form'];
                }
            }
        }

        $parent = parent::renderForm();
        $this->addJqueryPlugin(array('autocomplete', 'fancybox', 'typewatch'));
        return $parent;
    }

    public function getPreviewUrl(Product $product)
    {
        $id_lang = Configuration::get('PS_LANG_DEFAULT', null, null, Context::getContext()->shop->id);

        if (!ShopUrl::getMainShopDomain()) {
            return false;
        }

        $is_rewrite_active = (bool)Configuration::get('PS_REWRITING_SETTINGS');
        $preview_url = $this->context->link->getProductLink(
            $product,
            $this->getFieldValue($product, 'link_rewrite', $this->context->language->id),
            Category::getLinkRewrite($this->getFieldValue($product, 'id_category_default'), $this->context->language->id),
            null,
            $id_lang,
            (int)Context::getContext()->shop->id,
            0,
            $is_rewrite_active
        );

        if (!$product->active) {
            $admin_dir = dirname($_SERVER['PHP_SELF']);
            $admin_dir = substr($admin_dir, strrpos($admin_dir, '/') + 1);
            $preview_url .= ((strpos($preview_url, '?') === false) ? '?' : '&').'adtoken='.$this->token.'&ad='.$admin_dir.'&id_employee='.(int)$this->context->employee->id;
        }

        return $preview_url;
    }

    /**
    * Post treatment for suppliers
    */
    public function processSuppliers()
    {
        if ((int)Tools::getValue('supplier_loaded') === 1 && Validate::isLoadedObject($product = new Product((int)Tools::getValue('id_product')))) {
            // Get all id_product_attribute
            $attributes = $product->getAttributesResume($this->context->language->id);
            if (empty($attributes)) {
                $attributes[] = array(
                    'id_product_attribute' => 0,
                    'attribute_designation' => ''
                );
            }

            // Get all available suppliers
            $suppliers = Supplier::getSuppliers();

            // Get already associated suppliers
            $associated_suppliers = ProductSupplier::getSupplierCollection($product->id);

            $suppliers_to_associate = array();
            $new_default_supplier = 0;

            if (Tools::isSubmit('default_supplier')) {
                $new_default_supplier = (int)Tools::getValue('default_supplier');
            }

            // Get new associations
            foreach ($suppliers as $supplier) {
                if (Tools::isSubmit('check_supplier_'.$supplier['id_supplier'])) {
                    $suppliers_to_associate[] = $supplier['id_supplier'];
                }
            }

            // Delete already associated suppliers if needed
            foreach ($associated_suppliers as $key => $associated_supplier) {
                /** @var ProductSupplier $associated_supplier */
                if (!in_array($associated_supplier->id_supplier, $suppliers_to_associate)) {
                    $associated_supplier->delete();
                    unset($associated_suppliers[$key]);
                }
            }

            // Associate suppliers
            foreach ($suppliers_to_associate as $id) {
                $to_add = true;
                foreach ($associated_suppliers as $as) {
                    /** @var ProductSupplier $as */
                    if ($id == $as->id_supplier) {
                        $to_add = false;
                    }
                }

                if ($to_add) {
                    $product_supplier = new ProductSupplier();
                    $product_supplier->id_product = $product->id;
                    $product_supplier->id_product_attribute = 0;
                    $product_supplier->id_supplier = $id;
                    if ($this->context->currency->id) {
                        $product_supplier->id_currency = (int)$this->context->currency->id;
                    } else {
                        $product_supplier->id_currency = (int)Configuration::get('PS_CURRENCY_DEFAULT');
                    }
                    $product_supplier->save();

                    $associated_suppliers[] = $product_supplier;
                    foreach ($attributes as $attribute) {
                        if ((int)$attribute['id_product_attribute'] > 0) {
                            $product_supplier = new ProductSupplier();
                            $product_supplier->id_product = $product->id;
                            $product_supplier->id_product_attribute = (int)$attribute['id_product_attribute'];
                            $product_supplier->id_supplier = $id;
                            $product_supplier->save();
                        }
                    }
                }
            }

            // Manage references and prices
            foreach ($attributes as $attribute) {
                foreach ($associated_suppliers as $supplier) {
                    /** @var ProductSupplier $supplier */
                    if (Tools::isSubmit('supplier_reference_'.$product->id.'_'.$attribute['id_product_attribute'].'_'.$supplier->id_supplier) ||
                        (Tools::isSubmit('product_price_'.$product->id.'_'.$attribute['id_product_attribute'].'_'.$supplier->id_supplier) &&
                         Tools::isSubmit('product_price_currency_'.$product->id.'_'.$attribute['id_product_attribute'].'_'.$supplier->id_supplier))) {
                        $reference = pSQL(
                            Tools::getValue(
                                'supplier_reference_'.$product->id.'_'.$attribute['id_product_attribute'].'_'.$supplier->id_supplier,
                                ''
                            )
                        );

                        $price = (float)str_replace(
                            array(' ', ','),
                            array('', '.'),
                            Tools::getValue(
                                'product_price_'.$product->id.'_'.$attribute['id_product_attribute'].'_'.$supplier->id_supplier,
                                0
                            )
                        );

                        $price = Tools::ps_round($price, 6);

                        $id_currency = (int)Tools::getValue(
                            'product_price_currency_'.$product->id.'_'.$attribute['id_product_attribute'].'_'.$supplier->id_supplier,
                            0
                        );

                        if ($id_currency <= 0 || (!($result = Currency::getCurrency($id_currency)) || empty($result))) {
                            $this->errors[] = Tools::displayError('The selected currency is not valid');
                        }

                        // Save product-supplier data
                        $product_supplier_id = (int)ProductSupplier::getIdByProductAndSupplier($product->id, $attribute['id_product_attribute'], $supplier->id_supplier);

                        if (!$product_supplier_id) {
                            $product->addSupplierReference($supplier->id_supplier, (int)$attribute['id_product_attribute'], $reference, (float)$price, (int)$id_currency);
                            if ($product->id_supplier == $supplier->id_supplier) {
                                if ((int)$attribute['id_product_attribute'] > 0) {
                                    $data = array(
                                        'supplier_reference' => pSQL($reference),
                                        'wholesale_price' => (float)Tools::convertPrice($price, $id_currency)
                                    );
                                    $where = '
										a.id_product = '.(int)$product->id.'
										AND a.id_product_attribute = '.(int)$attribute['id_product_attribute'];
                                    ObjectModel::updateMultishopTable('Combination', $data, $where);
                                } else {
                                    $product->wholesale_price = (float)Tools::convertPrice($price, $id_currency); //converted in the default currency
                                    $product->supplier_reference = pSQL($reference);
                                    $product->update();
                                }
                            }
                        } else {
                            $product_supplier = new ProductSupplier($product_supplier_id);
                            $product_supplier->id_currency = (int)$id_currency;
                            $product_supplier->product_supplier_price_te = (float)$price;
                            $product_supplier->product_supplier_reference = pSQL($reference);
                            $product_supplier->update();
                        }
                    } elseif (Tools::isSubmit('supplier_reference_'.$product->id.'_'.$attribute['id_product_attribute'].'_'.$supplier->id_supplier)) {
                        //int attribute with default values if possible
                        if ((int)$attribute['id_product_attribute'] > 0) {
                            $product_supplier = new ProductSupplier();
                            $product_supplier->id_product = $product->id;
                            $product_supplier->id_product_attribute = (int)$attribute['id_product_attribute'];
                            $product_supplier->id_supplier = $supplier->id_supplier;
                            $product_supplier->save();
                        }
                    }
                }
            }
            // Manage defaut supplier for product
            if ($new_default_supplier != $product->id_supplier) {
                $this->object->id_supplier = $new_default_supplier;
                $this->object->update();
            }
        }
    }

    /**
    * Post treatment for warehouses
    */
    public function processWarehouses()
    {
        if ((int)Tools::getValue('warehouse_loaded') === 1 && Validate::isLoadedObject($product = new Product((int)$id_product = Tools::getValue('id_product')))) {
            // Get all id_product_attribute
            $attributes = $product->getAttributesResume($this->context->language->id);
            if (empty($attributes)) {
                $attributes[] = array(
                    'id_product_attribute' => 0,
                    'attribute_designation' => ''
                );
            }

            // Get all available warehouses
            $warehouses = Warehouse::getWarehouses(true);

            // Get already associated warehouses
            $associated_warehouses_collection = WarehouseProductLocation::getCollection($product->id);

            $elements_to_manage = array();

            // get form inforamtion
            foreach ($attributes as $attribute) {
                foreach ($warehouses as $warehouse) {
                    $key = $warehouse['id_warehouse'].'_'.$product->id.'_'.$attribute['id_product_attribute'];

                    // get elements to manage
                    if (Tools::isSubmit('check_warehouse_'.$key)) {
                        $location = Tools::getValue('location_warehouse_'.$key, '');
                        $elements_to_manage[$key] = $location;
                    }
                }
            }

            // Delete entry if necessary
            foreach ($associated_warehouses_collection as $awc) {
                /** @var WarehouseProductLocation $awc */
                if (!array_key_exists($awc->id_warehouse.'_'.$awc->id_product.'_'.$awc->id_product_attribute, $elements_to_manage)) {
                    $awc->delete();
                }
            }

            // Manage locations
            foreach ($elements_to_manage as $key => $location) {
                $params = explode('_', $key);

                $wpl_id = (int)WarehouseProductLocation::getIdByProductAndWarehouse((int)$params[1], (int)$params[2], (int)$params[0]);

                if (empty($wpl_id)) {
                    //create new record
                    $warehouse_location_entity = new WarehouseProductLocation();
                    $warehouse_location_entity->id_product = (int)$params[1];
                    $warehouse_location_entity->id_product_attribute = (int)$params[2];
                    $warehouse_location_entity->id_warehouse = (int)$params[0];
                    $warehouse_location_entity->location = pSQL($location);
                    $warehouse_location_entity->save();
                } else {
                    $warehouse_location_entity = new WarehouseProductLocation((int)$wpl_id);

                    $location = pSQL($location);

                    if ($location != $warehouse_location_entity->location) {
                        $warehouse_location_entity->location = pSQL($location);
                        $warehouse_location_entity->update();
                    }
                }
            }
            StockAvailable::synchronize((int)$id_product);
        }
    }

    /**
     * @param Product $obj
     * @throws Exception
     * @throws PrestaShopException
     * @throws SmartyException
     */
    public function initFormAssociations($obj)
    {
        $product = $obj;
        $data = $this->createTemplate($this->tpl_form);
        // Prepare Categories tree for display in Associations tab
        $root = Category::getRootCategory();
        $default_category = $this->context->cookie->id_category_products_filter ? $this->context->cookie->id_category_products_filter : Context::getContext()->shop->id_category;
        if (!$product->id || !$product->isAssociatedToShop()) {
            $selected_cat = Category::getCategoryInformations(Tools::getValue('categoryBox', array($default_category)), $this->default_form_language);
        } else {
            if (Tools::isSubmit('categoryBox')) {
                $selected_cat = Category::getCategoryInformations(Tools::getValue('categoryBox', array($default_category)), $this->default_form_language);
            } else {
                $selected_cat = Product::getProductCategoriesFull($product->id, $this->default_form_language);
            }
        }

        // Multishop block
        $data->assign('feature_shop_active', Shop::isFeatureActive());
        $helper = new HelperForm();
        if ($this->object && $this->object->id) {
            $helper->id = $this->object->id;
        } else {
            $helper->id = null;
        }
        $helper->table = $this->table;
        $helper->identifier = $this->identifier;

        // Accessories block
        $accessories = Product::getAccessoriesLight($this->context->language->id, $product->id);

        if ($post_accessories = Tools::getValue('inputAccessories')) {
            $post_accessories_tab = explode('-', $post_accessories);
            foreach ($post_accessories_tab as $accessory_id) {
                if (!$this->haveThisAccessory($accessory_id, $accessories) && $accessory = Product::getAccessoryById($accessory_id)) {
                    $accessories[] = $accessory;
                }
            }
        }
        $data->assign('accessories', $accessories);

        /* OVERRIDE */

        // Categoria gestionale
        $categories_gst = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'category_lang WHERE id_lang = '.$this->context->language->id.' ORDER BY name ASC');
        
        $data->assign('categories_gst', $categories_gst);

        // Prodotti alternativi
        $alternatives = Product::getAlternativesLightAll($this->context->language->id, $product->id);

        if ($post_alternatives = Tools::getValue('inputAlternatives')) {
            $post_alternatives_tab = explode('-', $post_alternatives);
            foreach ($post_accessories_tab as $alternative_id) {
                if (!$this->haveThisAccessory($alternative_id, $alternatives) && $alternative = Product::getAlternativeById($alternative_id)) {
                    $alternatives[] = $alternative;
                }
            }
        }
        $data->assign('alternatives', $alternatives);

        /* FINE OVERRIDE */

        //$product->manufacturer_name = Manufacturer::getNameById($product->id_manufacturer);

        $categories = array();
        foreach ($selected_cat as $key => $category) {
            $categories[] = $key;
        }

        $tree = new HelperTreeCategories('associated-categories-tree', 'Associated categories');
        $tree->setTemplate('tree_associated_categories.tpl')
            ->setHeaderTemplate('tree_associated_header.tpl')
            ->setRootCategory((int)$root->id)
            ->setUseCheckBox(true)
            ->setUseSearch(true)
            ->setSelectedCategories($categories);

        $data->assign(array('default_category' => $default_category,
                    'selected_cat_ids' => implode(',', array_keys($selected_cat)),
                    'selected_cat' => $selected_cat,
                    'id_category_default' => $product->getDefaultCategory(),
                    'category_tree' => $tree->render(),
                    'product' => $product,
                    'link' => $this->context->link,
                    'is_shop_context' => Shop::getContext() == Shop::CONTEXT_SHOP
        ));

        /* OVERRIDE */
        $this->displayWarning('Manca form di ricerca accessori/alternative');
        /* FINE OVERRIDE */

        $this->tpl_form_vars['custom_form'] = $data->fetch();
    }

    /**
     * @param Product $obj
     * @throws Exception
     * @throws SmartyException
     */
    public function initFormPrices($obj)
    {
        $data = $this->createTemplate($this->tpl_form);
        $product = $obj;
        if ($obj->id) {
            $shops = Shop::getShops();
            $countries = Country::getCountries($this->context->language->id);
            $groups = Group::getGroups($this->context->language->id);
            $currencies = Currency::getCurrencies();
            $attributes = $obj->getAttributesGroups((int)$this->context->language->id);
            $combinations = array();
            foreach ($attributes as $attribute) {
                $combinations[$attribute['id_product_attribute']]['id_product_attribute'] = $attribute['id_product_attribute'];
                if (!isset($combinations[$attribute['id_product_attribute']]['attributes'])) {
                    $combinations[$attribute['id_product_attribute']]['attributes'] = '';
                }
                $combinations[$attribute['id_product_attribute']]['attributes'] .= $attribute['attribute_name'].' - ';

                $combinations[$attribute['id_product_attribute']]['price'] = Tools::displayPrice(
                    Tools::convertPrice(
                        Product::getPriceStatic((int)$obj->id, false, $attribute['id_product_attribute']),
                        $this->context->currency
                    ), $this->context->currency
                );
            }
            foreach ($combinations as &$combination) {
                $combination['attributes'] = rtrim($combination['attributes'], ' - ');
            }
            $data->assign('specificPriceModificationForm', $this->_displaySpecificPriceModificationForm(
                $this->context->currency, $shops, $currencies, $countries, $groups)
            );

            $data->assign('ecotax_tax_excl', (float)$obj->ecotax);
            $this->_applyTaxToEcotax($obj);

            $data->assign(array(
                'shops' => $shops,
                'admin_one_shop' => count($this->context->employee->getAssociatedShops()) == 1,
                'currencies' => $currencies,
                'countries' => $countries,
                'groups' => $groups,
                'combinations' => $combinations,
                'multi_shop' => Shop::isFeatureActive(),
                'link' => new Link(),
                'pack' => new Pack()
            ));
        } else {
            $this->displayWarning($this->l('You must save this product before adding specific pricing'));
            $product->id_tax_rules_group = (int)Product::getIdTaxRulesGroupMostUsed();
            $data->assign('ecotax_tax_excl', 0);
        }

        $address = new Address();
        $address->id_country = (int)$this->context->country->id;
        $tax_rules_groups = TaxRulesGroup::getTaxRulesGroups(true);
        $tax_rates = array(
            0 => array(
                'id_tax_rules_group' => 0,
                'rates' => array(0),
                'computation_method' => 0
            )
        );

        foreach ($tax_rules_groups as $tax_rules_group) {
            $id_tax_rules_group = (int)$tax_rules_group['id_tax_rules_group'];
            $tax_calculator = TaxManagerFactory::getManager($address, $id_tax_rules_group)->getTaxCalculator();
            $tax_rates[$id_tax_rules_group] = array(
                'id_tax_rules_group' => $id_tax_rules_group,
                'rates' => array(),
                'computation_method' => (int)$tax_calculator->computation_method
            );

            if (isset($tax_calculator->taxes) && count($tax_calculator->taxes)) {
                foreach ($tax_calculator->taxes as $tax) {
                    $tax_rates[$id_tax_rules_group]['rates'][] = (float)$tax->rate;
                }
            } else {
                $tax_rates[$id_tax_rules_group]['rates'][] = 0;
            }
        }

        // prices part
        $data->assign(array(
            'link' => $this->context->link,
            'currency' => $currency = $this->context->currency,
            'tax_rules_groups' => $tax_rules_groups,
            'taxesRatesByGroup' => $tax_rates,
            'ecotaxTaxRate' => Tax::getProductEcotaxRate(),
            'tax_exclude_taxe_option' => Tax::excludeTaxeOption(),
            'ps_use_ecotax' => Configuration::get('PS_USE_ECOTAX'),
        ));

        $product->price = Tools::convertPrice($product->price, $this->context->currency, true, $this->context);
        if ($product->unit_price_ratio != 0) {
            $data->assign('unit_price', Tools::ps_round($product->price / $product->unit_price_ratio, 6));
        } else {
            $data->assign('unit_price', 0);
        }
        $data->assign('ps_tax', Configuration::get('PS_TAX'));

        $data->assign('country_display_tax_label', $this->context->country->display_tax_label);
        $data->assign(array(
            'currency', $this->context->currency,
            'product' => $product,
            'token' => $this->token
        ));

        /* OVERRIDE */

        $context = $this->context;

        if($product->id) {

            // PANEL ACQUISTO o ESOLVER

            $prezzi_esolver = Db::getInstance()->getRow('
                SELECT * 
                FROM product_esolver
                WHERE id_product = '.$product->id
            );
                        
            if(!$prezzi_esolver) {
                $prezzi_esolver['rebate_1'] = 0;
                $prezzi_esolver['rebate_2'] = 0;
                $prezzi_esolver['rebate_3'] = 0;
                $prezzi_esolver['wholesale_price'] = $product->wholesale_price;
            }
                
            $prezzi_esolver['margin_login_for_offer'] = Db::getInstance()->getValue('
                SELECT margin_login_for_offer 
                FROM '._DB_PREFIX_.'product 
                WHERE id_product = '.$product->id
            );

            $esolver_listino = number_format($product->listino, 2, '.', '');
            $esolver_sconto_acq_1 = number_format($product->sconto_acquisto_1, 2, '.', '');
            $esolver_sconto_acq_2 = number_format($product->sconto_acquisto_2, 2, '.', '');
            $esolver_sconto_acq_3 = number_format($product->sconto_acquisto_3, 2, '.', '');
            $esolver_acq_netto = (Tools::getIsset('wholesale_price_esolver') ? Tools::getValue('wholesale_price_esolver') : number_format($prezzi_esolver['wholesale_price'], 2, '.', ''));
            
            $check_employee = (($context->employee->id == 2 || $context->employee->id == 6 || $context->employee->id == 22) ? true : false); // Ezio, Federico, Carolina
            
            if($check_employee)
                $esolver_dollari = number_format($product->costo_dollari, 2, '.', '');

            // Pulsanti modifica e salva esolver: nascosti; vedi 1.4

            $data->assign(array(
                'check_employee' => $check_employee,
                'esolver_listino' => $esolver_listino ,
                'esolver_sconto_acq_1' => $esolver_sconto_acq_1,
                'esolver_sconto_acq_2' => $esolver_sconto_acq_2,
                'esolver_sconto_acq_3' => $esolver_sconto_acq_3,
                'esolver_acq_netto' => $esolver_acq_netto,
                'esolver_dollari' => $esolver_dollari
            ));
            
            // FINE ESOLVER


            // PREZZI SPECIALI
            
            // Vendita

            $sp_vendita = Db::getInstance()->getRow("
                SELECT * 
                FROM "._DB_PREFIX_."specific_price sp 
                WHERE sp.id_product = ".$product->id."
                    AND sp.to != '0000-00-00 00:00:00'
            ");

            $has_sp_v = ($sp_vendita['id_specific_price'] ? true : false);

            if($has_sp_v) {
                $sp_pieces_v = Db::getInstance()->getValue('
                    SELECT pieces 
                    FROM '._DB_PREFIX_.'specific_price_pieces 
                    WHERE id_specific_price = '.$sp_vendita['id_specific_price']
                );
                
                if($sp_pieces_v != '' && $sp_pieces_v <= 0)
                    $occhio_vendita = 1;
                else
                    $occhio_vendita = 0;

                $miglior_prezzo_a = Product::trovaMigliorPrezzoAcquisto($product->id);
                    
                $prezzo_sp_v = (($sp_vendita['reduction_type'] == 'amount') ? ($sp_vendita['price'] - $sp_vendita['reduction']) : ($sp_vendita['price'] - ($sp_vendita['price'] * $sp_vendita['reduction'])));
                
                // Va fatto prima o dopo il calcolo del margine?
                $prezzo_sp_v = str_replace(".", ",", $prezzo_sp_v);

                // Usa $prezzo_sp_v o solo $sp_vendita['price']???
                $margine_sp_v = ($prezzo_sp_v - $miglior_prezzo_a) / $prezzo_sp_v * 100;
                $margine_sp_v = number_format($margine_sp_v, 2, ",", "");
                $sp_pieces_v = ($sp_pieces_v != '' ? $sp_pieces_v : 'Illimitati');
                $data_sp_v = date("d/m/Y", strtotime($sp_vendita['to']));
            }

            // Acquisto
            
            $sp_acq = Db::getInstance()->getRow("
                SELECT * 
                FROM "._DB_PREFIX_."specific_price_wholesale spw 
                WHERE id_product = ".$_GET['id_product']." 
                    AND spw.to != '0000-00-00 00:00:00' 
            ");

            $has_sp_a = ($sp_acq['id_specific_price'] ? true : false);
            
            if($has_sp_a) {
                if($sp_acq['pieces'] != '' && $sp_acq['pieces'] <= 0)
                    $occhio_acquisto = 1;
                else
                    $occhio_acquisto = 0;

                $miglior_prezzo_v = Product::trovaMigliorPrezzo($product->id, 1, 1);
                    
                // Va fatto prima o dopo il calcolo del margine?
                $prezzo_sp_a = str_replace(".", ",", ($sp_acq['wholesale_price']));
                    
                $margine_sp_a = ($miglior_prezzo_v - $sp_acq['wholesale_price']) / $miglior_prezzo_v * 100;
                $margine_sp_a = number_format($margine_sp_v, 2, ",", "");
                $sp_pieces_a = ($sp_acq['pieces'] != '' ? $sp_acq['pieces'] : 'Illimitati');
                $data_sp_a = date("d/m/Y", strtotime($sp_acq['to']));
            }

            $has_sp_alert = ($has_sp_v && ($prezzo_sp_v > $product->price) ? true : false);

            $data->assign(array(
                // Vendita
                'has_sp_v' => $has_sp_v,
                'prezzo_sp_v' => $prezzo_sp_v,
                'margine_sp_v' => $margine_sp_v,
                'sp_pieces_v' => $sp_pieces_v,
                'data_sp_v' => $data_sp_v,
                'occhio_vendita' => $occhio_vendita,
                // Acquisto
                'has_sp_a' => $has_sp_a,
                'prezzo_sp_a' => $prezzo_sp_a,
                'margine_sp_a' => $margine_sp_a,
                'sp_pieces_a' => $sp_pieces_a,
                'data_sp_a' => $data_sp_a,
                'occhio_acquisto' => $occhio_acquisto,
                // Alert
                'has_sp_alert' => $has_sp_alert
            ));

            // FINE PREZZI SPECIALI

            // Se c'è un prezzo di acquisto speciale, questo sovrascrive il prezzo standard nel calcolo dei margini
            if($has_sp_a)
                $prezzo_acq_margini = $sp_acq['wholesale_price'];
            else
                $prezzo_acq_margini = $product->wholesale_price;

            // SALES

            $price_sales = Product::getPriceStatic($product->id, false, NULL, 2);
            // $wholesale_price_sales = Product::getCurrentWholesalePrice($product->id);
            $wholesale_price_sales = $prezzo_acq_margini;
            $margine_sales = ($price_sales - $wholesale_price_sales) / $price_sales * 100;

            $has_sales_alert = (($product->id && $price_sales && $margine_sales < 10) ? true : false);

            // Riga 1

            $sales_listino = number_format(htmlentities($product->listino, ENT_COMPAT, 'UTF-8'), 2, '.', '');
            $sales_sconto = number_format(htmlentities($product->scontolistinovendita, ENT_COMPAT, 'UTF-8'), 2, '.', '');
            $sales_prezzo = number_format(htmlentities($product->price, ENT_COMPAT, 'UTF-8'), 2, '.', ''); // price è già = listino - sconto
            $sales_marginalita = number_format((($product->price - $product->wholesale_price) / $product->price * 100), 2, '.', '');

            // Riga 2 - Spostata nel panel ACQUISTO o ESOLVER

            $sales_rebate_1 = (Tools::getIsset('rebate_1') ? Tools::getValue('rebate_1') : number_format($prezzi_esolver['rebate_1'], 2, '.', ''));
            $sales_rebate_2 = (Tools::getIsset('rebate_2') ? Tools::getValue('rebate_2') : number_format($prezzi_esolver['rebate_2'], 2, '.', ''));
            $sales_rebate_3 = (Tools::getIsset('rebate_3') ? Tools::getValue('rebate_3') : number_format($prezzi_esolver['rebate_3'], 2, '.', ''));
            $sales_acquisto = number_format(htmlentities($product->wholesale_price, ENT_COMPAT, 'UTF-8'), 2, '.', '');

            // Riga 3

            if($prezzi_esolver['margin_login_for_offer'] != 0){
                $prezzo_login_for_offer = ($product->wholesale_price * 100) / (100 - $prezzi_esolver['margin_login_for_offer']);
                $prezzo_login_for_offer = number_format($prezzo_login_for_offer, 2, '.', '');
            }
            else
                $prezzo_login_for_offer = 'ND';

            $margine_login_for_offer = (Tools::getIsset('margin_login_for_offer') ? Tools::getValue('margin_login_for_offer') : number_format($prezzi_esolver['margin_login_for_offer'], 2, '.', ''));
            
            $data->assign(array(
                // Alert
                'has_sales_alert' => $has_sales_alert,
                // Riga 1
                'sales_listino' => $sales_listino,
                'sales_sconto' => $sales_sconto,
                'sales_prezzo' => $sales_prezzo,
                'sales_marginalita' => $sales_marginalita,
                // Riga 2
                'sales_rebate_1' => $sales_rebate_1,
                'sales_rebate_2' => $sales_rebate_2,
                'sales_rebate_3' => $sales_rebate_3,
                'sales_acquisto' => $sales_acquisto,
                // Riga 3
                'prezzo_login_for_offer' => $prezzo_login_for_offer,
                'margine_login_for_offer' => $margine_login_for_offer
            ));

            // FINE SALES


            // SCONTI QUANTITA'

            // Variabili per fare i calcoli
            $qta_1 = $this->getQtSconti('sc_qta_1');
            $qta_2 = $this->getQtSconti('sc_qta_2');
            $qta_3 = $this->getQtSconti('sc_qta_3');

            $sc_qta_1 = $this->getSconti('sc_qta_1');
            $sc_qta_2 = $this->getSconti('sc_qta_2');
            $sc_qta_3 = $this->getSconti('sc_qta_3');

            $sc_qta_1p = $sc_qta_1 * 100;
            $sc_qta_2p = $sc_qta_2 * 100;
            $sc_qta_3p = $sc_qta_3 * 100;

            $prezzo_qta_1 = $product->price - ($product->price * $sc_qta_1);
            $prezzo_qta_2 = $product->price - ($product->price * $sc_qta_2);
            $prezzo_qta_3 = $product->price - ($product->price * $sc_qta_3);

            $margine_qta_1 = ($prezzo_qta_1 - $prezzo_acq_margini) / $prezzo_qta_1 * 100;
            $margine_qta_2 = ($prezzo_qta_2 - $prezzo_acq_margini) / $prezzo_qta_2 * 100;
            $margine_qta_3 = ($prezzo_qta_3 - $prezzo_acq_margini) / $prezzo_qta_3 * 100;

            // Variabili da visualizzare
            $qta_1 = (Tools::getIsset('quantita_1') ? Tools::getValue('quantita_1') : htmlentities($qta_1));
            $qta_2 = (Tools::getIsset('quantita_2') ? Tools::getValue('quantita_2') : htmlentities($qta_2));
            $qta_3 = (Tools::getIsset('quantita_3') ? Tools::getValue('quantita_3') : htmlentities($qta_3));

            $sc_qta_1p = (Tools::getIsset('sconto_quantita_1') ? Tools::getValue('sconto_quantita_1') : htmlentities($sc_qta_1p));
            $sc_qta_2p = (Tools::getIsset('sconto_quantita_2') ? Tools::getValue('sconto_quantita_2') : htmlentities($sc_qta_2p));
            $sc_qta_3p = (Tools::getIsset('sconto_quantita_3') ? Tools::getValue('sconto_quantita_3') : htmlentities($sc_qta_3p));

            $prezzo_qta_1 = (Tools::getIsset('prezzo_qta_1') ? Tools::getValue('prezzo_qta_1') : number_format($prezzo_qta_1, 2, '.', ''));
            $prezzo_qta_2 = (Tools::getIsset('prezzo_qta_2') ? Tools::getValue('prezzo_qta_2') : number_format($prezzo_qta_2, 2, '.', ''));
            $prezzo_qta_3 = (Tools::getIsset('prezzo_qta_3') ? Tools::getValue('prezzo_qta_3') : number_format($prezzo_qta_3, 2, '.', ''));

            $margine_qta_1 = (Tools::getIsset('margin_qta_1') ? Tools::getValue('margin_qta_1') : number_format($margine_qta_1, 2, '.', ''));
            $margine_qta_2 = (Tools::getIsset('margin_qta_2') ? Tools::getValue('margin_qta_2') : number_format($margine_qta_2, 2, '.', ''));
            $margine_qta_3 = (Tools::getIsset('margin_qta_3') ? Tools::getValue('margin_qta_3') : number_format($margine_qta_3, 2, '.', ''));

            $data->assign(array(
                'qta_1' => $qta_1,
                'qta_2' => $qta_2,
                'qta_3' => $qta_3,
                'sc_qta_1p' => $sc_qta_1p,
                'sc_qta_2p' => $sc_qta_2p,
                'sc_qta_3p' => $sc_qta_3p,
                'prezzo_qta_1' => $prezzo_qta_1,
                'prezzo_qta_2' => $prezzo_qta_2,
                'prezzo_qta_3' => $prezzo_qta_3,
                'margine_qta_1' => $margine_qta_1,
                'margine_qta_2' => $margine_qta_2,
                'margine_qta_3' => $margine_qta_3
            ));

            // FINE SCONTI QUANTITA'


            // SCONTI RIVENDITORI

            // Variabili per fare i calcoli
            $sc_riv_1 = $this->getSconti('sc_riv_1');
            $sc_riv_2 = $this->getSconti('sc_riv_2');
            $sc_riv_3 = $this->getSconti('sc_riv_3');

            $sc_riv_1p = $sc_riv_1 * 100;
            $sc_riv_2p = $sc_riv_2 * 100;
            $sc_riv_3p = $sc_riv_3 * 100;

            $prezzo_riv_1 = $product->listino - ($product->listino * $sc_riv_1);
            $prezzo_riv_2 = $product->listino - ($product->listino * $sc_riv_2);
            $prezzo_riv_3 = $product->listino - ($product->listino * $sc_riv_3);

            $margine_riv_1 = ($prezzo_riv_1 - $prezzo_acq_margini) / $prezzo_riv_1 * 100;
            $margine_riv_2 = ($prezzo_riv_2 - $prezzo_acq_margini) / $prezzo_riv_2 * 100;
            $margine_riv_3 = ($prezzo_riv_3 - $prezzo_acq_margini) / $prezzo_riv_3 * 100;

            // Variabili da visualizzare
            $sc_riv_1p = (Tools::getIsset('sconto_rivenditore_1') ? Tools::getValue('sconto_rivenditore_1') : htmlentities($sc_riv_1p));
            $sc_riv_2p = (Tools::getIsset('sconto_rivenditore_2') ? Tools::getValue('sconto_rivenditore_2') : htmlentities($sc_riv_2p));
            $sc_riv_3p = (Tools::getIsset('sconto_rivenditore_3') ? Tools::getValue('sconto_rivenditore_3') : htmlentities($sc_riv_3p));

            $prezzo_riv_1 = (Tools::getIsset('prezzo_riv_1') ? Tools::getValue('prezzo_riv_1') : number_format($prezzo_riv_1, 2, '.', ''));
            $prezzo_riv_2 = (Tools::getIsset('prezzo_riv_2') ? Tools::getValue('prezzo_riv_2') : number_format($prezzo_riv_2, 2, '.', ''));
            $prezzo_riv_3 = (Tools::getIsset('prezzo_riv_3') ? Tools::getValue('prezzo_riv_3') : number_format($prezzo_riv_3, 2, '.', ''));

            $margine_riv_1 = (Tools::getIsset('margin_riv_1') ? Tools::getValue('margin_riv_1') : number_format($margine_riv_1, 2, '.', ''));
            $margine_riv_2 = (Tools::getIsset('margin_riv_2') ? Tools::getValue('margin_riv_2') : number_format($margine_riv_2, 2, '.', ''));
            $margine_riv_3 = (Tools::getIsset('margin_riv_3') ? Tools::getValue('margin_riv_3') : number_format($margine_riv_3, 2, '.', ''));
            
            $data->assign(array(
                'sc_riv_1p' => $sc_riv_1p,
                'sc_riv_2p' => $sc_riv_2p,
                'sc_riv_3p' => $sc_riv_3p,
                'prezzo_riv_1' => $prezzo_riv_1,
                'prezzo_riv_2' => $prezzo_riv_2,
                'prezzo_riv_3' => $prezzo_riv_3,
                'margine_riv_1' => $margine_riv_1,
                'margine_riv_2' => $margine_riv_2,
                'margine_riv_3' => $margine_riv_3
            ));

            // FINE SCONTI RIVENDITORI


            // MARGINI MINIMI

            $map_price = Db::getInstance()->getValue('SELECT map FROM '._DB_PREFIX_.'product WHERE id_product = '.$product->id);

            $margin_min_cli = number_format(htmlentities($product->margin_min_cli, ENT_COMPAT, 'UTF-8'), 2, '.', '');
            $margin_min_riv = number_format(htmlentities($product->margin_min_riv, ENT_COMPAT, 'UTF-8'), 2, '.', '');

            if($map_price > 0)
                $map_price = number_format($map_price, 2, '.', '');

            $data->assign(array(
                'margin_min_cli' => $margin_min_cli,
                'margin_min_riv' => $margin_min_riv,
                'map_price' => $map_price
            ));

            // FINE MARGINI MINIMI

            $miglior_prezzo = Product::trovaMigliorPrezzo($product->id, 1, 1);
            $miglior_prezzo_acquisto = Product::trovaMigliorPrezzoAcquisto($product->id);
            $miglior_margine = $miglior_prezzo - $miglior_prezzo_acquisto;

            // PREMI AGENTI
            
            $marginalita_migliore_percentuale = (($miglior_prezzo - $miglior_prezzo_acquisto) / $miglior_prezzo * 100);
                        
            if($marginalita_migliore_percentuale > 20)
                $margine_provvigione = $miglior_margine * 30 / 100;
            else
                $margine_provvigione = $miglior_margine * 25 / 100;

            $provvigione = $margine_provvigione * 100 / $miglior_prezzo;
            $provvigione = number_format($provvigione, 2, '.', ''); // %

            $provvigione_personalizzata = Db::getInstance()->getValue('SELECT provvigione FROM '._DB_PREFIX_.'product WHERE id_product = '.$product->id);
            $provvigione_personalizzata = ($provvigione_personalizzata == 0 ? '' : number_format($provvigione_personalizzata, 2, '.', '')); // %

            $data->assign(array(
                'provvigione' => $provvigione,
                'provvigione_personalizzata' => $provvigione_personalizzata
            ));

            // FINE PREMI AGENTI
            

            // SCONTISTICA AMAZON
            
            // Sconto extra (uno per marketplace) che agisce sul miglior prezzo di vendita attuale per i clienti finali (NON rivenditori)
            
            // Correggere: migliorare usando $product->amazon_fr etc...
            $scontistica_amazon = Db::getInstance()->getRow('
                SELECT amazon_fr AS fr, amazon_uk AS uk, amazon_es AS es, amazon_de AS de, amazon_nl AS nl, amazon_it AS it, amazon_se as se, amazon_pl AS pl
                FROM '._DB_PREFIX_.'product 
                WHERE id_product = '.$product->id
            );
            
            $has_amazon = ($scontistica_amazon['de'] != 0 || $scontistica_amazon['es'] != 0 || $scontistica_amazon['uk'] != 0 || $scontistica_amazon['it'] != 0 || $scontistica_amazon['nl'] != 0 || $scontistica_amazon['pl'] != 0 ? true : false);

            $fr_sconto = (Tools::getIsset('sconto_amazon_fr') ? Tools::getValue('sconto_amazon_fr') : $scontistica_amazon['fr']);
            $de_sconto = (Tools::getIsset('sconto_amazon_de') ? Tools::getValue('sconto_amazon_de') : $scontistica_amazon['de']);
            $nl_sconto = (Tools::getIsset('sconto_amazon_nl') ? Tools::getValue('sconto_amazon_nl') : $scontistica_amazon['nl']);
            $se_sconto = (Tools::getIsset('sconto_amazon_se') ? Tools::getValue('sconto_amazon_se') : $scontistica_amazon['se']);
            $es_sconto = (Tools::getIsset('sconto_amazon_es') ? Tools::getValue('sconto_amazon_es') : $scontistica_amazon['es']);
            $it_sconto = (Tools::getIsset('sconto_amazon_it') ? Tools::getValue('sconto_amazon_it') : $scontistica_amazon['it']);
            $uk_sconto = (Tools::getIsset('sconto_amazon_uk') ? Tools::getValue('sconto_amazon_uk') : $scontistica_amazon['uk']);
            $pl_sconto = (Tools::getIsset('sconto_amazon_pl') ? Tools::getValue('sconto_amazon_pl') : $scontistica_amazon['pl']);

            $fr_prezzo = (Tools::getIsset('prezzo_amazon_fr') ? Tools::getValue('prezzo_amazon_fr') : number_format($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['fr'] / 100)), 2, '.', ''));
            $de_prezzo = (Tools::getIsset('prezzo_amazon_de') ? Tools::getValue('prezzo_amazon_de') : number_format($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['de'] / 100)), 2, '.', ''));
            $nl_prezzo = (Tools::getIsset('prezzo_amazon_nl') ? Tools::getValue('prezzo_amazon_nl') : number_format($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['nl'] / 100)), 2, '.', ''));
            $se_prezzo = (Tools::getIsset('prezzo_amazon_se') ? Tools::getValue('prezzo_amazon_se') : number_format($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['se'] / 100)), 2, '.', ''));
            $es_prezzo = (Tools::getIsset('prezzo_amazon_es') ? Tools::getValue('prezzo_amazon_es') : number_format($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['es'] / 100)), 2, '.', ''));
            $it_prezzo = (Tools::getIsset('prezzo_amazon_it') ? Tools::getValue('prezzo_amazon_it') : number_format($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['it'] / 100)), 2, '.', ''));
            $uk_prezzo = (Tools::getIsset('prezzo_amazon_uk') ? Tools::getValue('prezzo_amazon_uk') : number_format($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['uk'] / 100)), 2, '.', ''));
            $pl_prezzo = (Tools::getIsset('prezzo_amazon_pl') ? Tools::getValue('prezzo_amazon_pl') : number_format($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['pl'] / 100)), 2, '.', ''));

            $fr_margine = (Tools::getIsset('margin_amazon_fr') ? Tools::getValue('margin_amazon_fr') : number_format(((($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['fr'] / 100))) - $prezzo_acq_margini) * 100) / ($miglior_prezzo - ($miglior_prezzo * $scontistica_amazon['fr'] / 100)), 2, '.', ''));
            $de_margine = (Tools::getIsset('margin_amazon_de') ? Tools::getValue('margin_amazon_de') : number_format(((($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['de'] / 100))) - $prezzo_acq_margini) * 100) / ($miglior_prezzo - ($miglior_prezzo * $scontistica_amazon['de'] / 100)), 2, '.', ''));
            $nl_margine = (Tools::getIsset('margin_amazon_nl') ? Tools::getValue('margin_amazon_nl') : number_format(((($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['nl'] / 100))) - $prezzo_acq_margini) * 100) / ($miglior_prezzo - ($miglior_prezzo * $scontistica_amazon['nl'] / 100)), 2, '.', ''));
            $se_margine = (Tools::getIsset('margin_amazon_se') ? Tools::getValue('margin_amazon_se') : number_format(((($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['se'] / 100))) - $prezzo_acq_margini) * 100) / ($miglior_prezzo - ($miglior_prezzo * $scontistica_amazon['se'] / 100)), 2, '.', ''));
            $es_margine = (Tools::getIsset('margin_amazon_es') ? Tools::getValue('margin_amazon_es') : number_format(((($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['es'] / 100))) - $prezzo_acq_margini) * 100) / ($miglior_prezzo - ($miglior_prezzo * $scontistica_amazon['es'] / 100)), 2, '.', ''));
            $it_margine = (Tools::getIsset('margin_amazon_it') ? Tools::getValue('margin_amazon_it') : number_format(((($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['it'] / 100))) - $prezzo_acq_margini) * 100) / ($miglior_prezzo - ($miglior_prezzo * $scontistica_amazon['it'] / 100)), 2, '.', ''));
            $uk_margine = (Tools::getIsset('margin_amazon_uk') ? Tools::getValue('margin_amazon_uk') : number_format(((($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['uk'] / 100))) - $prezzo_acq_margini) * 100) / ($miglior_prezzo - ($miglior_prezzo * $scontistica_amazon['uk'] / 100)), 2, '.', ''));
            $pl_margine = (Tools::getIsset('margin_amazon_pl') ? Tools::getValue('margin_amazon_pl') : number_format(((($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['pl'] / 100))) - $prezzo_acq_margini) * 100) / ($miglior_prezzo - ($miglior_prezzo * $scontistica_amazon['pl'] / 100)), 2, '.', ''));

            $fr_alert = ((($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['fr'] / 100))) - $prezzo_acq_margini) * 100) / ($miglior_prezzo - ($miglior_prezzo * $scontistica_amazon['fr'] / 100));
            $de_alert = ((($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['de'] / 100))) - $prezzo_acq_margini) * 100) / ($miglior_prezzo - ($miglior_prezzo * $scontistica_amazon['de'] / 100));
            $nl_alert = ((($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['nl'] / 100))) - $prezzo_acq_margini) * 100) / ($miglior_prezzo - ($miglior_prezzo * $scontistica_amazon['nl'] / 100));
            $se_alert = ((($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['se'] / 100))) - $prezzo_acq_margini) * 100) / ($miglior_prezzo - ($miglior_prezzo * $scontistica_amazon['se'] / 100));
            $es_alert = ((($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['es'] / 100))) - $prezzo_acq_margini) * 100) / ($miglior_prezzo - ($miglior_prezzo * $scontistica_amazon['es'] / 100));
            $it_alert = ((($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['it'] / 100))) - $prezzo_acq_margini) * 100) / ($miglior_prezzo - ($miglior_prezzo * $scontistica_amazon['it'] / 100));
            $uk_alert = ((($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['uk'] / 100))) - $prezzo_acq_margini) * 100) / ($miglior_prezzo - ($miglior_prezzo * $scontistica_amazon['uk'] / 100));
            $pl_alert = ((($miglior_prezzo - ($miglior_prezzo * ($scontistica_amazon['pl'] / 100))) - $prezzo_acq_margini) * 100) / ($miglior_prezzo - ($miglior_prezzo * $scontistica_amazon['pl'] / 100));

            $fr_has_alert = ($fr_alert < 12.50 ? true : false);
            $de_has_alert = ($de_alert < 12.50 ? true : false);
            $nl_has_alert = ($nl_alert < 12.50 ? true : false);
            $se_has_alert = ($se_alert < 12.50 ? true : false);
            $es_has_alert = ($es_alert < 12.50 ? true : false);
            $it_has_alert = ($it_alert < 12.50 ? true : false);
            $uk_has_alert = ($uk_alert < 12.50 ? true : false);
            $pl_has_alert = ($pl_alert < 12.50 ? true : false);

            $data->assign(array(
                'has_amazon' => $has_amazon,
                // Sconti
                'fr_sconto' => $fr_sconto,
                'de_sconto' => $de_sconto,
                'nl_sconto' => $nl_sconto,
                'se_sconto' => $se_sconto,
                'es_sconto' => $es_sconto,
                'it_sconto' => $it_sconto,
                'uk_sconto' => $uk_sconto,
                'pl_sconto' => $pl_sconto,
                // Prezzi
                'fr_prezzo' => $fr_prezzo,
                'de_prezzo' => $de_prezzo,
                'nl_prezzo' => $nl_prezzo,
                'se_prezzo' => $se_prezzo,
                'es_prezzo' => $es_prezzo,
                'it_prezzo' => $it_prezzo,
                'uk_prezzo' => $uk_prezzo,
                'pl_prezzo' => $pl_prezzo,
                // Margini
                'fr_margine' => $fr_margine,
                'de_margine' => $de_margine,
                'nl_margine' => $nl_margine,
                'se_margine' => $se_margine,
                'es_margine' => $es_margine,
                'it_margine' => $it_margine,
                'uk_margine' => $uk_margine,
                'pl_margine' => $pl_margine,
                // Alert
                'fr_has_alert' => $fr_has_alert,
                'de_has_alert' => $de_has_alert,
                'nl_has_alert' => $nl_has_alert,
                'se_has_alert' => $se_has_alert,
                'es_has_alert' => $es_has_alert,
                'it_has_alert' => $it_has_alert,
                'uk_has_alert' => $uk_has_alert,
                'pl_has_alert' => $pl_has_alert
            ));
        
            // FINE SCONTISTICA AMAZON

            // PREZZI MIGLIORI

            $miglior_prezzo = Tools::displayPrice($miglior_prezzo, $context->currency);
            $miglior_prezzo_acquisto = Tools::displayPrice($miglior_prezzo_acquisto, $context->currency);
            $marginalita_migliore_percentuale = number_format($marginalita_migliore_percentuale, 2, ",", "");

            $data->assign(array(
                'miglior_prezzo' => $miglior_prezzo,
                'miglior_prezzo_acquisto' => $miglior_prezzo_acquisto,
                'marginalita_migliore_percentuale' => $marginalita_migliore_percentuale
            ));

            // FINE PREZZI MIGLIORI


            // Correggere! Fare una copia di questo per prezzi acquisto speciali! con modifiche (vedi 1.4)
            // PANEL SPECIFIC PRICES (OFFERTE SPECIALI)
            /*
            $fornitore_standard = Db::getInstance()->getValue('SELECT name FROM '._DB_PREFIX_.'supplier WHERE id_supplier = '.$product->id_supplier);
            // tpl: '</td><td>Fornitore</td><td colspan="3">'.($type == 'vnd' ? '<input type="text" size="11" readonly="readonly" value="'.$fornitore_standard.'" />' : '<select name="sp_supplier_w">'.$supplier_select.'</select>').'</td></tr>';
            
            // solo vnd:
            // tpl "Qta min.": <input type="text" name="sp_from_quantity" value="{$qta_min}" size="3" />
            $qta_min = ($id_specific_price > 0 ? $specificPrice['from_quantity'] : '1'); // correggi variabili: non esistono

            if($id_specific_price > 0)
                $pieces = Db::getInstance()->getValue('SELECT pieces FROM '._DB_PREFIX_.'specific_price_pieces WHERE id_specific_price = '.$id_specific_price);

            // tpl "Pz. in off.": <input type="text" name="sp_pieces" value="{$pz_in_off}" size="3" />
            $pz_in_off = ($id_specific_price > 0 ? $pieces : ''); // $type == 'acq' && $id_specific_price > 0 ? $specificPrice['pieces'] : ''

            // readonly: vnd. standard ($product->price), listino ($product->listino), acq. standard ($product->wholesale_price) -> se mi servono posso prenderli dagli input già caricati negli altri panel

            // Sconto</td><td><input type="text" id="sp_reduction_'.$idrand.'" name="sp_reduction" size="11" value="'.($id_specific_price > 0 ? number_format(((($product->price-$specificPrice['price'])*100)/$product->price),2,",","") : 0).'" onkeyup="calcPrezzoSconto(\'sp_reduction_'.$idrand.'\', \'sp_price_'.$idrand.'\', \'sp_vnd_standard_'.$idrand.'\'); calcolaMargine_'.$idrand.'()";") /></td>')
            // prezzo speciale: ...
            // prezzo acquisto: ...

            // abilita banda: <input name="abilita_banda" id="abilita_banda" type="checkbox" '.($abilita_banda == 1 ? 'checked="checked"' : '').' /> <img class="img_control" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/help.gif" title="Abilita una banda con scritto \'Offerta speciale\' in listing e scheda prodotto" />
            $abilita_banda = Db::getInstance()->getValue('SELECT abilita_banda FROM '._DB_PREFIX_.'specific_price WHERE id_specific_price = '.$id_specific_price);

            // frasi sui margini ecc: ...
            // table listino / vnd / sconti: ...
            // table disponibilità
            */
            // includere js:
            /*
            <script type="text/javascript">
            function calcolaMargine_'.$idrand.'() 
            {
                var numprice = document.getElementById(\'sp_price_'.$idrand.'\').value;
                var price = parseFloat(numprice.replace(/\s/g, "").replace(",", "."));
                
                if(numprice == "")
                    numprice = 0;
                
                var numacquisto = document.getElementById(\'sp_wholesale_price_'.$idrand.'\').value;
                
                if(numacquisto == "")
                    acquisto = 0;
                    
                var acquisto = parseFloat(numacquisto.replace(/\s/g, "").replace(",", "."));
                
                var marginalitatotale = (((price - acquisto)*100) / price);
            
                marginalitatotale = marginalitatotale.toFixed(2);
                
                var guadagno = price-acquisto;
                guadagno = guadagno.toFixed(2);
            
                document.getElementById(\'sp_marg_'.$idrand.'\').innerHTML=marginalitatotale.replace(".",",") + "%";
                document.getElementById(\'sp_guadagno_'.$idrand.'\').innerHTML=guadagno.replace(".",",") + " €";
                
                '.($type == 'acq' ? 
                
                '
                    var numprice_standard = document.getElementById(\'sp_vnd_standard_'.$idrand.'\').value;
                    var price_standard = parseFloat(numprice_standard.replace(/\s/g, "").replace(",", "."));

                    var marginalitatotale_standard = (((price_standard - acquisto)*100) / price_standard);
                    marginalitatotale_standard = marginalitatotale_standard.toFixed(2);
                    
                    var guadagno_standard = price_standard-acquisto;
                    guadagno_standard = guadagno_standard.toFixed(2);
                
                    document.getElementById(\'sp_marg_standard_'.$idrand.'\').innerHTML=marginalitatotale_standard.replace(".",",") + "%";
                    document.getElementById(\'sp_guadagno_standard_'.$idrand.'\').innerHTML=guadagno_standard.replace(".",",") + " €";
                
                
                '
                
                
                : '').'
            }
            
            function calcSconti_'.$idrand.'() 
            {
                var acq1 = parseFloat(document.getElementById("sp_reductionw1_'.$idrand.'").value.replace(/,/g, "."));
                if (acq1 == "" || isNaN(acq1))
                    acq1 = 0;
                    
                var acq2 = parseFloat(document.getElementById("sp_reductionw2_'.$idrand.'").value.replace(/,/g, "."));
                if (acq2 == "" || isNaN(acq2))
                    acq2 = 0;
                    
                var acq3 = parseFloat(document.getElementById("sp_reductionw3_'.$idrand.'").value.replace(/,/g, "."));
                
                if (acq3 == "" || isNaN(acq3))
                    acq3 = 0;
                    
                var listino = parseFloat(document.getElementById("sp_listino_'.$idrand.'").value.replace(/,/g, "."));
                
                if (listino == "" || isNaN(listino))
                    listino = 0;
                var newPrice = (listino * (100 - acq1) / 100) * ((100 - acq2)/100) * ((100 - acq3)/100);
                
                document.getElementById("sp_wholesale_price_'.$idrand.'").value = (isNaN(newPrice) == true || newPrice < 0) ? "" :
                ps_round(newPrice, 2);
            }
            </script>
            */

            $data->assign(array(
                'fornitore_standard' => $fornitore_standard,
                'qta_min' => $qta_min,
                'pz_in_off' => $pz_in_off,
                'abilita_banda' => $abilita_banda,
            ));
            
            // FINE SPECIFIC PRICES (OFFERTE SPECIALI)

        }

        $this->addJS(_PS_JS_DIR_.'admin/price.js');
        $this->addCSS(_PS_BO_DEFAULT_THEME_CSS_DIR_.'overrides.css');

        /* FINE OVERRIDE */

        $this->tpl_form_vars['custom_form'] = $data->fetch();
    }

    public function initFormSeo($product)
    {
        if (!$this->default_form_language) {
            $this->getLanguages();
        }

        $data = $this->createTemplate($this->tpl_form);

        $context = Context::getContext();
        $rewritten_links = array();
        foreach ($this->_languages as $language) {
            $category = Category::getLinkRewrite((int)$product->id_category_default, (int)$language['id_lang']);
            $rewritten_links[(int)$language['id_lang']] = explode(
                '[REWRITE]',
                $context->link->getProductLink($product, '[REWRITE]', $category, null, (int)$language['id_lang'])
            );
        }
        
        /* OVERRIDE */
        if(!isset($product->canonical))
            $product->canonical = 0;

        if(!isset($product->redirect_category))
            $product->redirect_category = 0;
        /* FINE OVERRIDE */

        $data->assign(array(
            'product' => $product,
            'languages' => $this->_languages,
            'id_lang' => $this->context->language->id,
            'ps_ssl_enabled' => Configuration::get('PS_SSL_ENABLED'),
            'curent_shop_url' => $this->context->shop->getBaseURL(),
            'default_form_language' => $this->default_form_language,
            'rewritten_links' => $rewritten_links
        ));

        /* OVERRIDE */

        $this->displayWarning('Bug: ajax SEO ita sparisce dopo il salvataggio? Limite caratteri meta description = ?');

        // Canonical-redir-EOL

		$res_corr = Db::getInstance()->executeS("
            SELECT p.id_product, p.reference, pl.name 
            FROM "._DB_PREFIX_."product p
                JOIN "._DB_PREFIX_."product_lang pl ON p.id_product = pl.id_product 
            WHERE p.reference != '' AND pl.id_lang = ".$context->language->id."
                AND p.active = 1
        ");
        
        $data->assign('redirect_canonical', $res_corr);
		
        // Categoria per redirect

        $res_cats = Db::getInstance()->executeS('
            SELECT c.id_category, cl.name
            FROM '._DB_PREFIX_.'category c 
                JOIN '._DB_PREFIX_.'category_lang cl ON c.id_category = cl.id_category 
            WHERE cl.id_lang = '.$context->language->id.'
                AND c.active = 1 
            ORDER BY cl.name ASC
        ');
        
        $data->assign('redirect_categories', $res_cats);
        
        /* // URL SE IL PRODOTTO E' FUORI PRODUZIONE: (lasciare commentato)
        {l s='URL se fuori produzione'}
        - uguale a friendly url con:
            input_value=$product->link_rewrite_fp
			input_name='link_rewrite_fp'
            size="55"
            pattern="^((?!(-p-)|(-m-)|(-c-)|(-pr-)|(-pi-)).)*$"
            onchange="updatefpURL();"
            Hint: 'Only letters and the "less" character are allowed' */

        // SEO (solo per italiano)

        if($product->id) {
            $parola_chiave = Db::getInstance()->getValue('
                SELECT seo_keyword 
                FROM '._DB_PREFIX_.'product_lang 
                WHERE id_lang = '.$context->language->id.' 
                    AND id_product = '.$product->id
            );
        }
        else
            $parola_chiave = '';

		$hidden_seo = Db::getInstance()->getValue('
            SELECT value 
            FROM fixed_ck 
            WHERE id_employee = '.$context->employee->id.' 
                AND name = "hidden_seo"
        ');

        $data->assign('parola_chiave', $parola_chiave);
        $data->assign('hidden_seo', $hidden_seo);

        /* MANCA NEL TPL, ma io lo lascerei visibile di default sotto a parola chiave; riportarlo negli altri controller che lo usano
        <script type='text/javascript'>
        $(document).ready(function(){
            $('#toggleseo').click(function() {
                $('#seoby').slideToggle('fast', function() {
                    if ($('#seoby').is(':hidden')) {
                        var hidden_seo = 1;
                    } else {
                        var hidden_seo = 0;
                    }
                    
                    $.ajax({
                        type: 'POST',
                        async: true,
                        url: 'ajax.php?nascondiMostraSeo',
                        data: {hidden_seo: hidden_seo, nascondiMostraSeo: 'y'},
                        success: function(data) {
                        },
                        error: function(xhr,stato,errori){
                        }
                    });
                });
            });
        });
        </script> */

        /* FINE OVERRIDE */

        $this->tpl_form_vars['custom_form'] = $data->fetch();
    }

    /**
     * Get an array of pack items for display from the product object if specified, else from POST/GET values
     *
     * @param Product $product
     * @return array of pack items
     */
    public function getPackItems($product = null)
    {
        $pack_items = array();

        if (!$product) {
            $names_input = Tools::getValue('namePackItems');
            $ids_input = Tools::getValue('inputPackItems');
            if (!$names_input || !$ids_input) {
                return array();
            }
            // ids is an array of string with format : QTYxID
            $ids = array_unique(explode('-', $ids_input));
            $names = array_unique(explode('¤', $names_input));

            if (!empty($ids)) {
                $length = count($ids);
                for ($i = 0; $i < $length; $i++) {
                    if (!empty($ids[$i]) && !empty($names[$i])) {
                        list($pack_items[$i]['pack_quantity'], $pack_items[$i]['id']) = explode('x', $ids[$i]);
                        $exploded_name = explode('x', $names[$i]);
                        $pack_items[$i]['name'] = $exploded_name[1];
                    }
                }
            }
        } else {
            $i = 0;
            foreach ($product->packItems as $pack_item) {
                $pack_items[$i]['id'] = $pack_item->id;
                $pack_items[$i]['pack_quantity'] = $pack_item->pack_quantity;
                $pack_items[$i]['name']    = $pack_item->name;
                $pack_items[$i]['reference'] = $pack_item->reference;
                $pack_items[$i]['id_product_attribute'] = isset($pack_item->id_pack_product_attribute) && $pack_item->id_pack_product_attribute ? $pack_item->id_pack_product_attribute : 0;
                $cover = $pack_item->id_pack_product_attribute ? Product::getCombinationImageById($pack_item->id_pack_product_attribute, Context::getContext()->language->id) : Product::getCover($pack_item->id);
                $pack_items[$i]['image'] = Context::getContext()->link->getImageLink($pack_item->link_rewrite, $cover['id_image'], 'home_default');
                // @todo: don't rely on 'home_default'
                //$path_to_image = _PS_IMG_DIR_.'p/'.Image::getImgFolderStatic($cover['id_image']).(int)$cover['id_image'].'.jpg';
                //$pack_items[$i]['image'] = ImageManager::thumbnail($path_to_image, 'pack_mini_'.$pack_item->id.'_'.$this->context->shop->id.'.jpg', 120);
                $i++;
            }
        }
        return $pack_items;
    }

    /**
     * @param Product $product
     * @throws Exception
     * @throws SmartyException
     */
    public function initFormPack($product)
    {
        $data = $this->createTemplate($this->tpl_form);

        // If pack items have been submitted, we want to display them instead of the actuel content of the pack
        // in database. In case of a submit error, the posted data is not lost and can be sent again.
        if (Tools::getValue('namePackItems')) {
            $input_pack_items = Tools::getValue('inputPackItems');
            $input_namepack_items = Tools::getValue('namePackItems');
            $pack_items = $this->getPackItems();
        } else {
            $product->packItems = Pack::getItems($product->id, $this->context->language->id);
            $pack_items = $this->getPackItems($product);
            $input_namepack_items = '';
            $input_pack_items = '';
            foreach ($pack_items as $pack_item) {
                $input_pack_items .= $pack_item['pack_quantity'].'x'.$pack_item['id'].'x'.$pack_item['id_product_attribute'].'-';
                $input_namepack_items .= $pack_item['pack_quantity'].' x '.$pack_item['name'].'¤';
            }
        }

        $data->assign(array(
            'input_pack_items' => $input_pack_items,
            'input_namepack_items' => $input_namepack_items,
            'pack_items' => $pack_items,
            'product_type' => (int)Tools::getValue('type_product', $product->getType())
        ));

        $this->tpl_form_vars['custom_form'] = $data->fetch();
    }

    public function initFormVirtualProduct($product)
    {
        $data = $this->createTemplate($this->tpl_form);

        $currency = $this->context->currency;

        /*
        * Form for adding a virtual product like software, mp3, etc...
        */
        $product_download = new ProductDownload();
        if ($id_product_download = $product_download->getIdFromIdProduct($this->getFieldValue($product, 'id'))) {
            $product_download = new ProductDownload($id_product_download);
        }
        $product->{'productDownload'} = $product_download;

        if ($product->productDownload->id && empty($product->productDownload->display_filename)) {
            $this->errors[] = Tools::displayError('A file name is required in order to associate a file');
            $this->tab_display = 'VirtualProduct';
        }

        // @todo handle is_virtual with the value of the product
        $exists_file = realpath(_PS_DOWNLOAD_DIR_).'/'.$product->productDownload->filename;
        $data->assign('product_downloaded', $product->productDownload->id);

        if (!file_exists($exists_file)
            && !empty($product->productDownload->display_filename)
            && empty($product->cache_default_attribute)) {
            $msg = sprintf(Tools::displayError('File "%s" is missing'),
                $product->productDownload->display_filename);
        } else {
            $msg = '';
        }

        $virtual_product_file_uploader = new HelperUploader('virtual_product_file_uploader');
        $virtual_product_file_uploader->setMultiple(false)->setUrl(
            Context::getContext()->link->getAdminLink('AdminProducts').'&ajax=1&id_product='.(int)$product->id
            .'&action=AddVirtualProductFile')->setPostMaxSize(Tools::getOctets(ini_get('upload_max_filesize')))
            ->setTemplate('virtual_product.tpl');

        $data->assign(array(
            'download_product_file_missing' => $msg,
            'download_dir_writable' => ProductDownload::checkWritableDir(),
            'up_filename' => strval(Tools::getValue('virtual_product_filename'))
        ));

        $product->productDownload->nb_downloadable = ($product->productDownload->id > 0) ? $product->productDownload->nb_downloadable : htmlentities(Tools::getValue('virtual_product_nb_downloable'), ENT_COMPAT, 'UTF-8');
        $product->productDownload->date_expiration = ($product->productDownload->id > 0) ? ((!empty($product->productDownload->date_expiration) && $product->productDownload->date_expiration != '0000-00-00 00:00:00') ? date('Y-m-d', strtotime($product->productDownload->date_expiration)) : '') : htmlentities(Tools::getValue('virtual_product_expiration_date'), ENT_COMPAT, 'UTF-8');
        $product->productDownload->nb_days_accessible = ($product->productDownload->id > 0) ? $product->productDownload->nb_days_accessible : htmlentities(Tools::getValue('virtual_product_nb_days'), ENT_COMPAT, 'UTF-8');
        $product->productDownload->is_shareable = $product->productDownload->id > 0 && $product->productDownload->is_shareable;

        $iso_tiny_mce = $this->context->language->iso_code;
        $iso_tiny_mce = (file_exists(_PS_JS_DIR_.'tiny_mce/langs/'.$iso_tiny_mce.'.js') ? $iso_tiny_mce : 'en');
        $data->assign(array(
            'ad' => __PS_BASE_URI__.basename(_PS_ADMIN_DIR_),
            'iso_tiny_mce' => $iso_tiny_mce,
            'product' => $product,
            'token' => $this->token,
            'currency' => $currency,
            'link' => $this->context->link,
            'is_file' => $product->productDownload->checkFile(),
            'virtual_product_file_uploader' => $virtual_product_file_uploader->render()
        ));
        $data->assign($this->tpl_form_vars);
        $this->tpl_form_vars['product'] = $product;
        $this->tpl_form_vars['custom_form'] = $data->fetch();
    }

    protected function _getFinalPrice($specific_price, $product_price, $tax_rate)
    {
        return $this->object->getPrice(false, $specific_price['id_product_attribute'], 2);
    }

    protected function _displaySpecificPriceModificationForm($defaultCurrency, $shops, $currencies, $countries, $groups)
    {
        /** @var Product $obj */
        if (!($obj = $this->loadObject())) {
            return;
        }

        $page = (int)Tools::getValue('page');
        $content = '';
        $specific_prices = SpecificPrice::getByProductId((int)$obj->id);
        $specific_price_priorities = SpecificPrice::getPriority((int)$obj->id);
		
        $tmp = array();
        foreach ($shops as $shop) {
            $tmp[$shop['id_shop']] = $shop;
        }
        $shops = $tmp;
        $tmp = array();
        foreach ($currencies as $currency) {
            $tmp[$currency['id_currency']] = $currency;
        }
        $currencies = $tmp;

        $tmp = array();
        foreach ($countries as $country) {
            $tmp[$country['id_country']] = $country;
        }
        $countries = $tmp;

        $tmp = array();
        foreach ($groups as $group) {
            $tmp[$group['id_group']] = $group;
        }
        $groups = $tmp;

        $length_before = strlen($content);
        if (is_array($specific_prices) && count($specific_prices)) {
            $i = 0;
            foreach ($specific_prices as $specific_price) {
                $id_currency = $specific_price['id_currency'] ? $specific_price['id_currency'] : $defaultCurrency->id;
                if (!isset($currencies[$id_currency])) {
                    continue;
                }

                $current_specific_currency = $currencies[$id_currency];
               

                if ($specific_price['from'] == '0000-00-00 00:00:00' && $specific_price['to'] == '0000-00-00 00:00:00') {
                    $period = $this->l('Unlimited');
                } else {
                    $period = $this->l('From').' '.($specific_price['from'] != '0000-00-00 00:00:00' ? $specific_price['from'] : '0000-00-00 00:00:00').'<br />'.$this->l('To').' '.($specific_price['to'] != '0000-00-00 00:00:00' ? $specific_price['to'] : '0000-00-00 00:00:00');
                }
                if ($specific_price['id_product_attribute']) {
                    $combination = new Combination((int)$specific_price['id_product_attribute']);
                    $attributes = $combination->getAttributesName((int)$this->context->language->id);
                    $attributes_name = '';
                    foreach ($attributes as $attribute) {
                        $attributes_name .= $attribute['name'].' - ';
                    }
                    $attributes_name = rtrim($attributes_name, ' - ');
                } else {
                    $attributes_name = $this->l('All combinations');
                }

                $rule = new SpecificPriceRule((int)$specific_price['id_specific_price_rule']);
                $rule_name = ($rule->id ? $rule->name : '--');

                if ($specific_price['id_customer']) {
                    $customer = new Customer((int)$specific_price['id_customer']);
                    if (Validate::isLoadedObject($customer)) {
                        $customer_full_name = $customer->firstname.' '.$customer->lastname;
                    }
                    unset($customer);
                }

                if (!$specific_price['id_shop'] || in_array($specific_price['id_shop'], Shop::getContextListShopID())) {
                    $content .= '
					<tr '.($i % 2 ? 'class="alt_row"' : '').'>
						<td>'.$rule_name.'</td>
						<td>Vendita</td>
						<td>'.$attributes_name.'</td>';

                    $can_delete_specific_prices = true;
                    if (Shop::isFeatureActive()) {
                        $id_shop_sp = $specific_price['id_shop'];
                        $can_delete_specific_prices = (count($this->context->employee->getAssociatedShops()) > 1 && !$id_shop_sp) || $id_shop_sp;
                        $content .= '
						<td>'.($id_shop_sp ? $shops[$id_shop_sp]['name'] : $this->l('All shops')).'</td>';
                    }
                    $price = Tools::ps_round($specific_price['price'], 2);
                    $fixed_price = ( $specific_price['price'] == -1) ? '--' : Tools::displayPrice($price, $current_specific_currency);
					$prezzo_finale = $price;
					 if ($specific_price['reduction_type'] == 'percentage') {
						$impact = '- '.($specific_price['reduction'] * 100).' %';
						$prezzo_finale -= ($prezzo_finale * $specific_price['reduction']);
					} elseif ($specific_price['reduction'] > 0) {
						$prezzo_finale -=$specific_price['reduction'];
						$impact = '- '.Tools::displayPrice(Tools::ps_round($specific_price['reduction'], 2), $current_specific_currency).' ';
						if ($specific_price['reduction_tax']) {
							$impact .= '('.$this->l('Tax incl.').')';
						} else {
							$impact .= '('.$this->l('Tax excl.').')';
						}
					} else {
						$impact = '--';
					}
					$prezzo_finale = Tools::displayPrice($price, $current_specific_currency);
                    $content .= '
						<td>'.($specific_price['id_currency'] ? $currencies[$specific_price['id_currency']]['name'] : $this->l('All currencies')).'</td>
						<td>'.($specific_price['id_country'] ? $countries[$specific_price['id_country']]['name'] : $this->l('All countries')).'</td>
						<td>'.($specific_price['id_group'] ? $groups[$specific_price['id_group']]['name'] : $this->l('All groups')).'</td>
						<td title="'.$this->l('ID:').' '.$specific_price['id_customer'].'">'.(isset($customer_full_name) ? $customer_full_name : $this->l('All customers')).'</td>
						<td>'.$fixed_price.'</td>
						<td>'.$impact.'</td>
						<td>'.$prezzo_finale.'</td>
						<td>'.$period.'</td>
						<td>'.$specific_price['from_quantity'].'</th>
						<td>'.((!$rule->id && $can_delete_specific_prices) ? '<a class="btn btn-default" name="delete_link" href="'.self::$currentIndex.'&id_product='.(int)Tools::getValue('id_product').'&action=deleteSpecificPrice&id_specific_price='.(int)($specific_price['id_specific_price']).'&token='.Tools::getValue('token').'"><i class="icon-trash"></i></a>': '').'</td>
					</tr>';
                    $i++;
                    unset($customer_full_name);
                }
            }
        }
		
		$specific_price_wholesale = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'specific_price_wholesale WHERE id_product = '.Tools::getValue('id_product'));
		
		foreach($specific_price_wholesale as $spw)
		{
			$content .= '
			<tr '.($i % 2 ? 'class="alt_row"' : '').'>
				<td>--</td>
				<td>Acquisto</td>
				<td>'.$attributes_name.'</td>';

			$can_delete_specific_prices_wholesale = true;
			$prezzo_finale = $spw['price'];
			$prezzo_finale -= ($spw['price'] * ($spw['reduction_1']/100));
			 $content .= '
						<td>--</td>
						<td>--</td>
						<td>--</td>
						<td>--</td>
						<td>'.Tools::displayPrice($spw['price'], $current_specific_currency).'</td>
						<td>-'.$spw['reduction_1'].'%</td>
						<td>'.Tools::displayPrice($prezzo_finale, $current_specific_currency).'</td>
						<td>'.$period.'</td>
						<td>'.$specific_price['from_quantity'].'</th>
						<td>'.(($can_delete_specific_prices_wholesale) ? '<a class="btn btn-default" name="delete_link_wholesale" href="'.self::$currentIndex.'&id_product='.(int)Tools::getValue('id_product').'&action=deleteSpecificWholesale&id_specific_price='.(int)($spw['id_specific_price']).'&token='.Tools::getValue('token').'"><i class="icon-trash"></i></a>': '').'</td>
					</tr>';
			$i++;
		}

        if ($length_before === strlen($content)) {
            $content .= '
				<tr>
					<td class="text-center" colspan="13"><i class="icon-warning-sign"></i>&nbsp;'.$this->l('No specific prices.').'</td>
				</tr>';
        }

        $content .= '
				</tbody>
			</table>
			</div>
			<div class="panel-footer">
				<a href="'.$this->context->link->getAdminLink('AdminProducts').($page > 1 ? '&submitFilter'.$this->table.'='.(int)$page : '').'" class="btn btn-default"><i class="process-icon-cancel"></i> '.$this->l('Cancel').'</a>
				<button id="product_form_submit_btn"  type="submit" name="submitAddproduct" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> '.$this->l('Save') .'</button>
				<button id="product_form_submit_btn"  type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> '.$this->l('Save and stay') .'</button>
			</div>
		</div>';

        $content .= '
		<script type="text/javascript">
			var currencies = new Array();
			currencies[0] = new Array();
			currencies[0]["sign"] = "'.$defaultCurrency->sign.'";
			currencies[0]["format"] = '.intval($defaultCurrency->format).';
			';
        foreach ($currencies as $currency) {
            $content .= '
				currencies['.$currency['id_currency'].'] = new Array();
				currencies['.$currency['id_currency'].']["sign"] = "'.$currency['sign'].'";
				currencies['.$currency['id_currency'].']["format"] = '.intval($currency['format']).';
				';
        }
        $content .= '
		</script>
		';

        // Not use id_customer
        if ($specific_price_priorities[0] == 'id_customer') {
            unset($specific_price_priorities[0]);
        }
        // Reindex array starting from 0
        $specific_price_priorities = array_values($specific_price_priorities);

        $content .= '<div class="panel">
		<h3>'.$this->l('Priority management').'</h3>
		<div class="alert alert-info">
				'.$this->l('Sometimes one customer can fit into multiple price rules. Priorities allow you to define which rule applies to the customer.').'
		</div>';

        $content .= '
		<div class="form-group">
			<label class="control-label col-lg-3" for="specificPricePriority1">'.$this->l('Priorities').'</label>
			<div class="input-group col-lg-9">
				<select id="specificPricePriority1" name="specificPricePriority[]">
					<option value="id_shop"'.($specific_price_priorities[0] == 'id_shop' ? ' selected="selected"' : '').'>'.$this->l('Shop').'</option>
					<option value="id_currency"'.($specific_price_priorities[0] == 'id_currency' ? ' selected="selected"' : '').'>'.$this->l('Currency').'</option>
					<option value="id_country"'.($specific_price_priorities[0] == 'id_country' ? ' selected="selected"' : '').'>'.$this->l('Country').'</option>
					<option value="id_group"'.($specific_price_priorities[0] == 'id_group' ? ' selected="selected"' : '').'>'.$this->l('Group').'</option>
				</select>
				<span class="input-group-addon"><i class="icon-chevron-right"></i></span>
				<select name="specificPricePriority[]">
					<option value="id_shop"'.($specific_price_priorities[1] == 'id_shop' ? ' selected="selected"' : '').'>'.$this->l('Shop').'</option>
					<option value="id_currency"'.($specific_price_priorities[1] == 'id_currency' ? ' selected="selected"' : '').'>'.$this->l('Currency').'</option>
					<option value="id_country"'.($specific_price_priorities[1] == 'id_country' ? ' selected="selected"' : '').'>'.$this->l('Country').'</option>
					<option value="id_group"'.($specific_price_priorities[1] == 'id_group' ? ' selected="selected"' : '').'>'.$this->l('Group').'</option>
				</select>
				<span class="input-group-addon"><i class="icon-chevron-right"></i></span>
				<select name="specificPricePriority[]">
					<option value="id_shop"'.($specific_price_priorities[2] == 'id_shop' ? ' selected="selected"' : '').'>'.$this->l('Shop').'</option>
					<option value="id_currency"'.($specific_price_priorities[2] == 'id_currency' ? ' selected="selected"' : '').'>'.$this->l('Currency').'</option>
					<option value="id_country"'.($specific_price_priorities[2] == 'id_country' ? ' selected="selected"' : '').'>'.$this->l('Country').'</option>
					<option value="id_group"'.($specific_price_priorities[2] == 'id_group' ? ' selected="selected"' : '').'>'.$this->l('Group').'</option>
				</select>
				<span class="input-group-addon"><i class="icon-chevron-right"></i></span>
				<select name="specificPricePriority[]">
					<option value="id_shop"'.($specific_price_priorities[3] == 'id_shop' ? ' selected="selected"' : '').'>'.$this->l('Shop').'</option>
					<option value="id_currency"'.($specific_price_priorities[3] == 'id_currency' ? ' selected="selected"' : '').'>'.$this->l('Currency').'</option>
					<option value="id_country"'.($specific_price_priorities[3] == 'id_country' ? ' selected="selected"' : '').'>'.$this->l('Country').'</option>
					<option value="id_group"'.($specific_price_priorities[3] == 'id_group' ? ' selected="selected"' : '').'>'.$this->l('Group').'</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="col-lg-9 col-lg-offset-3">
				<p class="checkbox">
					<label for="specificPricePriorityToAll"><input type="checkbox" name="specificPricePriorityToAll" id="specificPricePriorityToAll" />'.$this->l('Apply to all products').'</label>
				</p>
			</div>
		</div>
		<div class="panel-footer">
				<a href="'.$this->context->link->getAdminLink('AdminProducts').($page > 1 ? '&submitFilter'.$this->table.'='.(int)$page : '').'" class="btn btn-default"><i class="process-icon-cancel"></i> '.$this->l('Cancel').'</a>
				<button id="product_form_submit_btn"  type="submit" name="submitAddproduct" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> '.$this->l('Save') .'</button>
				<button id="product_form_submit_btn"  type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> '.$this->l('Save and stay') .'</button>
			</div>
		</div>
		';
        return $content;
    }

    protected function _getCustomizationFieldIds($labels, $alreadyGenerated, $obj)
    {
        $customizableFieldIds = array();
        if (isset($labels[Product::CUSTOMIZE_FILE])) {
            foreach ($labels[Product::CUSTOMIZE_FILE] as $id_customization_field => $label) {
                $customizableFieldIds[] = 'label_'.Product::CUSTOMIZE_FILE.'_'.(int)($id_customization_field);
            }
        }
        if (isset($labels[Product::CUSTOMIZE_TEXTFIELD])) {
            foreach ($labels[Product::CUSTOMIZE_TEXTFIELD] as $id_customization_field => $label) {
                $customizableFieldIds[] = 'label_'.Product::CUSTOMIZE_TEXTFIELD.'_'.(int)($id_customization_field);
            }
        }
        $j = 0;
        for ($i = $alreadyGenerated[Product::CUSTOMIZE_FILE]; $i < (int)($this->getFieldValue($obj, 'uploadable_files')); $i++) {
            $customizableFieldIds[] = 'newLabel_'.Product::CUSTOMIZE_FILE.'_'.$j++;
        }
        $j = 0;
        for ($i = $alreadyGenerated[Product::CUSTOMIZE_TEXTFIELD]; $i < (int)($this->getFieldValue($obj, 'text_fields')); $i++) {
            $customizableFieldIds[] = 'newLabel_'.Product::CUSTOMIZE_TEXTFIELD.'_'.$j++;
        }
        return implode('¤', $customizableFieldIds);
    }

    protected function _displayLabelField(&$label, $languages, $default_language, $type, $fieldIds, $id_customization_field)
    {
        foreach ($languages as $language) {
            $input_value[$language['id_lang']] = (isset($label[(int)($language['id_lang'])])) ? $label[(int)($language['id_lang'])]['name'] : '';
        }

        $required = (isset($label[(int)($language['id_lang'])])) ? $label[(int)($language['id_lang'])]['required'] : false;

        $template = $this->context->smarty->createTemplate('controllers/products/input_text_lang.tpl',
            $this->context->smarty);
        return '<div class="form-group">'
            .'<div class="col-lg-6">'
            .$template->assign(array(
                'languages' => $languages,
                'input_name'  => 'label_'.$type.'_'.(int)($id_customization_field),
                'input_value' => $input_value
            ))->fetch()
            .'</div>'
            .'<div class="col-lg-6">'
            .'<div class="checkbox">'
            .'<label for="require_'.$type.'_'.(int)($id_customization_field).'">'
            .'<input type="checkbox" name="require_'.$type.'_'.(int)($id_customization_field).'" id="require_'.$type.'_'.(int)($id_customization_field).'" value="1" '.($required ? 'checked="checked"' : '').'/>'
            .$this->l('Required')
            .'</label>'
            .'</div>'
            .'</div>'
            .'</div>';
    }

    protected function _displayLabelFields(&$obj, &$labels, $languages, $default_language, $type)
    {
        $content = '';
        $type = (int)($type);
        $labelGenerated = array(Product::CUSTOMIZE_FILE => (isset($labels[Product::CUSTOMIZE_FILE]) ? count($labels[Product::CUSTOMIZE_FILE]) : 0), Product::CUSTOMIZE_TEXTFIELD => (isset($labels[Product::CUSTOMIZE_TEXTFIELD]) ? count($labels[Product::CUSTOMIZE_TEXTFIELD]) : 0));

        $fieldIds = $this->_getCustomizationFieldIds($labels, $labelGenerated, $obj);
        if (isset($labels[$type])) {
            foreach ($labels[$type] as $id_customization_field => $label) {
                $content .= $this->_displayLabelField($label, $languages, $default_language, $type, $fieldIds, (int)($id_customization_field));
            }
        }
        return $content;
    }

    /**
     * @param Product $obj
     * @throws Exception
     * @throws SmartyException
     */
    public function initFormCustomization($obj)
    {
        $data = $this->createTemplate($this->tpl_form);

        if ((bool)$obj->id) {
            if ($this->product_exists_in_shop) {
                $labels = $obj->getCustomizationFields();

                $has_file_labels = (int)$this->getFieldValue($obj, 'uploadable_files');
                $has_text_labels = (int)$this->getFieldValue($obj, 'text_fields');

                $data->assign(array(
                    'obj' => $obj,
                    'table' => $this->table,
                    'languages' => $this->_languages,
                    'has_file_labels' => $has_file_labels,
                    'display_file_labels' => $this->_displayLabelFields($obj, $labels, $this->_languages, Configuration::get('PS_LANG_DEFAULT'), Product::CUSTOMIZE_FILE),
                    'has_text_labels' => $has_text_labels,
                    'display_text_labels' => $this->_displayLabelFields($obj, $labels, $this->_languages, Configuration::get('PS_LANG_DEFAULT'), Product::CUSTOMIZE_TEXTFIELD),
                    'uploadable_files' => (int)($this->getFieldValue($obj, 'uploadable_files') ? (int)$this->getFieldValue($obj, 'uploadable_files') : '0'),
                    'text_fields' => (int)($this->getFieldValue($obj, 'text_fields') ? (int)$this->getFieldValue($obj, 'text_fields') : '0'),
                ));
            } else {
                $this->displayWarning($this->l('You must save the product in this shop before adding customization.'));
            }
        } else {
            $this->displayWarning($this->l('You must save this product before adding customization.'));
        }

        $this->tpl_form_vars['custom_form'] = $data->fetch();
    }

    // CORREGGERE: finire tpl e js
    public function initFormAttachments($obj)
    {
        if (!$this->default_form_language) {
            $this->getLanguages();
        }

        $data = $this->createTemplate($this->tpl_form);
        $data->assign('default_form_language', $this->default_form_language);

        if ((bool)$obj->id) {
            if ($this->product_exists_in_shop) {
                $attachment_name = array();
                $attachment_description = array();
                foreach ($this->_languages as $language) {
                    $attachment_name[$language['id_lang']] = '';
                    $attachment_description[$language['id_lang']] = '';
                }

                $iso_tiny_mce = $this->context->language->iso_code;
                $iso_tiny_mce = (file_exists(_PS_JS_DIR_.'tiny_mce/langs/'.$iso_tiny_mce.'.js') ? $iso_tiny_mce : 'en');

                $attachment_uploader = new HelperUploader('attachment_file');
                $attachment_uploader->setMultiple(false)->setUseAjax(true)->setUrl(
                    Context::getContext()->link->getAdminLink('AdminProducts').'&ajax=1&id_product='.(int)$obj->id
                    .'&action=AddAttachment')->setPostMaxSize((Configuration::get('PS_ATTACHMENT_MAXIMUM_SIZE') * 1024 * 1024))
                    ->setTemplate('attachment_ajax.tpl');

                $data->assign(array(
                    'obj' => $obj,
                    'table' => $this->table,
                    'ad' => __PS_BASE_URI__.basename(_PS_ADMIN_DIR_),
                    'iso_tiny_mce' => $iso_tiny_mce,
                    'languages' => $this->_languages,
                    'id_lang' => $this->context->language->id,
                    'attach1' => Attachment::getAttachments($this->context->language->id, $obj->id, true),
                    'attach2' => Attachment::getAttachments($this->context->language->id, $obj->id, false),
                    'default_form_language' => (int)Configuration::get('PS_LANG_DEFAULT'),
                    'attachment_name' => $attachment_name,
                    'attachment_description' => $attachment_description,
                    'attachment_uploader' => $attachment_uploader->render()
                ));
            } else {
                $this->displayWarning($this->l('You must save the product in this shop before adding attachements.'));
            }
        } else {
            $this->displayWarning($this->l('You must save this product before adding attachements.'));
        }

        $this->tpl_form_vars['custom_form'] = $data->fetch();

        // Override
        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'allegati_prodotto.js');
        $this->displayWarning('Correggere allegati_prodotto.js e select2mul');
    }

    /**
     * @param Product $product
     * @throws Exception
     * @throws SmartyException
     */
    public function initFormInformations($product)
    {
        if (!$this->default_form_language) {
            $this->getLanguages();
        }

        $data = $this->createTemplate($this->tpl_form);

        $currency = $this->context->currency;

        $data->assign(array(
            'languages' => $this->_languages,
            'default_form_language' => $this->default_form_language,
            'currency' => $currency
        ));
        $this->object = $product;
        //$this->display = 'edit';
        $data->assign('product_name_redirected', Product::getProductName((int)$product->id_product_redirected, null, (int)$this->context->language->id));
        /*
        * Form for adding a virtual product like software, mp3, etc...
        */
        $product_download = new ProductDownload();
        if ($id_product_download = $product_download->getIdFromIdProduct($this->getFieldValue($product, 'id'))) {
            $product_download = new ProductDownload($id_product_download);
        }

        $product->{'productDownload'} = $product_download;

        $product_props = array();
        // global informations
        array_push($product_props, 'reference', 'ean13', 'upc',
        'available_for_order', 'show_price', 'online_only',
        'id_manufacturer'
        );

        // specific / detailled information
        array_push($product_props,
        // physical product
        'width', 'height', 'weight', 'active',
        // virtual product
        'is_virtual', 'cache_default_attribute',
        // customization
        'uploadable_files', 'text_fields'
        );
        // prices
        array_push($product_props,
            'price', 'wholesale_price', 'id_tax_rules_group', 'unit_price_ratio', 'on_sale',
            'unity', 'minimum_quantity', 'additional_shipping_cost',
            'available_now', 'available_later', 'available_date'
        );

        if (Configuration::get('PS_USE_ECOTAX')) {
            array_push($product_props, 'ecotax');
        }

        foreach ($product_props as $prop) {
            $product->$prop = $this->getFieldValue($product, $prop);
        }

        $product->name['class'] = 'updateCurrentText';
        if (!$product->id || Configuration::get('PS_FORCE_FRIENDLY_PRODUCT')) {
            $product->name['class'] .= ' copy2friendlyUrl';
        }

        $images = Image::getImages($this->context->language->id, $product->id);

        if (is_array($images)) {
            foreach ($images as $k => $image) {
                $images[$k]['src'] = $this->context->link->getImageLink($product->link_rewrite[$this->context->language->id], $product->id.'-'.$image['id_image'], ImageType::getFormatedName('small'));
            }
            $data->assign('images', $images);
        }
        $data->assign('imagesTypes', ImageType::getImagesTypes('products'));

        $product->tags = Tag::getProductTags($product->id);

        $data->assign('product_type', (int)Tools::getValue('type_product', $product->getType()));
        $data->assign('is_in_pack', (int)Pack::isPacked($product->id));

        $check_product_association_ajax = false;
        if (Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_ALL) {
            $check_product_association_ajax = true;
        }

        /* OVERRIDE */

        // PANEL 1 DX

        if(!$product->id) {  // test!!!!!!!!! eliminare? correggere
            $product->id = 0;
            $product->id_manufacturer = 0;
        }
        else {
            // Mostro un avviso se reference, supplier_reference e ean13 esistono già in un altro prodotto

            if($product->reference == '') { }
            else {
                $rowdoppio = Db::getInstance()->getRow("SELECT id_product, reference FROM "._DB_PREFIX_."product WHERE reference = '".$product->reference."' AND active != 2 AND id_product != ".$product->id);
                
                if($rowdoppio['reference'] == $product->reference) {
                    $href_doppio = 'index.php?controller=AdminProducts&id_product='.$rowdoppio["id_product"].'&updateproduct&token='.$this->token;
                    $avviso_doppio = 'Esiste già un prodotto nel database associato al codice di riferimento che hai inserito: <a href="'.$href_doppio.'" target="_blank">'.$rowdoppio['id_product'].'</a>.';
                }
            }

            if($product->supplier_reference == '') { }
            else {
                $rowdoppio = Db::getInstance()->getRow("SELECT id_product, supplier_reference FROM "._DB_PREFIX_."product WHERE supplier_reference = '".$product->supplier_reference."' AND active != 2 AND id_product != ".$product->id);
                
                if($rowdoppio['supplier_reference'] === $product->supplier_reference) {
                    $href_doppio = 'index.php?controller=AdminProducts&id_product='.$rowdoppio["id_product"].'&updateproduct&token='.$this->token;
                    $avviso_doppio = 'Esiste già un prodotto nel database associato al codice SKU che hai inserito: <a href="'.$href_doppio.'" target="_blank">'.$rowdoppio['id_product'].'</a>.';
                }
            }

            if($product->ean13 == '') { }
            else {
                $rowdoppio = Db::getInstance()->getRow("SELECT id_product, ean13 FROM "._DB_PREFIX_."product WHERE ean13 = '".$product->ean13."' AND active != 2 AND id_product != ".$product->id);
                
                if($rowdoppio['ean13'] === $product->ean13) {
                    $href_doppio = 'index.php?controller=AdminProducts&id_product='.$rowdoppio["id_product"].'&updateproduct&token='.$this->token;
                    $avviso_doppio = 'Esiste già un prodotto nel database associato al codice EAN che hai inserito: <a href="'.$href_doppio.'" target="_blank">'.$rowdoppio['id_product'].'</a>.';
                }
            }

            $data->assign('avviso_doppio', $avviso_doppio);
        }

        $product->date_available = ($product->date_available == '1946-01-01' ? date('d-m-Y') : $product->date_available);
        $product->data_arrivo = ($product->data_arrivo == '1946-01-01' ? date('d-m-Y') : ($product->data_arrivo == '0000-00-00' || $product->data_arrivo == '1970-01-01' ? '' : $product->data_arrivo));

        $product->manufacturer_name = Manufacturer::getNameById($product->id_manufacturer);
        $product->supplier_name = Supplier::getNameById($product->id_supplier);

        $data_margin_cli = Db::getInstance()->getValue('SELECT margin_min_cli FROM '._DB_PREFIX_.'manufacturer WHERE id_manufacturer = '.$product->id_manufacturer);
        $data_margin_riv = Db::getInstance()->getValue('SELECT margin_min_riv FROM '._DB_PREFIX_.'manufacturer WHERE id_manufacturer = '.$product->id_manufacturer);
        $data_supplier = Manufacturer::getSupplierById($product->id_manufacturer);
        $data_othersuppliers = Manufacturer::getOtherSuppliersById($product->id_manufacturer);

        $fornitori = Db::getInstance()->executeS('SELECT id_supplier, name FROM '._DB_PREFIX_.'supplier ORDER BY name');
        $altri_fornitori = Db::getInstance()->getValue('SELECT other_suppliers FROM '._DB_PREFIX_.'product WHERE id_product = '.$product->id);
		$altri_fornitori = unserialize($altri_fornitori);

        $data->assign('data_margin_cli', $data_margin_cli);
        $data->assign('data_margin_riv', $data_margin_riv);
        $data->assign('data_supplier', $data_supplier);
        $data->assign('data_othersuppliers', $data_othersuppliers);

        $data->assign('fornitori', $fornitori);
        $data->assign('altri_fornitori', $altri_fornitori);
        
        // AVVISI CANONICAL
        
        $padrecanonical = Db::getInstance()->executeS("
            SELECT id_product, id_category_default, reference 
            FROM "._DB_PREFIX_."product 
            WHERE reference != '' AND canonical != 0 AND canonical = ".$product->id
        );

        $figlicanonical = array();
        foreach($padrecanonical as $padre) {
            $figlicanonical[] = $padre['reference'];
        }

        if(count($figlicanonical) > 0)
            $canonical = true;
        else
            $canonical = false;

        $data->assign('canonical', $canonical);
        $data->assign('padrecanonical', $padrecanonical);

        // Video -> modificato rispetto alla 1.4: aggiunto foreach per poter eliminare tutti i video del prodotto

        $rowsvideos = Db::getInstance()->executeS('SELECT id, content FROM product_video WHERE id_product = '.$product->id);

        foreach($rowsvideos as &$rowvideos)
            $rowvideos['videoc'] = str_replace('embed/', 'watch?v=', $rowvideos['content']);

        $data->assign('rowsvideos', $rowsvideos);

        // Comparaprezzi

        // CORREGGERE! Come fa ad essere diverso da 1? Se !$numcompara, il prodotto non esiste! Se è per distinguere add da update, meglio usare $product->id 
        $numcompara = Db::getInstance()->getValue('
            SELECT COUNT(amazon) 
            FROM '._DB_PREFIX_.'product 
            WHERE id_product = '.$product->id
        );

        // JS: function checkUncheckSome(controller,theElements) -> per ora messa nel tpl

        if(!$numcompara) {
            // solo tpl senza set variabili rowcompara e style???
        } 
        else {
            // tpl: usare $product->comparaprezzi_check
            /*$rowcompara = Db::getInstance()->getRow('
                SELECT trovaprezzi, google_shopping, amazon, eprice, comparaprezzi_check 
                FROM '._DB_PREFIX_.'product 
                WHERE id_product = '.$product->id
            );*/
            
            $amazon_check = explode(";", $product->amazon);

            // CORREGGERE: va bene questa condizione per input checked? Tools::getIsset('tutti_i_comparaprezzi') è necessario?
            $amazon_check_it = ((in_array("5", $amazon_check) || (Tools::getIsset('amazon_it_checkbox') && Tools::getIsset('tutti_i_comparaprezzi'))) ? true : false); 
            $amazon_check_fr = ((in_array("2", $amazon_check) || (Tools::getIsset('amazon_fr_checkbox') && Tools::getIsset('tutti_i_comparaprezzi'))) ? true : false); 
            $amazon_check_es = ((in_array("3", $amazon_check) || (Tools::getIsset('amazon_es_checkbox') && Tools::getIsset('tutti_i_comparaprezzi'))) ? true : false); 
            $amazon_check_uk = ((in_array("1", $amazon_check) || (Tools::getIsset('amazon_uk_checkbox') && Tools::getIsset('tutti_i_comparaprezzi'))) ? true : false); 
            $amazon_check_de = ((in_array("4", $amazon_check) || (Tools::getIsset('amazon_de_checkbox') && Tools::getIsset('tutti_i_comparaprezzi'))) ? true : false); 
            $amazon_check_nl = ((in_array("6", $amazon_check) || (Tools::getIsset('amazon_nl_checkbox') && Tools::getIsset('tutti_i_comparaprezzi'))) ? true : false); 
            $amazon_check_se = ((in_array("7", $amazon_check) || (Tools::getIsset('amazon_se_checkbox') && Tools::getIsset('tutti_i_comparaprezzi'))) ? true : false); 
            $amazon_check_pl = ((in_array("8", $amazon_check) || (Tools::getIsset('amazon_pl_checkbox') && Tools::getIsset('tutti_i_comparaprezzi'))) ? true : false); 

            $actives = Db::getInstance()->getRow('
                SELECT * 
                FROM amazon_products 
                WHERE id_product = '.$product->id
            );

            $amazon_it_style = ($actives['it'] > 0 ? "font-weight:bold; color:#008000" : "");
            $amazon_fr_style = ($actives['fr'] > 0 ? "font-weight:bold; color:#008000" : "");
            $amazon_es_style = ($actives['es'] > 0 ? "font-weight:bold; color:#008000" : "");
            $amazon_uk_style = ($actives['uk'] > 0 ? "font-weight:bold; color:#008000" : "");
            $amazon_de_style = ($actives['de'] > 0 ? "font-weight:bold; color:#008000" : "");
            $amazon_nl_style = ($actives['nl'] > 0 ? "font-weight:bold; color:#008000" : "");
            $amazon_se_style = ($actives['se'] > 0 ? "font-weight:bold; color:#008000" : "");
            $amazon_pl_style = ($actives['pl'] > 0 ? "font-weight:bold; color:#008000" : "");

            //$data->assign('rowcompara', $rowcompara);
            
            $data->assign('amazon_check_it', $amazon_check_it);
            $data->assign('amazon_check_fr', $amazon_check_fr);
            $data->assign('amazon_check_es', $amazon_check_es);
            $data->assign('amazon_check_uk', $amazon_check_uk);
            $data->assign('amazon_check_de', $amazon_check_de);
            $data->assign('amazon_check_nl', $amazon_check_nl);
            $data->assign('amazon_check_se', $amazon_check_se);
            $data->assign('amazon_check_pl', $amazon_check_pl);

            $data->assign('amazon_it_style', $amazon_it_style);
            $data->assign('amazon_fr_style', $amazon_fr_style);
            $data->assign('amazon_es_style', $amazon_es_style);
            $data->assign('amazon_uk_style', $amazon_uk_style);
            $data->assign('amazon_de_style', $amazon_de_style);
            $data->assign('amazon_nl_style', $amazon_nl_style);
            $data->assign('amazon_se_style', $amazon_se_style);
            $data->assign('amazon_pl_style', $amazon_pl_style);
        }

        // Punti di forza Amazon

        // Correggere! Sbagliato perchè applica prima il mio offset [0], [1], ... e poi quello di default (id_lang). Come faccio ad usare punti_di_forza in questa struttura?
        $punti_di_forza = Db::getInstance()->getValue('SELECT punti_di_forza FROM '._DB_PREFIX_.'product_lang WHERE id_lang = '.$this->context->language->id.' AND id_product = '.$product->id);
		$punti_di_forza = explode(':::::', $punti_di_forza);

        $data->assign('punti_di_forza', $punti_di_forza);

        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'altri_fornitori.js');

        /* FINE OVERRIDE */

        // TinyMCE
        $iso_tiny_mce = $this->context->language->iso_code;
        $iso_tiny_mce = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso_tiny_mce.'.js') ? $iso_tiny_mce : 'en');
        $data->assign(array(
            'ad' => dirname($_SERVER['PHP_SELF']),
            'iso_tiny_mce' => $iso_tiny_mce,
            'check_product_association_ajax' => $check_product_association_ajax,
            'id_lang' => $this->context->language->id,
            'product' => $product,
            'token' => $this->token,
            'currency' => $currency,
            'link' => $this->context->link,
            'PS_PRODUCT_SHORT_DESC_LIMIT' => Configuration::get('PS_PRODUCT_SHORT_DESC_LIMIT') ? Configuration::get('PS_PRODUCT_SHORT_DESC_LIMIT') : 400
        ));

        //$this->tpl_form_vars['product'] = $product;
        $data->assign($this->tpl_form_vars);

        $this->tpl_form_vars['custom_form'] = $data->fetch();

    }

    public function initFormShipping($obj)
    {
        $data = $this->createTemplate($this->tpl_form);
        $data->assign(array(
                        'product' => $obj,
                        'ps_dimension_unit' => Configuration::get('PS_DIMENSION_UNIT'),
                        'ps_weight_unit' => Configuration::get('PS_WEIGHT_UNIT'),
                        'carrier_list' => $this->getCarrierList(),
                        'currency' => $this->context->currency,
                        'country_display_tax_label' =>  $this->context->country->display_tax_label
                    ));
                    
        $this->displayWarning($this->l('Correggere configuration FREE_SHIPPING_INCLUDE_PRD e finire assicurazione trasporto'));
        
        // CORREGGERE: aggiungere "FREE_SHIPPING_INCLUDE_PRD" in ps_configuration (nella 1.4 aveva value 240)
        $row = Configuration::get('FREE_SHIPPING_INCLUDE_PRD');
        
        $trasp = unserialize($row['value']);
        $au = $obj->id;
        
        foreach($trasp as $el) {
            if ($au == $el) {
                $uu = 1;
                break;
            }
            else
                $uu = 0;
        }
        
        if($au == "")
            $uu = 0;

        $tg_checked = (Tools::getIsset('check_trasporto_gratuito') || Tools::getIsset('trasporto_gratuito_forzato') ? 1 : 0);

        $data->assign('uu', $uu);
        $data->assign('tg_checked', $tg_checked);

        $this->tpl_form_vars['custom_form'] = $data->fetch();
    }

    protected function getCarrierList()
    {
        $carrier_list = Carrier::getCarriers($this->context->language->id, false, false, false, null, Carrier::ALL_CARRIERS);

        if ($product = $this->loadObject(true)) {
            /** @var Product $product */
            $carrier_selected_list = $product->getCarriers();
            foreach ($carrier_list as &$carrier) {
                foreach ($carrier_selected_list as $carrier_selected) {
                    if ($carrier_selected['id_reference'] == $carrier['id_reference']) {
                        $carrier['selected'] = true;
                        continue;
                    }
                }
            }
        }
        return $carrier_list;
    }

    protected function addCarriers($product = null)
    {
        if (!isset($product)) {
            $product = new Product((int)Tools::getValue('id_product'));
        }

        if (Validate::isLoadedObject($product)) {
            $carriers = array();

            if (Tools::getValue('selectedCarriers')) {
                $carriers = Tools::getValue('selectedCarriers');
            }

            $product->setCarriers($carriers);
        }
    }

    public function ajaxProcessaddProductImage()
    {
        self::$currentIndex = 'index.php?tab=AdminProducts';
        $product = new Product((int)Tools::getValue('id_product'));
        $legends = Tools::getValue('legend');

        if (!is_array($legends)) {
            $legends = (array)$legends;
        }

        if (!Validate::isLoadedObject($product)) {
            $files = array();
            $files[0]['error'] = Tools::displayError('Cannot add image because product creation failed.');
        }

        $image_uploader = new HelperImageUploader('file');
        $image_uploader->setAcceptTypes(array('jpeg', 'gif', 'png', 'jpg'))->setMaxSize($this->max_image_size);
        $files = $image_uploader->process();

        foreach ($files as &$file) {
            $image = new Image();
            $image->id_product = (int)($product->id);
            $image->position = Image::getHighestPosition($product->id) + 1;

            foreach ($legends as $key => $legend) {
                if (!empty($legend)) {
                    $image->legend[(int)$key] = $legend;
                }
            }

            if (!Image::getCover($image->id_product)) {
                $image->cover = 1;
            } else {
                $image->cover = 0;
            }

            if (($validate = $image->validateFieldsLang(false, true)) !== true) {
                $file['error'] = Tools::displayError($validate);
            }

            if (isset($file['error']) && (!is_numeric($file['error']) || $file['error'] != 0)) {
                continue;
            }

            if (!$image->add()) {
                $file['error'] = Tools::displayError('Error while creating additional image');
            } else {
                if (!$new_path = $image->getPathForCreation()) {
                    $file['error'] = Tools::displayError('An error occurred during new folder creation');
                    continue;
                }

                $error = 0;

                if (!ImageManager::resize($file['save_path'], $new_path.'.'.$image->image_format, null, null, 'jpg', false, $error)) {
                    switch ($error) {
                        case ImageManager::ERROR_FILE_NOT_EXIST :
                            $file['error'] = Tools::displayError('An error occurred while copying image, the file does not exist anymore.');
                            break;

                        case ImageManager::ERROR_FILE_WIDTH :
                            $file['error'] = Tools::displayError('An error occurred while copying image, the file width is 0px.');
                            break;

                        case ImageManager::ERROR_MEMORY_LIMIT :
                            $file['error'] = Tools::displayError('An error occurred while copying image, check your memory limit.');
                            break;

                        default:
                            $file['error'] = Tools::displayError('An error occurred while copying image.');
                            break;
                    }
                    continue;
                } else {
                    $imagesTypes = ImageType::getImagesTypes('products');
                    $generate_hight_dpi_images = (bool)Configuration::get('PS_HIGHT_DPI');

                    foreach ($imagesTypes as $imageType) {
                        if (!ImageManager::resize($file['save_path'], $new_path.'-'.stripslashes($imageType['name']).'.'.$image->image_format, $imageType['width'], $imageType['height'], $image->image_format)) {
                            $file['error'] = Tools::displayError('An error occurred while copying image:').' '.stripslashes($imageType['name']);
                            continue;
                        }

                        if ($generate_hight_dpi_images) {
                            if (!ImageManager::resize($file['save_path'], $new_path.'-'.stripslashes($imageType['name']).'2x.'.$image->image_format, (int)$imageType['width']*2, (int)$imageType['height']*2, $image->image_format)) {
                                $file['error'] = Tools::displayError('An error occurred while copying image:').' '.stripslashes($imageType['name']);
                                continue;
                            }
                        }
                    }
                }

                unlink($file['save_path']);
                //Necesary to prevent hacking
                unset($file['save_path']);
                Hook::exec('actionWatermark', array('id_image' => $image->id, 'id_product' => $product->id));

                if (!$image->update()) {
                    $file['error'] = Tools::displayError('Error while updating status');
                    continue;
                }

                // Associate image to shop from context
                $shops = Shop::getContextListShopID();
                $image->associateTo($shops);
                $json_shops = array();

                foreach ($shops as $id_shop) {
                    $json_shops[$id_shop] = true;
                }

                $file['status']   = 'ok';
                $file['id']       = $image->id;
                $file['position'] = $image->position;
                $file['cover']    = $image->cover;
                $file['legend']   = $image->legend;
                $file['path']     = $image->getExistingImgPath();
                $file['shops']    = $json_shops;

                @unlink(_PS_TMP_IMG_DIR_.'product_'.(int)$product->id.'.jpg');
                @unlink(_PS_TMP_IMG_DIR_.'product_mini_'.(int)$product->id.'_'.$this->context->shop->id.'.jpg');
            }
        }

        die(Tools::jsonEncode(array($image_uploader->getName() => $files)));
    }

    /**
     * @param Product $obj
     * @throws Exception
     * @throws SmartyException
     */
    public function initFormImages($obj)
    {
        $data = $this->createTemplate($this->tpl_form);

        if ((bool)$obj->id) {
            if ($this->product_exists_in_shop) {
                $data->assign('product', $this->loadObject());

                $shops = false;
                if (Shop::isFeatureActive()) {
                    $shops = Shop::getShops();
                }

                if ($shops) {
                    foreach ($shops as $key => $shop) {
                        if (!$obj->isAssociatedToShop($shop['id_shop'])) {
                            unset($shops[$key]);
                        }
                    }
                }

                $data->assign('shops', $shops);

                $count_images = Db::getInstance()->getValue('
					SELECT COUNT(id_product)
					FROM '._DB_PREFIX_.'image
					WHERE id_product = '.(int)$obj->id
                );

                $images = Image::getImages($this->context->language->id, $obj->id);
                foreach ($images as $k => $image) {
                    /* OVERRIDE */
                    $img = new Image($image['id_image']);
                    
                    clearstatcache();
                    
                    $image_url = _PS_BASE_URL_._THEME_PROD_DIR_.$img->getExistingImgPath();
                    list($imwidth, $imheight) = getimagesize($image_url.".jpg");

                    $image_url = _PS_ROOT_DIR_._THEME_PROD_DIR_.$img->getExistingImgPath();
                    $fileimg_size_normal = round((filesize($image_url.".jpg")/1024), 2);
                    $fileimg_size_large = round((filesize($image_url."-large_default.jpg")/1024), 2);
                    $fileimg_size_box = round((filesize($image_url."-thickbox_default.jpg")/1024), 2);
                    $fileimg_size_medium = round((filesize($image_url."-medium_default.jpg")/1024), 2);
                    $fileimg_size_small = round((filesize($image_url."-small_default.jpg")/1024), 2);
                    
                    $img->imwidth = $imwidth;
                    $img->imheight = $imheight;
                    $img->original = $fileimg_size_normal;
                    $img->large = $fileimg_size_large;
                    $img->box = $fileimg_size_box;
                    $img->medium = $fileimg_size_medium;
                    $img->small = $fileimg_size_small;

                    $images[$k] = $img;
                    /* FINE OVERRIDE */
                    
                    // $images[$k] = new Image($image['id_image']);
                }

                if ($this->context->shop->getContext() == Shop::CONTEXT_SHOP) {
                    $current_shop_id = (int)$this->context->shop->id;
                } else {
                    $current_shop_id = 0;
                }

                $languages = Language::getLanguages(true);
                $image_uploader = new HelperImageUploader('file');
                $image_uploader->setMultiple(!(Tools::getUserBrowser() == 'Apple Safari' && Tools::getUserPlatform() == 'Windows'))
                    ->setUseAjax(true)->setUrl(
                    Context::getContext()->link->getAdminLink('AdminProducts').'&ajax=1&id_product='.(int)$obj->id
                    .'&action=addProductImage');

                $data->assign(array(
                        'countImages' => $count_images,
                        'id_product' => (int)Tools::getValue('id_product'),
                        'id_category_default' => (int)$this->_category->id,
                        'images' => $images,
                        'iso_lang' => $languages[0]['iso_code'],
                        'token' =>  $this->token,
                        'table' => $this->table,
                        'max_image_size' => $this->max_image_size / 1024 / 1024,
                        'up_filename' => (string)Tools::getValue('virtual_product_filename_attribute'),
                        'currency' => $this->context->currency,
                        'current_shop_id' => $current_shop_id,
                        'languages' => $this->_languages,
                        'default_language' => (int)Configuration::get('PS_LANG_DEFAULT'),
                        'image_uploader' => $image_uploader->render()
                ));

                $type = ImageType::getByNameNType('%', 'products', 'height');
                if (isset($type['name'])) {
                    $data->assign('imageType', $type['name']);
                } else {
                    $data->assign('imageType', ImageType::getFormatedName('small'));
                }
            } else {
                $this->displayWarning($this->l('You must save the product in this shop before adding images.'));
            }
        } else {
            $this->displayWarning($this->l('You must save this product before adding images.'));
        }

        $this->tpl_form_vars['custom_form'] = $data->fetch();
    }

    public function initFormCombinations($obj)
    {
        return $this->initFormAttributes($obj);
    }

    /**
     * @param Product $product
     * @throws Exception
     * @throws SmartyException
     */
    public function initFormAttributes($product)
    {
        $data = $this->createTemplate($this->tpl_form);
        if (!Combination::isFeatureActive()) {
            $this->displayWarning($this->l('This feature has been disabled. ').
                ' <a href="index.php?tab=AdminPerformance&token='.Tools::getAdminTokenLite('AdminPerformance').'#featuresDetachables">'.$this->l('Performances').'</a>');
        } elseif (Validate::isLoadedObject($product)) {
            if ($this->product_exists_in_shop) {
                if ($product->is_virtual) {
                    $data->assign('product', $product);
                    $this->displayWarning($this->l('A virtual product cannot have combinations.'));
                } else {
                    $attribute_js = array();
                    $attributes = Attribute::getAttributes($this->context->language->id, true);
                    foreach ($attributes as $k => $attribute) {
                        $attribute_js[$attribute['id_attribute_group']][$attribute['id_attribute']] = $attribute['name'];
                        natsort($attribute_js[$attribute['id_attribute_group']]);
                    }

                    $currency = $this->context->currency;

                    $data->assign('attributeJs', $attribute_js);
                    $data->assign('attributes_groups', AttributeGroup::getAttributesGroups($this->context->language->id));

                    $data->assign('currency', $currency);

                    $images = Image::getImages($this->context->language->id, $product->id);

                    $data->assign('tax_exclude_option', Tax::excludeTaxeOption());
                    $data->assign('ps_weight_unit', Configuration::get('PS_WEIGHT_UNIT'));

                    $data->assign('ps_use_ecotax', Configuration::get('PS_USE_ECOTAX'));
                    $data->assign('field_value_unity', $this->getFieldValue($product, 'unity'));

                    $data->assign('reasons', $reasons = StockMvtReason::getStockMvtReasons($this->context->language->id));
                    $data->assign('ps_stock_mvt_reason_default', $ps_stock_mvt_reason_default = Configuration::get('PS_STOCK_MVT_REASON_DEFAULT'));
                    $data->assign('minimal_quantity', $this->getFieldValue($product, 'minimal_quantity') ? $this->getFieldValue($product, 'minimal_quantity') : 1);
                    $data->assign('available_date', ($this->getFieldValue($product, 'available_date') != 0) ? stripslashes(htmlentities($this->getFieldValue($product, 'available_date'), $this->context->language->id)) : '0000-00-00');

                    $i = 0;
                    $type = ImageType::getByNameNType('%', 'products', 'height');
                    if (isset($type['name'])) {
                        $data->assign('imageType', $type['name']);
                    } else {
                        $data->assign('imageType', ImageType::getFormatedName('small'));
                    }
                    $data->assign('imageWidth', (isset($image_type['width']) ? (int)($image_type['width']) : 64) + 25);
                    foreach ($images as $k => $image) {
                        $images[$k]['obj'] = new Image($image['id_image']);
                        ++$i;
                    }
                    $data->assign('images', $images);

                    $data->assign($this->tpl_form_vars);
                    $data->assign(array(
                        'list' => $this->renderListAttributes($product, $currency),
                        'product' => $product,
                        'id_category' => $product->getDefaultCategory(),
                        'token_generator' => Tools::getAdminTokenLite('AdminAttributeGenerator'),
                        'combination_exists' => (Shop::isFeatureActive() && (Shop::getContextShopGroup()->share_stock) && count(AttributeGroup::getAttributesGroups($this->context->language->id)) > 0 && $product->hasAttributes())
                    ));
                }
            } else {
                $this->displayWarning($this->l('You must save the product in this shop before adding combinations.'));
            }
        } else {
            $data->assign('product', $product);
            $this->displayWarning($this->l('You must save this product before adding combinations.'));
        }

        $this->tpl_form_vars['custom_form'] = $data->fetch();
    }

    /**
     * @param Product $product
     * @param Currency|array|int $currency
     * @return string
     */
    public function renderListAttributes($product, $currency)
    {
        $this->bulk_actions = array('delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')));
        $this->addRowAction('edit');
        $this->addRowAction('default');
        $this->addRowAction('delete');

        $default_class = 'highlighted';

        $this->fields_list = array(
            'attributes' => array('title' => $this->l('Attribute - value pair'), 'align' => 'left'),
            'price' => array('title' => $this->l('Impact on price'), 'type' => 'price', 'align' => 'left'),
            'weight' => array('title' => $this->l('Impact on weight'), 'align' => 'left'),
            'reference' => array('title' => $this->l('Reference'), 'align' => 'left'),
            'ean13' => array('title' => $this->l('EAN-13'), 'align' => 'left'),
            'upc' => array('title' => $this->l('UPC'), 'align' => 'left')
        );

        if ($product->id) {
            /* Build attributes combinations */
            $combinations = $product->getAttributeCombinations($this->context->language->id);
            $groups = array();
            $comb_array = array();
            if (is_array($combinations)) {
                $combination_images = $product->getCombinationImages($this->context->language->id);
                foreach ($combinations as $k => $combination) {
                    $price_to_convert = Tools::convertPrice($combination['price'], $currency);
                    $price = Tools::displayPrice($price_to_convert, $currency);

                    $comb_array[$combination['id_product_attribute']]['id_product_attribute'] = $combination['id_product_attribute'];
                    $comb_array[$combination['id_product_attribute']]['attributes'][] = array($combination['group_name'], $combination['attribute_name'], $combination['id_attribute']);
                    $comb_array[$combination['id_product_attribute']]['wholesale_price'] = $combination['wholesale_price'];
                    $comb_array[$combination['id_product_attribute']]['price'] = $price;
                    $comb_array[$combination['id_product_attribute']]['weight'] = $combination['weight'].Configuration::get('PS_WEIGHT_UNIT');
                    $comb_array[$combination['id_product_attribute']]['unit_impact'] = $combination['unit_price_impact'];
                    $comb_array[$combination['id_product_attribute']]['reference'] = $combination['reference'];
                    $comb_array[$combination['id_product_attribute']]['ean13'] = $combination['ean13'];
                    $comb_array[$combination['id_product_attribute']]['upc'] = $combination['upc'];
                    $comb_array[$combination['id_product_attribute']]['id_image'] = isset($combination_images[$combination['id_product_attribute']][0]['id_image']) ? $combination_images[$combination['id_product_attribute']][0]['id_image'] : 0;
                    $comb_array[$combination['id_product_attribute']]['available_date'] = strftime($combination['available_date']);
                    $comb_array[$combination['id_product_attribute']]['default_on'] = $combination['default_on'];
                    if ($combination['is_color_group']) {
                        $groups[$combination['id_attribute_group']] = $combination['group_name'];
                    }
                }
            }

            if (isset($comb_array)) {
                foreach ($comb_array as $id_product_attribute => $product_attribute) {
                    $list = '';

                    /* In order to keep the same attributes order */
                    asort($product_attribute['attributes']);

                    foreach ($product_attribute['attributes'] as $attribute) {
                        $list .= $attribute[0].' - '.$attribute[1].', ';
                    }

                    $list = rtrim($list, ', ');
                    $comb_array[$id_product_attribute]['image'] = $product_attribute['id_image'] ? new Image($product_attribute['id_image']) : false;
                    $comb_array[$id_product_attribute]['available_date'] = $product_attribute['available_date'] != 0 ? date('Y-m-d', strtotime($product_attribute['available_date'])) : '0000-00-00';
                    $comb_array[$id_product_attribute]['attributes'] = $list;
                    $comb_array[$id_product_attribute]['name'] = $list;

                    if ($product_attribute['default_on']) {
                        $comb_array[$id_product_attribute]['class'] = $default_class;
                    }
                }
            }
        }

        foreach ($this->actions_available as $action) {
            if (!in_array($action, $this->actions) && isset($this->$action) && $this->$action) {
                $this->actions[] = $action;
            }
        }

        $helper = new HelperList();
        $helper->identifier = 'id_product_attribute';
        $helper->table_id = 'combinations-list';
        $helper->token = $this->token;
        $helper->currentIndex = self::$currentIndex;
        $helper->no_link = true;
        $helper->simple_header = true;
        $helper->show_toolbar = false;
        $helper->shopLinkType = $this->shopLinkType;
        $helper->actions = $this->actions;
        $helper->list_skip_actions = $this->list_skip_actions;
        $helper->colorOnBackground = true;
        $helper->override_folder = $this->tpl_folder.'combination/';

        return $helper->generateList($comb_array, $this->fields_list);
    }

    /**
     * @param Product $obj
     * @throws Exception
     * @throws SmartyException
     */
    public function initFormQuantities($obj)
    {
        if (!$this->default_form_language) {
            $this->getLanguages();
        }

        $data = $this->createTemplate($this->tpl_form);
        $data->assign('default_form_language', $this->default_form_language);

        if ($obj->id) {
            if ($this->product_exists_in_shop) {
                // Get all id_product_attribute
                $attributes = $obj->getAttributesResume($this->context->language->id);
                if (empty($attributes)) {
                    $attributes[] = array(
                        'id_product_attribute' => 0,
                        'attribute_designation' => ''
                    );
                }

                // Get available quantities
                $available_quantity = array();
                $product_designation = array();

                foreach ($attributes as $attribute) {
                    // Get available quantity for the current product attribute in the current shop
                    $available_quantity[$attribute['id_product_attribute']] = isset($attribute['id_product_attribute']) && $attribute['id_product_attribute'] ? (int)$attribute['quantity'] : (int)$obj->quantity;
                    // Get all product designation
                    $product_designation[$attribute['id_product_attribute']] = rtrim(
                        $obj->name[$this->context->language->id].' - '.$attribute['attribute_designation'],
                        ' - '
                    );
                }

                $show_quantities = true;
                $shop_context = Shop::getContext();
                $shop_group = new ShopGroup((int)Shop::getContextShopGroupID());

                // if we are in all shops context, it's not possible to manage quantities at this level
                if (Shop::isFeatureActive() && $shop_context == Shop::CONTEXT_ALL) {
                    $show_quantities = false;
                }
                // if we are in group shop context
                elseif (Shop::isFeatureActive() && $shop_context == Shop::CONTEXT_GROUP) {
                    // if quantities are not shared between shops of the group, it's not possible to manage them at group level
                    if (!$shop_group->share_stock) {
                        $show_quantities = false;
                    }
                }
                // if we are in shop context
                elseif (Shop::isFeatureActive()) {
                    // if quantities are shared between shops of the group, it's not possible to manage them for a given shop
                    if ($shop_group->share_stock) {
                        $show_quantities = false;
                    }
                }

                $data->assign('ps_stock_management', Configuration::get('PS_STOCK_MANAGEMENT'));
                $data->assign('has_attribute', $obj->hasAttributes());
                // Check if product has combination, to display the available date only for the product or for each combination
                if (Combination::isFeatureActive()) {
                    $data->assign('countAttributes', (int)Db::getInstance()->getValue('SELECT COUNT(id_product) FROM '._DB_PREFIX_.'product_attribute WHERE id_product = '.(int)$obj->id));
                } else {
                    $data->assign('countAttributes', false);
                }
                // if advanced stock management is active, checks associations
                $advanced_stock_management_warning = false;
                if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && $obj->advanced_stock_management) {
                    $p_attributes = Product::getProductAttributesIds($obj->id);
                    $warehouses = array();

                    if (!$p_attributes) {
                        $warehouses[] = Warehouse::getProductWarehouseList($obj->id, 0);
                    }

                    foreach ($p_attributes as $p_attribute) {
                        $ws = Warehouse::getProductWarehouseList($obj->id, $p_attribute['id_product_attribute']);
                        if ($ws) {
                            $warehouses[] = $ws;
                        }
                    }
                    $warehouses = Tools::arrayUnique($warehouses);

                    if (empty($warehouses)) {
                        $advanced_stock_management_warning = true;
                    }
                }
                if ($advanced_stock_management_warning) {
                    $this->displayWarning($this->l('If you wish to use the advanced stock management, you must:'));
                    $this->displayWarning('- '.$this->l('associate your products with warehouses.'));
                    $this->displayWarning('- '.$this->l('associate your warehouses with carriers.'));
                    $this->displayWarning('- '.$this->l('associate your warehouses with the appropriate shops.'));
                }

                $pack_quantity = null;
                // if product is a pack
                if (Pack::isPack($obj->id)) {
                    $items = Pack::getItems((int)$obj->id, Configuration::get('PS_LANG_DEFAULT'));

                    // gets an array of quantities (quantity for the product / quantity in pack)
                    $pack_quantities = array();
                    foreach ($items as $item) {
                        /** @var Product $item */
                        if (!$item->isAvailableWhenOutOfStock((int)$item->out_of_stock)) {
                            $pack_id_product_attribute = Product::getDefaultAttribute($item->id, 1);
                            $pack_quantities[] = Product::getQuantity($item->id, $pack_id_product_attribute) / ($item->pack_quantity !== 0 ? $item->pack_quantity : 1);
                        }
                    }

                    // gets the minimum
                    if (count($pack_quantities)) {
                        $pack_quantity = $pack_quantities[0];
                        foreach ($pack_quantities as $value) {
                            if ($pack_quantity > $value) {
                                $pack_quantity = $value;
                            }
                        }
                    }

                    if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && !Warehouse::getPackWarehouses((int)$obj->id)) {
                        $this->displayWarning($this->l('You must have a common warehouse between this pack and its product.'));
                    }
                }

                $data->assign(array(
                    'attributes' => $attributes,
                    'available_quantity' => $available_quantity,
                    'pack_quantity' => $pack_quantity,
                    'stock_management_active' => Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT'),
                    'product_designation' => $product_designation,
                    'product' => $obj,
                    'show_quantities' => $show_quantities,
                    'order_out_of_stock' => Configuration::get('PS_ORDER_OUT_OF_STOCK'),
                    'pack_stock_type' => Configuration::get('PS_PACK_STOCK_TYPE'),
                    'token_preferences' => Tools::getAdminTokenLite('AdminPPreferences'),
                    'token' => $this->token,
                    'languages' => $this->_languages,
                    'id_lang' => $this->context->language->id
                ));
            } else {
                $this->displayWarning($this->l('You must save the product in this shop before managing quantities.'));
            }
        } else {
            $this->displayWarning($this->l('You must save this product before managing quantities.'));
        }

        /* OVERRIDE */

        if ($obj->id) {

            // DISPONIBILITA'

            $disponibilita = Product::showProductDisponibilita($obj->id);
            $data->assign('disponibilita', $disponibilita);
            
            // FINE DISPONIBILITA'
            
            // VENDUTO
            
            $venduto30  = Db::getInstance()->getValue('SELECT SUM(product_quantity) from '._DB_PREFIX_.'orders o join '._DB_PREFIX_.'order_detail od on o.id_order = od.id_order join '._DB_PREFIX_.'cart c on o.id_cart = c.id_cart where od.product_reference = "'.$obj->reference.'" and o.date_add BETWEEN NOW() - INTERVAL 30 DAY AND NOW()'); 
            $venduto60  = Db::getInstance()->getValue('SELECT SUM(product_quantity) from '._DB_PREFIX_.'orders o join '._DB_PREFIX_.'order_detail od on o.id_order = od.id_order join '._DB_PREFIX_.'cart c on o.id_cart = c.id_cart where od.product_reference = "'.$obj->reference.'" and o.date_add BETWEEN NOW() - INTERVAL 60 DAY AND NOW()'); 
            $venduto90  = Db::getInstance()->getValue('SELECT SUM(product_quantity) from '._DB_PREFIX_.'orders o join '._DB_PREFIX_.'order_detail od on o.id_order = od.id_order join '._DB_PREFIX_.'cart c on o.id_cart = c.id_cart where od.product_reference = "'.$obj->reference.'" and o.date_add BETWEEN NOW() - INTERVAL 90 DAY AND NOW()'); 
            $venduto120 = Db::getInstance()->getValue('SELECT SUM(product_quantity) from '._DB_PREFIX_.'orders o join '._DB_PREFIX_.'order_detail od on o.id_order = od.id_order join '._DB_PREFIX_.'cart c on o.id_cart = c.id_cart where od.product_reference = "'.$obj->reference.'" and o.date_add BETWEEN NOW() - INTERVAL 120 DAY AND NOW()'); 
            $venduto150 = Db::getInstance()->getValue('SELECT SUM(product_quantity) from '._DB_PREFIX_.'orders o join '._DB_PREFIX_.'order_detail od on o.id_order = od.id_order join '._DB_PREFIX_.'cart c on o.id_cart = c.id_cart where od.product_reference = "'.$obj->reference.'" and o.date_add BETWEEN NOW() - INTERVAL 150 DAY AND NOW()'); 
            $venduto180 = Db::getInstance()->getValue('SELECT SUM(product_quantity) from '._DB_PREFIX_.'orders o join '._DB_PREFIX_.'order_detail od on o.id_order = od.id_order join '._DB_PREFIX_.'cart c on o.id_cart = c.id_cart where od.product_reference = "'.$obj->reference.'" and o.date_add BETWEEN NOW() - INTERVAL 180 DAY AND NOW()'); 

            $venduto30 = ($venduto30 > 0 ? $venduto30 : 0);
            $venduto60 = ($venduto60 > 0 ? $venduto60 : 0);
            $venduto90 = ($venduto90 > 0 ? $venduto90 : 0);
            $venduto120 = ($venduto120 > 0 ? $venduto120 : 0);
            $venduto150 = ($venduto150 > 0 ? $venduto150 : 0);
            $venduto180 = ($venduto180 > 0 ? $venduto180 : 0);

            $data->assign(array(
                'venduto30' => $venduto30,
                'venduto60' => $venduto60,
                'venduto90' => $venduto90,
                'venduto120' => $venduto120,
                'venduto150' => $venduto150,
                'venduto180' => $venduto180
            ));

            // FINE VENDUTO

            // Messaggio disponibilità
            $date_available_msg = htmlentities($obj->date_available_msg, ENT_COMPAT, 'UTF-8');
            $data->assign('date_available_msg', $date_available_msg);

            // Qta al 08/01/2020
            $esolver_qt = $obj->qt_esolver;
            // ?
            $supplier_qt = $obj->supplier_quantity;
            // Mod. qta mag.
            $stock_qt = $obj->stock_quantity;

            $data->assign('esolver_qt', $esolver_qt);
            $data->assign('supplier_qt', $supplier_qt);
            $data->assign('stock_qt', $stock_qt);
        }

        /* FINE OVERRIDE */

        $this->tpl_form_vars['custom_form'] = $data->fetch();
    }

    /**
     * @param Product $obj
     * @throws Exception
     * @throws SmartyException
     */
    public function initFormSuppliers($obj)
    {
        $data = $this->createTemplate($this->tpl_form);

        if ($obj->id) {
            if ($this->product_exists_in_shop) {
                // Get all id_product_attribute
                $attributes = $obj->getAttributesResume($this->context->language->id);
                if (empty($attributes)) {
                    $attributes[] = array(
                        'id_product' => $obj->id,
                        'id_product_attribute' => 0,
                        'attribute_designation' => ''
                    );
                }

                $product_designation = array();

                foreach ($attributes as $attribute) {
                    $product_designation[$attribute['id_product_attribute']] = rtrim(
                        $obj->name[$this->context->language->id].' - '.$attribute['attribute_designation'],
                        ' - '
                    );
                }

                // Get all available suppliers
                $suppliers = Supplier::getSuppliers();

                // Get already associated suppliers
                $associated_suppliers = ProductSupplier::getSupplierCollection($obj->id);

                // Get already associated suppliers and force to retreive product declinaisons
                $product_supplier_collection = ProductSupplier::getSupplierCollection($obj->id, false);

                $default_supplier = 0;

                foreach ($suppliers as &$supplier) {
                    $supplier['is_selected'] = false;
                    $supplier['is_default'] = false;

                    foreach ($associated_suppliers as $associated_supplier) {
                        /** @var ProductSupplier $associated_supplier */
                        if ($associated_supplier->id_supplier == $supplier['id_supplier']) {
                            $associated_supplier->name = $supplier['name'];
                            $supplier['is_selected'] = true;

                            if ($obj->id_supplier == $supplier['id_supplier']) {
                                $supplier['is_default'] = true;
                                $default_supplier = $supplier['id_supplier'];
                            }
                        }
                    }
                }

                $data->assign(array(
                    'attributes' => $attributes,
                    'suppliers' => $suppliers,
                    'default_supplier' => $default_supplier,
                    'associated_suppliers' => $associated_suppliers,
                    'associated_suppliers_collection' => $product_supplier_collection,
                    'product_designation' => $product_designation,
                    'currencies' => Currency::getCurrencies(),
                    'product' => $obj,
                    'link' => $this->context->link,
                    'token' => $this->token,
                    'id_default_currency' => Configuration::get('PS_CURRENCY_DEFAULT'),
                ));
            } else {
                $this->displayWarning($this->l('You must save the product in this shop before managing suppliers.'));
            }
        } else {
            $this->displayWarning($this->l('You must save this product before managing suppliers.'));
        }

        $this->tpl_form_vars['custom_form'] = $data->fetch();
    }

    /**
     * @param Product $obj
     * @throws Exception
     * @throws SmartyException
     */
    public function initFormWarehouses($obj)
    {
        $data = $this->createTemplate($this->tpl_form);

        if ($obj->id) {
            if ($this->product_exists_in_shop) {
                // Get all id_product_attribute
                $attributes = $obj->getAttributesResume($this->context->language->id);
                if (empty($attributes)) {
                    $attributes[] = array(
                        'id_product' => $obj->id,
                        'id_product_attribute' => 0,
                        'attribute_designation' => ''
                    );
                }

                $product_designation = array();

                foreach ($attributes as $attribute) {
                    $product_designation[$attribute['id_product_attribute']] = rtrim(
                        $obj->name[$this->context->language->id].' - '.$attribute['attribute_designation'],
                        ' - '
                    );
                }

                // Get all available warehouses
                $warehouses = Warehouse::getWarehouses(true);

                // Get already associated warehouses
                $associated_warehouses_collection = WarehouseProductLocation::getCollection($obj->id);

                $data->assign(array(
                    'attributes' => $attributes,
                    'warehouses' => $warehouses,
                    'associated_warehouses' => $associated_warehouses_collection,
                    'product_designation' => $product_designation,
                    'product' => $obj,
                    'link' => $this->context->link,
                    'token' => $this->token
                ));
            } else {
                $this->displayWarning($this->l('You must save the product in this shop before managing warehouses.'));
            }
        } else {
            $this->displayWarning($this->l('You must save this product before managing warehouses.'));
        }

        $this->tpl_form_vars['custom_form'] = $data->fetch();
    }

    /**
     * @param Product $obj
     * @throws Exception
     * @throws SmartyException
     */
    public function initFormFeatures($obj)
    {
        if (!$this->default_form_language) {
            $this->getLanguages();
        }

        $data = $this->createTemplate($this->tpl_form);
        $data->assign('default_form_language', $this->default_form_language);
        $data->assign('languages', $this->_languages);

        if (!Feature::isFeatureActive()) {
            $this->displayWarning($this->l('This feature has been disabled. ').' <a href="index.php?tab=AdminPerformance&token='.Tools::getAdminTokenLite('AdminPerformance').'#featuresDetachables">'.$this->l('Performances').'</a>');
        } else {
            if ($obj->id) {
                if ($this->product_exists_in_shop) {
                    $features = Feature::getFeatures($this->context->language->id, (Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP));

                    foreach ($features as $k => $tab_features) {
                        $features[$k]['current_item'] = false;
                        $features[$k]['val'] = array();

                        $custom = true;
                        foreach ($obj->getFeatures() as $tab_products) {
                            if ($tab_products['id_feature'] == $tab_features['id_feature']) {
                                $features[$k]['current_item'] = $tab_products['id_feature_value'];
                            }
                        }

                        $features[$k]['featureValues'] = FeatureValue::getFeatureValuesWithLang($this->context->language->id, (int)$tab_features['id_feature']);
                        if (count($features[$k]['featureValues'])) {
                            foreach ($features[$k]['featureValues'] as $value) {
                                if ($features[$k]['current_item'] == $value['id_feature_value']) {
                                    $custom = false;
                                }
                            }
                        }

                        if ($custom) {
                            $feature_values_lang = FeatureValue::getFeatureValueLang($features[$k]['current_item']);
                            foreach ($feature_values_lang as $feature_value) {
                                $features[$k]['val'][$feature_value['id_lang']] = $feature_value;
                            }
                        }
                    }

                    $data->assign('available_features', $features);
                    $data->assign('product', $obj);
                    $data->assign('link', $this->context->link);
                    $data->assign('default_form_language', $this->default_form_language);
                } else {
                    $this->displayWarning($this->l('You must save the product in this shop before adding features.'));
                }
            } else {
                $this->displayWarning($this->l('You must save this product before adding features.'));
            }
        }
        $this->tpl_form_vars['custom_form'] = $data->fetch();

        /* OVERRIDE */

        $this->displayWarning('Manca gruppo; meglio comprare un modulo');

        // Aggiungere gruppo: $feature['group_name']

        // Inserire nel posto giusto le regole (vedi 1.4):
        /*if($tab_features['id_feature'] == '924' && ($current_item == '12847' || $current_item == '12894'))
        {
            echo '<script type="text/javascript">
            $(document).ready(function(){
                $(\'#features_925\').hide(); $(\'#features_926\').hide(); $(\'#features_927\').hide(); $(\'#features_928\').hide(); $(\'#features_486\').hide(); $(\'#features_930\').hide(); $(\'#features_931\').hide(); $(\'#features_932\').hide(); $(\'#features_933\').hide(); $(\'#features_934\').hide(); $(\'#features_935\').hide(); $(\'#features_936\').hide(); $(\'#features_937\').hide(); $(\'#features_938\').show(); $(\'#features_939\').show(); $(\'#features_716\').show(); $(\'#features_940\').show(); $(\'#features_941\').show(); $(\'#features_942\').show(); $(\'#features_483\').show(); $(\'#features_484\').show(); $(\'#features_943\').show(); $(\'#features_944\').show(); $(\'#features_945\').show(); $(\'#features_946\').show();
            });
            </script>';
        }
        else if($tab_features['id_feature'] == '924' && ($current_item == '12848'))
        {
            echo '<script type="text/javascript">
            $(document).ready(function(){
                $(\'#features_925\').show(); $(\'#features_926\').show(); $(\'#features_927\').show(); $(\'#features_928\').show(); $(\'#features_486\').show(); $(\'#features_930\').show(); $(\'#features_931\').show(); $(\'#features_932\').show(); $(\'#features_933\').show(); $(\'#features_934\').show(); $(\'#features_935\').show(); $(\'#features_936\').show(); $(\'#features_937\').show(); $(\'#features_938\').hide(); $(\'#features_939\').hide(); $(\'#features_716\').hide(); $(\'#features_940\').hide(); $(\'#features_941\').hide(); $(\'#features_942\').hide(); $(\'#features_483\').hide(); $(\'#features_484\').hide(); $(\'#features_943\').hide(); $(\'#features_944\').hide(); $(\'#features_945\').hide(); $(\'#features_946\').hide();
            });
            </script>';
        }*/
        // Vedi onchange="$(\'.custom_'.$tab_features['id_feature'].'_\').val(\'\'); ...
        /* if($tab_features['id_feature'] == 917) ... */
        /* $df_feature */
        /* tasto "Clicca qui per aggiungere un valore al menu a tendina" */
    }

    public function ajaxProcessProductQuantity()
    {
        if ($this->tabAccess['edit'] === '0') {
            return die(Tools::jsonEncode(array('error' => $this->l('You do not have the right permission'))));
        }
        if (!Tools::getValue('actionQty')) {
            return Tools::jsonEncode(array('error' => $this->l('Undefined action')));
        }

        $product = new Product((int)Tools::getValue('id_product'), true);
        switch (Tools::getValue('actionQty')) {
            case 'depends_on_stock':
                if (Tools::getValue('value') === false) {
                    die(Tools::jsonEncode(array('error' =>  $this->l('Undefined value'))));
                }
                if ((int)Tools::getValue('value') != 0 && (int)Tools::getValue('value') != 1) {
                    die(Tools::jsonEncode(array('error' =>  $this->l('Incorrect value'))));
                }
                if (!$product->advanced_stock_management && (int)Tools::getValue('value') == 1) {
                    die(Tools::jsonEncode(array('error' =>  $this->l('Not possible if advanced stock management is disabled. '))));
                }
                if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && (int)Tools::getValue('value') == 1 && (Pack::isPack($product->id) && !Pack::allUsesAdvancedStockManagement($product->id)
                    && ($product->pack_stock_type == 2 || $product->pack_stock_type == 1 ||
                        ($product->pack_stock_type == 3 && (Configuration::get('PS_PACK_STOCK_TYPE') == 1 || Configuration::get('PS_PACK_STOCK_TYPE') == 2))))) {
                    die(Tools::jsonEncode(array('error' => $this->l('You cannot use advanced stock management for this pack because').'<br />'.
                        $this->l('- advanced stock management is not enabled for these products').'<br />'.
                        $this->l('- you have chosen to decrement products quantities.'))));
                }

                StockAvailable::setProductDependsOnStock($product->id, (int)Tools::getValue('value'));
                break;

            case 'pack_stock_type':
                $value = Tools::getValue('value');
                if ($value === false) {
                    die(Tools::jsonEncode(array('error' =>  $this->l('Undefined value'))));
                }
                if ((int)$value != 0 && (int)$value != 1
                    && (int)$value != 2 && (int)$value != 3) {
                    die(Tools::jsonEncode(array('error' =>  $this->l('Incorrect value'))));
                }
                if ($product->depends_on_stock && !Pack::allUsesAdvancedStockManagement($product->id) && ((int)$value == 1
                    || (int)$value == 2 || ((int)$value == 3 && (Configuration::get('PS_PACK_STOCK_TYPE') == 1 || Configuration::get('PS_PACK_STOCK_TYPE') == 2)))) {
                    die(Tools::jsonEncode(array('error' => $this->l('You cannot use this stock management option because:').'<br />'.
                        $this->l('- advanced stock management is not enabled for these products').'<br />'.
                        $this->l('- advanced stock management is enabled for the pack'))));
                }

                Product::setPackStockType($product->id, $value);
                break;

            case 'out_of_stock':
                if (Tools::getValue('value') === false) {
                    die(Tools::jsonEncode(array('error' =>  $this->l('Undefined value'))));
                }
                if (!in_array((int)Tools::getValue('value'), array(0, 1, 2))) {
                    die(Tools::jsonEncode(array('error' =>  $this->l('Incorrect value'))));
                }

                StockAvailable::setProductOutOfStock($product->id, (int)Tools::getValue('value'));
                break;

            case 'set_qty':
                if (Tools::getValue('value') === false || (!is_numeric(trim(Tools::getValue('value'))))) {
                    die(Tools::jsonEncode(array('error' =>  $this->l('Undefined value'))));
                }
                if (Tools::getValue('id_product_attribute') === false) {
                    die(Tools::jsonEncode(array('error' =>  $this->l('Undefined id product attribute'))));
                }

                StockAvailable::setQuantity($product->id, (int)Tools::getValue('id_product_attribute'), (int)Tools::getValue('value'));
                Hook::exec('actionProductUpdate', array('id_product' => (int)$product->id, 'product' => $product));

                // Catch potential echo from modules
                $error = ob_get_contents();
                if (!empty($error)) {
                    ob_end_clean();
                    die(Tools::jsonEncode(array('error' => $error)));
                }
                break;
            case 'advanced_stock_management' :
                if (Tools::getValue('value') === false) {
                    die(Tools::jsonEncode(array('error' =>  $this->l('Undefined value'))));
                }
                if ((int)Tools::getValue('value') != 1 && (int)Tools::getValue('value') != 0) {
                    die(Tools::jsonEncode(array('error' =>  $this->l('Incorrect value'))));
                }
                if (!Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && (int)Tools::getValue('value') == 1) {
                    die(Tools::jsonEncode(array('error' =>  $this->l('Not possible if advanced stock management is disabled. '))));
                }

                $product->setAdvancedStockManagement((int)Tools::getValue('value'));
                if (StockAvailable::dependsOnStock($product->id) == 1 && (int)Tools::getValue('value') == 0) {
                    StockAvailable::setProductDependsOnStock($product->id, 0);
                }
                break;

        }
        die(Tools::jsonEncode(array('error' => false)));
    }

    public function getCombinationImagesJS()
    {
        /** @var Product $obj */
        if (!($obj = $this->loadObject(true))) {
            return;
        }

        $content = 'var combination_images = new Array();';
        if (!$allCombinationImages = $obj->getCombinationImages($this->context->language->id)) {
            return $content;
        }
        foreach ($allCombinationImages as $id_product_attribute => $combination_images) {
            $i = 0;
            $content .= 'combination_images['.(int)$id_product_attribute.'] = new Array();';
            foreach ($combination_images as $combination_image) {
                $content .= 'combination_images['.(int)$id_product_attribute.']['.$i++.'] = '.(int)$combination_image['id_image'].';';
            }
        }
        return $content;
    }

    public function haveThisAccessory($accessory_id, $accessories)
    {
        foreach ($accessories as $accessory) {
            if ((int)$accessory['id_product'] == (int)$accessory_id) {
                return true;
            }
        }
        return false;
    }
    
    // Override
    public function haveThisAlternative($alternative_id, $alternatives)
    {
        foreach ($alternatives as $alternative) {
            if ((int)$alternative['id_product'] == (int)$alternative_id) {
                return true;
            }
        }
        return false;
    }

    protected function initPack(Product $product)
    {
        $this->tpl_form_vars['is_pack'] = ($product->id && Pack::isPack($product->id)) || Tools::getValue('type_product') == Product::PTYPE_PACK;
        $product->packItems = Pack::getItems($product->id, $this->context->language->id);

        $input_pack_items = '';
        if (Tools::getValue('inputPackItems')) {
            $input_pack_items = Tools::getValue('inputPackItems');
        } else {
            foreach ($product->packItems as $pack_item) {
                $input_pack_items .= $pack_item->pack_quantity.'x'.$pack_item->id.'-';
            }
        }
        $this->tpl_form_vars['input_pack_items'] = $input_pack_items;

        $input_namepack_items = '';
        if (Tools::getValue('namePackItems')) {
            $input_namepack_items = Tools::getValue('namePackItems');
        } else {
            foreach ($product->packItems as $pack_item) {
                $input_namepack_items .= $pack_item->pack_quantity.' x '.$pack_item->name.'¤';
            }
        }
        $this->tpl_form_vars['input_namepack_items'] = $input_namepack_items;
    }

    /**
     * AdminProducts display hook
     *
     * @param $obj
     *
     * @throws PrestaShopException
     */
    public function initFormModules($obj)
    {
        $id_module = Db::getInstance()->getValue('SELECT `id_module` FROM `'._DB_PREFIX_.'module` WHERE `name` = \''.pSQL($this->tab_display_module).'\'');
        $this->tpl_form_vars['custom_form'] = Hook::exec('displayAdminProductsExtra', array(), (int)$id_module);
    }

    /**
     * delete all items in pack, then check if type_product value is 2.
     * if yes, add the pack items from input "inputPackItems"
     *
     * @param Product $product
     * @return bool
     */
    public function updatePackItems($product)
    {
        Pack::deleteItems($product->id);
        // lines format: QTY x ID-QTY x ID
        if (Tools::getValue('type_product') == Product::PTYPE_PACK) {
            $product->setDefaultAttribute(0);//reset cache_default_attribute
            $items = Tools::getValue('inputPackItems');
            $lines = array_unique(explode('-', $items));

            // lines is an array of string with format : QTYxIDxID_PRODUCT_ATTRIBUTE
            if (count($lines)) {
                foreach ($lines as $line) {
                    if (!empty($line)) {
                        $item_id_attribute = 0;
                        count($array = explode('x', $line)) == 3 ? list($qty, $item_id, $item_id_attribute) = $array : list($qty, $item_id) = $array;
                        if ($qty > 0 && isset($item_id)) {
                            if (Pack::isPack((int)$item_id || $product->id == (int)$item_id)) {
                                $this->errors[] = Tools::displayError('You can\'t add product packs into a pack');
                            } elseif (!Pack::addItem((int)$product->id, (int)$item_id, (int)$qty, (int)$item_id_attribute)) {
                                $this->errors[] = Tools::displayError('An error occurred while attempting to add products to the pack.');
                            }
                        }
                    }
                }
            }
        }
    }

    public function getL($key)
    {
        $trad = array(
            'Default category:' => $this->l('Default category'),
            'Catalog:' => $this->l('Catalog'),
            'Consider changing the default category.' => $this->l('Consider changing the default category.'),
            'ID' => $this->l('ID'),
            'Name' => $this->l('Name'),
            'Mark all checkbox(es) of categories in which product is to appear' => $this->l('Mark the checkbox of each categories in which this product will appear.')
        );
        return $trad[$key];
    }

    protected function _displayUnavailableProductWarning()
    {
        $content = '<div class="alert">
			<span>'.$this->l('Your product will be saved as a draft.').'</span>
				<a href="#" class="btn btn-default pull-right" onclick="submitAddProductAndPreview()" ><i class="icon-external-link-sign"></i> '.$this->l('Save and preview').'</a>
				<input type="hidden" name="fakeSubmitAddProductAndPreview" id="fakeSubmitAddProductAndPreview" />
			</div>';
        $this->tpl_form_vars['warning_unavailable_product'] = $content;
    }

    public function ajaxProcessCheckProductName()
    {
        if ($this->tabAccess['view'] === '1') {
            $search = Tools::getValue('q');
            $id_lang = Tools::getValue('id_lang');
            $limit = Tools::getValue('limit');
            if (Context::getContext()->shop->getContext() != Shop::CONTEXT_SHOP) {
                $result = false;
            } else {
                $result = Db::getInstance()->executeS('
					SELECT DISTINCT pl.`name`, p.`id_product`, pl.`id_shop`
					FROM `'._DB_PREFIX_.'product` p
					LEFT JOIN `'._DB_PREFIX_.'product_shop` ps ON (ps.id_product = p.id_product AND ps.id_shop ='.(int)Context::getContext()->shop->id.')
					LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
						ON (pl.`id_product` = p.`id_product` AND pl.`id_lang` = '.(int)$id_lang.')
					WHERE pl.`name` LIKE "%'.pSQL($search).'%" AND ps.id_product IS NULL
					GROUP BY pl.`id_product`
					LIMIT '.(int)$limit);
            }
            die(Tools::jsonEncode($result));
        }
    }

    public function ajaxProcessUpdatePositions()
    {
        if ($this->tabAccess['edit'] === '1') {
            $way = (int)(Tools::getValue('way'));
            $id_product = (int)Tools::getValue('id_product');
            $id_category = (int)Tools::getValue('id_category');
            $positions = Tools::getValue('product');
            $page = (int)Tools::getValue('page');
            $selected_pagination = (int)Tools::getValue('selected_pagination');

            if (is_array($positions)) {
                foreach ($positions as $position => $value) {
                    $pos = explode('_', $value);

                    if ((isset($pos[1]) && isset($pos[2])) && ($pos[1] == $id_category && (int)$pos[2] === $id_product)) {
                        if ($page > 1) {
                            $position = $position + (($page - 1) * $selected_pagination);
                        }

                        if ($product = new Product((int)$pos[2])) {
                            if (isset($position) && $product->updatePosition($way, $position)) {
                                $category = new Category((int)$id_category);
                                if (Validate::isLoadedObject($category)) {
                                    hook::Exec('categoryUpdate', array('category' => $category));
                                }
                                echo 'ok position '.(int)$position.' for product '.(int)$pos[2]."\r\n";
                            } else {
                                echo '{"hasError" : true, "errors" : "Can not update product '.(int)$id_product.' to position '.(int)$position.' "}';
                            }
                        } else {
                            echo '{"hasError" : true, "errors" : "This product ('.(int)$id_product.') can t be loaded"}';
                        }

                        break;
                    }
                }
            }
        }
    }

    public function ajaxProcessPublishProduct()
    {
        if ($this->tabAccess['edit'] === '1') {
            if ($id_product = (int)Tools::getValue('id_product')) {
                $bo_product_url = dirname($_SERVER['PHP_SELF']).'/index.php?tab=AdminProducts&id_product='.$id_product.'&updateproduct&token='.$this->token;

                if (Tools::getValue('redirect')) {
                    die($bo_product_url);
                }

                $product = new Product((int)$id_product);
                if (!Validate::isLoadedObject($product)) {
                    die('error: invalid id');
                }

                $product->active = 1;

                if ($product->save()) {
                    die($bo_product_url);
                } else {
                    die('error: saving');
                }
            }
        }
    }

    public function processImageLegends()
    {
        if (Tools::getValue('key_tab') == 'Images' && Tools::getValue('submitAddproductAndStay') == 'update_legends' && Validate::isLoadedObject($product = new Product((int)Tools::getValue('id_product')))) {
            $id_image = (int)Tools::getValue('id_caption');
            $language_ids = Language::getIDs(false);
            foreach ($_POST as $key => $val) {
                if (preg_match('/^legend_([0-9]+)/i', $key, $match)) {
                    foreach ($language_ids as $id_lang) {
                        if ($val && $id_lang == $match[1]) {
                            Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'image_lang SET legend = "'.pSQL($val).'" WHERE '.($id_image ? 'id_image = '.(int)$id_image : 'EXISTS (SELECT 1 FROM '._DB_PREFIX_.'image WHERE '._DB_PREFIX_.'image.id_image = '._DB_PREFIX_.'image_lang.id_image AND id_product = '.(int)$product->id.')').' AND id_lang = '.(int)$id_lang);
                        }
                    }
                }
            }
        }
    }

    public function displayPreviewLink($token = null, $id, $name = null)
    {
        $tpl = $this->createTemplate('helpers/list/list_action_preview.tpl');
        if (!array_key_exists('Bad SQL query', self::$cache_lang)) {
            self::$cache_lang['Preview'] = $this->l('Preview', 'Helper');
        }

        $tpl->assign(array(
            'href' => $this->getPreviewUrl(new Product((int)$id)),
            'action' => self::$cache_lang['Preview'],
        ));

        return $tpl->fetch();
    }

    /* OVERRIDE */

    // Kit; CORREGGERE (o eliminare se troviamo un modulo per i bundle)
    public function initFormBundles($obj)
    {
        $data = $this->createTemplate($this->tpl_form);
        $this->tpl_form_vars['custom_form'] = $data->fetch();

        echo '
		<div class="tab-page" id="step3">
				<h4 class="tab">4. Kit</h4>';

		echo '<b>'.$this->l('Bundles for this product:').'.</b>';
			
		echo '</div>';
    }

    public function initFormEsolver($obj)
    {
        $data = $this->createTemplate($this->tpl_form);
        
        $context = Context::getContext();

        // Uso executeS al posto di getRow per comodità nella modifica dei valori dell'array
        $p_row = Db::getInstance()->executeS("
            SELECT id_category_default, tipo_anagrafica, cl.name as categoria, s.name as fornitore, m.name as costruttore, modello, reference, p.id_product, supplier_reference, cod_fornitore_1, ean13, ean_descrizione, ean_quantita, ean_tipo, intrastat, name_short, pl.name, fuori_produzione, p.condition, p.date_add, p.date_upd,'' as manufacturer_code, p.id_manufacturer,'' as category_code, id_category_gst, weight, p.id_supplier, note_fornitore,
                '' as codice_fornitore, altro_esolver, note, tempo_approvvigionamento, codice_barre_fornitore,note, note_2,stock_quantity,  amazon_quantity, ordinato_quantity, impegnato_quantity, '' as ordinato_a_fornitore, supplier_quantity, esprinet_quantity, attiva_quantity, itancia_quantity, intracom_quantity, asit_quantity, ingram_quantity, arrivo_quantity, arrivo_esprinet_quantity, arrivo_attiva_quantity, '' as arrivo_itancia_quantity, arrivo_intracom_quantity, arrivo_ingram_quantity, data_arrivo, data_arrivo_esprinet, '' as data_arrivo_itancia, data_arrivo_intracom, data_arrivo_ingram, 
                listino, p.sconto_acquisto_1, p.sconto_acquisto_2, p.sconto_acquisto_3, p.wholesale_price, acquisto_in_dollari, scorta_minima, scorta_minima_amazon, blocco_prezzi, login_for_offer, scontolistinovendita, price, '' as trasporto_gratuito, date_available, '' as data_arrivo_fornitore, 
                '' as sconto_rivenditore_1, '' as sconto_rivenditore_2, rebate_1,'Rebate 1' as descrizione_rebate_1, rebate_2,'Rebate 2' as descrizione_rebate_2, rebate_3,
                'Rebate 3' as descrizione_rebate_3, premio_target,'Premio' as descrizione_premio,pezzi_in_confezione, nr, pz, '' as contenuto_confezione, '' as garanzia, asin, commissione_amazon, commissione_eprice 
            FROM "._DB_PREFIX_."product p
                LEFT JOIN campi_esolver ce on p.id_product = ce.id_product
                LEFT JOIN "._DB_PREFIX_."product_lang pl on p.id_product = pl.id_product
                LEFT JOIN product_esolver pe on p.id_product = pe.id_product
                LEFT JOIN "._DB_PREFIX_."category_lang cl on p.id_category_gst = cl.id_category
                LEFT JOIN "._DB_PREFIX_."manufacturer m on p.id_manufacturer = m.id_manufacturer
                LEFT JOIN "._DB_PREFIX_."supplier s on p.id_supplier = s.id_supplier
            WHERE pl.id_lang = ".$context->language->id."
                AND cl.id_lang = ".$context->language->id."
                AND p.id_product = ".$obj->id
        );

        // Modifico i valori dell'array
        foreach($p_row as &$product)
        {
            if($product['id_manufacturer'] == 150 && ($product['id_category_default'] == 119 || $product['id_category_default'] == 248 || $product['id_category_default'] == 249))
            {	
                $product['tipo_anagrafica'] = 4;
                $product['modello'] = 2;
            }
            else
            {	
                $product['tipo_anagrafica'] = 1;
                $product['modello'] = 1;
            }	
            
            $virtual = ProductDownload::getIdFromIdProduct($product['id_product']);
            if($virtual)
                $product['downloadable'] = 'S&igrave;';
            else
                $product['downloadable'] = 'No';
            
            $product['manufacturer_code'] = substr(strtoupper($product['costruttore']),0,3).sprintf("%'.03d", $product['id_manufacturer']);
            $product['category_code'] = substr(strtoupper($product['categoria']),0,3).sprintf("%'.03d", $product['id_category_gst']);
            $product['codice_fornitore'] = substr(strtoupper($product['fornitore']),0,2).sprintf("%'.03d", $product['id_supplier']);
            
            $product['tipo'] = Db::getInstance()->getValue('SELECT value FROM '._DB_PREFIX_.'feature_value_lang fvl JOIN '._DB_PREFIX_.'feature_product fp ON fvl.id_feature_value = fp.id_feature_value JOIN '._DB_PREFIX_.'feature_lang fl ON fl.id_feature = fp.id_feature WHERE fvl.id_lang = '.$context->language->id.' AND fl.name LIKE "Tipo" AND fp.id_product = '.$obj->id.' GROUP BY fvl.id_feature_value');
            // ATTENZIONE! Correggere le query che dipendono da id_feature se cambia
            $product['serie'] =      Db::getInstance()->getValue('SELECT value FROM '._DB_PREFIX_.'feature_value_lang fvl JOIN '._DB_PREFIX_.'feature_product fp ON fvl.id_feature_value = fp.id_feature_value WHERE fvl.id_lang = '.$context->language->id.' AND fp.id_feature = 917 AND fp.id_product = '.$obj->id.' GROUP BY fvl.id_feature_value');
            $product['confezione'] = Db::getInstance()->getValue('SELECT value FROM '._DB_PREFIX_.'feature_value_lang fvl JOIN '._DB_PREFIX_.'feature_product fp ON fvl.id_feature_value = fp.id_feature_value WHERE fvl.id_lang = '.$context->language->id.' AND fp.id_feature = 450 AND fp.id_product = '.$obj->id.' GROUP BY fvl.id_feature_value');
            $product['garanzia'] =   Db::getInstance()->getValue('SELECT value FROM '._DB_PREFIX_.'feature_value_lang fvl JOIN '._DB_PREFIX_.'feature_product fp ON fvl.id_feature_value = fp.id_feature_value WHERE fvl.id_lang = '.$context->language->id.' AND fp.id_feature = 449 AND fp.id_product = '.$obj->id.' GROUP BY fvl.id_feature_value');
            
            $product['ordinato'] = Db::getInstance()->getValue('SELECT sum(product_quantity - (REPLACE(cons,"0_",""))) FROM '._DB_PREFIX_.'order_detail od JOIN '._DB_PREFIX_.'orders o ON o.id_order = od.id_order JOIN '._DB_PREFIX_.'order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM '._DB_PREFIX_.'order_history moh WHERE moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN '._DB_PREFIX_.'order_state os ON (os.id_order_state = oh.id_order_state) WHERE od.product_id = '.$obj->id.' AND os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state != 20 AND os.id_order_state != 35 AND os.id_order_state != 26 GROUP BY od.product_id');
                                        
            if(!$product['ordinato'])
                $product['ordinato'] = 0;
            
            // CORREGGERE: aggiungere "FREE_SHIPPING_INCLUDE_PRD" in ps_configuration (nella 1.4 aveva value 240)
            $row = Configuration::get('FREE_SHIPPING_INCLUDE_PRD');
            
            $trasp = unserialize($row['value']);

            $au = $obj->id;
            
            foreach($trasp as $el) {
                if ($au == $el) {
                    $uu = 1;
                    break;
                }
                else {
                    $uu = 0;
                }
            }

            if($au == "") {
                $uu = 0; // inutilizzato?
            }
            
            $product['sconto_rivenditore_1'] = Db::getInstance()->getValue('SELECT reduction FROM '._DB_PREFIX_.'specific_price WHERE specific_price_name = "sc_riv_1" AND id_product = '.$obj->id);
            $product['sconto_rivenditore_2'] = Db::getInstance()->getValue('SELECT reduction FROM '._DB_PREFIX_.'specific_price WHERE specific_price_name = "sc_riv_2" AND id_product = '.$obj->id);
            
            $product['sconto_rivenditore_1'] *= 100;
            $product['sconto_rivenditore_2'] *= 100;

            $product['name'] = substr($product['name'], 0, 50); // Descr. EZ (50 car.)
            $product['date_add'] = Tools::displayDate($product['date_add'], $context->language->id);
            $product['date_upd'] = Tools::displayDate($product['date_upd'], $context->language->id);

            $product['data_arrivo'] = (($product['data_arrivo'] != '1946-01-01') ? (!$product['data_arrivo'] || $product['data_arrivo'] == '0000-00-00' || $product['data_arrivo'] == '1970-01-01' ? 'ND' : date("d/m/Y",strtotime($product['data_arrivo']))) : date('d/m/Y'));
            $product['data_arrivo_esprinet'] = (($product['data_arrivo_esprinet'] != '1946-01-01') ? (!$product['data_arrivo_esprinet'] || $product['data_arrivo_esprinet'] == '0000-00-00' || $product['data_arrivo_esprinet'] == '1970-01-01' ? 'ND' : date("d/m/Y",strtotime($product['data_arrivo_esprinet']))) : date('d/m/Y'));
            // Non sono campi del db!
            $product['data_arrivo_attiva'] = (($product['data_arrivo_attiva'] != '1946-01-01') ? (!$product['data_arrivo_attiva'] || $product['data_arrivo_attiva'] == '0000-00-00' || $product['data_arrivo_attiva'] == '1970-01-01' ? 'ND' : date("d/m/Y",strtotime($product['data_arrivo_attiva']))) : date('d/m/Y'));
            $product['data_arrivo_itancia'] = (($product['data_arrivo_itancia'] != '1946-01-01') ? (!$product['data_arrivo_itancia'] || $product['data_arrivo_itancia'] == '0000-00-00' || $product['data_arrivo_itancia'] == '1970-01-01' ? 'ND' : date("d/m/Y",strtotime($product['data_arrivo_itancia']))) : date('d/m/Y'));
            
            $product['data_arrivo_ingram'] = (($product['data_arrivo_ingram'] != '1946-01-01') ? (!$product['data_arrivo_ingram'] || $product['data_arrivo_ingram'] == '0000-00-00' || $product['data_arrivo_ingram'] == '1970-01-01' ? 'ND' : date("d/m/Y",strtotime($product['data_arrivo_ingram']))) : date('d/m/Y'));
            $product['data_arrivo_intracom'] = (($product['data_arrivo_intracom'] != '1946-01-01') ? (!$product['data_arrivo_intracom'] || $product['data_arrivo_intracom'] == '0000-00-00' || $product['data_arrivo_intracom'] == '1970-01-01' ? 'ND' : date("d/m/Y",strtotime($product['data_arrivo_intracom']))) : date('d/m/Y'));
            $product['date_available'] = (($product['date_available'] != '1946-01-01') ? (!$product['date_available'] || $product['date_available'] == '0000-00-00' || $product['date_available'] == '1970-01-01' ? 'ND' : date("d/m/Y",strtotime($product['date_available']))) : date('d/m/Y'));
            
            // Non è un campo del db!
            if($product['arrivo_itancia_quantity'] == '')
                $product['arrivo_itancia_quantity'] = 0;
            
            $product['listino'] = number_format($product['listino'], 2, ",", "");
            $product['sconto_acquisto_1'] = number_format($product['sconto_acquisto_1'], 2, ",", "");
            $product['sconto_acquisto_2'] = number_format($product['sconto_acquisto_2'], 2, ",", "");
            $product['sconto_acquisto_3'] = number_format($product['sconto_acquisto_3'], 2, ",", "");
            $product['wholesale_price'] = number_format($product['wholesale_price'], 2, ",", "");
            $product['scontolistinovendita'] = number_format($product['scontolistinovendita'], 2, ",", "");
            $product['price'] = number_format($product['price'], 2, ",", "");
            $product['sconto_rivenditore_1'] = number_format($product['sconto_rivenditore_1'], 2, ",", "");
            $product['sconto_rivenditore_2'] = number_format($product['sconto_rivenditore_2'], 2, ",", "");
            $product['rebate_1'] = number_format($product['rebate_1'], 2, ",", "");
            $product['rebate_2'] = number_format($product['rebate_2'], 2, ",", "");
            $product['rebate_3'] = number_format($product['rebate_3'], 2, ",", "");
            
            $product['fuori_produzione_string'] = ($product['fuori_produzione'] == 0 ? 'In produzione' : 'Fuori produzione');
            $product['condition_string'] = ($product['condition'] == 'new' ? 'No' : 'S&igrave;');
            $product['acquisto_in_dollari_string'] = ($product['acquisto_in_dollari'] == 1 ? 'S&igrave;' : 'No');
            $product['blocco_prezzi_string'] = ($product['blocco_prezzi'] == 0 ? 'No' : 'S&igrave');
            $product['login_for_offer_string'] = ($product['login_for_offer'] == 0 ? 'No' : 'S&igrave');
            $product['trasporto_gratuito_string'] = ($product['trasporto_gratuito'] == 1 ? 'S&igrave;' : 'No');
        }

        $product = $p_row[0]; // Per agevolare conversione 1.4 -> 1.6

        $listini_fornitori = Db::getInstance()->executeS('SELECT s.name as fornitore, pli.listino, pli.sconto_acquisto_1, pli.sconto_acquisto_2, pli.sconto_acquisto_3, pli.wholesale_price FROM product_listini pli JOIN '._DB_PREFIX_.'product p ON p.id_product = pli.id_product JOIN '._DB_PREFIX_.'supplier s ON pli.id_supplier = s.id_supplier WHERE p.id_supplier != pli.id_supplier AND p.id_product = '.$obj->id);

        foreach($listini_fornitori as &$pli)
        {
            $pli['sconto_acquisto_1'] = number_format($pli['sconto_acquisto_1'], 2, ",", "");
            $pli['sconto_acquisto_2'] = number_format($pli['sconto_acquisto_2'], 2, ",", "");
            $pli['sconto_acquisto_3'] = number_format($pli['sconto_acquisto_3'], 2, ",", "");
            $pli['wholesale_price'] = number_format($pli['wholesale_price'], 2, ",", "");
        }

        $data->assign('obj', $obj);
        $data->assign('product', $product);
        $data->assign('listini_fornitori', $listini_fornitori);

        $this->tpl_form_vars['custom_form'] = $data->fetch();
        
        // Rifatto tutto nel tpl, tenuti gli echo per sicurezza

        /*echo '
        <fieldset>
            <legend>Sezione articolo</legend>
            <table cellpadding="5" cellspacing="5">
                <tr><td style="width:150px">ID CRM</td><td colspan="7"><input type="text" size="130" name="id_crm" readonly="readonly" value="'.$obj->id.'" /></td></tr>
                
                <tr><td style="width:150px">Tipo anagrafica</td><td colspan="7"><input type="text" size="130" name="tipo_anagrafica" readonly="readonly" value="'.$product['tipo_anagrafica'].'" /></td></tr>
                <tr><td>Modello</td><td colspan="7"><input type="text" size="130" name="modello" readonly="readonly" value="'.$product['modello'].'" /></td></tr>
                <tr><td>Produttore</td><td colspan="7"><input type="text" size="130" value="'.$product['costruttore'].'"  readonly="readonly" /></td></tr>
                
                <tr><td>Codice articolo  EZ</td><td colspan="7"><input type="text" size="130" value="'.$product['reference'].'" readonly="readonly" /></td></tr>
                <tr><td>Codice ID Database</td><td colspan="7"><input type="text" size="130" value="'.$product['id_product'].'" readonly="readonly" /></td></tr>
                <tr><td>Cod. Produttore (SKU)</td><td colspan="7"><input type="text" size="130"  value="'.$product['supplier_reference'].'" readonly="readonly" /></td></tr>
                <tr><td>Codifica fornitore 1</td><td colspan="7"><input type="text" size="130" value="'.$product['cod_fornitore_1'].'" readonly="readonly" /></td></tr>
                <tr><td>Cod. EAN</td><td><input type="text" size="11" value="'.$product['ean13'].'" readonly="readonly" /></td>
                <td>EAN descrizione</td><td><input type="text" size="11" name="ean_descrizione" value="'.$product['ean_descrizione'].'" /></td>
                <td>EAN quantità</td><td><input type="text" size="6" name="ean_quantita" value="'.$product['ean_quantita'].'" /></td>
                <td>EAN tipo</td><td><input type="text" size="6" name="ean_tipo" value="'.$product['ean_tipo'].'" /></td></tr>
                
                <tr><td>Codice Instrastat</td><td colspan="7"><input type="text" size="130" name="intrastat" value="'.$product['intrastat'].'" /></td></tr>
                <tr><td>Descr. Ez (50 car.)</td><td colspan="7"><input type="text" size="130" name="name_short" value="'.substr($product['name'],0,50).'" readonly="readonly" /></td></tr>
                <tr><td>Descr. estesa Ez</td><td colspan="7"><input type="text" size="130" value="'.$product['name'].'" readonly="readonly" /></td></tr>
                
                <tr><td>In produzione?</td><td>
                <input type="text" size="12" value="'.($product['fuori_produzione'] == 0 ? 'In produzione' : 'Fuori produzione').'" readonly="readonly" />
                </td><td>Usato (sì-no)</td><td><input type="text" size="12" value="'.($product['condition'] == 'new' ? 'No' : 'S&igrave;').'" readonly="readonly" />
                </td><td></td><td></td></tr>
                
                <tr><td>Data inserimento</td><td><input type="text" size="12" value="'.Tools::displayDate($product['date_add'],5).'" readonly="readonly"/>
                </td><td>Data ultima modifica</td><td><input type="text" size="12" value="'.Tools::displayDate($product['date_upd'],5).'" readonly="readonly"/>
                </td><td>Data validit&agrave;</td><td><input type="text" size="12" value=""  readonly="readonly" /></td><td></td></tr>
                
                <!-- <tr><td>Codice del produttore</td><td><input type="text" size="12"  value="'.$product['manufacturer_code'].'" readonly="readonly"/> -->
                
                <tr><td>Codice della famiglia</td><td><input type="text" size="12" value="'.$product['category_code'].'" readonly="readonly"/></td>
                <td>Descrizione famiglia</td><td><input type="text" size="12" value="'.$product['categoria'].'" readonly="readonly" />
                </td>
                <td></td><td></td></tr>
                
                <tr><td>Prodotto scaricabile</td><td><input type="text" size="12" value="'.$product['downloadable'].'"  readonly="readonly" /></td>
                <td>Codice prod scaricabile</td><td><input type="text" size="12" value="'.$product['codice_prodotto_scaricabile'].'" readonly="readonly" /></td><td></td><td></td></tr>
                
                <tr><td>Peso package</td><td><input type="text" size="12" value="'.$product['weight'].'"  readonly="readonly" /></td>
                <td>Peso netto</td><td><input type="text" size="12" value=""  readonly="readonly" /></td>
                <td></td><td></td></tr>
                
                <tr><td>Nome fornitore primario</td><td><input type="text" size="12" value="'.$product['fornitore'].'" readonly="readonly" /></td>
                <td>Note fornitore</td><td><input type="text" size="12" value="'.$product['note_fornitore'].'" readonly="readonly" /></td>
                <td>Codice del fornitore esterno</td><td><input type="text" size="12" value="'.$product['codice_fornitore'].'" readonly="readonly" />
                </td>	<td></td><td></td></tr>
                
                <tr><td>Tipo</td><td colspan="7"><input type="text" size="130"  value="'.$product['tipo'].'" readonly="readonly" /></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                <tr><td>Serie</td><td colspan="7"><input type="text" size="130"  value="'.$product['serie'].'" readonly="readonly" /></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                <tr><td>Altro</td><td colspan="7"><input type="text" size="130" name="altro_esolver" value="'.$product['altro_esolver'].'" /></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                <tr><td>Note</td><td colspan="7"><input type="text" size="130" value="'.$product['note'].'" readonly="readonly" /></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                <tr><td>Tempo di approvvigionamento</td><td colspan="7"><input type="text" size="130" name="tempo_approvvigionamento" value="'.$product['tempo_approvvigionamento'].'" /></td><td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                <tr><td>Codice a barre c/o fornitore</td><td colspan="7"><input type="text" size="130" name="codice_barre_fornitore" value="'.$product['codice_barre_fornitore'].'" /></td><td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                <tr><td>Note 1</td><td colspan="7"><input type="text" size="130" value="'.$product['note'].'" readonly="readonly" /></td><td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                <tr><td>Note 2</td><td  colspan="7"><input type="text" size="130" name="note_2" value="'.$product['note_2'].'" /></td><td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                
                <tr><td>Pz in confezione</td><td colspan="7"><input type="text" size="130" name="pezzi_in_confezione" value="'.$product['pezzi_in_confezione'].'" /></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                <!-- <tr><td>Nr</td><td><input type="text" size="130" name="nr" value="'.$product['nr'].'" /> -->
                <!-- <tr><td>Pz</td><td><input type="text" size="130" name="pz" value="'.$product['pz'].'" /> -->

                <tr><td>Contenuto confezione</td><td colspan="7"><input type="text" size="130" value="'.$product['confezione'].'" readonly="readonly" /></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
                <tr><td>Garanzia</td><td colspan="7"><input type="text" size="130" value="'.$product['garanzia'].'" readonly="readonly" /></td><td></td><td></td>	<td></td><td></td>	<td></td><td></td>	<td></td><td></td></tr>
            </table>
        </fieldset><br />
        ';
        
        echo '
        <fieldset>
            <legend>Sezione magazzino Ezdirect e fornitori</legend>
            <table cellpadding="5" cellspacing="5">
                <tr><td style="width:150px">Magazzino EZ</td><td><input type="text" size="10"  value="'.$product['stock_quantity'].'" readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td style="width:150px">Magazzino Amazon</td><td><input type="text" size="10"  value="'.$product['amazon_quantity'].'" readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td>Scorta minima</td><td><input type="text" size="10" value="'.$product['scorta_minima'].'" readonly="readonly" style="text-align:right" /></td>
                <tr><td>Scorta minima Amazon</td><td><input type="text" size="10" value="'.$product['scorta_minima_amazon'].'" readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td>Ord da clienti</td><td><input type="text" size="10" value="'.$product['ordinato'].'" readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td>Imp Ord. Su gestionale</td><td><input type="text" size="10" value="'.$product['impegnato_quantity'].'" readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td>Ordinato a fornitore 1</td><td><input type="text" size="10" value="'.$product['ordinato_quantity'].'" readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                
                <tr><td>Magazzino Allnet</td><td><input type="text" size="10"  value="'.$product['supplier_quantity'].'" readonly="readonly" style="text-align:right" /></td>
                <td>In arrivo Allnet</td><td><input type="text" size="10" value="'.$product['arrivo_quantity'].'" readonly="readonly"  style="text-align:right"/></td>
                <td>Data arrivo allnet</td><td><input type="text" size="10" value="'.(($product['data_arrivo'] != '1946-01-01') ? ($product['data_arrivo'] == '0000-00-00' || $product['data_arrivo'] == '1970-01-01' ? 'ND' : date("d/m/Y",strtotime($product['data_arrivo']))) : date('d/m/Y')).'" readonly="readonly"  style="text-align:right"/></td></tr>
                
                <tr><td>Magazzino Esprinet</td><td><input type="text" size="10"  value="'.$product['esprinet_quantity'].'" readonly="readonly" style="text-align:right" /></td>
                <td>In arrivo Esprinet</td><td><input type="text" size="10" value="'.$product['arrivo_esprinet_quantity'].'" readonly="readonly"  style="text-align:right"/></td>
                <td>Data arrivo Esprinet</td><td><input type="text" size="10" value="'.(($product['data_arrivo_esprinet'] != '1946-01-01') ? ($product['data_arrivo_esprinet'] == '0000-00-00' || $product['data_arrivo_esprinet'] == '1970-01-01' ? 'ND' : date("d/m/Y",strtotime($product['data_arrivo_esprinet']))) : date('d/m/Y')).'" readonly="readonly"  style="text-align:right"/></td></tr>
                
                <tr><td>Magazzino Attiva</td><td><input type="text" size="10"  value="'.$product['attiva_quantity'].'" readonly="readonly" style="text-align:right" /></td>
                <td>In arrivo Attiva</td><td><input type="text" size="10" value="'.$product['arrivo_attiva_quantity'].'" readonly="readonly"  style="text-align:right"/></td>
                <td>Data arrivo Attiva</td><td><input type="text" size="10" value="'.(($product['data_arrivo_attiva'] != '1946-01-01') ? ($product['data_arrivo_attiva'] == '0000-00-00' || $product['data_arrivo_attiva'] == '1970-01-01' ? 'ND' : date("d/m/Y",strtotime($product['data_arrivo_attiva']))) : date('d/m/Y')).'" readonly="readonly"  style="text-align:right"/></td></tr>
                
                <tr><td>Magazzino Itancia</td><td><input type="text" size="10"  value="'.$product['itancia_quantity'].'" readonly="readonly"  style="text-align:right"/>
                </td><td>In arrivo Itancia</td><td><input type="text" size="10"  value="'.$product['arrivo_itancia_quantity'].'" readonly="readonly"  style="text-align:right"/></td><td>Data arrivo Itancia</td><td><input type="text" size="10"  value="'.$product['data_arrivo_itancia'].'" readonly="readonly" style="text-align:right" /></td></tr>
                
                <tr><td>Magazzino Intracom</td><td><input type="text" size="10"  value="'.$product['intracom_quantity'].'" readonly="readonly"  style="text-align:right"/>
                </td><td>In arrivo Intracom</td><td><input type="text" size="10"  value="'.$product['arrivo_intracom_quantity'].'" readonly="readonly"  style="text-align:right"/></td><td>Data arrivo Intracom</td><td><input type="text" size="10"  value="'.$product['data_arrivo_intracom'].'" readonly="readonly" style="text-align:right" /></td></tr>
                
                <tr><td>Magazzino ASIT</td><td><input type="text" size="10"  value="'.$product['asit_quantity'].'" readonly="readonly"  style="text-align:right"/>
                </td><td></td><td></td><td></td><td></td></tr>
                
                <tr><td>Disponibile da</td><td><input type="text" size="10" value="'.(($product['date_available'] != '1946-01-01') ? ($product['date_available'] == '0000-00-00' || $product['date_available'] == '1970-01-01' ? 'ND' : date("d/m/Y",strtotime($product['date_available']))) : date('d/m/Y')).'" readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
            </table>		
        </fieldset><br />
        ';
        
        echo '
        <fieldset>
            <legend>Sezione listini</legend>
            <table cellpadding="5" cellspacing="5">
                <tr><td style="width:150px">Listino costruttore</td><td><input type="text" size="8" value="'.number_format($product['listino'],2,",","").'" readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                
                <tr><td>Sconto 1 '.$product['fornitore'].'</td><td><input type="text" size="8"  value="'.number_format($product['sconto_acquisto_1'],2,",","").' %"  readonly="readonly" style="text-align:right" /></td>
                <td>Sconto 2 '.$product['fornitore'].'</td><td><input type="text" size="8" value="'.number_format($product['sconto_acquisto_2'],2,",","").' %" readonly="readonly" style="text-align:right" /></td>
                <td>Sconto 3 '.$product['fornitore'].'</td><td><input type="text" size="8" value="'.number_format($product['sconto_acquisto_3'],2,",","").' %" readonly="readonly" style="text-align:right" /></td>
                <td>Netto acq. '.$product['fornitore'].'</td><td><input type="text" size="8" value="'.number_format($product['wholesale_price'],2,",","").'"  readonly="readonly" style="text-align:right" /></td></tr>
        ';
        
        foreach($listini_fornitori as $pli)
        {
            echo '
                <tr><td>Sconto 1 '.$pli['fornitore'].'</td><td><input type="text" size="8"  value="'.number_format($pli['sconto_acquisto_1'],2,",","").' %"  readonly="readonly" style="text-align:right" /></td>
                <td>Sconto 2 '.$pli['fornitore'].'</td><td><input type="text" size="8" value="'.number_format($pli['sconto_acquisto_2'],2,",","").' %" readonly="readonly" style="text-align:right" /></td>
                <td>Sconto 3 '.$pli['fornitore'].'</td><td><input type="text" size="8" value="'.number_format($pli['sconto_acquisto_3'],2,",","").' %" readonly="readonly" style="text-align:right" /></td>
                <td>Netto acq.  '.$pli['fornitore'].'</td><td><input type="text" size="8" value="'.number_format($pli['wholesale_price'],2,",","").'"  readonly="readonly" style="text-align:right" /></td></tr>
            ';
        }
        
        echo '		
                <tr><td>Acquisto in dollari</td><td><input type="text" size="8"  value="'.($product['acquisto_in_dollari'] == 1 ? 'S&igrave;' : 'No').'"  readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                
                <tr><td>Blocco prezzi</td><td><input type="text" size="8" value="'.($product['blocco_prezzi'] == 0 ? 'No' : 'S&igrave').'" readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                
                <tr><td>Login for offer</td><td><input type="text" size="8" value="'.($product['login_for_offer'] == 0 ? 'No' : 'S&igrave').'" readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                
                <tr><td>Sconto cliente</td><td><input type="text" size="8"  value="'.number_format($product['scontolistinovendita'],2,",","").' %" readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                
                <tr><td>Prezzo vendita</td><td><input type="text" size="8" value="'.number_format($product['price'],2,",","").'" readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                <tr><td>Trasporto gratuito</td><td><input type="text" size="8" value="'.($product['trasporto_gratuito'] == 1 ? 'S&igrave;' : 'No').'" readonly="readonly" style="text-align:right" /></td>
                <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                
                <tr><td>SC riv1</td><td><input type="text" size="8"  value="'.number_format($product['sconto_rivenditore_1'],2,",","").' %" readonly="readonly" style="text-align:right" /></td><td>Sc riv 2</td><td><input type="text" size="8" value="'.number_format($product['sconto_rivenditore_2'],2,",","").' %" readonly="readonly" style="text-align:right" /></td><td></td><td></td></tr>
                
                <tr><td>Rebate 1</td><td><input type="text" size="8" value="'.number_format($product['rebate_1'],2,",","").' %" readonly="readonly" style="text-align:right" /></td><td>Descrizione rebate 1</td><td><input type="text" size="8" name="descrizione_rebate_1" value="'.$product['descrizione_rebate_1'].'" readonly="readonly"style="text-align:right" /></td><td></td><td></td></tr>
                <tr><td>Rebate 2</td><td><input type="text" size="8" value="'.number_format($product['rebate_2'],2,",","").' %" readonly="readonly" style="text-align:right" /></td><td>Descrizione rebate 2</td><td><input type="text" size="8" name="descrizione_rebate_2" value="'.$product['descrizione_rebate_2'].'" readonly="readonly"style="text-align:right" /></td><td></td><td></td></tr>
                <tr><td>Rebate 3</td><td><input type="text" size="8" value="'.number_format($product['rebate_3'],2,",","").' %" readonly="readonly" style="text-align:right" /></td><td>Descrizione rebate 3</td><td><input type="text" size="8" name="descrizione_rebate_3" value="'.$product['descrizione_rebate_3'].'" readonly="readonly"style="text-align:right" /></td><td></td><td></td></tr>
                <tr><td>Premio target</td><td><input type="text" size="8" name="premio_target" value="'.$product['premio_target'].'" style="text-align:right" /></td>
                <tr><td>Descrizione premio</td><td><input type="text" size="8" name="descrizione_premio" value="'.$product['descrizione_premio'].'" readonly="readonly"style="text-align:right" /></td><td></td><td></td></tr>
            </table>
        </fieldset><br />
        ';
        
        echo '
        <fieldset><legend>Sezione marketplace</legend>
            <table>
                <tr><td style="width:150px">ASIN</td><td><input type="text" size="110" value="'.$product['asin'].'"  readonly="readonly" />
                <tr><td>Commissione amazon</td><td><input type="text" size="110" name="commissione_amazon" value="'.$product['commissione_amazon'].'" />
                <tr><td>Commissione Eprice</td><td><input type="text" size="110" name="commissione_eprice" value="'.$product['commissione_eprice'].'" />
            </table>
        </fieldset>
        ';
        
        echo '<p style="text-align:center"><input type="submit" class="button" name="accoda_esolver" value="Accoda a CSV eSolver e scrivi file" />';
        echo '</div>';*/
    }
    
    public function initFormStorico($obj)
    {
        $data = $this->createTemplate($this->tpl_form);

        $context = Context::getContext();

        $listino_attuale = number_format($obj->listino, 2, ",", "");
        $acquisto_attuale = number_format($obj->wholesale_price, 2, ",", "");
        $vendita_attuale = number_format($obj->price, 2, ",", "");

        $storico_prezzi = Db::getInstance()->executeS('
            SELECT * 
            FROM storico_prezzi 
            WHERE id_product = '.$obj->id.' 
            ORDER BY date DESC
        ');

        foreach ($storico_prezzi as &$storico){
            $storico['date'] = Tools::displayDate($storico['date'], $context->language->id);
            $storico['storico'] = str_replace('<td', '<td style="text-align:right"', str_replace('.', ',', $storico['storico']));
        }

        $data->assign('listino_attuale', $listino_attuale);
        $data->assign('acquisto_attuale', $acquisto_attuale);
        $data->assign('vendita_attuale', $vendita_attuale);
        $data->assign('storico_prezzi', $storico_prezzi);

        $this->tpl_form_vars['custom_form'] = $data->fetch();
    }

    // Questa funzione prende per scontato che reduction_type = percentage -> correggere! Fare controllo reduction_type
    public function getSconti($name)
	{
        $row = Db::getInstance()->getRow("
            SELECT price, reduction
            FROM "._DB_PREFIX_."specific_price 
            WHERE id_product = ".Tools::getValue('id_product')."
                AND specific_price_name = '".$name."'
        ");
		
		$sconto = $row['reduction'];
		
        // Se sto cercando lo sconto rivenditore 2 e non esiste, lo calcolo aumentando del 3% il prezzo finale del rivenditore 1
		if($name == 'sc_riv_2' && !$row) {
			$name = 'sc_riv_1';

			$row = Db::getInstance()->getRow("
                SELECT price, reduction
                FROM "._DB_PREFIX_."specific_price 
                WHERE id_product = ".Tools::getValue('id_product')."
                    AND specific_price_name = '".$name."'
            ");

            if(!$row)
                $sconto = 0;
            else {
                $finito = $row['price'] - ($row['price'] * $row['reduction']); // Prezzo finale riv. 1
                $finito = $finito + ($finito * 3 / 100); // Prezzo finale riv. 2
    
                $sconto = ($row['price'] - $finito) / $row['price'];
            }
		}

		return $sconto;
	}
	
	public function getQtSconti($name)
	{
		$qt = Db::getInstance()->getValue("
            SELECT from_quantity
            FROM "._DB_PREFIX_."specific_price 
            WHERE id_product = ".Tools::getValue('id_product')."
                AND specific_price_name = '".$name."'
        ");

        if(!$qt)
            $qt = 0;

		return $qt;
	}

    public function updSconti($sconto, $qta, $group, $specific_price_name, $product, $listino) 
    {
		Db::getInstance()->execute("
            REPLACE INTO "._DB_PREFIX_."specific_price (id_product, id_group, price, from_quantity, reduction, reduction_type, specific_price_name) 
            VALUES (".$product.", ".$group.", ".$listino.", ".$qta.", ".$sconto.", 'percentage', '".$specific_price_name."')
        ");
	}

    // Finire di correggere e testare!
    public function aggiornaCoseNonPredefinite($object) 
    {
        // Prezzi speciali (tab Prices) -> non funziona! testare ancora

		$spw_wholesale = Db::getInstance()->getValue('SELECT wholesale_price FROM '._DB_PREFIX_.'specific_price_wholesale WHERE id_product = '.$object->id);
		
		if($spw_wholesale > 0)
		{
			if($object->listino != Tools::getValue('vecchio_listino')) // Se Listino Vendita è stato modificato
			{
				$spw = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'specific_price_wholesale WHERE id_product = '.$object->id);
				foreach($spw as $s)
				{
					$new_spw = ($object->listino * ((100 - $s['reduction_1'])/100) * ((100 - $s['reduction_2'])/100) * ((100 - $s['reduction_3'])/100));
					Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'specific_price_wholesale SET wholesale_price = '.$new_spw.' WHERE id_specific_price = '.$s['id_specific_price']);
				}
			}
		}

        // Quantità (tab Quantities)
		
		$stock_quantity = Tools::getValue('stock_quantity'); // Mod. qta mag.
		$supplier_quantity = Db::getInstance()->getValue('SELECT supplier_quantity FROM '._DB_PREFIX_.'product WHERE id_product = '.$object->id);
		$itancia_quantity = Db::getInstance()->getValue('SELECT itancia_quantity FROM '._DB_PREFIX_.'product WHERE id_product = '.$object->id);
		$esprinet_quantity = Db::getInstance()->getValue('SELECT esprinet_quantity FROM '._DB_PREFIX_.'product WHERE id_product = '.$object->id);
		$attiva_quantity = Db::getInstance()->getValue('SELECT attiva_quantity FROM '._DB_PREFIX_.'product WHERE id_product = '.$object->id);
		$ingram_quantity = Db::getInstance()->getValue('SELECT ingram_quantity FROM '._DB_PREFIX_.'product WHERE id_product = '.$object->id);
		$asit_quantity = Db::getInstance()->getValue('SELECT asit_quantity FROM '._DB_PREFIX_.'product WHERE id_product = '.$object->id);
		$intracom_quantity = Db::getInstance()->getValue('SELECT intracom_quantity FROM '._DB_PREFIX_.'product WHERE id_product = '.$object->id);
		$total_quantity = $stock_quantity + $supplier_quantity + $esprinet_quantity + $attiva_quantity + $ingram_quantity + $itancia_quantity + $asit_quantity + $intracom_quantity;
		
		Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product SET stock_quantity = ".Tools::getValue('stock_quantity')." WHERE id_product = ".$object->id);
		Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product SET quantity = ".$total_quantity." WHERE id_product = ".$object->id);
		
        // Amazon (tab Informations e Prices)
		Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product SET no_amazon = '".addslashes($_POST['no_amazon'])."', fnsku = '".$_POST['fnsku']."', amazon_free_shipping = '".(isset($_POST['amazon_free_shipping']) ? 1 : 0)."', sku_amazon = '".$_POST['sku_amazon']."', amazon_it = ".str_replace(',','.',$_POST['sconto_amazon_it']).", amazon_fr = ".str_replace(',','.',$_POST['sconto_amazon_fr']).", amazon_uk = ".str_replace(',','.',$_POST['sconto_amazon_uk']).", amazon_es = ".str_replace(',','.',$_POST['sconto_amazon_es']).", amazon_de = ".str_replace(',','.',$_POST['sconto_amazon_de']).", amazon_nl = ".str_replace(',','.',$_POST['sconto_amazon_nl']).", amazon_se = ".str_replace(',','.',$_POST['sconto_amazon_se']).", amazon_pl = ".str_replace(',','.',$_POST['sconto_amazon_pl'])." WHERE id_product = ".$object->id);
		
        // Parola chiave (tab SEO - Solo per italiano)
		Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product_lang SET seo_keyword = '".$_POST['parola_chiave']."' WHERE id_lang = ".$this->context->language->id." AND id_product = ".$object->id);

        // Esolver (tab Prices - Acquisto) -> si potrebbe migliorare aggiungendo una association nella classe Product per le tabelle esolver
		Db::getInstance()->execute("REPLACE INTO product_esolver VALUES ($object->id, '".$_POST['rebate_1']."', '".$_POST['rebate_2']."', '".$_POST['rebate_3']."', '".$_POST['wholesale_price_esolver']."')");

        // Esolver (tab eSolver) -> da testare! si potrebbe migliorare aggiungendo una association nella classe Product per le tabelle esolver
        Db::getInstance()->execute("REPLACE INTO campi_esolver VALUES ($object->id, '".$_POST['tipo_anagrafica']."', '".$_POST['modello']."', '".addslashes($_POST['ean_descrizione'])."', '".addslashes($_POST['ean_tipo'])."', '".$_POST['ean_quantita']."', '".addslashes($_POST['intrastat'])."', '".addslashes($_POST['name_short'])."', '".addslashes($_POST['altro_esolver'])."', '".addslashes($_POST['tempo_approvvigionamento'])."', '".$_POST['codice_barre_fornitore']."', '".$_POST['pezzi_in_confezione']."', '".$_POST['nr']."', '".$_POST['pz']."', '".$_POST['commissione_amazon']."', '".$_POST['commissione_eprice']."', '".addslashes($_POST['note_2'])."', '".addslashes($_POST['premio_target'])."')");
		
        // Video (tab Informations)

		if(isset($_POST['cancellavideo'])) {
            foreach($_POST['cancellavideo'] as $cancvid) {
                Db::getInstance()->execute("DELETE FROM product_video WHERE id = ".$cancvid);
            }
        }
		
        // Inutilizzato, tenuto per test sui prodotti vecchi
		if($_POST['video1'] != '') {
            $video1 = str_replace("youtu.be/", "www.youtube.com/embed/", $_POST['video1']);
            $video1 = preg_replace('/\&.*/', '', $video1);

			Db::getInstance()->execute("INSERT INTO product_video (id, id_product, content) VALUES (NULL, ".$object->id.", '".$video1."')");
		}
		
        // Inutilizzato, tenuto per test sui prodotti vecchi
        if($_POST['video2'] != '') {
			$video2 = str_replace("youtu.be", "www.youtube.com/embed/", $_POST['video2']);
			$video2 = preg_replace('/\&.*/', '', $video2);
			
			Db::getInstance()->execute("INSERT INTO product_video (id, id_product, content) VALUES (NULL, ".$object->id.", '".$video2."')");
		}

        // Altri fornitori (tab Informations)
			
		$suppliers = array();
		foreach ($_POST['other_suppliers'] as $supplier) {
			$suppliers[] = $supplier;
		}
		$suppliers = serialize($suppliers);

		Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product SET other_suppliers = '".$suppliers."' WHERE id_product = ".$object->id);
		
        // Sconti rivenditore e sconti quantità (tab Prices)

		if(isset($_POST['sconto_rivenditore_1'])) {
			$_POST['sconto_rivenditore_1'] = $_POST['sconto_rivenditore_1']/100;

			Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."specific_price WHERE id_product = ".$object->id." AND specific_price_name = 'sc_riv_1'");
            $this->updSconti($_POST['sconto_rivenditore_1'], 0, 3, "sc_riv_1", $object->id, $object->listino);
		}
			
		if($_POST['sconto_rivenditore_1'] == 0) {
			//$this->updSconti($_POST['sconto_rivenditore_1'], 0, 3, "sc_riv_1", $object->id, $object->listino);
            Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."specific_price WHERE id_product = ".$object->id." AND specific_price_name = 'sc_riv_1'");
		}
			
		if(isset($_POST['sconto_rivenditore_2'])) {
			$_POST['sconto_rivenditore_2'] = $_POST['sconto_rivenditore_2']/100;
				
			Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."specific_price WHERE id_product = ".$object->id." AND specific_price_name = 'sc_riv_2'");
			// $this->updSconti("$_POST[sconto_rivenditore_2]", 0, 22, "sc_riv_2", $object->id, $object->listino); // Non serve, viene calcolato a partire a sc_riv_1
		}
			
		if($_POST['sconto_rivenditore_2'] == 0) {
			//$this->updSconti("$_POST[sconto_rivenditore_2]", 0, 22, "sc_riv_2", $object->id, $object->listino);
			Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."specific_price WHERE id_product = ".$object->id." AND specific_price_name = 'sc_riv_2'");
		}
			
		if(isset($_POST['sconto_rivenditore_3'])) {
			$_POST['sconto_rivenditore_3'] = $_POST['sconto_rivenditore_3']/100;
				
			Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."specific_price WHERE id_product = ".$object->id." AND specific_price_name = 'sc_riv_3'");
			$this->updSconti("$_POST[sconto_rivenditore_3]", 0, 12, "sc_riv_3", $object->id, $object->listino);
		}
			
		if($_POST['sconto_rivenditore_3'] == 0) {
			//$this->updSconti("$_POST[sconto_rivenditore_3]", 0, 12, "sc_riv_3", $object->id, $object->listino);
			Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."specific_price WHERE id_product = ".$object->id." AND specific_price_name = 'sc_riv_3'");
		}

		if(isset($_POST['sconto_quantita_1'])) {
			$_POST['sconto_quantita_1'] = $_POST['sconto_quantita_1'] / 100;
		
			Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."specific_price WHERE id_product = ".$object->id." AND specific_price_name = 'sc_qta_1'");
			$this->updSconti($_POST['sconto_quantita_1'], $_POST['quantita_1'], 1, "sc_qta_1", $object->id, $object->price);
		}
			
		if($_POST['sconto_quantita_1'] == 0 || $_POST['quantita_1'] == 0) {
			//$this->updSconti($_POST['sconto_quantita_1'], $_POST['quantita_1'], 1, "sc_qta_1", $object->id, $object->price);
			Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."specific_price WHERE id_product = ".$object->id." AND specific_price_name = 'sc_qta_1'");
		}
				
		if(isset($_POST['sconto_quantita_2'])) {
			$_POST['sconto_quantita_2'] = $_POST['sconto_quantita_2']/100;
			
			Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."specific_price WHERE id_product = ".$object->id." AND specific_price_name = 'sc_qta_2'");
			$this->updSconti("$_POST[sconto_quantita_2]", "$_POST[quantita_2]", 1, "sc_qta_2", $object->id, $object->price);
		}
			
		if($_POST['sconto_quantita_2'] == 0 || $_POST['quantita_2'] == 0) {
			//$this->updSconti("$_POST[sconto_quantita_2]", "$_POST[quantita_2]", 1, "sc_qta_2", $object->id, $object->price);
			Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."specific_price WHERE id_product = ".$object->id." AND specific_price_name = 'sc_qta_2'");
		}
			
        if(isset($_POST['sconto_quantita_3'])) {
			$_POST['sconto_quantita_3'] = $_POST['sconto_quantita_3']/100;

			Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."specific_price WHERE id_product = ".$object->id." AND specific_price_name = 'sc_qta_3'");
			$this->updSconti("$_POST[sconto_quantita_3]", "$_POST[quantita_3]", 1, "sc_qta_3", $object->id, $object->price);
		}
			
		if($_POST['sconto_quantita_3'] == 0 || $_POST['quantita_3'] == 0) {
			//$this->updSconti("$_POST[sconto_quantita_3]", "$_POST[quantita_3]", 1, "sc_qta_3", $object->id, $object->price);
			Db::getInstance()->execute("DELETE FROM "._DB_PREFIX_."specific_price WHERE id_product = ".$object->id." AND specific_price_name = 'sc_qta_3'");
		}
        
        // Comparaprezzi (tab Informations)
			
		if(isset($_POST['trovaprezzi_add'])) {
			Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product SET trovaprezzi = 1 WHERE id_product = ".$object->id);
		}
		else {
			Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product SET trovaprezzi = 0 WHERE id_product = ".$object->id);
		}
			
		if(isset($_POST['google_shopping_add'])) {
			Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product SET google_shopping = 1 WHERE id_product = ".$object->id);
		}
		else {
		    Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product SET google_shopping = 0 WHERE id_product = ".$object->id);
		}
			
		if(isset($_POST['eprice_add'])) {
			Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product SET eprice = 1 WHERE id_product = ".$object->id);
		}
		else {
			Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product SET eprice = 0 WHERE id_product = ".$object->id);
		}
			
        $amazon_array = array();
        if(isset($_POST['amazon_it_checkbox']))
            $amazon_array[] = 5;
        if(isset($_POST['amazon_fr_checkbox'])) 
            $amazon_array[] = 2;	
        if(isset($_POST['amazon_es_checkbox'])) 
            $amazon_array[] = 3;	
        if(isset($_POST['amazon_uk_checkbox'])) 
            $amazon_array[] = 1;	
        if(isset($_POST['amazon_de_checkbox'])) 
            $amazon_array[] = 4;
        if(isset($_POST['amazon_nl_checkbox'])) 
            $amazon_array[] = 6;
        if(isset($_POST['amazon_se_checkbox'])) 
            $amazon_array[] = 7;
        if(isset($_POST['amazon_pl_checkbox'])) 
            $amazon_array[] = 8;
        
        $amazon_string = implode(";",$amazon_array);
			
        Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product SET amazon = '".$amazon_string."' WHERE id_product = ".$object->id);
        if($amazon_string != '')
            Db::getInstance()->execute("REPLACE INTO amazon_products (id_product) VALUES (".$object->id.")");
        
        if(isset($_POST['tutti_add'])) {
            Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product SET comparaprezzi_check = 1 WHERE id_product = ".$object->id);
        }
        else {
            Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product SET comparaprezzi_check = 0 WHERE id_product = ".$object->id);
        }
        
        if(isset($_POST['trovaprezzi_add']) && isset($_POST['google_shopping_add'])) {
            Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product SET comparaprezzi_check = 1 WHERE id_product = ".$object->id);
        }
        else {
            Db::getInstance()->execute("UPDATE "._DB_PREFIX_."product SET comparaprezzi_check = 0 WHERE id_product = ".$object->id);
        }

        // Punti di forza Amazon (tab Informations)

        /* // Correggere: per usare questo devo togliere il salvataggio automatico e assegnare gli indici [n][id_lang]
        $languages = Language::getLanguages(false);
        foreach ($languages as $language)
        {
            $punti_di_forza = Tools::getValue('punti_di_forza_0_'.$language['id_lang']).':::::'.Tools::getValue('punti_di_forza_1_'.$language['id_lang']).':::::'.Tools::getValue('punti_di_forza_2_'.$language['id_lang']).':::::'.Tools::getValue('punti_di_forza_3_'.$language['id_lang']).':::::'.Tools::getValue('punti_di_forza_4_'.$language['id_lang']);
            Db::getInstance()->execute('UPDATE "._DB_PREFIX_."product_lang SET punti_di_forza = "'.htmlentities(stripslashes($punti_di_forza), ENT_COMPAT, 'UTF-8').'" WHERE id_lang = '.$language['id_lang'].' AND id_product = '.$object->id);
        }
        */

        // Trasporto gratuito (tab Shipping)

        // CORREGGERE: aggiungere "FREE_SHIPPING_INCLUDE_PRD" in ps_configuration (nella 1.4 aveva value 240)
        $row = Configuration::get('FREE_SHIPPING_INCLUDE_PRD');

        $trasp = unserialize($row['value']);
        $au = $object->id;

        foreach($trasp as $el) {
            if ($au == $el) {
                $var_a = 1;
                break;
            }
            else {
                $var_a = 0;
            }
        }

		if(isset($_POST['trasporto_gratuito_forzato'])) {
			if($var_a == 0) {
			    $trasp[] = $au;
			}
        }
		else {
			if($var_a == 1) {
			    $key = array_search($au, $trasp);
				unset($trasp[$key]);
			}
		}

        $rowtrasportogratuito = serialize($trasp);
        // CORREGGERE: aggiungere "FREE_SHIPPING_INCLUDE_PRD" in ps_configuration (nella 1.4 aveva value 240); correggere id_configuration (non è più 240) e decommentare
        // Db::getInstance()->execute("REPLACE INTO "._DB_PREFIX_."configuration (id_configuration, name, value) VALUES (240, 'FREE_SHIPPING_INCLUDE_PRD', '$rowtrasportogratuito')");
    }

    public function salvatutto()
	{
        if(Tools::getIsset('updateproduct')){
            
            $id_prodotto = $_GET['id_product']; // correggere: usarlo o eliminarlo

            // Funzioni da richiamare / cose da fare in ordine:
            // specific prices -> controllare nomi campi
            // attachments -> fatto (controllare modifiche commentate)
            // reference -> fatto
            // supplier reference -> fatto
            // bundle -> fatto, testare
            // update o add product -> fatto, testare add
            
            Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'order_detail SET category_id = '.Tools::getValue('id_category_default').' WHERE product_id = '.Tools::getValue('id_product'));
            
            // Gestione prezzi speciali
            /*
            $specificPrices = SpecificPrice::getByProductId((int)(Tools::getValue('id_product')));
            
            foreach($specificPrices as $specificPrice) {
        
                $idprzspc = $specificPrice['id_specific_price'];
                //$nomebottoneprzspc = "submitPricesModifiche_$idprzspc";

                $hiddenidprzspc = $_POST["id_specific_price_$idprzspc"];
                $fromquantityprzspc = $_POST["sp_from_quantity_$idprzspc"];
            
                $currencyprzspc = $_POST["sp_id_currency_$idprzspc"];
                $wholesalepricespc = str_replace(",",".",$_POST["sp_wholesale_price_$idprzspc"]);
                $countryprzspc = $_POST["sp_id_country_$idprzspc"];
                $groupprzspc = $_POST["sp_id_group_$idprzspc"];
                $priceprzspc = str_replace(",",".",$_POST["sp_price_$idprzspc"]);
                $reductionprzspc = 0; // $_POST["sp_reduction_$idprzspc"]/100;
                $fromprzspc = date("Y-m-d 23:59:59", strtotime($_POST["sp_from_$idprzspc"]));
                $toprzspc = date("Y-m-d 23:59:59", strtotime($_POST["sp_to_$idprzspc"]));
                
                Db::getInstance()->execute("
                    UPDATE `"._DB_PREFIX_."specific_price` 
                    SET `from_quantity` = '".$fromquantityprzspc."', 
                        `id_currency` = '".$currencyprzspc."',
                        `id_group` = '".$groupprzspc."',
                        `id_country` = '".$countryprzspc."',
                        `price` = '".$priceprzspc."',
                        `wholesale_price` = '".$wholesalepricespc."',
                        `reduction` = '".$reductionprzspc."',
                        `from` = '".$fromprzspc."',
                        `to` = '".$toprzspc."'
                    WHERE `id_specific_price` = '".$hiddenidprzspc."'
                ");
                
                $piecesspc =  $_POST["sp_pieces_$idprzspc"];
                
                if($piecesspc == '')
                {
                    Db::getInstance()->getValue("DELETE FROM "._DB_PREFIX_."specific_price_pieces WHERE id_specific_price = ".$hiddenidprzspc);
                }
                else
                {
                    if($piecesspc >= 0) {
                        $ctpcs = Db::getInstance()->getValue("SELECT count(id_specific_price) FROM "._DB_PREFIX_."specific_price_pieces WHERE `id_specific_price` = '$hiddenidprzspc'");
                        if($ctpcs > 0) {
                            $stockspc = Db::getInstance()->getValue("SELECT stock FROM "._DB_PREFIX_."specific_price_pieces WHERE id_specific_price = ".$hiddenidprzspc);
                            $stockspc = $piecesspc;
                            
                            Db::getInstance()->execute("UPDATE `"._DB_PREFIX_."specific_price_pieces` SET `pieces` = '$piecesspc',`stock` = '$piecesspc' WHERE `id_specific_price` = '$hiddenidprzspc'");
                        }
                        else {
                            Db::getInstance()->execute("INSERT INTO `"._DB_PREFIX_."specific_price_pieces` (id_specific_price, id_product, pieces, stock) VALUES ('$hiddenidprzspc', ".Tools::getValue('id_product').", '$piecesspc', '$piecesspc')");
                        }
                    }
                }
            }
            */

            $this->processAttachments();
            
            // Controllo se reference, supplier_reference e ean13 esistono già in un altro prodotto

            if($_POST['reference'] == '') { }
            else {
                $rowdoppio = Db::getInstance()->getRow("SELECT id_product, reference FROM "._DB_PREFIX_."product WHERE reference = '".$_POST['reference']."' AND active != 2 AND id_product != ".$object->id);
                
                if($rowdoppio['reference'] == $_POST['reference']) {
                    $this->errors[] = Tools::displayError('Esiste già un prodotto nel database associato al codice di riferimento che hai inserito.');
                }
            }

            if($_POST['supplier_reference'] == '') { }
            else {
                $rowdoppio = Db::getInstance()->getRow("SELECT id_product, supplier_reference FROM "._DB_PREFIX_."product WHERE supplier_reference = '".$_POST['supplier_reference']."' AND active != 2 AND id_product != ".$object->id);
                
                if($rowdoppio['supplier_reference'] === $_POST['supplier_reference']) {
                    $this->errors[] = Tools::displayError('Esiste già un prodotto nel database associato al codice SKU che hai inserito.');
                }
            }

            if($_POST['ean13'] == '') { }
            else {
                $rowdoppio = Db::getInstance()->getRow("SELECT id_product, ean13 FROM "._DB_PREFIX_."product WHERE ean13 = '".$_POST['ean13']."' AND active != 2 AND id_product != ".$object->id);
                
                if($rowdoppio['ean13'] === $_POST['ean13']) {
                    $this->errors[] = Tools::displayError('Esiste già un prodotto nel database associato al codice EAN che hai inserito.');
                }
            }

            // Update della proprietà active dei bundle (Kit) del prodotto

            if(isset($_GET['id_product'])) {
                if($_POST['active'] == 0 || $_POST['price'] == 0)
                    $bundle_active = 0;
                else
                    $bundle_active = 1;
                
                Db::getInstance()->execute("UPDATE bundle SET active = ".$bundle_active." WHERE father = ".$_GET['id_product']);
                            
                $child_bundles = Db::getInstance()->executeS("SELECT * FROM bundle WHERE bundle_products LIKE '%".$_GET['id_product']."%'");
                foreach($child_bundles as $child) {
                    $products_bundle = explode(";",$child['bundle_products']);
                                
                    foreach ($products_bundle as $product_bundle) {
                        if($product_bundle == $_GET['id_product']) {
                            Db::getInstance()->execute("UPDATE bundle SET active = ".$bundle_active." WHERE id_bundle = ".$child['id_bundle']);
                        }
                    }
                }
            }

            // CORREGGERE! è un input hidden nella tab features
            /*if(isset($_POST['inviaFeatures'])) {
                if (Validate::isLoadedObject($product = new Product((int)(Tools::getValue('id_product')))))
                {
                    // delete all objects
                    $product->deleteFeatures();

                    // add new objects
                    $languages = Language::getLanguages(false);
                    foreach ($_POST AS $key => $val)
                    {
                        if (preg_match('/^feature_([0-9]+)_value/i', $key, $match))
                        {
                            if ($val)
                                $product->addFeaturesToDB($match[1], $val);
                            else
                            {
                                if ($default_value = $this->checkFeatures($languages, $match[1]))
                                {
                                    $id_value = $product->addFeaturesToDB($match[1], 0, 1,  Tools::getValue('custom_'.$match[1].'_'.(int)$language['id_lang']));
                                    foreach ($languages AS $language)
                                    {
                                        if ($cust = Tools::getValue('custom_'.$match[1].'_'.(int)$language['id_lang']))
                                            $product->addFeaturesCustomToDB($id_value, (int)$language['id_lang'], $cust);
                                        else
                                            $product->addFeaturesCustomToDB($id_value, (int)$language['id_lang'], $default_value);
                                    }
                                }
                            }
                        }
                    }
                }
            }*/

            $this->display = 'edit';
            $this->processUpdate();
        }
        else if(Tools::getIsset('addproduct')){

            // Controllo se reference, supplier_reference e ean13 esistono già in un altro prodotto

            if($_POST['reference'] == '') { }
            else {
                $rowdoppio = Db::getInstance()->getRow("SELECT id_product, reference FROM "._DB_PREFIX_."product WHERE reference = '".$_POST['reference']."' AND active != 2");
                
                if($rowdoppio['reference'] == $_POST['reference']) {
                    $this->errors[] = Tools::displayError('Esiste già un prodotto nel database associato al codice di riferimento che hai inserito.');
                }
            }

            if($_POST['supplier_reference'] == '') { }
            else {
                $rowdoppio = Db::getInstance()->getRow("SELECT id_product, supplier_reference FROM "._DB_PREFIX_."product WHERE supplier_reference = '".$_POST['supplier_reference']."' AND active != 2");
                
                if($rowdoppio['supplier_reference'] === $_POST['supplier_reference']) {
                    $this->errors[] = Tools::displayError('Esiste già un prodotto nel database associato al codice SKU che hai inserito.');
                }
            }

            if($_POST['ean13'] == '') { }
            else {
                $rowdoppio = Db::getInstance()->getRow("SELECT id_product, ean13 FROM "._DB_PREFIX_."product WHERE ean13 = '".$_POST['ean13']."' AND active != 2");
                
                if($rowdoppio['ean13'] === $_POST['ean13']) {
                    $this->errors[] = Tools::displayError('Esiste già un prodotto nel database associato al codice EAN che hai inserito.');
                }
            }

            //$this->display = 'add';
            $this->processAdd();
        }
	}

    // Correggere: non funziona in edit ma solo nella lista prodotti
    /*public function initToolbarTitle()
    {
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('TEST'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }*/

    // AGGIUNGERE JS NEL FORM VALIDATE PRODUCT:
    /*try {
        $(document).ready(function () {
            
            warn = false;
            
            $("#product").submit(function () {
                warn=false;
            });
            
            $("#product").change( function(){
                warn=true;
            });
    
            $(window).bind('beforeunload', function(){ 
                if (warn) {
                    var salvamodifiche = window.confirm("Ci sono modifiche non salvate. Clicca su OK per uscire e salvare, clicca su Annulla per uscire senza salvare."); 
                
                    if (salvamodifiche) {
                        $("#salvatutto").trigger("click");
                        alert("Modifiche salvate correttamente.");
                    }
                }
    
            });
        });
    }
    catch(e) {
    }*/
}
