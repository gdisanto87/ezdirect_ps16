<?php

class AdminStrumentiAmazonControllerCore extends AdminController
{
    public function __construct()
    {
        $this->displayInformation('Correggere i file inclusi (problemi permessi) e le date di listorders');

        $this->bootstrap = true;
        $this->lang = false;

        $this->context = Context::getContext();

        $this->display = 'view';

        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Strumenti Amazon'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }

    public function renderView()
    {
        if(Tools::getIsset('submitfeed'))
		{
			$avviso = 'Attendere il caricamento nel riquadro sottostante finch&egrave; non compaiono i messaggi di conferma. La procedura impiega tipicamente 2 minuti circa';
			// test
			$iframe = '<iframe style="width:100%; height: 450px" src="procedure-amazon.php?test=y"></iframe>';
			// $iframe = '<iframe style="width:100%; height: 450px" src="procedure-amazon.php?prodotti=y&submitfrominst=y"></iframe>';
		}
		
		if(Tools::getIsset('listorders'))
		{
			if(Tools::getIsset('importa'))
			{
				$data_dal_a = explode("-", $_POST['arco_dal']);
				$data_dal = $data_dal_a[2]."-".$data_dal_a[1]."-".$data_dal_a[0]; 
				
				$data_al_a = explode("-", $_POST['arco_al']);
				$data_al = $data_al_a[2]."-".$data_al_a[1]."-".$data_al_a[0]; 
				
				$avviso = 'Attendere il caricamento nel riquadro sottostante finch&egrave; non compaiono i messaggi di conferma. La procedura impiega pi&ugrave; tempo a seconda della lunghezza del periodo scelto.';
				
				// test server 1.6
				$iframe = '<iframe style="width:100%; height: 450px" src="https://46.101.235.11/docs/disponibilita/MarketplaceWebServiceOrders/Samples/ListOrdersSample.php?ordini=y&submitfrominst=y&date_from='.$data_dal.'&date_to='.$data_al.'"></iframe>';
				// $iframe = '<iframe style="width:100%; height: 450px" src="https://www.ezdirect.it/docs/disponibilita/MarketplaceWebServiceOrders/Samples/ListOrdersSample.php?ordini=y&submitfrominst=y&date_from='.$data_dal.'&date_to='.$data_al.'"></iframe>';
			}
		}
		
		if(Tools::getIsset('submitfattura'))
		{
			$avviso = 'Attendere il caricamento nel riquadro sottostante.';

			// test server 1.6 -> forbidden!
			$iframe = '<iframe style="width:100%; height: 450px" src="../docs/disponibilita/invio_fattura_amazon_post.php"></iframe>';
			// $iframe = '<iframe style="width:100%; height: 450px" src="https://www.ezdirect.it/docs/disponibilita/invio_fattura_amazon_post.php"></iframe>';
		}
        
		/*if(Tools::getIsset('amazonfeed'))
		{
			$avviso = 'Attendere il caricamento nel riquadro sottostante finch&egrave; non compaiono i messaggi di conferma. La procedura impiega tipicamente 3-4 minuti circa';
			$iframe = '<iframe style="width:100%; height: 450px" src="procedure-amazon.php?report=y&submitfrominst=y"></iframe>';
		}*/

		$this->tpl_view_vars = array(
            'avviso' => $avviso,
            'iframe' => $iframe,
        );

        return parent::renderView();
    }
}