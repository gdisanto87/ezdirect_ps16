<?php

class AdminStatsAcquistiControllerCore extends AdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->lang = false;

        $this->context = Context::getContext();

        $this->display = 'view';

        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Statistiche Acquisti'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }

    public function renderView()
    {
        $this->displayInformation('Correggere $cookie->acquistiStats, inserire datatable e yetii o sostituirlo');
        // Solo Ezio, Barbara, Federico e Carolina possono vedere le statistiche sugli acquisti
        $check_employee = (($this->context->employee->id == 1 || $this->context->employee->id == 2 || $this->context->employee->id == 6 || $this->context->employee->id == 22) ? true : false);

        // Correggere: devo fare unset cookie prima?
        $cookie = $this->context->cookie; // correggere: è giusto?

        $mesi = array();
        $mesi['01'] = array();
        $mesi['02'] = array();
        $mesi['03'] = array();
        $mesi['04'] = array();
        $mesi['05'] = array();
        $mesi['06'] = array();
        $mesi['07'] = array();
        $mesi['08'] = array();
        $mesi['09'] = array();
        $mesi['10'] = array();
        $mesi['11'] = array();
        $mesi['12'] = array();

        if($acquistiStats['period_c_start'] == '')
        {
            $latest_row = Db::getInstance()->getValue('SELECT data_registrazione FROM '._DB_PREFIX_.'fatture_acquisto ORDER BY data_registrazione DESC');
            $acquistiStats = array();
            $acquistiStats['period'] = date('M',strtotime($latest_row));
            $acquistiStats['year'] = date('Y',strtotime($latest_row));
            $acquistiStats['period_c_start'] = date('m',strtotime($latest_row));
            $acquistiStats['period_c_end'] = date('m',strtotime($latest_row));
            $acquistiStats['year_c_start'] = date('Y',strtotime($latest_row));
            $acquistiStats['year_c_end'] = date('Y',strtotime($latest_row));
            $acquistiStats = serialize($acquistiStats);
            $cookie->acquistiStats = $acquistiStats;
        }
			
        if($cookie->acquistiStats == '')
        {
            $latest_row = Db::getInstance()->getValue('SELECT data_registrazione FROM '._DB_PREFIX_.'fatture_acquisto ORDER BY data_registrazione DESC');
            $acquistiStats = array();
            $acquistiStats['period'] = date('M',strtotime($latest_row));
            $acquistiStats['year'] = date('Y',strtotime($latest_row));
            $acquistiStats['period_c_start'] = date('m',strtotime($latest_row));
            $acquistiStats['period_c_end'] = date('m',strtotime($latest_row));
            $acquistiStats['year_c_start'] = date('Y',strtotime($latest_row));
            $acquistiStats['year_c_end'] = date('Y',strtotime($latest_row));
            $acquistiStats = serialize($acquistiStats);
            $cookie->acquistiStats = $acquistiStats;
        }
			
        if(Tools::getIsset('vai-acquisti'))
        {
            $acquistiStats = array();
            $acquistiStats['period'] = Tools::getValue('select-month');
            $acquistiStats['year'] = Tools::getValue('select-year');
            $acquistiStats = serialize($acquistiStats);
            $cookie->acquistiStats = $acquistiStats;
        }	
        
        if(Tools::getIsset('vai-acquisti-c'))
        {
            $acquistiStats = array();
            $acquistiStats['period_c_start'] = Tools::getValue('select-month_c_start');
            $acquistiStats['year_c_start'] = Tools::getValue('select-year_c_start');
            $acquistiStats['period_c_end'] = Tools::getValue('select-month_c_end');
            $acquistiStats['year_c_end'] = Tools::getValue('select-year_c_end');
            $acquistiStats = serialize($acquistiStats);
            $cookie->acquistiStats = $acquistiStats;
        }
        
        if(Tools::getIsset('reset-acquisti'))
        {
            $latest_row = Db::getInstance()->getValue('SELECT data_registrazione FROM '._DB_PREFIX_.'fatture_acquisto ORDER BY data_registrazione DESC');
            $acquistiStats = array();
            $acquistiStats['period'] = date('M',strtotime($latest_row));
            $acquistiStats['year'] = date('Y',strtotime($latest_row));
            $acquistiStats['period_c_start'] = date('m',strtotime($latest_row));
            $acquistiStats['period_c_end'] = date('m',strtotime($latest_row));
            $acquistiStats['year_c_start'] = date('Y',strtotime($latest_row));
            $acquistiStats['year_c_end'] = date('Y',strtotime($latest_row));
            $acquistiStats = serialize($acquistiStats);
            $cookie->acquistiStats = $acquistiStats;
        }
        
        $acquistiStats = unserialize($cookie->acquistiStats);
			
		$mese_attuale = date('M');

        if($acquistiStats['period'] != '')
        {
            switch($acquistiStats['period']) {
                case 'Jan': $mese = 'Gennaio'; $mese_inizio = '01'; $mese_fine = '01'; $td = cal_days_in_month(CAL_GREGORIAN, 1, $acquistiStats['year']); break;		
                case 'Feb': $mese = 'Febbraio'; $mese_inizio = '02'; $mese_fine = '02'; $td = cal_days_in_month(CAL_GREGORIAN, 2, $acquistiStats['year']); break;
                case 'Mar': $mese = 'Marzo'; $mese_inizio = '03'; $mese_fine = '03'; $td = cal_days_in_month(CAL_GREGORIAN, 3, $acquistiStats['year']); break;
                case 'Apr': $mese = 'Aprile'; $mese_inizio = '04'; $mese_fine = '04'; $td = cal_days_in_month(CAL_GREGORIAN, 4, $acquistiStats['year']); break;
                case 'May': $mese = 'Maggio'; $mese_inizio = '05'; $mese_fine = '05'; $td = cal_days_in_month(CAL_GREGORIAN, 5, $acquistiStats['year']); break;
                case 'Jun': $mese = 'Giugno'; $mese_inizio = '06'; $mese_fine = '06'; $td = cal_days_in_month(CAL_GREGORIAN, 6, $acquistiStats['year']); break;
                case 'Jul': $mese = 'Luglio'; $mese_inizio = '07'; $mese_fine = '07'; $td = cal_days_in_month(CAL_GREGORIAN, 7, $acquistiStats['year']); break;
                case 'Aug': $mese = 'Agosto'; $mese_inizio = '08'; $mese_fine = '08'; $td = cal_days_in_month(CAL_GREGORIAN, 8, $acquistiStats['year']); break;
                case 'Sep': $mese = 'Settembre'; $mese_inizio = '09'; $mese_fine = '09'; $td = cal_days_in_month(CAL_GREGORIAN, 9, $acquistiStats['year']); break;
                case 'Oct': $mese = 'Ottobre'; $mese_inizio = '10'; $mese_fine = '10'; $td = cal_days_in_month(CAL_GREGORIAN, 10, $acquistiStats['year']); break;
                case 'Nov': $mese = 'Novembre'; $mese_inizio = '11'; $mese_fine = '11'; $td = cal_days_in_month(CAL_GREGORIAN, 11, $acquistiStats['year']); break;
                case 'Dec': $mese = 'Dicembre'; $mese_inizio = '12'; $mese_fine = '12'; $td = cal_days_in_month(CAL_GREGORIAN, 12, $acquistiStats['year']); break;
                case 'q1': $mese = 'Q1: Gennaio - Marzo'; $mese_inizio = '01'; $mese_fine = '03'; $td = '31'; break;
                case 'q2': $mese = 'Q2: Aprile - Giugno'; $mese_inizio = '04'; $mese_fine = '06'; $td = '30'; break;
                case 'q3': $mese = 'Q3: Luglio - Settembre'; $mese_inizio = '07'; $mese_fine = '09'; $td = '30'; break;
                case 'q4': $mese = 'Q4: Ottobre - Dicembre'; $mese_inizio = '10'; $mese_fine = '12'; $td = '31'; break;
                case 'anno': $mese = 'Intero anno'; $mese_inizio = '01'; $mese_fine = '12'; $td = '31'; break;
                default: $mese = $mese_attuale; break;
            }
        }
        else if($acquistiStats['period_c_start'] != '')
        {
            $mese = $acquistiStats['period_c_start']; 
            $mese_inizio = $acquistiStats['period_c_start']; 
            $mese_fine = $acquistiStats['period_c_end']; 
            $td = cal_days_in_month(CAL_GREGORIAN, (int)$acquistiStats['period_c_end'], $acquistiStats['year']);
        }

        // Commentato 1.4
        /*
        $riepilogo_html .= '<!-- <form method="post" action="index.php?controller=AdminStatsAcquisti&token='.$this->token.'"> Seleziona il periodo (singolo):'; 
        $riepilogo_html .= '
        <select name="select-month" id="select-month" style="height:28px; margin-right:10px">
            <option value="q1" '.($acquistiStats['period'] == 'q1' ? 'selected="selected"' : '').'>Quarter 1</option>
            <option value="q2" '.($acquistiStats['period'] == 'q2' ? 'selected="selected"' : '').'>Quarter 2</option>
            <option value="q3" '.($acquistiStats['period'] == 'q3' ? 'selected="selected"' : '').'>Quarter 3</option>
            <option value="q4" '.($acquistiStats['period'] == 'q4' ? 'selected="selected"' : '').'>Quarter 4</option>
            <option value="anno" '.($acquistiStats['period'] == 'anno' ? 'selected="selected"' : '').'>Intero anno</option>
        </select>';
        $riepilogo_html .= '<select name="select-year" id="select-year"  style="height:28px;">';

        for($i = 2017; $i<=date("Y"); $i++)
            $riepilogo_html .= '<option value="'.$i.'" '.($acquistiStats['year'] == $i ? 'selected="selected"' : '').'>'.$i.'</option>';

        $riepilogo_html .= '</select><input type="submit" name="vai-acquisti" value="Vai" class="button" style="margin-left:10px; margin-top:-3px; height:28px" /></form><br /> -->';
        */
        
        $riepilogo_html .= '<form method="post" action="index.php?controller=AdminStatsAcquisti&token='.$this->token.'">Seleziona il periodo DA: '; 
        $riepilogo_html .= '<select name="select-month_c_start" id="select-month_c_start" style="height:28px; margin-right:10px">
            <option value="01" '.($acquistiStats['period_c_start'] == '01' ? 'selected="selected"' : '').'>Gennaio</option>
            <option value="02" '.($acquistiStats['period_c_start'] == '02' ? 'selected="selected"' : '').'>Febbraio</option>
            <option value="03" '.($acquistiStats['period_c_start'] == '03' ? 'selected="selected"' : '').'>Marzo</option>
            <option value="04" '.($acquistiStats['period_c_start'] == '04' ? 'selected="selected"' : '').'>Aprile</option>
            <option value="05" '.($acquistiStats['period_c_start'] == '05' ? 'selected="selected"' : '').'>Maggio</option>
            <option value="06" '.($acquistiStats['period_c_start'] == '06' ? 'selected="selected"' : '').'>Giugno</option>
            <option value="07" '.($acquistiStats['period_c_start'] == '07' ? 'selected="selected"' : '').'>Luglio</option>
            <option value="08" '.($acquistiStats['period_c_start'] == '08' ? 'selected="selected"' : '').'>Agosto</option>
            <option value="09" '.($acquistiStats['period_c_start'] == '09' ? 'selected="selected"' : '').'>Settembre</option>
            <option value="10" '.($acquistiStats['period_c_start'] == '10' ? 'selected="selected"' : '').'>Ottobre</option>
            <option value="11" '.($acquistiStats['period_c_start'] == '11' ? 'selected="selected"' : '').'>Novembre</option>
            <option value="12" '.($acquistiStats['period_c_start'] == '12' ? 'selected="selected"' : '').'>Dicembre</option>
        </select>';
        
        $riepilogo_html .= '<select name="select-year_c_start" id="select-year_c_start"  style="height:28px;">
        ';
        for($i = 2017; $i<=date("Y"); $i++)
            $riepilogo_html .= '<option value="'.$i.'" '.($acquistiStats['year_c_start'] == $i ? 'selected="selected"' : '').'>'.$i.'</option>';
        $riepilogo_html .= '
        </select>';
        
        $riepilogo_html .= '&nbsp;&nbsp;&nbsp; A: <select name="select-month_c_end" id="select-month_c_end" style="height:28px; margin-right:10px">
            <option value="01" '.($acquistiStats['period_c_end'] == '01' ? 'selected="selected"' : '').'>Gennaio</option>
            <option value="02" '.($acquistiStats['period_c_end'] == '02' ? 'selected="selected"' : '').'>Febbraio</option>
            <option value="03" '.($acquistiStats['period_c_end'] == '03' ? 'selected="selected"' : '').'>Marzo</option>
            <option value="04" '.($acquistiStats['period_c_end'] == '04' ? 'selected="selected"' : '').'>Aprile</option>
            <option value="05" '.($acquistiStats['period_c_end'] == '05' ? 'selected="selected"' : '').'>Maggio</option>
            <option value="06" '.($acquistiStats['period_c_end'] == '06' ? 'selected="selected"' : '').'>Giugno</option>
            <option value="07" '.($acquistiStats['period_c_end'] == '07' ? 'selected="selected"' : '').'>Luglio</option>
            <option value="08" '.($acquistiStats['period_c_end'] == '08' ? 'selected="selected"' : '').'>Agosto</option>
            <option value="09" '.($acquistiStats['period_c_end'] == '09' ? 'selected="selected"' : '').'>Settembre</option>
            <option value="10" '.($acquistiStats['period_c_end'] == '10' ? 'selected="selected"' : '').'>Ottobre</option>
            <option value="11" '.($acquistiStats['period_c_end'] == '11' ? 'selected="selected"' : '').'>Novembre</option>
            <option value="12" '.($acquistiStats['period_c_end'] == '12' ? 'selected="selected"' : '').'>Dicembre</option>
        </select>';
        
        $riepilogo_html .= '<select name="select-year_c_end" id="select-year_c_end"  style="height:28px;">
        ';
        for($i = 2017; $i<=date("Y"); $i++)
            $riepilogo_html .= '<option value="'.$i.'" '.($acquistiStats['year_c_end'] == $i ? 'selected="selected"' : '').'>'.$i.'</option>';
        $riepilogo_html .= '
        </select>
        <input type="submit" name="vai-acquisti-c" value="Vai" class="button" style="margin-left:10px; margin-top:-3px; height:28px" />
        <input type="submit" name="reset-acquisti" value="Resetta" class="button" style="margin-left:10px; margin-top:-3px; height:28px" />
        </form>';

        if($acquistiStats['year'] != '') {
            $data_inizio = $acquistiStats['year'].'-'.$mese_inizio.'-01 00:00:00';
            $data_finale = date('Y-m-t 23:59:59', strtotime($acquistiStats['year'].'-'.$mese_fine.'-11'));
            $data_fine = date('Y-m-t 23:59:59', strtotime($acquistiStats['year'].'-'.$mese_fine.'-11'));
        }
        else {
            $data_inizio = $acquistiStats['year_c_start'].'-'.$mese_inizio.'-01 00:00:00';
            $data_finale = date('Y-m-t 23:59:59', strtotime($acquistiStats['year_c_end'].'-'.$mese_fine.'-11'));
            $data_fine = date('Y-m-t 23:59:59', strtotime($acquistiStats['year_c_end'].'-'.$mese_fine.'-11'));
        }

        $fatturato_acquisto_totale = Db::getInstance()->executeS('
            SELECT data_registrazione, importo, tipo_documento, classe_documento 
            FROM '._DB_PREFIX_.'fatture_acquisto 
            WHERE data_registrazione >= "'.$data_inizio.'" AND data_registrazione <= "'.$data_finale.'" AND tipo_documento != 750
        ');
        
        $fatturato_acquisto_prodotti = Db::getInstance()->executeS('
            SELECT data_registrazione, importo, tipo_documento, classe_documento 
            FROM '._DB_PREFIX_.'fatture_acquisto 
            WHERE data_registrazione >= "'.$data_inizio.'" AND data_registrazione <= "'.$data_finale.'" AND tipo_documento != 750 AND id_manufacturer > 0 
        ');

        foreach($fatturato_acquisto_totale as $fa)
        {
            $data_explode = explode('-',$fa['data_registrazione']);
            $mese = $data_explode[1];
            if($fa['classe_documento'] == 41)
                $mesi[$mese]['acquisto_totale'] += 0;
            else if($fa['tipo_documento'] == 720 || $fa['tipo_documento'] == 721 || $fa['tipo_documento'] == 723)
                $mesi[$mese]['acquisto_totale'] -= $fa['importo'];
            else
                $mesi[$mese]['acquisto_totale'] += $fa['importo'];
        }
        
        foreach($fatturato_acquisto_prodotti as $fa)
        {
            $data_explode = explode('-',$fa['data_registrazione']);
            $mese = $data_explode[1];
            if($fa['classe_documento'] == 41)
                $mesi[$mese]['acquisto_prodotti'] += 0;
            else if($fa['tipo_documento'] == 720 || $fa['tipo_documento'] == 721 || $fa['tipo_documento'] == 723)
                $mesi[$mese]['acquisto_prodotti'] -= $fa['importo'];
            else
                $mesi[$mese]['acquisto_prodotti'] += $fa['importo'];
        }

        $riepilogo_html .= '<strong>2017</strong><br />';
        $riepilogo_html .= '<table class="table"><thead><tr><th>Mese</th><!-- <th>Vendite</th> --><th>Acquisti (totali)</th><th>Acquisti (solo prodotti)</th><!-- <th>Margine</th><th>Margine %</th> --></tr></thead>';
        
        $tot_vendite = 0;
        $tot_acquisti = 0;
        $tot_acquisti_iva = 0;
        $fine_ctrl = 0;
        
        foreach($mesi as $k => $v)
        {
            if($k >= $mese_inizio && $k <= $mese_fine)
            {
                $tot_vendite += $v['fatturato'];
                $tot_acquisti += $v['acquisto_totale'];
                $tot_acquisti_prod += $v['acquisto_prodotti'];
                
                $riepilogo_html .= '<tr><td style="text-align:right">'.($v['acquisto'] == 0 ? $k : '<!-- <a href="'.$currentIndex.'index.php?controller=AdminStatsAcquisti&token='.$this->token.'&v_acq='.$v['acquisto'].'&mese='.$k.'"> -->'.$k.' <!-- </a> -->').'</td>
                <!-- <td style="text-align:right; width:250px">'.number_format($v['fatturato'],2,","," ").'</td> -->
                <td style="text-align:right; width:250px">'.number_format($v['acquisto_totale'],2,","," ").'</td>
                <td style="text-align:right; width:250px">'.number_format($v['acquisto_prodotti'],2,","," ").'</td>
                <!--	<td style="text-align:right">'.number_format($margine,2,","," ").'</td>
                <td style="text-align:right">'.number_format($marginp,2,","," ").'</td> --></tr>';
            }
        }
        
        $riepilogo_html .= '<tr><td><strong>Totale</strong><!-- <td>'.$tot_vendite.'</td> --><td style="text-align:right">'.number_format($tot_acquisti,2,",",".").'</td><td style="text-align:right">'.number_format($tot_acquisti_prod,2,","," ").'</td></tr>';
    
        $riepilogo_html .= "</table>";
        
        $tot_fatturato = Db::getInstance()->getValue('
            SELECT SUM(importo) 
            FROM '._DB_PREFIX_.'fatture_acquisto 
            WHERE data_registrazione >= "'.$data_inizio.'" AND data_registrazione <= "'.$data_fine.'"
        ');
            
        $ca['man'] = Db::getInstance()->ExecuteS('
            SELECT 
                x.*, sum(tot_acquisti1-tot_note) tot_acquisti, SUM(tot_acquisti_iva1-tot_note_iva) tot_acquisti_iva, sum(orderQty1) orderQty 
            FROM (
                SELECT 
                    m.name,
                    fa.partita_iva as vat_number,
                    SUM(fa.quantita) as orderQty1,
                    SUM(CASE WHEN classe_documento != 41 THEN fa.importo ELSE 0 END) as tot_acquisti1,
                    SUM(CASE WHEN tipo_documento = 720 or tipo_documento = 721 or tipo_documento = 723 THEN fa.importo ELSE 0 END) as tot_note,
                    SUM(CASE WHEN tipo_documento = 720 or tipo_documento = 721 or tipo_documento = 723 THEN (fa.importo + (fa.importo * fa.aliquota / 100)) ELSE 0 END) as tot_note_iva,
                    SUM(CASE WHEN classe_documento != 41 THEN (fa.importo + (fa.importo * fa.aliquota / 100)) ELSE 0 END) as tot_acquisti_iva1,
                    c.id_category AS category,
                    m.id_manufacturer as manufacturer 
                FROM '._DB_PREFIX_.'fatture_acquisto fa
                LEFT JOIN `'._DB_PREFIX_.'category` c ON (c.id_category = fa.id_category)
                LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (fa.id_manufacturer = m.id_manufacturer)
                WHERE `data_registrazione` BETWEEN "'.$data_inizio.'" AND "'.$data_fine.'" AND tipo_documento != 750 
                GROUP BY fa.id_riga
            ) x
            GROUP BY manufacturer
            '.(isset($_POST['ordinaman']) ? 'ORDER BY (CASE WHEN manufacturer > 0 THEN 1 ELSE 0 END) DESC, '.$_POST['ordinaman'] : 'ORDER BY (CASE WHEN manufacturer > 0 THEN 1 ELSE 0 END) DESC, tot_acquisti DESC').'
        ');

        // Correggere: manca yetii! Sostituire tablesorter con datatable
        $riepilogo_html .= '
        <script type="text/javascript" src="yetii.js"></script>
        <div id="tab-container-cart" style="background-color:#fffff !important">
        <ul id="tab-container-cart-nav" class="tab-containers-nav">
        <li><a href="#per-costruttore"><strong>Per costruttore</strong></a></li>
        <li><a href="#per-fornitore"><strong>Per fornitore</strong></a></li>
        </ul>
        ';

        $riepilogo_html .= '
        <div class="yetii-cart" id="per-fornitore">
        <form method="post" action="#ordina-man"><br />
        <table class="table tablesorter float" border="0" cellspacing="0" cellspacing="0" id="table-costruttori">
        <thead><tr><th></th><th style="width:380px">'.('Costruttore').'</th>
    
        <th style="text-align:right" style="width:80px">'.('Qt').'</th> 
        
        <th style="text-align:right" style="width:150px">'.('Acquisti').'</th>
            
        <th style="text-align:right" style="width:150px">'.('% acquisti').'</th> <th style="text-align:right">'.('% Progr.').'</th>  <!-- <th>'.('Avg price').'
        </th>  -->
        <!-- <th>Margine lordo</th><th>Margine %</th> -->
        </tr></thead><tbody>';
                
        $m_tot_conteggio = 0;
        $m_tot_conteggio_p = 0;
        $m_tot_acquisti = 0;
        $m_tot_acquisti_p = 0;
        $count_man = 0;	
    
        $progressivo_man = 0;
        $progressivo_subman = 0;
        $progressivo_prodman = 0;
        $previous_subman = 0;
        
        $manufacturer_count_chart = 0;
        $manufacturer_data_chart = '';
        $other_manufacturer_tot = 0;

        foreach ($ca['man'] as $manrow) 
        {
            if($manrow['manufacturer'] == 0) {
                $riepilogo_html .= '</tbody><tr style="background-color:#ebebeb">
                    <td></td>
                    <td><strong style="color:red">Totale prodotti</td>
                    <td align="right"><strong style="color:red">'.$m_tot_conteggio.'</strong></td>
                    <td align="right"><strong style="color:red">'.Tools::displayPrice($m_tot_vendite, $currency).'</strong></td>
                    <td align="right"><strong style="color:red">100,0%</strong></td>
                    <td align="right"><strong style="color:red">'.number_format($progressivo_man, 1, ',', ' ').'%</strong></td> 
                </tr>';
                        
                $riepilogo_html .= '<tr><th></th><th style="width:380px">'.('Costruttore').'</th>
                    <th style="text-align:right" style="width:80px">'.('Qt').'</th> 
                    <th style="text-align:right" style="width:150px">'.('Acquisti').'</th>
                    <th style="text-align:right" style="width:150px">'.('% acquisti').'</th> <th style="text-align:right">'.('% Progr.').'</th>  <!-- <th>'.('Avg price').'
                    </th>  -->
                    <!-- <th>Margine lordo</th><th>Margine %</th> -->
                    </tr></thead>
                <tbody>';
            }
            
            if($manufacturer_count_chart < 12)
                $manufacturer_data_chart .= '["'.$manrow['name'].'", '.number_format($manrow['tot_acquisti'],2,".","").'],';
            else
                $other_manufacturer_tot += number_format($manrow['tot_acquisti'],2,".","");	
            
            $riepilogo_html .= '
            <tr style="visibility:visible; background-color:#ebebeb">
                <td><img src="../img/admin/add.gif" alt="Espandi" style="cursor:pointer" title="Espandi" id="man_espandi_'.$manrow['manufacturer'].'" />
                
                <script type="text/javascript">
                    $(document).ready(function(){
                        $("#man_espandi_'.$manrow['manufacturer'].'").click(function(){
                            $("tr.man_subcat_'.$manrow['manufacturer'].'").toggle();
                            if($("tr.man_subcat_'.$manrow['manufacturer'].':visible").length) {
                                $(this).attr("src", "../img/admin/forbbiden.gif");	
                                $("#table-costruttori").trigger("pagerComplete.tsSticky");
                            }
                            else {
                                $(this).attr("src", "../img/admin/add.gif");
                                $(".subcat_button_'.$manrow['manufacturer'].'").attr("src", "../img/admin/add.gif");
                                $("tr.man_prodsubcat_'.$manrow['manufacturer'].'").hide();
                                $("#table-costruttori").trigger("pagerComplete.tsSticky");
                            }
                            return false;
                        });
                    });
                </script>
                
                </td><td style="font-weight:bold">'.(empty($manrow['name']) ? ('Non presente su CRM') : $manrow['name']).'</td>
            ';

            $progressivo_man += (100 * ($manrow['tot_acquisti']) / $tot_fatturato);
            
            $riepilogo_html .= '
                <td align="right">'.round($manrow['orderQty'],0).'</td>
                <td align="right">'.Tools::displayPrice($manrow['tot_acquisti'], $currency).'</td>
                <td align="right">'.((int)$tot_fatturato ? number_format((100 * $manrow['tot_acquisti'] / ($tot_fatturato)), 1, ',', ' ') : '0').'%</td>
                <td align="right">'.number_format($progressivo_man, 1, ',', ' ').'%</td>
            </tr>
            ';

            $ca['mansubcat'] = Db::getInstance()->executeS('
                SELECT x.*, sum(tot_acquisti1-tot_note) tot_acquisti, SUM(tot_acquisti_iva1-tot_note_iva) tot_acquisti_iva, sum(orderQty1) orderQty 
                FROM (
                    SELECT
                        '.(empty($manrow['name']) ? '"Categoria sconosciuta" as name' : 'cl.name').',
                        SUM(fa.quantita) as orderQty1,
                        SUM(CASE WHEN classe_documento != 41 THEN fa.importo ELSE 0 END) as tot_acquisti1,
                        SUM(CASE WHEN tipo_documento = 720 or tipo_documento = 721 or tipo_documento = 723 THEN fa.importo ELSE 0 END) as tot_note,
                        SUM(CASE WHEN tipo_documento = 720 or tipo_documento = 721 or tipo_documento = 723 THEN (fa.importo + (fa.importo * fa.aliquota / 100)) ELSE 0 END) as tot_note_iva,
                        SUM(CASE WHEN classe_documento != 41 THEN (fa.importo + (fa.importo * fa.aliquota / 100)) ELSE 0 END) as tot_acquisti_iva1,
                        '.(empty($manrow['name']) ? '0 as category' : 'c.id_category AS category').', 
                        '.(empty($manrow['name']) ? '0 as manufacturer' : 'm.id_manufacturer as manufacturer ').' 
                    FROM '._DB_PREFIX_.'fatture_acquisto fa
                    '.(empty($manrow['name']) ? '' : 'LEFT JOIN `'._DB_PREFIX_.'category` c ON (c.id_category = fa.id_category)').'
                    '.(empty($manrow['name']) ? '' : 'LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (c.id_category = cl.id_category)').'
                    '.(empty($manrow['name']) ? '' : 'LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (fa.id_manufacturer = m.id_manufacturer)').'
                    WHERE '.(empty($manrow['name']) ? '' : 'cl.id_lang = '.$this->context->language->id.' AND ').' `data_registrazione` BETWEEN "'.$data_inizio.'" AND "'.$data_fine.'" 
                        AND fa.id_manufacturer = '.(empty($manrow['name']) ? 0 : $manrow['manufacturer']).'
                        AND tipo_documento != 750 GROUP BY id_riga
                ) x
                GROUP BY category
                '.(isset($_POST['ordinaman']) ? 'ORDER BY '.$_POST['ordinaman'] : 'ORDER BY tot_acquisti DESC').'
            ');

            foreach ($ca['mansubcat'] as $mansubcatrow)
            {
                $progressivo_subman += (100 * $mansubcatrow['tot_acquisti'] / $tot_fatturato);

                $riepilogo_html .= '
                <tr class="invisible-table-row man_subcat_'.$manrow['manufacturer'].'" style="display:none">
                    <td><img src="../img/admin/add.gif" alt="Espandi" style="cursor:pointer" title="Espandi" id="man_subcat_espandi_'.$mansubcatrow['category'].'-'.$manrow['manufacturer'].'" class="subcat_button_'.$manrow['manufacturer'].'" />
                    
                    <script type="text/javascript">
                        $(document).ready(function(){
                            $("#man_subcat_espandi_'.$mansubcatrow['category'].'-'.$manrow['manufacturer'].'").click(function(){
                                $("tr[rel=\'man_prodsubcat_'.$mansubcatrow['category'].'-'.$manrow['manufacturer'].'\']").toggle();
                                if($("tr[rel=\'man_prodsubcat_'.$mansubcatrow['category'].'-'.$manrow['manufacturer'].'\']:visible").length) {
                                    $(this).attr("src", "../img/admin/forbbiden.gif");	
                                }
                                else {
                                    $(this).attr("src", "../img/admin/add.gif");
                                }
                                return false;
                            });
                        });
                    </script>

                    </td>
                    <td>'.(empty($mansubcatrow['category']) ? ('Categoria sconosciuta') : $mansubcatrow['name']).'</td>
                    <td align="right">'.round($mansubcatrow['orderQty'],0).'</td>
                    <td align="right">'.Tools::displayPrice($mansubcatrow['tot_acquisti'], $currency).'</td>
                    <td align="right">'.((int)$tot_fatturato ? number_format((100 * $mansubcatrow['tot_acquisti'] / ($tot_fatturato)), 1, ',', ' ') : '0').'%</td>
                    <td align="right">'.number_format($progressivo_subman, 1, ',', ' ').'%</td> 
                </tr>';

                $ca['prodmansubcat'] = Db::getInstance()->executeS('
                    SELECT x.*, sum(tot_acquisti1-tot_note) tot_acquisti, SUM(tot_acquisti_iva1-tot_note_iva) tot_acquisti_iva, sum(orderQty1) orderQty 
                    FROM (
                        SELECT
                            fa.codice_articolo,
                            fa.descrizione_articolo,
                            SUM(fa.quantita) as orderQty1,
                            SUM(CASE WHEN classe_documento != 41 THEN fa.importo ELSE 0 END) as tot_acquisti1,
                            SUM(CASE WHEN tipo_documento = 720 or tipo_documento = 721 or tipo_documento = 723 THEN fa.importo ELSE 0 END) as tot_note,
                            SUM(CASE WHEN tipo_documento = 720 or tipo_documento = 721 or tipo_documento = 723 THEN (fa.importo + (fa.importo * fa.aliquota / 100)) ELSE 0 END) as tot_note_iva,
                            SUM(CASE WHEN classe_documento != 41 THEN (fa.importo + (fa.importo * fa.aliquota / 100)) ELSE 0 END) as tot_acquisti_iva1,
                            fa.id_category AS category, 
                            fa.id_manufacturer as manufacturer 
                        FROM '._DB_PREFIX_.'fatture_acquisto fa
                        WHERE fa.id_category = '.$mansubcatrow['category'].'
                            AND tipo_documento != 750 
                            AND fa.id_manufacturer = '.($mansubcatrow['manufacturer'] == 0 ? 0 : $manrow['manufacturer']).' 
                            AND `data_registrazione` BETWEEN "'.$data_inizio.'" AND "'.$data_fine.'"
                        '.($mansubcatrow['manufacturer'] == 0 ? 'GROUP BY fa.id_riga' : 'GROUP BY fa.id_riga').'
                    ) x 
                    GROUP BY x.codice_articolo
                    '.(isset($_POST['ordinaman']) ? 'ORDER BY '.$_POST['ordinaman'] : 'ORDER BY tot_acquisti DESC').'
                ');
            
                foreach ($ca['prodmansubcat'] as $prodmansubcatrow)
                {
                    $progressivo_prodman += (100 * $prodmansubcatrow['tot_acquisti'] / $tot_fatturato);

                    $riepilogo_html .= '
                    <tr rel="man_prodsubcat_'.$mansubcatrow['category'].'-'.$manrow['manufacturer'].'" class="invisible-table-row man_prodsubcat_'.$manrow['manufacturer'].'" style="display:none">
                        <td></td><td><div style="margin-left:35px"><table style="width:345px; border:0px" class="table"><tr style="border:0px"><td style="border:0px; width:95px; text-align:left"><!-- <span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($prodmansubcatrow['product']).'"><a href="index.php?controller=AdminCatalogExFeatures&id_product='.$prodmansubcatrow['product'].'&updateproduct&token='.$tokenProducts.'" target="_blank"> --> '.(empty($prodmansubcatrow['reference']) ?  $prodmansubcatrow['codice_articolo'] : $prodmansubcatrow['reference']).'<!-- </a></span> --></td><td style="border:0px; width:250px">'.(empty($prodmansubcatrow['descrizione_articolo']) ? ('Articolo sconosciuto') : $prodmansubcatrow['descrizione_articolo']).'</td></tr></table></div></td>
                        <td align="right">'.round($prodmansubcatrow['orderQty'],0).'</td>
                        <td align="right">'.Tools::displayPrice($prodmansubcatrow['tot_acquisti'], $currency).'</td>
                        <td align="right">'.((int)$tot_fatturato ? number_format((100 * $prodmansubcatrow['tot_acquisti'] / ($tot_fatturato)), 1, ',', ' ') : '0').'%</td>
                        <td align="right">'.number_format($progressivo_prodman, 1, ',', ' ').'%</td> 
                    </tr>';
                }

                $previous_subman = $manrow['manufacturer'];
            }

            $m_tot_conteggio += $manrow['orderQty'];
            $m_tot_conteggio_p += (100 * $manrow['orderQty'] / ($this->t4+$this->t12));
            $m_tot_vendite += $manrow['tot_acquisti'];
            $m_tot_vendite_p += (100 * $manrow['tot_acquisti'] / $tot_fatturato);
            
            $count_man++;
        }

        $riepilogo_html .= '</tbody><tfoot>
        <tr style="background-color:#ebebeb">
            <td></td>
            <td><strong style="color:red">Totale</td>
            <td align="right"><strong style="color:red">'.$m_tot_conteggio.'</strong></td>
            <td align="right"><strong style="color:red">'.Tools::displayPrice($m_tot_vendite, $currency).'</strong></td>
            <td align="right"><strong style="color:red">100,0%</strong></td>
            <td align="right"><strong style="color:red">'.number_format($progressivo_man, 1, ',', ' ').'%</strong></td>
        </tr></tfoot></table>
        ';				
				
		$riepilogo_html .= '</form><br /><br /><div style="clear:both"></div>
            <p>
            <form action="" method="post">
            <br />
            
			<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
			<script type="text/javascript">google.charts.load("current", {"packages":["corechart"]});
			    google.charts.setOnLoadCallback(drawChart2);
                // Callback that creates and populates a data table,
                // instantiates the pie chart, passes in the data and
                // draws it.
			    function drawChart2() 
                {
                    // Create the data table.
                    var data = new google.visualization.DataTable();
                    data.addColumn("string", "Topping");
                    data.addColumn("number", "Slices");
                    data.addRows([
                    '.$manufacturer_data_chart.'
                    ["Altri", '.$other_manufacturer_tot.']
                    ]);

                    // Set chart options
                    var options = {"title":"Grafico costruttori",
                    is3D: true,
                    legend: "labeled",
                    pieSliceText: "none",
                    chartArea:{left:20,top:0,width:"97%",height:"97%"},
                    "width":850,
                    "height":500};

                    // Instantiate and draw our chart, passing in some options.
                    var chart = new google.visualization.PieChart(document.getElementById("chart_costruttori_div"));
                    chart.draw(data, options);
                    
                    // Instantiate and draw our chart, passing in some options.
                    var chart_div = document.getElementById("chart_categorie_image_div");
                    var chart = new google.visualization.PieChart(document.getElementById("chart_categorie_image_div"));
                    // Wait for the chart to finish drawing before calling the getImageURI() method.
                    google.visualization.events.addListener(chart, "ready", function () {
                        chart_div.innerHTML = "<img id=\'image_categorie\' style=\'display:none\' src=\'" + chart.getImageURI() + "\'>";
                        console.log(chart_div.innerHTML);
                    });

                    chart.draw(data, options);
                }
			</script>

            <div id="chart_costruttori_div"></div>
            <div id="chart_categorie_image_div"></div>
            <br /><br />
            </form>	
            </p>
        ';

        $riepilogo_html .= '</div>';
        $riepilogo_html .= '<div class="yetii-cart" id="per-fornitore">';

        $tot_fatturato_sup = Db::getInstance()->getValue('
            SELECT SUM(importo) 
            FROM '._DB_PREFIX_.'fatture_acquisto 
            WHERE data_registrazione >= "'.$data_inizio.'" AND data_registrazione <= "'.$data_fine.'"
        ');
					
		$ca['sup'] = Db::getInstance()->executeS('
			SELECT x.*, sum(tot_acquisti1-tot_note) tot_acquisti, SUM(tot_acquisti_iva1-tot_note_iva) tot_acquisti_iva, sum(orderQty1) orderQty 
            FROM (
                SELECT
                    fa.codice_cliente manufacturer,
                    fa.partita_iva as vat_number,
                    SUM(fa.quantita) as orderQty1,
                    SUM(CASE WHEN classe_documento != 41 THEN fa.importo ELSE 0 END) as tot_acquisti1,
                    SUM(CASE WHEN tipo_documento = 720 or tipo_documento = 721 or tipo_documento = 723 THEN fa.importo ELSE 0 END) as tot_note,
                    SUM(CASE WHEN tipo_documento = 720 or tipo_documento = 721 or tipo_documento = 723 THEN (fa.importo + (fa.importo * fa.aliquota / 100)) ELSE 0 END) as tot_note_iva,
                    SUM(CASE WHEN classe_documento != 41 THEN (fa.importo + (fa.importo * fa.aliquota / 100)) ELSE 0 END) as tot_acquisti_iva1,
                    c.id_category AS category
				FROM '._DB_PREFIX_.'fatture_acquisto fa
				LEFT JOIN `'._DB_PREFIX_.'category` c ON (c.id_category = fa.id_category)
				WHERE `data_registrazione` BETWEEN "'.$data_inizio.'" AND "'.$data_fine.'" 
                    AND tipo_documento != 750 
				GROUP BY fa.id_riga
                ) x 
            GROUP BY manufacturer 
			'.(isset($_POST['ordinasup']) ? 'ORDER BY '.$_POST['ordinasup'] : 'ORDER BY tot_acquisti DESC').'
		');
		
		$riepilogo_html .= '<form method="post" action="#ordina-man"><br />
            <table class="table tablesorter float" border="0" cellspacing="0" cellspacing="0" id="table-fornitori">
            <thead><tr><th></th><th style="width:380px">'.('Fornitore').'</th>
            <th style="text-align:right">'.('Qt').'</th> 
            <th style="text-align:right">'.('Acquisti').'</th>
            <th style="text-align:right">'.('% acquisti').'</th> <th style="text-align:right">'.('% Progr.').'</th>
            <th style="text-align:right">'.('Acquisti con IVA').'</th>
            </tr></thead><tbody>
        ';
						
        $s_tot_conteggio = 0;
        $s_tot_conteggio_p = 0;
        $s_tot_acquisti = 0;
        $s_tot_acquisti_p = 0;
        $count_sup = 0;	
    
        $progressivo_sup = 0;
        $progressivo_subsup = 0;
        $progressivo_prodsup = 0;
        $previous_subsup = 0;
        
        $manufacturer_count_chart_sup = 0;
        $manufacturer_data_chart_sup = '';
        $other_manufacturer_tot_sup = 0;

        foreach ($ca['sup'] as $suprow)
        {
            $namesup = Db::getInstance()->getValue('SELECT company FROM '._DB_PREFIX_.'customer WHERE codice_esolver = '.$suprow['manufacturer']);
					
            if($suprow['manufacturer'] == 454)
                $namesup = 'Allnet';
            else if($suprow['manufacturer'] == 568)
                $namesup = 'Gigaset';
            else if($suprow['manufacturer'] == 322)
                $namesup = 'Samsung';
            
            if($namesup == '')
                $namesup = Db::getInstance()->getValue('SELECT company FROM '._DB_PREFIX_.'customer WHERE vat_number LIKE "%'.$suprow['vat_number'].'%"');
            
            if($manufacturer_count_chart_sup < 12)
                $manufacturer_data_chart_sup .= '["'.$namesup.'", '.number_format($suprow['tot_acquisti'],2,".","").'],';
            else
                $other_manufacturer_tot_sup += number_format($suprow['tot_acquisti'],2,".","");

            $riepilogo_html .= '<tr style="background-color:#ebebeb">
                <td><img src="../img/admin/add.gif" alt="Espandi" style="cursor:pointer" title="Espandi" id="sup_espandi_'.$suprow['manufacturer'].'" /> 
                
                <script type="text/javascript">
                    $(document).ready(function(){
                        $("#sup_espandi_'.$suprow['manufacturer'].'").click(function(){
                            $("tr.sup_subcat_'.$suprow['manufacturer'].'").toggle();
                            if($("tr.sup_subcat_'.$suprow['manufacturer'].':visible").length) {
                                $(this).attr("src", "../img/admin/forbbiden.gif");	
                                $("#table-fornitori").trigger("pagerComplete.tsSticky");
                            }
                            else {
                                $(this).attr("src", "../img/admin/add.gif");
                                $(".subcat_button_'.$suprow['manufacturer'].'").attr("src", "../img/admin/add.gif");
                                $("tr.sup_prodsubcat_'.$suprow['manufacturer'].'").hide();
                                $("#table-fornitori").trigger("pagerComplete.tsSticky");
                            }
                            return false;
                        });
                    });
                </script>

                </td>
                <td style="font-weight:bold">'.(empty($namesup) ? ('Fornitore non presente') : $namesup).' ('.$suprow['manufacturer'].')</td>
            ';
            
            $progressivo_sup += (100 * ($suprow['tot_acquisti']) / $tot_fatturato);
                
            $riepilogo_html .= '
                <td align="right">'.round($suprow['orderQty'],0).'</td>
                <td align="right">'.Tools::displayPrice($suprow['tot_acquisti'], $currency).'</td>
                <td align="right">'.((int)$tot_fatturato_sup ? number_format((100 * $suprow['tot_acquisti'] / ($tot_fatturato_sup)), 1, ',', ' ') : '0').'%</td>
                <td align="right">'.number_format($progressivo_sup, 1, ',', ' ').'%</td> 
            ';
                
            $riepilogo_html .= '<td align="right">'.Tools::displayPrice($suprow['tot_acquisti_iva'], $currency).'</td></tr>';

            $ca['supsubcat'] = Db::getInstance()->executeS('
                SELECT fa.*, sum(tot_acquisti1-tot_note) tot_acquisti, SUM(tot_acquisti_iva1-tot_note_iva) tot_acquisti_iva, sum(orderQty1) orderQty 
                FROM (
                    SELECT
                        fa.id_riga, fa.codice_cliente, 
                        fa.id_manufacturer AS costruttore,
                        (CASE WHEN m.name != "" THEN m.name ELSE "Costruttore non presente" END) name,
                        SUM(fa.quantita) as orderQty1,
                        SUM(CASE WHEN classe_documento != 41 THEN fa.importo ELSE 0 END) as tot_acquisti1,
                        SUM(CASE WHEN tipo_documento = 720 or tipo_documento = 721 or tipo_documento = 723 THEN fa.importo ELSE 0 END) as tot_note,
                        SUM(CASE WHEN tipo_documento = 720 or tipo_documento = 721 or tipo_documento = 723 THEN (fa.importo + (fa.importo * fa.aliquota / 100)) ELSE 0 END) as tot_note_iva,
                        SUM(CASE WHEN classe_documento != 41 THEN (fa.importo + (fa.importo * fa.aliquota / 100)) ELSE 0 END) as tot_acquisti_iva1
                    FROM '._DB_PREFIX_.'fatture_acquisto fa
                    LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (fa.id_manufacturer = m.id_manufacturer)
                    WHERE `data_registrazione` BETWEEN "'.$data_inizio.'" AND "'.$data_fine.'" 
                    AND tipo_documento != 750 GROUP BY fa.id_riga
                ) fa 
                WHERE fa.codice_cliente = '.(empty($suprow['manufacturer']) ? 0 : $suprow['manufacturer']).'
                GROUP BY costruttore
                '.(isset($_POST['ordinaman']) ? 'ORDER BY '.$_POST['ordinaman'] : 'ORDER BY tot_acquisti DESC').'
            ');

            foreach ($ca['supsubcat'] as $supsubcatrow)
            {
                $progressivo_subsup += (100 * $supsubcatrow['tot_acquisti'] / $tot_fatturato);
                
                $riepilogo_html .= '
                    <tr class="invisible-table-row sup_subcat_'.$suprow['manufacturer'].'" style="display:none">
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;'.(empty($supsubcatrow['name']) ? '' : '<img src="../img/admin/add.gif" alt="Espandi" style="cursor:pointer" title="Espandi" id="sup_subcat_espandi_'.($supsubcatrow['costruttore'] > 0 ? $supsubcatrow['costruttore'] : 0).'-'.$suprow['manufacturer'].'" class="subcat_button_'.$suprow['manufacturer'].'" />').'
                        
                        <script type="text/javascript">
                            $(document).ready(function(){
                                $("#sup_subcat_espandi_'.$supsubcatrow['costruttore'].'-'.$suprow['manufacturer'].'").click(function(){
                                    $("tr[rel=\'sup_prodsubcat_'.$supsubcatrow['costruttore'].'-'.$suprow['manufacturer'].'\']").toggle();
                                    if($("tr[rel=\'sup_prodsubcat_'.$supsubcatrow['costruttore'].'-'.$suprow['manufacturer'].'\']:visible").length) {
                                        $(this).attr("src", "../img/admin/forbbiden.gif");	
                                    }
                                    else {
                                        $(this).attr("src", "../img/admin/add.gif");
                                    }
                                    return false;
                                });
                            });
                        </script>

                        </td>
                        <td>'.(empty($supsubcatrow['name']) ? ('Categoria sconosciuta') : $supsubcatrow['name']).'</td>
                        <td align="right">'.round($supsubcatrow['orderQty'],0).'</td>
                        <td align="right">'.Tools::displayPrice($supsubcatrow['tot_acquisti'], $currency).'</td>
                        <td align="right">'.((int)$tot_fatturato_sup ? number_format((100 * $supsubcatrow['tot_acquisti'] / ($tot_fatturato_sup)), 1, ',', ' ') : '0').'%</td>
                        <td align="right">'.number_format($progressivo_subsup, 1, ',', ' ').'%</td> 
                        <td align="right">'.Tools::displayPrice($supsubcatrow['tot_acquisti_iva'], $currency).'</td>
                    </tr>
                ';

                $ca['prodsupsubcat'] = Db::getInstance()->executeS('
                    SELECT fa.*,  sum(tot_acquisti1-tot_note) tot_acquisti, SUM(tot_acquisti_iva1-tot_note_iva) tot_acquisti_iva, sum(orderQty1) orderQty 
                    FROM (
                        SELECT
                            id_manufacturer,
                            codice_cliente,
                            id_riga,
                            fa.codice_articolo,
                            fa.descrizione_articolo,
                            SUM(fa.quantita) as orderQty1,
                            SUM(CASE WHEN classe_documento != 41 THEN fa.importo ELSE 0 END) as tot_acquisti1,
                            SUM(CASE WHEN tipo_documento = 720 or tipo_documento = 721 or tipo_documento = 723 THEN fa.importo ELSE 0 END) as tot_note,
                            SUM(CASE WHEN tipo_documento = 720 or tipo_documento = 721 or tipo_documento = 723 THEN (fa.importo + (fa.importo * fa.aliquota / 100)) ELSE 0 END) as tot_note_iva,
                            SUM(CASE WHEN classe_documento != 41 THEN (fa.importo + (fa.importo * fa.aliquota / 100)) ELSE 0 END) as tot_acquisti_iva1,
                            fa.id_category AS category, 
                            fa.id_manufacturer as manufacturer 
                        FROM '._DB_PREFIX_.'fatture_acquisto fa 
                        WHERE tipo_documento != 750 AND `data_registrazione` BETWEEN "'.$data_inizio.'" AND "'.$data_fine.'"
                        GROUP BY fa.id_riga
                    ) fa 
                    WHERE fa.id_manufacturer = '.($supsubcatrow['costruttore'] == 0 ? '0 AND fa.codice_cliente = "'.$suprow['manufacturer'].'"' : $supsubcatrow['costruttore'].' AND fa.codice_cliente = "'.$suprow['manufacturer'].'"').'
                    '.($supsubcatrow['manufacturer'] == 0 ? 'GROUP BY fa.id_riga' : 'GROUP BY fa.codice_articolo').'
                    '.(isset($_POST['ordinaman']) ? 'ORDER BY '.$_POST['ordinaman'] : 'ORDER BY tot_acquisti DESC').'
                ');

                foreach ($ca['prodsupsubcat'] as $prodsupsubcatrow)
                {
                    $progressivo_prodman += (100 * $prodsupsubcatrow['tot_acquisti'] / $tot_fatturato);

                    $riepilogo_html .= '
                        <tr rel="sup_prodsubcat_'.$supsubcatrow['costruttore'].'-'.$suprow['manufacturer'].'" class="invisible-table-row sup_prodsubcat_'.$suprow['manufacturer'].'" style="display:none">
                            <td></td>
                            <td><div style="margin-left:35px"><table style="width:345px; border:0px" class="table"><tr style="border:0px"><td style="border:0px; width:95px; text-align:left"><!-- <span style="cursor:pointer" class="span-reference" title="'.Product::showProductTooltip($prodsupsubcatrow['product']).'"><a href="index.php?controller=AdminCatalogExFeatures&id_product='.$prodsupsubcatrow['product'].'&updateproduct&token='.$tokenProducts.'" target="_blank"> --> '.(empty($prodsupsubcatrow['reference']) ?  $prodsupsubcatrow['codice_articolo'] : $prodsupsubcatrow['reference']).'<!-- </a></span> --></td><td style="border:0px; width:250px">'.(empty($prodsupsubcatrow['descrizione_articolo']) ? ('Descrizione articolo non presente') : $prodsupsubcatrow['descrizione_articolo']).'</td></tr></table></div></td>
                            <td align="right">'.round($prodsupsubcatrow['orderQty'],0).'</td>
                            <td align="right">'.Tools::displayPrice($prodsupsubcatrow['tot_acquisti'], $currency).'</td>
                            <td align="right">'.((int)$tot_fatturato_sup ? number_format((100 * $prodsupsubcatrow['tot_acquisti'] / ($tot_fatturato_sup)), 1, ',', ' ') : '0').'%</td>
                            <td align="right">'.number_format($progressivo_prodsup, 1, ',', ' ').'%</td> 
                            <td align="right">'.Tools::displayPrice($prodsupsubcatrow['tot_acquisti_iva'], $currency).'
                            </td>
                        </tr>
                    ';
                }
                
                $previous_subsup = $suprow['manufacturer'];
            }

            $sup_tot_conteggio += $suprow['orderQty'];
            $sup_tot_conteggio_p += (100 * $suprow['orderQty'] / ($this->t4+$this->t12));
            $sup_tot_vendite += $suprow['tot_acquisti'];
            $sup_tot_vendite_p += (100 * $suprow['tot_acquisti'] / $tot_fatturato_sup);
            $sup_tot_vendite_iva += $suprow['tot_acquisti_iva'];
            $count_sup++;
        }

        $riepilogo_html .= '</tbody>
            <tfoot>
            <tr style="background-color:#ebebeb">
                <td></td>
                <td><strong style="color:red">Totale</td>
                <td align="right"><strong style="color:red">'.$sup_tot_conteggio.'</strong></td>
                <td align="right"><strong style="color:red">'.Tools::displayPrice($sup_tot_vendite, $currency).'</strong></td>
                <td align="right"><strong style="color:red">100,0%</strong></td>
                <td align="right"><strong style="color:red">'.number_format($progressivo_sup, 1, ',', ' ').'%</strong></td>
                <td align="right"><strong style="color:red">'.Tools::displayPrice($sup_tot_vendite_iva, $currency).'</strong></td>		
            </tr>
            </tfoot>
        ';
								
		$riepilogo_html .= '</table></form><br /><br /><div style="clear:both"></div>
		    <p>
			<form action="" method="post">
			<br />
			<script type="text/javascript">
	            google.charts.load("current", {"packages":["corechart"]});
			  
				google.charts.setOnLoadCallback(drawChart3);
                // Callback that creates and populates a data table,
                // instantiates the pie chart, passes in the data and
                // draws it.
                function drawChart3() 
                {
                    // Create the data table.
                    var data = new google.visualization.DataTable();
                    data.addColumn("string", "Topping");
                    data.addColumn("number", "Slices");
                    data.addRows([
                    '.$manufacturer_data_chart_sup.'
                    ["Altri", '.$other_manufacturer_tot_sup.']
                    ]);

                    // Set chart options
                    var options = {"title":"Grafico fornitori",
                    is3D: true,
                    legend: "labeled",
                    pieSliceText: "none",
                    chartArea:{left:20,top:0,width:"97%",height:"97%"},
                    "width":850,
                    "height":500};

                    // Instantiate and draw our chart, passing in some options.
                    var chart = new google.visualization.PieChart(document.getElementById("chart_fornitori_div"));
                    chart.draw(data, options);
                    
                    // Instantiate and draw our chart, passing in some options.
                    var chart_div = document.getElementById("chart_fornitori_image_div");
                    var chart = new google.visualization.PieChart(document.getElementById("chart_fornitori_image_div"));
                    // Wait for the chart to finish drawing before calling the getImageURI() method.
                    google.visualization.events.addListener(chart, "ready", function () {
                        chart_div.innerHTML = "<img id=\'image_categorie\' style=\'display:none\' src=\'" + chart.getImageURI() + "\'>";
                        console.log(chart_div.innerHTML);
                    });

                    chart.draw(data, options);
			  	}
			</script>
            <div id="chart_fornitori_div"></div>
            <div id="chart_fornitori_image_div"></div>
            <br /><br />
            </form>	
            </p>
        ';
				
		$riepilogo_html .= '</div>';
		$riepilogo_html .= '</div>';
				
		$riepilogo_html .= '
            <script type="text/javascript">
            var tabber2 = new Yetii({
            id: "tab-container-cart",
            tabclass: "yetii-cart",
            });
            </script>
        ';

        // Se serve:
        // $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'override_stats_acquisti.js');

        $this->tpl_view_vars = array(
            'check_employee' => $check_employee,
            'riepilogo_html' => $riepilogo_html, // Variabile che sostituisce gli echo finchè non viene portato tutto in bootstrap
        );

        return parent::renderView();
    }
}
