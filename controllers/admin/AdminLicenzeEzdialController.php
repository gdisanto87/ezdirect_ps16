<?php

class AdminLicenzeEzdialControllerCore extends AdminController
{
    public function __construct()
    {
        $this->displayWarning('Usate tabelle di prova al posto del server ezdial (da eliminare finiti i test); sostituire Db::getInstance() con $this per usare l\'altro db?');
        $this->bootstrap = true;
        $this->lang = false;

        $this->context = Context::getContext();

        $this->display = 'view';

        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Licenze EzDial'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }

    public function renderView()
    {
		// CORREGGERE: cancellare le 3 tabelle ezdial finiti i test!
        if(Tools::getIsset('function'))
        {
            if(Tools::getValue('function') == 'crea') {
				
				if(Tools::getIsset('submit_crea'))
				{
					// decommentare, per ora uso db principale come test
					/*$this->connection = mysqli_connect('localhost','ezdirectezdial','zRv16^p1qwudsuh5(a');
					mysqli_select_db($this->connection,'ezdir_ezdial');*/

					if(is_numeric(trim(Tools::getValue('crea_licenza')))){
						$num = trim(Tools::getValue('crea_licenza'));
						for($i = 0; $i < $num; $i++)
						{
							$lii = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(16/strlen($x)) )),1,16);
							Db::getInstance()->execute('INSERT INTO ezdial_licenze VALUES (NULL, "'.$lii.'")'); // prova!
							// $this->execute('INSERT INTO ezdial_licenze VALUES (NULL, "'.$lii.'")'); // versione corretta con db esterno?
							
							$lii_string .= $lii.'<br />';
						}
					}
					else
						$lii_string = '';
                    
                    $this->tpl_view_vars = array(
                        'lii_string' => $lii_string,
                    );
				}
            }
            else if(Tools::getValue('function') == 'dissocia') {
				
				if(Tools::getIsset('submit_dissocia'))
				{
					// Decommentare quando ci sarà il server ezdial; TEST su db principale
					/*$connection = mysql_connect('localhost','ezdirectezdial','zRv16^p1qwudsuh5(a') or die('connection error');
					mysql_select_db('ezdir_ezdial',$connection);*/
					
					$macs = Db::getInstance()->executeS('SELECT * FROM ezdial_mac_address WHERE mac_address LIKE "%'.Tools::getValue('cerca_licenza').'%"');
					$lics = Db::getInstance()->executeS('SELECT * FROM ezdial_licenze WHERE licenza LIKE "%'.Tools::getValue('cerca_licenza').'%"');
					
					if(count($macs) > 0 || count($lics) > 0)
					{
						$table_ml .= '<table class="table"><thead><tr><th>Licenza</th><th>MAC Address</th><th>Svincola</th></tr></thead>';
						
						foreach($macs as &$mac)
						{
							$licenza_assegnata = Db::getInstance()->getValue('SELECT id_licenza FROM ezdial_mac_licenze WHERE id_mac_address = '.$mac['id_mac_address']);
							
							if($licenza_assegnata > 0)
							{
								$lic_mac = Db::getInstance()->getValue('SELECT licenza FROM ezdial_licenze WHERE id_licenza = '.$licenza_assegnata);
								$macli = $mac['mac_address'].'</td><td><input type="checkbox" name="svincola['.$licenza_assegnata.'_'.$mac['id_mac_address'].']" /></td>';
							}	
							else
							{
								$lic_mac = '<em>Nessuna licenza assegnata</em></td><td>';
								$macli = $mac['mac_address'].'</td><td></td>';
							}

                            // $mac['lic_mac_tpl'] = $lic_mac; // decommentare quando la table verrà fatta nel tpl
                            // $mac['macli_tpl'] = $macli; // decommentare quando la table verrà fatta nel tpl
							$table_ml .= '<tr><td>'.$lic_mac.'</td><td>'.$macli;
						}
						
						foreach($lics as &$lic)
						{
							$mac_assegnato = Db::getInstance()->getValue('SELECT id_mac_address FROM ezdial_mac_licenze WHERE id_licenza = '.$lic['id_licenza']);
							
							if($mac_assegnato > 0)
							{
								$mac_lic = Db::getInstance()->getValue('SELECT mac_address FROM ezdial_mac_address WHERE id_mac_address = '.$mac_assegnato);
								$mac_lic .= '</td><td><input type="checkbox" name="svincola['.$lic['id_licenza'].'_'.$mac_assegnato.']" />';
							}	
							else
								$mac_lic = '<em>Non assegnata</em></td><td>';
							
                            // $lic['mac_lic_tpl'] = $mac_lic; // decommentare quando la table verrà fatta nel tpl
							$table_ml .= '<tr><td>'.$lic['licenza'].'</td><td>'.$mac_lic.'</td></tr>';
						}
						
						$table_ml .= '</table><br /><input class="button" name="submit_dissocia_mac" value="Svincola licenze flaggate" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" type="submit" /></form><br /><br />';
						
						// Decommentare quando ci sarà il server ezdial; TEST su db principale
						/*$connection = mysql_connect(_DB_SERVER_,_DB_USER_,_DB_PASSWD_) or die('connection error');
						mysql_select_db(_DB_NAME_,$connection);*/
					}
					else
						$table_ml .= '<strong style="color:red; font-size:16px">La ricerca per "'.Tools::getValue('cerca_licenza').'" non ha prodotto risultati</strong><br /><br />';
				
                    $this->tpl_view_vars = array(
                        'macs' => $macs,
                        'lics' => $lics,
                        'table_ml' => $table_ml,
                    );
                }
				else if(Tools::getIsset('submit_dissocia_mac'))
				{
					// Decommentare quando ci sarà il server ezdial; TEST su db principale
					/*$connection = mysql_connect('localhost','ezdirectezdial','zRv16^p1qwudsuh5(a') or die('connection error');
					mysql_select_db('ezdir_ezdial',$connection);*/
					
					foreach($_POST['svincola'] as $key => $value)
					{
						$ids = explode('_',$key);
						Db::getInstance()->execute('DELETE FROM ezdial_mac_licenze WHERE id_licenza = '.$ids[0].' AND id_mac_address = '.$ids[1]);
						Db::getInstance()->execute('DELETE FROM ezdial_mac_address WHERE id_mac_address = '.$ids[1]);
					}
					
					// Decommentare quando ci sarà il server ezdial; TEST su db principale
					/*$connection = mysql_connect(_DB_SERVER_,_DB_USER_,_DB_PASSWD_) or die('connection error');
					mysql_select_db(_DB_NAME_,$connection);*/
				}
            }
            else if(Tools::getValue('function') == 'associa') {
                
				if(Tools::getIsset('submit_associa'))
				{
					// Decommentare quando ci sarà il server ezdial; TEST su db principale
					/*$connection = mysqli_connect('localhost','ezdirectezdial','zRv16^p1qwudsuh5(a') or die('connection error');
					mysqli_select_db($connection,'ezdir_ezdial');*/
				
					$lics = Db::getInstance()->executeS('SELECT * FROM ezdial_licenze WHERE licenza LIKE "%'.Tools::getValue('cerca_licenza').'%"');
					
					if(count($lics) > 0)
					{
						$table_ml .= '<table class="table"><thead><tr><th>Licenza</th><th>MAC Address</th></tr></thead>';
						
						foreach($lics as &$lic)
						{
							$mac_assegnato = Db::getInstance()->getValue('SELECT id_mac_address FROM ezdial_mac_licenze WHERE id_licenza = '.$lic['id_licenza']);
							if($mac_assegnato > 0)
								$mac_lic = Db::getInstance()->getValue('SELECT mac_address FROM ezdial_mac_address WHERE id_mac_address = '.$mac_assegnato);
							else
								$mac_lic = '<input name="inserisci_mac['.$lic['licenza'].']" type="text" id="inserisci_mac['.$lic['licenza'].']" onfocus="if(this.value==\'Inserisci MAC address...\'){this.value=\'\';};return false;" onblur="if(this.value==\'\'){this.value=\'Inserisci MAC address...\';};return false;" name="search_query" value="Inserisci MAC address..." autocomplete="off" style="width:350px">';
							
                            // $lic['mac_lic_tpl'] = $mac_lic; // decommentare quando la table verrà fatta nel tpl
							$table_ml .= '<tr><td>'.$lic['licenza'].'</td><td>'.$mac_lic.'</td></tr>';
						}
						
						$table_ml .= '</table><br /><input class="button" name="submit_associa_mac" value="Salva associazioni" type="submit" onclick="javascript: var surec=window.confirm(\'Sei sicuro?\'); if (surec) { return true; } else { return false; }" /></form><br /><br />';
						
						// Decommentare quando ci sarà il server ezdial; TEST su db principale
						/*$connection = mysqli_connect(_DB_SERVER_,_DB_USER_,_DB_PASSWD_);
						mysqli_select_db($connection,_DB_NAME_);*/
					}
					else
						$table_ml .= '<strong style="color:red; font-size:16px">La ricerca per "'.Tools::getValue('cerca_licenza').'" non ha prodotto risultati</strong><br /><br />';
					
                    $this->tpl_view_vars = array(
                        'lics' => $lics,
                        'table_ml' => $table_ml,
                    );
				}
				else if(Tools::getIsset('submit_associa_mac'))
				{
					// Decommentare quando ci sarà il server ezdial; TEST su db principale
					/*$connection = mysql_connect('localhost','ezdirectezdial','zRv16^p1qwudsuh5(a') or die('connection error');
					mysql_select_db('ezdir_ezdial',$connection);*/
					
					foreach($_POST['inserisci_mac'] as $key => $value)
					{
						if($value != '' && $value != 'Inserisci MAC address...') 
						{
							$id_licenza = Db::getInstance()->getValue('SELECT id_licenza FROM ezdial_licenze WHERE licenza = "'.$key.'"');
							$mac = Db::getInstance()->getValue('SELECT id_mac_address FROM ezdial_mac_address WHERE mac_address = "'.$value.'"');
							if($mac <= 0)
							{
								Db::getInstance()->execute('INSERT INTO ezdial_mac_address VALUES (NULL, "'.$value.'")');
								// $id_mac = mysqli_insert_id(); // non funziona
								$id_mac = Db::getInstance()->Insert_ID();
							}
							else
								$id_mac = $mac;
							
							Db::getInstance()->execute('INSERT INTO ezdial_mac_licenze VALUES ("'.$id_mac.'", "'.$id_licenza.'")');
						}
					}

					// Decommentare quando ci sarà il server ezdial; TEST su db principale
					/*$connection = mysql_connect(_DB_SERVER_,_DB_USER_,_DB_PASSWD_) or die('connection error');
					mysql_select_db(_DB_NAME_,$connection);*/
				}
            }
            else if(Tools::getValue('function') == 'cerca') {
				
				if(Tools::getIsset('submit_cerca'))
				{
					// Decommentare quando ci sarà il server ezdial; TEST su db principale
					/*$connection = mysqli_connect('localhost','ezdirectezdial','zRv16^p1qwudsuh5(a') or die('connection error');
					mysqli_select_db($connection,'ezdir_ezdial');*/
					
					$macs = Db::getInstance()->executeS('SELECT * FROM ezdial_mac_address WHERE mac_address LIKE "%'.Tools::getValue('cerca_licenza').'%"');
					$lics = Db::getInstance()->executeS('SELECT * FROM ezdial_licenze WHERE licenza LIKE "%'.Tools::getValue('cerca_licenza').'%"');
					
					if(count($macs) > 0 || count($lics) > 0)
					{
						$table_ml .= '<table class="table"><thead><tr><th>Licenza</th><th>MAC Address</th></tr></thead>';

						foreach($macs as &$mac)
						{
							$licenza_assegnata = Db::getInstance()->getValue('SELECT id_licenza FROM ezdial_mac_licenze WHERE id_mac_address = '.$mac['id_mac_address']);
							if($licenza_assegnata > 0)
								$lic_mac = Db::getInstance()->getValue('SELECT licenza FROM ezdial_licenze WHERE id_licenza = '.$licenza_assegnata);
							else
								$lic_mac = '<em>Nessuna licenza assegnata</em>';

                            // $mac['lic_mac_tpl'] = $lic_mac; // Decommentare quando la table verrà fatta nel tpl
							
							$table_ml .= '<tr><td>'.$lic_mac.'</td><td>'.$mac['mac_address'].'</td></tr>';
						}
						
						foreach($lics as &$lic)
						{
							$mac_assegnato = Db::getInstance()->getValue('SELECT id_mac_address FROM ezdial_mac_licenze WHERE id_licenza = '.$lic['id_licenza']);
							if($mac_assegnato > 0)
								$mac_lic = Db::getInstance()->getValue('SELECT mac_address FROM ezdial_mac_address WHERE id_mac_address = '.$mac_assegnato);
							else
								$mac_lic = '<em>Non assegnata</em>';

                            // $lic['mac_lic_tpl'] = $mac_lic; // Decommentare quando la table verrà fatta nel tpl
							
							$table_ml .= '<tr><td>'.$lic['licenza'].'</td><td>'.$mac_lic.'</td></tr>';
						}
						
						$table_ml .= '</table>';
						
						// Decommentare quando ci sarà il server ezdial; TEST su db principale
						/*$connection = mysqli_connect(_DB_SERVER_,_DB_USER_,_DB_PASSWD_);
						mysqli_select_db($connection,_DB_NAME_);*/
					}
					else
						$table_ml = '<strong style="color:red; font-size:16px">La ricerca per "'.Tools::getValue('cerca_licenza').'" non ha prodotto risultati</strong>';
					
                    $this->tpl_view_vars = array(
                        'macs' => $macs,
                        'lics' => $lics,
                        'table_ml' => $table_ml,
                    );
				}
            }
        }

        return parent::renderView();
    }
}