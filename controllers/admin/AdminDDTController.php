<?php

// Potrei togliere Core e trasformare in override
class AdminNoteConsegnaControllerCore extends AdminController
{
    public function __construct()
    {
        $this->displayWarning('WARNING: Page under construction!');
        $this->displayInformation('FUNZIONAMENTO DA VERIFICARE PRIMA DI MODIFICARE IL CONTROLLER; POTREBBE ESSERE ELIMINATO');
        
        $this->bootstrap = true;
        $this->lang = false;

        $this->default_form_language = $this->context->language->id;
        
        parent::__construct();
    }
}