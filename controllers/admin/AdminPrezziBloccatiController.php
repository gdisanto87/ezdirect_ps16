<?php

class AdminPrezziBloccatiControllerCore extends AdminController
{
    protected $_defaultOrderBy = 'm!name';
    protected $_defaultOrderWay = 'ASC';

    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'product';
        $this->className = 'Product';
        $this->lang = false;
        $this->explicitSelect = true;

        $this->allow_export = true;

        $this->context = Context::getContext();
        $this->context->currency = Currency::getDefaultCurrency();

        $this->_select = 'pl.name, m.name';
		
        $this->_join = '
            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (a.`id_product` = pl.`id_product`)
            LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (a.`id_manufacturer` = m.`id_manufacturer`)
        ';

        $this->_where = '
            AND pl.id_lang = '.(int)$this->context->language->id.'
            AND a.blocco_prezzi = 1
        ';

        $this->_use_found_rows = true;

        $this->fields_list = array(
            'id_product' => array(
                'title' => $this->l('ID Prodotto'),  
                'align' => 'center'
            ),
            'reference' => array(
                'title' => $this->l('Codice Prodotto'), 
                'align' => 'center',
                'callback' => 'creaTooltip',
                'remove_onclick' => true
            ),
            'nome_prodotto' => array(
                'title' => $this->l('Descrizione Prodotto'),
                'align' => 'center', 
                'filter_key' => 'pl!name'
            ),
            'costruttore' => array(
                'title' => $this->l('Costruttore'),
                'align' => 'center', 
                'filter_key' => 'm!name' 
            ),
            'stock_quantity' => array(
                'title' => $this->l('Mag. EZ'),
                'align' => 'center',
            ),
            'listino' => array(
                'title' => $this->l('Listino'),
                'align' => 'text-right',
                'class' => 'fixed-width-xs',
                'callback' => 'format_prezzo'
            ),
            'wholesale_price' => array(
                'title' => $this->l('Acquisto'),
                'align' => 'text-right',
                'class' => 'fixed-width-xs',
                'callback' => 'format_prezzo'
            ),
            'price' => array(
                'title' => $this->l('Prezzo web'),
                'align' => 'text-right',
                'class' => 'fixed-width-xs',
                'callback' => 'format_prezzo'
            )
        );

        $this->addRowAction('delete');
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?'),
                'icon' => 'icon-trash'
            )
        );

        $this->redirect_after = ' '; // necessario per function redirect()
        
        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Prezzi Bloccati'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }
    
    public function initToolbar()
    {
        parent::initToolbar();

        unset($this->toolbar_btn['new']); // Toglie il pulsante ADD NEW dalla toolbar
    }

    protected function redirect()
    {
        // action = edit
        if(Tools::getIsset('id_product') && Tools::getIsset('updateproduct')){
            $this->currentIndex = 'AdminProducts';
            $this->token = Tools::getAdminTokenLite('AdminProducts');
            $this->redirect_after = '/ezadmin/index.php?controller='.$this->currentIndex.'&id_product='.Tools::getValue('id_product').'&updateproduct&token='.$this->token;
            Tools::redirectAdmin($this->redirect_after);
        }
        // Se action = delete, non elimino il prodotto, ma setto blocco_prezzi = 0
        else if(Tools::getIsset('id_product') && Tools::getIsset('deleteproduct')){
            Db::getInstance()->execute('UPDATE '._DB_PREFIX_.'product SET blocco_prezzi = 0 WHERE id_product = '.Tools::getValue('id_product'));            
            Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
        }
    }

    // spostare in una classe / file di utilità ed usarla dichiarandola una sola volta
    public function format_prezzo($value)
	{
        // CON SIMBOLO EURO E . PER I DECIMALI
        /* $prezzo = (float)(str_replace(",",".",$value));
        $prezzo = Tools::displayPrice($prezzo, $this->context->currency); */

        $prezzo = number_format($value, 2, ',', '.');

        return '<span>'.$prezzo.'</span>';
    }

    public function creaTooltip($value, $tr)
	{
        $tokenProducts = Tools::getAdminTokenLite('AdminProducts');
        $href_prodotto = '/ezadmin/index.php?controller=AdminProducts&id_product='.$tr['id_product'].'&updateproduct&token='.$tokenProducts;
        return '<a class="tooltip_prodotto" title="'.Product::showProductTooltip($tr['id_product']).'" href="'.$href_prodotto.'" target="_blank">'.$value.'</a>';
    }
}