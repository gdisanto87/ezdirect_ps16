<?php

class AdminValorizzazioneMagazzinoControllerCore extends AdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->lang = false;

        $this->context = Context::getContext();

        $this->display = 'view';

        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Valorizzazione Magazzino'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }

    public function renderView()
    {
        if(isset($_POST['vai_valorizzazione'])) 
        {
            if(Tools::getIsset('marchi'))
                $marchi = Tools::getValue('marchi');
            else
            {
                foreach ($_POST['per_marchio'] as $marchio) 
                {
                    if($marchio == 'tutto' || $marchio == 'logistica_amazon' || $marchio == 'logistica_amazon_tutto') {
                        $marchi .= "p.id_manufacturer > 0 OR ";
                    }
                    else
                    {
                        $nomemarchio = Db::getInstance()->getValue("SELECT name FROM "._DB_PREFIX_."manufacturer WHERE id_manufacturer = ".$marchio);
                        
                        $strisciamarchi .= "- ".$nomemarchio."<br />";
                        $marchiinclusi .= $nomemarchio."; ";
                        $marchi .= "p.id_manufacturer = $marchio OR ";
                        $marchi2 .= "od.manufacturer_id = $marchio OR ";
                    }
                }	
            }

            if(Tools::getIsset('querysql')) {
                $querysql = $_POST['querysql'];
            }
            else
            {
                $querysql = '
                    SELECT 
                        m.id_manufacturer AS manufacturer, 
                        '.($marchio == 'logistica_amazon' ? 'SUM(p.wholesale_price * p.impegnato_amazon)' : ($marchio == 'tutto' ?  'SUM(p.wholesale_price*p.stock_quantity)' : 'SUM(p.wholesale_price*p.stock_quantity) + SUM(p.wholesale_price*p.impegnato_amazon) ')).' as valore, 
                        SUM(p.ordinato_quantity*p.wholesale_price) as valore_ordinato,
                        '.($marchio == 'logistica_amazon' ? 'SUM(p.impegnato_amazon)' : ($marchio == 'tutto' ? 'SUM(p.stock_quantity)' : 'SUM(p.stock_quantity) + SUM(p.impegnato_amazon)')).' AS stock, "--" as prezzo_acquisto, 
                        SUM(p.impegnato_quantity) as impegnato, 
                        SUM(p.ordinato_quantity) AS ordinato, 
                        '.($marchio == 'logistica_amazon_tutto' ? 'SUM(p.stock_quantity) ez, SUM(p.impegnato_amazon) amz,' : '').'
                        m.name AS costruttore
                    FROM `'._DB_PREFIX_.'product` p
                    LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (p.id_manufacturer = m.id_manufacturer)
                    '.((int)$this->context->cookie->stats_id_zone ? $join : '').'
                    WHERE 
                        '.($marchio == 'logistica_amazon' ? 'p.impegnato_amazon > 0' : ($marchio == 'tutto' ? 'p.stock_quantity > 0' : '(p.stock_quantity > 0 OR p.impegnato_amazon > 0)')).'
                        AND p.stock_quantity < 9999
                        AND ('.$marchi.' p.id_manufacturer = 9999999999999999999999)
                    GROUP BY m.id_manufacturer
                ';
            }
            
            $sql = $querysql.''.(isset($_POST['ordinaman']) ? 'ORDER BY '.$_POST['ordinaman'] : 'ORDER BY valore DESC');
            
            $tot_stock = 0;
            $tot_valore = 0;
            $tot_stock_netto = 0;
            $tot_valore_netto = 0;
            $tot_impegnato = 0;
            $tot_ordinato = 0;
            $progr_tot = 0;
            $tot_ez = 0;
            $tot_amz = 0;
            
            $ca['man'] = Db::getInstance()->executeS(str_replace('\\','',$sql));
            
            $totale = 0;
            
            foreach($ca['man'] as $manrow)
                $totale += $manrow['valore']; 
            
            $riepilogo_html .= "<form method='post' action=''>";
        
            $riepilogo_html .= "<input type='hidden' name='querysql' value='".htmlentities((isset($_POST['querysql']) ? $_POST['querysql'] : $querysql),ENT_QUOTES)."' />";
            $riepilogo_html .= "<input type='hidden' name='vai-valorizzazione' value='y' />";
            $riepilogo_html .= "<input type='hidden' name='marchi' value='".$marchi."' />";
            
            $riepilogo_html .= "<strong>NB: le licenze e i prodotti scaricabili NON sono calcolati in questa tabella in quanto valorizzati a quantit&agrave; 9999 perch&eacute; devono rimanere sempre disponibili.</strong><br /><br />";
            $riepilogo_html .= " I valori sono calcolati in base al prezzo &ldquo;standard&rdquo; di acquisto. Non si tiene conto dei prezzi speciali, rebate, premi, sconti extra del fornitore. Nel caso dei prodotti importanti sono al lordo dei costi di trasporto. Al netto dell'impegnato = al netto degli ordini aperti (ancora da consegnare o spedire ai clienti).<br /><br />";

            $riepilogo_html .= (Tools::getIsset('al_netto') ? 'Hai selezionato: <strong>Quantit&agrave; e valore al netto dell\'impegnato</strong><br /><br />' : '');
            $riepilogo_html .= (Tools::getIsset('ordinato') ? 'Hai selezionato: <strong>Vedi anche ordinato a fornitore</strong><br /><br />' : '');
            
            // Correggere: applicando la classe tabella_datatable non funziona più il resto del js
            $riepilogo_html .= '
            <table class="table float" id="valorizzazione_table">
            <thead><tr>
            <th title="Costruttore" style="width:200px">Costruttore</th>
            <th>Prezzo acquisto
            
            </th>
            <th>'.($marchio == 'logistica_amazon' ? 'Rim. Amazon' : ($marchio == 'tutto' ? 'Qta Mag. EZ' : 'Qta Tot.')).' <br /></th>
            
            '.(Tools::getIsset('al_netto') ? '<th ><span class="span-reference" title="Quantita al netto dell\'impegnato dai clienti">Al netto impegnato</span></th>' : '').'
            
            '.(Tools::getIsset('ordinato') ? '<th ><span class="span-reference" title="Quantita dell\'ordinato a fornitore">Qta ordinato</span></th>' : '').'
            
            '.($marchio == 'logistica_amazon_tutto' ? '<th>Mag. Ez</th><th>Mag. Amz</th>' : '').'
            
            
            <th>Valore </th>
            '.(Tools::getIsset('al_netto') ? '<th ><span class="span-reference" title="Valore al netto dell\'impegnato">Al netto imp.</span></th>' : '').'
            
            '.(Tools::getIsset('ordinato') ? '<th ><span class="span-reference" title="Valore dell\'ordinato a fornitore">Valore ordinato</span></th>' : '').'
            
            <th>% tot.</th>
            
            <th>% progr.</th>
            <!-- <th>Tot. Impegnato <br /> <input type="submit" name="ordinaman" style="width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/up.gif)" value="impegnato ASC" /></a>   
            <input type="submit" name="ordinaman" style="width:9px; border:0px; background:transparent; color:transparent; cursor:pointer; height:9px; background-image:url(../img/admin/down.gif)" value="impegnato DESC" /></a> </th> -->
            <th ><span class="span-reference" title="Ordinato al fornitore">Ord a f.</span></th>
            </tr></thead><tbody><tr></tr>';
            $progr_tot_mansubcat = 0;
            $progr_tot_prodmansubcat = 0;
            
            foreach($ca['man'] as $manrow)
            {
                if(Tools::getIsset('al_netto'))
                {
                    $qta_ord_clienti = Db::getInstance()->getValue('SELECT SUM(somma) FROM (SELECT od.manufacturer_id, (product_quantity - (REPLACE(cons,"0_",""))) somma FROM order_detail od JOIN orders o ON o.id_order = od.id_order JOIN cart c ON c.id_cart = o.id_cart
                    JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE  moh.id_order_state != 18 AND moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state) WHERE c.name NOT LIKE "%prime%" AND od.manufacturer_id = '.$manrow['manufacturer'].' AND os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state != 20 AND os.id_order_state != 26  AND os.id_order_state != 29 AND os.id_order_state != 30 AND os.id_order_state != 32 AND os.id_order_state != 35 GROUP BY od.product_id) x GROUP by x.manufacturer_id');
                        
                    $valore_ord_clienti = 	Db::getInstance()->getValue('SELECT SUM(valore) FROM (SELECT od.manufacturer_id, wholesale_price valore FROM order_detail od JOIN orders o ON o.id_order = od.id_order 
                    JOIN order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM order_history moh WHERE moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN order_state os ON (os.id_order_state = oh.id_order_state) WHERE  od.manufacturer_id = '.$manrow['manufacturer'].' AND os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state != 20 AND os.id_order_state != 26  AND os.id_order_state != 29 AND os.id_order_state != 30 AND os.id_order_state != 32 AND os.id_order_state != 35 GROUP BY od.product_id) x GROUP by x.manufacturer_id');
                }	
                $riepilogo_html .= '
                    <script type="text/javascript">
                            $(document).ready(function(){
                                $("#man_espandi_'.$manrow['manufacturer'].'").click(function(){
                                    $("tr.man_subcat_'.$manrow['manufacturer'].'").toggle();
                                    if($("tr.man_subcat_'.$manrow['manufacturer'].':visible").length) {
                                        $(this).attr("src", "../img/admin/forbbiden.gif");	
                                    }
                                    else {
                                        $(this).attr("src", "../img/admin/add.gif");
                                        $(".subcat_button_'.$manrow['manufacturer'].'").attr("src", "../img/admin/add.gif");
                                        $("tr.man_prodsubcat_'.$manrow['manufacturer'].'").hide();
                                    }
                                    return false;
                                });
                            });
                    </script>
                ';
                
                $percent = ((100 * $manrow['valore']) / $totale);
                $progr_tot += $percent;
                
                $riepilogo_html .= '<tr>';
                $riepilogo_html .= '<td data-sort="'.$manrow['costruttore'].'"><img src="../img/admin/add.gif" alt="Espandi" style="cursor:pointer" title="Espandi" id="man_espandi_'.$manrow['manufacturer'].'" /> <strong>'.($manrow['costruttore'] == '' ? 'Prodotti senza marchio' : $manrow['costruttore']).'</strong></td>';
                $riepilogo_html .= '<td>'.$manrow['prezzo_acquisto'].'</td>';
                $riepilogo_html .= '<td style="text-align:right">'.$manrow['stock'].'</td>';
                
                if(Tools::getIsset('al_netto'))
                    $riepilogo_html .= '<td style="text-align:right">'.($manrow['stock'] - $qta_ord_clienti).'</td>';
                
                if(Tools::getIsset('ordinato'))
                    $riepilogo_html .= '<td style="text-align:right">'.($manrow['ordinato']).'</td>';
                
                $riepilogo_html .= ($marchio == 'logistica_amazon_tutto' ? '<td style="text-align:right">'.$manrow['ez'].'</td><td style="text-align:right">'.$manrow['amz'].'</td>' : '');
                $riepilogo_html .= '<td style="text-align:right">'.number_format($manrow['valore'],2,",","").'</td>';
                
                if(Tools::getIsset('al_netto'))
                    $riepilogo_html .= '<td style="text-align:right">'.number_format(($manrow['valore'] - $valore_ord_clienti),2,",","").'</td>';
                
                if(Tools::getIsset('ordinato'))
                    $riepilogo_html .= '<td style="text-align:right">'.($manrow['valore_ordinato']).'</td>';
                
                $riepilogo_html .= '<td style="text-align:right">'.number_format($percent,2,",","").'%</td>';
                $riepilogo_html .= '<td style="text-align:right">'.number_format($progr_tot,2,",","").'%</td>';
                //	$riepilogo_html .= '<td style="text-align:right">'.$manrow['impegnato'].'</td>';
                $riepilogo_html .= '<td style="text-align:right">'.$manrow['ordinato'].'</td>';
                $riepilogo_html .= '</tr>';
                
                $tot_stock += $manrow['stock'];
                $tot_ordinato += $manrow['ordinato'];
                $tot_stock_netto += ($manrow['stock'] - $qta_ord_clienti);
                $tot_valore += $manrow['valore'];
                $tot_valore_netto += ($manrow['valore'] - $valore_ord_clienti);
                $tot_valore_ordinato += ($manrow['valore_ordinato']);
                $tot_impegnato += $manrow['impegnato'];
                $tot_ordinato += $manrow['ordinato'];
                
                if($marchio == 'logistica_amazon_tutto')
                {
                    $tot_ez += $manrow['ez'];
                    $tot_amz += $manrow['amz'];
                }
                
                $ca['mansubcat'] = Db::getInstance()->executeS('
                    SELECT 
                        '.($marchio == 'logistica_amazon' ? 'SUM(p.wholesale_price * p.impegnato_amazon)' : ($marchio == 'tutto' ?  'SUM(p.wholesale_price*p.stock_quantity)' : 'SUM(p.wholesale_price*p.stock_quantity) + SUM(p.wholesale_price*p.impegnato_amazon) ')).' as valore,  '.($marchio == 'logistica_amazon' ? 'SUM(p.impegnato_amazon)' : ($marchio == 'tutto' ? 'SUM(p.stock_quantity)' : 'SUM(p.stock_quantity) + SUM(p.impegnato_amazon)')).' AS stock, "--" as prezzo_acquisto, SUM(p.impegnato_quantity) as impegnato, SUM(p.ordinato_quantity) AS ordinato,
                        SUM(p.wholesale_price*p.ordinato_quantity) AS valore_ordinato,
                        '.($marchio == 'logistica_amazon_tutto' ? 'SUM(p.stock_quantity) ez, SUM(p.impegnato_amazon) amz,' : '').'
                        cl.name, c.id_category as category
                    FROM `'._DB_PREFIX_.'product` p 
                    LEFT JOIN `'._DB_PREFIX_.'category` c ON c.id_category = p.id_category_default
                    LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (c.id_category = cl.id_category AND cl.id_lang = '.$this->context->language->id.')
                    WHERE 
                        '.($marchio == 'logistica_amazon' ? 'p.impegnato_amazon > 0' : ($marchio == 'tutto' ? 'p.stock_quantity > 0' : '(p.stock_quantity > 0 OR p.impegnato_amazon > 0)')).' AND p.stock_quantity < 9999
                        AND p.stock_quantity < 9999
                        AND p.id_manufacturer = '.(empty($manrow['costruttore']) ? 0 : $manrow['manufacturer']).'
                    GROUP BY category 
                    '.(isset($_POST['ordinaman']) ? 'ORDER BY '.$_POST['ordinaman'] : 'ORDER BY valore DESC').'
                ');                
                                    
                foreach ($ca['mansubcat'] as $mansubcatrow)
                {
                    if(Tools::getIsset('al_netto'))
                    {
                        $qta_ord_clienti = Db::getInstance()->getValue('SELECT SUM(somma) FROM (SELECT od.manufacturer_id, (product_quantity - (REPLACE(cons,"0_",""))) somma FROM '._DB_PREFIX_.'order_detail od JOIN '._DB_PREFIX_.'orders o ON o.id_order = od.id_order 
                            JOIN '._DB_PREFIX_.'order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM '._DB_PREFIX_.'order_history moh WHERE moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN '._DB_PREFIX_.'order_state os ON (os.id_order_state = oh.id_order_state) WHERE  od.manufacturer_id = '.$manrow['manufacturer'].' AND od.category_id = '.$mansubcatrow['category'].' AND os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state != 20 AND os.id_order_state != 26  AND os.id_order_state != 29 AND os.id_order_state != 30 AND os.id_order_state != 35 AND os.id_order_state != 32 GROUP BY od.product_id) x GROUP by x.manufacturer_id
                        ');
                        
                        $valore_ord_clienti = 	Db::getInstance()->getValue('SELECT SUM(valore) FROM (SELECT od.manufacturer_id, wholesale_price valore FROM '._DB_PREFIX_.'order_detail od JOIN '._DB_PREFIX_.'orders o ON o.id_order = od.id_order 
                            JOIN '._DB_PREFIX_.'order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM '._DB_PREFIX_.'order_history moh WHERE moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN '._DB_PREFIX_.'order_state os ON (os.id_order_state = oh.id_order_state) WHERE  od.manufacturer_id = '.$manrow['manufacturer'].' AND od.category_id = '.$mansubcatrow['category'].' AND os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state != 20 AND os.id_order_state != 26  AND os.id_order_state != 29 AND os.id_order_state != 30 AND os.id_order_state != 32 AND os.id_order_state != 35 GROUP BY od.product_id) x GROUP by x.manufacturer_id
                        ');
                    }
                    
                    $percent_mansubcat = ((100 * $mansubcatrow['valore']) / $totale);
                    $progr_tot_mansubcat += $percent_mansubcat;
                
                    $riepilogo_html .= '<tr class="invisible-table-row man_subcat_'.$manrow['manufacturer'].'" style="display:none">';
                    $riepilogo_html .= '
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;<img src="../img/admin/add.gif" alt="Espandi" style="cursor:pointer" title="Espandi" id="man_subcat_espandi_'.$mansubcatrow['category'].'-'.$manrow['manufacturer'].'" class="subcat_button_'.$manrow['manufacturer'].'" />'.(empty($mansubcatrow['category']) ? 'Sconosciuto' : $mansubcatrow['name']).'
                        <script type="text/javascript">
                            $(document).ready(function(){
                                $("#man_subcat_espandi_'.$mansubcatrow['category'].'-'.$manrow['manufacturer'].'").click(function(){
                                    $("tr[rel=\'man_prodsubcat_'.$mansubcatrow['category'].'-'.$manrow['manufacturer'].'\']").toggle();
                                    if($("tr[rel=\'man_prodsubcat_'.$mansubcatrow['category'].'-'.$manrow['manufacturer'].'\']:visible").length) {
                                        $(this).attr("src", "../img/admin/forbbiden.gif");	
                                    }
                                    else {
                                        $(this).attr("src", "../img/admin/add.gif");
                                    }
                                    return false;
                                });
                            });
                        </script>
                        </td>
                    ';

                    $riepilogo_html .= '<td>'.$mansubcatrow['prezzo_acquisto'].'</td>';
                    $riepilogo_html .= '<td style="text-align:right">'.$mansubcatrow['stock'].'</td>';
                    
                    if(Tools::getIsset('al_netto'))
                        $riepilogo_html .= '<td style="text-align:right">'.($mansubcatrow['stock'] - $qta_ord_clienti).'</td>';
                    
                    if(Tools::getIsset('ordinato'))
                        $riepilogo_html .= '<td style="text-align:right">'.($mansubcatrow['ordinato']).'</td>';
                    
                    $riepilogo_html .= ($marchio == 'logistica_amazon_tutto' ? '<td style="text-align:right">'.$mansubcatrow['ez'].'</td><td style="text-align:right">'.$mansubcatrow['amz'].'</td>' : '');
                    $riepilogo_html .= '<td style="text-align:right">'.number_format($mansubcatrow['valore'],2,",","").'</td>';
                    
                    if(Tools::getIsset('al_netto'))
                        $riepilogo_html .= '<td style="text-align:right">'.number_format(($mansubcatrow['valore'] - $valore_ord_clienti),2,",","").'</td>';
                    
                    if(Tools::getIsset('ordinato'))
                        $riepilogo_html .= '<td style="text-align:right">'.($mansubcatrow['valore_ordinato']).'</td>';
                    
                    $riepilogo_html .= '<td style="text-align:right">'.number_format($percent_mansubcat,2,",","").'%</td>';
                    $riepilogo_html .= '<td style="text-align:right">'.number_format($progr_tot_mansubcat,2,",","").'%</td>';
                    //$riepilogo_html .= '<td style="text-align:right">'.$mansubcatrow['impegnato'].'</td>';
                    $riepilogo_html .= '<td style="text-align:right">'.$mansubcatrow['ordinato'].'</td>';
                    $riepilogo_html .= '</tr>';
                    
                    $ca['prodmansubcat'] = Db::getInstance()->executeS('
                        SELECT 
                            '.($marchio == 'logistica_amazon' ? 'SUM(p.wholesale_price*p.impegnato_amazon)' : ($marchio == 'tutto' ?  'SUM(p.wholesale_price*p.stock_quantity)' : 'SUM(p.wholesale_price*p.stock_quantity) + SUM(p.wholesale_price*p.impegnato_amazon) ')).' as valore,  '.($marchio == 'logistica_amazon' ? 'SUM(p.impegnato_amazon)' : ($marchio == 'tutto' ? 'SUM(p.stock_quantity)' : 'SUM(p.stock_quantity) + SUM(p.impegnato_amazon)')).' AS stock, p.wholesale_price as prezzo_acquisto, SUM(p.impegnato_quantity) as impegnato, SUM(p.ordinato_quantity) AS ordinato,
                            SUM(p.wholesale_price*p.ordinato_quantity) AS valore_ordinato,
                            '.($marchio == 'logistica_amazon_tutto' ? 'SUM(p.stock_quantity) ez, SUM(p.impegnato_amazon) amz,' : '').'
                            pl.name AS name,
                            p.reference AS reference,
                            p.id_product as product
                        FROM `'._DB_PREFIX_.'product` p
                        LEFT JOIN `'._DB_PREFIX_.'category` c ON c.id_category = p.id_category_default
                        LEFT JOIN  `'._DB_PREFIX_.'product_lang` pl ON (p.id_product = pl.id_product AND pl.id_lang = '.$this->context->language->id.')  
                        WHERE 
                            '.($marchio == 'logistica_amazon' ? 'p.impegnato_amazon > 0' : ($marchio == 'tutto' ? 'p.stock_quantity > 0' : '(p.stock_quantity > 0 OR p.impegnato_amazon > 0)')).' AND p.stock_quantity < 9999
                            AND p.stock_quantity > 0'.'
                            AND p.stock_quantity < 9999
                            AND p.id_category_default = '.$mansubcatrow['category'].' 
                            AND p.id_manufacturer = '.(empty($manrow['costruttore']) ? 0 : $manrow['manufacturer']).' 
                        GROUP BY p.id_product 
                        '.(isset($_POST['ordinaman']) ? 'ORDER BY '.$_POST['ordinaman'] : 'ORDER BY valore DESC').'
                    ');
                                            
                    foreach ($ca['prodmansubcat'] as $prodmansubcatrow)
                    {
                        if(Tools::getIsset('al_netto'))
                        {
                            $qta_ord_clienti = Db::getInstance()->getValue('SELECT SUM(somma) FROM (SELECT od.manufacturer_id, (product_quantity - (REPLACE(cons,"0_",""))) somma FROM '._DB_PREFIX_.'order_detail od JOIN '._DB_PREFIX_.'orders o ON o.id_order = od.id_order 
                                JOIN '._DB_PREFIX_.'order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM '._DB_PREFIX_.'order_history moh WHERE moh.id_order_state != 18 AND moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN '._DB_PREFIX_.'order_state os ON (os.id_order_state = oh.id_order_state) WHERE  od.product_id = '.$prodmansubcatrow['product'].' AND os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state != 20 AND os.id_order_state != 26  AND os.id_order_state != 29 AND os.id_order_state != 30 AND os.id_order_state != 32 AND os.id_order_state != 35 GROUP BY od.product_id) x GROUP by x.manufacturer_id
                            ');
                            
                            $valore_ord_clienti = 	Db::getInstance()->getValue('SELECT SUM(valore) FROM (SELECT od.manufacturer_id, wholesale_price valore FROM '._DB_PREFIX_.'order_detail od JOIN '._DB_PREFIX_.'orders o ON o.id_order = od.id_order 
                                JOIN '._DB_PREFIX_.'order_history oh ON (oh.id_order_history = (SELECT MAX(id_order_history) FROM '._DB_PREFIX_.'order_history moh WHERE moh.id_order_state != 27 AND moh.id_order_state != 28 AND moh.id_order = od.id_order GROUP BY moh.id_order)) JOIN '._DB_PREFIX_.'order_state os ON (os.id_order_state = oh.id_order_state) WHERE od.product_id = '.$prodmansubcatrow['product'].' AND os.id_order_state != 4 AND os.id_order_state != 5 AND os.id_order_state != 6 AND os.id_order_state != 7 AND os.id_order_state != 8 AND os.id_order_state != 14 AND os.id_order_state != 16  AND os.id_order_state != 20 AND os.id_order_state != 26  AND os.id_order_state != 29 AND os.id_order_state != 30 AND os.id_order_state != 32 AND os.id_order_state != 35 GROUP BY od.product_id) x GROUP by x.manufacturer_id
                            ');
                        }
                        
                        $percent_prodmansubcat = ((100 * $prodmansubcatrow['valore']) / $totale);
                        $progr_tot_prodmansubcat += $percent_prodmansubcat;

                        $tokenProducts = Tools::getAdminTokenLite('AdminProducts');
                        
                        $riepilogo_html .= '<tr rel="man_prodsubcat_'.$mansubcatrow['category'].'-'.$manrow['manufacturer'].'" class="invisible-table-row man_prodsubcat_'.$manrow['manufacturer'].'" style="display:none">
                        <td><div style="margin-left:35px"><table style="width:200px; border:0px" class="table"><tr style="border:0px"><td style="border:0px; width:95px; text-align:left"><a href="index.php?controller=AdminProducts&id_product='.$prodmansubcatrow['product'].'&updateproduct&token='.$tokenProducts.'" target="_blank">'.(empty($prodmansubcatrow['reference']) ? '' : $prodmansubcatrow['reference']).'</a></td><td style="border:0px; width:250px">'.(empty($prodmansubcatrow['name']) ? 'Sconosciuto' : $prodmansubcatrow['name']).'</td></tr></table></div></td>';
                        
                        $riepilogo_html .= '<td style="text-align:right">'.number_format($prodmansubcatrow['prezzo_acquisto'],2,",","").'</td>';
                        
                        $riepilogo_html .= '<td style="text-align:right">'.$prodmansubcatrow['stock'].'</td>';
                        
                        if(Tools::getIsset('al_netto'))
                            $riepilogo_html .= '<td style="text-align:right">'.($prodmansubcatrow['stock'] - $qta_ord_clienti).'</td>';
                        
                        if(Tools::getIsset('ordinato'))
                            $riepilogo_html .= '<td style="text-align:right">'.($prodmansubcatrow['ordinato']).'</td>';
                        
                        $riepilogo_html .= ($marchio == 'logistica_amazon_tutto' ? '<td style="text-align:right">'.$prodmansubcatrow['ez'].'</td><td style="text-align:right">'.$prodmansubcatrow['amz'].'</td>' : '');
                        $riepilogo_html .= '<td style="text-align:right">'.number_format($prodmansubcatrow['valore'],2,",","").'</td>';
                        
                        if(Tools::getIsset('al_netto'))
                            $riepilogo_html .= '<td style="text-align:right">'.number_format(($prodmansubcatrow['valore'] - $valore_ord_clienti),2,",","").'</td>';
                        
                        if(Tools::getIsset('ordinato'))
                            $riepilogo_html .= '<td style="text-align:right">'.($prodmansubcatrow['valore_ordinato']).'</td>';
                        
                        $riepilogo_html .= '<td style="text-align:right">'.number_format($percent_prodmansubcat,2,",","").'%</td>';
                        $riepilogo_html .= '<td style="text-align:right">'.number_format($progr_tot_prodmansubcat,2,",","").'%</td>';
                        //$riepilogo_html .= '<td style="text-align:right">'.$prodmansubcatrow['impegnato'].'</td>';
                        $riepilogo_html .= '<td style="text-align:right">'.$prodmansubcatrow['ordinato'].'</td>';
                        $riepilogo_html .= '</tr>';	
                    }
                }
            }
				
            $riepilogo_html .= '
                <tr><td>Totale</td><td>--</td><td style="text-align:right">'.$tot_stock.'</td>'.(Tools::getIsset('al_netto') ? '<td style="text-align:right">'.$tot_stock_netto.'</td>' : '')
                .(Tools::getIsset('ordinato') ? '<td style="text-align:right">'.$tot_ordinato.'</td>' : '')
                .''.($marchio == 'logistica_amazon_tutto' ? '<td style="text-align:right">'.$tot_ez.'</td>
                <td style="text-align:right">'.$tot_amz.'</td>' : '')
                .'<td style="text-align:right">'.round($tot_valore,2).'</td>'
                .(Tools::getIsset('al_netto') ? '<td style="text-align:right">'.round($tot_valore_netto,2).'</td>' : '')
                .(Tools::getIsset('ordinato') ? '<td style="text-align:right">'.round($tot_valore_ordinato,2).'</td>' : '')
                .'<td></td><td></td><!-- <td style="text-align:right">'.$tot_impegnato.'</td> --><td style="text-align:right">'.$tot_ordinato
                .'</td></tr></table>
            ';

            $riepilogo_html .= "</form>";

            $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'datatable_ready.js');
            $this->addJS(_PS_BO_DEFAULT_THEME_DIR_.'plugins/DataTables/datatables.min.js');
            $this->addCSS(_PS_BO_DEFAULT_THEME_DIR_.'plugins/DataTables/datatables.min.css');

            $this->tpl_view_vars = array(
                'riepilogo_html' => $riepilogo_html, // Variabile che sostituisce gli echo finchè non viene portato tutto in bootstrap
            );
        }
        else 
        {            
            $resultsman = Db::getInstance()->executeS("SELECT id_manufacturer, name FROM "._DB_PREFIX_."manufacturer ORDER BY name");
            
            $this->addjQueryPlugin('select2');
            $this->addJS(_PS_JS_DIR_.'jquery/plugins/select2/select2_locale_'.$this->context->language->iso_code.'.js');
            $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'select2_ready.js');

            $this->tpl_view_vars = array(
                'marchi' => $resultsman,
            );
        }

        return parent::renderView();
    }
}
