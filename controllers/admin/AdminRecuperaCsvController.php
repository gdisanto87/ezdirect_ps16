<?php

class AdminRecuperaCsvControllerCore extends AdminController
{
    public function __construct()
    {
        $this->displayWarning('TESTARE RECUPERACSV E RECUPERABDL: non funziona la scrittura dei file');

        $this->bootstrap = true;
        $this->lang = false;

        $this->context = Context::getContext();

        $this->display = 'view';

        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Recupera CSV'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }

    public function renderView()
    {
        // Impiegati autorizzati a recuperare csv bdl
        $check_employee = ($this->context->employee->id == 6 || $this->context->employee->id == 22 ? true : false); // true: employee = Federico o Carolina
        $errore = 0;

        if(Tools::getIsset('submitcsv'))
        {
			$id_order = Tools::getValue('recupera_ordine');

            if(ctype_digit($id_order)){ 
                Product::RecuperaCSV($id_order);
            }
            else {
                $errore = 1;
                $this->errors[] = Tools::displayError('Inserisci un numero d\'ordine valido');
            }
		}
		else if(Tools::getIsset('submitbdl') && $check_employee)
        {
			$id_bdl = Tools::getValue('recupera_bdl');

            if(ctype_digit($id_bdl)){
                Product::RecuperaBDL($id_bdl);
            }
            else {
                $errore = 1;
                $this->errors[] = Tools::displayError('Inserisci un numero d\'ordine valido');
            }
		}

        $this->tpl_view_vars = array(
            'check_employee' => $check_employee,
            'errore' => $errore,
        );

        return parent::renderView();
    }
}