<?php

class AdminInvendutiControllerCore extends AdminController
{
    public function __construct()
    {        
        $this->bootstrap = true;
		$this->table = 'product';
        $this->lang = false;
        $this->explicitSelect = true;

        $this->context = Context::getContext();

		$this->_select = 'pl.name, m.name, 
                            (SELECT o2.date_add
                            FROM '._DB_PREFIX_.'orders o2
                                JOIN '._DB_PREFIX_.'order_detail od2 ON o2.id_order = od2.id_order 
                            WHERE od2.product_id = a.id_product
                            ORDER BY o2.id_order DESC
                            LIMIT 1) as ultimo_movimento
        ';
		
        $this->_join = '
            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (a.`id_product` = pl.`id_product`)
            LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (a.`id_manufacturer` = m.`id_manufacturer`)
        ';
        
        $this->_where = '
            AND a.stock_quantity > 0 
            AND a.stock_quantity != 999999 
            AND pl.id_lang = '.(int)$this->context->language->id.'
            AND a.id_product NOT IN (
                SELECT product_id 
                FROM '._DB_PREFIX_.'order_detail od 
                    JOIN '._DB_PREFIX_.'orders o ON od.id_order = o.id_order 
                WHERE o.date_add BETWEEN 
                    date_sub(curdate(),interval 30 day) AND 
                    date_sub(curdate(),interval 0 day)
            ) 
        ';

        $this->_use_found_rows = true;

        $this->fields_list = array(
            'id_product' => array(
                'title' => $this->l('ID Prodotto'),  
                'align' => 'center'
            ),
            'reference' => array(
                'title' => $this->l('Codice Prodotto'), 
                'align' => 'center',
                'callback' => 'creaTooltip',
                'remove_onclick' => true
            ),
            'supplier_reference' => array(
                'title' => $this->l('Codice SKU'), 
                'align' => 'center'
            ),
            'costruttore' => array(
                'title' => $this->l('Costruttore'),
                'align' => 'center', 
                'filter_key' => 'm!name' 
            ),
            'prodotto' => array(
                'title' => $this->l('Descrizione Prodotto'),
                'align' => 'center', 
                'filter_key' => 'pl!name',
                'callback' => 'cleanName'
            ),
            'stock_quantity' => array(
                'title' => $this->l('Quantità'),
                'align' => 'center',
            ),
            'ultimo_movimento' => array(
                'title' => $this->l('Ultimo Movimento'), 
                'align' => 'center',
                'type' => 'date',
                // 'filter_key' => 'pl!id_product',
                // 'callback' => 'getUltimoMovimento',
                'search' => false // Correggere: il filtro non funziona
            )
        );

        $this->redirect_after = ' '; // necessario per function redirect()
        
        parent::__construct();
    }

    public function renderList()
    {
        $this->addJS(_PS_BO_DEFAULT_THEME_JS_DIR_.'tooltip_prodotto.js');

        return parent::renderList();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Prodotti Invenduti (non movimentati da più di 30 giorni)'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
    }
    
    public function initToolbar()
    {
        parent::initToolbar();

        unset($this->toolbar_btn['new']); // Toglie il pulsante ADD NEW dalla toolbar
    }

    protected function redirect()
    {
        // action = edit
        if(Tools::getIsset('id_product') && Tools::getIsset('updateproduct')){
            $this->currentIndex = 'AdminProducts';
            $this->token = Tools::getAdminTokenLite('AdminProducts');
            $this->redirect_after = '/ezadmin/index.php?controller='.$this->currentIndex.'&id_product='.Tools::getValue('id_product').'&updateproduct&token='.$this->token;
            Tools::redirectAdmin($this->redirect_after);
        }
    }

    public function cleanName($value)
    {
        return preg_replace('/"/', "", $value);  
    }

    // Data dell'ultimo ordine effettuato in cui è presente il prodotto
    /*public function getUltimoMovimento($value)
	{
        $data = Db::getInstance()->getValue('
            SELECT o.date_add 
            FROM '._DB_PREFIX_.'orders o 
                JOIN '._DB_PREFIX_.'order_detail od ON o.id_order = od.id_order 
            WHERE od.product_id = '.$value.' 
            ORDER BY o.id_order DESC
        ');

        if(!$data)
            $data = 'Mai';
        else {
            $data = new DateTime($data);
            $data = $data->format('d/m/Y');
        }

        return '<span>'.$data.'</span>';
    }*/

    public function creaTooltip($value, $tr)
	{
        $tokenProducts = Tools::getAdminTokenLite('AdminProducts');
        $href_prodotto = '/ezadmin/index.php?controller=AdminProducts&id_product='.$tr['id_product'].'&updateproduct&token='.$tokenProducts;
        return '<a class="tooltip_prodotto" title="'.Product::showProductTooltip($tr['id_product']).'" href="'.$href_prodotto.'" target="_blank">'.$value.'</a>';
    }
}