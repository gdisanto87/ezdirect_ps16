<?php

class AdminOrigineOrdiniControllerCore extends AdminController
{
    public function __construct()
    {
        $this->displayInformation('Correggere le query e i nomi delle colonne origine: non si capisce cosa dovrebbero mostrare');
        
        $this->bootstrap = true;
        $this->table = 'order';
        $this->className = 'Order';
        $this->lang = false;
        $this->explicitSelect = true;
        
        $this->allow_export = true;
        
        $this->addRowAction('view');

        $this->_select = '
            (SELECT b.id_order FROM '._DB_PREFIX_.'orders b WHERE b.id_order = a.id_order) as first
        ';

        $this->_where = '
            AND a.id_customer > 0
        ';

        $this->_orderBy = 'id_order';
        $this->_orderWay = 'DESC';

        $this->_use_found_rows = true;

        $this->fields_list = array(
            'id_order' => array(
                'title' => $this->l('ID'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs'
            ),
            'date_add' => array(
                'title' => $this->l('Date'),
                'align' => 'text-right',
                'type' => 'datetime',
                'filter_key' => 'a!date_add'
            ),
            'id_customer' => array(
                'title' => $this->l('Customer ID'),
            ),
            'questo' => array(
                'title' => $this->l('Origine questo ordine'),
                'search' => false,
                'orderby' => false,
                'filter_key' => 'a!id_order',
                'callback' => 'getThisSource'
            ),
            'first' => array(
                'title' => $this->l('Origine primo ordine'),
                'search' => false,
                'orderby' => false,
                'tmpTableFilter' => true,
                'callback' => 'getFirstSource',
            ),
        );

        $this->redirect_after = ' '; // necessario per function redirect()
        
        parent::__construct();
    }

    public function initToolbar()
    {
        parent::initToolbar();

        unset($this->toolbar_btn['new']); // Toglie il pulsante ADD NEW dalla toolbar
    }

    protected function redirect()
    {
        // Redirect a view order se action = view
        if(Tools::getIsset('id_order') && Tools::getIsset('vieworder')){
            $this->currentIndex = 'AdminOrders';
            $this->token = Tools::getAdminTokenLite('AdminOrders');
            $this->redirect_after = '/ezadmin/index.php?controller='.$this->currentIndex.'&id_order='.Tools::getValue('id_order').'&vieworder&token='.$this->token;
            Tools::redirectAdmin($this->redirect_after);
        }
    }

    public function getThisSource($value)
    {
        $source = $this->getThisOrderSources($value);

        if(empty($source))
            $origine_ordine = '<i>Sconosciuto</i>';
        else 
        {
            $origine_ordine = preg_replace('/^www./', '', parse_url($source['http_referer'], PHP_URL_HOST));
            if($origine_ordine == '')
                $origine_ordine = 'Diretto';
            
            if(preg_match('/\&/', $source['keywords']) && preg_match('/google/', $source['http_referer'])) 
                $origine_ordine = 'Google CPC';
            //provare con pregmatch ampersand in keyword per distinguere adwords da organico... 
            
            if(preg_match('/googleads/', $source['http_referer'])) 
                $origine_ordine = 'Google CPC';
        }

        return $origine_ordine;
    }

    public function getFirstSource($value)
    {
        $primo_ordine = $this->getFirstOrderSources($value);

        if(empty($primo_ordine))
            $origine_primo_ordine = '<i>Sconosciuto</i>';
        else 
        {
            $origine_primo_ordine = preg_replace('/^www./', '', parse_url($primo_ordine['http_referer'], PHP_URL_HOST));
            if($origine_primo_ordine == '')
                $origine_primo_ordine = 'Diretto';
                
            if(preg_match('/\&/', $primo_ordine['keywords']) && preg_match('/google/', $primo_ordine['http_referer'])) 
                $origine_primo_ordine = 'Google CPC';
            //provare con pregmatch ampersand in keyword per distinguere adwords da organico... 
            
            if(preg_match('/googleads/', $primo_ordine['http_referer'])) 
                $origine_primo_ordine = 'Google CPC';	
        }

        return $origine_primo_ordine;
    }

    // Correggere: perchè quel controllo su date_add?
    public static function getThisOrderSources($id_order)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
            SELECT cos.http_referer, cos.request_uri, cos.keywords, cos.date_add
            FROM '._DB_PREFIX_.'orders o
            INNER JOIN '._DB_PREFIX_.'guest g ON g.id_customer = o.id_customer
            INNER JOIN '._DB_PREFIX_.'connections co ON co.id_guest = g.id_guest
            INNER JOIN '._DB_PREFIX_.'connections_source cos ON cos.id_connections = co.id_connections
            WHERE id_order = '.(int)($id_order).'
            AND cos.date_add >= DATE_SUB(o.date_add, INTERVAL 1 WEEK)
            ORDER BY cos.date_add ASC
        ');
	}
	
    // Correggere: non ha senso chiamarla "Origine primo ordine"
	public static function getFirstOrderSources($id_order)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
            SELECT cos.http_referer, cos.request_uri, cos.keywords, cos.date_add
            FROM '._DB_PREFIX_.'orders o
            INNER JOIN '._DB_PREFIX_.'guest g ON g.id_customer = o.id_customer
            INNER JOIN '._DB_PREFIX_.'connections co ON co.id_guest = g.id_guest
            INNER JOIN '._DB_PREFIX_.'connections_source cos ON cos.id_connections = co.id_connections
            WHERE id_order = '.(int)($id_order).'
            ORDER BY cos.date_add ASC
        ');
	}
}