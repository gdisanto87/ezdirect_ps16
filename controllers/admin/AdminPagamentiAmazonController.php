<?php

// Potrei togliere Core e trasformare in override
class AdminPagamentiAmazonControllerCore extends AdminController
{
    public function __construct()
    {
        $this->displayInformation('DISATTIVATO! To-Do: correggere i file inclusi');
        
        $this->bootstrap = true;
        $this->lang = false;

        $this->context = Context::getContext();

        $this->display = 'view';
        
        parent::__construct();
    }

    public function renderView()
    {
        //$is_agente = ($this->context->employee->id_profile == 7 ? true : false); // true: l'employee loggato è un agente

        if(Tools::getIsset('tipo'))
		{
            // Riepiloghi complessivi periodi
            if(Tools::getValue('tipo') == 'riepiloghi') 
            {
                // Ricerca normale
				if(Tools::getIsset('invia_form'))
					include('../docs/disponibilita/MWSFinancesService/Samples/ListFinancialEventGroupsSample.php');
                
                // Ricerca pagamenti precedenti al 10 aprile 2016
				if(Tools::getIsset('invia_form2'))
					include('../docs/disponibilita/MWSFinancesService/Samples/ListFinancialEventGroupsSample2.php');
			}
			else // Singoli ordini e nessuna selezione
				include('../docs/disponibilita/MWSFinancesService/Samples/ListFinancialEventsSample.php');
		}
		else
		{
            // include_once('functions.php'); // ?

            $marketplace = array(
                array(
                    'value' => it,
                    'name' => IT
                ),
                array(
                    'value' => fr,
                    'name' => FR
                ),
                array(
                    'value' => es,
                    'name' => ES
                ),
                array(
                    'value' => de,
                    'name' => DE
                ),
                array(
                    'value' => uk,
                    'name' => UK
                ),
            );
        }
        
        $this->tpl_view_vars = array(
            //'is_agente' => $is_agente,
            'marketplace' => $marketplace,
        );

        return parent::renderView();
    }
}
