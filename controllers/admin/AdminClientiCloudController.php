<?php

// Potrei togliere Core e trasformare in override
class AdminClientiCloudControllerCore extends AdminController
{
    public function __construct()
    {
        $this->displayInformation('Da completare quando sapremo come trovare se il cloud è stato pagato o meno');

        $this->bootstrap = true;
        $this->table = 'order';
        $this->lang = false;
        $this->explicitSelect = true;

        $this->allow_export = true;

        $this->context = Context::getContext();
        $this->context->currency = Currency::getDefaultCurrency();
        
        $this->_select = '
            c.*,
            (CASE c.is_company
            WHEN 1
            THEN c.company
            ELSE
            CONCAT(c.firstname," ",c.lastname)
            END
            ) cliente
        ';
        
        $this->_join = '
            JOIN '._DB_PREFIX_.'customer c ON a.id_customer = c.id_customer
            JOIN '._DB_PREFIX_.'cart ca ON a.id_cart = ca.id_cart
            JOIN cart_ezcloud ce ON a.id_cart = ce.id_cart
        ';

        $this->_where = ' AND (ce.periodicita = "mese" OR ce.periodicita = "trimestre" OR ce.periodicita = "anno")';
               
        $this->_use_found_rows = false;

        $this->fields_list = array(
            'id_customer' => array(
                'title' => $this->l('ID CRM'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
                'filter_key' => 'a!id_customer',
            ),
            'codice_esolver' => array(
                'title' => $this->l('Codice eSolver'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
                'filter_key' => 'c!codice_esolver',
            ),
            'cliente' => array(
                'title' => $this->l('Cliente'),
                'align' => 'text-left',
                'search' => false,
                'orderby' => false
            ),
            'id_order' => array(
                'title' => $this->l('N° ordine'),
                'align' => 'text-right',
            ),/*
            'pagato' => array(
                'title' => $this->l('Pagato?'),
                'align' => 'text-center',
                'class' => 'fixed-width-xs',
                'callback' => 'printV',
                'type' => 'select',
                'list' => array(1 => 'Sì', 0 => 'No'),
                'filter_key' => 'pagato'
            ),*/
            'date_add' => array(
                'title' => $this->l('Data ordine'),
                'align' => 'text-right',
                'type' => 'date',
                'callback' => 'format_data',
                'filter_key' => 'a!date_add',
            ),
        );

        $this->redirect_after = ' '; // necessario per function redirect()

        $this->addRowAction('view');
        
        parent::__construct();
    }

    public function initToolbarTitle()
	{
        parent::initToolbarTitle();

		array_pop($this->toolbar_title);
		$this->toolbar_title[] = sprintf($this->l('Clienti Cloud'));
		
        array_pop($this->meta_title);
        if (count($this->toolbar_title) > 0) {
            $this->addMetaTitle($this->toolbar_title[count($this->toolbar_title) - 1]);
        }
	}

    public function initToolbar()
    {
        parent::initToolbar();

        unset($this->toolbar_btn['new']); // Toglie il pulsante ADD NEW dalla toolbar
    }

    // Redirect all'anagrafica del cliente se action = view
    protected function redirect()
    {
        if(Tools::getIsset('id_order') && Tools::getIsset('vieworder')){
            $id_customer = Db::getInstance()->getValue('SELECT id_customer FROM '._DB_PREFIX_.'orders WHERE id_order = '.Tools::getValue('id_order'));
            $this->currentIndex = 'AdminCustomers';
            $this->token = Tools::getAdminTokenLite('AdminCustomers');
            $this->redirect_after = '/ezadmin/index.php?controller='.$this->currentIndex.'&id_customer='.$id_customer.'&viewcustomer&tab_name=cloud&token='.$this->token;
            Tools::redirectAdmin($this->redirect_after);
        }
    }

    // Stampa l'icona V se $value != 0
    public function printV($value)
    {
        return ($value ? '<i class="icon-check" style="color:green;"></i>' : '');
    }

    public function format_data($value)
	{
        $data = new DateTime($value);
        $data = $data->format('d/m/Y');

        return '<span>'.$data.'</span>';
    }
}