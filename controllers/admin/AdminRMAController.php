<?php

// Potrei togliere Core e trasformare in override
class AdminRMAControllerCore extends AdminController
{
    public function __construct()
    {
        $this->displayWarning('WARNING: Page under construction!');
        $this->displayInformation('Errore filtri e order by list??? Correggere list header, bulk delete, modifica rich a forn e prodotto arrivato; vedi AdminCustomerThreads');
        
        $this->bootstrap = true;
        $this->table = 'customer_thread';
        $this->className = 'CustomerThread';
        $this->lang = false;
        $this->explicitSelect = true;

        $this->allow_export = true;
        //$this->delete = true;

        $this->context = Context::getContext();
        $this->context->currency = Currency::getDefaultCurrency();

        $this->_select = '
            CONCAT(
                CASE a.id_contact
                WHEN 2
                    THEN "A"
                WHEN 4
                    THEN "T"
                WHEN 3
                    THEN "V"
                WHEN 6
                    THEN "R"
                WHEN 7
                    THEN "M"
                WHEN 8
                    THEN "D"
                WHEN 9
                    THEN "S"
                END, 
                SUBSTRING(a.date_add, 3, 2), 
                a.id_customer_thread 
            ) AS ticket,

            a.id_employee as id_employeez,
            r.codice_rma codice_rma, 
            r.rma_product prodotto_rma,
            r.richiesto_rma,
            r.codice_rma_fornitore,
            r.arrivoezcli,

            (CASE c.id_default_group
            WHEN 1
                THEN "Clienti web"
            WHEN 3
                THEN "Rivenditori"
            ELSE 
                "Altro"
            END
            ) AS gruppo_clienti,

            c.company,
            
            (CASE c.is_company
            WHEN 1 
                THEN c.vat_number
            ELSE 
                c.tax_code
            END) AS cfpi,

            (SELECT reference FROM '._DB_PREFIX_.'product WHERE id_product = a.id_product) prodotto,
            c.vat_number,
            c.tax_code,

            (CASE c.is_company
            WHEN 0
                THEN CONCAT(c.firstname," ",c.lastname)
            WHEN 1
                THEN c.company
            ELSE
                CONCAT(c.firstname," ",c.lastname)
            END
            ) AS customer, 
            
            cl.id_contact as contact_type, 

            (CASE
            WHEN cl.name LIKE "%contabil%"
                THEN "Contabilita"
            WHEN cl.name LIKE "%ordini%"
                THEN "Ordini"
            WHEN cl.name = "Assistenza tecnica postvendita"
                THEN "Assistenza"
            WHEN cl.name = "Rivenditori"
                THEN "Rivenditori"
            WHEN cl.name = "Messaggi"
                THEN "Messaggi"
            WHEN cl.name = "RMA"
                THEN "RMA"
            ELSE 
                "Altro"
            END	
            ) AS contact,

            a.status AS stato,
            a.date_upd AS data_act, 
            
            (SELECT IFNULL(CONCAT(e.firstname," ",LEFT(e.lastname, 1),"."), "--")
            FROM `'._DB_PREFIX_.'customer_thread` ct 
            INNER JOIN '._DB_PREFIX_.'employee e ON e.id_employee = ct.id_employee
            WHERE ct.id_employee > 0 
                AND ct.`id_customer_thread` = a.`id_customer_thread` 
            ORDER BY ct.date_add DESC LIMIT 1) AS employee,

            c.id_customer as id_cliente
        ';

        $this->_join = '
            LEFT JOIN `'._DB_PREFIX_.'customer` c ON c.`id_customer` = a.`id_customer`
            LEFT JOIN rma r ON r.`id_customer_thread` = a.`id_customer_thread`
            LEFT JOIN `'._DB_PREFIX_.'product` p ON a.`id_product` = p.`id_product`
            LEFT JOIN `'._DB_PREFIX_.'contact_lang` cl ON (cl.`id_contact` = a.`id_contact` AND cl.`id_lang` = '.(int)$this->context->language->id.') 
        ';
		
		$this->_join .= ''.(Tools::getIsset('cercaticket') ? 'JOIN (SELECT c1.message, c1.id_customer_thread FROM '._DB_PREFIX_.'customer_message c1 JOIN rma r1 ON c1.id_customer_thread = r1.id_customer_thread WHERE c1.message LIKE "%'.Tools::getValue('cercaticket').'%" OR r1.seriale LIKE "%'.Tools::getValue('cercaticket').'%" OR r1.codice_rma LIKE "%'.Tools::getValue('cercaticket').'%" OR r1.rma_product LIKE "%'.Tools::getValue('cercaticket').'%") cm ON a.id_customer_thread = cm.id_customer_thread' : '');

        $this->_where = ' AND a.id_contact = 9 AND a.id_employee > 0 ';

        /* // CONTROLLARE
        if(isset($_GET['date']) && $_GET['date'] == 'oggi') {
			$this->_where = ' AND a.date_add BETWEEN "'.date("Y-m-d").' 00:00:00" AND "'.date("Y-m-d").' 23:59:59" ';
        }
        */

        $this->_group .= 'GROUP BY a.id_customer_thread';
        
        /* // CONTROLLARE
        if(isset($_GET['type'])) {
            if(is_numeric($_GET['type'])) {
                $this->_group.= 'AND a.id_contact = '.$_GET['type'].' ';
            }
            else {
                $this->_group.= 'AND a.id_contact = 3523453453 ';
            }
        }*/
        
        $this->_use_found_rows = true;

        $statusArray = array(
			'open' => $this->l('Open'),
			'closed' => $this->l('Closed'),
			'pending1' => $this->l('In lavorazione'),
			'pen' => $this->l('Aperto + In lavorazione')
		);

        $the_employees = Db::getInstance()->executeS('
            SELECT `id_employee`, `firstname`, `lastname`
            FROM `'._DB_PREFIX_.'employee`
            WHERE `active` = 1
            ORDER BY `firstname` ASC
        ');

        $employees_list[0] = 'Nessuno';
        foreach ($the_employees as $employee) {
            $employees_list[$employee['id_employee']] = $this->findEmployee($employee['id_employee']);
        }

        $this->fields_list = array(
            'id_customer_thread' => array(
                'title' => $this->l('id_customer_thread'), 
                'search' => false,
                'orderby' => false,
            ),
			'ticket' => array(
                'title' => $this->l('ID'), 
                'filter' => true, 
                'search' => true, 
                'width' => 25, 
                'tmpTableFilter' => true, 
                'filter_type' => 'string'
            ),
			'stato' => array(
                'title' => $this->l('Status'), 
                'type' => 'select', 
                'list' => $statusArray, 
                'align' => 'center', 
                'filter_key' => 'stato', 
                'tmpTableFilter' => true, 
                'filter_type' => 'string',
                'callback' => 'editStato',
                'remove_onclick' => true
            ),
			'id_employeez' => array(
                'title' => $this->l('In carico a'), 
                'type' => 'select', 
                'list' => $employees_list, 
                'filter_key' => 'a!id_employee', 
                'callback' => 'editIncarico',
                'remove_onclick' => true
            ),
			'customer' => array(
                'title' => $this->l('Customer'), 
                'filter_key' => 'customer', 
                'tmpTableFilter' => true
            ),
			'codice_rma' => array(
                'title' => $this->l('Cod. RMA'), 
                'filter_key' => 'codice_rma', 
                'tmpTableFilter' => true
            ),
			'prodotto' => array(
                'title' => $this->l('Prodotto'), 
                'filter_key' => 'prodotto', 
                'tmpTableFilter' => true
            ),
			'richiesto_rma' => array(
                'title' => $this->l('Rich. a forn.'), 
                'align' => 'text-center', 
                'class' => 'fixed-width-xs',
                'type' => 'select',
                'list' => array(1 => 'Sì', 0 => 'No'),
                'orderby' => false,
                'filter_key' => 'r!richiesto_rma',
                'callback' => 'printRichiestoIcon',
            ),
			'arrivoezcli' => array(
                'title' => $this->l('Prodotto arrivato'), 
                'align' => 'text-center', 
                'class' => 'fixed-width-xs',
                'type' => 'select',
                'list' => array(1 => 'Sì', 0 => 'No'),
                'orderby' => false,
                'filter_key' => 'r!arrivoezcli',
                'callback' => 'printArrivoIcon',
            ),
			'codice_rma_fornitore' => array(
                'title' => $this->l('Cod. RMA Forn.'), 
                'filter_key' => 'codice_rma_fornitore', 
                'tmpTableFilter' => true
            ),
			'data_act' => array(
                'title' => $this->l('Data'), 
                'type' => 'date', 
                'tmpTableFilter' => true,
                'callback' => 'format_data',
            ),
        );
        
        $this->redirect_after = ' ';

        $this->addRowAction('view');
        $this->addRowAction('delete');
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?'),
                'icon' => 'icon-trash'
            )
        );
        
        parent::__construct();
    }

    public function renderList()
    {
        /* CORREGGERE, QUESTO E' DI CUSTOMERS */
        if ((Tools::isSubmit('submitBulkdelete'.$this->table) || Tools::isSubmit('delete'.$this->table)) && $this->tabAccess['delete'] === '1') {
            $this->tpl_list_vars = array(
                //'delete_customer' => true,
                'REQUEST_URI' => $_SERVER['REQUEST_URI'],
                'POST' => $_POST
            );
        }

        return parent::renderList();
    }

    public function initToolbar()
    {
        parent::initToolbar();

        unset($this->toolbar_btn['new']); // Toglie il pulsante ADD NEW dalla toolbar
    }

    protected function redirect()
    {
        // Redirect alla view del ticket nell'anagrafica cliente se action = view
        if(Tools::getIsset('id_customer_thread') && Tools::getIsset('viewcustomer_thread')){
            $id_customer = Db::getInstance()->getValue('SELECT id_customer FROM '._DB_PREFIX_.'customer_thread WHERE id_customer_thread = '.Tools::getValue('id_customer_thread'));
            $this->currentIndex = 'AdminCustomers';
            $this->token = Tools::getAdminTokenLite('AdminCustomers');
            $this->redirect_after = '/ezadmin/index.php?controller='.$this->currentIndex.'&id_customer='.$id_customer.'&viewcustomer&tab_name=tickets&azione=tickets&viewticket&id_customer_thread='.Tools::getValue('id_customer_thread').'&token='.$this->token;
            Tools::redirectAdmin($this->redirect_after);
        }
        // Redirect a AdminPlannerController se action = delete, per poi tornare ad AdminRMAController
        else if(Tools::getIsset('id_customer_thread') && Tools::getIsset('deletecustomer_thread')){
            $this->currentIndex = 'AdminPlanner';
            $this->token = Tools::getAdminTokenLite('AdminPlanner');
            $this->redirect_after = '/ezadmin/index.php?controller='.$this->currentIndex.'&id_customer_thread='.Tools::getValue('id_customer_thread').'&del&typedel=9&token='.$this->token.'&backtorma';
            Tools::redirectAdmin($this->redirect_after);
        }
    }

    public function postProcess()
    {
        if(isset($_POST['cambiastatust_listing'])) {
            Db::getInstance()->execute("
                UPDATE "._DB_PREFIX_."customer_thread 
                SET status = '".Tools::getValue('cambiastatust_listing')."', date_upd = '".date("Y-m-d H:i:s")."' 
                WHERE id_customer_thread = '".Tools::getValue('id_customer_thread')."'
            ");
            
            switch(Tools::getValue('cambiastatust_listing'))
            {
                case 'open': $switch_status = 3; break;
                case 'pending1': $switch_status = 4; break;
                case 'closed': $switch_status = 5; break;
                default: $switch_status = 10; break;
            }	
            
            Customer::Storico(Tools::getValue('id_customer_thread'), 'T', $this->context->employee->id, $switch_status);
            Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.$this->token);
        }

        if(isset($_POST['cambiaincaricot'])) {
            Customer::cambiaIncaricoVeloce(Tools::getValue('tipo'), Tools::getValue('cambiaincaricot'), Tools::getValue('id_thread'), Tools::getValue('precedente_incarico'), Tools::getValue('id_ticket_univoco'), Tools::getValue('id_customer'));
            Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.$this->token);
        }
    }

    public function editStato($value, $tr)
    {
        $sigla = 't';
        $id_azione = $tr['ticket'];
        $id_customer = $tr['id_cliente'];
        $thread_key = 'id_customer_thread';

        $status_edit = 
            '<form style="margin-left:5px; display:block;" name="cambiastato'.$sigla.'_'.$id_azione.'" id="cambiastato'.$sigla.'_'.$id_azione.'" action="" method="post" />	
                <input type="hidden" name="id_customer" value="'.$id_customer.'" />
                <input type="hidden" name="'.$thread_key.'" value="'.$id_azione.'" />
                <input type="hidden" name="back" value="1" />
                <select name="cambiastatus'.$sigla.'_listing" onchange="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { document.forms[\'cambiastato'.$sigla.'_'.$id_azione.'\'].submit(); } else { this.value=\''.$value.'\'}">
                    <option style="padding-left:30px;" value="closed" '.($value == 'closed' ? 'selected="selected"' : '').'>C</option>
                    <option style="padding-left:30px;" value="pending1" '.($value == 'pending1' ? 'selected="selected"' : '').'>L</option>
                    <option style="padding-left:30px;" value="open" '.($value == 'open' ? 'selected="selected"' : '').'>A</option>
                </select>
            </form>
        ';

        return $status_edit;
    }

    public function editIncarico($value, $tr)
    {
        $sigla = 't';
        $tipo = 'ticket';
        $id_azione = $tr['ticket'];
        $id_customer = $tr['id_cliente'];

        $in_carico_edit = '
            <form style="margin-left:5px; display:block;" name="cambiaincarico'.$sigla.'_'.$tr['id_customer_thread'].'" id="cambiaincarico'.$sigla.'_'.$tr['id_customer_thread'].'" action="" method="post" />
                <a id="'.$tr['id_customer_thread'].'"> </a>
                <input type="hidden" name="id_customer" value="'.$id_customer.'" />
                <input type="hidden" name="id_ticket_univoco" value="'.$id_azione.'" />
                <input type="hidden" name="back" value="1" />
                <input type="hidden" name="id_thread" value="'.$tr['id_customer_thread'].'" />
                <input type="hidden" name="precedente_incarico" value="'.$value.'" />
                <input type="hidden" name="tipo" value="'.$tipo.'" />
                <select name="cambiaincarico'.$sigla.'" onchange="javascript: var sure=window.confirm(\'Sei sicuro?\'); if (sure) { document.forms[\'cambiaincarico'.$sigla.'_'.$tr['id_customer_thread'].'\'].submit(); } else { this.value=\''.$value.'\'}">
                    <option value="0" '.($value == '0' ? 'selected="selected"' : '').'>--Nessuno--</option>
        ';

        $employees = Employee::getEmployees();

        foreach($employees as $employee)
            $in_carico_edit .= '<option value="'.$employee['id_employee'].'" '.($value == $employee['id_employee'] ? 'selected="selected"' : '').'>'.$employee['firstname'].' '.substr($employee['lastname'], 0, 1).'</option>';
        
        $in_carico_edit .= '
                </select>
            </form>
        ';

        return $in_carico_edit;
    }

    public function format_data($value)
	{
        $data = new DateTime($value);
        $data = $data->format('d/m/Y');

        return '<span>'.$data.'</span>';
    }

    public function processChangeRichiestoVal()
    {
        $richiesto_old = Db::getInstance()->getValue('SELECT richiesto_rma FROM rma WHERE id_customer_thread = '.$this->id_object);
        if ($richiesto_old === false) {
            $this->errors[] = Tools::displayError('An error occurred while updating rma information.');
        }

        $richiesto_new = ($richiesto_old ? 0 : 1);

        Db::getInstance()->execute('UPDATE rma SET richiesto_rma = '.$richiesto_new.' WHERE id_customer_thread = '.$this->id_object);

        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    }

    public function processChangeArrivoVal()
    {
        $arrivoezcli_old = Db::getInstance()->getValue('SELECT arrivoezcli FROM rma WHERE id_customer_thread = '.$this->id_object);
        if ($arrivoezcli_old === false) {
            $this->errors[] = Tools::displayError('An error occurred while updating rma information.');
        }

        $arrivoezcli_new = ($arrivoezcli_old ? 0 : 1);

        Db::getInstance()->execute('UPDATE rma SET arrivoezcli = '.$arrivoezcli_new.' WHERE id_customer_thread = '.$this->id_object);

        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    }

    public function printRichiestoIcon($value, $rma)
    {
        return '<a class="list-action-enable '.($value ? 'action-enabled' : 'action-disabled').'" href="index.php?'.htmlspecialchars('tab=AdminRMA&id_customer_thread='
            .(int)$rma['id_customer_thread'].'&changeRichiestoVal&token='.Tools::getAdminTokenLite('AdminRMA')).'">
				'.($value ? '<i class="icon-check"></i>' : '<i class="icon-remove"></i>').
            '</a>';
    }

    public function printArrivoIcon($value, $rma)
    {
        return '<a class="list-action-enable '.($value ? 'action-enabled' : 'action-disabled').'" href="index.php?'.htmlspecialchars('tab=AdminRMA&id_customer_thread='
            .(int)$rma['id_customer_thread'].'&changeArrivoVal&token='.Tools::getAdminTokenLite('AdminRMA')).'">
				'.($value ? '<i class="icon-check"></i>' : '<i class="icon-remove"></i>').
            '</a>';
    }

    public function initProcess()
    {
        parent::initProcess();

        if (Tools::isSubmit('changeRichiestoVal') && $this->id_object) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'change_richiesto_val';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        } elseif (Tools::isSubmit('changeArrivoVal') && $this->id_object) {
            if ($this->tabAccess['edit'] === '1') {
                $this->action = 'change_arrivo_val';
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        }
    }

    // Esiste già in AdminCarts, spostare in una classe / file di utilità ed usarla dichiarandola una sola volta
    public function findEmployee($value)
    {
        if($value){
            $employee = Db::getInstance()->getRow('SELECT firstname, lastname FROM '._DB_PREFIX_.'employee WHERE id_employee = '.$value);

            $firstname = $employee['firstname'];
            $lastname = $employee['lastname'][0]; // Iniziale del cognome
            $name = $firstname.' '.$lastname.'.';
        }
        else
            $name = '-';
        
        return $name;
    }
}