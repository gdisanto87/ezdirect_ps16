{*
* 2007-2011 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2011 PrestaShop SA
*  @version  Release: $Revision: 1.4 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='Product Comparison'}{/capture}

{include file="$tpl_dir./breadcrumb.tpl"}
<h1>{l s='Product Comparison'}</h1>

{if $hasProduct}
<div class="products_block">
	<table id="product_comparison">
		<tr>
			<td width="20%"></td>
			{assign var='taxes_behavior' value=false}
			{if $use_taxes && (!$priceDisplay  || $priceDisplay == 2)}
				{assign var='taxes_behavior' value=true}
			{/if}
			{foreach from=$products item=product name=for_products}
				{assign var='replace_id' value=$product->id|cat:'|'}

					<td width="{$width}%" class="ajax_block_product comparison_infos">
						<h5><a href="{$product->getLink()}" title="{$product->name|truncate:64:'...'|escape:'htmlall':'UTF-8'}">{$product->name|truncate:64:'...'|escape:'htmlall':'UTF-8'}</a></h5>
				
						<div class="product_desc"><a href="{$product->getLink()}" title="{l s='More'}">{$product->description_short|strip_tags|truncate:45:'...'}</a></div>
						
						<div class="comparison_product_infos" style="text-align:center; width:80%; margin-top:-70px">
							<a href="{$product->getLink()}" title="{$product->name|escape:html:'UTF-8'}" class="product_image" style="display:block; margin:0 auto">
							<img src="{$link->getImageLink($product->link_rewrite, $product->id_image, 'home')}" alt="{$product->name|escape:html:'UTF-8'}" width="{$homeSize.width}" height="{$homeSize.height}" style="display:block; margin:0 auto" />
							</a>
						</div>	
						<br />
						<div style="position:relative; width:100%; text-align:center">
							{if isset($product->show_price) && $product->show_price && !isset($restricted_country_mode) && !$PS_CATALOG_MODE}
								{if $product->price == 0}
									<p class="price_container"><a style="display:block;margin-top:12px" href="{$link->getPageLink('cms.php?id_cms=32')}">{l s='Ask for a quotation'}</a><br /></p>
								{else}
		
									<table style="width:90%">
									{if isset($product->a_partire_da)}
										<tr>
										<td style="vertical-align:middle; text-align:center">	<p class="price_container"><span class="price">
										{l s='From'}
										{assign var=$riduzione value=$product->a_partire_da}
										{assign var=$prezzofinale value=$product->getPrice($taxes_behavior) - ($product->getPrice($taxes_behavior)*$riduzione)|floatval}
										{convertPrice price=$product->getPrice($taxes_behavior) - ($product->getPrice($taxes_behavior)*$product->a_partire_da)}
										</p>
										</td>
									{else}
										<tr>
										<td style="vertical-align:middle; text-align:center">
										<p class="price_container"><span class="price">{convertPrice price=$product->getPrice($taxes_behavior)}</span> 	
										</p></td>	
					
					
									{/if}
					
									{if (!$product->hasAttributes() OR (isset($add_prod_display) AND ($add_prod_display == 1))) AND $product->minimal_quantity == 1 AND $product->customizable != 2 AND !$PS_CATALOG_MODE}
										{if ($product->quantity > 0 OR $product->allow_oosp)}
											<td style="vertical-align:middle; text-align:center"><a class="ajax_add_to_cart_button" style="text-decoration:none; background-color:#e62026; color:#ffffff; border:1px solid #bdc2c9; position:relative; display:block; margin-top:0px" name="{$product->id|intval}" id="ajax_id_product_{$product->id}" href="{$link->getPageLink('cart.php')}?qty=1&amp;id_product={$product->id}&amp;token={$static_token}&amp;add" title="{l s='Buy'}">{l s='Buy'}</a></td>
										{else}
						
										{/if}
										</tr>
									{/if}
									</table>
								{/if}
			
							<p style='text-align:center'>
							<a class="cmp_remove" href="{$link->getPageLink('products-comparison.php')}" rel="ajax_id_product_{$product->id}">{l s='Remove'}</a>
							</p>
			
							{else}
								<div style="height:23px;"></div>
							{/if}
						</div>
					</td>
			{/foreach}
		</tr>

		<tr class="comparison_header">
			<td>
				{l s='Features'}
			</td>
			{section loop=$products|count step=1 start=0 name=td}
			<td></td>
			{/section}
		</tr>

		{if $ordered_features}
        {* Added by module for grouped features *}
		{foreach from=$ordered_features item=group}
            	<tr><td colspan="{$products|@count+1}"> <br /><strong>{$group.name}</strong></td></tr>
            {foreach from=$group.features item=feature}
            	<tr>
            		{cycle values='comparison_feature_odd,comparison_feature_even' assign='classname'}
            		<td class="{$classname}" style="padding-left: 10px">
            			{$feature.name|escape:'htmlall':'UTF-8'}
            		</td>
            
            			{foreach from=$products item=product name=for_products}
					{assign var='product_id' value=$product->id}
					{assign var='feature_id' value=$feature.id_feature}
					{if isset($product_features[$product_id])}
						{assign var='tab' value=$product_features[$product_id]}
						<td  width="{$width}%" class="{$classname} comparison_infos">{$tab[$feature_id]|escape:'htmlall':'UTF-8'|replace:'4*':'<img src="https://www.ezdirect.it/images/4.gif" alt="4" title="4" width="64" height="12" />'|replace:'5*':'<img src="https://www.ezdirect.it/images/5.gif" alt="5" title="5" width="64" height="12" />'|replace:'3*':'<img src="https://www.ezdirect.it/images/3.gif" alt="3" title="3" width="64" height="12" />'|replace:'2*':'<img src="https://www.ezdirect.it/images/2.gif" alt="2" title="2" width="64" height="12" />'|replace:'1*':'<img src="https://www.ezdirect.it/images/1.gif" alt="1" title="1" width="64" height="12" />'|replace:'s&igrave;':'<img src="https://www.ezdirect.it/images/si.gif" alt="s�" title="s�" width="14" height="14" />'}</td>
					{else}
						<td  width="{$width}%" class="{$classname} comparison_infos">{$tab}</td>
					{/if}
				{/foreach}
            	</tr>
             {/foreach}  {* feature loop *}
		{/foreach} {* group loop *}
		{else}
			<tr>
				<td></td>
				<td colspan="{$products|@count + 1}">{l s='No features to compare'}</td>
			</tr>
		{/if}

		{$HOOK_EXTRA_PRODUCT_COMPARISON}
	</table>
</div>
{else}
	<p class="warning">{l s='There is no product in the comparator'}</p>
{/if}

