{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}




<tr id="product_{$product.id_product}_{$product.id_product_attribute}" class="{if $smarty.foreach.productLoop.last}last_item{elseif $smarty.foreach.productLoop.first}first_item{/if}{if isset($customizedDatas.$productId.$productAttributeId) AND $quantityDisplayed == 0}alternate_item{/if} cart_item">
	<td class="cart_product">
		{if $product.active == 1}<a href="{$link->getProductLink($product.id_product, $product.link_rewrite, $product.category)|escape:'htmlall':'UTF-8'}">{else}{/if}<img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'small')}" alt="{$product.name|escape:'htmlall':'UTF-8'}" {if isset($smallSize)}width="{$smallSize.width}" height="{$smallSize.height}" {/if} />{if $product.active == 1}</a>{else}{/if}
	</td>
	<td class="cart_description" style="width:200px">
		<h5>{if $product.active == 1}<a href="{$link->getProductLink($product.id_product, $product.link_rewrite, $product.category)|escape:'htmlall':'UTF-8'}">{else}<span style="font-size:12px">{/if}{$product.name|escape:'htmlall':'UTF-8'}{if $product.active == 1}</a>{else}</span>{/if}</h5>
		{if isset($product.attributes) && $product.attributes}{if $product.active == 1}<a href="{$link->getProductLink($product.id_product, $product.link_rewrite, $product.category)|escape:'htmlall':'UTF-8'}">{else}{/if}{$product.attributes|escape:'htmlall':'UTF-8'}{if $product.active == 1}</a>{else}{/if}{/if}
		{if $smarty.now|date_format:"%Y-%m-%d" < $product.date_available}
		<br />
		{l s='This product will be available from'} {$product.date_available|date_format:"%d-%m-%Y"}
		{else}
		{/if}
	</td>
	<td class="cart_ref">{if $product.reference}{$product.reference|escape:'htmlall':'UTF-8'}{else}--{/if}</td>
	<td class="cart_availability">
		{if $product.active AND $product.quantity_def > 0 AND !$PS_CATALOG_MODE}
			<img src="{$img_dir}icon/available.gif" alt="{l s='Available'}" width="14" height="14" />
		{else}
			<img src="{$img_dir}icon/unavailable.gif" alt="{l s='Out of stock'}" width="14" height="14" />
		{/if}
	</td>
	<td class="cart_unit">
		<span class="price" id="product_price_{$product.id_product}_{$product.id_product_attribute}">
			{if !$priceDisplay}{convertPrice price=$product.price_wt}{else}{convertPrice price=$product.price}{/if}
		</span>
	</td>
	<td>
	<div style="text-align:center">
				{if $preventivo == 1}{else}<a rel="nofollow" class="cart_quantity_delete_edit" id="{$product.id_product}_{$product.id_product_attribute}" href="{$link->getPageLink('cart.php', true)}?delete&amp;id_product={$product.id_product|intval}&amp;ipa={$product.id_product_attribute|intval}&amp;token={$token_cart}" title="{l s='Delete'}"><img src="{$img_dir}icon/icon_delete.gif" alt="{l s='Delete'}" class="icon" height="18" width="18" /></a>
				
				<a rel="nofollow" href="order-opc.php" title="{l s='Refresh Cart'}"><img src="{$img_dir}icon/refresh2.gif" alt="{l s='Refresh'}" class="icon" height="18" width="18" /></a>
				{/if}
			</div>
			</td>
	
	<td class="cart_quantity" style="text-align: center; vertical-align:center"{if isset($customizedDatas.$productId.$productAttributeId) AND $quantityDisplayed == 0} {/if}>
		{if isset($customizedDatas.$productId.$productAttributeId) AND $quantityDisplayed == 0}<span id="cart_quantity_custom_{$product.id_product}_{$product.id_product_attribute}" >{$product.customizationQuantityTotal}</span>{/if}
		{if !isset($customizedDatas.$productId.$productAttributeId) OR $quantityDisplayed > 0}
		<div style="position:relative; width:55px; display:block; margin:0 auto; {if $preventivo == 1}text-align:right{/if}">
			{if $preventivo == 1} <strong>{$product.cart_quantity-$quantityDisplayed}</strong> {/if}
			<div id="cart_quantity_button" style="float:left; margin-top:-5px">

			</div>
			<div style="float: left; margin-top:0px">
			{if $preventivo == 1}
			
			{else}<input type="hidden" value="{if $quantityDisplayed == 0 AND isset($customizedDatas.$productId.$productAttributeId)}{$customizedDatas.$productId.$productAttributeId|@count}{else}{$product.cart_quantity-$quantityDisplayed}{/if}" name="quantity_{$product.id_product}_{$product.id_product_attribute}_hidden" />
				<div class="quantity_input_div">
				
					<input size="2" {if $product.id_product == 31110 || $product.id_product == 31109  || $product.reference == 'OMAGGIO' || ( $product.cart_free == 1)} readonly="readonly" 
					{else}{/if}  class="cart_quantity_input quantity_input" type="text" value="{if $quantityDisplayed == 0 AND isset($customizedDatas.$productId.$productAttributeId)}{$customizedDatas.$productId.$productAttributeId|@count}{else}{$product.cart_quantity-$quantityDisplayed}{/if}"  name="quantity_{$product.id_product}_{$product.id_product_attribute}"  id="quantity_{$product.id_product}_{$product.id_product_attribute}" style="margin-top:0px; margin-right:0px" />{/if}
					
					{if $product.id_product == 31110 || $product.id_product == 31109 || $product.reference == 'OMAGGIO' || ( $product.cart_free == 1)}
					{else}
						{if $preventivo == 1}
						{else}
							<div class="quantity_input_buttons">
								<span class="cart_quantity_span span_up cart_quantity_up" id="cart_quantity_up_{$product.id_product}_{$product.id_product_attribute}" onclick="quantity_cart_add('quantity_{$product.id_product}_{$product.id_product_attribute}'); ">+</span>
								<span class="cart_quantity_span span_down cart_quantity_down" id="cart_quantity_down_{$product.id_product}_{$product.id_product_attribute}" onclick="quantity_cart_rmv('quantity_{$product.id_product}_{$product.id_product_attribute}'); ">-</span>
							</div>
						{/if}
					{/if}
				</div>
			
			</div>
			<div style="clear:both"></div>
		</div>
		{/if}
	</td>
	<td class="cart_total" style="padding-right:5px; >
		<span class="price" id="total_product_price_{$product.id_product}_{$product.id_product_attribute}">
			{if $quantityDisplayed == 0 AND isset($customizedDatas.$productId.$productAttributeId)}
				{if !$priceDisplay}{displayPrice price=$product.total_customization_wt}{else}{displayPrice price=$product.total_customization}{/if}
			{else}
				{if !$priceDisplay}{displayPrice price=$product.total_wt}{else}{displayPrice price=$product.total}{/if}
			{/if}
		</span>
		
	</td>
</tr>
