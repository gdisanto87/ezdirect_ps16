{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{include file="$tpl_dir./breadcrumb.tpl"}
{include file="$tpl_dir./errors.tpl"}

{if isset($category)}
	{if $category->id AND $category->active}

		<h1 class='category'>
			{strip}
				{$category->name|escape:'htmlall':'UTF-8'}
				</h1>
			
			{/strip}
	
		{if $scenes}
			<!-- Scenes -->
			{include file="$tpl_dir./scenes.tpl" scenes=$scenes}
		{else}

		{/if}
		<br />
		
	
		<div id="category-description">
		{if $category->description}
			<div class="cat_desc">{$category->description|regex_replace:"/http\:\/\/www\.ez/":"https://www.ez"|replace:'<td>Si</td>':'<td><img src="https://www.ezdirect.it/images/si.gif" alt="s&igrave;" title="s&igrave;" width="14" height="14" /></td>'}</div><div style="clear:both"></div>
		{/if}
		</div>
		{if isset($subcategories)}
		<!-- Subcategories -->
		<div id="subcategories">
			<ul class="inline_list">
			{foreach from=$subcategories item=subcategory}
				<li>
					<a href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'htmlall':'UTF-8'}" title="{$subcategory.name|escape:'htmlall':'UTF-8'}">
						{if $subcategory.id_image}
							<img src="{$link->getCatImageLink($subcategory.link_rewrite, $subcategory.id_image, 'medium')}" alt="" width="90" height="90" style="border:0px; display:block; margin:0 auto" />
						{else}
							<img src="{$img_cat_dir}default-medium.jpg" title="{$subcategory.name|escape:'htmlall':'UTF-8'}" alt="{$subcategory.name|escape:'htmlall':'UTF-8'}" width="{$mediumSize.width}" height="{$mediumSize.height}" />
						{/if}
					</a>
					<br />
					<a href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'htmlall':'UTF-8'}">{$subcategory.name|escape:'htmlall':'UTF-8'}</a>
				</li>
			{/foreach}
			{if $category->id == 23}
			<li>
				<a href="https://www.ezdirect.it/centralino-virtuale/" title="Centralino virtuale">
					<img src="https://www.ezdirect.it/367442-40898-large/ezcloud-basic-centralino-virtuale-cloud.jpg" alt="" width="90" height="90" style="border:0px; display:block; margin:0 auto" />
				</a>
				<br />
				<a href="https://www.ezdirect.it/centralino-virtuale/">Centralino virtuale</a>
			</li>
			{/if}
			</ul>
			<br class="clear"/>
		</div>
		{/if}
<br />
		{if $products}
		&nbsp;&nbsp;&nbsp;<span class="category-product-count">{include file="$tpl_dir./category-count.tpl"}</span><br />
<div class='block_listing_header'>
<div class='block-compare'>
{include file="$tpl_dir./product-compare.tpl"}

</div>
<div class='block-sort'>
				{include file="$tpl_dir./product-sort.tpl"}
</div>
</div>		
	
				{include file="$tpl_dir./product-list.tpl" products=$products}
				{include file="$tpl_dir./product-compare.tpl"}	
					
			<br /><br />
				{include file="$tpl_dir./pagination.tpl"}
			{elseif !isset($subcategories)}
				<p class="warning">{l s='There are no products in this category.'}</p>
			{/if}
	{elseif $category->id}
		<p class="warning">{l s='This category is currently unavailable.'}</p>
	{/if}
{/if}
<br /><br />

		
