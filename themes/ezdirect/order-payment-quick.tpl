{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}



<h3 class="payment_title">{l s='Choose your payment method'}</h3>

	<p class="bubble" style="right: 135px; position:absolute; top:30px; margin-top:3px; display:none" id="payment_notice">
	{l s='You must choose a payment method before continue'}
	</p>

	<table id="paymentTable" class="std">
		<thead>
			<tr>
				<th class="payment_action first_item"></th>
				<th class="payment_name item">{l s='Payment method'}</th>
				
				<th class="payment_info"></th>
			</tr>
		</thead>
		<tbody>
			<tr class="first_item">
			<td><input type="radio" onclick="updateCarrierSelectionAndGift(); $('#payment_notice').hide();" name="payment" {if $payment_method_cart == "Bonifico" || $payment_method_cart == "Bonifico anticipato" || $payment_method_cart == "Bonifico Anticipato"}checked="checked"{/if} id="bonifico_p" value="modules/bankwire/payment.php" /></td>
			<td><label for="bonifico_p">{l s='Bankwire'} - <span style='font-size:10px'>IBAN: IT82S0503413643000000001454 </span></label></td>
			<td></td>
			</tr>
			{if $blacklist == 1}
			{else}
			<tr class="alternate_item">
			<td><input type="radio" onclick="updateCarrierSelectionAndGift(); $('#payment_notice').hide();" name="payment" {if $payment_method_cart == "Paypal"}checked="checked"{/if} id="paypal_p" value="modules/paypal/payment/submit.php" /></td>
			<td><label for="paypal_p">{l s='Paypal'}</label></td>
			<td></td>
			</tr>
			{if ($is_company == 0 && $is_registered == 1 && $payment_method_cart != "Contanti alla consegna") || $cookie->risk == 1}
			{else}
			<tr class="first_item">
			<td><input type="radio" onclick="updateCarrierSelectionAndGift(); $('#payment_notice').hide();" name="payment" id="cheque_p" {if $payment_method_cart == "Contanti alla consegna"}checked="checked"{/if} value="modules/cheque/payment.php" /></td>
			<td><label for="contanti_p">Contanti alla consegna (non si accettano assegni) </label></td>
			<td></td>
			</tr>
			{/if}
			
			{if ($is_company == 0 && $is_registered == 1 && $payment_method_cart != "Contrassegno") || $cookie->risk == 1}
			{else}
			<!-- <tr class="first_item">
			<td><input type="radio" onclick="updateCarrierSelectionAndGift(); $('#payment_notice').hide();" name="payment" id="contrassegno_p" {if $payment_method_cart == "Contrassegno"}checked="checked"{/if} value="modules/cashondeliverywithfee/validation.php" /></td>
			<td><label for="contrassegno_p">Contanti alla consegna (non si accettano assegni) <span id="spese_contrassegno" rel="{$id_group}" style="color:#da0f00; font-weight:bold"></span></label></td>
			<td></td>
			</tr> -->
			{/if}
			<tr class="alternate_item">
			<td><input type="radio" onclick="updateCarrierSelectionAndGift(); $('#payment_notice').hide();" name="payment" id="gestpay_p" {if $payment_method_cart == "Carta"}checked="checked"{/if} value="modules/gestpay/payment.php" /></td>
			<td><label for="gestpay_p">{l s='Credit card'}</label></td>
			<td></td>
			</tr>
			{if ($payment_method_default != "Bonifico" && $payment_method_default != 'Carta Amazon' && $payment_method_default != "Bonifico Bancario" && $payment_method_cart != "Bonifico anticipato" && $payment_method_cart != "Bonifico Anticipato" && $payment_method_default != "GestPay" && $payment_method_default != "nessuno" && $payment_method_default != "Carta" && $payment_method_default != "Paypal" && $payment_method_default != "PayPal" && $payment_method_default != "paypal" && $payment_method_default != "Contrassegno" && $payment_method_default != "" && $payment_method_default != $payment_method_cart) && $cookie->risk == 0}
			<tr class="alternate_item">
			<td><input type="radio" onclick="updateCarrierSelectionAndGift(); $('#payment_notice').hide();" name="payment" checked="checked" id="other_payment_p_2" value="modules/other_payment/payment.php" /></td>
			<td><label for="other_payment_p_2">{$payment_method_default}</label></td>
			<td></td>
			</tr>
			{/if}
			
			{if ($payment_method_cart != "Bonifico" && $payment_method_cart != "Bonifico anticipato" && $payment_method_cart != "Bonifico Anticipato"  && $payment_method_cart != "Bonifico Bancario" && $payment_method_cart != "GestPay"   && $payment_method_cart != "nessuno" && $payment_method_cart != "Carta" && $payment_method_cart != "Paypal" && $payment_method_default != "PayPal" && $payment_method_default != "paypal" && $payment_method_cart != "Contrassegno" && $payment_method_cart != "") && $cookie->risk == 0}
			<tr class="alternate_item">
			<td><input type="radio" onclick="updateCarrierSelectionAndGift(); $('#payment_notice').hide();" name="payment" checked="checked" id="other_payment_p" value="modules/other_payment/payment.php" /></td>
			<td><label for="other_payment_p">{$payment_method_cart}</label></td>
			<td></td>
			</tr>
			{/if}
			
			<tr><td></td><td><a href="{$base_dir_ssl}modules/paypal/express/submit.php">Paga con PayPal Express Checkout, pagamento veloce </a></td>
			{/if}
		</tbody>
	</table>
			
			
								{if $id_group == 3  || $id_group == 10 || $id_group == 12}
{else}
<!-- <br /><a href="#voucher" id="codici_coupon">{l s='Got coupon codes? Click here!'}</a> -->


	
{/if}
				

				
				
		