{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}



	<script type="text/javascript">
		var txtFree = "{l s='Free!'}";
	</script>


{if !$virtual_cart && $giftAllowed && $cart->gift == 1}
<script type="text/javascript">
{literal}
// <![CDATA[
    $('document').ready( function(){
		if ($('input#gift').is(':checked'))
			$('p#gift_div').show();
    });
//]]>
{/literal}
</script>
{/if}



{if $virtual_cart}
	<input id="input_virtual_carrier" class="hidden" type="hidden" name="id_carrier" value="0" />
{else}
	<h3 class="carrier_title">{l s='Choose your delivery method'}</h3>

	
	{if $trasporto_gratuito == 1}
	
		{l s='For this cart you have free shipping'}
		<input type="hidden" name="id_carrier" value="{$selected_carrier}" checked="checked" id="id_carrier{$selected_carrier}" />
	
	{else}
	
		<div id="HOOK_BEFORECARRIER">{if isset($carriers)}{$HOOK_BEFORECARRIER}{/if}</div>
		{if isset($isVirtualCart) && $isVirtualCart}
		<p class="warning">{l s='No carrier needed for this order'}</p>
		{else}
		{if $recyclablePackAllowed}
		<p class="checkbox">
			<input type="checkbox" name="recyclable" id="recyclable" value="1" {if $recyclable == 1}checked="checked"{/if} />
			<label for="recyclable">{l s='I agree to receive my order in recycled packaging'}.</label>
		</p>
		{/if}
		<p class="warning" id="noCarrierWarning" {if isset($carriers) && $carriers && count($carriers)}style="display:none;"{/if}>{l s='There are no carriers available that deliver to this address.'}</p>
		<script type="text/javascript">
		updateCarrierSelectionAndGift();
		</script>
	
		<table id="carrierTable" class="std" {if !isset($carriers) || !$carriers || !count($carriers)}style="display:none;"{/if}>
			<thead>
				<tr>
					<th class="carrier_action first_item"></th>
					<th class="carrier_name item">{l s='Carrier'}</th>
					
					<th class="carrier_price last_item">{l s='Price'}</th>
				</tr>
			</thead>
			<tbody>
			{if isset($carriers)}
				{foreach from=$carriers item=carrier name=myLoop}
					<tr class="{if $smarty.foreach.myLoop.first}first_item{elseif $smarty.foreach.myLoop.last}last_item{/if} {if $smarty.foreach.myLoop.index % 2}alternate_item{else}item{/if}">
						<td class="carrier_action radio">
							{if $carrier.name != 'Ritiro in sede Ezdirect'}
							<input type="radio" name="id_carrier" value="{$carrier.id_carrier|intval}" id="id_carrier{$carrier.id_carrier|intval}"  {if $opc}onclick="updateCarrierSelectionAndGift();"{/if} {if !($carrier.is_module AND $opc AND !$isLogged)}{if $carrier.id_carrier == $checked}checked="checked"{/if}{else}{/if} />
							{else}
							{/if}
						</td>
						<td class="carrier_name">
							<label for="id_carrier{$carrier.id_carrier|intval}">
								{if $carrier.img}<img src="{$carrier.img|escape:'htmlall':'UTF-8'}" alt="{$carrier.name|escape:'htmlall':'UTF-8'}" />{else}{$carrier.name|escape:'htmlall':'UTF-8'}{/if}
								
							</label>
						</td>
						
						<td class="carrier_price">
						{if $carrier.name != 'Ritiro in sede Ezdirect'}
								
							{if $carrier.price || ($transport_cost != '' && $transport_cost > 0)}
								<span class="price">
									{if $carrier.id_carrier == $edited_carrier}
										{if $transport_cost != ''}
											
											{if $priceDisplay == 1}{convertPrice price=$transport_cost}{else}{convertPrice price=$transport_cost}{/if}
										{else}
											
											{if $priceDisplay == 1}{convertPrice price=$carrier.price_tax_exc}{else}{convertPrice price=$carrier.price}{/if}
										
										{/if}
									{else}
										{if $priceDisplay == 1}{convertPrice price=$carrier.price_tax_exc}{else}{convertPrice price=$carrier.price}{/if}
									{/if}
								</span>
								{if $use_taxes}{if $priceDisplay == 1} {l s='(tax excl.)'}{else} {l s='(tax incl.)'}{/if}{/if}
							{else}
								{l s='Free!'}
							{/if}
						{else}
							<em>Gentile Cliente, in periodo di emergenza Covid-19 non si effettua il ritiro presso la nostra sede, ma soltanto la spedizione via corriere espresso.</em>
						{/if}
						</td>
					</tr>
				{/foreach}
				<tr id="HOOK_EXTRACARRIER">{$HOOK_EXTRACARRIER}</tr>
			{/if}
			</tbody>
		</table>
	{/if}
	
	<br /><br /><br />
	{if $id_group == 3  || $id_group == 10 || $id_group == 12}
	{else}
		<form action="{if $opc}{$link->getPageLink('order-opc.php', true)}{else}{$link->getPageLink('order.php', true)}{/if}" method="post" id="voucher" style="border:0px" >
			<fieldset id="coupon-fieldset" style="height:215px">
				<br /><br /><br />
				{l s='Got a coupon code?'} {l s='Insert it here:'}
				<br />
				<p style="border:0px" >
					<!-- <label for="discount_name">{l s='Insert it here:'}</label> -->
					
					<input type="hidden" name="submitDiscount" />
					
					<img src='{$img_ps_dir}coupon.jpg' alt='Coupon' title='Coupon' style="float:left; margin-top:3px; margin-left:-5px" />
					<input type="text" id="discount_name" name="discount_name" value="{if isset($discount_name) && $discount_name}{$discount_name}{/if}" style="margin-left:0px; font-size:16px; float:left; height:25px; width:146px; margin-top:3px"/>
					<input type="submit" name="submitAddDiscount" id="submitAddDiscount" value="{l s='Add'}" class="button" style="background-color:#000099; font-weight:bold; margin-top:3px; width:130px; height:27px; float:left; margin-left:-7px" />
					<div style="clear:both"></div>
				</p>
			{if $displayVouchers}
				<h4>{l s='Take advantage of our offers:'}</h4>
				<div id="display_cart_vouchers" style="border:0px" >
				{foreach from=$displayVouchers item=voucher}
					<span onclick="$('#discount_name').val('{$voucher.name}');return false;" class="voucher_name">{$voucher.name}</span> - {$voucher.description} <br />
				{/foreach}
				</div>
			{/if}
			</fieldset>
		</form>
	{/if}
	
	<div style="display: none;" id="extra_carrier"></div>

		{if $giftAllowed}
		<h3 class="gift_title">{l s='Gift'}</h3>
		<p class="checkbox">
			<input type="checkbox" name="gift" id="gift" value="1" {if $cart->gift == 1}checked="checked"{/if} onclick="$('#gift_div').toggle('slow');" />
			<label for="gift">{l s='I would like the order to be gift-wrapped.'}</label>
			<br />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			{if $gift_wrapping_price > 0}
				({l s='Additional cost of'}
				<span class="price" id="gift-price">
					{if $priceDisplay == 1}{convertPrice price=$total_wrapping_tax_exc_cost}{else}{convertPrice price=$total_wrapping_cost}{/if}
				</span>
				{if $use_taxes}{if $priceDisplay == 1} {l s='(tax excl.)'}{else} {l s='(tax incl.)'}{/if}{/if})
			{/if}
		</p>
		<p id="gift_div" class="textarea">
			<label for="gift_message">{l s='If you wish, you can add a note to the gift:'}</label>
			<textarea rows="5" cols="35" id="gift_message" name="gift_message">{$cart->gift_message|escape:'htmlall':'UTF-8'}</textarea>
		</p>
		{/if}
	{/if}
{/if}

