{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<script type="text/javascript" src="{$js_dir}etc/stars.js">
</script>




{if isset($products)}

	<!-- Products list -->
	<ul id="product_list" class="clear">
	{foreach from=$products item=product name=products}
	
		{if $smarty.get.id_category == 204 ||  $smarty.get.id_category == 86 || $smarty.get.id_category == 189}
		{else}
			<li class="ajax_block_product {if $smarty.foreach.products.first}first_item{elseif $smarty.foreach.products.last}last_item{/if} {if $smarty.foreach.products.index % 2}alternate_item{else}item{/if} clearfix">
			
			<div class="center_block">
				<div class="center_block_left">
				{if $product.location == 'bundle'}
				<div class="kit-listing">
				<img src="{$img_ps_dir}kit.png" alt="Kit" title="Kit" width="45" height="45" />
				</div>
				{/if}
				{if $product.cheapnet == 'cheapnet_6'}
				<div class="cheapnet">
				<a href='https://www.ezdirect.it/cheapnetcoupons.php'><img src="{$img_ps_dir}cheapnet-300.png" alt="Con questo prodotto hai diritto a un Coupon Cheapnet da 300 minuti gratis" title="Con questo prodotto hai diritto a un Coupon Cheapnet da 300 minuti gratis" width="57" height="57" /></a>
				</div>
				{else if $product.cheapnet == 'cheapnet_30'}
				<div class="cheapnet">
				<a href='https://www.ezdirect.it/cheapnetcoupons.php'><img src="{$img_ps_dir}cheapnet-600.png" alt="Con questo prodotto hai diritto a un Coupon Cheapnet da 600 minuti gratis" title="Con questo prodotto hai diritto a un Coupon Cheapnet da 600 minuti gratis" width="57" height="57" /></a>
				</div>

				{else}
				{/if}
				
				{php}
					/*
					{if $product.activate_coupon == 1}
					<div class="coupon"><a href="{$product.link|escape:'htmlall':'UTF-8'}" title="{$product.name|escape:'htmlall':'UTF-8'}"><img src="{$img_ps_dir}coupon.png" alt="Coupon" title="Coupon" /></a></div>
					{else}
					{/if}
					*/
				{/php}
				
				{if $product.condition == 'refurbished'}
					<div style="position:absolute; top:0px; left:0px"><img style="width:40px" src="https://www.ezdirect.it/img/ricondizionato.png" title="Ricondizionato" alt="Ricondizionato" /></div>
				{/if}
				
				<!-- {if $product.migliorprezzo_link == 1 && $product.virtual == 0 && $product.price_tax_exc > 0 && ($product.fuori_produzione != 1 && !preg_match("/fuori produzione/i", $product.description_short))} -->
						
				<!--	{if $product.id_manufacturer == 133} <div class="ribbon_gold" style="right:5px; cursor:pointer" onclick="window.location = '{$product.link|escape:'htmlall':'UTF-8'}'"><span>Miglior prezzo</span></div> {/if} -->
					
				<!-- {/if} -->
				
				{if $product.abilita_banda == 1}
					<div class="ribbon" style="right:5px; cursor:pointer" onclick="window.location = '{$product.link|escape:'htmlall':'UTF-8'}'"><span>Offerta speciale</span></div>
				{/if}				

{if $product.fuori_produzione == 1  || preg_match("/fuori produzione/i", $product.description_short)}
				<div class="ribbon" style="right:5px; cursor:pointer" onclick="window.location = '{$product.link|escape:'htmlall':'UTF-8'}'"><span>{l s='Out of production'}</span></div>
			{/if}
					<a class="product_img_link" href="{$product.link|escape:'htmlall':'UTF-8'}" title="{$product.name|escape:'htmlall':'UTF-8'}"><img class="product_img_link_2" src="{if $product.id_product|strpos:"b"}https://www.ezdirect.it/img/b/{$product.id_image}-large.jpg{else}{$link->getImageLink($product.link_rewrite, $product.id_image, 'medium')}{/if}" alt="{$product.legend|escape:'htmlall':'UTF-8'}"  {if $product.secondary_image != ''}onmouseover="this.src='{$link->getImageLink($product.link_rewrite, $product.secondary_image, 'medium')}'" onmouseout="this.src='{$link->getImageLink($product.link_rewrite, $product.id_image, 'medium')}'"{/if} width="160" height="160" /></a>
					{if $search_products}<br />
						<span class="improve-your-search">
						<a href="{$link->getCategoryLink($product.id_category_default,$link->getCatRewrite($product.id_category_default))}">{l s='Improve your search'}</a>
						</span>
					{/if}
				</div>
					
				<h3 class="listing_titles">{if isset($product.new) && $product.new == 1}{/if}<a class="product_name_title" href="{$product.link|escape:'htmlall':'UTF-8'}" title="{$product.name|escape:'htmlall':'UTF-8'}" {if $product.id_product|strpos:"b"}id="bundle_name_title_bundle-{$product.id_product|replace:'b':''}"{/if}>{$product.name|escape:'htmlall':'UTF-8'}</a></h3>
				<p class="product_codes">
				{if $product.reference}
					{l s='Reference:'} {$product.reference|escape:'htmlall':'UTF-8'}
				{else}{/if}
				{if $product.supplier_reference}
					&nbsp;&nbsp;&nbsp;&nbsp; {l s='SKU reference:'} {$product.supplier_reference|escape:'htmlall':'UTF-8'}
				{else}{/if}
					
				</p>
				<p class="product_desc" style="font-size:12px"><a href="{$product.link|escape:'htmlall':'UTF-8'}" title="{$product.description_short|strip_tags:'UTF-8'|truncate:140:'...'}">{$product.description_short|truncate:140:'...'|strip_tags:'UTF-8'}</a>
				<br /><br />
				{if $product.punti_di_forza}
					<span class="punti-di-forza">{$product.punti_di_forza|escape:'UTF-8'|replace:'4*':'<img src="https://www.ezdirect.it/images/4.gif" alt="4" title="4"  width="64" height="12" />'|replace:'5*':'<img src="https://www.ezdirect.it/images/5.gif" alt="5" title="5" width="64" height="12" />'|replace:'3*':'<img src="https://www.ezdirect.it/images/3.gif" alt="3" title="3" width="64" height="12" />'|replace:'2*':'<img src="https://www.ezdirect.it/images/2.gif" alt="2" title="2" width="64" height="12" />'|replace:'1*':'<img src="https://www.ezdirect.it/images/1.gif" alt="1" title="1" width="64" height="12" />'|replace:'s&igrave;':'<img src="https://www.ezdirect.it/images/si.gif" alt="s&igrave;" title="s&igrave;" width="14" height="14" />'}</span>
					
					<br /><br />
				{else}
				{/if}
				{if $product.login_for_offer == 1 && !$cookie->isLogged()}
					<a rel="nofollow" href="{$link->getPageLink('authentication.php', true)}"><strong style="background-color: #fff785; color:#ff0000" class="listing_titles">Accedi per vedere il prezzo scontato</strong></a>
					
				{/if}
					
				<div id="bottom-line">
	
				
					
				<div class="reviews-line">
					{if $product.location == 'bundle'}
						{if $product.real_quantity > 0}
							<img src='{$img_ps_dir}green_alert.gif' alt='{l s='Available'}' />
							<strong style='color:#048c04'>{l s='Available'}</strong>
						{else}
							<img src='{$img_ps_dir}orange_alert.gif' alt='{l s='Periodical'}' />
							<strong style='color:#ed8e00'>{l s='Periodical'}</strong>
						{/if}
					{else if $product.fuori_produzione == 0 && $product.price > 0 || ($product.fuori_produzione == 1 && $product.price > 0 && $product.real_quantity > 0)}
						{if $product.id_category_default == 119}
							<img src='{$img_ps_dir}green_alert.gif' alt='{l s='Available'}' />
							<strong style='color:#048c04'>{l s='Available'}</strong>
						{else}
							{if $product.real_quantity > 0}
								
								
								<img src='{$img_ps_dir}green_alert.gif' alt='{l s='Available'}' />
								<strong style='color:#048c04'>{l s='Available'}</strong>
								
							{else}
								<img src='{$img_ps_dir}orange_alert.gif'  alt='{l s='Periodical'}' />
								<strong style='color:#ed8e00'>{l s='Periodical'}</strong>
							{/if}
						{/if}
{else}
<div style="width:110px">&nbsp;</div>
					{/if}
				</div>
							
					<div class="reviews-line">
						<a href="{$product.link|escape:'htmlall':'UTF-8'}#idTab5" rel="nofollow"><span class="stars">{$product.averageTotal}</span> <span class="total_reviews listing_titles">({$product.total_reviews} {l s='reviews'})</span></a>
					</div>
					<div class="reviews-line comparator-item">
						{if isset($comparator_max_item) && $comparator_max_item}
							{if $product.location == 'bundle'}
							{else}
								<input type="checkbox" style="margin-top:2px" class="comparator" id="comparator_item_{$product.id_product}" value="comparator_item_{$product.id_product}" {if isset($compareProducts) && in_array($product.id_product, $compareProducts)}checked{/if}/> <label for="comparator_item_{$product.id_product}"></label> {l s='Compare'}
							{/if}
						{/if}
					</div>
					{if $product.migliorprezzo_link == 1 && $product.price_tax_exc > 0 && ($product.fuori_produzione != 1 && !preg_match("/fuori produzione/i", $product.description_short))}
						
						<!-- <div class="reviews-line get-best-price"><a rel="nofollow" href="https://www.ezdirect.it/modules/formprevendita/form.php?category={$product.id_product}"><img src='{$img_ps_dir}logo-miglior-prezzo.png' alt='Ottieni miglior prezzo' title='Ottieni miglior prezzo' /><strong style="color:#ff0000" class="listing_titles">Ottieni miglior prezzo</strong></a></div> -->
					{/if}
					
					
				<div style="clear:both"></div>
				</div>
				</p>
			</div>		
					<div class='acquista-listing'>
					
						{if $product.listino>0 && $product.price >0 && $product.fuori_produzione == 0 && $product.listino>$product.price_tax_exc || ($product.fuori_produzione == 1 && $product.price > 0 && $product.quantity > 0)}
							<span style="color:#383838">{l s='List price'}</span>  <span style="text-decoration:line-through; color:#383838">{convertPrice price=$product.listino}</span> {l s='t.e.'}<br />
						{else}
						{/if}
						{if ($product.quantity_discount && $product.price >0 && $product.fuori_produzione == 0) || ($product.fuori_produzione == 1 && $product.price > 0 && $product.quantity_discount && $product.quantity > 0)}
							
								{assign var="i" value="0"}
								{assign var=numItems value=$product.quantity_discount|@count}
								{foreach from=$product.quantity_discount item='quantity_discount' name='product.quantity_discounts'}
									
									{if $i == 0}				
										{l s='From'}
								
										<span class="price" style="display: inline;">{convertPrice price=$quantity_discount.price * (1 - $quantity_discount.reduction)|floatval}</span>  <span style="color:red">{l s='t.e.'}</span>
									{else}	
									{/if}
									{$i = $i+1}
								{/foreach}
							
						
						{else if ($product.price >0 && $product.fuori_produzione == 0) || ($product.fuori_produzione == 1 && $product.price > 0 && $product.quantity > 0)}
								{if isset($product.show_price) && $product.price > 0 && $product.show_price && !isset($restricted_country_mode)}<span class="price" style="display: inline;">{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}</span>  <span style="color:red">{l s='t.e.'}{if $product.prezzospeciale == 1}*{/if}</span><br />{/if}
						
							{/if}
							
							<p>	
							{if $cookie->id_default_group == 3  || $cookie->id_default_group == 10  || $cookie->id_default_group == 12}
							{else} 
								{if $product.freeshipping == true || $product.price_tax_exc > 4999999999999999}
									<strong>{l s='Free shipping'}</strong><br />
								{/if}
							{/if}	
							
							{if $product.prezzospeciale == 1}
								<div>
								{if $product.pezzi_offerta > 0}
									<span style="color:red"><strong>*</strong></span><strong style="color:#000000">{$product.pezzi_offerta}</strong>
									{if $product.pezzi_offerta == 1}
										<strong>{l s='piece'}</strong>
									{else}
										<strong>{l s='pieces'}</strong>
									{/if}
									<span style="color:red">
									<strong>{l s='in special price until '} {$product.scontofinoa}</strong></span>
								{else}
									<span style="color:red"><strong>{l s='Special price until '} {$product.scontofinoa}</strong></span>
								{/if}
								</div>
							{/if}

						</p>
						
						{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}<span class="on_sale">{l s='On sale!'}</span>
						{elseif isset($product.quantity_discounts)}<span class="discount">{l s='Reduced price!'}</span>{/if}
							{if isset($product.online_only) && $product.online_only}<span class="online_only">{l s='Online only!'}</span>{/if}
							{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
						
						{if $product.fuori_produzione == 0 && $product.quantity_discount && $product.price >0 || ($product.fuori_produzione == 1 && $product.price > 0 && $product.quantity_discount && $product.quantity > 0)}
								<img style="border:0px; margin-left:-12px" class="sconti_quantita" src="{$img_ps_dir}sconti-quantita.gif" alt="{l s='Quantity discounts'}" title="{l s='Quantity discounts'}" width="149" height="16" />
							{/if}
						{/if}
							
						<br />	<br />
						
						{if $product.migliorprezzo_link == 1}
						<!-- <a href="https://www.ezdirect.it/modules/formprevendita/form.php?category={$product.id_product}" rel="nofollow">{l s='Ottieni il miglior prezzo'}</a> -->
						{/if}
						
						<br />	<br />
						</div>			
					<div class="right_block">
					{if $product.price_tax_exc == 0 || ($product.fuori_produzione == 1 && $product.quantity == 0)}
					
						{if $product.fuori_produzione == 1 || preg_match("/fuori produzione/i", $product.description_short)}  
							<p class="fp">
							<strong>{l s='This product is out of production'}.</strong><br /><br />
							<a href="{$link->getCategoryLink($product.id_category_default,$link->getCatRewrite($product.id_category_default))}">{l s='Search for similar products'}</a>
							
							</p>
						{else}
							<p class="fp">
							
							
							<a href="https://www.ezdirect.it/modules/formprevendita/form.php?category={$product.id_product}" rel="nofollow">{l s='Ask for a quotation'}</a>
							</p>
						
						
						{/if}
					

					{else}
					
				
						
						
					
					
					
						
						{if $product.fuori_produzione == 0 && ($product.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $product.available_for_order && !isset($restricted_country_mode) && $product.minimal_quantity <= 1 && $product.customizable != 2 && !$PS_CATALOG_MODE || ($product.fuori_produzione == 1 && $product.price > 0 && $product.quantity > 0)}
							
								{if $product.location == 'bundle'}
								<div class="quantity-div" style="position:absolute; right:-2px; bottom:41px">			
									<div style="float:left; padding-top:4px;">{l s='Quantity :'}</div>
									<div class="quantity_input_div">
										<input type="text" name="qty_bdl_{$product.id_product|replace:'b':''}" id="qty_bdl_{$product.id_product|replace:'b':''}" class="text quantity_input" value="{if isset($quantityBackup)}{$quantityBackup|intval}{else}1{/if}" size="2" maxlength="3" />
										<div class="quantity_input_buttons">
											<span class="span_up" onclick="quantity_cart_add('qty_bdl_{$product.id_product|replace:'b':''}');">+</span>
											<span class="span_down" onclick="quantity_cart_rmv('qty_bdl_{$product.id_product|replace:'b':''}');">-</span>
										</div>
									</div>
									<div style="clear:both"></div>
								</div>	
									<input type="hidden" value="https://www.ezdirect.it/img/b/{$product.id_image}-small.jpg" id="bundle_img_link_bundle-{$product.id_product|replace:'b':''}" />
								
									<div style="display:block; height:5px"></div>
									<div class="acquista-listing-button">
										
										<form id="buy_block" action="{$link->getPageLink('cart.php')}" method="post">
										<input type="hidden" name="bundle" value="{$product.id_product|replace:'b':''}" />
										
										<input type="submit" value="{l s='Add to cart'}" title="{l s='Add to cart'}" id="bundle_{$product.id_product|replace:'b':''}" class="ajax_add_to_cart_button" style="width:140px; font-family: Arial; background-color:#e62026; color:#ffffff; border:1px solid #bdc2c9" />
									</form>
									
									</div><div style="clear:both"></div>
								{else}
								
								
								
								<div class="quantity-div" style="position:absolute; right:-2px; bottom:41px">
									<div style="float:left; padding-top:4px;">{l s='Quantity :'}</div>
								
									
									<div class="quantity_input_div">
										<input type="text" name="ajax_qty_to_add_to_cart[{$product.id_product|intval}]" id="quantity_wanted_{$product.id_product|intval}" class="text quantity_input" value="{if isset($quantityBackup)}{$quantityBackup|intval}{else}1{/if}" size="2" maxlength="3" />
										<div class="quantity_input_buttons">
											<span class="span_up" onclick="quantity_cart_add('quantity_wanted_{$product.id_product|intval}');">+</span>
											<span class="span_down" onclick="quantity_cart_rmv('quantity_wanted_{$product.id_product|intval}');">-</span>
										</div>
									</div>
									<div style="clear:both"></div>
								</div>
									<div style="display:block; height:5px"></div>
									<div class="acquista-listing-button">
									
									<a rel="nofollow" class="ajax_add_to_cart_button" style="background-color:#e62026; color:#ffffff; border:1px solid #bdc2c9" name="{$product.id_product|intval}" id="ajax_id_product_{$product.id_product|intval}" href="{$link->getPageLink('cart.php')}?add&amp;id_product={$product.id_product|intval}{if isset($static_token)}&amp;token={$static_token}{/if}" title="{l s='Add'} {$product.name} {l s='to cart'}">{l s='Add to cart'}</a>
									</div><div style="clear:both"></div>
								{/if}
							
						{/if}
						
					
					{/if}		
			
			
			</div>	
				
				
				
				<div class="d_block">







				</div>
			</li>
		{/if} 
		{if isset($product.bundle_layered) && $product.bundle_layered != ""}
			{$product.bundle_layered}
		
		{else}
		{/if}
	{/foreach}
	</ul>
	<!-- /Products list -->
{/if}
