<hr />
<div class="row section-header wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;"> </div>
<!-- /wp:html -->
<!-- wp:html -->
<section class="row">
<div class="container">
<div class="row">
<h2 style="text-align: center;"><strong>Soluzioni per comunicare? Ezdirect!</strong></h2>
<ul>
	<li><span style="font-family: georgia, palatino;">Prima azienda per la vendita "on line" con supporto prevendita, installazione e postvendita in tutta Italia nella telefonia e audioconferenza professionale.<br />
</span></li>
	<li><span style="font-family: georgia, palatino;">45.000 clienti Italia (90% aziende - 7% rivenditori - 3% privati)</span></li>
	<li><span style="font-family: georgia, palatino;">Presenti su marketplace IT/FR/ES/DE/UK</span></li>
	<li><span style="font-family: georgia, palatino;">Feedback dai clienti certificati (4,9/5 - By Ekomi)</span></li>
	<li><span style="font-family: georgia, palatino;">Garanzia del miglior prezzo <br />
</span></li>
	<li>Prova gratuita (try &amp; buy)</li>
	<li><span style="font-family: georgia, palatino;">Pagamento differito e noleggio (renting)</span></li>
	<li><span style="font-family: georgia, palatino;">Gestione postvendita diretto</span></li>
	<li><span style="font-family: georgia, palatino;">Consegne veloci</span></li>
	<li><span style="font-family: georgia, palatino;">Preventivi personalizzati con garanzia soddisfatto o rimborsato al 100%</span></li>
	<li><span style="font-family: georgia, palatino;">Consulente esperto dedicato</span></li>
	<li><span style="font-family: georgia, palatino;">Staff di 11 persone (di cui 4 per progettazione e supporto tecnico)</span></li>
	<li><span style="font-family: georgia, palatino;">Installazione in tutta Italia con partners autorizzati</span></li>
	<li><span style="font-family: georgia, palatino;">Rivenditore autorizzato (Sennheiser, Jabra, Yealink, Snom, Yeastar, Gigaset, 3CX, Polycom, Avaya, Fanvil...)</span></li>
</ul>
</div>
</div>
</section>
<section id="Informazioni e contatti" class="row ulteriori">
<!-- <div class="container"><a href="https://www.ezdirect.it/preventivi-quotazioni-miglior-prezzo-cuffie-centralini"><img class="aligncenter" src="https://www.ezdirect.it/blog/wp-content/themes/WhosWho/INFO2.jpg" alt="" width="70%" /></a></div> -->
<section class="row newsletter">
<div class="container">
<div class="row section-header wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
<h2 style="text-align: center;"><strong>Recensioni</strong></h2>
</div>
<script type="text/javascript">
{literal}
// Instantiate the Bootstrap carousel
$('.multi-item-carousel').carousel({
  interval: false
});

// for every slide in carousel, copy the next slide's item in the slide.
// Do the same for the next, next item.
$('.multi-item-carousel .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  if (next.next().length>0) {
    next.next().children(':first-child').clone().appendTo($(this));
  } else {
  	$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
  }
});
{/literal}
</script>
<div class="row">
<div class="col-md-12">
<div id="theCarousel" class="carousel slide multi-item-carousel">
<div class="carousel-inner" style="padding-left: 0%;">
<div class="item active">
<div class="col-xs-4" style="width: 33%; text-align: center;"><a href="https://www.ekomi.it/opinioni-ezdirectit.html"><img class="img-responsive" style="display: block; margin: 0 auto;" src="https://connect.ekomi.de/widget/44D97E5AAD9DE69-1.gif" /></a></div>
<div class="col-xs-4" style="width: 33%; text-align: center;"><a href="https://www.ekomi.it/opinioni-ezdirectit.html"><img class="img-responsive" style="display: block; margin: 0 auto;" src="https://connect.ekomi.de/widget/44D97E5AAD9DE69-2.gif" /></a></div>
<div class="col-xs-4" style="width: 33%; text-align: center;"><a href="https://www.ekomi.it/opinioni-ezdirectit.html"><img class="img-responsive" style="display: block; margin: 0 auto;" src="https://connect.ekomi.de/widget/44D97E5AAD9DE69-3.gif" /></a></div>
</div>
<div class="item">
<div class="col-xs-4" style="width: 33%; text-align: center;"><a href="https://www.ekomi.it/opinioni-ezdirectit.html"><img class="img-responsive" style="display: block; margin: 0 auto;" src="https://connect.ekomi.de/widget/44D97E5AAD9DE69-4.gif" /></a></div>
<div class="col-xs-4" style="width: 33%; text-align: center;"><a href="https://www.ekomi.it/opinioni-ezdirectit.html"><img class="img-responsive" style="display: block; margin: 0 auto;" src="https://connect.ekomi.de/widget/44D97E5AAD9DE69-5.gif" /></a></div>
<div class="col-xs-4" style="width: 33%; text-align: center;"><a href="https://www.ekomi.it/opinioni-ezdirectit.html"><img class="img-responsive" style="display: block; margin: 0 auto;" src="https://connect.ekomi.de/widget/44D97E5AAD9DE69-6.gif" /></a></div>
</div>
<div class="item">
<div class="col-xs-4" style="width: 33%; text-align: center;"><a href="https://www.ekomi.it/opinioni-ezdirectit.html"><img class="img-responsive" style="display: block; margin: 0 auto;" src="https://connect.ekomi.de/widget/44D97E5AAD9DE69-7.gif" /></a></div>
<div class="col-xs-4" style="width: 33%; text-align: center;"><a href="https://www.ekomi.it/opinioni-ezdirectit.html"><img class="img-responsive" style="display: block; margin: 0 auto;" src="https://connect.ekomi.de/widget/44D97E5AAD9DE69-8.gif" /></a></div>
<div class="col-xs-4" style="width: 33%; text-align: center;"><a href="https://www.ekomi.it/opinioni-ezdirectit.html"><img class="img-responsive" style="display: block; margin: 0 auto;" src="https://connect.ekomi.de/widget/44D97E5AAD9DE69-9.gif" /></a></div>
</div>
<!-- add  more items here --></div>
<!-- Left and right controls --> <a class="left carousel-control" style="width: 5%;" href="#theCarousel" data-slide="prev"> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" style="width: 5%;" href="#theCarousel" data-slide="next"> <span class="sr-only">Next</span> </a></div>
<div> </div>
</div>
</div>
</div>
</section>
<!-- /wp:html -->
<p style="text-align: center; margin-top: 2%;">Ezdirect srl - Via Nerino Garbuio (SS1 Aurelia) snc - 54038, Montignoso, Massa Carrara - Tel. (+39) 0585821163 - <a href="https://www.ezdirect.it">www.ezdirect.it</a></p>
<p style="text-align: center; margin-top: -1%;">P.IVA e CF 01164670455 - Capitale Sociale 80.000,00 € i.v. - Copyright - Tutti i diritti riservati</p>
<p style="text-align: center;"><a href="https://www.youtube.com/user/ezdirectit" target="_blank" rel="noopener noreferrer"><img src="https://www.ezdirect.it/blog/wp-content/themes/WhosWho/youtube.jpg" /></a> <a href="https://it-it.facebook.com/pages/Ezdirect-srl/53805768063" target="_blank" rel="noopener noreferrer"><img src="https://www.ezdirect.it/blog/wp-content/themes/WhosWho/face.jpg" /></a> <a href="https://twitter.com/ezdirect_it" target="_blank" rel="noopener noreferrer"><img src="https://www.ezdirect.it/blog/wp-content/themes/WhosWho/twit.jpg" /></a> <a href="http://www.linkedin.com/company/ezdirect-srl" target="_blank" rel="noopener noreferrer"> <img src="https://www.ezdirect.it/blog/wp-content/themes/WhosWho/pl/logo-linkedin-round.png" /></a></p>
<!-- /wp:html --><!-- /wp:html -->
<!-- wp:html -->
<p><script src="https://www.ezdirect.it/blog/wp-content/themes/WhosWho/pl/assets/jquery-2.js"></script></p>
<!-- /wp:html -->
<!-- wp:html -->
<p><script src="https://www.ezdirect.it/blog/wp-content/themes/WhosWho/pl/assets/bootstrap.js"></script></p>
<!-- /wp:html -->
<!-- wp:html -->
<p><script src="https://www.ezdirect.it/blog/wp-content/themes/WhosWho/pl/assets/owl.js"></script></p>
<!-- /wp:html -->
<!-- wp:html -->
<p><script src="https://www.ezdirect.it/blog/wp-content/themes/WhosWho/pl/assets/jquery.js"></script></p>
<!-- /wp:html -->
<!-- wp:html -->
<p><script src="https://www.ezdirect.it/blog/wp-content/themes/WhosWho/pl/assets/jquery_002.js"></script></p>
<!-- /wp:html -->
<!-- wp:html -->
<p><script src="https://www.ezdirect.it/blog/wp-content/themes/WhosWho/pl/assets/bootstrap-select.js"></script></p>
<!-- /wp:html -->
<!-- wp:html -->
<p><!-- <script src="https://www.ezdirect.it/blog/wp-content/themes/WhosWho/pl/assets/js_002"></script> --></p>
<!-- /wp:html -->
<!-- wp:html -->
<p><script src="https://www.ezdirect.it/blog/wp-content/themes/WhosWho/pl/assets/wow.js"></script></p>
<!-- /wp:html -->
<!-- wp:html -->
<p><script src="https://www.ezdirect.it/blog/wp-content/themes/WhosWho/pl/assets/validate.js"></script></p>
<!-- /wp:html -->
<!-- wp:html -->
<p><script src="https://www.ezdirect.it/blog/wp-content/themes/WhosWho/pl/assets/pre-order.js"></script></p>
<!-- /wp:html -->
<!-- wp:html -->
<p><script src="https://www.ezdirect.it/blog/wp-content/themes/WhosWho/pl/assets/subscribe.js"></script></p>
<!-- /wp:html -->
<!-- wp:html -->
<p><script src="https://www.ezdirect.it/blog/wp-content/themes/WhosWho/pl/assets/contact.js"></script></p>
<!-- /wp:html -->
<!-- wp:html --><!-- /wp:html -->
<!-- wp:html --><!-- /wp:html -->
<div id="fb-root"> </div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/it_IT/sdk.js#xfbml=1&version=v3.3"></script> <!-- wp:html -->
<p><script src="https://www.ezdirect.it/blog/wp-content/themes/WhosWho/pl/assets/main.js"></script></p>
<!-- /wp:html -->
<p>&nbsp;</p>
<p><a onclick="$('#id01').show(); $('#preventivo_v').hide();" href="javascript:void(0)"><img id="preventivo_v" style="cursor: pointer; position: fixed; bottom: 35%; right: 0px; z-index: 999999;" src="https://www.ezdirect.it/img/contattaci-v.png" /></a></p> 
<div id="id01" class="modal-cv" style="display: none;">
<form id="contatto_form_n" class="modal-content-cv animatemodal-cv" action="https://www.ezdirect.it/modules/formprevendita/form.php?step=2&amp;cv=y" method="post">
<div class="imgcontainermodal-cv"><span class="closemodal-cv" onclick="$('#id01').hide(); $('#preventivo_v').show();" title="Close Modal">×</span></div>
<div class="containermodal-cv">
<div style="font-size: 110%;">
<p style="text-align: center; font-size: 11px;"><strong style="font-size: 210%;">Contattaci</strong></p>
<br style="margin-top: -3%;" />
Fornisci il tuo recapito e ti chiameremo per fornirti tutti i chiarimenti e predisporre insieme una quotazione su misura. <br />
<br />
</div>
<table style="width: 100%;">
<tbody>
<tr>
<td><input class="input-modal-cv" name="firstname" required="" type="text" placeholder="Nome*" /></td>
</tr>
<tr>
<td><input class="input-modal-cv" name="lastname" required="" type="text" placeholder="Cognome*" /></td>
</tr>
<tr>
<td><input class="input-modal-cv" name="company" required="" type="text" placeholder="Azienda*" /></td>
</tr>
<tr>
<td><input class="input-modal-cv" name="email" required="" type="email" placeholder="Email*" /></td>
</tr>
<tr>
<td><input class="input-modal-cv" name="phone" required="" type="text" placeholder="Telefono*" /></td>
</tr>
<tr>
<td><input class="input-modal-cv" name="vat_number" type="text" placeholder="Partita IVA" /></td>
</tr>
</tbody>
</table>
<div id="comunica_esigenze" style="color: #000; background-color: #fff; width: 100%; padding: 4px 7px; margin: 8px 0;"><strong>Comunicaci le tue esigenze*</strong><br />
<input name="category" type="hidden" value="landing page telecamere" /> <input name="source" type="hidden" value="landing page telecamere" /> <textarea class="input-modal-cv" name="message" required=""></textarea></div>
<input style="margin-left: 0px;" name="acconsento" required="" type="checkbox" /> Spuntando questa casella acconsento di contattarmi in merito a questo prodotto/prova e al trattamento dei dati personali secondo la nuova normativa. <a style="color: #ff6c00;" href="https://www.ezdirect.it/guide/6-informativa-sulla-privacy" target="_blank" rel="noopener noreferrer">Clicca qui per visualizzare l'informativa sulla privacy</a> <button id="submitMessage" class="input-modal-cv" name="submitMessage" type="submit">Invia richiesta</button></div>
</form>
</div>
</section>

<script type="text/javascript">
{literal}
 var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-19658774-1']);
		  _gaq.push (['_gat._anonymizeIp']);
		  _gaq.push(['_trackPageview']);

		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		 {/literal} 
</script>

</body>
</html>

