$(document).ready(function() {
				if(document.getElementById('is_company_1').checked) {
					$("#company_p").show();
					$("#vat_number_p").show();
					$("#employees_number_p").show();
					$("#tax_code_az_p").show();
					$("#tax_code_help").show();
				}
				else {
					$("#company_p").hide();
					$("#vat_number_p").hide();
					$("#employees_number_p").hide();
					$("#tax_code_az_p").hide();
					$("#tax_code_help").hide();
				}
					$("input[name$='is_company1']").click(function() {
						var is_company = $(this).val();
					
						if(is_company == 1) {
							$("#company_p").show();
							$("#vat_number_p").show();
							$("#employees_number_p").show();
							$("#tax_code_az_p").show();
							$("#tax_code_help").show();
						}
						else {
							$("#company_p").hide();
							$("#vat_number_p").hide();
							$("#tax_code_az_p").hide();
							$("#employees_number_p").hide();
							$("#tax_code_help").hide();
						}
					});
				});