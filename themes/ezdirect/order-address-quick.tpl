{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14430 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $opc}
	{assign var="back_order_page" value="order-opc.php"}
{else}
	{assign var="back_order_page" value="order.php"}
{/if}

{*
** Retro compatibility for PrestaShop version < 1.4.2.5 with a recent theme
** Syntax smarty for v2
*}

{* Will be deleted for 1.5 version and more *}
{if !isset($formatedAddressFieldsValuesList)}
	{$ignoreList.0 = "id_address"}
	{$ignoreList.1 = "id_country"}
	{$ignoreList.2 = "id_state"}
	{$ignoreList.3 = "id_customer"}
	{$ignoreList.4 = "id_manufacturer"}
	{$ignoreList.5 = "id_supplier"}
	{$ignoreList.6 = "date_add"}
	{$ignoreList.7 = "date_upd"}
	{$ignoreList.8 = "active"}
	{$ignoreList.9 = "deleted"}	
	
	{* PrestaShop 1.4.0.17 compatibility *}
	{if isset($addresses)}
		{foreach from=$addresses key=k item=address}
			{counter start=0 skip=1 assign=address_key_number}
			{$id_address = $address.id_address}
			{foreach from=$address key=address_key item=address_content}
				{if !in_array($address_key, $ignoreList)}
					{$formatedAddressFieldsValuesList.$id_address.ordered_fields.$address_key_number = $address_key}
					{$formatedAddressFieldsValuesList.$id_address.formated_fields_values.$address_key = $address_content}
					{counter}
				{/if}
			{/foreach}
		{/foreach}
	{/if}
{/if}

<script type="text/javascript">
// <![CDATA[

	var formatedAddressFieldsValuesList = new Array();

	{foreach from=$formatedAddressFieldsValuesList key=id_address item=type}
		formatedAddressFieldsValuesList[{$id_address}] =
		{ldelim}
			'ordered_fields':[
				{foreach from=$type.ordered_fields key=num_field item=field_name name=inv_loop}
					{if !$smarty.foreach.inv_loop.first},{/if}"{$field_name}"
				{/foreach}
			],
			'formated_fields_values':{ldelim}
					{foreach from=$type.formated_fields_values key=pattern_name item=field_name name=inv_loop}
						{if !$smarty.foreach.inv_loop.first},{/if}"{$pattern_name}":"{$field_name|escape:'htmlall':'UTF-8'}"
					{/foreach}
				{rdelim}
		{rdelim}
	{/foreach}

	function getAddressesTitles()
	{ldelim}
		return {ldelim}
						'invoice': "<div style='margin-top:-5px'>Il tuo indirizzo di fatturazione<br /><span style='font-weight:normal'>(pu&ograve; coincidere con indirizzo di consegna)</span></div>",
						'delivery': "{l s='Your delivery address'}"
			{rdelim};

	{rdelim}
	
	function buildAddressBlock(id_address, address_type, dest_comp)
	{ldelim}
		var adr_titles_vals = getAddressesTitles();
		var li_content = formatedAddressFieldsValuesList[id_address]['formated_fields_values'];
		var ordered_fields_name = ['title'];

		ordered_fields_name = ordered_fields_name.concat(formatedAddressFieldsValuesList[id_address]['ordered_fields']);
		ordered_fields_name = ordered_fields_name.concat(['update']);
		
		dest_comp.html('');

		li_content['title'] = adr_titles_vals[address_type];
		li_content['update'] = '<a href="{$link->getPageLink('address.php', true)}?id_address='+id_address+'&amp;back={$back_order_page}?step=1{if $back}&mod={$back}{/if}" title="{l s='Update'}"></a>';

		appendAddressList(dest_comp, li_content, ordered_fields_name);
	{rdelim}

	function appendAddressList(dest_comp, values, fields_name)
	{ldelim}
		for (var item in fields_name)
		{ldelim}
			var name = fields_name[item];
			var value = getFieldValue(name, values);
			if (value != "")
			{ldelim}
				var new_li = document.createElement('li');
				new_li.className = 'address_'+ name;

				
				if(name == 'firstname' || name == 'lastname') { 
				new_li.className = '';
				}
				else {
				new_li.className = 'address_'+ name;
				}
				
				if(name == 'firstname') { 
				new_li.innerHTML = '<table style="margin-left:-10px; margin-bottom:-18px;   font-size:12px"><tr><td style="width:80px">Nome</td> <td>' + getFieldValue(name, values) + '</td></tr></table>';
				}
				else if(name == 'cognome') { 
				new_li.innerHTML = '<table style="margin-left:-10px; margin-bottom:-18px;   font-size:12px"><tr><td style="width:80px">Cognome</td> <td> ' + getFieldValue(name, values) + '</td></tr></table>';
				}
				else if(name == 'address1') { 
				new_li.innerHTML = '<table style="margin-left:-10px; margin-bottom:-18px;   font-size:12px"><tr><td style="width:80px">Indirizzo</td> <td> ' + getFieldValue(name, values) + '</td></tr></table>';
				}
				else if(name == 'suburb' && getFieldValue(name, values) != '') { 
				new_li.innerHTML = '<table style="margin-left:-10px; margin-bottom:-18px;   font-size:12px"><tr><td style="width:80px">Localit&agrave;</td> <td> ' + getFieldValue(name, values) + '</td></tr></table>';
				}
				else if(name == 'c_o' && getFieldValue(name, values) != '') { 
				new_li.innerHTML = '<table style="margin-left:-10px; margin-bottom:-18px;   font-size:12px"><tr><td style="width:80px">C/O</td> <td> ' + getFieldValue(name, values) + '</td></tr></table>';
				}
				else if(name == 'postcode') { 
				new_li.innerHTML = '<table style="margin-left:-10px; margin-bottom:-18px;   font-size:12px"><tr><td style="width:80px">CAP</td> <td>' + getFieldValue(name, values) + '</td></tr></table>';
				}
				else if(name == 'city') { 
				new_li.innerHTML = '<table style="margin-left:-10px; margin-bottom:-18px;   font-size:12px"><tr><td style="width:80px">Citt&agrave;</td> <td> ' + getFieldValue(name, values) + '</td></tr></table>';
				}
				else if(name == 'State:name') { 
				new_li.innerHTML = '<table style="margin-left:-10px; margin-bottom:-18px;   font-size:12px"><tr><td style="width:80px">Provincia</td> <td>' + getFieldValue(name, values) + '</td></tr></table>';
				}
				else if(name == 'Country:name') { 
				new_li.innerHTML = '<table style="margin-left:-10px; margin-bottom:-18px;   font-size:12px"><tr><td style="width:80px">Nazione</td> <td>' + getFieldValue(name, values) + '</td></tr></table>';
				}
				else if(name == 'phone') { 
				new_li.innerHTML = '<table style="margin-left:-10px; margin-bottom:-18px;   font-size:12px"><tr><td style="width:80px">Telefono</td> <td>' + getFieldValue(name, values) + '</td></tr></table>';
				}
				else if(name == 'phone_mobile') { 
				new_li.innerHTML = '<table style="margin-left:-10px; margin-bottom:-18px;   font-size:12px"><tr><td style="width:80px">Cellulare</td> <td>' + getFieldValue(name, values) + '</td></tr></table>';
				}
				else {
				new_li.innerHTML = '' + getFieldValue(name, values) + '';
				}
				
				
				dest_comp.append(new_li);
			{rdelim}
		{rdelim}
	{rdelim}

	function getFieldValue(field_name, values)
	{ldelim}
		var reg=new RegExp("[ ]+", "g");

		var items = field_name.split(reg);
		var vals = new Array();

		for (var field_item in items)
			vals.push(values[items[field_item]]);
		return vals.join(" ");
	{rdelim}

//]]>


	
</script>


	

	
		<br />
		<p class="checkbox" style="display:none;">
			<input type="checkbox" name="same" id="addressesAreEquals" value="1" onclick="updateAddressesDisplayQuick();{if $opc}updateAddressSelection();{/if}" {if $cart->id_address_invoice == $cart->id_address_delivery || $addresses|@count == 1}{/if} />
			<label for="addressesAreEquals">{l s='Use the same address for billing.'}</label>
		</p>
		
		<p id="address_invoice_form" class="select" style="display: none;">
		
		
			<label for="id_address_invoice" style="display: none;" class="strong">{l s='Choose a billing address:'}</label>
			<select style="display:none" name="id_address_invoice" id="id_address_invoice">
			
			{if $cart->id_address_invoice != 0}
				{foreach from=$addresses key=k item=address}
					<option value="{$address.id_address|intval}" {if $address.id_address == $cart->id_address_invoice}selected="selected"{/if}>{$address.alias|escape:'htmlall':'UTF-8'}</option>
				{/foreach}
			{else}
			
				{section loop=$addresses name=address}
					{if $addresses[address].fatturazione == 1}<option value="{$addresses[address].id_address|intval}" selected="selected">{$addresses[address].alias|escape:'htmlall':'UTF-8'}</option>{else}{/if}
				{/section}
			{/if}
			</select>
			
		</p>
		
			<ul class="address alternate_item {if $cart->isVirtualCart()}full_width{/if}" id="address_invoice">
		</ul>
		<ul class="address item" id="address_delivery" {if $cart->id_address_invoice == $cart->id_address_delivery}style="display:none"{else}{/if}>
	
		</ul>
	<script type="text/javascript">
	updateAddressesDisplayQuick();
	</script>
		
		
		<br class="clear" />
		<div style="display:block">
		<p class="address_delivery select" style="display:block; margin-left:10px; margin-top:-10px; margin-bottom:5px">
		<br />
		<br />
		<input type="checkbox" name="ddt_senza_prezzi" id="ddt_senza_prezzi" onclick="ddtSenzaTrasporti({$cart->id})" {if $cart->id_address_invoice == $cart->id_address_delivery}style="display:none"{else}{/if} /><span id="ddt_senza_prezzi_text" {if $cart->id_address_invoice == $cart->id_address_delivery}style="display:none"{else}{/if}>{l s='Transport document without prices, please'}	</span><br /><br />
			<label for="id_address_delivery">{l s='Choose a delivery address:'}</label>
			<select name="id_address_delivery" id="id_address_delivery" class="address_select" style="width:100px" onchange="updateAddressesDisplayQuick();{if $opc}updateAddressSelection();{/if}">

			{foreach from=$addresses key=k item=address}
				<option value="{$address.id_address|intval}" {if $address.id_address == $cart->id_address_delivery || $address.id_address == 84494}selected="selected"{/if}>{$address.alias|escape:'htmlall':'UTF-8'}</option>
			{/foreach}
			
			</select>
			
			{if $cookie->id_customer == 31010 || $cookie->id_customer == 65457}
			<script type="text/javascript">
				updateAddressesDisplayQuick(); updateAddressSelection();
			</script>	
			{/if}
		</p>
		</div>
		<p class="address_add submit">
			<a href="{$link->getPageLink('address.php', true)}?back={$back_order_page}&cart={$cart->id}&step=1{if $back}&mod={$back}{/if}" title="{l s='Add'}" class="button_large" style="display:block; margin-left:5px; padding-top:8px; height:22px; width:295px">{l s='Add a new address'}</a>
			<p style="margin-left:8px; margin-top:3px">{l s='If your delivery address is the same of your invoice address, it is not necessary to specify a delivery address'}</p>
		</p>
		
	</p>
</form>
<br /><br />


<br />


