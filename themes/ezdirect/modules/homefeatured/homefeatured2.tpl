{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}


<script type="text/javascript" src="{$js_dir}unslider.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(function() {
		$('.banner').unslider({
			speed: 500,               //  The speed to animate each slide (in milliseconds)
			delay: 5000,              //  The delay between slide animations (in milliseconds)
			keys: true,               //  Enable keyboard (left, right) arrow shortcuts
			dots: true,               //  Display dot navigation
		});
	});
});
</script>

<div id="banner-home">
	<div class="banner">
		<ul>
		
		{foreach from=$banners item='banner' name='banners'}
			<li><a href="{$banner.link}" target="_blank"><img src="{$img_ps_dir}banner-home/{$banner.immagine}" alt="{$banner.descrizione}" title="{$banner.descrizione}" width="575" height="230" /></a></li>			
		{/foreach}

			
		</ul>
	</div>
</div>


<div id="links-home">
	<ul>
		<li><table><tr><td style='width:50px; text-align:center'><img src='{$img_ps_dir}ez.jpg' alt='Ezdirect' title='Ezdirect' /></td><td style='width:150px'>Il primo sito di telefonia per aziende in cerca di soluzioni Easy & Direct.</td></tr></table></li>
		<li><table><tr><td style='width:50px; text-align:center'><a href='{$link->getCMSLink(3, $link->getCMSRewrite(3))}'><img src='{$img_ps_dir}coccarda.jpg' alt='Garanzia Ezdirect' title='Garanzia Ezdirect' /></a></td><td style='width:150px'><a href='{$link->getCMSLink(3, $link->getCMSRewrite(3))}'>100% soddisfatto<br /> (garanzia Ezdirect)</a></td></tr></table></li>
		<li><table><tr><td style='width:50px; text-align:center'><a href='{$link->getCMSLink(32, $link->getCMSRewrite(32))}'><img src='{$img_ps_dir}preventivi-piccolo.jpg' alt='Preventivi gratuiti' title='Preventivi gratuiti' /></a></td><td style='width:150px'><a href='{$link->getCMSLink(32, $link->getCMSRewrite(32))}'>Preventivi gratuiti: richiedi una quotazione al miglior prezzo</a></td></tr></table></li>
		<li><table><tr><td style='width:50px; text-align:center'><a href='{$link->getCMSLink(65, $link->getCMSRewrite(65))}'><img src='{$img_ps_dir}corporate-piccolo.jpg' alt='Corporate account' title='Corporate account' /></a></td><td><a href='{$link->getCMSLink(65, $link->getCMSRewrite(65))}'>Corporate Account: account manager dedicato per grandi aziende e P.A.</a></td></tr></table></li>
	</ul>
</div>

<div style="clear:both"></div>

	<!-- MODULE Home Featured Products -->
	<div id="home_featured_module">
		<div id='coin-slider'>
			{if isset($products) AND $products}
				{assign var='liHeight' value=342}
				{assign var='nbItemsPerLine' value=4}
				{assign var='nbLi' value=$products|@count}
				{math equation="nbLi/nbItemsPerLine" nbLi=$nbLi nbItemsPerLine=$nbItemsPerLine assign=nbLines}
				{math equation="nbLines*liHeight" nbLines=$nbLines|ceil liHeight=$liHeight assign=ulHeight}
				{assign var='i' value='0'}
				{foreach from=$products item=product name=homeFeaturedProducts}
				{if $i==0 || $i==1}
					<div class='home-top-level' style='{if $i==2 || $i==3}margin-top:15px;{/if} {if $i==0 || $i==2}margin-left:0px;{/if}'>
					<div style='position:absolute; left:0px; top:0px;'>
					<div class='home-top-category'>
					<span><a href="{$product.macro_link}">{$product.macrocategory}</a></span>
					</div>
					{if $product.cheapnet == 'cheapnet_6'}
						<div class='cheapnet' style="top:0px; left:0px">
						<a href='https://www.ezdirect.it/cheapnetcoupons.php'><img src="{$img_ps_dir}cheapnet-300.png" alt="Con questo prodotto hai diritto a un Coupon Cheapnet da 300 minuti gratis" title="Con questo prodotto hai diritto a un Coupon Cheapnet da 300 minuti gratis" width="57" height="57" /></a>
						</div>
					{else if $product.cheapnet == 'cheapnet_30'}
						<div class='cheapnet' style="top:0px; left:0px">
						<a href='https://www.ezdirect.it/cheapnetcoupons.php'><img src="{$img_ps_dir}cheapnet-600.png" alt="Con questo prodotto hai diritto a un Coupon Cheapnet da 600 minuti gratis" title="Con questo prodotto hai diritto a un Coupon Cheapnet da 600 minuti gratis" width="57" height="57" /></a>
						</div>
					{else}
					{/if}
					</div>
					<div style="margin-top:26px">
					<a class="product_img_link"  href="{$product.link}" title="{$product.name|escape:html:'UTF-8'}"><img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home')}"  {if $product.secondary_image != ''}onmouseover="this.src='{$link->getImageLink($product.link_rewrite, $product.secondary_image, 'home')}'" onmouseout="this.src='{$link->getImageLink($product.link_rewrite, $product.id_image, 'home')}'"{/if} alt="{$product.name|escape:html:'UTF-8'}" width="215" height="176" class="classhome" /></a> <!-- class="product_image" -->
					</div>
					<div style='position:absolute; right:0px; top:25px; width:225px' class='home-top-level-title'>
	
					<h2 style='font-size:15px; text-align:right; font-family:Arial,Sans-serif'><a class="product_name_title" href="{$product.link}" title="{$product.name|escape:'htmlall':'UTF-8'}">{$product.name|truncate:80:'...'|escape:'htmlall':'UTF-8'}</a></h2>
					</div>	
					<div style='position:absolute; right:0px; top:90px; margin-top:3px; color:#383838; font-size:12px' class='home-desc'>
					{$product.home_subcategories}
					</div>
					<div style='position:absolute; right:0px; top:64px' class='home-featured-price'>
			
					{if $product.prezzospeciale == 1}
						<span style='font-size:20px; color:red'>{convertPrice price=$product.special_price}</span>
					{else}
				
						{if $product.quantity_discount}
							{assign var="xx" value="0"}
							{assign var=numItems value=$product.quantity_discount|@count}
							{foreach from=$product.quantity_discount item='quantity_discount' name='product.quantity_discounts'}
								{if $xx == 0}				
									{l s='From' mod='homefeatured'}
									<span style='font-size:20px;'>{convertPrice price=$quantity_discount.price - ($quantity_discount.price * $quantity_discount.reduction)|floatval}</span> 
								{else}	
								{/if}
								{$xx = $xx+1}
							{/foreach}
					
						{else}
							{if $product.show_price AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}<span style='font-size:20px;'>{if !$priceDisplay}{convertPrice price=$product.price_tax_exc}{else}{convertPrice price=$product.price_tax_exc}{/if}</span>{else}{/if}
						{/if}
					{/if}
					</div>
					
					</div>
			
			{if $i==1}
			<div style="clear:both"></div>
			{/if}
			
			
			
			{else if $i==2 || $i==3 || $i==4 || $i==5 || $i==6 || $i==7 || $i==8 || $i==9 || $i==10 || $i==11 || $i==12 || $i==13  || $i==14 || $i==15 || $i==16 || $i==17}
			{if $i==5 || $i==9 || $i==13  || $i==17}
			<div class='home-low-level' style='font-family:Arial; margin-right:-1px {if $i > 9}; height:240px{/if}'>
			{else}
			<div class='home-low-level' style='font-family:Arial; {if $i > 9}; height:240px{/if}'>
			{/if}
			<div class='home-medium-category'>
			<span><a href="{$product.macro_link}">{$product.macrocategory}</a></span>
			</div>
			<div style=''>
			
			
		
						
						
						{if $product.cheapnet == 'cheapnet_6'}
			<div class='cheapnet' style="top:20px; left:0px; width:30px; z-index:9999">
			<a href='https://www.ezdirect.it/cheapnetcoupons.php'><img src="{$img_ps_dir}cheapnet-300.png" alt="Con questo prodotto hai diritto a un Coupon Cheapnet da 300 minuti gratis" title="Con questo prodotto hai diritto a un Coupon Cheapnet da 300 minuti gratis" width="57" height="57" /></a>
			</div>
			{else if $product.cheapnet == 'cheapnet_30'}
			<div class='cheapnet' style="top:20px; left:0px;  width:30px; z-index:9999">
			<a href='https://www.ezdirect.it/cheapnetcoupons.php'><img src="{$img_ps_dir}cheapnet-600.png" alt="Con questo prodotto hai diritto a un Coupon Cheapnet da 600 minuti gratis" title="Con questo prodotto hai diritto a un Coupon Cheapnet da 600 minuti gratis" width="57" height="57" /></a>
			</div>
			{else}
			{/if}			
						<a href="{$product.link}" style="display:block; position:relative" title="{$product.name|escape:html:'UTF-8'}"  class="product_img_link"><img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home')}" {if $product.secondary_image != ''}onmouseover="this.src='{$link->getImageLink($product.link_rewrite, $product.secondary_image, 'home')}'" onmouseout="this.src='{$link->getImageLink($product.link_rewrite, $product.id_image, 'home')}'"{/if} class="classhome" alt="{$product.name|escape:html:'UTF-8'}" /></a>
			</div>
			<div class='home-medium-price'>
			
			{if $product.prezzospeciale == 1}
					<span style='font-size:16px; font-weight:bold; color:red'>{convertPrice price=$product.special_price}</span>
				{else}
							{if $product.quantity_discount}
						{assign var="xx" value="0"}
						{assign var=numItems value=$product.quantity_discount|@count}
						{foreach from=$product.quantity_discount item='quantity_discount' name='product.quantity_discounts'}
						{if $xx == 0}				
						{l s='From' mod='homefeatured'}
						
						<span style='font-size:16px;'><strong>{convertPrice price=$quantity_discount.price - ($quantity_discount.price * $quantity_discount.reduction)|floatval}</strong></span> 
						{else}	
						{/if}
						{$xx = $xx+1}
						{/foreach}
						
					{else}
					{if $product.show_price AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}<span style='font-size:16px;'><strong>{if !$priceDisplay}{convertPrice price=$product.price_tax_exc}{else}{convertPrice price=$product.price_tax_exc}{/if}</strong></span>{else}{/if}
					{/if}
				{/if}
			
			</div>
			
			
			
			<div style='position:absolute; left:0px; padding-left:7px; padding-right:7px; top:200px; width:200px'>
			<h5 style='font-size:15px'><a class="product_name_title" href="{$product.link}" title="{$product.name|escape:'htmlall':'UTF-8'}">{$product.name|truncate:50:'...'|escape:'htmlall':'UTF-8'}</a></h5>
			</div>	
			<div class="home-medium-subcategories">
			<!-- {$product.desc_homepage|strip_tags|truncate:120:'...'} -->
			
			{if $i > 9}{else}{$product.home_subcategories}{/if}
			
			</div>
			
			
			
			
			{if $i==9}
			</div><div style='clear:both'></div>
				<h4 class='homepage'>{l s='Featured products' mod='homefeatured'}</h4>
				{else if $i==13}
				</div><div style='clear:both'></div>
				<h4 class='homepage'>{l s='New products' mod='homefeatured'}</h4>
				
				{else}
				</div>
				{/if}
				
				
				{else if $i==10 || $i==11 || $i==12 || $i==13}
				{/if}
			{$i = $i+1}
			{/foreach}
			<div style='clear:both'></div>

	{else}
		<p>{l s='No featured products' mod='homefeatured'}</p>
	{/if}
</div>
</div>
<!-- /MODULE Home Featured Products -->
