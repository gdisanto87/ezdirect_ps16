{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<p style='text-align:center'>
<a href="{$link->getPageLink('prices-drop.php', true)}"><img src="{$base_dir_ssl}img/offerte-speciali-new.jpg" width="180" height="92" alt="{l s='Special offers' mod='block_offerte_speciali'}" title="{l s='Special offers' mod='block_offerte_speciali'}"></a> 
</p>

<p style="text-align:center">

<br />
	{literal}
	<p style='text-align:center'>Recensioni certificate</p>
	<!-- eKomiWidget START -->
<div id="eKomiWidget_default"></div>
<!-- eKomiWidget END -->

<!-- eKomiLoader START, only needed once per page -->
<script type="text/javascript"> 
	(function(){
		eKomiIntegrationConfig = new Array(
			{certId:'44D97E5AAD9DE69'}
		);
		if(typeof eKomiIntegrationConfig != "undefined"){for(var eKomiIntegrationLoop=0;eKomiIntegrationLoop<eKomiIntegrationConfig.length;eKomiIntegrationLoop++){
			var eKomiIntegrationContainer = document.createElement('script');
			eKomiIntegrationContainer.type = 'text/javascript'; eKomiIntegrationContainer.defer = true;
			eKomiIntegrationContainer.src = (document.location.protocol=='https:'?'https:':'http:') +"//connect.ekomi.de/integration_1501070221/" + eKomiIntegrationConfig[eKomiIntegrationLoop].certId + ".js";
			document.getElementsByTagName("head")[0].appendChild(eKomiIntegrationContainer);
		}}else{if('console' in window){ console.error('connectEkomiIntegration - Cannot read eKomiIntegrationConfig'); }}
	})();
</script>
<!-- eKomiLoader END, only needed once per page -->

{/literal}



<br />
</p>
