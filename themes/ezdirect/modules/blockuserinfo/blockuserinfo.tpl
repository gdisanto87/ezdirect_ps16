{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* https://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to https://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14011 $
*  @license    https://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- Block user information module HEADER -->
<div id="header_user">
	<div>
	<ul class='blockuserinfoul' style='font-size:12px'>
		{if $cookie->isLogged()}
			
			<li onmouseover="document.getElementById('menu_user').src='{$img_dir}icon/menu_user_w.png'"  onmouseout="document.getElementById('menu_user').src='{$img_dir}icon/menu_user.png'"><strong><a href="{$link->getPageLink('my-account.php', true)}" rel="nofollow" title="{l s='Your Account' mod='blockuserinfo'}"><img src='{$img_dir}icon/menu_user.png' id="menu_user" alt='{l s='Your account' mod='blockuserinfo'}' title='{l s='Your account' mod='blockuserinfo'}' /><span>{l s='Your Account' mod='blockuserinfo'}</span></a></strong></li>
		{else}
		
		<li onmouseover="document.getElementById('menu_user').src='{$img_dir}icon/menu_user_w.png'"  onmouseout="document.getElementById('menu_user').src='{$img_dir}icon/menu_user.png'"><strong><a href="{$link->getPageLink('authentication.php', true)}?back=my-account.php" rel="nofollow" title="{l s='Your Account' mod='blockuserinfo'}"><img id="menu_user" src='{$img_dir}icon/menu_user.png' alt='{l s='Your account' mod='blockuserinfo'}' title='{l s='Your account' mod='blockuserinfo'}' /><span>{l s='Your Account' mod='blockuserinfo'}</span></a></strong></li>
		{/if}
		
		{if $latest_offer > 0}
		
		<li><strong><a href="{$link->getPageLink('modules/mieofferte/offerte.php', true)}?id_cart={$latest_offer}&id_customer={$cookie->id_customer}" rel="nofollow" title="{l s='View my latest offer' mod='blockuserinfo'}"><img src='{$img_dir}icon/menu_latest_offer.png' alt='{l s='View my latest offer' mod='blockuserinfo'}' title='{l s='View my latest offer' mod='blockuserinfo'}' /><span>{l s='View my latest offer' mod='blockuserinfo'}</span></a></strong></li>
		
		
		{else}
		{/if}
		
		
		<li onmouseover="document.getElementById('menu_quotations').src='{$img_dir}icon/menu_quotations_w.png'"  onmouseout="document.getElementById('menu_quotations').src='{$img_dir}icon/menu_quotations.png'" style="position:relative"><strong><a href="
		{if $offer_count > 0}
		{$link->getPageLink('modules/mieofferte/offerte.php', true)}
		{else}
		{$link->getPageLink('modules/formprevendita/form.php', true)}
		{/if}
		"><img id="menu_quotations" src='{$img_dir}icon/menu_quotations.png' alt='{l s='Quotations' mod='blockuserinfo'}' title='{l s='Quotations' mod='blockuserinfo'}' /><span>{l s='Preventivi' mod='blockuserinfo'}</span> {if $offer_count > 0} <span style="color:#ff0000; font-weight:bold">({$offer_count})</span>{/if}</a></strong>
		
		</li>
		
		<li onmouseover="document.getElementById('menu_contacts').src='{$img_dir}icon/menu_contacts_w.png'"  onmouseout="document.getElementById('menu_contacts').src='{$img_dir}icon/menu_contacts.png'"><strong><a href="https://www.ezdirect.it/guide/39-contatti"><img id="menu_contacts" src='{$img_dir}icon/menu_contacts.png' alt='{l s='Contacts' mod='blockuserinfo'}' title='{l s='Contacts' mod='blockuserinfo'}' /><span>{l s='Contacts' mod='blockuserinfo'}</span></a></strong></li>
		
		<li  onmouseover="document.getElementById('menu_cart').src='{$img_dir}icon/menu_cart_w.png'"  onmouseout="document.getElementById('menu_cart').src='{$img_dir}icon/menu_cart.png'"><strong><a href="https://www.ezdirect.it/ordine-veloce" rel="nofollow" title="{l s='Your Shopping Cart' mod='blockuserinfo'}"><img id="menu_cart" src='{$img_dir}icon/menu_cart.png' alt='{l s='Cart:' mod='blockuserinfo'}' title='{l s='Cart:' mod='blockuserinfo'}' /><span>{l s='Cart:' mod='blockuserinfo'}</span></a></strong></li>
		<li  onmouseover="document.getElementById('menu_assistance').src='{$img_dir}icon/menu_assistance_w.png'"  onmouseout="document.getElementById('menu_assistance').src='{$img_dir}icon/menu_assistance.png'"><strong><a href="https://www.ezdirect.it/guide/55-assistenza"><img id="menu_assistance" src='{$img_dir}icon/menu_assistance.png' alt='{l s='Assistance' mod='blockuserinfo'}' title='{l s='Assistance' mod='blockuserinfo'}' /><span>{l s='Assistance' mod='blockuserinfo'}</span></a></strong></li>
		<li  onmouseover="document.getElementById('menu_blog').src='{$img_dir}icon/menu_blog_w.png'"  onmouseout="document.getElementById('menu_blog').src='{$img_dir}icon/menu_blog.png'"><strong><a href="https://www.ezdirect.it/blog/"><img id="menu_blog" src='{$img_dir}icon/menu_blog.png' alt='{l s='Blog' mod='blockuserinfo'}' title='{l s='Blog' mod='blockuserinfo'}' /><span>{l s='Blog' mod='blockuserinfo'}</span></a></strong></li>
		<li onmouseover="document.getElementById('menu_resellers').src='{$img_dir}icon/menu_resellers_w.png'"  onmouseout="document.getElementById('menu_resellers').src='{$img_dir}icon/menu_resellers.png'" ><strong><a href="https://www.ezdirect.it/autenticazione?cliente=rivenditore" rel="nofollow"><img id="menu_resellers" src='{$img_dir}icon/menu_resellers.png' alt='{l s='Resellers' mod='blockuserinfo'}' title='{l s='Resellers' mod='blockuserinfo'}' /><span>{l s='Resellers' mod='blockuserinfo'}</span></a></strong></li>
		
		<li onmouseover="document.getElementById('menu_manufacturers').src='{$img_dir}icon/menu_manufacturers_w.png'"  onmouseout="document.getElementById('menu_manufacturers').src='{$img_dir}icon/menu_manufacturers.png'" ><strong><a href="https://www.ezdirect.it/produttori?mode=0" rel="nofollow"><img id="menu_manufacturers" src='{$img_dir}icon/menu_manufacturers.png' alt='{l s='Manufacturers' mod='blockuserinfo'}' title='{l s='Manufacturers' mod='blockuserinfo'}' /><span>{l s='Marchi' mod='blockuserinfo'}</span></a></strong></li>
		{if $lang_iso == 'en'}
	
		{foreach from=$languages key=k item=language name="languages"}
			<li {if $language.iso_code == $lang_iso}class="selected_language"{/if}>
				{if $language.iso_code != $lang_iso}
					{assign var=indice_lang value=$language.id_lang}
					{if isset($lang_rewrite_urls.$indice_lang)}
						<a href="{$lang_rewrite_urls.$indice_lang|escape:htmlall}" title="{$language.name}">
					{else}
						<a href="{$link->getLanguageLink($language.id_lang)|escape:htmlall}" title="{$language.name}">
					{/if}

				{/if}
					<img src="{$img_lang_dir}{$language.id_lang}.jpg" alt="{$language.iso_code}" width="16" height="11" />
				{if $language.iso_code != $lang_iso}
					</a>
				{/if}
			</li>
		{/foreach}


{/if}
	
		</ul>
	</div>

</div>
<!-- /Block user information module HEADER -->
