{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

	<a class="box-sx-img" href="https://www.ezdirect.it/preventivi-quotazioni-miglior-prezzo-cuffie-centralini">
	<img src="{$img_ps_dir}preventivi-new.jpg" alt="{l s='Preventivi'}" title="{l s='Preventivi' mod='blockcms'}" /></a>
	<a class="box-sx-img" href="https://www.ezdirect.it/ti-richiamiamo-noi">
	<img src="{$img_ps_dir}richiamiamonoi-new.jpg" alt="{l s='Call me back'}" title="{l s='Call me back' mod='blockcms'}" /></a>
	
	<a class="box-sx-img" href="https://www.ezdirect.it/guide/65-corporate-account-ezdirect-account-manager-dedicato-per-grandi-aziende-e-pa"><img src="{$img_ps_dir}corporate-new.jpg" alt="{l s='Corporate account'}" title="{l s='Corporate account' mod='blockcms'}" /></a>
	
	<a class="box-sx-img" href="https://www.ezdirect.it/guide/69-ezdirect-azienda-accreditata-mepa"><img src="{$img_ps_dir}acquistinrete-new.jpg" alt="{l s='MEPA'}" title="{l s='MEPA' mod='blockcms'}" /></a>
	

	

{if $block == 1}
	<!-- Block CMS module -->
	{foreach from=$cms_titles key=cms_key item=cms_title}
		<div id="informations_block_left_{$cms_key}" class="block informations_block_left" >
			<h4><a href="https://www.ezdirect.it/guide/category/5-guide-e-consigli">{if !empty($cms_title.name)}{$cms_title.name}{else}{$cms_title.category_name}{/if}</a></h4>
			<ul class="block_content">
			{if $cms_title.name == 'Guide e consigli'}
			
			<li><a href='https://www.ezdirect.it/blog/sistemi-e-dispositivi-per-audioconferenza/' target='_blank'>Audioconferenza</a></li>
			<li><a href='https://www.ezdirect.it/file/54405/fanvil%20telefoni%202019-80.pdf' target='_blank'>Citofoni VoIP</a></li>
			<li>
<a href='https://www.ezdirect.it/guide/25-cuffie-guida-alla-scelta-telefoniche-con-microfono' title='Cuffie telefoniche'>{l s='Cuffie bluetooth e con cavo'}</a>
</li><li>
			</li><li>
			<a href='https://www.ezdirect.it/centralini-telefonici/3201-centralino-telefonico-ez308-pbx-3-linee-disa-fax.html'>{l s='Centralino telefonico'}</a>
</li>
<li>
<a href='https://www.ezdirect.it/centralini-telefonici/'>{l s='Centralino VoIP'}</a>
</li>
<li>
<a href='https://www.ezdirect.it/centralino-virtuale/'>{l s='Centralino virtuale'}</a>
</li>
<li><a href="https://www.ezdirect.it/guide/76-centralino-virtuale-come-funziona" title="Come funziona Ezcloud">Come funziona Ezcloud</a></li>	
<li><a href="https://www.ezdirect.it/guide/77-configurazione-telefoni-e-softphone-centralino-virtuale-ezcloud" title="Configurazioni Ezcloud">Configurazioni Ezcloud</a></li>	
<li><a href="https://www.ezdirect.it/blog/radioguide-whisper-per-gruppi-e-visite-guidate/" title="Radioguide">Radioguide</a></li>	
<li>
<a href="https://www.ezdirect.it/guide/70-ripetitori-di-segnale-gsm-umts-lte-come-funzionano" title="Ripetitori di segnale GSM UMTS LTE">Ripetitori GSM UMTS LTE</a></li>	
<li>
<a href='https://www.ezdirect.it/telefoni-fissi/'>{l s='Telefoni VoIP'}</a>

</li>
<li>
<a href='https://www.ezdirect.it/file/54405/fanvil%20telefoni%202019-80.pdf'>{l s='Telefoni Fanvil'}</a>

</li>
<li>
<a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/" title="Videoconferenza Cloud">Videoconferenza Cloud</a></li>
{if $smarty.server.PHP_SELF|basename == 'index.php'}

<li>
<a href='https://www.ezdirect.it/blog/videoconferenza-professionale-videocamere-sistemi-app/'>{l s='Videoconferenza'}</a>

</li>
<li>
<a href='https://www.ezdirect.it/file/54403/yeastar-serie%20s-2019.pdf'>{l s='Yeastar guide'}</a>

</li>


					{/if}
					
					
			{else}		
			{/if}
			
			
			
			
			</ul>
		</div>
	{/foreach}
{/if}


