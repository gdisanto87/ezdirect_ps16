{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if $id_cms_category != 7}

		{if !$content_only}
				</div>

<!-- Right -->
				
			</div>

<!-- Footer -->
			<div style="clear:both"></div>
		</div>
	{/if}
	

</div> <!-- contenitore -->

<div id="footer">
<div id="footer-in">

{if $page_name == 'index'} 
		
		
		{$HOOK_FOOTER}
		
		{else}		
	
		{/if}	
	
	
<br />
		<div id="footer_blocks">
			<div id="footer_block_1" class="footer_blocks"><a href='{$link->getCMSLink2(37, $link->getCMSRewrite(37))}'>{l s='GLS tracking'}</a><br />
			<img src='{$img_ps_dir}gls.jpg' width="133" height="40" alt="{l s='GLS tracking'}" title="{l s='GLS tracking'}">
			</div>
			<div id="footer_block_2" class="footer_blocks">{l s='Safe payments with'}<br />
			<img src='{$img_ps_dir}pagamenti.jpg' width="200" height="50" alt="{l s='Safe payments'}" title="{l s='Safe payments'}">
			
			</div>
			<div id="footer_block_3" class="footer_blocks">
			{l s='Safe data with encrypted connection'}<br />
			<img src='{$img_ps_dir}rapid-ssl.jpg' width="88" height="48" alt="{l s='Secured by Rapid SSL'}" title="{l s='Secured by Rapid SSL'}">
			<img src='{$img_ps_dir}geotrust.jpg' width="101" height="40" alt="{l s='Verified by Geotrust'}" title="{l s='Verified by Geotrust'}">
			
			
			</div>
			
			<div id="footer_block_4" class="footer_blocks">
			<a href='{$link->getCMSLink2(65, $link->getCMSRewrite(65))}'><img width="202" height="50" src='{$img_ps_dir}corporate-account-2.jpg' alt="{l s='Corporate Account'}" title="{l s='Corporate account'}"></a>
			</div>
			
			<div id="footer_block_5" class="footer_blocks">
			<a href="{$link->getPageLink('authentication.php', true)}?cliente=rivenditore"><img width="168" height="40" src='{$img_ps_dir}rivenditore.gif' alt="{l s='Resellers'}" title="{l s='Resellers'}"></a>
			
			</div>
			
			<div style="clear:both"></div>
		</div>	
			<br />
			<p style='text-align:center; background-color:#ffffff; padding:3px; '>
		{l s='Our products sorted by initial letter'}:<br />
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_a/" >A</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_b/" >B</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_c/" >C</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_d/" >D</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_e/" >E</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_f/" >F</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_g/" >G</a> - 
		H - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_i/" >I</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_j/" >J</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_k/" >K</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_l/" >L</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_m/" >M</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_n/" >N</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_o/" >O</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_p_1/" >PA-PK</a> - 
		<a style='padding:2px;' href="https://www.ezdirect.it/lettera_p_2/" >PL-PZ</a> - 
		Q - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_r/" >R</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_s/" >S</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_t/" >T</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_u/" >U</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_v/" >V</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_w/" >W</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_x/" >X</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_y/" >Y</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_z/" >Z</a> - 
		</p>
			
			
			<div style="width: 100%; margin-left:15px;">
 <table id="footer-table-blocks">
  <tbody><tr>
    <td class="tdfooter">
	<h3>Azienda</h3>
      <ul class="menufooter">
        <li><a href="{$link->getCMSLink2(4, $link->getCMSRewrite(4))}" title="Chi siamo">Chi siamo</a></li>
        <li><a href="{$link->getCMSLink2(39, $link->getCMSRewrite(39))}" title="Contatti">Contatti</a></li>
        <li><a href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}" title="Condizioni">Condizioni</a></li>
        <li><a target="_blank" href="https://www.ezdirect.it/sitemap.php" title="Sitemap">Sitemap</a></li>
    </ul>
	</td>
    <td class="tdfooter">
       <h3>Pagamenti</h3>
       <ul class="menufooter">
         <li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#contrassegno" title="Contrassegno">Contrassegno</a></li>
         <li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#bonifico" title="Bonifico all'ordine">Bonifico all'ordine</a></li>
         <li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#paypal" title="Paypal">Paypal</a></li>
         <li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#noleggio" title="Noleggio Leasing">Noleggio Leasing</a></li>
         <li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#prezzi" title="Prezzi">Prezzi</a></li>
    </ul></td>
    <td class="tdfooter">
       <h3>Consegna prodotti</h3>
         <ul class="menufooter">
           <li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#consegne" title="Spedizione">Spedizione</a></li>
           <li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#consegne" title="Modalita di consegna">Modalit&agrave;&nbsp; di consegna</a></li>
           <li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#consegne" title="Spese di consegna">Spese di consegna</a></li>
           <li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#consegne" title="Fatturazione">Fatturazione</a></li>
    </ul></td>
    <td class="tdfooter">
       <h3>Servizi</h3>
 
       <ul class="menufooter">
         <li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#supporto" title="Supporto pre vendita">Supporto pre vendita</a></li>
         <li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#assistenza" title="Assistenza tecnica">Assistenza tecnica</a></li>
         <li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#rma" title="Riparazioni">Riparazioni</a></li>
         <li><a href="https://www.ezdirect.it/preventivi-quotazioni-miglior-prezzo-cuffie-centralini" title="Preventivi">Preventivi</a></li>
		 <li><a rel="nofollow" href="{$link->getPageLink('authentication.php', true)}?cliente=rivenditore">Rivenditori</a></li>
    </ul></td>
    <td class="tdfooter">
  <h3>Garanzie</h3>    
              <ul class="menufooter">
                <li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#garanzie" title="Garanzia sui prodotti">Garanzia sui prodotti</a></li>
                <li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#recesso" title="Diritto di recesso">Diritto di recesso</a></li>
                <li><a rel="nofollow" href="{$link->getCMSLink2(6, $link->getCMSRewrite(6))}" title="Privacy">Privacy</a></li>
                <li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#rma" title="RMA">RMA</a></li>
    </ul></td>
  </tr>
</tbody></table>
</div>


</div>
			
</div>	<!-- fine footer -->

<div id="footer-2">
	<div id="footer-2-in">
			<div id="footer-social">
			<h4 style="font-size:16px; text-align:center">{l s='Follow us on'}</h4>
			
			<table style='text-align:center; display:block; margin:0 auto; margin-left:10px; '>
				<tr>

				<td style='width:40px; vertical-align:center'>
				<a href='https://it-it.facebook.com/pages/Ezdirect-srl/53805768063' target='_blank' class='facebook' title='Facebook' rel='nofollow'></a>
				</td>

				<td style='width:40px; vertical-align:center'>
				<a href='https://twitter.com/ezdirect_it' target='_blank' class='twitter' title='Twitter' rel='nofollow'></a>
				</td>

				<td style='width:40px; vertical-align:center'>
				<a href='http://www.linkedin.com/company/ezdirect-srl' target='_blank' class='linkedin' title='Linkedin' rel='nofollow'></a>
				</td>

				</tr>
			</table><br /><br />
			</div>
			<div id="footer-bottom">
				<p style="margin-top: 10px; font-family: verdana; font-size: 13px;">
				<strong>Ezdirect Srl a socio unico</strong> Via Nerino Garbuio (SS1 Aurelia) Snc, 54038, Montignoso, Massa Carrara | P.iva CCIAA 01164670455, REA 118272, capitale sociale 80.000 &euro; (interamente versato), SDI: USAL8PV

				<br />
				<strong>Ezdirect srl</strong> si rivolge ad aziende, imprese, professionisti, P.A., che utilizzano i prodotti per la loro attivit&agrave; e per la rivendita. Tel: <a href="tel:+39-0585-821163">0585821163 </a>
				<br />

				</p>

				<div style="margin-bottom: 10px;" >
					<p style="font-size: 13px; font-family: arial;">Le informazioni riportate su www.ezdirect.it possono essere soggette a modifiche senza preavviso | I marchi e i loghi in questo sito sono di propriet&agrave; dei legittimi proprietari.

					<br />
					&Egrave; vietata la riproduzione anche parziale dei contenuti e della grafica del sito senza autorizzazione.
					<br />
					 
					</p><br />
				</div>

			</div>
			<div style="clear:both"></div>
		</div>	
	</div>	
			
			
			
	<div id="live-help" style="height:70px; width:180px; border:0px; position:fixed; {if $cookie->full_site == 1}bottom:48px{else}bottom:0px{/if}; z-index:999; right:0px; display:none">
		<a rel="nofollow" href="https://www.ezdirect.it/mibew/client.php?locale=it" target="_blank" onclick="if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 &amp;&amp; window.event.preventDefault) window.event.preventDefault();this.newWindow = window.open('https://www.ezdirect.it/mibew/client.php?locale=it&amp;url='+escape(document.location.href)+'&amp;referrer='+escape(document.referrer), 'webim', 'toolbar=0,scrollbars=0,location=0,status=1,menubar=0,width=640,height=480,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;"><img src="https://www.ezdirect.it/mibew/b.php?i=webim&amp;lang=it" alt="Live help desk" title="Live help desk" style="border:0px" width="180" height="68" />
		</a>
	</div>
	
<!--	{$footer_mobile} -->
	
	<a href="#0" class="to-top" title="Back To Top"><div class="back-to-top-arrow-up"></div></a>
	
	
<script src="https://apis.google.com/js/platform.js" async defer></script>​

<g:ratingbadge merchant_id=7615115></g:ratingbadge>​

	<!-- Linkedin Insight -->
	<script type="text/javascript">
		{literal}
			_linkedin_partner_id = "118888";
			window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
			window._linkedin_data_partner_ids.push(_linkedin_partner_id);
			</script><script type="text/javascript">
			(function(){var s = document.getElementsByTagName("script")[0];
			var b = document.createElement("script");
			b.type = "text/javascript";b.async = true;
			b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
			s.parentNode.insertBefore(b, s);})();
		{/literal}
	</script>
	<noscript>
		<img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=118888&fmt=gif" />
	</noscript>
	<!-- End Linkedin Insight -->
	
	<!-- Start of oct8ne code -->
	<script type="text/javascript">
		{literal}
			var oct8ne = document.createElement("script");
			oct8ne.server = "backoffice-eu.oct8ne.com/";
			oct8ne.type = "text/javascript";
			oct8ne.async = true;
			oct8ne.license ="20280654309687A8A7CBFDF31117F032";
			oct8ne.src = (document.location.protocol == "https:" ? "https://" : "http://") + "static-eu.oct8ne.com/api/v2/oct8ne-api-2.3.js?" + (Math.round(new Date().getTime() / 86400000));
			oct8ne.locale = "it-IT";
			oct8ne.baseUrl ="//www.ezdirect.it/";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(oct8ne, s);
		{/literal}
	</script>
	<!--End of oct8ne code --> 

	</body>
</html>

{else}
{include file="$tpl_dir./landing_footer.tpl"}
{/if}

