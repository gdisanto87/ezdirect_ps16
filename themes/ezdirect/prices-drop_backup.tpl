{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{capture name=path}{l s='Price drop'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<h1>{l s='Price drop'}</h1>

	{if $categories} 
		{foreach from=$categories item=category}
		
			<a href="{$link->getPageLink('prices-drop.php', true)}?category={$category->id}" class="prices_drop_button">{$category->name|truncate:25:'...':false|escape:'htmlall':'UTF-8'}</a>
	
	
		{/foreach}
	{/if}
	<a href="{$link->getPageLink('prices-drop.php', true)}" class="prices_drop_button">{l s='List all products'}</a>
	
	<div style="clear:both"></div>
	{if $products}
		<div class='block_listing_header'>
		<div class='block-compare'>
		{include file="$tpl_dir./product-compare.tpl"}
		&nbsp;&nbsp;&nbsp;<span class="category-product-count">{include file="$tpl_dir./category-count.tpl"}</span>
		</div>
		<div class='block-sort'>
						{include file="$tpl_dir./product-sort.tpl"}
		</div>
		</div>	
		{include file="$tpl_dir./product-list.tpl" products=$products}
{include file="$tpl_dir./product-compare.tpl"}
<br /><br />
		{include file="$tpl_dir./pagination.tpl"}
	{else}
		<p class="warning">{l s='No price drop.'}</p>
	{/if}