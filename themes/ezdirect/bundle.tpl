<script type="text/javascript" src="{$js_dir}etc/stars.js">
</script>

<div class="prices">
<h1>{$name}</h1>
{if $reference != ''}
<span style="font-size:11px; color:#717171; display:block; text-align:center">{l s='Bundle code'}: {$reference}</span>
{else}
<span style="font-size:11px; color:#717171; display:block; text-align:center"></span>
{/if}


{assign var=i value=1}
{foreach from=$products item=product name=products}
<div class="bundle_block" {if $i == 5}style="margin-right:0px"{else}{/if}>



{if $product.active != 1 || ($father_product->fuori_produzione == 1 && $father_product->price == 0 && $father_product->quantity == 0)}
<h2 title="{$product.name|escape:'htmlall':'UTF-8'}">{$product.name|truncate:50:'...':true|escape:'htmlall':'UTF-8'}</h2>
<img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'medium')}"  alt="{$product.legend|escape:'htmlall':'UTF-8'}" width="110" height="110" style="display:block; margin:0 auto" />
<span class="quantity-bundle-in"><strong>{$product.quantity_in_bundle}x</strong></span>
<div class="bottom">{$product.description_short|truncate:140:'...'|strip_tags:'UTF-8'}<br />

{else}
<h2 title="{$product.name|escape:'htmlall':'UTF-8'}"><a href="{$product.link|escape:'htmlall':'UTF-8'}" class="product_img_link" title="{$product.name|escape:'htmlall':'UTF-8'}" >{$product.name|truncate:50:'...':true|escape:'htmlall':'UTF-8'}</a></h2>
<a href="{$product.link|escape:'htmlall':'UTF-8'}" class="product_img_link" title="{$product.name|escape:'htmlall':'UTF-8'}"><img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'medium')}"  alt="{$product.legend|escape:'htmlall':'UTF-8'}" {if isset($mediumSize)} width="110" height="110"{/if} style="display:block; margin:0 auto; max-width:110px" /></a>
<span class="quantity-bundle-in"><strong>{$product.quantity_in_bundle}x</strong></span>
<div class="bottom">
<br />
<a href="{$product.link|escape:'htmlall':'UTF-8'}#idTab5" rel="nofollow"><span class="stars">{$product.averageTotal}</span> <br />({$product.total_reviews} {l s='reviews'})</a>
<br /><br />
{$product.description_short|truncate:140:'...'|strip_tags:'UTF-8'}<br /><a href="{$product.link|escape:'htmlall':'UTF-8'}" target="_blank">{l s='Further details'}...</a>
{/if}

</div>



{if $products|@count == $i || $i == 5}
{else}
<div class="piu"></div>
{$i = $i+1}
{/if}

</div>
{/foreach}

{foreach from=$products item=product name=products}
	{if isset($cookie->id_customer) && $cookie->is_company == 0 && ($product.id_product == 30983 || $product.id_product == 317789 || $product.id_product == 31024 || $product.id_product == 3201 || $product.id_product == 28699 || $product.id_product == 28815 || $product.id_product == 31062 || $product.id_product == 28817 || $product.id_product == 31066 || $product.id_product == 317691 || $product.id_product == 28820 || $product.id_product == 28819 || $product.id_product == 28764 || $product.id_product == 317790 || $product.id_product == 317791 || $product.id_product == 28818 || $product.id_product == 31059 || $product.id_product == 31078)}
		{assign var="no_privato" value="1"}		
		{break}
	{else if isset($cookie->id_customer) && $cookie->is_company == 0 && ($product.id_product == 31127 || $product.id_product == 31167 || $product.id_product == 30927 || $product.id_product == 31166 || $product.id_product == 30938)}
		{assign var="no_privato" value="1"}	
		{break}		
	{else}
		{assign var="no_privato" value="0"}		
	{/if}
{/foreach}
<div style="clear:both"></div>
	
{if $no_privato == 1}
	<strong>Kit non vendibile a consumatori finali / privati</strong>
				<br /><br />
	{else}
	
		{if $active == 0 || ($father_product->fuori_produzione == 1 && $father_product->quantity == 0)}<br /><br />
		<strong>
		{l s='This bundle is not available'}
		</strong>
		{else}
<br /><br /><br /><br /><br />
			
			<table>
			<tr>
			
			<td style="width:570px; text-align:right">
				
				
				{if $end_price > $best_price}
					
					<table>
								<tr>
								<td>
								<span style="font-size:18px; color:#e62026">{l s='Total price of this kit'}:</span> <!-- {l s='From'} --> </td><td style='text-align:right'><span style="font-size:18px; color:#e62026">{convertPrice price=$end_price}</span><span style="font-size:13px; color:#e62026"></td><td>{l s='t.e.'} ({convertPrice price=$end_price_ii} {l s='t.i.'})</td><td><!-- {l s='for'} {$highest_price}+ {l s='kits'} --></td></tr>
								<tr>
								<td></td><td style='text-align:right'></td></tr>
								</table>
								
				{else}
					<span style="font-size:18px; color:#e62026">{l s='Total price of this kit'}: {convertPrice price=$end_price}</span> <span style="font-size:13px; color:#e62026">{l s='t.e.'}</span>  ({convertPrice price=$end_price_ii} {l s='t.i.'})
				{/if}
				
			</td>
			<td style="width:160px; text-align:center">
				<div class="quantity_wanted_bdl" style="width:80px; text-align:center; display:block; float:left; margin-left:30px; margin-top:-10px">{l s='Quantity'}<br />
								
								<div class="quantity_input_div">
									<input type="text" name="qty_bdl_{$id_bundle}" id="qty_bdl_{$id_bundle}" class="text quantity_input quantity_bundle" value="{if isset($quantityBackup)}{$quantityBackup|intval}{else}1{/if}" size="2" maxlength="3" />
									<div class="quantity_input_buttons">
										<span class="span_up" onclick="quantity_cart_add('qty_bdl_{$id_bundle}');">+</span>
										<span class="span_down" onclick="quantity_cart_rmv('qty_bdl_{$id_bundle}');">-</span>
									</div>
								</div>
								<div style="clear:both"></div>
								<br />
								</div>
								<div class="acquista-listing-button-product">		
									<form id="buy_block" {if $PS_CATALOG_MODE AND !isset($groups)}class="hidden"{/if} action="{$link->getPageLink('cart.php')}" method="post">
										<input type="hidden" name="bundle" value="{$id_bundle}" />
										<input type="submit" value="{l s='Add to cart'}" title="{l s='Add to cart'}" id="bundle_{$id_bundle}" class="ajax_add_to_cart_button" style="width:80px; background-image:none; background-color:#e62026; margin-left:30px; padding:5px" />
									</form>
								</div>
				
			</td>
			</tr>
			</table>

			
		{/if}
	{/if}
	
	<div style="clear:both"></div>
	
	</div>

{if $supporto_da_remoto->id > 0 || $estensione_garanzia->id > 0 || $telegestione->id > 0}
<div class="prices">
	<table>
			<tr>
			<td style='width:220px'>
			<span style="font-size:18px; color:#006633">{l s='Optional services'}</span>
			</td>
			<td style='width:550px'>

				<table cellpadding="8" cellspacing="8" style="width:550px">
				<tr><td></td><td></td><td></td></tr>
				{if $supporto_da_remoto->id > 0}
				<tr>
				<td style="width:160px"><strong>{l s='Remote support'}</strong></td><td style="width:250px"><a href="product.php?id_product={$supporto_da_remoto->id}" target="_blank">{$supporto_da_remoto->name}</a>
				</td><td style="width:70px">{convertPrice price=$supporto_da_remoto->price}</td>
					<td style="width:80px">
					<div class="acquista-listing-button-product">	
						<a class="ajax_add_to_cart_button" style="width:65px; font-size:13px; background-image:none; background-color:#e62026; margin-left:5px; padding:5px" name="{$supporto_da_remoto->id}" id="ajax_id_product_{$supporto_da_remoto->id}" href="{$link->getPageLink('cart.php')}?add&amp;id_product={$supporto_da_remoto->id}{if isset($static_token)}&amp;token={$static_token}{/if}" title="{l s='Add'}">{l s='Add'}</a>
						
				
					</div>
					</td>
				</tr>
				{else}{/if}
				
				{if $telegestione->id > 0}
				<tr>
				<td style="width:160px"><strong>{l s='Teleassistenza'}</strong></td><td style="width:250px"><a href="product.php?id_product={$telegestione->id}" target="_blank">{$telegestione->name}</a>
				</td><td style="width:70px">{convertPrice price=$telegestione->price}</td>
					<td style="width:80px">
					<div class="acquista-listing-button-product">	
						<a class="ajax_add_to_cart_button" style="width:65px; font-size:13px; background-image:none; background-color:#e62026; margin-left:5px; padding:5px" name="{$telegestione->id}" id="ajax_id_product_{$telegestione->id}" href="{$link->getPageLink('cart.php')}?add&amp;id_product={$telegestione->id}{if isset($static_token)}&amp;token={$static_token}{/if}" title="{l s='Add'}">{l s='Add'}</a>
						
				
					</div>
					</td>
				</tr>
				{else}{/if}
				
				
				{if $estensione_garanzia->id > 0}
				<tr>
				<td style="width:160px"><strong>{l s='Warranty extension'}</strong></td><td style="width:250px"> <a href="product.php?id_product={$estensione_garanzia->id}" target="_blank">{$estensione_garanzia->name}</a></td><td style="width:70px">{convertPrice price=$estensione_garanzia->price}</td>
					<td style="width:80px">
					<div class="acquista-listing-button-product">	
						<a class="ajax_add_to_cart_button" style="width:65px; font-size:13px; background-image:none; background-color:#e62026; margin-left:5px; padding:5px" name="{$estensione_garanzia->id}" id="ajax_id_product_{$estensione_garanzia->id}" href="{$link->getPageLink('cart.php')}?add&amp;id_product={$estensione_garanzia->id}{if isset($static_token)}&amp;token={$static_token}{/if}" title="{l s='Add'}">{l s='Add'}</a>
				
					</div>
					</td>
				</tr>
				{else}{/if}
				</table>
			</td>
	</table>
</div>
{/if}
	<br />
		{if $product.active == 1 && ($father_product->fuori_produzione == 0)}
			<div id="same-category-carousel">

				<h4 class="h4tab">{l s='Other bundles you may like'}</h4>
				
				{l s='WARNING: kit\'s prices could change if kit\'s products have quantity discounts. Please enter the quantity to verify the price of the kit for the desired quantity. You can always change the quantity of your products by entering the cart summary'}
				<br /><br />
				<table style="width:98%; margin: 0 auto;">
				{foreach from=$other_bundles item=other_bundle name=bundle_list}
						<tr>
							<td style='border-bottom:1px dotted #d4d4d4; text-align:left'>
								<table style="position:relative">
										<tr>
									{$father_product->getBundlesDisplay($other_bundle.id_bundle)}
										</tr>
								</table>
								
								<a href="{$link->getBundleLink($other_bundle.id_bundle)}" id="bundle_name_title_bundle-{$other_bundle.id_bundle}">
								
								{if $cookie->id_default_group == 3 || $cookie->id_default_group == 12}
								{$other_bundle.bundle_name|regex_replace:"/in omaggio/":""|regex_replace:"/omaggio/":""}
								{else}
								{$other_bundle.bundle_name}
								{/if}
								</a><br /><span style="color:#000000">(Codice:{$other_bundle.bundle_ref})</span><br /><br />
							</td>
							<td style='border-bottom:1px dotted #d4d4d4'>
								<img src="{$img_ps_dir}e_bundle.png" alt="" title="" />
							</td>
							<td style="text-align:center;width:190px;border-bottom:1px dotted #d4d4d4">
							{assign var='bundleprezzi' value=$father_product->getBundlesPrices($other_bundle.id_bundle)}
						
								
								{if $bundleprezzi.0 > $bundleprezzi.2}
								<table>
								<tr>
								<td>
								{l s='From'}</td><td style='text-align:right'><span style="font-size:18px; color:#e62026">{convertPrice price=$bundleprezzi.2}</span><span style="font-size:13px; color:#e62026"></td><td>{l s='t.e.'}</td><td>{l s='for'} {$bundleprezzi.3}+ {l s='kits'}</td></tr>
								<tr>
								<td></td><td style='text-align:right'> {convertPrice price=$bundleprezzi.0}</td><td>{l s='t.e.'}</td><td>{l s='for 1 kit'}</td></tr>
								</table>
								{else}
								<span style="font-size:18px; color:#e62026">{convertPrice price=$bundleprezzi.0}</span> <span style="font-size:13px; color:#e62026">{l s='t.e.'}</span>
								{/if}
								 
							
							</td>
							<td style='margin-right:0px; vertical-align:center; text-align:right; border-bottom:1px dotted #d4d4d4; padding-top:-15px'>
							<div style="float:right">
								<div class="quantity_wanted_bdl" style="width:80px; text-align:center; display:block; margin-left:30px; margin-bottom:-10px">{l s='Quantity'}<br />
								
							
								<div class="quantity_input_div">
									<input type="text" name="qty_bdl_{$other_bundle.id_bundle}" id="qty_bdl_{$other_bundle.id_bundle}" class="text quantity_input quantity_bundle" value="{if isset($quantityBackup)}{$quantityBackup|intval}{else}1{/if}" size="2" maxlength="3" />
									<div class="quantity_input_buttons">
										<span class="span_up" onclick="quantity_cart_add('qty_bdl_{$other_bundle.id_bundle}');">+</span>
										<span class="span_down" onclick="quantity_cart_rmv('qty_bdl_{$other_bundle.id_bundle}');">-</span>
									</div>
								</div>
								<div style="clear:both"></div>
								
								<input type='hidden' value='https://www.ezdirect.it/img/b/{$other_bundle.id_image}-{$other_bundle.id_bundle}-small.jpg' id='bundle_img_link_bundle-{$other_bundle.id_bundle}' />
								
								</div><br />
								<div class="acquista-listing-button-product">		
									<form id="buy_block" {if $PS_CATALOG_MODE AND !isset($groups)}class="hidden"{/if} action="{$link->getPageLink('cart.php')}" method="post">
										<input type="hidden" name="bundle" value="{$other_bundle.id_bundle}" />
										<input type="submit" value="{l s='Add to cart'}" title="{l s='Add to cart'}" id="bundle_{$other_bundle.id_bundle}" class="ajax_add_to_cart_button" style="width:80px; background-image:none; background-color:#e62026; margin-left:30px; padding:5px" />
									</form>
								</div>
							</div>
							</td>
						</tr>
					{/foreach}
				</table>
			</div>


			<script type='text/javascript' src="{$js_dir}bundle_quantity_preview.js">
			 

			 
			</script>
		{/if}
		

	

