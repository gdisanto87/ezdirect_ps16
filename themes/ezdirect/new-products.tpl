{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='New products'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<h1>{l s='New products'}</h1>

{if $products}
		<div class='block_listing_header'>
<div class='block-compare'>
{include file="$tpl_dir./product-compare.tpl"}
&nbsp;&nbsp;&nbsp;<span class="category-product-count"></span>
</div>
<div class='block-sort'>
				{include file="$tpl_dir./product-sort.tpl"}
</div>
</div>	
		{include file="$tpl_dir./product-list.tpl" products=$products}
{include file="$tpl_dir./product-compare.tpl"}
<br /><br />
		
	{else}
		<p class="warning">{l s='There are no products in this category.'}</p>
	{/if}
