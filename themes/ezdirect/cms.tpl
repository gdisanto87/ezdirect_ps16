{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if isset($cms) && ($content_only == 0)}
	{include file="$tpl_dir./breadcrumb.tpl"}
{/if}
{if isset($cms) && !isset($category)}
	{if !$cms->active}
		<br />
		<div id="admin-action-cms">
			<p>{l s='This CMS page is not visible to your customers.'}
			<input type="hidden" id="admin-action-cms-id" value="{$cms->id}" />
			<input type="submit" value="{l s='Publish'}" class="exclusive" onclick="submitPublishCMS('{$base_dir}{$smarty.get.ad}', 0)"/>			
			<input type="submit" value="{l s='Back'}" class="exclusive" onclick="submitPublishCMS('{$base_dir}{$smarty.get.ad}', 1)"/>			
			</p>
			<div class="clear" ></div>
			<p id="admin-action-result"></p>
			</p>
		</div>
	{/if}
	<div class="rte{if $content_only} content_only{/if}">
		{$cms->content|regex_replace:"/http\:\/\/www\.ez/":"https://www.ez"}
	</div>
	{if $cms->id == 1 || $cms->id == 18 || $cms->id == 4 || $cms->id == 3 || $cms->id == 5 || $cms->id == 6 || $cms->id == 7 || $cms->id == 30 || $cms->id == 31 || $cms->id == 32 || $cms->id == 33 || $cms->id == 34 || $cms->id == 35 || $cms->id == 36 || $cms->id == 37 || $cms->id == 39 || $cms->id == 51 || $cms->id == 52 || $cms->id == 55 || $cms->id == 59}
	{else}
	<div class="social">

			<a rel="nofollow" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=http%3A//{php} $url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; echo $url; {/php}" target="_blank"><img src="{$img_ps_dir}linkedin.gif" style="margin-left:0px" width="18" height="18" class="image-social" alt="{l s='Linkedin'}" title="{l s='Linkedin'}" /></a>
			<a rel="nofollow" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2F{php} $url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; echo $url; {/php}" target="_blank"><img src="{$img_ps_dir}facebook.gif" width="18" height="18"  class="image-social" alt="{l s='Facebook'}" title="{l s='Facebook'}" /></a>
			<a rel="nofollow" href="https://twitter.com/home/?status=Check+out+EZDIRECT+@+http%3A%2F%2F{php} $url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; echo $url; {/php}%2F" target="_blank"><img src="{$img_ps_dir}twitter.gif" width="18" height="18" class="image-social" alt="{l s='Twitter'}" title="{l s='Twitter'}" /></a>

			<a rel="nofollow" href="https://plus.google.com/share?url={php} $url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; echo $url; {/php}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="{$img_ps_dir}googleplus.gif" width="18" height="18" class="image-social" alt="{l s='Google Plus'}" title="{l s='Google Plus'}" /></a>
	
			</div>
		{/if}
{elseif isset($category)}
	<div class="block-cms">
		<h1>{$category->name|escape:'htmlall':'UTF-8'}</h1>
		{if isset($sub_category) & !empty($sub_category)}	
			<h4>{l s='List of sub categories in '}{$category->name}{l s=':'}</h4>
			<ul class="bullet">
				{foreach from=$sub_category item=subcategory}
					<li style="margin-bottom:5px">
						<a href="{$link->getCMSCategoryLink2($subcategory.id_cms_category, $subcategory.link_rewrite)|escape:'htmlall':'UTF-8'}">{$subcategory.name|escape:'htmlall':'UTF-8'}</a>
					</li>
				{/foreach}
			</ul>
		{/if}
		{if isset($cms_pages) & !empty($cms_pages)}
		<h4>{l s='List of pages in'}&nbsp;{$category->name}{l s=':'}</h4>
			<ul class="bullet">
				{foreach from=$cms_pages item=cmspages}
					<li style="margin-bottom:5px">
						<a href="{$link->getCMSLink2($cmspages.id_cms, $cmspages.link_rewrite)|escape:'htmlall':'UTF-8'}">{$cmspages.meta_title|escape:'htmlall':'UTF-8'}</a>
					</li>
				{/foreach}
			</ul>
		{/if}
	</div>
	
{else}
	{l s='This page does not exist.'}
{/if}
<br />