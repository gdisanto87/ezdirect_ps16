{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='My account'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}
<h1>{l s='My account'}</h1>

<h4>{l s='Welcome to your account. Here you can manage your addresses and orders.'}</h4>
<p style='font-size:16px; font-weight:bold; text-align:center'>{l s='Your customer code is'}: {$cookie->id_customer}</p>
<br />
{if ($cookie->customer_phone == '' && $cookie->customer_phone_mobile == '') || $cookie->customer_tax_code == '' || ($cookie->is_company == 1 && $cookie->customer_vat_number == '')}
<strong style="color:red">{l s='WARNING'}</strong>
<br />
{l s='There are some missing required data in your account. If you do not fill them you will not able to order products on our website. Here there are the missing or not valid fields:'}
<br /><br />
{if $cookie->customer_phone == '' && $cookie->customer_phone_mobile == ''}
{l s='Phone number'} - <a href="{$link->getPageLink('addresses.php', true)}">{l s='Click here to update your phone number'}</a><br />
{/if}
{if $cookie->customer_tax_code == ''}
{l s='Tax code'} - <a href="{$link->getPageLink('identity.php', true)}">{l s='Click here to update your tax code'}</a><br />
{/if}
{if $cookie->is_company == 1}
{if $cookie->customer_vat_number == ''}
{l s='VAT number'} - <a href="{$link->getPageLink('identity.php', true)}">{l s='Click here to update your vat number'}</a><br />
{/if}
{/if}
{else}

{if $orders && count($orders)}

	<div class='my-account-block'>
	<h4 class="h4tab">{l s='Last orders'} <a style="font-size:11px; color:#ffffff" href="{$link->getPageLink('history.php', true)}" title="">({l s='View all'})</a></h4>
	<table id="order-list" class="std" style="width:100%">
		<thead>
			<tr>
				<td><strong>{l s='Order'}</strong></th>
				<td><strong>{l s='Date'}</strong></th>
				<td><strong>{l s='Total price'}</strong></th>
				<td><strong>{l s='Payment'}</strong></th>
				<td><strong>{l s='Status'}</strong></th>
				<td><strong>{l s='Invoice'}</strong></th>
				<td style="width:65px">&nbsp;</strong></th>
			</tr>
		</thead>
		<tbody>
		{foreach from=$orders item=order name=myLoop}
			<tr class="{if $smarty.foreach.myLoop.first}first_item{elseif $smarty.foreach.myLoop.last}last_item{else}item{/if} {if $smarty.foreach.myLoop.index % 2}alternate_item{/if}">
				<td class="history_link bold">
					{if isset($order.invoice) && $order.invoice && isset($order.virtual) && $order.virtual}<img src="{$img_dir}icon/download_product.gif" class="icon" alt="{l s='Products to download'}" width="22" height="22" title="{l s='Products to download'}" />{/if}
					{l s='#'}{$order.id_order}
				</td>
				<td class="history_date bold">{dateFormat date=$order.date_add full=0}</td>
				<td class="history_price"><span class="price">{displayPrice price=$order.total_paid_real currency=$order.id_currency no_utf8=false convert=false}</span></td>
				<td class="history_method">{$order.payment|escape:'htmlall':'UTF-8'}</td>
				<td class="history_state">{if isset($order.order_state)}{$order.order_state|escape:'htmlall':'UTF-8'}{/if}</td>
				<td class="history_invoice">
				{if (isset($order.invoice) && $order.invoice && isset($order.invoice_number) && $order.invoice_number) && isset($invoiceAllowed) && $invoiceAllowed == true}
					<a href="{$link->getPageLink('pdf-invoice.php', true)}?id_order={$order.id_order|intval}" title="{l s='Invoice'}"><img src="{$img_dir}icon/pdf.gif" alt="{l s='Invoice'}" width="16" height="16" class="icon" /></a>
					<a href="{$link->getPageLink('pdf-invoice.php', true)}?id_order={$order.id_order|intval}" title="{l s='Invoice'}">{l s='PDF'}</a>
				{else}-{/if}
				</td>
				<td class="history_detail">
					
				</td>
			</tr>
		{/foreach}
		</tbody>
	</table>
	
	<br />
	</div>
	
	<div id="block-order-detail" class="hidden">&nbsp;</div>
	{else}
		
	{/if}

	{if $are_there_offers == 1}
	<div class='my-account-block' style='font-size:16px; padding-top:5px; padding-bottom:5px; text-align:center'> 
	<a href="{$link->getPageLink('modules/mieofferte/offerte.php', true)}"><img src="{$img_ps_dir}ci-sono-offerte-per-te.jpg" alt="{l s='There are offers for you'}" title="{l s='There are offers for you'}" class="icon" /></a>
	</div>
	{/if}

	<div class='my-account-block' style='font-size:16px; padding-top:5px; padding-bottom:5px; text-align:center'> 
	<img src="{$img_dir}icon/contact.gif" width="22" height="22" alt="{l s='Email'}" class="icon" /> {l s='Email with which you have registered your account'}: <strong>{$cookie->email}</strong>
	</div>

	<div class='my-account-block'> 
	<h4 class="h4tab">{l s='My addresses'} <a style="font-size:11px; color:#ffffff" href="{$link->getPageLink('addresses.php', true)}" title="">({l s='View all'})</a></h4>
	<div style='display:block; float:left; width: 250px; margin-left:15px; text-align:left'>
	<strong>{l s='Invoice address'}</strong><br /><br />
	<table class="my_account_table">
	<tr><td style="width:90px">{l s='Name'}</td><td><strong>{$fatturazione.firstname}</strong></td></tr>
	<tr><td>{l s='Last name'}</td><td><strong>{$fatturazione.lastname}</strong></td></tr>
	{if $fatturazione.is_company == 1}
	<tr><td>{l s='Company'}</td><td><strong>{$fatturazione.company}</strong></td></tr>
	<tr><td>{l s='VAT number'}</td><td><strong>{$fatturazione.vat_number}</strong></td></tr>
	{else}{/if}
	<tr><td>{l s='Tax code'}</td><td><strong>{$fatturazione.tax_code}</strong></td></tr>
	<tr><td>{l s='Address'}</td><td><strong>{$fatturazione.address1}</strong></td></tr>
	<tr><td>{l s='Zip'}</td><td><strong>{$fatturazione.postcode}</strong></td></tr>
	<tr><td>{l s='City'}</td><td><strong>{$fatturazione.city}</strong></td></tr>
	<tr><td>{l s='State'}</td><td><strong>{$fatturazione.iso_code}</strong></td></tr>
	<tr><td>{l s='Country'}</td><td><strong>{$fatturazione.country}</strong></td></tr>
	</table>
	</div>
	
	<div style='display:block; float:left; width: 250px; margin-left:15px; text-align:left'>
	<strong>{l s='Shipping address'}</strong><br /><br />
	<table class="my_account_table">
	<tr><td style="width:80px">{l s='Name'}</td><td><strong>{$spedizione1.firstname}</strong></td></tr>
	<tr><td>{l s='Last name'}</td><td><strong>{$spedizione1.lastname}</strong></td></tr>
	
	<tr><td>{l s='Address'}</td><td><strong>{$spedizione1.address1}</strong></td></tr>
	<tr><td>{l s='Zip'}</td><td><strong>{$spedizione1.postcode}</strong></td></tr>
	<tr><td>{l s='City'}</td><td><strong>{$spedizione1.city}</strong></td></tr>
	<tr><td>{l s='State'}</td><td><strong>{$spedizione1.iso_code}</strong></td></tr>
	<tr><td>{l s='Country'}</td><td><strong>{$spedizione1.country}</strong></td></tr>
	</table>
	</div>
	
	
		<div style='display:block; float:left; width: 230px; margin-left:15px; text-align:left'>
		<br /><br /><br /><br /><br /><br /><br /><br /><br />
	<a href="{$link->getPageLink('addresses.php', true)}" title="">{l s='View all my addresses'}...</a>
	
	</div>
	
	
	
	<div style='clear:both'></div>
	<br />
	</div>
	
	{if $fido_tot > 0}
	<div class='my-account-block'> 
	<h4 class="h4tab">{l s='Area finance'}</h4>
	
	<p style='text-align:center'>
	{l s='Hai un credito residuo di'}<br /><br />
	<span style="font-size:24px; {if $crediti_fido > 0} color:#0c8f2d; {else} {/if}">{$crediti_fido} &euro;</span><br /><br />
	Il tuo fido &egrave; di <span>{$fido} &euro;</span>
	</p>
		
	</div>
	
	{/if}
	
	<div class='my-account-block'> 
	<h4 class="h4tab">{l s='Account area'}</h4>
	<table style='width:100%'>
	<tr>
		<td style='width:120px'>
		<a href="{$link->getPageLink('identity.php', true)}"><img width="60" height="60" src='{$img_dir}icon/dati-personali.gif' alt="{l s='Information'}" title="{l s='Information'}" /></a><br />
		<a href="{$link->getPageLink('identity.php', true)}" title="{l s='Information'}">{l s='My personal information'}</a>
		</td>
		
		<td style='width:120px'>
		<a href="{$link->getPageLink('addresses.php', true)}"><img width="60" height="60" src='{$img_dir}icon/i-miei-indirizzi.gif' alt="{l s='Addresses'}" title="{l s='Addresses'}" /></a><br />
		<a href="{$link->getPageLink('addresses.php', true)}" title="{l s='Addresses'}">{l s='My addresses'}</a>
		</td>
		
		<td style='width:120px'>
		<a href="{$base_dir_ssl}modules/mieofferte/offerte.php"><img src='{$img_dir}icon/offers.gif' alt="{l s='My offers'}" width="60" height="60" title="{l s='My offers'}" /></a><br />
		<a href="{$base_dir_ssl}modules/mieofferte/offerte.php" title="{l s='My offers'}">{l s='My offers'}</a>
		</td>
		
		<td style='width:120px'>
		<a href="{$base_dir_ssl}modules/mytickets/tickets.php"><img width="60" height="60" src='{$img_dir}icon/tickets.gif' alt="{l s='My tickets'}" title="{l s='My tickets'}" /></a><br />
		<a href="{$base_dir_ssl}modules/mytickets/tickets.php" title="{l s='My tickets'}">{l s='My tickets'}</a>
		</td>
		
		<td style='width:120px'>
		<a href="{$base_dir_ssl}modules/mailalerts/myalerts.php"><img width="60" height="60" src='{$img_dir}icon/avvisi.gif' alt="{l s='My alerts'}" title="{l s='My alerts'}" /></a><br />
		<a href="{$base_dir_ssl}modules/mailalerts/myalerts.php" title="{l s='My alerts'}">{l s='My alerts'}</a>
		</td>
		
		
	</tr>
	</table>
		
	</div>

	
	
		<div class='my-account-block'> 
	<h4 class="h4tab">{l s='Orders area'}</h4>
	<table style='width:100%'>
	<tr>
		<td style='width:110px'>
		<a href="{$link->getPageLink('history.php', true)}"><img src='{$img_dir}icon/i-miei-ordini.gif' alt="{l s='Orders'}" width="60" height="60" title="{l s='Orders'}" /></a><br />
		<a href="{$link->getPageLink('history.php', true)}" title="{l s='Orders'}">{l s='History and details of my orders'}</a>
		</td>
		
		
		
		
		
		<td style='width:110px'>
		<a href="{$base_dir_ssl}modules/miefatture/fatture.php"><img src='{$img_dir}icon/invoices.gif' alt="{l s='My invoices'}" width="60" height="60" title="{l s='My invoices'}" /></a><br />
		<a href="{$base_dir_ssl}modules/miefatture/fatture.php" title="{l s='My invoices'}">{l s='My invoices'}</a>
		</td>
		
		<td style='width:110px'>
		<a href="{$link->getPageLink('order-slip.php', true)}"><img src='{$img_dir}icon/note-di-credito.gif' alt="{l s='Credit slips'}" width="60" height="60" title="{l s='Credit slips'}" /></a><br />
		<a href="{$link->getPageLink('order-slip.php', true)}" title="{l s='Credit slips'}">{l s='My credit slips'}</a>
		</td>
		
		<td style='width:110px'>
		<a href="{$link->getPageLink('discount.php', true)}"><img src='{$img_dir}icon/i-miei-coupon.gif' alt="{l s='Vouchers'}" width="60" height="60" title="{l s='Vouchers'}" /></a><br />
		<a href="{$link->getPageLink('discount.php', true)}" title="{l s='Vouchers'}">{l s='My vouchers'}</a>
		</td>
		{if $cookie->id_default_group == 3}
{else}
		<td style='width:110px'>
		<a href="{$base_dir_ssl}modules/loyalty/loyalty-program.php"><img src='{$img_dir}icon/punti-fedelta.gif' alt="{l s='My loyalty points'}" width="60" height="60" title="{l s='My loyalty points'}" /></a><br />
		<a href="{$base_dir_ssl}modules/loyalty/loyalty-program.php" title="{l s='My loyalty points'}">{l s='My loyalty points'}</a>
		</td>
{/if}
	</tr>
	</table>
	
	</div>

		<div class='my-account-block'> 
	<h4 class="h4tab">{l s='Contacts area'}</h4>
	<table style='width:100%; vertical-align:top'>
	<tr>
		<td style='width:110px; vertical-align:top'>
		<a href="{$base_dir_ssl}modules/formprevendita/form.php"><img src='{$img_dir}icon/preventivi.gif' title="{l s='Products quotation'}" width="60" height="60" alt="{l s='Products quotation'}" /></a><br />
		<a href="{$base_dir_ssl}modules/formprevendita/form.php" title="{l s='Products quotation'}">{l s='Products quotation'}</a>
		</td>
		
		<td style='width:110px; vertical-align:top'>
		<a href="{$link->getPageLink('contact-form.php', true)}?step=contabilita"><img src='{$img_dir}icon/assistenza-amministrativa.gif' width="60" height="60" title="{l s='Administrative assistance'}" alt="{l s='Administrative assistance'}" /></a><br />
		<a href="{$link->getPageLink('contact-form.php', true)}?step=contabilita" title="{l s='Administrative assistance'}">{l s='Administrative assistance'}</a>
		</td>
		
		<td style='width:110px; vertical-align:top'>
		<a href="{$link->getPageLink('contact-form.php', true)}?step=assistenza-ordini"><img src='{$img_dir}icon/assistenza-ordini.gif' width="60" height="60" title="{l s='Administrative assistance'}" alt="{l s='Orders assistance'}" /></a><br />
		<a href="{$link->getPageLink('contact-form.php', true)}?step=assistenza-ordini" title="{l s='Orders assistance'}">{l s='Orders assistance'}</a>
		</td>
		
		<td style='width:110px; vertical-align:top'>
		<a href="{$link->getPageLink('contact-form.php', true)}?step=tecnica"><img src='{$img_dir}icon/assistenza-tecnica.gif' width="60" height="60" title="{l s='Technical assistance'}" alt="{l s='Technical assistance'}" /></a><br />
		<a href="{$link->getPageLink('contact-form.php', true)}?step=tecnica" title="{l s='Technical assistance'}">{l s='Technical assistance'}</a>
		</td>
		
		<td style='width:110px; vertical-align:top'>
		<a href="{$link->getPageLink('cms.php?id_cms=36', true)}"><img src='{$img_dir}icon/rma.gif' title="{l s='RMA'}" width="60" height="60" alt="{l s='RMA'}" /></a><br />
		<a href="{$link->getPageLink('cms.php?id_cms=36', true)}" title="{l s='RMA'}">{l s='RMA'}</a>
		</td>
	
	
		
		<td style='width:110px; vertical-align:top'>
		<a href="{$link->getPageLink('contact-form.php', true)}?step=rivenditori"><img src='{$img_dir}icon/rivenditori.gif' width="60" height="60" title="{l s='Resellers'}" alt="{l s='Resellers'}" /></a><br />
		<a href="{$link->getPageLink('contact-form.php', true)}?step=rivenditori" title="{l s='Resellers'}">{l s='Resellers'}</a>
		</td>
	
	</tr>
	
	</table>
		
	</div>
	
	<div class='my-account-block'> 
	<h4 class="h4tab">{l s='Data deletion area'}</h4>
	<table style='width:100%; vertical-align:top'>
	<tr>
		<td style='width:110px; vertical-align:top'>
		<a href="https://www.ezdirect.it/modules/cancellami/cancella-miei-dati.php"><img width="60" height="60" src='{$img_dir}icon/cancella.jpg' alt="{l s='Data deletion area'}" title="{l s='Data deletion area'}" /></a><br />
		
		<a href="https://www.ezdirect.it/modules/cancellami/cancella-miei-dati.php">{l s='Do you want to delete your data from our website? Please click on this link: you will find some further informations and a button to send a request to our technical staff'}</a> <br /><br />
		</td>
	
	</tr>
	
	</table>
		
	</div>
	
	
	
	
	
{/if}