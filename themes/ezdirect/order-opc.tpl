{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $smarty.now|date_format:"%Y-%m-%d" < "2019-08-19"}
{literal}
<script type="text/javascript">
				$(document).ready(function() {
					$("#hidden_link").fancybox().trigger("click");
				});
				</script>
			<a id="hidden_link" href="#data"></a>

			<div style="display:none"><div id="data">
			<p><br /><br /></p>
<p>
<strong>Gentile cliente, ti informiamo che le consegne per ordini effettuati sul nostro portale, saranno gestite a partire dal giorno 19 Agosto.
<br /><br />
Ci scusiamo per l'eventuale disagio.</strong></p><br /><br /></div></div>
{/literal}
{/if}
<div id="cart_env">

{if $PS_CATALOG_MODE}
	{capture name=path}{l s='Your shopping cart'}{/capture}
	{include file="$tpl_dir./breadcrumb.tpl"}
	<h2 id="cart_title">{l s='Your shopping cart'}</h2>
	<p class="warning">{l s='This store has not accepted your new order.'}</p>
{else}
<script type="text/javascript">
	// <![CDATA[
	var imgDir = '{$img_dir}';
	var authenticationUrl = '{$link->getPageLink("authentication.php", true)}';
	var orderOpcUrl = '{$link->getPageLink("order-opc.php", true)}';
	var historyUrl = '{$link->getPageLink("history.php", true)}';
	var guestTrackingUrl = '{$link->getPageLink("guest-tracking.php", true)}';
	var addressUrl = '{$link->getPageLink("address.php", true)}';
	var orderProcess = 'order-opc';
	var guestCheckoutEnabled = {$PS_GUEST_CHECKOUT_ENABLED|intval};
	var currencySign = '{$currencySign|html_entity_decode:2:"UTF-8"}';
	var currencyRate = '{$currencyRate|floatval}';
	var currencyFormat = '{$currencyFormat|intval}';
	var currencyBlank = '{$currencyBlank|intval}';
	var displayPrice = {$priceDisplay};
	var taxEnabled = {$use_taxes};
	var conditionEnabled = {$conditions|intval};
	var countries = new Array();
	var countriesNeedIDNumber = new Array();
	var countriesNeedZipCode = new Array();
	var vat_management = {$vat_management|intval};

	var txtWithTax = "{l s='(tax incl.)'}";
	var txtWithoutTax = "{l s='(tax excl.)'}";
	var txtHasBeenSelected = "{l s='has been selected'}";
	var txtNoCarrierIsSelected = "{l s='No carrier has been selected'}";
	var txtNoCarrierIsNeeded = "{l s='No carrier is needed for this order'}";
	var txtConditionsIsNotNeeded = "{l s='No terms of service must be accepted'}";
	var txtTOSIsAccepted = "{l s='Terms of service is accepted'}";
	var txtTOSIsNotAccepted = "{l s='Terms of service have not been accepted'}";
	var txtThereis = "{l s='There is'}";
	var txtErrors = "{l s='error(s)'}";
	var txtDeliveryAddress = "{l s='Delivery address'}";
	var txtInvoiceAddress = "{l s='Invoice address'}";
	var txtModifyMyAddress = "{l s='Modify my address'}";
	var txtInstantCheckout = "{l s='Instant checkout'}";
	var errorCarrier = "{$errorCarrier}";
	var errorTOS = "{$errorTOS}";
	var checkedCarrier = "{if isset($checked)}{$checked}{else}0{/if}";

	var addresses = new Array();
	var isLogged = {$isLogged|intval};
	var isGuest = {$isGuest|intval};
	var isVirtualCart = {$isVirtualCart|intval};
	var isPaymentStep = {$isPaymentStep|intval};
	//]]>
</script>

{if $cart_by_ezdirect == 1}
	{if $sta_usando_diverso == 1}
		<script type="text/javascript">alert("{l s='You have edited an offer made by Ezdirect'}.");</script>
	{/if}

{/if}

	{if $productNumber}
	
		{if $isLogged && $are_there_offers == 1}
		<div style="width:100%; display:inline-block; margin:0 auto">
		{if $preventivo == 1}
			<div class="box-carrello-img" style="font-size:18px; float:left; background-image:url({$img_ps_dir}id-offerta.jpg)">{$id_cart}-{$revisioni}</div>
			<div class="box-carrello-img" style="font-size:18px; float:left; background-image:url({$img_ps_dir}referente.jpg)">{$referente}</div>
		{/if}
		
			<!-- <a href="{$base_dir_ssl}modules/mieofferte/offerte.php" class="button_large" title="{l s='View all my offers'}" style="display:block; float:left; margin-top:10px; margin-bottom:20px; width:28%; padding-top:7px; height:23px"><img src='{$img_dir}button-icon/cart.png' alt='View all my offers' title='View all my offers' /><span>{l s='View all my offers'}</span></a> -->
			
			<!-- <a href="order-opc.php" class="button_large" title="{l s='View all my offers'}" style="display:block; float:left; margin-left:2%; margin-top:10px; margin-bottom:20px; width:28%; padding-top:7px; height:23px"><img src='{$img_dir}button-icon/update.png' alt='{l s='Refresh cart'}' title='{l s='Refresh cart'}' /><span>{l s='Refresh cart'}</span></a> -->
			
			<div class="box-carrello-img" style="font-size:18px; float:left; background-image:url({$img_ps_dir}vedi-le-offerte.jpg)"><a href="{$base_dir_ssl}modules/mieofferte/offerte.php" style="display:block;margin-top:-20px; text-decoration:none">{l s='View all my offers'}</a></div>
			
			<div class="box-carrello-img" style="font-size:18px; float:left; background-image:url({$img_ps_dir}aggiorna-offerta.jpg)"><a href="order-opc.php" style="display:block;margin-top:-20px; text-decoration:none">{l s='Refresh cart'}</a></div>
	
			
		</div>
	<div style="clear:both"></div>
	{/if}
	
		{if $premessa != ''}
		<div class="shopping_cart_table" {if !$opc}{else}style="width:98.3%; padding:5px; margin-left:1px"{/if}>
		<h2>Premessa</h2>
		{$premessa}
		</div><br /><div style="clear:both"></div>
		{/if}
	
		<div class="shopping_cart_table" id="shopping_cart_steps" style="position:relative; display:block; margin:0 auto; width:99.2%;">
		<div class="shopping_cart_header" style="z-index:1; height:35px; background-color:#db6906; background-repeat:no-repeat;">
		<div style="width:32%; display:block; margin-top:7px; margin-right:10px; float:left">
			<h2 style="color:#fff">1. {l s='Your data'}</h2>
		</div>
		<div style="width:33%; display:block; margin-top:7px; float:left">
			<h2 style="color:#fff">2. {l s='Delivery methods'}</h2>
		</div>
		<div style="width:32%; margin-top:7px; padding-left:10px; display:block; float:left">
			<h2 style="color:#fff">3. {l s='Payment methods'}</h2>
		</div>
		</div><div class="clear:both"></div>
		
			<div id="opc-account" style="width:32%; display:block; float:left; border-right:1px solid #c1c1c1">
			 <h2 class="shopping-h2">1. {l s='Your data'}</h2>
				{if $isLogged AND !$isGuest}
					{include file="$tpl_dir./order-address-quick.tpl"}
				{else}
					<!-- Create account / Guest account / Login block -->
					{include file="$tpl_dir./order-opc-new-account.tpl"}
					<!-- END Create account / Guest account / Login block -->
				{/if}
			</div>
			<div id="opc-shipping" style="width:33%; padding-left:10px; padding-right:10px; position:relative; display:block; float:left">
			
				<h2 class="shopping-h2">2. {l s='Delivery methods'}</h2> 
				<!-- Carrier -->
				{include file="$tpl_dir./order-carrier-quick.tpl"}
				<!-- END Carrier -->
				{if $check_fido == 'no'}
				
				{else}
					<div style="display:none" id="check_fido">
					{if ($orderTotal > $crediti_fido)}
					
					{assign var="stop_fido" value="true"}
					
					{else}
					{/if}
					</div>
				{/if}
						{if $cfcheck == 0}
		<p><br />
		
		<div style="position:relative;height:350px; "><span style="color:red">	{l s='There are some details missing in your account: if you are a end user customer you must specify your tax code in your account details, and if you are a company you must specify both tax code and vat number. You must also specify a valid address. Please click on the single details to fix them'}.</span><br />
			{l s='Here there are your missing details'}: <br />
			{if $check_tx == 1} <a href="{$link->getPageLink('identity.php', true)}" target="_blank">{l s='Tax code'}</a> | {else}{/if}
			{if $check_vat == 1} <a href="{$link->getPageLink('identity.php', true)}" target="_blank">{l s='VAT number'}</a> | {else}{/if}
			{if $check_phone == 1} <a href="{$link->getPageLink('address.php', true)}?id_address={$id_address|intval}" target="_blank">{l s='Phone number'}</a> | {else}{/if}
			{if $check_address == 1} <a href="{$link->getPageLink('address.php', true)}?id_address={$id_address|intval}" target="_blank">{l s='Address'}</a> | {else}{/if}
			{if $check_postcode == 1} <a href="{$link->getPageLink('address.php', true)}?id_address={$id_address|intval}" target="_blank">{l s='Postcode'}</a> | {else}{/if}
			{if $check_city == 1} <a href="{$link->getPageLink('address.php', true)}?id_address={$id_address|intval}" target="_blank">{l s='City'}  |</a>{else} {/if}
			{if $check_state == 1} <a href="{$link->getPageLink('address.php', true)}?id_address={$id_address|intval}" target="_blank">{l s='State'}  |</a>{else} {/if}
			{if $check_country == 1} <a href="{$link->getPageLink('address.php', true)}?id_address={$id_address|intval}" target="_blank">{l s='Country'} |</a>{else} {/if}
			</div>
			{else if $cfcheck == 2}
			{l s='You must login to continue'}
			{else}
			{/if}
			<div style="clear:both"></div>
			</div>
			
			<script type="text/javascript">
				{if $validity_check == 1}
					alert("{l s='Sorry, your offer has expired. Please contact our staff at'} 0585 821163");
				{/if}
				
				{literal}
				function controlla_termini() {
				{/literal}			
				
				{if ($payment_method_cart == "R.B. 30 GG. D.F. F.M." || $payment_method_cart == "R.B. 60 GG. D.F. F.M." || $payment_method_cart == "R.B. 60 GG. D.F. F.M." || $payment_method_cart == "Bonifico 30 gg. fine mese" || $payment_method_cart == "Bonifico 60 gg. fine mese" || $payment_method_cart == "Bonifico 90 gg. fine mese")}
				
					{if ($check_fido == 'yes' && $orderTotal > $crediti_fido)}
					{literal}
						if(document.getElementById('other_payment_p').checked) {
							$('#check_fido').show();
							$.fancybox('Gentile Cliente, non &egrave; possibile gestire questo ordine con pagamento differito.<br /><br />Ti invitiamo a comunicare con il nostro ufficio amministrativo per i chiarimenti del caso.<br /><br />Chiama lo 0585821163 (selezione 4), oppure apri un ticket <a href="https://www.ezdirect.it/contattaci?step=contabilita">qui</a>',
								 {
									
									'autoScale': true,
									 'transitionIn': 'elastic',
									 'transitionOut': 'elastic',
									 'speedIn': 100,
									 'speedOut': 100,
									 'autoDimensions': true,
									 'centerOnScroll': true,
									 'overlayShow': true
									 
								 });
							return false;
						}
						else
						{
							$('#check_fido').hide();
						}
					{/literal}
					{else}
					{/if}
				{else}
				{/if}
				{if $validity_check == 1}
					alert("{l s='Sorry, your offer has expired. Please contact our staff at'} 0585 821163");
					return false;
				{/if}

				 {if !isset($carriers) || !$carriers || count($carriers) == 0}
					alert("{l s='Sorry, shipping is currently not available for your country. Please contact our staff at'} 0585 821163");
					return false;
				 {/if}
				 
				 {literal}
					if ($('input[name=payment]:checked').length > 0) {
						
					}
					else {
						
						$('#payment_notice').show();
						$('html, body').animate({
							scrollTop: ($("#payment_notice").offset().top)-155
						}, 300);
						alert("{/literal}{l s='You must choose a payment method before continue'}. {l s='If you have problems please contact our customer service'}{literal}");
							return false;
					
					}
					
				
					
					if(document.getElementById('cgv').checked) {

					}
						
						
					else {
						$('#cgv_notice').show();
						$('html, body').animate({
							scrollTop: ($("#cgv_notice").offset().top)-155
						}, 300);
						alert("{/literal}{l s='You must accept our terms before continue'}. {l s='If you have problems please contact our customer service'}{literal}");
						return false;
					}
				
					
					fbq('track', 'InitiateCheckout');
			

				}
				
				{/literal}
			</script>
			<form action="order-payment.php" method="post">
			<div id="opc-payment" style="width:31.5%; display:block; float:left; padding-left:10px; margin-bottom:365px; border-left:1px solid #c1c1c1">
				<h2 class="shopping-h2">3. {l s='Payment methods'}</h2>
				<!-- Payment -->
				{include file="$tpl_dir./order-payment-quick.tpl"}
				<!-- END Payment -->
				

			</div>
			
			<div style="clear:both"></div>
			
		
			
			
			<p id="insert-comment">
			&nbsp;
			{l s='Would you like to add a comment?'}<br /> <input type="text" value="{if isset($oldMessage)}{$oldMessage}{else}{l s='Insert text here'}{/if}" id="message" name="message" onfocus="if {literal} (this.value == {/literal} '{l s='Insert text here'}') {literal} { this.value = '' } else { } {/literal}" />
			</p>
			
				{if $conditions AND $cms_id}<br /><br />
	
	<p class="bubble" style="right: 10px; position:absolute; bottom:168px; margin-top:3px; display:none" id="cgv_notice">
	{l s='You must accept our terms before continue'}
	</p>
	
	<p id="cart-hand">
	<img src='{$img_ps_dir}cart-hand.jpg' alt="{l s='I agree to the terms of service and adhere to them unconditionally.'}" title="{l s='I agree to the terms of service and adhere to them unconditionally.'}" />
	</p>
	
	<p class="checkbox" style="right: 200px; position:absolute; bottom:34px; margin-top:3px">
		<input type="checkbox" onclick="$('#cgv_notice').hide();" name="cgv" id="cgv" value="1" {if $checkedTOS}checked="checked"{/if} />
		<label for="cgv"><strong>{l s='I agree to the terms of service and adhere to them unconditionally.'}</strong></label> <a href="cms.php?id_cms=3" target="_blank">{l s='(read)'}</a><br /><br /><br /><br /><br /><br />
	</p>
	<script type="text/javascript">$('a.iframe').fancybox();</script>
{/if}
			
			
			<p id="aiuto-telefono" style="position:absolute; right:10px; bottom:0px; width:575px; height:107px">
				<a href="tel:+39-0585091298"><img src="{$img_ps_dir}aiuto-numero-verde.gif" alt="{l s='Need help? Call our free number 800 52 97 67'}" width="500" height="107" /></a><br />
			
		
			
			
			</p><div style="clear:both"></div>
			
			{if $preventivo == 1 && $date_check == 1}
		
			<script type="text/javascript">
		
					alert("Gentile cliente, ti inviamo questo messaggio, in quanto sei registrato sul nostro sito web www.ezdirect.it, ed hai nel carrello (inseriti da te o da ns consulenti a fronte di una tua richiesta di preventivo), uno o piu prodotti, i quali hanno subito una variazione di prezzo. Suggeriamo di verificare, accedendo al tuo account. Per ulteriore supporto contatta il nostro team allo 0585821163, oppure apri un ticket dalla tua area personale.");

			</script>
			
			{else}
			{if $cfcheck == 0 || $cfcheck == 2}
			<p class="cart_navigation" style="position:absolute; right:10px; bottom:95px">
				{if !$isLogged}
				<input type="submit" class='button_large' id='payment_button' onclick='alert("{l s='You must login to continue'} {l s='If you have problems please contact our customer service'}."); return false;' style="display:block; margin-left:5px;" value="{l s='Continue'}" />
				{else if $errors|@count > 0}
				<input type="submit" class='button_large' id='payment_button' onclick='alert("{l s='There are some problems in your cart. Please check the errors dialog'}. {l s='If you have problems please contact our customer service'}"); return false;' style="display:block; position:absolute; right:5px" value="{l s='Continue'}" />
				{else}
				<input type="submit" class='button_large' id='payment_button' onclick='alert("{l s='There are some details missing in your account: if you are a end user customer you must specify your tax code in your account details, and if you are a company you must specify both tax code and vat number. Please go to your account and update your profile details. MISSING DETAILS: '}{if $check_tx == 1}{l s='Tax code'}{else}{/if}{if $check_vat == 1}{l s='VAT number'}{else}{/if}{if $check_phone == 1}{l s='Phone number'}{else}{/if}. {l s='If your details seem correct, please update this page. If your problem does not solve, please contact our customer service (800 529767)'}"); return false;' style="display:block; margin-left:5px;" value="{l s='Continue'}" />
				{/if}
			{else}
			<p class="cart_navigation" style="position:absolute; right:10px; bottom:100px">
					{if $errors|@count > 0}
					{else}
		
			
		
				
					<input type="submit" class='button_large' id='payment_button' onclick="return controlla_termini();" style="display:block; margin-left:5px;" value="{l s='Continue'}" />
			
			
			
			{/if}
		
		{/if}
			{/if}
			</p>
			<div style="clear:both"></div>
		</div>
		<div style="clear:both"></div>
		<!-- Shopping Cart -->
		<br />
		{include file="$tpl_dir./shopping-cart-quick.tpl"}
		<!-- End Shopping Cart -->
	{else}
		{capture name=path}{l s='Your shopping cart'}{/capture}
		{include file="$tpl_dir./breadcrumb.tpl"}
		<h2>{l s='Your shopping cart'}</h2>
		<div style="margin-top:5px; text-align:center; margin:0auto">
			<!-- <div class="block myaccount" style=" display: inline-block; height:71px; overflow:hidden;">
			<a href="order-opc.php"><img src="{$base_dir_ssl}/img/aggiorna-carrello.gif" title="{l s='Refresh cart'}" alt="{l s='Refresh cart'}" /></a>
			</div> -->
			
			{if $are_there_offers == 1}
			<div class="block myaccount" style=" display: inline-block; height:71px; overflow:hidden;">
			<a href="{$link->getPageLink('modules/mieofferte/offerte.php', true)}?id_cart={$ultima_offerta}&id_customer={$cookie->id_customer}"><img src="{$base_dir_ssl}/img/clicca-per-preventivo.gif" title="{l s='My offers' mod='blockpreventivi'}" alt="{l s='My offers' mod='blockpreventivi'}" /></a>
			</div>

			{/if}
			<div style="clear:both"></div>
		</div>	
		<p class="warning" style="margin-left:0px; margin-right:0px">{l s='Your shopping cart is empty.'}</p>
		
			
		
		
		<div class="shopping_cart_table" style="height:127px; position:relative; margin-left:0px; margin-top:-7px;">
<div style="display:block;float:left;">
<img src="{$img_ps_dir}contatta-servizio-clienti.gif" alt="Customer care 0585 821163" width="122" height="127" />
</div>
<div style="display:block;float:left">
<a href="tel:+39-0585091298"><img src="{$img_ps_dir}se-hai-bisogno-di-aiuto.gif" alt="{l s='Se hai bisogno di aiuto per il tuo ordine, contatta il servizio clienti al numero 0585 821163'}" width="602" height="90" /></a><br />
<a target='_blank' href='https://www.ezdirect.it/contattaci?step=assistenza-ordini'><img src="{$img_ps_dir}oppure-apri-ticket.gif" width="454" height="15" alt="{l s='Oppure apri un ticket cliccando qui'}" style="border:0px" /></a>
</div>
<p class="clear"></p>
</div>
		<div style="clear:both"></div></div>
	{/if}
{/if}



<div><div>

