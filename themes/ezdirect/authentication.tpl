{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{*
** Compatibility code for Prestashop older than 1.4.2 using a recent theme
** Ignore list isn't require here
** $address exist in every PrestaShop version
*}

{* Will be deleted for 1.5 version and more *}
{* Smarty code compatibility v2 *}
{* If ordered_adr_fields doesn't exist, it's a PrestaShop older than 1.4.2 *}
{if !isset($dlv_all_fields)}
		{$dlv_all_fields.0 = 'company'}
		{$dlv_all_fields.1 = 'firstname'}
		{$dlv_all_fields.2 = 'lastname'}
		{$dlv_all_fields.3 = 'address1'}
		{$dlv_all_fields.4 = 'address2'}
		{$dlv_all_fields.5 = 'suburb'}
		{$dlv_all_fields.6 = 'c_o'}
		{$dlv_all_fields.7 = 'postcode'}
		{$dlv_all_fields.8 = 'city'}
		{$dlv_all_fields.9 = 'country'}
		{$dlv_all_fields.10 = 'state'}
{/if}

{capture name=path}{l s='Login'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<script type="text/javascript" src="{$js_dir}etc/noSpaces.js">
</script>
<script type="text/javascript" src="{$js_dir}etc/profileChange.js">
</script>
<script type="text/javascript" src="https://www.ezdirect.it/themes/ezdirect-new/js/tools.js">
</script>

<script type="text/javascript">
// <![CDATA[
idSelectedCountry = {if isset($smarty.post.id_state)}{$smarty.post.id_state|intval}{else}false{/if};
countries = new Array();
countriesNeedIDNumber = new Array();
countriesNeedZipCode = new Array();
{if isset($countries)}
	{foreach from=$countries item='country'}
		{if isset($country.states) && $country.contains_states}
			countries[{$country.id_country|intval}] = new Array();
			{foreach from=$country.states item='state' name='states'}
				countries[{$country.id_country|intval}].push({ldelim}'id' : '{$state.id_state}', 'name' : '{$state.name|escape:'htmlall':'UTF-8'}'{rdelim});
			{/foreach}
		{/if}
		{if $country.need_identification_number}
			countriesNeedIDNumber.push({$country.id_country|intval});
		{/if}
		{if isset($country.need_zip_code)}
			countriesNeedZipCode[{$country.id_country|intval}] = {$country.need_zip_code};
		{/if}
	{/foreach}
{/if}
$(function(){ldelim}
	$('.id_state option[value={if isset($smarty.post.id_state)}{$smarty.post.id_state}{else}{if isset($address)}{$address->id_state|escape:'htmlall':'UTF-8'}{/if}{/if}]').attr('selected', 'selected');
{rdelim});
//]]>
{if $vat_management}
	{literal}
	$(document).ready(function() {
		$('#company').blur(function(){
			vat_number();
		});
		vat_number();
		function vat_number()
		{
			if ($('#company').val() != '')
				$('#vat_number').show();
			else
				$('#vat_number').hide();
		}
	});
	{/literal}
{/if}
</script>
<h1>{if !isset($email_create)}{l s='Log in'}{else}{l s='Create your account'}{/if}</h1>
{assign var='current_step' value='login'}


{include file="$tpl_dir./errors.tpl"}
{assign var='stateExist' value=false}

{if $back == 'order.php'}
{include file="$tpl_dir./order-steps.tpl"}
<div class="shopping_cart_table" id="authentication_form">
{else}
<div id="authentication_form">
{/if}
{if !isset($email_create)}
	<div class="block">
		<form action="{$link->getPageLink('authentication.php', true)}{if $smarty.get.back}?back={$smarty.get.back}{/if}{if $smarty.get.cliente == 'rivenditore'}?cliente=rivenditore{else}{/if}" method="post" class="std">
			<fieldset>
			
				<h3>{l s='Create your account'}</h3>
				<p style="padding:10px"><strong>{if $smarty.get.cliente == 'rivenditore'}{l s='Partners and resellers area'}.{else}{l s='Enter your e-mail address to create an account'}. {/if}</strong></p>
				<p style="padding:10px">
			<span><input type="text" id="email_create" name="email_create" style="margin-top:-10px" value="{if isset($smarty.post.email_create)}{$smarty.post.email_create|escape:'htmlall':'UTF-8'|stripslashes}{/if}" class="account_input" /></span>
				
				{if isset($back)}<input type="hidden" class="hidden" name="back" value="{$back|escape:'htmlall':'UTF-8'}" />{/if}
				<div style="clear:both"></div>
					<input type="submit" id="SubmitCreate" name="SubmitCreate" class="button_large" style="margin-left:12px; margin-top:16px" value="{l s='Create your account'}" />
					<input type="hidden" class="hidden" name="SubmitCreate" value="{l s='Create your account'}" />
				</p>
				{if $smarty.get.cliente == 'rivenditore'}
				<p style="padding:10px"><strong>
				{l s='In the next step you will receive the instructions for the reserved area'}.</strong></p>{else}{/if}
			</fieldset>
		</form>
	</div>
	<div class="block" style="margin-left:20px">
		<form action="{$link->getPageLink('authentication.php', true)}{if $smarty.get.back}?back={$smarty.get.back}{/if}{if $smarty.get.cliente == 'rivenditore'}?cliente=rivenditore{else}{/if}" method="post" class="std">
		
			<fieldset>
				<h3>{l s='Already registered ?'}</h3>
				<table class='authentication'>
				<tr>
				<td style='text-align:right; padding-right:10px'>{l s='E-mail address'}</td>
					<td><input type="text" id="email" name="email" style="width:250px" value="{if isset($smarty.post.email)}{$smarty.post.email|escape:'htmlall':'UTF-8'|stripslashes}{/if}" class="account_input" /></td></tr>
				<tr>
				<td style='text-align:right; padding-right:10px'>{l s='Password'}</td>
					<td><input type="password" id="passwd" name="passwd" style="width:250px; background: transparent url('themes/ezdirect/img/icon/login_password.gif') 0px 50%; background-repeat: no-repeat" value="{if isset($smarty.post.passwd)}{$smarty.post.passwd|escape:'htmlall':'UTF-8'|stripslashes}{/if}" class="account_input" /></td>
				</tr>
				<tr>
				<td></td>
				<td>
				<br />
					{if isset($back)}<input type="hidden" class="hidden" name="back" value="{$back|escape:'htmlall':'UTF-8'}" />{/if}
					<input type="submit" id="SubmitLogin" name="SubmitLogin" class="button_large" value="{l s='Log in'}" />
				</td></tr>
				</table>
				<p class="lost_password"><a href="{$link->getPageLink('password.php')}">{l s='Forgot your password?'}</a></p>
			</fieldset>
		</form>
	</div>
		{if $back == 'order.php'}
</div>
{/if}
	{if isset($inOrderProcess) && $inOrderProcess && $PS_GUEST_CHECKOUT_ENABLED}
	{/if}
{else}
<form action="{$link->getPageLink('authentication.php', true)}{if $smarty.get.cliente == 'rivenditore'}?cliente=rivenditore{else}{/if}" method="post" id="account-creation_form" class="std">
	{$HOOK_CREATE_ACCOUNT_TOP}
	<fieldset class="account_creation">
	<h3>{l s='Your profile'}</h3>
	<p class="required text" style="text-align:center">
	{if $smarty.get.cliente == 'rivenditore'}
<input type="hidden" name="is_company" value="1" id="company">

{else}
	
	
	<table cellspacing="30" style='text-align:center; width:430px; margin:0 auto; vertical-align:top; border:0px'>
	<tr style="text-align:center; ">
		<td style='width:200px; border:1px solid #272974; text-align:center;  vertical-align:top; font-size:18px;'>
{l s='Company'}<br />


<input type="radio" name="is_company" value="1" id="company"
{if isset($smarty.post.is_company1) && ($smarty.post.is_company1=='1')}
checked="checked" 
{else}

{/if}
{if $smarty.get.cliente == 'rivenditore'}
checked="checked"{else}
{/if}
 onclick="profileChange(1);profileChange2(1)"><br />
<span style="font-size:12px">{l s='Firms, professional users, public administrations, VAT number owners...'}</span>
</td>

{if $smarty.get.cliente == 'rivenditore'}{else}
	<td style='width:200px; border:1px solid #da6903; text-align:center; vertical-align:top;  font-size:18px;'>
	{l s='End user'}<br />
<input type='radio' name='is_company' value='0' id='private'
{if isset($smarty.post.is_company1) && ($smarty.post.is_company1=='0')}
checked='checked' 
{else}
{/if}
onclick='profileChange(0);profileChange2(0)'><br />
<span style='font-size:12px'>{l s='End users with a tax code.'}</span><br />
	</td>{/if}

</tr>
</table>
{/if}
</p>

{if $smarty.get.cliente == 'rivenditore'}<br /><br />
<p style='font-size:12px; margin-top:-40px; margin-left:20px; margin-right:20px; width:80%; text-align:left'>
Area riservata  Partner - Rivenditori<br /><br />
<strong>Per poter accedere al listino riservato ai partners e rivenditori:</strong>
<br /><br />
1. Compila il modulo con i dati anagrafici richiesti per la registrazione come utente.<br /><br />
2. Invia una visura camerale utilizzando l'apposito modulo che trovi <a href='{$link->getPageLink('contact-form.php', true)}?step=rivenditori' target='_blank'>cliccando su questo link</a>. Allega la tua visura camerale selezionando il file nel campo "Allega file".<br /><br />
3. Riceverai un messaggio di posta elettronica per abilitarti con profilo rivenditore (salvo verifica della visura, che dovr&agrave; espressamente indicare le categorie merceologiche e /o l'attivit&agrave; sociale compatibile con l'attivit&agrave; di impiantistica, installazione, rivendita di sistemi, software, e prodotti informatici, TLC, forniture per ufficio etc).<br /><br />

Grazie per la tua cortese collaborazione.
<br /><br />
Staff Ezdirect.it</p>{else}{/if}


	</fieldset>
<div id="company_data2" name="company_data2" style="
{if isset($smarty.post.is_company1) && ($smarty.post.is_company1 == 0)}
{'display:inline'}
{else if isset($smarty.post.is_company1) && ($smarty.post.is_company1 == '1')}
{'display:none'}
{else if !isset($smarty.post.is_company1)}
{'display:none'}
{else}
{'display:none'}
{/if}




">
	<form action="{$link->getPageLink('authentication.php', true)}{if $smarty.get.cliente == 'rivenditore'}?cliente=rivenditore{else}{/if}" method="post" id="account-creation_form" class="std">
	<fieldset class="account_creation">
		<h3>{l s='Your personal information'}</h3>

		<p class="required text">
			<label for="customer_firstname">{l s='First name'}</label>
			<input onkeyup="$('#firstname').val(this.value);" type="text" class="text" id="customer_firstname" name="customer_firstname" value="{if isset($smarty.post.customer_firstname)}{$smarty.post.customer_firstname}{/if}" />
			<sup>*</sup>
		</p>
		<p class="required text">
			<label for="customer_lastname">{l s='Last name'}</label>
			<input onkeyup="$('#lastname').val(this.value);" type="text" class="text" id="customer_lastname" name="customer_lastname" value="{if isset($smarty.post.customer_lastname)}{$smarty.post.customer_lastname}{/if}" />
			<sup>*</sup>
		</p>
		<p class="required text">
			<label for="email">{l s='E-mail'}</label>
			<input type="text" class="text" id="email" name="email" value="{if isset($smarty.post.email)}{$smarty.post.email}{/if}" />
			<sup>*</sup>
		</p>
		
		<p class="required password">
			<label for="passwd">{l s='Password'}</label>
			<input type="password" class="text" name="passwd" id="passwd" />
			<sup>*</sup>
			<span class="form_info">{l s='(5 characters min.)'}</span>
		</p>
		<p class="required password">
<label for="passwd_confirm">{l s='Confirm password'}</label>
<input type="password" class="text" name="passwd_confirm" id="passwd_confirm" />
<sup>*</sup>
</p>
<script type="text/javascript">
$(function(){ldelim}
$("#passwd_confirm").parents("form").submit(function(){ldelim}
if ($('#passwd').attr('value') == $('#passwd_confirm').attr('value'))
return true;
alert("{l s='Yours both passwords filled in do not match.'}");
return false;
{rdelim});
{rdelim});
</script> 
<p class="required text">
						<label for="tax_code">{l s='Tax code'}</label>
						<input type="text" class="no_spaces" name="tax_code" value="{if isset($smarty.post.tax_code)}{$smarty.post.tax_code}{/if}" />
						
						 ({l s='Insert a 16 characters tax code'})
					</p>
				<p class="text">
						<label for="codice_univoco">{l s='SDI'}</label>
						<input type="text" class="text" id="codice_univoco" name="codice_univoco" value="{if isset($smarty.post.codice_univoco)}{$smarty.post.codice_univoco}{/if}" />
					</p>
					<p class="text">
						<label for="pec">{l s='PEC'}</label>
						<input type="text" class="text" id="pec" name="pec" value="{if isset($smarty.post.pec)}{$smarty.post.pec}{/if}" />
					</p>	
		<p class="select">
			<span>{l s='Date of Birth'}</span>
			<select id="days" name="days">
				<option value="">-</option>
				{foreach from=$days item=day}
					<option value="{$day|escape:'htmlall':'UTF-8'}" {if ($sl_day == $day)} selected="selected"{/if}>{$day|escape:'htmlall':'UTF-8'}&nbsp;&nbsp;</option>
				{/foreach}
			</select>
			{*
				{l s='January'}
				{l s='February'}
				{l s='March'}
				{l s='April'}
				{l s='May'}
				{l s='June'}
				{l s='July'}
				{l s='August'}
				{l s='September'}
				{l s='October'}
				{l s='November'}
				{l s='December'}
			*}
			<select id="months" name="months">
				<option value="">-</option>
				{foreach from=$months key=k item=month}
					<option value="{$k|escape:'htmlall':'UTF-8'}" {if ($sl_month == $k)} selected="selected"{/if}>{l s="$month"}&nbsp;</option>
				{/foreach}
			</select>
			<select id="years" name="years">
				<option value="">-</option>
				{foreach from=$years item=year}
					<option value="{$year|escape:'htmlall':'UTF-8'}" {if ($sl_year == $year)} selected="selected"{/if}>{$year|escape:'htmlall':'UTF-8'}&nbsp;&nbsp;</option>
				{/foreach}
			</select>
		</p>
		
		
	</fieldset>
	<fieldset class="account_creation">
		<h3>{l s='Your address'}</h3>
			
				
					<input type="hidden" class="text" id="firstname" name="firstname" value="{if isset($smarty.post.firstname)}{$smarty.post.firstname}{/if}" />
			
					<input type="hidden" class="text" id="lastname" name="lastname" value="{if isset($smarty.post.lastname)}{$smarty.post.lastname}{/if}" />
				
				<p class="required select">
					<label for="id_country">{l s='Country'}</label>
					<select name="id_country" id="id_country" onchange="countryChange()";>
						<option value="">-</option>
						{foreach from=$countries item=v}
						<option value="{$v.id_country}" {if ($sl_country == $v.id_country)} selected="selected"{/if}>{$v.name|escape:'htmlall':'UTF-8'}</option>
						{/foreach}
					</select>
					<sup>*</sup>
				</p>
			<script type="text/javascript">
				countryChange();
			</script>
			<div class="ee_address">
				<p class="required postcode text">
					<label for="postcode">{l s='Zip / Postal Code'}</label>
					<input type="text" class="text" name="postcode" id="postcode" value="{if isset($smarty.post.postcode)}{$smarty.post.postcode}{/if}" onkeyup="$('#postcode').val($('#postcode').val().toUpperCase());" />
					<sup>*</sup>
				</p>
				<p class="required text">
					<label for="city">{l s='City'}</label>
					<input type="text" class="text" name="city" id="city" value="{if isset($smarty.post.city)}{$smarty.post.city}{/if}" />
					<sup>*</sup>
				</p>
				<!--
					if customer hasn't update his layout address, country has to be verified
					but it's deprecated
				-->
				{assign var='stateExist' value=true}
				<p class="required id_state select">
					<label for="id_state">{l s='State'}</label>
					<select name="id_state" id="id_state">
						<option value="">-</option>
					</select>
					<sup>*</sup>
				</p>
			</div>
			<div class="it_address">
				<p class="required postcode text">
					<label for="cap">{l s='Zip / Postal Code'}</label>
					<input type="text" class="text" name="postcode" id="cap" value="{if isset($smarty.post.postcode)}{$smarty.post.postcode}{/if}" onblur="$('#cap').val($('#cap').val().toUpperCase());" onkeyup="cercaCitta();">
					<sup>*</sup>
				</p>
			
				<p class="required text">
					<label for="citta">{l s='City'}</label>
					<select name='city' id='citta' onchangeonclick='cercaProvincia();'>
						<option name=''>-- Prima selezionare CAP --</option>
						{if isset($smarty.post.city)}
							<option name='{$smarty.post.city}' selected='selected'>{$smarty.post.city}</option>
						{else}
							{if isset($address->city)}
								<option name='{$address->city|escape:'htmlall':'UTF-8'}' selected='selected'>{$address->city|escape:'htmlall':'UTF-8'}</option>
							{/if}
						{/if}
						
					</select><sup>*</sup>
				</p>
					<!--
						if customer hasn't update his layout address, country has to be verified
						but it's deprecated
					-->
				{assign var='stateExist' value=true}

				<p class="required id_state select">
					<label for="provincia">{l s='State'}</label>
					<input id="provincia" name="provincia_mark" readonly="readonly" type="text" value="{if isset($smarty.post.provincia_mark)}{$smarty.post.provincia_mark}{else}{$this_state}{/if}" />
					<input id="provincia_hidden" name="id_state" type="hidden" value="{if isset($smarty.post.id_state)}{$smarty.post.id_state}{else}{if isset($address->id_state)}{$address->id_state|escape:'htmlall':'UTF-8'}{/if}{/if}" />
					<sup>*</sup>
	
				</p>
				
			</div>
		{if $stateExist eq false}
			<p class="required id_state select">
				<label for="id_state">{l s='State'}</label>
				<select name="id_state" id="id_state">
					<option value="">-</option>
				</select>
				<sup>*</sup>
			</p>
		{/if}
		<p class="required text">
		<script type="text/javascript">
		function suggerisciIndirizzo(indirizzo, cap, citta, campo) {
			$.ajax({
				  url:"cap.php?suggerisciIndirizzo=y",
				  type: "POST",
				  data: { 
				  indirizzo: indirizzo,
				  cap: cap,
				  citta: citta,
				  campo: campo
				  },
				  success:function(r){
					console.log(r);
					$("#address1_consigli").html(r)
				  },
				  error: function(xhr,stato,errori){
					
				  }
			});
		
		}
		</script>
			<label for="address1">{l s='Address'}</label>
			<input type="text" class="text" name="address1" id="address1" value="{if isset($smarty.post.address1)}{$smarty.post.address1}{/if}"  />
			<sup>*</sup>
			
			<div id="address1_consigli">
			
			</div>
		</p>
		<p style="margin-left:50px;">{l s='You must register at least one phone number'} <sup style="color:red;">*</sup></p>
		<p class="text">
			<label for="phone">{l s='Home phone'}</label>
			<input type="text" class="text" onkeyup="this.value = this.value.replace(/\s/g,'');" name="phone" id="phone" value="{if isset($smarty.post.phone)}{$smarty.post.phone}{/if}" />
		</p>
		<p class="text">
			<label for="phone_mobile">{l s='Mobile phone'}</label>
			<input type="text" class="text" onkeyup="this.value = this.value.replace(/\s/g,'');" name="phone_mobile" id="phone_mobile" value="{if isset($smarty.post.phone_mobile)}{$smarty.post.phone_mobile}{/if}" />
		</p>
			<p class="text">
			<label for="fax">{l s='Fax number'}</label>
			<input type="text" class="text" name="fax" id="fax" value="{if isset($smarty.post.fax)}{$smarty.post.fax}{/if}" />
		</p>
		<p class="required text" id="address_alias">
			<label for="alias">{l s='Assign an address title for future reference'} </label>
			<input type='hidden' id='fatturazione' name='fatturazione' value='1' />
			<input type="text" class="text" name="alias" id="alias" value="{if isset($smarty.post.alias)}{$smarty.post.alias}{else}{l s='My address'}{/if}" />
			<sup>*</sup>
		</p>
		
		<br /><br />
		<p class="required text" >
			
			<label for="newsletter">{l s='Sign up for our newsletter'}</label>
			<input type="checkbox" name="newsletter" id="newsletter" value="1"  />
		</p>
		<p style="text-align:center; display:block; width:80%">
		Ho letto e compreso la Vostra <a href='https://www.ezdirect.it/guide/6-informativa-sulla-privacy' target='_blank'>Privacy Policy</a> e:
		<br /><br />
			
			<input type="radio" name="consenso_1" id="consenso11" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
			<input type="radio" name="consenso_1" id="consenso10" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
			
			<br /><br />
			al trattamento dei dati personali per l’esecuzione dei servizi forniti tramite il Sito da parte di Ezdirect S.r.l., o ad una o più obbligazioni contrattualmente convenute, ai sensi dell’art. 1, comma 1, lettera a), b) e c) della citata informativa (consenso obbligatorio; la mancata prestazione comporterà l’impossibilità di fruire dei servizi offerti, di effettuare l’iscrizione, e di effettuare acquisti tramite il Sito).
			<br /><br />
			
			<input type="radio" name="consenso_2"  id="consenso21" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
			<input type="radio" name="consenso_2"  id="consenso20" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
			
			<br /><br />
			al trattamento dei dati personali per la realizzazione, da parte di Ezdirect S.r.l., di indagini dirette a verificare il grado di soddisfazione sui servizi offerti, ai sensi dell’art. 1, comma 2, lettera a) della citata informativa (consenso facoltativo).

			<br /><br />
			
			<input type="radio" name="consenso_3"  id="consenso31" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
			<input type="radio" name="consenso_3"  id="consenso30" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
			
			<br /><br />
			al trattamento dei dati personali a fini di marketing e/o comunicazione commerciale da parte di Ezdirect S.r.l., connesse alle attività svolte da parte della stessa e/o da parte di soggetti terzi, ai sensi dell’art. 1, comma 2, lettera b), della citata informativa (consenso facoltativo).
			<br /><br />
			

		</p>
		
				<input type='hidden' id='is_company1' name='is_company1' value='0' />
	
	
{$HOOK_CREATE_ACCOUNT_FORM}
	<p>
		<input type="hidden" name="email_create" value="1" />
		<input type="hidden" name="is_new_customer" value="1" />
		{if isset($back)}
			{if $back == 'order.php'}
				<input type="hidden" class="hidden" name="back" value="thank-you.php?ordine=si" />
			{else}
				<input type="hidden" class="hidden" name="back" value="thank-you.php" />
			{/if}
		{/if}
		
		<!-- <script src="https://www.google.com/recaptcha/api.js" async defer></script> -->
		
		<input type="submit" name="submitAccount" id="submitAccount" value="{l s='Register'}" class="button_large" />
		<span><sup>*</sup>{l s='Required field'}</span>
							
		<div class="text-xs-center">
			<!-- <div class="g-recaptcha" data-sitekey="6LciANsZAAAAALhFLBBxrvJJICdKqsPVxXHfPh2i"></div> -->
		</div>
		<br /><br />
		
		
	</p>
	</fieldset>
</form>

</div>

<!-- COMPANY ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->




<div id="company_data" name="company_data" style="
{if isset($smarty.post.is_company1) && ($smarty.post.is_company1 == 0)}
{'display:none'}
{else if isset($smarty.post.is_company1) && ($smarty.post.is_company1 == '1')}
{'display:inline'}
{else if !isset($smarty.post.is_company1)}
{'display:none'}
{else}
{'display:none'}
{/if}
{if $smarty.get.cliente == 'rivenditore'}display:inline{else}{/if}

">
<form action="{$link->getPageLink('authentication.php', true)}{if $smarty.get.cliente == 'rivenditore'}?cliente=rivenditore{else}{/if}" method="post" id="account-creation_form_company" class="std">


	<fieldset class="account_creation">
		<h3>{l s='Company\'s data'}</h3>
		{if $smarty.get.cliente == 'rivenditore'}<input type='hidden' value='1' name='rivenditore' id='rivenditore' />
		<input type="hidden" class="hidden" name="back" value="thank-you.php?cliente=rivenditore" />
		{else}{/if}

		<p class="required text">
			<label for="customer_firstname2">{l s='Representative\'s first name'}</label>
			<input onkeyup="$('#firstname2').val(this.value);" type="text" class="text" id="customer_firstname2" name="customer_firstname" value="{if isset($smarty.post.customer_firstname)}{$smarty.post.customer_firstname}{/if}" />
			<input type='hidden' id='is_company1' name='is_company1' value='1' />
			<sup>*</sup>
		</p>
		<p class="required text">
			<label for="customer_lastname2">{l s='Representative\'s last name'}</label>
			<input onkeyup="$('#lastname2').val(this.value);" type="text" class="text" id="customer_lastname2" name="customer_lastname" value="{if isset($smarty.post.customer_lastname)}{$smarty.post.customer_lastname}{/if}" />
			<sup>*</sup>
		</p>
		<p class="required text">
			<label for="ruolo2">{l s='Role'}</label>
			<input onkeyup="$('#ruolo2').val(this.value);" type="text" class="text" id="ruolo2" name="ruolo" value="{if isset($smarty.post.ruolo)}{$smarty.post.ruolo}{/if}" />
			
		</p>
		
				<p class="required text">
			<label for="customer_company">{l s='Company'}</label>
			<input type="text" class="text" id="company" name="company" value="{if isset($smarty.post.company)}{$smarty.post.company}{/if}" />
			<sup>*</sup>
		</p>
<p class="required text">
						<label for="vat_number">{l s='VAT number'}</label>
						<input type="text" class="no_spaces" name="vat_number" value="{if isset($smarty.post.vat_number)}{$smarty.post.vat_number}{/if}" />
						
						({l s='Insert a 11 characters vat number'})
					</p>
<p class="required text">
<script type="text/javascript" src="{$js_dir}jquery.tooltip.js"></script>
<script type="text/javascript" src="{$js_dir}etc/tax_code_help.js">
</script>

						<label for="tax_code">{l s='Company tax code'}</label>
						<input type="text" class="no_spaces" id="tax_code" name="tax_code" value="{if isset($smarty.post.tax_code)}{$smarty.post.tax_code}{/if}" />
						<sup><span id="tax_code_help" style="cursor:pointer; position:absolute; right:90px" title="Se operi per conto di una societ&agrave; di capitali (esempio S.R.L o S.P.A.) il C.F. da inserire &egrave; sempre quello aziendale, quindi non devi usare il C.F. personale. Se operi per conto di una societ&agrave; di persone o ditta individuale, il C.F. necessario per la registrazione, &egrave; comuqnue quello aziendale, che, in alcuni casi, potrebbe coincidere con quello personale (es. ditte individuali). Per avere supporto in fase di registrazione, contatta il nostro staff al numero 0585821163. Grazie per la cortese collaborazione." ><img src="{$img_dir}icon/help.gif" alt="{l s='Help'}" /></span></sup>
						({l s='Insert a 11 or 16 characters tax code'})
					</p>
					<p class="required text">
						<label for="employees_number">{l s='Employees number'}</label>
						<select name="employees_number">
						<option value="Da 1 a 10">{l s='1 to 10'}</option>
						<option value="Da 11 a 50">{l s='11 to 50'}</option>
						<option value="50 piu">{l s='More than 50'}</option>
						</select>
						
						<sup>*</sup>
					</p>
		<p class="required text">
			<label for="email">{l s='E-mail'}</label>
			<input type="text" class="text" id="email2" name="email" value="{if isset($smarty.post.email)}{$smarty.post.email}{/if}" />
			<sup>*</sup>
		</p>
		
		<p class="required password">
			<label for="passwd">{l s='Password'}</label>
			<input type="password" class="text" name="passwd" id="passwd2" />
			<sup>*</sup>
			<span class="form_info">{l s='(5 characters min.)'}</span>
		</p>
		<p class="required password">
<label for="passwd_confirm">{l s='Confirm password'}</label>
<input type="password" class="text" name="passwd_confirm" id="passwd_confirm2" />
<sup>*</sup>
</p>
<script type="text/javascript">
$(function(){ldelim}
$("#passwd_confirm").parents("form").submit(function(){ldelim}
if ($('#passwd').attr('value') == $('#passwd_confirm').attr('value'))
return true;
alert("{l s='Yours both passwords filled in do not match.'}");
return false;
{rdelim});
{rdelim});
</script> 
<p class="text">
						<label for="codice_univoco">{l s='SDI'}</label>
						<input type="text" class="text" id="codice_univoco" name="codice_univoco" value="{if isset($smarty.post.codice_univoco)}{$smarty.post.codice_univoco}{/if}" />
						<sup>**</sup>
					</p>
					<p class="text">
						<label for="pec">{l s='PEC'}</label>
						<input type="text" class="text" id="pec" name="pec" value="{if isset($smarty.post.pec)}{$smarty.post.pec}{/if}" />
						<sup>**</sup>
					</p>
					
		<p class="select">
			<span>{l s='Representative\'s date of birth'}</span>
			<select id="days2" name="days">
				<option value="">-</option>
				{foreach from=$days item=day}
					<option value="{$day|escape:'htmlall':'UTF-8'}" {if ($sl_day == $day)} selected="selected"{/if}>{$day|escape:'htmlall':'UTF-8'}&nbsp;&nbsp;</option>
				{/foreach}
			</select>
			{*
				{l s='January'}
				{l s='February'}
				{l s='March'}
				{l s='April'}
				{l s='May'}
				{l s='June'}
				{l s='July'}
				{l s='August'}
				{l s='September'}
				{l s='October'}
				{l s='November'}
				{l s='December'}
			*}
			<select id="months2" name="months">
				<option value="">-</option>
				{foreach from=$months key=k item=month}
					<option value="{$k|escape:'htmlall':'UTF-8'}" {if ($sl_month == $k)} selected="selected"{/if}>{l s="$month"}&nbsp;</option>
				{/foreach}
			</select>
			<select id="years2" name="years">
				<option value="">-</option>
				{foreach from=$years item=year}
					<option value="{$year|escape:'htmlall':'UTF-8'}" {if ($sl_year == $year)} selected="selected"{/if}>{$year|escape:'htmlall':'UTF-8'}&nbsp;&nbsp;</option>
				{/foreach}
			</select>
		</p>
	
		<p class="text">
						<label for="fte_notes">**</label>
						<textarea readonly="readonly" id="fte_note" style="overflow:hidden; border:0px; background:#ffffff; width:500px; height:70px">Le aziende italiane sono tenute a inserire almeno o il codice SDI o la PEC. Non accettiamo comunicazioni del codice SDI via mail. Ti ricordiamo che in mancanza di tali dati, potrai collegarti al sito dell'Agenzia delle Entrate e scaricare la fattura.</textarea>
					</p>
		
	</fieldset>
	<fieldset class="account_creation">
		<h3>{l s='Company\'s address'}</h3>
		{foreach from=$dlv_all_fields item=field_name}
		
				{if $field_name eq "firstname"}
				
					<input type="hidden" class="text" id="firstname2" name="firstname" value="{if isset($smarty.post.firstname)}{$smarty.post.firstname}{/if}" />
				{/if}	
			{if $field_name eq "lastname"}
			
					<input type="hidden" class="text" id="lastname2" name="lastname" value="{if isset($smarty.post.lastname)}{$smarty.post.lastname}{/if}" />
			{/if}		
			{if $field_name eq "company"}
			
	
					<input type="hidden" class="text" id="company2" name="company2" value="{if isset($smarty.post.company)}{$smarty.post.company}{/if}" />
			{/if}
			
			<script type="text/javascript">
				countryChange2();
			</script>
			{if $field_name eq "Country:name" || $field_name eq "country"}
				<p class="required select">
					<label for="id_country">{l s='Country'}</label>
					<select name="id_country" id="id_country2" onchange="countryChange2()";>
						<option value="">-</option>
						{foreach from=$countries item=v}
						<option value="{$v.id_country}" {if ($sl_country == $v.id_country)} selected="selected"{/if}>{$v.name|escape:'htmlall':'UTF-8'}</option>
						{/foreach}
					</select>
					<sup>*</sup>
				</p>
			{/if}
			
			<div class="ee_address">
			{if $field_name eq "postcode"}
				<p class="required postcode text">
					<label for="postcode">{l s='Zip / Postal Code'}</label>
					<input type="text" class="text" name="postcode" id="postcode2" value="{if isset($smarty.post.postcode)}{$smarty.post.postcode}{/if}" onkeyup="$('#postcode').val($('#postcode').val().toUpperCase());" />
					<sup>*</sup>
				</p>
			{/if}	
						
			{if $field_name eq "city"}
				<p class="required text">
					<label for="city">{l s='City'}</label>
					<input type="text" class="text" name="city" id="city2" value="{if isset($smarty.post.city)}{$smarty.post.city}{/if}" />
					<sup>*</sup>
				</p>
				<!--
					if customer hasn't update his layout address, country has to be verified
					but it's deprecated
				-->
			{/if}
			
			{if $field_name eq "State:name" || $field_name eq 'state'}
				{assign var='stateExist' value=true}
				<p class="required id_state select">
					<label for="id_state">{l s='State'}</label>
					<select name="id_state" id="id_state2">
						<option value="">-</option>
					</select>
					<sup>*</sup>
				</p>
			{/if}
			</div>
			
			<div class="it_address">
				{if $field_name eq "postcode"}
				<p class="required postcode text">
					<label for="cap2">{l s='Zip / Postal Code'}</label>
					<input type="text" class="text" name="postcode" id="cap2" value="{if isset($smarty.post.postcode)}{$smarty.post.postcode}{/if}" onblur="$('#cap2').val($('#cap2').val().toUpperCase());" onkeyup="cercaCitta2();">
					<sup>*</sup>
				</p>
				{/if}
				
				{if $field_name eq "city"}
				<p class="required text">
					<label for="citta2">{l s='City'}</label>
					<select name='city' id='citta2' onclick='cercaProvincia2();'>
						<option name=''>-- Prima selezionare CAP --</option>
						{if isset($smarty.post.city)}
							<option name='{$smarty.post.city}' selected='selected'>{$smarty.post.city}</option>
						{else}
							{if isset($address->city)}
								<option name='{$address->city|escape:'htmlall':'UTF-8'}' selected='selected'>{$address->city|escape:'htmlall':'UTF-8'}</option>
							{/if}
						{/if}
						
					</select><sup>*</sup>
				</p>
					<!--
						if customer hasn't update his layout address, country has to be verified
						but it's deprecated
					-->
				{/if}
				{if $field_name eq "State:name" || $field_name eq 'state'}
				{assign var='stateExist' value=true}

				<p class="required id_state select">
					<label for="provincia2">{l s='State'}</label>
					<input id="provincia2" name="provincia_mark" readonly="readonly" type="text" value="{if isset($smarty.post.provincia_mark)}{$smarty.post.provincia_mark}{else}{$this_state}{/if}" />
					<input id="provincia_hidden2" name="id_state" type="hidden" value="{if isset($smarty.post.id_state)}{$smarty.post.id_state}{else}{if isset($address->id_state)}{$address->id_state|escape:'htmlall':'UTF-8'}{/if}{/if}" />
					<sup>*</sup>
	
				</p>
				{/if}
			</div>
		{/foreach}
		{if $stateExist eq false}
			<p class="required id_state select">
				<label for="id_state">{l s='State'}</label>
				<select name="id_state" id="id_state2">
					<option value="">-</option>
				</select>
				<sup>*</sup>
			</p>
		{/if}
		<p class="required text">
		
		<script type="text/javascript">
		function suggerisciIndirizzo2(indirizzo, cap, citta, campo) {
			$.ajax({
				  url:"cap.php?suggerisciIndirizzo=y",
				  type: "POST",
				  data: { 
				  indirizzo: indirizzo,
				  cap: cap,
				  citta: citta,
				  campo: campo
				  },
				  success:function(r){
					console.log(r);
					$("#address12_consigli").html(r)
				  },
				  error: function(xhr,stato,errori){
					
				  }
			});
		
		}
		</script>
		
			<label for="address1">{l s='Address'}</label>
			<input type="text" class="text" name="address1" id="address12" value="{if isset($smarty.post.address1)}{$smarty.post.address1}{/if}" />
			<sup>*</sup>
			<!-- <span class="inline-infos">{l s='Street address, P.O. box, company name, c/o'}</span> -->
		</p>
		
			<sup>*</sup>
			
			<div id="address12_consigli">
			
			</div>
	
		<p style="margin-left:50px;">{l s='You must register at least one phone number'} <sup style="color:red;">*</sup></p>
		<p class="text">
			<label for="phone">{l s='Home phone'}</label>
			<input type="text" class="text" name="phone" onkeyup="this.value = this.value.replace(/\s/g,'');" id="phone2" value="{if isset($smarty.post.phone)}{$smarty.post.phone}{/if}" />
		</p>
		<p class="text">
			<label for="phone_mobile">{l s='Mobile phone'}</label>
			<input type="text" class="text" name="phone_mobile" id="phone_mobile2" onkeyup="this.value = this.value.replace(/\s/g,'');" value="{if isset($smarty.post.phone_mobile)}{$smarty.post.phone_mobile}{/if}" />
		</p>
			<p class="text">
			<label for="fax">{l s='Fax number'}</label>
			<input type="text" class="text" name="fax" id="fax2" value="{if isset($smarty.post.fax)}{$smarty.post.fax}{/if}" />
		</p>
		<p class="required text" id="address_alias">
			<label for="alias">{l s='Assign an address title for future reference'} </label>
			<input type='hidden' id='fatturazione' name='fatturazione' value='1' />
			<input type="text" class="text" name="alias" id="alias2" value="{if isset($smarty.post.alias)}{$smarty.post.alias}{else}{l s='My address'}{/if}" />
			<sup>*</sup>
		</p>
			
		<br /><br />
		<p class="required text" >
			
			<label for="newsletter">{l s='Sign up for our newsletter'}</label>
			<input type="checkbox" name="newsletter" id="newsletter2" value="1"  />
		</p>
	
		<p style="text-align:center; display:block; width:80%">
		Ho letto e compreso la Vostra <a href='https://www.ezdirect.it/guide/6-informativa-sulla-privacy' target='_blank'>Privacy Policy</a> e:
		<br /><br />
			
			<input type="radio" name="consenso_1" id="consenso121" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
			<input type="radio" name="consenso_1" id="consenso120" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
			
			<br /><br />
			al trattamento dei dati personali per l’esecuzione dei servizi forniti tramite il Sito da parte di Ezdirect S.r.l., o ad una o più obbligazioni contrattualmente convenute, ai sensi dell’art. 1, comma 1, lettera a), b) e c) della citata informativa (consenso obbligatorio; la mancata prestazione comporterà l’impossibilità di fruire dei servizi offerti, di effettuare l’iscrizione, e di effettuare acquisti tramite il Sito).
			<br /><br />
			
			<input type="radio" name="consenso_2" id="consenso221" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
			<input type="radio" name="consenso_2" id="consenso220" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
			
			<br /><br />
			al trattamento dei dati personali per la realizzazione, da parte di Ezdirect S.r.l., di indagini dirette a verificare il grado di soddisfazione sui servizi offerti, ai sensi dell’art. 1, comma 2, lettera a) della citata informativa (consenso facoltativo).

			<br /><br />
			
			<input type="radio" name="consenso_3" id="consenso321" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
			<input type="radio" name="consenso_3" id="consenso320" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
			
			<br /><br />
			al trattamento dei dati personali a fini di marketing e/o comunicazione commerciale da parte di Ezdirect S.r.l., connesse alle attività svolte da parte della stessa e/o da parte di soggetti terzi, ai sensi dell’art. 1, comma 2, lettera b), della citata informativa (consenso facoltativo).
			<br /><br />
			

		</p>
		
{$HOOK_CREATE_ACCOUNT_FORM}
	<p>
		<input type="hidden" name="email_create" value="1" />
		<input type="hidden" name="is_new_customer" value="1" />
		{if isset($back)}
		
			{if $back == 'order.php'}
				<input type="hidden" class="hidden" name="back" value="thank-you.php?ordine=si" />
			{else}
				<input type="hidden" class="hidden" name="back" value="thank-you.php" />
			{/if}
		
		{/if}
		
		
		<!-- <script src="https://www.google.com/recaptcha/api.js" async defer></script> -->
		
		<input type="submit" name="submitAccount" id="submitAccount2" value="{l s='Register'}" class="button_large" />
		<span><sup>*</sup>{l s='Required field'}</span>					
		
		<div class="text-xs-center">
			<!-- <div class="g-recaptcha" data-sitekey="6LciANsZAAAAALhFLBBxrvJJICdKqsPVxXHfPh2i"></div> -->
		</div>
		<br /><br />
		
	</p>
	</fieldset>
</form>
</div>
{/if}

