{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='Contact'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<script type='text/javascript'>
{assign var="type" value=""}
$(document).ready(function () {
{if $smarty.get.step == 'contabilita'}
{$type = 2}
$('option[value="2"]').attr('selected', 'selected').parent().focus();
{elseif $smarty.get.step == 'tecnica'}
{$type = 4}
$('option[value="4"]').attr('selected', 'selected').parent().focus();
{elseif $smarty.get.step == 'rivenditori'}
{$type = 3}
$('option[value="3"]').attr('selected', 'selected').parent().focus();
{elseif $smarty.get.step == 'quotazione'}
{$type = 5}
$('option[value="5"]').attr('selected', 'selected').parent().focus();
{elseif $smarty.get.step == 'callmeback'}
{$type = 6}
$('option[value="6"]').attr('selected', 'selected').parent().focus();
{elseif $smarty.get.step == 'assistenza-ordini'}
{$type = 8}
$('option[value="8"]').attr('selected', 'selected').parent().focus();
{elseif $smarty.get.step == 'messaggi'}
{$type = 8}
$('option[value="7"]').attr('selected', 'selected').parent().focus();
{elseif $smarty.get.step == 'rma'}
{$type = 9}
$('option[value="9"]').attr('selected', 'selected').parent().focus();
{elseif $smarty.get.step == ''}
{$type = 0}
$('option[value="0"]').attr('selected', 'selected').parent().focus();
{/if}
$('#id_contact').trigger('change');
});

	function getOrderId() {
		var productId = document.getElementById('tuttiprodotti').value;
		$.ajax({
			   type: 'GET',
			   url: baseDir + 'contact-form.php',
			   async: true,
			   cache: false,
			   data: '&getOrderId=true&id_product='+productId ,
			   success: function(resp) {
				$('#id_order').val(resp);
			   },
			   error: function(XMLHttpRequest, textStatus, errorThrown)
				{
					alert('ERROR' + textStatus + ' ' + errorThrown);					
				}
		});
 



	}



</script>

<h1>{l s='Customer Service'}</h1>

{if !$isLogged}

{l s='Technical assistance module is only for registered users'}


<div id="authentication_form">
	<div class="block">
		<form action="{$link->getPageLink('authentication.php', true)}{if $smarty.get.cliente == 'rivenditore'}?cliente=rivenditore{else}{/if}" method="post" class="std">
			<fieldset>
			
				<h3>{l s='Create your account'}</h3>
				<p style="padding:10px"><strong>{if $smarty.get.cliente == 'rivenditore'}{l s='Partners and resellers area'}.{else}{l s='Enter your e-mail address to create an account'}. {/if}</strong></p>
				<p style="padding:10px">
			<span><input type="text" id="email_create" name="email_create" style="margin-top:-10px" value="{if isset($smarty.post.email_create)}{$smarty.post.email_create|escape:'htmlall':'UTF-8'|stripslashes}{/if}" class="account_input" /></span>
				
				{if isset($back)}<input type="hidden" class="hidden" name="back" value="{$back|escape:'htmlall':'UTF-8'}" />{/if}
				<div style="clear:both"></div>
					<input type="submit" id="SubmitCreate" name="SubmitCreate" class="button_large" style="margin-left:12px; margin-top:16px" value="{l s='Create your account'}" />
					<input type="hidden" class="hidden" name="SubmitCreate" value="{l s='Create your account'}" />
				</p>
				{if $smarty.get.cliente == 'rivenditore'}
				<p style="padding:10px"><strong>
				{l s='In the next step you will receive the instructions for the reserved area'}.</strong></p>{else}{/if}
			</fieldset>
		</form>
	</div>
	<div class="block" style="margin-left:20px">
		<form action="{$link->getPageLink('authentication.php', true)}{if $smarty.get.cliente == 'rivenditore'}?cliente=rivenditore{else}{/if}" method="post" class="std">
			<fieldset>
				<h3>{l s='Already registered ?'}</h3>
				<table class='authentication'>
				<tr>
				<td style='text-align:right; padding-right:10px'>{l s='E-mail address'}</td>
					<td><input type="text" id="email" name="email" style="width:250px" value="{if isset($smarty.post.email)}{$smarty.post.email|escape:'htmlall':'UTF-8'|stripslashes}{/if}" class="account_input" /></td></tr>
				<tr>
				<td style='text-align:right; padding-right:10px'>{l s='Password'}</td>
					<td><input type="password" id="passwd" name="passwd" style="width:250px; background: transparent url('themes/ezdirect/img/icon/login_password.gif') 0px 50% no-repeat" value="{if isset($smarty.post.passwd)}{$smarty.post.passwd|escape:'htmlall':'UTF-8'|stripslashes}{/if}" class="account_input" /></td>
				</tr>
				<tr>
				<td></td>
				<td>
				<br />
					{if isset($back)}<input type="hidden" class="hidden" name="back" value="{$back|escape:'htmlall':'UTF-8'}" />
			{else}<input type="hidden" class="hidden" name="back" value="{if $smarty.get.id_customer_thread > 0}contact-form.php?id_customer_thread={$smarty.get.id_customer_thread}&token={$smarty.get.token} {else}contact-form.php?step={$smarty.get.step}{/if}" />
			{/if}
					<input type="submit" id="SubmitLogin" name="SubmitLogin" class="button_large" value="{l s='Log in'}" />
				</td></tr>
				</table>
				<p class="lost_password"><a href="{$link->getPageLink('password.php')}">{l s='Forgot your password?'}</a></p>
			</fieldset>
		</form>
	</div>
</div>
{else}


	{if isset($confirmation)}
		<p>{l s='Your message has been successfully sent to our team.'}</p>
		
		{if isset($rma_template) && !isset($smarty.get.id_customer_thread)}
			<div align="center">
				<table border="1" cellpadding="0" style="border-collapse: collapse" width="595" id="table1" bordercolor="#C0C0C0">
				<tr>
				  <td>
				  <table border="0" cellpadding="4" style="border-collapse: collapse" width="100%" id="table2">
					<tr>
					  <td width="170">
					  <img border="0" src="https://www.ezdirect.it/rma/logo_ez.jpg" width="170" height="57"></td>
					  <td align="left" valign="top"><strong><font face="Verdana" size="2"><u>NON 
					  SI ACCETTA MERCE SENZA NUMERO RMA<br />
					  </u></font><font face="Verdana"></font></strong><br />
						<script language="Javascript1.2">
						<!--
						var message = "Stampa RMA";
						function printpage() {
						window.print();  
						}
						document.write("<form><input type=button "
						+"value=\""+message+"\" onClick=\"printpage()\"></form>");

						//-->
						</script>

					</td>
					</tr>
				  </table>
				  <table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" width="100%" id="table3" bordercolor="#C0C0C0">
					<tr>
					  <td bordercolor="#F7F7F7" bgcolor="#EBEBEB">
					  <table border="0" cellpadding="0" style="border-collapse: collapse" width="100%" id="table6">
						<tr>
						  <td width="27%" align="left" valign="top"><font face="Verdana" size="2">Ragione Sociale/Nome:</font></td>
						  <td width="73%" align="left" valign="top"><font face="Verdana" size="2"> 
					  <strong>{$company}</strong></font></td>
						</tr>
						<tr>
						  <td width="27%" align="left" valign="top">
						  <font face="Verdana" size="2">Persona di riferimento</font></td>
						  <td width="73%" align="left" valign="top"> <font face="Verdana" size="2"> 
					  <strong>{$customer_name}</strong></font></td>
						</tr>
						<tr>
						  <td width="27%" align="left" valign="top">
						  <font face="Verdana" size="2">Indirizzo</font></td>
						  <td width="73%" align="left" valign="top"> <font face="Verdana" size="2"> 
					  <strong>{$address}</strong></font></td>
						</tr>
						<tr>
						  <td width="27%" align="left" valign="top"><font face="Verdana" size="2">N.Telefono</font></td>
						  <td width="73%" align="left" valign="top"> <strong><font face="Verdana" size="2">{$phone}</font></strong></td>
						</tr>
						<tr>
						  <td width="27%" align="left" valign="top"><font face="Verdana" size="2">Fax:</font></td>
						  <td width="73%" align="left" valign="top"><font face="Verdana" size="2"> <strong>{$fax}</strong></font></td>
						</tr>
						<tr>
						  <td width="27%" align="left" valign="top"><font face="Verdana" size="2">E-Mail:</font></td>
						  <td width="73%" align="left" valign="top"><font face="Verdana" size="2"> <strong>{$email}</strong></font></td>
						</tr>
						<tr>
						  <td width="27%" align="left" valign="top"><font face="Verdana" size="2">Nome prodotto:</font></td>
						  <td width="73%" align="left" valign="top"><font face="Verdana" size="2"> <strong>{$product_name}</strong></font></td>
						</tr>
						<tr>
						  <td width="27%" align="left" valign="top"><font face="Verdana" size="2">DDT/fatt.acquisto:</font></td>
						  <td width="73%" align="left" valign="top"><font face="Verdana" size="2"> <strong>{$fattura}</strong></font></td>
						</tr>
					  </table>
					  </td>
					</tr>
					</table>
				  <p><font face="Verdana"><strong><font size="2">Tipo di RMA:</font>
				  </strong><font size="2">{$rma_type}<br />
				  <strong><br />
				  </strong>Motivazione del reso merce o descrizione problema 
				  riscontrato:<strong><br />
				 {$rma_motivation}</strong><br /></font></font>
				  <strong><font face="Verdana" size="2"><br />
				  Tipo di trasporto: {$rma_shipping}
				  </font></strong></p>
				  <table border="0" cellpadding="0" style="border-collapse: collapse" width="100%" id="table4" bgcolor="#000000">
					<tr>
					  <td><strong><font color="#FFFFFF" face="Verdana">AUTORIZZAZIONE AL RESO 
					  (Riservato Ezdirect)</font></strong></td>
					</tr>
				  </table>
				  <p><font size="4" face="Verdana"><strong>Numero RMA </font>
				  <font size="6" face="Verdana"> <br />
				  </font><font size="4" face="Verdana"><br />
				  </font>
				  </strong><font face="Verdana"><strong><font size="2">Note:___________________________________________________________</font></strong><br />
				  &nbsp;</font><table border="0" cellpadding="0" style="border-collapse: collapse" width="100%" id="table5">
					<tr>
					  <td width="33%"><font face="Verdana"><strong><font size="2">Data: 
					  _______________</font></strong></font></td>
					  <td width="67%"><strong><font face="Verdana" size="2">Operatore 
					  Ezdirect:___________________________</font></strong></td>
					  </tr>
					</table>
					<table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" width="100%" id="table7" bordercolor="#000000">
					  <tr>
						<td width="197">&nbsp;</td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td width="197"><strong><font face="Verdana" size="2">Data arrivo da cliente</font></strong></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td width="197"><strong><font face="Verdana" size="2">Data invio (reso) a cliente</font></strong></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td width="197"><strong><font face="Verdana" size="2">Verifica confezione</font></strong></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td width="197"><strong><font face="Verdana" size="2">Inviato in 
						riparazione il</font></strong></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td width="197"><strong><font face="Verdana" size="2">Inviato a</font></strong></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td width="197"><strong><font face="Verdana" size="2">DDT N</font></strong></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td width="197"><strong><font face="Verdana" size="2">Esito test</font></strong></td>
						<td>&nbsp;</td>
					  </tr>
					  <tr>
						<td width="197"><strong><font face="Verdana" size="2">Prove effettuate da</font></strong></td>
						<td>&nbsp;</td>
					  </tr>
				  </table>
				  <p align="center"><font face="Verdana"><strong><font size="2">
					INDICARE IL NUMERO RMA ALL'ESTERNO DELLA CONFEZIONE<br />
					NON UTILIZZARE LA CONFEZIONE ORIGINALE DEL PRODOTTO PER IL TRASPORTO<br />
					<br />
					Ezdirect srl <br />Via Nerino Garbuio Snc 54038 Montignoso Massa Carrara</font></strong></font></td>
				</tr>
			  </table>
			</div>

			<p align="center">&nbsp;</p>
			<div align="center">
			  <table border="0" cellpadding="0" style="border-collapse: collapse" width="595" id="table8" bordercolor="#C0C0C0">
				<tr>
				  <td>
				  <p class="MsoNormal"><strong>
				  <span style="font-size:10.0pt;font-family:Verdana">Grazie per aver 
				  richiesto il modulo RMA.</span></strong><span style="font-size:10.0pt;font-family:Verdana">&nbsp;</span></p>
				  <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Verdana">
				  Per noi &egrave; importante rispondere nel minor tempo possibile alle esigenze 
				  del cliente. <br />
				  Per consentirci di avviare al meglio la procedura di rientro e reso dei 
				  prodotti, ti chiediamo di prestare attenzione ai nostri consigli:<br />
				  <br />
				  <strong>Non utilizzare la confezione originale del prodotto per l'invio, </strong>ma 
				  inseriscila all'interno di un'ulteriore scatola - confezione (In caso di 
				  reso, se il prodotto non &egrave; perfetto in ogni sua parte, quindi anche nel 
				  confezionamento, non sar&agrave; accettato).<br />
				  <strong><br />
				  Esegui l'imballo con cura </strong>proteggendo al meglio i prodotti dalle 
				  conseguenze di eventuali urti accidentali.</span></p>
				  <p class="MsoNormal"><strong>
				  <span style="font-size:10.0pt;font-family:Verdana">Applica la parte 
				  sottostante sulla scatola </span></strong>
				  <span style="font-size:10.0pt;font-family:Verdana">e assicurati che 
				  l'etichetta non sia posticcia (che non si stacchi quindi durante il 
				  trasporto a causa di un nastro&nbsp; poco adesivo)</span></p>
				  <p class="MsoNormal"><span style="font-size:10.0pt;font-family:Verdana">
				
				  Grazie per la collaborazione<br />
				  
				  Staff Ezdirect</span></td>
				</tr>
				<tr>
				  <td>
				  <p align="center">************
				  <span style="font-size: 30.0pt; font-family: Wingdings 2">%</span> 
				  ************ </td>
				</tr>
				<tr>
				  <td><hr size="1">
				  <p><strong><font size="5" face="Verdana">Numero RMA: </font>
				  <font size="7" face="Verdana"></font></strong></p>
				  <hr size="1">
				  <p><strong><font face="Verdana" size="5">Destinatario<br />
				  </font><font face="Verdana" size="7">Ezdirect Srl<br />
				  </font><font face="Verdana" size="5">Via Nerino Garbuio Snc<br />
				  54038 Montignoso (MS)</font></strong></p>
				  <hr size="1">
				  <p><strong><font face="Verdana">Mittente: </font></strong><font face="Verdana" size="2"> 
					  <strong>{$company} - {$customer_name}<br />
				  {$address}<br />
				  Telefono: </strong></font> <strong><font face="Verdana" size="2">{$phone}</font></strong></p>
				  <p><strong><font face="Verdana" size="2">VETTORE: </font></strong></td>
				</tr>
				<tr>
				  <td>
				  <p align="center">************
				  <span style="font-size: 30.0pt; font-family: Wingdings 2">%</span>************
				  </td>
				</tr>
			  </table>
			</div>
		
		
		
		{/if}
		
		
		
		
		
		<ul class="footer_links">
			<li><a href="{$base_dir}"><img class="icon" alt="" src="{$img_dir}icon/home.gif" width="22" height="22" /></a><a href="{$base_dir}">{l s='Home'}</a></li>
		</ul>
	{elseif isset($alreadySent)}
		<p>{l s='Your message has already been sent.'}</p>
		<ul class="footer_links">
			<li><a href="{$base_dir}"><img class="icon" alt="" src="{$img_dir}icon/home.gif" width="22" height="22" /></a><a href="{$base_dir}">{l s='Home'}</a></li>
		</ul>
	{else}
	
		{if !($primoPassaggioOk) && !($smarty.get.token) && !($smarty.get.rif)}
		
			<h2 style="margin-top:16px; font-size:20px">La tua domanda riguarda un caso gi&agrave; trattato? Se s&igrave;, sceglilo da questa lista:</h2><br /><br />
		
			<table class="std">
				<thead>
					<tr>
						<th class="first_item" style="text-align:left">{l s='Numero ticket'}</th>
						<th style="text-align:left">{l s='Cosa riguarda il caso...' mod='mytickets'}</th>
						<th style="text-align:left">{l s='Data ultimo messaggio' mod='mytickets'}</th>
						<th class="last_item" style="text-align:left">{l s='Rispondi' mod='mytickets'}</th>
					</tr>
				</thead>
				<tbody>
				{foreach from=$tickets item=ticket}
				
				
				
				
				
				<tr>
					<td><a href="{$link->getPageLink('modules/mytickets/tickets.php', true)}?id_customer_thread={$ticket.id_customer_thread}{if $ticket.id_contact == 'preventivo' || $ticket.id_contact == 'tirichiamiamonoi'}&type=pv{/if}">
				
			
					{assign var='anno' value=$ticket.date_add|substr:2:2}
				
					{if $ticket.id_contact == 2}
					{assign var='sigla' value='A'}
					{else if $ticket.id_contact == 4}
					{assign var='sigla' value='T'}
					{else if $ticket.id_contact == 3}
					{assign var='sigla' value='V'}
					{else if $ticket.id_contact == 7}
					{assign var='sigla' value='M'}
					{else if $ticket.id_contact == 6}
					{assign var='sigla' value='R'}
					{else if $ticket.id_contact == 8}
					{assign var='sigla' value='D'}
					{else if $ticket.id_contact == 9}
					{assign var='sigla' value='S'}
					{else if $ticket.id_contact == 'preventivo'}
					{assign var='sigla' value='P'}
					{else if $ticket.id_contact == 'tirichiamiamonoi'}
					{assign var='sigla' value='R'}
					{/if}
					
					{assign var='id_ticket' value=$sigla|cat:$anno|cat:$ticket.id_customer_thread}
				
					{$id_ticket}
					</a>
				</td>
					
					<td>{$ticket.message|html_entity_decode}</td>
					<td>{$ticket.last_msg|date_format:"%d/%m/%Y, %H:%M:%S"}</td>
					<td><a class="button_large" style="width: 100px;

height: 20px;

display: block;

text-decoration: none;

vertical-align: middle;

padding-top: 7px;

margin-top: 3px;

margin-bottom: 5px;" href="{if $ticket.status == 'open' || $ticket.status == 'pending1'}contact-form.php?id_customer_thread={$ticket.id_customer_thread}&token={$ticket.token} {else}contact-form.php?rif=T{$ticket.id_customer_thread}{/if}&step={$smarty.get.step}">{l s='Apri ticket'}...</a></td>
					
				</tr>
				</tbody>
			{/foreach}
			</table>
		
			<form method="post" action="contact-form.php?step={$smarty.get.step}">
			<input type='hidden' name='primoPassaggioOk' value='{$primoPassaggioOk}' />
			<p style="text-align:center">
			<input type="submit" class="button_large" style="width:300px; height:40px" value="Se il caso &egrave; nuovo, clicca qui per continuare" />
			</form>
			</p>
			
		{else}
			{if $customerThread.status == 'closed' && $smarty.get.step == 'reopen'}
			
				{if $customerThread.id_contact == 4}
				{assign var=contact_step value="tecnica"}
				{else if $customerThread.id_contact == 2}
				{assign var=contact_step value="contabilita"}
				{else if $customerThread.id_contact == 3}
				{assign var=contact_step value="rivenditori"}
				{else if $customerThread.id_contact == 6}
				{assign var=contact_step value="callmeback"}
				{else if $customerThread.id_contact == 8}
				{assign var=contact_step value="assistenza-ordini"}
				{else if $customerThread.id_contact == 7}
				{assign var=contact_step value="messaggi"}
				{else if $customerThread.id_contact == 9}
				{assign var=contact_step value="rma"}
				{/if}
				
				
					{l s='You cannot answer to this ticket because it has been closed by our staff'}. 
					<br /><br />
					{l s='If you must write something more'}, <a href="contact-form.php?id_customer_thread={$smarty.get.id_customer_thread}&token={$smarty.get.token}&reopen">{l s='please click on this link in order to reopen this ticket'}</a>
		
		
			{else}
				{if isset($smarty.get.step)}
				{else}
					<p>{l s='For questions about an order or for more information about our products'}. <br />
					<strong>{l s='Marked fields (*) are required'}</strong>.</p>
				{/if}
					{include file="$tpl_dir./errors.tpl"}
					<form action="{$request_uri}" method="post" class="std" enctype="multipart/form-data">
					<input type='hidden' name='id_customer' value='{$id_customer}' />
					<input type='hidden' name='primoPassaggioOk' value='{$primoPassaggioOk}' />
						<fieldset>
							<p class="select">
								<label for="id_contact"><strong>{l s='Subject Heading'}</strong></label>
							{if isset($customerThread.id_contact)}
								{foreach from=$contacts item=contact}
									{if $contact.id_contact == $customerThread.id_contact}
										<input type="text" style="width:400px" id="contact_name" name="contact_name" value="{$contact.name|escape:'htmlall':'UTF-8'}" readonly="readonly" />
										<input type="hidden" name="id_contact" value="{$contact.id_contact}" />
									{/if}
								{/foreach}
							</p>
							{else}
								{if isset($smarty.get.step) && $smarty.get.step != ''}
								<input type="hidden" id="id_contact" name="id_contact" style="width:400px" value="{$type}" />
									{foreach from=$contacts item=contact}
										{if isset($type) && $type == $contact.id_contact}
											&nbsp;&nbsp;&nbsp;{$contact.name|escape:'htmlall':'UTF-8'}
										{/if}
									{/foreach}
								
								
								{else}
								
									<select id="id_contact" name="id_contact" style="width:400px" onchange="showElemFromSelect('id_contact', 'desc_contact')">
										<option value="0">{l s='-- Choose --'}</option>
									{foreach from=$contacts item=contact}
									
										{if $isLogged == 1}
											{if $contact.id_contact == 6 || (isset($smarty.post.id_contact) && $contact.id_contact == 9 && $smarty.post.id_contact != $contact.id_contact) || ($contact.id_contact == 9 && $smarty.get.step != 'rma')}
											{else}
												<option value="{$contact.id_contact|intval}" {if isset($smarty.post.id_contact) && $smarty.post.id_contact == $contact.id_contact}selected="selected"{/if}>{$contact.name|escape:'htmlall':'UTF-8'}</option>
											{/if}
										
										{else}
											<option value="{$contact.id_contact|intval}" {if isset($smarty.post.id_contact) && $smarty.post.id_contact == $contact.id_contact}selected="selected"{/if}>{$contact.name|escape:'htmlall':'UTF-8'}</option>
											
										{/if}
									{/foreach}
									</select>
								
								<br />
								{foreach from=$contacts item=contact}
									<p id="desc_contact{$contact.id_contact|intval}" class="desc_contact" style="display:none; margin-left:235px">
										{$contact.description}
									</p>
								{/foreach}
								{/if}
							</p>
							{if !$smarty.get.step || $smarty.get.step == 'messaggi' || $smarty.get.step == ''}
						
					{else}
					{/if}
							{/if}
							<p class="text">
								<label for="email"><strong>{l s='E-mail address'}*</strong></label>
								{if isset($customerEmail)}
									<input type="text" id="email" style="width:400px" name="from" value="{$customerEmail}" readonly="readonly" />
								{else}
									<input type="text" id="email" style="width:400px" name="from" value="{$email}" />
								{/if}
							</p>
							<p class="text">
								<label for="phone"><strong>{l s='Phone number'}*</strong></label>
								{if !empty($customerThread.phone)}
									<input type="text" style="width:400px" name="phone" value="{$customerThread.phone}" readonly="readonly" />
								{else}
									<input type="text" style="width:400px" name="phone" value="{$phone}" />
								{/if}
							</p>
							{if $contact.id_contact == 9}
								<p class="text">
								<label for="address"><strong>{l s='Please select your address'}*</strong><br /></label>
								<select name='address' style="width:400px">
								{foreach $addresses as $address}
								<option value='{$address.id_address}'>{$address.address1} - {$address.postcode} {$address.city} ({$address.iso_code})</option>
								{/foreach}
								</select>
							
							{/if}
						{if !$PS_CATALOG_MODE}
							
							<script src="{$js_dir}select2.js" type="text/javascript"></script>
					
								<script type="text/javascript">
								$(document).ready(function() { $("#tuttiprodotti").select2(); });
								
								</script>
								
							
							
							{if isset($isLogged) && $isLogged}
								<p class="text">
								<label for="id_product"><strong>{l s='Product'}{if $contact.id_contact == 9}*{/if}</strong>
								{if $smarty.get.step == 'tecnica'}<br />
								{l s='Please select the product for which you are requesting assistance'}
								{else}<br />
								{l s='Please select a product you have bought or a product in our catalog'}
								{/if}
								</label>
								{if empty($customerThread.id_product)}
									<select style="display:block; margin-left:10px; width:400px" id="tuttiprodotti" name="id_product"><option value="0">{l s='-- Choose --'}</option>{$orderedProductList}</select>
								{elseif $customerThread.id_product > 0}
									<input style="width:400px" type="text" name="id_product" id="id_product" value="{$customerThread.id_product|intval}" readonly="readonly" />
								{/if}
								</p>
								
								
								{if $contact.id_contact == 9}
								<br />
								<p class="text">
									<label for="rma_product">{l s='If the product is not in the list above, please write it in this box'}
									</label>
									<input type="text" style="width:400px" name="rma_product" value="{$rma_product}" />
									</p>
								<br />
								{/if}
								{if (!isset($customerThread.id_order) || $customerThread.id_order > 0)}
								<p class="text">
								{if $contact.id_contact == 9}
									<label for="id_order"><strong>{l s='Invoice ID'}*</strong><br />
									{l s='Please specify the ID of your invoice'}
								{else}
									<label for="id_order"><strong>{l s='Order ID'}{if $contact.id_contact == 9}*{/if}</strong><br />
									{l s='Please specify the ID of your order'}
								{/if}
									</label>
									{if !isset($customerThread.id_order) && isset($isLogged) && $isLogged == 1}
										
										<select name="id_order" style="width:400px" id="id_order" ><option value="0">{l s='-- Choose --'}</option>
										{if $contact.id_contact == 9}
											{$invoiceList}
										{else}
											{$orderList}
										{/if}
										</select>
									{elseif !isset($customerThread.id_order) && !isset($isLogged)}
										<input type="text" name="id_order" style="width:400px" id="id_order" value="{if isset($customerThread.id_order) && $customerThread.id_order > 0}{$customerThread.id_order|intval}{else}{if isset($smarty.post.id_order)}{$smarty.post.id_order|intval}{/if}{/if}" />
									{elseif $customerThread.id_order > 0}
										<input type="text" name="id_order" style="width:400px" id="id_order" value="{$customerThread.id_order|intval}" readonly="readonly" />
									{/if}
								</p>
								{/if}
								
								
								{if empty($customerThread.id_product)}
								<p class="text">
								<label for="id_ticket"><strong>{l s='Ticket'}</strong><br />
								
								{l s='Please select a ticket'}
								
								</label>
								
									<select style="width:400px" id="id_ticket" name="id_ticket"><option value="0">{l s='-- Choose --'}</option>{$ticketList}</select>
								
								</p>
								{/if}
								
								{if $smarty.get.step == 'tecnica'}
								<p class="text">
								<label for="priorita"><strong>{l s='Priority'}</strong><br />
								
								{l s='Please select the priority'}
								
								</label>
								
									<select style="width:400px" id="priorita" name="priorita">
									<option value="low">{l s='Low'}</option>
									<option value="medium">{l s='Medium'}</option>
									{if $cookie->id_default_group == 8 || $cookie->id_default_group == 19}{literal}<option value="high" onclick="var txt; var r = confirm('Si tratta davvero di una richiesta di alta urgenza?'); if (r == true) {
	} else { this.value = 'medium' ;} ">{l s='High'}</option>{/literal}{/if}
									
									</select>
								
								</p>
								{/if}
								
								{if $smarty.post.id_contact == 9 || ($contact.id_contact == 9 && $smarty.get.step == 'rma')}
									
								
									<p class="text">
									<label for="quantity"><strong>{l s='Please select product quantity'}*</strong>
									</label>
									<input type="text" style="width:400px" name="quantity" value="{$quantity}" />
									</p>
									
									<p class="text">
									<label for="seriale"><strong>{l s='Please insert the product serial'}</strong><br />
									{l s='If your product does not have a serial number, please leave this field blank'}
									</label>
									<input type="text" style="width:400px" name="seriale" value="{$seriale}" />
									</p>
									
									<p class="text">
									<label for="quantity"><strong>{l s='Please select the RMA type'}*</strong>
									</label>
									<div id="desc_rma_type" class="desc_contact" style="margin-left:245px; margin-top:-10px; width:400px; text-align:justify">
										
									<input type="radio" name="rma_type" value="Riparazione" /><strong>{l s='REPAIR'}</strong><br />{l s='Customers accept repairing their products if the repairing costs of products without warranty is lower than 20,00 euro including VAT and excluding transportation costs (this rate is applied also for not accepted repair quotations). For costs greater than 20,00 euros, we will send a quotation to your fax number or your email address. If we are not receiving an anwer by two days, your products will be returned with charge of costs.'}<br /><br />
									<input type="radio" name="rma_type" value="Reso" /><strong>{l s='RETURNED GOODS'}</strong><br />{l s='We accept returned goods only if perfectly packaged with all their accessories and if not broken, scratched and in their original packages without stickers and tags other than the originals. PLEASE DO NOT USE the product packaging as container: please insert it in a packing box. The authorization for the returned goods, except in the case of specific indications provided by our staff, is  subject to the paragraphs 5.1, 5.2 and 5.3 of our'} <a href='https://www.ezdirect.it/guide/3-termini-e-condizioni-acquisto-on-line-ezdirect#recesso'>{l s='terms and conditions'}</a>.
									</p>
									</div>
									
									<p class="text">
									<label for="quantity"><strong>{l s='Please select the shipping type'}*</strong>
									</label>
									<div id="desc_rma_type" class="desc_contact" style="margin-left:245px; margin-top:-10px; width:400px; text-align:justify">
									<input type="radio" name="rma_shipping" value="Corriere Cliente" /><strong>{l s='Customer carrier'}</strong><br />
									<input type="radio" name="rma_shipping" value="Corriere Ezdirect" /><strong>{l s='Ezdirect carrier - charge of 12 euros VAT excluded for each run.'}</strong><br />
									{l s='ATTENTION PLEASE: if you want to use Ezdirect carrier you have to book the pick up calling GLS carrier number 0298035556'}
									</p>
									</div>
								{else}
								{/if}
							{else}
							
								{if $contact.id_contact == 5}
									
								
									<p class="text">
									<label for="id_product"><strong>{l s='Product'}</strong>
									{if $smarty.get.step == 'tecnica'}<br />
									{l s='Please select the product for which you are requesting assistance'}
									{else}<br />
									{l s='Please select a product you have bought or a product in our catalog'}
									{/if}
									</label>
									<select style="display:block; margin-left:10px; width:400px" id="tuttiprodotti" name="id_product" style="width:300px;"><option value="0">{l s='-- Choose --'}</option>{$tuttiprodotti}</select>
									</p>
									{if (!isset($customerThread.id_order) || $customerThread.id_order > 0)}
									<p class="text">
										<label for="id_order"><strong>{l s='Order ID'}{if $contact.id_contact == 9}*{/if}</strong><br />
										{l s='Please specify the ID of your order'}
										
										</label>
										{if !isset($customerThread.id_order) && isset($isLogged) && $isLogged == 1}
											<select name="id_order" style="width:400px" id="id_order" ><option value="0">{l s='-- Choose --'}</option>{$orderList}</select>
										{elseif !isset($customerThread.id_order) && !isset($isLogged)}
											<input type="text" name="id_order" style="width:400px" id="id_order" value="{if isset($customerThread.id_order) && $customerThread.id_order > 0}{$customerThread.id_order|intval}{else}{if isset($smarty.post.id_order)}{$smarty.post.id_order|intval}{/if}{/if}" />
										{elseif $customerThread.id_order > 0}
											<input type="text" name="id_order" style="width:400px" id="id_order" value="{$customerThread.id_order|intval}" readonly="readonly" />
										{/if}
									</p>
									{/if}
								{else}
								{/if}
								
								
							
							{/if}
							
							
							
						{/if}
						{if $fileupload == 1}
							<p class="text">
							<label for="fileUpload"><strong>{l s='Attach File'}</strong><br />
							{l s='Use this field if you want to send us files'}
							</label>
								<input type="hidden" name="MAX_FILE_SIZE" value="200000000" />
								<input style="width:400px" type="file" name="fileUpload[]" id="fileUpload1" /><br />
								<input style="width:400px; margin-top:2px" type="file" name="fileUpload[]" id="fileUpload2" />
							</p>
						{/if}
						<p class="textarea">
							{if $contact.id_contact == 9}
							<label for="message"><strong>{l s='Please insert a reason for your RMA'}*</strong><br />
							{else}
							<label for="message"><strong>{l s='Notes'}*</strong><br />
							{/if}
							{l s='Message'}</label>
							 <textarea id="message" name="message" rows="15" cols="20" style="width:400px;height:220px">{if isset($message)}{$message|escape:'htmlall':'UTF-8'|stripslashes}{/if}</textarea>
						</p>
						
						<p class="text">
								<label for="privacy"><strong><a href="{$link->getPageLink('privacy.php', true)}" target="_blank">{l s='I accept privacy terms'}</a>*</strong><br />
								</label>
								 <input type="checkbox" name="privacy" />
							</p>
						
						<p class="submit">
							<input type="submit" name="submitMessage" id="submitMessage" value="{l s='Send'}" class="button_large" onclick="$(this).hide(); fbq('track', 'Contact');" />
						</p>
					</fieldset>
				</form>
			{/if}
		{/if}	
	{/if}
{/if}