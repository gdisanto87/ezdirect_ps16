{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='Your shopping cart'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}


<div>


{assign var='current_step' value='summary'}
{include file="$tpl_dir./errors.tpl"}



{if isset($empty)}
	<p class="warning">{l s='Your shopping cart is empty.'}</p>
{elseif $PS_CATALOG_MODE}
	<p class="warning">{l s='This store has not accepted your new order.'}</p>
{else}
	<script type="text/javascript">
	// <![CDATA[
	var currencySign = '{$currencySign|html_entity_decode:2:"UTF-8"}';
	var currencyRate = '{$currencyRate|floatval}';
	var currencyFormat = '{$currencyFormat|intval}';
	var currencyBlank = '{$currencyBlank|intval}';
	var txtProduct = "{l s='product'}";
	var txtProducts = "{l s='products'}";
	// ]]>
	</script>
	<p style="display:none" id="emptyCartWarning" class="warning">{l s='Your shopping cart is empty.'}</p>

{if $preventivo == 1}
<div class="shopping_cart_table" {if !$opc}{else}style="width:98.3%; text-align:center; padding:5px"{/if}>
<strong>{l s='WARNING: Carts created by our staff cannot be edited'}.<br />{l s='If you want to edit your cart'}, <a href="{$link_needs}">{l s='please click here to specify your needs'}.</a></strong>
</div>
{/if}

{if $customer->pec == '' && $customer->codice_univoco == ''}
<div class="shopping_cart_table" {if !$opc}{else}style="width:98.3%; text-align:center; padding:5px"{/if}>
<strong>{l s='ATTENZIONE: inserire o il codice SDI, o la tua PEC'}.<br /><a href='https://www.ezdirect.it/identita'>Clicca qui per aggiornare il tuo profilo con i dati necessari.</a>  Ti ricordiamo che in mancanza di tali dati, potrai collegarti al sito dell'Agenzia delle Entrate e scaricare la fattura.</strong>
</div>

{/if}

{if $canoni > 0}
<div class="shopping_cart_table" {if !$opc}{else}style="width:98.3%; text-align:left; padding:5px"{/if}>
Gentile cliente, il presente carrello/ordine &egrave; costituito da 2 sezioni.<br /><br />

<strong>Cosa sono i "canoni periodici ezcloud"?</strong> Sono i costi del periodo (mese o anno) di utilizzo.<br />

&Egrave; quindi un canone periodico fatturato/addebitato per il periodo di competenza. {if $canone_mensile > 0}Il tuo canone mensile &egrave; di <strong>{$canone_mensile} &euro; / mese</strong>.{/if}<br /><br />

<strong>Quando inizio a pagare il canone del servizio?</strong> Il canone del primo periodo di utilizzo sar&agrave; quantificato sulla base dei giorni effettivi di utilizzo, a partire dal giorno di attivazione (che sar&agrave; notificato anche con una comunicazione scritta dal nostro staff).<br />

Se ad esempio viene attivato il giorno 15 settembre, verranno addebitati (il mese successivo), i 15 giorni di settembre e, da ottobre,verr&agrave; fatturato/addebitato (il mese successivo) il canone mensile per intero di ottobre e cos&igrave; via...<br /><br />

<strong>Cosa contiene la sezione 2 Prodotti e servizi in conto vendita?</strong><br />

La seconda sezione riguarda i costi "una tantum", (da pagare in fase di ordine) ovvero: Servizio di attivazione, servizio di configurazione, prodotti vari e accessori.
</div>
{/if}
<br />
{include file="$tpl_dir./order-steps.tpl"}
<div class="shopping_cart_table" id="shopping_cart_products" style="position:relative; width:99.3%">
<div class="shopping_cart_header" style=" height:24px; background-color:#db6906;  background-repeat:no-repeat;">
<h2 style="color:#fff">{l s='Shopping cart summary'}</h2>
</div>
	<table id="cart_summary" style="padding:5px" class="std">
		<thead>
			{if $canoni > 0}
			<tr>
			<th colspan="8">SEZIONE 1: CANONI PERIODICI EZCLOUD</th></tr>
			{/if}
			<tr>
				<th style="color:#000; background-image:none; width:75px" class="cart_product first_item">{l s='Product'}</th>
				<th style="color:#000; background-image:none; width:40%" class="cart_description item">{l s='Description'}</th>
				<th style="color:#000; background-image:none; width:135px; text-align:center" class="cart_ref item">{l s='Ref.'}</th>
				<th style="color:#000; background-image:none; width:50px; text-align:center" class="cart_availability item">{l s='Avail.'}</th>
				<th style="color:#000; background-image:none; width:125px; text-align:right" class="cart_unit item">{l s='Unit price'}</th>
				<th style="color:#000; background-image:none; width:70px; text-align:center" class="cart_quantity item"></th>
				<th style="color:#000; background-image:none; width:70px; text-align:center" class="cart_quantity item">{l s='Qty'}</th>
				<th style="color:#000; background-image:none; width:125px; text-align:right" class="cart_total last_item">{l s='Total'}</th>
			</tr>
		</thead>
		<tfoot style="font-size:16px">
				<tr class="cart_total_price">
					<td colspan="7">
					
					{if $cart_by_ezdirect == 1}
						{if $sta_usando_diverso == 1}
						{else}
						<!-- <a href="{$link->getPageLink('modules/mieofferte/download_offerta.php')}?download=pdf" class="button_large" title="{l s='Download offer'}" style="float:left;  padding-top:7px; height:23px; margin-left:10px; font-size:14px">{l s='Download offer'}</a> -->
						{/if}
					{/if}
					</td>
					<td>
					
					</td>
				</tr>
			{if $use_taxes}
				{if $priceDisplay}
					<tr class="cart_total_price">
						<td colspan="7">{l s='Total products'}{if $display_tax_label} {l s='(tax excl.)'}{/if}{l s=':'}</td>
						<td class="price" style="font-size:16px; color:#d10000" id="total_product">{displayPrice price=$total_products}</td>
					</tr>
				{else}
					<tr class="cart_total_price">
						<td colspan="7">{l s='Total products'}{if $display_tax_label} {l s='(tax incl.)'}{/if}{l s=':'}</td>
						<td class="price" style="font-size:16px; color:#d10000" id="total_product">{displayPrice price=$total_products_wt}</td>
					</tr>
				{/if}
			{else}
				<tr class="cart_total_price">
					<td colspan="7">{l s='Total products:'}</td>
					<td class="price" style="font-size:16px; color:#d10000" id="total_product">{displayPrice price=$total_products}</td>
				</tr>
			{/if}
			<tr class="cart_total_voucher" {if $total_discounts == 0}style="display: none;"{/if}>
				<td colspan="7">
				{if $use_taxes}
					{if $priceDisplay}
						{l s='Total vouchers'}{if $display_tax_label} {$discount_name} <a href="{if $opc}{$link->getPageLink('order-opc.php', true)}{else}{$link->getPageLink('order.php', true)}{/if}?deleteDiscount={$id_discount}" title="{l s='Delete'}" style="text-decoration:none; color:#0000FF"><img src="{$img_dir}icon/icon_delete.gif" alt="{l s='Delete'}" class="icon" width="18" height="18" /></a> {l s='(tax excl.)'}{/if}{l s=':'}
					{else}
						{l s='Total vouchers'}{if $display_tax_label} {l s='(tax incl.)'}{/if}{l s=':'}
					{/if}
				{else}
					{l s='Total vouchers:'}
				{/if}
				</td>
				<td class="price-discount" id="total_discount">
				{if $use_taxes}
					{if $priceDisplay}
						{displayPrice price=$total_discounts_tax_exc}
					{else}
						{displayPrice price=$total_discounts}
					{/if}
				{else}
					{displayPrice price=$total_discounts_tax_exc}
				{/if}
				</td>
			</tr>
			<tr class="cart_total_voucher" {if $total_wrapping == 0}style="display: none;"{/if}>
				<td colspan="7">
				{if $use_taxes}
					{if $priceDisplay}
						{l s='Total gift-wrapping'}{if $display_tax_label} {l s='(tax excl.)'}{/if}{l s=':'}
					{else}
						{l s='Total gift-wrapping'}{if $display_tax_label} {l s='(tax incl.)'}{/if}{l s=':'}
					{/if}
				{else}
					{l s='Total gift-wrapping:'}
				{/if}
				</td>
				<td class="price-discount" id="total_wrapping">
				{if $use_taxes}
					{if $priceDisplay}
						{displayPrice price=$total_wrapping_tax_exc}
					{else}
						{displayPrice price=$total_wrapping}
					{/if}
				{else}
					{displayPrice price=$total_wrapping_tax_exc}
				{/if}
				</td>
			</tr>
			{if $use_taxes}
				{if $priceDisplay}
					<tr class="cart_total_delivery">
						<td colspan="7">{l s='Total shipping'}{if $display_tax_label} {l s='(tax excl.)'}{/if}{l s=':'}</td>
						<td id="total_shipping">{displayPrice price=$shippingCostTaxExc}</td>
					</tr>
				{else}
					<tr class="cart_total_delivery">
						<td colspan="7">{l s='Total shipping'}{if $display_tax_label} {l s='(tax incl.)'}{/if}{l s=':'}</td>
						<td id="total_shipping" >{displayPrice price=$shippingCost}</td>
					</tr>
				{/if}
			{else}
				<tr class="cart_total_delivery">
					<td colspan="7">{l s='Total shipping:'}</td>
					<td  id="total_shipping" >{displayPrice price=$shippingCostTaxExc}</td>
				</tr>
			{/if}

			{if $use_taxes}
			<tr class="cart_total_price">
				<td colspan="7">
					{if $display_tax_label}
						{l s='Total (tax excl.):'}
					{else}
						{l s='Subtotal:'}
					{/if}
				</td>
				<td class="price" style="font-size:16px; color:#d10000" id="total_price_without_tax">{displayPrice price=$total_price_without_tax}</td>
			</tr>
			<tr class="cart_total_tax">
				<td colspan="7">
					{if $display_tax_label}
						{l s='Total tax:'}
					{else}
						{l s='Estimated Sales Tax:'}
					{/if}
				</td>
				<td  id="total_tax">{displayPrice price=$total_tax}</td>
			</tr>
			<tr class="cart_total_price">
				<td colspan="7">
					{if $display_tax_label}
						{l s='Total (tax incl.):'}
					{else}
						{l s='Total:'}
					{/if}
					<br />(IVA dove applicabile)
				</td>
				<td class="price" style="font-size:16px; color:#d10000" id="total_price">{displayPrice price=$total_price}</td>
			</tr>
			{else}
			<tr class="cart_total_price">
				<td colspan="7">{l s='Total:'}</td>
				<td class="price" style="font-size:16px; color:#d10000" id="total_price">{displayPrice price=$total_price_without_tax}</td>
			</tr>
			{/if}
			<tr class="cart_free_shipping" {if $free_ship <= 0 || $isVirtualCart} style="display: none;" {/if}>
					<td colspan="7" style="white-space: normal;">{l s='Remaining amount to be added to your cart in order to obtain free shipping:'}</td>
					<td id="free_shipping" class="price">{displayPrice price=$free_ship}</td>
				</tr>
		</tfoot>
		<tbody>
		{assign var='stop' value=0}
		{foreach from=$products item=product name=productLoop}
			{assign var='productId' value=$product.id_product}
			{assign var='productAttributeId' value=$product.id_product_attribute}
			{assign var='quantityDisplayed' value=0}
			
			{if $sezione == $product.section}

<!-- -->
{else}
{$sezione = $product.section}
<thead>
			
			<tr>
			<th colspan="8">SEZIONE 2: PRODOTTI E SERVIZI IN C/VENDITA</th></tr>
			
			<tr>
				<th style="color:#000; background-image:none; width:75px" class="cart_product first_item">{l s='Product'}</th>
				<th style="color:#000; background-image:none; width:40%" class="cart_description item">{l s='Description'}</th>
				<th style="color:#000; background-image:none; width:135px; text-align:center" class="cart_ref item">{l s='Ref.'}</th>
				<th style="color:#000; background-image:none; width:50px; text-align:center" class="cart_availability item">{l s='Avail.'}</th>
				<th style="color:#000; background-image:none; width:125px; text-align:right" class="cart_unit item">{l s='Unit price'}</th>
				<th style="color:#000; background-image:none; width:70px; text-align:center" class="cart_quantity item"></th>
				<th style="color:#000; background-image:none; width:70px; text-align:center" class="cart_quantity item">{l s='Qty'}</th>
				<th style="color:#000; background-image:none; width:125px; text-align:right" class="cart_total last_item">{l s='Total'}</th>
			</tr>
		</thead>
		
{/if}

			{* Display the product line *}
			{include file="$tpl_dir./shopping-cart-product-line.tpl"}
			{* Then the customized datas ones*}
			{if isset($customizedDatas.$productId.$productAttributeId)}
				{foreach from=$customizedDatas.$productId.$productAttributeId key='id_customization' item='customization'}
					<tr id="product_{$product.id_product}_{$product.id_product_attribute}_{$id_customization}" class="alternate_item cart_item">
						<td colspan="5">
							{foreach from=$customization.datas key='type' item='datas'}
								{if $type == $CUSTOMIZE_FILE}
									<div class="customizationUploaded">
										<ul class="customizationUploaded">
											{foreach from=$datas item='picture'}<li><img src="{$pic_dir}{$picture.value}_small" alt="" class="customizationUploaded" /></li>{/foreach}
										</ul>
									</div>
								{elseif $type == $CUSTOMIZE_TEXTFIELD}
									<ul class="typedText">
										{foreach from=$datas item='textField' name='typedText'}<li>{if $textField.name}{$textField.name}{else}{l s='Text #'}{$smarty.foreach.typedText.index+1}{/if}{l s=':'} {$textField.value}</li>{/foreach}
									</ul>
								{/if}
							{/foreach}
						</td>
						<td class="cart_quantity">
							<div style="float:right">
								<a rel="nofollow" id="{$product.id_product}_{$product.id_product_attribute}_{$id_customization}" href="{$link->getPageLink('cart.php', true)}?delete&amp;id_product={$product.id_product|intval}&amp;ipa={$product.id_product_attribute|intval}&amp;id_customization={$id_customization}&amp;token={$token_cart}"><img src="{$img_dir}icon/icon_delete.gif" alt="{l s='Delete'}" title="{l s='Delete this customization'}" width="18" height="18" class="icon" /></a>
							</div>
							<div id="cart_quantity_button" style="float:left">
							<a rel="nofollow" id="cart_quantity_up_{$product.id_product}_{$product.id_product_attribute}_{$id_customization}" href="{$link->getPageLink('cart.php', true)}?add&amp;id_product={$product.id_product|intval}&amp;ipa={$product.id_product_attribute|intval}&amp;id_customization={$id_customization}&amp;token={$token_cart}" title="{l s='Add'}"><img src="{$img_dir}icon/quantity_up.gif" alt="{l s='Add'}" width="14" height="9" /></a><br />
							{if $product.minimal_quantity < ($customization.quantity -$quantityDisplayed) OR $product.minimal_quantity <= 1}
							<a rel="nofollow" id="cart_quantity_down_{$product.id_product}_{$product.id_product_attribute}_{$id_customization}" href="{$link->getPageLink('cart.php', true)}?add&amp;id_product={$product.id_product|intval}&amp;ipa={$product.id_product_attribute|intval}&amp;id_customization={$id_customization}&amp;op=down&amp;token={$token_cart}" title="{l s='Subtract'}">
								<img src="{$img_dir}icon/quantity_down.gif" alt="{l s='Subtract'}" width="14" height="9" />
							</a>
							{else}
							<a style="opacity: 0.3;" id="cart_quantity_down_{$product.id_product}_{$product.id_product_attribute}_{$id_customization}" href="#" title="{l s='Subtract'}">
								<img src="{$img_dir}icon/quantity_down.gif" alt="{l s='Subtract'}" width="14" height="9" />
							</a>
							{/if}
							</div>
							<input type="hidden" value="{$customization.quantity}" name="quantity_{$product.id_product}_{$product.id_product_attribute}_{$id_customization}_hidden"/>
							<input size="2" type="text" value="{$customization.quantity}" class="cart_quantity_input" name="quantity_{$product.id_product}_{$product.id_product_attribute}_{$id_customization}"/>
						</td>
						<td class="cart_total"></td>
					</tr>
					{assign var='quantityDisplayed' value=$quantityDisplayed+$customization.quantity}
				{/foreach}
				{* If it exists also some uncustomized products *}
				{if $product.quantity-$quantityDisplayed > 0}{include file="$tpl_dir./shopping-cart-product-line.tpl"}{/if}
			{/if}
		{/foreach}
		</tbody>
	{if sizeof($discounts)}
		<tbody>
		{foreach from=$discounts item=discount name=discountLoop}
		<!--	<tr class="cart_discount {if $smarty.foreach.discountLoop.last}last_item{elseif $smarty.foreach.discountLoop.first}first_item{else}item{/if}" id="cart_discount_{$discount.id_discount}" style="font-weight:bold">
				<td></td>
				<td class="cart_discount_name" colspan="1">
				{if $prodotti_con_sconto}
					<strong>Sezione coupon</strong><br /><br />
					{assign var='i' value=0}
					
					 <strong>{l s='Discount applied to: '}</strong>
					{foreach from=$prodotti_con_sconto item=prodotto_con_sconto}
						{$i = $i+1}
						{$prodotto_con_sconto}
						{if $i == ($prodotti_con_sconto|@count)}{else} - {/if} 
						
					{/foreach} 
				{else}
				{/if}
				<span style="float:right; margin-top:-15px">Codice coupon: </span>
				</td>
				
				<td style="text-align:center" colspan="1">{$discount.name}</td>
				<td></td>
				<td></td>
				<td style="text-align:center"><a href="{if $opc}{$link->getPageLink('order-opc.php', true)}{else}{$link->getPageLink('order.php', true)}{/if}?deleteDiscount={$discount.id_discount}" title="{l s='Delete'}"><img src="{$img_dir}icon/icon_delete.gif" alt="{l s='Delete'}" class="icon" width="18" height="18" /></a></td>
				<td></td>
				<td class="cart_discount_price"><strong>Sconto</strong>: <span class="price-discount">
					
					{if $discount.value_real > 0}
						{if !$priceDisplay}{displayPrice price=$discount.value_real*-1}{else}{displayPrice price=$discount.value_tax_exc*-1}{/if}
					{/if}
				</span></td>
			</tr> -->
		{/foreach}
		</tbody>
	{/if}
	</table>
</div>



<p class="cart_navigation">
	<form action="order-payment.php" method="post">
	{if !$opc}<a href="{$link->getPageLink('order.php', true)}?step=1{if $back}&amp;back={$back}{/if}" class="exclusive" title="{l s='Next'}">{l s='Next'} &raquo;</a>{/if}
	<a href="{if (isset($smarty.server.HTTP_REFERER) && strstr($smarty.server.HTTP_REFERER, $link->getPageLink('order.php'))) || !isset($smarty.server.HTTP_REFERER)}{$link->getPageLink('index.php')}{else}{$smarty.server.HTTP_REFERER|escape:'htmlall':'UTF-8'|secureReferrer}{/if}" class="button_large" title="{l s='Continue shopping'}" style="float:left; padding-top:7px; height:23px">&laquo; {l s='Continue shopping'}</a>
{if $isLogged && $are_there_offers == 1}
	<a href="{$base_dir_ssl}modules/mieofferte/offerte.php" class="button_large" title="{l s='View all my offers'}" style="float:left; margin-left:10px;  padding-top:7px; height:23px">{l s='View all my offers'}</a>
	
	<a href="order-opc.php" class="button_large" style="float:left; margin-left:10px;  padding-top:7px; height:23px">{l s='Refresh cart'}</a>
	
	{/if}
	{if $cart_by_ezdirect == 1}
		{if $sta_usando_diverso == 1}
		{else}
		<a href="{$link->getPageLink('modules/mieofferte/download_offerta.php')}?download=pdf" class="button_large" title="{l s='Download offer'}" style="float:left;  padding-top:7px; height:23px; margin-left:10px"><img src='{$img_dir}button-icon/pdf.png' alt='{l s='Download offer'}' title='{l s='Download offer'}' /> {l s='Download offer'}</a>
	
		<a href="{$base_dir_ssl}modules/mieofferte/offerte.php?new_cart=yes" class="button_large" title="{l s='Create a new cart'}" style="float:left; margin-left:10px; padding-top:7px; height:23px">{l s='Create a new cart'}</a>
		{/if}
	{else}{/if}
	
	
	{if $preventivo == 1 && $date_check == 1}
		
	
	{else}
		
		{if $cfcheck == 0 || $cfcheck == 2}
		
			{if !$isLogged}
				<input type="submit" class='button_large' id='payment_button_shopping' onclick='alert("{l s='You must login to continue'}. {l s='If you have problems please contact our customer service'}"); return false;' style="{if isset($carriers) && $carriers && count($carriers)}display:block;{else}display:block;{/if} position:absolute; right:5px" value="{l s='Continue'}" />
			{else}
				<input type="submit" class='button_large' id='payment_button_shopping' onclick='alert("{l s='There are some details missing in your account: if you are a end user customer you must specify your tax code in your account details, and if you are a company you must specify both tax code and vat number. Please go to your account and update your profile details. MISSING DETAILS: '}{if $check_tx == 1}{l s='Tax code'}{else}{/if}{if $check_vat == 1}{l s='VAT number'}{else}{/if}{if $check_phone == 1}{l s='Phone number'}{else}{/if}. {l s='If your details seem correct, please update this page. If your problem does not solve, please contact our customer service (800 529767)'}"); return false;' style="{if isset($carriers) && $carriers && count($carriers)}display:block;{else}display:block;{/if} position:absolute; right:5px" value="{l s='Continue'}" />
			{/if}
		{else}
			
				{if $errors|@count > 0}
				<input type="submit" class='button_large' id='payment_button_shopping' onclick='alert("{l s='There are some problems in your cart. Please check the errors dialog'}. {l s='If you have problems please contact our customer service'}"); return false;' style="{if isset($carriers) && $carriers && count($carriers)}display:block;{else}display:block;{/if} position:absolute; right:5px" value="{l s='Continue'}" />
				{else}
				<input type="submit" class='button_large' id='payment_button_shopping' onclick="return controlla_termini();" style="{if isset($carriers) && $carriers && count($carriers)}display:block;{else}display:block;{/if} position:absolute; right:5px" value="{l s='Continue'}" />
				{/if}
			
		{/if}
	{/if}
	</form>
</p>
		<!-- COUPON -->
							<div style="display:none">
	
</div>

			<script type="text/javascript">$('#codici_coupon').fancybox();</script>	
			<!-- FINE COUPON -->
<br /><br />
{if !empty($esigenze)}
<div class="shopping_cart_table" {if !$opc}{else}style="width:98.3%; padding:5px"{/if}><br />
<strong>{l s='Esigenze'}</strong><br /><br />
{$esigenze}

</div><br />
{/if}

{if !empty($risorse)}
<div class="shopping_cart_table" {if !$opc}{else}style="width:98.3%; padding:5px"{/if}><br />
<strong>{l s='Risorse'}</strong><br /><br />
{$risorse}

</div><br />
{/if}

{if !empty($note_carrello)}
<div class="shopping_cart_table" {if !$opc}{else}style="width:98.3%; padding:5px"{/if}><br />
<strong>{l s='Notes'}</strong><br /><br />
{$note_carrello}

</div><br />
{/if}

{if !empty($link_allegati)}
<div class="shopping_cart_table" {if !$opc}{else}style="width:98.3%; padding:5px"{/if}><br />
<strong>{l s='Allegati'}</strong><br /><br />
{$link_allegati}

</div><br />
{/if}


{if $noleggio > 0 && $total_price_without_tax > 1499}
<div class="shopping_cart_table" {if !$opc}{else}style="width:98.3%; padding:5px;"{/if}><br />

<div style="float:left; width:400px">
<strong>{l s='Rent proposal'}</strong> ({l s='Amounts without taxes'})<br />
{l s='Do you want to take advantage of our rent proposal?'}<br />
{l s='Please call your consultant by phone (+39 0585 821163) for further informations about our rent proposal'}.<br /><br />
<a href="{$link->getCMSLink(3, $link->getCMSRewrite(3))}#rateale" title="Condizioni di vendita">{l s='Click here to read the terms and conditions'}</a>
</div>

<div style="float:left">
<table class="rte">
<tr>

<th style="border:1px solid black">{l s='Amount'}</th>
<th style="border:1px solid black">{l s='Contract Fee'} una tantum  </th>
<th style="border:1px solid black">{l s='Frequency'} </th>
<th style="border:1px solid black">{l s='Fee'} </th>
<th style="border:1px solid black">{l s='Lenght'}</th>
<th style="border:1px solid black">{l s='Ransom'} </th>
</tr>
<tr>
<td style="border:1px solid black">{displayPrice price=$total_price_without_tax}</td>
<td style="border:1px solid black"> {$spese_contratto} &euro;</td>
<td style="border:1px solid black"> 
{if $total_price_without_tax > 1500 && $total_price_without_tax < 2999} {l s='3 Months'} {else}{l s='Monthly'}{/if}
</td>
<td style="border:1px solid black"> {if $total_price_without_tax > 1500 && $total_price_without_tax < 2999} {(($rata_mensile|replace:",":".")*3)|replace:".":","} {else} {$rata_mensile}  {/if}  &euro;</td>
<td style="border:1px solid black"> {$mesi_noleggio} {l s='Months'}</td>
<td style="border:1px solid black"> {$rata_mensile} &euro;</td>
</tr>
</table>
<br />
Spese di incasso: 4 &euro; + IVA
</div>

<div style="clear:both"></div>

<br />
</div>
{/if}


<br />
<div id="HOOK_SHOPPING_CART" style="border:0px">{$HOOK_SHOPPING_CART}</div>


{* Define the style if it doesn't exist in the PrestaShop version*}
{* Will be deleted for 1.5 version and more *}
{if !isset($addresses_style)}
	{$addresses_style.company = 'address_company'}
	{$addresses_style.vat_number = 'address_company'}
	{$addresses_style.firstname = 'address_name'}
	{$addresses_style.lastname = 'address_name'}
	{$addresses_style.address1 = 'address_address1'}
	{$addresses_style.address2 = 'address_address2'}
	{$addresses_style.city = 'address_city'}
	{$addresses_style.country = 'address_country'}
	{$addresses_style.phone = 'address_phone'}
	{$addresses_style.phone_mobile = 'address_phone_mobile'}
	{$addresses_style.alias = 'address_title'}
{/if}
{if $carrier->id AND !isset($virtualCart)}
	
	{/if}
{if (($carrier->id AND !isset($virtualCart)) OR $delivery->id OR $invoice->id) AND !$opc}
<div class="order_delivery">
	
	
</div>

{/if}

<p>
{if $check_quantities == 1}
<strong>{l s='The quantity of one of the products you are purchasing is higher than the quantity we have in stock. If you want more informations about shippping,'} <a href="https://www.ezdirect.it/contattaci?step=assistenza-ordini" target="_blank">{l s='please click here'}</a>.</strong>
{else}
{/if}
</p>
<br />
<p class="clear"><br /><br /></p>
<div class="clear"></div>
<p class="cart_navigation_extra">
	<span id="HOOK_SHOPPING_CART_EXTRA">{$HOOK_SHOPPING_CART_EXTRA}</span>
</p>
{/if}
</div>





<p class="clear"></p>


</div>

</div>
