{*
* 2007-2011 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2011 PrestaShop SA
*  @version  Release: $Revision: 1.4 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<script type="text/javascript" src="{$js_dir}etc/idTab5.js">
</script>


<div itemscope="itemscope" itemtype="http://schema.org/Product">

{include file="$tpl_dir./errors.tpl"}
{if $errors|@count == 0}
<script type="text/javascript">
// <![CDATA[

// PrestaShop internal settings
var currencySign = '{$currencySign|html_entity_decode:2:"UTF-8"}';
var currencyRate = '{$currencyRate|floatval}';
var currencyFormat = '{$currencyFormat|intval}';
var currencyBlank = '{$currencyBlank|intval}';
var taxRate = {$tax_rate|floatval};
var jqZoomEnabled = {if $jqZoomEnabled}true{else}false{/if};

//JS Hook
var oosHookJsCodeFunctions = new Array();

// Parameters
var id_product = '{$product->id|intval}';
var productHasAttributes = {if isset($groups)}true{else}false{/if};
var quantitiesDisplayAllowed = {if $display_qties == 1}true{else}false{/if};
var quantityAvailable = {if $display_qties == 1 && $product->quantity}{$product->quantity}{else}0{/if};
var allowBuyWhenOutOfStock = {if $allow_oosp == 1}true{else}false{/if};
var availableNowValue = '{$product->available_now|escape:'quotes':'UTF-8'}';
var availableLaterValue = '{$product->available_later|escape:'quotes':'UTF-8'}';
var productPriceTaxExcluded = {$product->getPriceWithoutReduct(true)|default:'null'} - {$product->ecotax};
var reduction_percent = {if $product->specificPrice AND $product->specificPrice.reduction AND $product->specificPrice.reduction_type == 'percentage'}{$product->specificPrice.reduction*100}{else}0{/if};
var reduction_price = {if $product->specificPrice AND $product->specificPrice.reduction AND $product->specificPrice.reduction_type == 'amount'}{$product->specificPrice.reduction}{else}0{/if};
var specific_price = {if $product->specificPrice AND $product->specificPrice.price}{$product->specificPrice.price}{else}0{/if};
var specific_currency = {if $product->specificPrice AND $product->specificPrice.id_currency}true{else}false{/if};
var group_reduction = '{$group_reduction}';
var default_eco_tax = {$product->ecotax};
var ecotaxTax_rate = {$ecotaxTax_rate};
var currentDate = '{$smarty.now|date_format:'%Y-%m-%d %H:%M:%S'}';
var maxQuantityToAllowDisplayOfLastQuantityMessage = {$last_qties};
var noTaxForThisProduct = {if $no_tax == 1}true{else}false{/if};
var displayPrice = {$priceDisplay};
var productReference = '{$product->reference|escape:'htmlall':'UTF-8'}';
var productAvailableForOrder = {if (isset($restricted_country_mode) AND $restricted_country_mode) OR $PS_CATALOG_MODE}'0'{else}'{$product->available_for_order}'{/if};
var productShowPrice = '{if !$PS_CATALOG_MODE}{$product->show_price}{else}0{/if}';
var productUnitPriceRatio = '{$product->unit_price_ratio}';
var idDefaultImage = {if isset($cover.id_image_only)}{$cover.id_image_only}{else}0{/if};

// Customizable field
var img_ps_dir = '{$img_ps_dir}';
var customizationFields = new Array();
{assign var='imgIndex' value=0}
{assign var='textFieldIndex' value=0}
{foreach from=$customizationFields item='field' name='customizationFields'}
	{assign var="key" value="pictures_`$product->id`_`$field.id_customization_field`"}
	customizationFields[{$smarty.foreach.customizationFields.index|intval}] = new Array();
	customizationFields[{$smarty.foreach.customizationFields.index|intval}][0] = '{if $field.type|intval == 0}img{$imgIndex++}{else}textField{$textFieldIndex++}{/if}';
	customizationFields[{$smarty.foreach.customizationFields.index|intval}][1] = {if $field.type|intval == 0 && isset($pictures.$key) && $pictures.$key}2{else}{$field.required|intval}{/if};
{/foreach}

// Images
var img_prod_dir = '{$img_prod_dir}';
var combinationImages = new Array();

{if isset($combinationImages)}
	{foreach from=$combinationImages item='combination' key='combinationId' name='f_combinationImages'}
		combinationImages[{$combinationId}] = new Array();
		{foreach from=$combination item='image' name='f_combinationImage'}
			combinationImages[{$combinationId}][{$smarty.foreach.f_combinationImage.index}] = {$image.id_image|intval};
		{/foreach}
	{/foreach}
{/if}

combinationImages[0] = new Array();
{if isset($images)}
	{foreach from=$images item='image' name='f_defaultImages'}
		combinationImages[0][{$smarty.foreach.f_defaultImages.index}] = {$image.id_image};
	{/foreach}
{/if}

// Translations
var doesntExist = '{l s='The product does not exist in this model. Please choose another.' js=1}';
var doesntExistNoMore = '{l s='This product is no longer in stock' js=1}';
var doesntExistNoMoreBut = '{l s='with those attributes but is available with others' js=1}';
var uploading_in_progress = '{l s='Uploading in progress, please wait...' js=1}';
var fieldRequired = '{l s='Please fill in all required fields' js=1}';

{if isset($groups)}
	// Combinations
	{foreach from=$combinations key=idCombination item=combination}
		addCombination({$idCombination|intval}, new Array({$combination.list}), {$combination.quantity}, {$combination.price}, {$combination.ecotax}, {$combination.id_image}, '{$combination.reference|addslashes}', {$combination.unit_impact}, {$combination.minimal_quantity});
	{/foreach}
	// Colors
	{if $colors|@count > 0}
		{if $product->id_color_default}var id_color_default = {$product->id_color_default|intval};{/if}
	{/if}
{/if}
//]]>




</script>
<script>
  fbq('track', 'ViewContent', {
    content_ids: '{$product->reference}',
  });
</script>


{include file="$tpl_dir./breadcrumb.tpl"}

<div id="product-header">
	<div id="product-header-left">
		<h1 class="product_name_title" itemprop="name">{$product->name|escape:'htmlall':'UTF-8'}</h1>
		<div class="mini-description">
			{if $product->description_short}
				<div id="short_description_content" class="rte align_justify"><span>{$product->description_short}</span></div>
			{/if}
			
		</div>
		<br />
		{if $product->reference}
			{l s='Reference:'} {$product->reference|escape:'htmlall':'UTF-8'} &nbsp;&nbsp;
			{else}{/if}
			{if $product->supplier_reference}
			{l s='SKU reference:'} <span itemprop="sku">{$product->supplier_reference|escape:'htmlall':'UTF-8'}</span> &nbsp;&nbsp;
			{else}{/if}
				{if $product->ean13}
			{l s='EAN:'} <span itemprop="isbn">{$product->ean13|escape:'htmlall':'UTF-8'}</span>
		{else}{/if}
		
			
			
		
			
	</div>
	<div id="product-header-right">
	{assign var=xx value=$img_manu_dir|cat:$product->id_manufacturer|cat:'-manufacturerprod.jpg'}
		{if $product->manufacturer_name}
			<a title="{$product->manufacturer_name}" href="{$link->getManufacturerLink2($product_manufacturer)}"><img src="{$img_manu_dir}{$product->id_manufacturer}-manufacturerprod.jpg" alt="{$product->manufacturer_name}" title="{$product->manufacturer_name}" style="border:0px;width:230px;min-height:60px;max-height:115px;" /></a>
			
			<br />
			<span class="stars" title="{$averageTotal|number_format:2:",":""} / 5 {l s='based on'} {$total_reviews} {l s='reviews'}">{$averageTotal}</span> 
			{if $total_reviews == 0} 
		{else}
				<a style="text-decoration:none" rel="nofollow" href="#idTab5" 
				onclick='var d = document.getElementById("idTab5"); d.className = d.className + "selected"; $(".idTabs").idTabs("idTab5"); $("html,body").animate({
				scrollTop: $("#idTab5").offset().top  - 150}, 1900); '>{$total_reviews} recensioni</a> | 
			</a> 
		{/if}	
			{literal}
			<a style="text-decoration:none" href="#write_a_comment" onclick="var d = document.getElementById('idTab5'); d.className = d.className + 'selected'; $('.idTabs').idTabs('idTab5'); $('html,body').animate({ scrollTop: $('#write_a_comment').offset().top  - 114}, 1900); $('#sendComment').slideDown('slow');$('#addCommentButton').slideUp('slow');">
			{/literal}
			{if $total_reviews == 0} 
				{l s='Be the first to write a review'}
			{else}
				{l s='Write a review'}
			{/if}
			</a>
	</div>
	<div style="clear:both"></div>
</div>
<div id="primary_block" class="clearfix">


	{if isset($adminActionDisplay) && $adminActionDisplay}
	<div id="admin-action">
		<p>{l s='This product is not visible to your customers.'}
		<input type="hidden" id="admin-action-product-id" value="{$product->id}" />
		<input type="submit" value="{l s='Publish'}" class="exclusive" onclick="submitPublishProduct('{$base_dir}{$smarty.get.ad}', 0)"/>
		<input type="submit" value="{l s='Back'}" class="exclusive" onclick="submitPublishProduct('{$base_dir}{$smarty.get.ad}', 1)"/>
		</p>
		<div class="clear" ></div>
		<p id="admin-action-result"></p>
		</p>
	</div>
	{/if}

	{if isset($confirmation) && $confirmation}
	<p class="confirmation">
		{$confirmation}
	</p>
	{/if}

<div style="display: flex;">
	<!-- right infos-->
	<div id="pb-right-column">
		
		
		
		<!-- product img-->
		{if $have_image}
			
		{/if}	
		<div id="image-block">
			{if $product->fuori_produzione == 1}
				<div class="ribbon"><span>{l s='Out of production'}</span></div>
			{/if}
			{if $migliorprezzo_link == 1 && $product->fuori_produzione != 1}
						
				<!-- <div class="ribbon_gold"><span>Miglior prezzo</span></div> -->
			{/if}
			
			{if $abilita_banda == 1}
						
				<div class="ribbon"><span>Offerta speciale</span></div> 
			{/if}
				
			{if $product->condition == 'refurbished'}
				<div style="position:absolute; top:0px; left:0px"><img src="https://www.ezdirect.it/img/ricondizionato.png" title="Ricondizionato" alt="Ricondizionato" /></div>
			{/if}
		{if $activate_coupon == 1}
			<div class="coupon_g">
				<img src="{$img_ps_dir}coupon_g.png" alt="Coupon" title="Coupon" />
			</div>
		{else}
		{/if}
		
		{if $have_image}
			<img itemprop="image" class="image_thickbox" src="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large')}"
				{if $jqZoomEnabled}class="jqzoom" alt="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'thickbox')}"{else} title="{$product->name|escape:'htmlall':'UTF-8'}" alt="{$product->name|escape:'htmlall':'UTF-8'}" {/if} id="bigpic" width="370" height="370" />
		{else}
		
			<img src="{$img_manu_dir}{$product->id_manufacturer}-large.jpg" alt="" title="{$product->name|escape:'htmlall':'UTF-8'}"  style="display:block; margin:0 auto; border:1px solid #d4d4d4" />
		{/if}
		
		{if $cheapnet == 'cheapnet_6'}
			<div id="cheapnet">
				<a href='https://www.ezdirect.it/cheapnetcoupons.php'><img src="{$img_ps_dir}cheapnet-300.png" alt="Con questo prodotto hai diritto a un Coupon Cheapnet da 300 minuti gratis" title="Con questo prodotto hai diritto a un Coupon Cheapnet da 300 minuti gratis" width="57" height="57" /></a>
			</div>
		{else if $cheapnet == 'cheapnet_30'}
			<div id="cheapnet">
				<a href='https://www.ezdirect.it/cheapnetcoupons.php'><img src="{$img_ps_dir}cheapnet-600.png" alt="Con questo prodotto hai diritto a un Coupon Cheapnet da 600 minuti gratis" title="Con questo prodotto hai diritto a un Coupon Cheapnet da 600 minuti gratis" width="57" height="57" /></a>
			</div>
		{else}
		{/if}
		
		</div>
		<br />

		<!-- thumbnails -->
		<div id="views_block" {if isset($images) && count($images) < 2}class="hidden"{/if}>
		{if isset($images) && count($images) > 3}
			<span class="view_scroll_spacer"><a id="view_scroll_left" class="hidden" title="{l s='Other views'}" 	href="javascript:{ldelim}{rdelim}"></a></span>
		{/if}
		<div id="thumbs_list">
		{if isset($images)}

			<ul id="thumbs_list_frame">
				
					{foreach from=$images item=image name=thumbnails}
					{assign var=imageIds value="`$product->id`-`$image.id_image`"}
					<li id="thumbnail_{$image.id_image}">
						<a href="{$link->getImageLink($product->link_rewrite, $imageIds, 'thickbox')}"  rel="group1" class="fancybox {if (isset($image.cover) AND $image.cover == 1) OR (!isset($image.cover) AND $smarty.foreach.thumbnails.first)}shown{/if}" title="{$image.legend|htmlspecialchars}">
							<img id="thumb_{$image.id_image}" src="{$link->getImageLink($product->link_rewrite, $imageIds, 'medium')}" alt="{$image.legend|htmlspecialchars}" height="50" width="50" />
						</a>
					</li>
					{/foreach}
					
						</ul>

						{/if}
			</div>
				
		
			

		{if isset($images) && count($images) > 3}<span class="view_scroll_spacer"><a id="view_scroll_right" title="{l s='Other views'}" href="javascript:{ldelim}{rdelim}"></a></span>{/if}
		</div>

						<div id="video_list">
						<script type="text/javascript" src="{$js_dir}etc/video_stars.js">
		</script>
			{if $videos}
			<ul style="width:280px">
			{foreach from=$videos item=v}
				<li><a href='{$v.content|escape:'quotes':'UTF-8'}' class='video-fc'><img src="{$img_dir}video.gif" alt="Video" title="Video" width="66" height="23" /></a></li>
			{/foreach}
		</ul>
		{/if}
		</div>


		<div style="clear:both"></div>
		<div style="float:left; margin-left:0px">	
			
			<p class="align_center clear"><span id="wrapResetImages" style="display: none;"><img src="{$img_dir}icon/cancel_16x18.gif" alt="{l s='Cancel'}" width="16" height="18"/> <a id="resetImages" href="{$link->getProductLink2($product)}" onclick="$('span#wrapResetImages').hide('slow');return (false);">{l s='Display all pictures'}</a></span></p>{/if}
			

		</div>
			
	</div>

	<div id="pb-center-column">
		<div id="pb-mini-desc">{$mini_description}...
	
			<br /><br />{literal}<a href="#" onclick='$("html,body").animate({scrollTop: $("#idTab1").offset().top - 150},1900);'>Leggi tutto</a>{/literal}<br /><br />
		</div>
		{if $confezione != ''}
				
		<strong>{l s='Package'}</strong>: {$confezione}<br  />
		
		{else}

		{/if}
		<br />
		<hr class="footer_hr" />
		<a href="https://www.ezdirect.it/preventivi-quotazioni-miglior-prezzo-cuffie-centralini?category={$product->id}" rel="nofollow"><img src="{$img_dir}product-preventivo.jpg"  style="border:0px; margin-left:11px" alt="{l s='Free quotation'}" title="{l s='Free quotation'}" /></a>
		
		<a rel="nofollow" href="https://www.ezdirect.it/ti-richiamiamo-noi"><img src="{$img_dir}product-ti-richiamiamo-noi.jpg"  style="border:0px" alt="{l s='We will call you back'}" title="{l s='We will call you back'}" /></a>
				
			<a href="javascript:void(0)" rel="nofollow" onclick="$('.sc-fzXfMz').trigger('click');"><img src="{$img_dir}product-supporto-in-linea.jpg" style="border:0px" alt="{l s='Live help desk'}" title="{l s='Live help desk'}" /></a>
			
		</a>

		<hr class="footer_hr" />
		<img src="{$img_dir}product-carte.jpg"  style="border:0px" alt="" title="" />
			
	</div>

	<!-- left infos-->
	<div id="pb-left-column">
	
		
		
		{if $real_quantity <= 0 && ($alternatives|count) > 0 && $product->id_category_default == 266}
		<script type="text/javascript" src="https://www.ezdirect.it/js/sweetalert2.js"></script>
		<link rel="stylesheet" href="https://www.ezdirect.it/css/sweetalert2.css" />

		<script type="text/javascript">

					$(document).ready(function(){
						var alternatives = document.getElementById("my_alternatives").innerHTML;
							swal({
							  title: 'Selezione di prodotti simili o alternativi',
							  text: '',
							  customClass: 'swal-wide',
							  confirmButtonText: 'Chiudi questo avviso',
							 html: alternatives,
							});
							
						
					});
				</script>
				<div style="display:none" id="my_alternatives">
		<div id="alternatives-carousel" style="border:0px; margin-top:10px; margin-left:-7px">
		<div style="right:-20px; width:35px; position:absolute; top:-50px"><a href="javascript:void(0)" onclick="swal.closeModal(); return false;"><img src='https://www.ezdirect.it/img/close-b.png' alt='' title='Chiudi' /></a></div>
				<div id="alternatives-carousel-int">
					<table style="width: 100%; table-layout: fixed; margin:0 auto" cellspacing="12">
					{assign var=j value=0}
					{foreach from=$alternatives item=alternative name=alternatives_list}
						{assign var='alternativeLink' value=$link->getProductLink($alternative.id_product, $alternative.link_rewrite, $alternative.category)}
						
						{if $j == 0}
							<tr>
						{else}
						{/if}
						
						
						<td>
							
							<div class="product_desc" style="margin-top:0px"><br />
								<a href="{$alternativeLink|escape:'htmlall':'UTF-8'}" title="{$alternative.name|escape:'htmlall':'UTF-8'}" class="product_image"><img src="{$link->getImageLink($alternative.link_rewrite, $alternative.id_image, 'medium')}" alt="{$alternative.name|escape:'htmlall':'UTF-8'}" width="{$mediumSize.width}" height="{$mediumSize.height}" /></a>
					
							</div>
							<div style="text-align:left">
								<span style="font-size:13px; display:block; width:168px; height:50px" class="alternative_name"><a href="{$alternativeLink|escape:'htmlall':'UTF-8'}" title="{$alternative.name|escape:'htmlall':'UTF-8'}">{$alternative.name|truncate:70:'...':true|escape:'htmlall':'UTF-8'}</a></span>
								<p class="product_alternatives_price" >
								
								{if $alternative.price == 0 || ($alternative.fuori_produzione == 1 && $alternative.real_quantity == 0)}
								<a href='http://www.ezdirect.it/preventivi-quotazioni-miglior-prezzo-cuffie-centralini?category={$alternative.id_product}' >Richiedi preventivo</a>
								{else}
									{if $alternative.quantity_discount && $alternative.prezzospeciale == 0}
					
										{assign var="i" value="0"}
										{assign var=numItems value=$alternative.quantity_discount|@count}
										{foreach from=$alternative.quantity_discount item='quantity_discount' name='product.quantity_discounts'}
										{if $i == $numItems-1}				

										{l s='From '} <span class="price" style="display: inline;"> {convertPrice price=$quantity_discount.price - ($quantity_discount.price * $quantity_discount.reduction)|floatval}</span> 
										{else}	
										{/if}
										{$i = $i+1}
										{/foreach}
					
									{else}
									{if isset($alternative.show_price) && $alternative.show_price && !isset($restricted_country_mode)}<span class="price" style="display: inline;">{if !$priceDisplay}{convertPrice price=$alternative.price}{else}{convertPrice price=$alternative.price_tax_exc}{/if}</span>{/if}

									{/if}

									</p>
									<p style=" height: 70px; font-size:14px">
									{if $alternative.price == 0  ||($alternative.fuori_produzione == 1 && $alternative.real_quantity <= 0)}

		{if $alternative.date_available_msg != "" && $alternative.real_quantity <= 0}
		<img style='width:18px' src='{$img_ps_dir}orange_alert.gif'  alt='{l s='Available from'} {$alternative.date_available|date_format:"%d/%m/%Y"}' /><strong>{l s='Data disponibilità'}: {$alternative.date_available_msg}</strong>
		{else}
		{if $smarty.now|date_format:"%Y-%m-%d" < $alternative.date_available}
								<img style='width:18px' src='{$img_ps_dir}orange_alert.gif'  alt='{l s='Available from'} {$alternative.date_available|date_format:"%d/%m/%Y"}' /><strong>{l s='Available from'} {$alternative.date_available|date_format:"%d/%m/%Y"}</strong>

		{else}
		{/if}
		{/if}
		{if ($smarty.now|date_format:"%Y-%m-%d" > $alternative.date_available) && $alternative.real_quantity <= 0 && $smarty.now|date_format:"%Y-%m-%d" < $alternative.data_arrivo}



								<img style='width:18px' src='{$img_ps_dir}orange_alert.gif'  alt='{l s='Available from'} {$alternative.data_arrivo|date_format:"%d/%m/%Y"}' /><strong>{l s='Available from'} {$alternative.data_arrivo|date_format:"%d/%m/%Y"}</strong>
						
		{else}
		{/if}
		{else}	
						{if $alternative.date_available_msg != "" && $alternative.real_quantity <= 0}
							<img style='width:18px' src='{$img_ps_dir}orange_alert.gif'  alt='{l s='Available from'} {$alternative.date_available|date_format:"%d/%m/%Y"}' /><strong>{$alternative.date_available_msg}</strong>


						{else if $smarty.now|date_format:"%Y-%m-%d" < $alternative.date_available}
								<img style='width:18px'  src='{$img_ps_dir}orange_alert.gif'  alt='{l s='Available from'} {$alternative.date_available|date_format:"%d/%m/%Y"}' /><strong> {l s='Available from'} {$alternative.date_available|date_format:"%d/%m/%Y"}</strong>
						
						{else if $smarty.now|date_format:"%Y-%m-%d" > $alternative.date_available && $alternative.real_quantity <= 0 && $smarty.now|date_format:"%Y-%m-%d" < $alternative.data_arrivo}
								<img style='width:18px' src='{$img_ps_dir}orange_alert.gif'  alt='{l s='Available from'} {$alternative.data_arrivo|date_format:"%d/%m/%Y"}' /><strong>{l s='Available from'} {$alternative.data_arrivo|date_format:"%d/%m/%Y"}</strong>
							
						{else}
						
							{if $alternative.id_category_default == 119}
								<img style='width:18px' src='{$img_ps_dir}green_alert.gif' alt='{l s='Available'}' /> <strong>{l s='Available'}</strong>
								<span itemprop="availability" content="http://schema.org/InStock"></span>
								
							{else}
							
								{if $alternative.real_quantity > 0}
								
								{if $alternative.stock_quantity == 0 && $alternative.esprinet_quantity == 0 && $alternative.supplier_quantity == 0 && ($alternative.id_manufacturer != 27)}
										<img style='width:18px' src='{$img_ps_dir}green_alert.gif' alt='{l s='Available'}' /> <strong>{l s='Available'}</strong> <span itemprop="availability" content="http://schema.org/InStock"></span>: {if $alternative.id_manufacturer == 198} Tempi di spedizione 3-7 gg. {else} {$alternative.available_now} {/if}
								{else}
										<img style='width:18px' src='{$img_ps_dir}green_alert.gif' alt='{l s='Available'}' /> <strong>{l s='Available'}</strong>{if $product->id_category_default != 266}: {if $alternative.available_now != ''} {$alternative.available_now} {else} <!-- {l s='Shipping in 1/3 days'} --> {l s='shipping within'} {$data_spedizione|date_format:"%d/%m/%Y"} {/if} <span itemprop="availability" content="http://schema.org/InStock"></span>{if $alternative.id_manufacturer == 198}: Tempi di spedizione 3-7 gg. {/if}{/if}
								{/if}
								{else}
									{if preg_match("/fuori produzione/i", $alternative.description_short)}  
									{else}
										<p style="font-size: 13px; "><img style='width:25px' src='{$img_dir}yellow_alert_2.jpg'  alt='{l s='Ordinabile'}' />{l s='Prodotto ordinabile'}</p>
										{if $alternative.available_later != ''}
										{$alternative.available_later}
										{else}
										Attualmente non disponibile, richiedi quotazione con tempi di consegna <a href='https://www.ezdirect.it/preventivi-quotazioni-miglior-prezzo-cuffie-centralini'>qui</a>, oppure contatta il nostro staff allo 0585 821163
										{/if}
										<!-- - <a href="{$link->getPageLink('cms.php?id_cms=39')}">{l s='info'}</a> -->
									{/if}
								{/if}
							{/if}
						
						{/if}
						
					{/if}
					</p><br />
											<p style="padding-left:0px; height:140px; font-size:14px" class="description_short_alternatives">
											<a href="{$alternative.link|escape:'htmlall':'UTF-8'}#idTab5" rel="nofollow"><span class="stars">{$alternative.averageTotal}</span> </a>
											<br /><br />
											
											{$alternative.description_short|truncate:140:'...':true}
											
											</p>
											
											{if $alternative.available_for_order AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}
											<div class="acquista-listing-button">
											<a class="ajax_add_to_cart_button" name="{$alternative.id_product}" href="{$link->getPageLink('cart.php')}?qty=1&amp;id_product={$alternative.id_product|intval}&amp;token={$static_token}&amp;add" id="ajax_id_product_{$alternative.id_product|intval}" title="{l s='Add'} {$alternative.name|escape:'htmlall':'UTF-8'} {l s='to cart'}" style="float:none; margin:0 auto; width:80%">
											
											{l s='Add to cart'}
											
											</a>
											</div>
											{/if}
											<br />
										{/if}
										
									</div>
								</td>
							{if $smarty.foreach.alternatives_list.last} 
								</tr>
							{else}
								{if $j == 2}
									</tr>
								{assign var=j value="0"}
								{else}
								{assign var=j value=$j+1}
								{/if}	
							{/if}
							{/foreach}
							</table>
							
							
						
						</div>
						<div class="" style="display: block; text-align:center">
							
							<a style="  display:block; margin:0 auto; " href="tel:800529767"><img src='https://www.ezdirect.it/img/num-verde.jpg' alt="Numero verde" title="Numero verde 800529767" /></a><br />
							
							<a style="   display:block;  margin:0 auto; width:32%; border: 0;    border-radius: 3px;    -webkit-box-shadow: none;    box-shadow: none;    color: #fff;    cursor: pointer;    font-size: 17px;    font-weight: 500;      padding: 10px 32px; background-color: rgb(48, 133, 214); border-left-color: rgb(48, 133, 214); border-right-color: rgb(48, 133, 214);" onclick="window.location = '{$link->getCategoryLink($product->id_category_default,$link->getCatRewrite($product->id_category_default))}'">Cerca prodotti simili</a>
						
							</div>		
				</div>
		
	</div>
			
	{/if}	
	
</div>
		
<div class='codes'>	<!-- codici -->
			<meta itemprop="ratingValue" content="{$averageTotal}" />
			<meta itemprop="ratingCount" content="{$total_reviews}" />
	</div>
	
			
	
	
{if $isLoggedBackPrd == 1}
<br />
{else}{/if}


		<div class="prices">

		{if ($product->show_price AND !isset($restricted_country_mode)) OR isset($groups) OR $product->reference OR (isset($HOOK_PRODUCT_ACTIONS) && $HOOK_PRODUCT_ACTIONS)}
		
		<!-- add to cart form-->
		
		
		<form id="buy_block" {if $PS_CATALOG_MODE AND !isset($groups) AND $real_quantity > 0}class="hidden"{/if} action="{$link->getPageLink('cart.php')}" method="post">
		 <div itemprop="offers" itemscope="itemscope" itemtype="http://schema.org/Offer">
			<!-- hidden datas -->

				
				
				
				<input type="hidden" name="token" value="{$static_token}" />
				<input type="hidden" name="id_product" value="{$product->id|intval}" id="product_page_product_id" />
				<input type="hidden" name="product_quantity_available" value="{$real_quantity|intval}" />
				<input type="hidden" name="add" value="1" />
				<input type="hidden" name="id_product_attribute" id="idCombination" value="" />
		<!-- fine hidden datas -->

			
			<!-- prezzi -->
				{if $product->price == 0 || ($product->fuori_produzione == 1 && $real_quantity == 0)}
				{if $product->fuori_produzione == 1 || preg_match("/fuori produzione/i", $product->description_short)}  
				<br /><br />
				<span class="improve-your-search">{l s='This product is out of production'}.</span><br /><br />
			<a href="{$link->getCategoryLink($product->id_category_default,$link->getCatRewrite($product->id_category_default))}">{l s='Search for similar products'}</a>
			
				
				{else}
			<a rel="nofollow" href="https://www.ezdirect.it/preventivi-quotazioni-miglior-prezzo-cuffie-centralini?category={$product->id}" 
		onclick='var d = document.getElementById("quotation");
d.className = d.className + "selected";
<br /><br />
'>{l s='Ask for a quotation'}</a>
			{/if}
		
			
			{else}
			{if $product->show_price AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}
				
					{if !$priceDisplay || $priceDisplay == 2}
					{assign var='productPrice' value=$product->getPrice(true, $smarty.const.NULL)}
					{assign var='productPriceWithoutRedution' value=$product->getPriceWithoutReduct(false, $smarty.const.NULL)}
					{elseif $priceDisplay == 1}
						{assign var='productPrice' value=$product->getPrice(false, $smarty.const.NULL)}
						{assign var='productPriceWithoutRedution' value=$product->getPriceWithoutReduct(true, $smarty.const.NULL)}
					{/if}
					{if $product->on_sale}
						
					{elseif $product->specificPrice AND $product->specificPrice.reduction AND $productPriceWithoutRedution > $productPrice}
						
					{/if}
					
					
					{if $product->listino>0 && $product->listino>$product->getPrice(false, $smarty.const.NULL)}
			
			{else}
			
			
			{/if}
					
					
					{if $priceDisplay > 0 && $priceDisplay <= 2}
						
						{if $product->listino>0 && $product->listino>$product->getPrice(false, $smarty.const.NULL)}

						{/if}
					
						{if $product->listino>0 && $product->listino>$product->getPrice(false, $smarty.const.NULL)}
						
						{else}
					
						{/if}
					
						
							{if $tax_enabled}
								{if $priceDisplay == 1}{else}{l s='tax incl.'}{/if}
							{/if}
					{/if}
					
					{if $priceDisplay == 2}
					{if $product->price == 0  || ($product->fuori_produzione == 1 && $real_quantity == 0)}{else}	{convertPrice price=$product->getPrice(false, $smarty.const.NULL)} {l s='tax excl.'}{/if}
					{/if}
					
				{if $product->specificPrice AND $product->specificPrice.reduction}
					
					{if $priceDisplay >= 0 && $priceDisplay <= 2}
						{if $productPriceWithoutRedution > $productPrice}
							
								{if $tax_enabled}
									{if $priceDisplay == 1}<!-- {l s='tax excl.'} -->{else}{/if}
								{/if}
						{/if}
					{/if}
					
				{/if}
				{if $product->specificPrice AND $product->specificPrice.reduction_type == 'percentage'}
				<!-- {$product->specificPrice.reduction*100} -->
				{/if}
				
			
			
				{*close if for show price*}
			{/if}
			{/if}
			<!-- fine prezzi -->
		
 
 
 
<!-- sconti quantita -->	
<div id="prezzi-container">


	
	
	<ul id="prezzi-container-nav" style="position:absolute; list-style-type:none; display:inline; top:-19px; left:10px">
			<li id='przcont-prices'><h2><span class='przcont-a' style="text-align:center; margin:0; padding:0; text-decoration:none; font-style:normal;  cursor:pointer"><strong>{l s='Prices'}</strong></span></h2></li>
			<li><h2><span class='przcont-a' style="text-align:center; margin:0; padding:0; text-decoration:none; font-style:normal;  cursor:pointer"><strong>{l s='Quotation'}</strong></span></h2></li>
			<li><h2><span class='przcont-a' style="text-align:center; margin:0; padding:0; text-decoration:none; font-style:normal;  cursor:pointer"><strong>{l s='Payment'}</strong></span></h2></li>
			{if $isLoggedBackPrd == 1}
			<!-- VISUALIZZAZIONE IMPIEGATO -->
			<li><h2><a class='przcont-a' href="#prezzi-impiegato"><span class='przcont-a' style="text-align:center; margin:0; padding:0; text-decoration:none; font-style:normal; cursor:pointer" ><strong>Riservato</strong></span></a></h2></li>
			{else}{/if}
	</ul>
	
	<br /><br />
	<script type="text/javascript" src="{$js_dir}yetii.js"></script>
	

	
	<div class="tab" id="prezzi-prodotto">
	
		
		{if $product->price == 0  || ($product->fuori_produzione == 1 && $real_quantity == 0)}
		<span style="font-size:28px; color:#e62026; display:none" itemprop="price">0</span>
		{else}	
		
		<div id="quantityDiscount">
					<table class="tabella-sconti-quantita">
					<tr>
					<td>{if $product->listino > 0}Listino ({l s='t.e.'})</td><td style="text-align:right; width:50%">{convertPrice price=$product->listino} </td>{else}{/if}
					</tr>
					
					{if $product->price == 0  || ($product->fuori_produzione == 1 && $real_quantity == 0)}
					{else}
						{if $quantity_discounts}
							{if $product->listino > 0}
								<tr><td>{assign var="i" value="0"}
								{assign var=numItems value=$quantity_discounts|@count}
								
								{l s='You save up to'}</td> <td style="text-align:right"><strong>{convertPrice price=(round($product->listino,2) - round($firstPrice,2))}</strong></td></tr>
								
							{else}{/if}
						{else}
							{if $product->listino > 0 && $product->listino > $productPrice}
								{assign var="firstPrice" value=$productPrice|floatval}
								<tr><td>
								{l s='You save'}</td><td style="text-align:right"> <strong>{convertPrice price=(round($product->listino,2) - round($firstPrice,2))}</strong></span></td></tr>
							{else}
							{/if}
						{/if}
					{/if}
					
			{if $quantity_discounts}
					
					{foreach from=$quantity_discounts item='quantity_discount' name='quantity_discounts'}
						{if $j == 0}
					 
							<tr><td style="vertical-align:top">Prezzo</td>
							<td style="vertical-align:top; text-align:right; width:50%">
								
									<span itemprop="priceCurrency" content="EUR"></span>
									<meta itemprop="priceValidUntil" content="2099-12-31"></span>
									<span style="font-size:22px; " itemprop="price"><strong>{assign var="firstPrice" value=$productPrice - $quantity_discount.real_value|floatval} {convertPrice price=$productPrice} </strong></span> <!-- <span style="font-size:14px; color:#e62026">{l s='tax excl.'}</span> -->
									<br />
									
									{convertPrice price=$product->getPrice(true, $smarty.const.NULL)} {l s='tax incl.'}	
									
							</td></tr>
						{else}
						{/if}
							
						{$j = $j+1}
					{/foreach}
				


					<tr>
					<td style="font-weight:normal; border:0px" colspan="3">{l s='Price details'}</td><td></td>
					</tr>
					<tr>
					
					<td colspan="2">
						<table style="width:100%">
							<tr>
							{assign var="i" value="0"}
							{assign var=numItems value=$quantity_discounts|@count}
							
							<td style="color:#383838; border:0px">
								1 + {l s='pieces'}
							
							</td>
					
							{foreach from=$quantity_discounts item='quantity_discount' name='quantity_discounts'}
							
								{if $i == $numItems-1}
							 
									<td style="color:#383838; border:0px">{$quantity_discount.quantity|intval}
									+ {l s='pieces'}
						
								{else}	
									<td style="color:#383838; border:0px">{$quantity_discount.quantity|intval}
									+ {l s='pieces'}
								
								{/if}
				  
								
				  
								{$i = $i+1}
								</td>
							{/foreach}
							</tr>
							<tr>
							{assign var="j" value="0"}
							
							
					
						<td><strong style="font-size:14px">{convertPrice price=$productPrice}</strong>  {l s='t.e.'}
						<br />
						<span style='font-size:13px'>{convertPrice price=$product->getPrice(true, $smarty.const.NULL)} 
						{l s='t.i.'} </span> </td>
				
					
					
					{foreach from=$quantity_discounts item='quantity_discount' name='quantity_discounts'}
						<td style="vertical-align:bottom">
						
							<strong style="font-size:14px">{convertPrice price=$product->getPrice(false, $smarty.const.NULL) - ($product->getPrice(false, $smarty.const.NULL) * $quantity_discount.reduction)}</strong> {l s='t.e.'}
							<br />
							<span style='font-size:13px'>{convertPrice price=$product->getPrice(true, $smarty.const.NULL) - ($product->getPrice(true, $smarty.const.NULL) * $quantity_discount.reduction)} {l s='t.i.'}
							</span>
						</td>
					{/foreach}
					</tr>
						</table>
					</td></tr>	
					
					</table>
				</div>
			{else}
				
				
				<tr>
				<td style="vertical-align:top">Prezzo</td>
				<td style="vertical-align:top; text-align:right; width:50%"><span style="font-size:22px; "  itemprop="price"><strong>{convertPrice price=$productPrice} </strong></span>
				<span itemprop="priceCurrency" content="EUR"></span>
				<meta itemprop="priceValidUntil" content="2099-12-31"></span>
				<br />
				{convertPrice price=$product->getPrice(true)} {l s='tax incl.'}	
				</td>
				</tr>
				
				<tr>

				<td colspan="2" style="border:0px; font-size:14px; color:#e62026">
				{if $prezzospeciale == 1}
					{if $pezzi_offerta > 0}
						<strong style="color:#000000">{$pezzi_offerta}
								{if $pezzi_offerta == 1}
									{l s='piece'}</strong>
								{else}
									{l s='pieces '}</strong>
								{/if}
						<strong>{l s='in special price until '} {$scontofinoa}</strong>
					{else}
						<strong>{l s='Special price until'} {$scontofinoa}</strong>
					{/if}
				{else}
					<!-- <strong>{l s='Price'}</strong> -->
				{/if}
				
				</td></tr>
				</table>
				</div>
		
			{/if}
			<!-- fine sconti quantita -->
			
		{/if}
		
		
		<div class="carrello1">
<!-- quantita -->
	{if $product->price == 0  || ($product->fuori_produzione == 1 && $real_quantity == 0)}
		{else}
			
			
			
			
			
			<div id="quantity_wanted_p" {if !$product->available_for_order OR $PS_CATALOG_MODE} style="font-size:14px; display: none;"{else}style="font-size:14px;"{/if}><table><tr><td style="width:80%; text-align:left">{l s='Quantity'}</td><td style="width:20%">
				<div class="quantity_input_div">
					<input type="text" name="qty" id="quantity_wanted" class="text quantity_input" value="{if isset($quantityBackup)}{$quantityBackup|intval}{else}{if $product->minimal_quantity > 1}{$product->minimal_quantity}{else}1{/if}{/if}" size="2" maxlength="3" {if $product->minimal_quantity > 1}onkeyup="checkMinimalQuantity({$product->minimal_quantity});"{/if} />
					<div class="quantity_input_buttons">
						<span class="span_up" onclick="quantity_cart_add('quantity_wanted');">+</span>
						<span class="span_down" onclick="quantity_cart_rmv('quantity_wanted');">-</span>
					</div>
				</div>
				<div style="clear:both"></div>
				</td></tr></table>
			</div>

			
			<!-- minimal quantity wanted -->
			<p id="minimal_quantity_wanted_p" {if $product->minimal_quantity <= 1 OR !$product->available_for_order OR $PS_CATALOG_MODE} style="display: none;"{/if}>{l s='You must add '}<b id="minimal_quantity_label">{$product->minimal_quantity}</b>{l s=' as a minimum quantity to buy this product.'}</p>
			{if $product->minimal_quantity > 1}
			<script type="text/javascript">
				checkMinimalQuantity();
			</script>
		{/if}
			
<!-- fine quantita -->

	

{if $smarty.now|date_format:"%Y-%m-%d" > $product->date_available} 
	{if preg_match("/fuori produzione/i", $product->description_short)}  

	{else}
		{if $real_quantity <= 0}

			<br />
		{/if}

	{/if}
{else}
{if $real_quantity <= 0}

			<br />
		{/if}
{/if}
{if $product->listino > 0}
{else}
<br /><br />
{/if}

{if $product->login_for_offer == 1 && !$cookie->isLogged()}
					<a rel="nofollow" href="{$link->getPageLink('authentication.php', true)}"><strong style="background-color: #fff785; color:#ff0000" class="listing_titles">Accedi per vedere il prezzo scontato</strong></a>
					
				{/if}
				
				
				
<p>{if  !$product->available_for_order OR (isset($restricted_country_mode) AND $restricted_country_mode) OR $PS_CATALOG_MODE} style="display: none;"{/if} id="add_to_cart" class="buttons_bottom_block"><input type="submit" id="add_to_cart_button" name="{$product->id}" value="{l s='Add to cart'}" title="{l s='Add to cart'}" class="ajax_add_to_cart_button" /></p></div>
{/if}


	
	
	

<br /><br /><hr style="border:1px solid #e5e5e5;" /><br />
	<!-- disponibilita -->

				

{if $product->price == 0  ||($product->fuori_produzione == 1 && $real_quantity <= 0)}

{if $product->date_available_msg != "" && $real_quantity <= 0}
<p style='text-align:center'><img src='{$img_dir}orange_alert.png'  alt='{l s='Available from'} {$product->date_available|date_format:"%d/%m/%Y"}' /></p><p><img src='{$img_dir}icon-spedizione.png' alt='' title='' /> {l s='Data disponibilità'}: {$product->date_available_msg}</p>
{else}
{if $smarty.now|date_format:"%Y-%m-%d" < $product->date_available}
						<p style='text-align:center'><img src='{$img_dir}orange_alert.png'  alt='{l s='Available from'} {$product->date_available|date_format:"%d/%m/%Y"}' /></p><p><img src='{$img_dir}icon-spedizione.png' alt='' title='' /> {l s='Available from'} {$product->date_available|date_format:"%d/%m/%Y"}</p>

{else}
{/if}
{/if}
{if ($smarty.now|date_format:"%Y-%m-%d" > $product->date_available) && $real_quantity <= 0 && $smarty.now|date_format:"%Y-%m-%d" < $product->data_arrivo}



						<p style='text-align:center'><img src='{$img_dir}orange_alert.png'  alt='{l s='Available from'} {$product->data_arrivo|date_format:"%d/%m/%Y"}' /></p><p><img src='{$img_dir}icon-spedizione.png' alt='' title='' /> {l s='Available from'} {$product->data_arrivo|date_format:"%d/%m/%Y"}</p>
				
{else}
{/if}
{else}	
				{if $product->date_available_msg != "" && $real_quantity <= 0}
					<p style='text-align:center'><img src='{$img_dir}orange_alert.png'  alt='{l s='Available from'} {$product->date_available|date_format:"%d/%m/%Y"}' /></p><p><img src='{$img_dir}icon-spedizione.png' alt='' title='' /> {$product->date_available_msg}</p>


				{else if $smarty.now|date_format:"%Y-%m-%d" < $product->date_available}
						<p style='text-align:center'><img src='{$img_dir}orange_alert.png'  alt='{l s='Available from'} {$product->date_available|date_format:"%d/%m/%Y"}' /></p><p><img src='{$img_dir}icon-spedizione.png' alt='' title='' />  {l s='Available from'} {$product->date_available|date_format:"%d/%m/%Y"}</p>
				
				{else if $smarty.now|date_format:"%Y-%m-%d" > $product->date_available && $real_quantity <= 0 && $smarty.now|date_format:"%Y-%m-%d" < $product->data_arrivo}
						<p style='text-align:center'><img src='{$img_dir}orange_alert.png'  alt='{l s='Available from'} {$product->data_arrivo|date_format:"%d/%m/%Y"}' /></p><p><img src='{$img_dir}icon-spedizione.png' alt='' title='' /> {l s='Available from'} {$product->data_arrivo|date_format:"%d/%m/%Y"}</p>
					
				{else}
				
					{if $product->id_category_default == 119}
						<p style='text-align:center'><img src='{$img_dir}green_alert.png' alt='{l s='Available'}' /> </p><p><img src='{$img_dir}icon-spedizione.png' alt='' title='' /> Disponibilit&agrave;
						<span itemprop="availability" content="http://schema.org/InStock"></span>
						</p>
					{else}
					
						{if $real_quantity > 0}
						
						{if $product->stock_quantity == 0 && $product->esprinet_quantity == 0 && $product->supplier_quantity == 0 && ($product->id_manufacturer != 27)}
							<p style='text-align:center'>	<img src='{$img_dir}green_alert.png' alt='{l s='Available'}' /> </p><p><img src='{$img_dir}icon-spedizione.png' alt='' title='' /> Disponibilit&agrave; <span itemprop="availability" content="http://schema.org/InStock"></span>: {if $product->id_manufacturer == 198} Tempi di spedizione 3-7 gg. {else} {$product->available_now}</p> {/if}
						{else}
							<p style='text-align:center'>	<img src='{$img_dir}green_alert.png' alt='{l s='Available'}' /> </p><p><img src='{$img_dir}icon-spedizione.png' alt='' title='' /> Disponibilit&agrave;{if $product->id_category_default != 266}: {if $product->available_now != ''} {$product->available_now} {else} <!-- {l s='Shipping in 1/3 days'} --> {l s='shipping within'} {$data_spedizione|date_format:"%d/%m/%Y"} {/if} <span itemprop="availability" content="http://schema.org/InStock"></span> {if $product->id_manufacturer == 198}: Tempi di spedizione 3-7 gg. {/if}{/if}</p>
						{/if}
						{else}
							{if preg_match("/fuori produzione/i", $product->description_short)}  
							{else}
								 <p> <p style="font-size: 13px; "><img style='width:25px' src='{$img_dir}yellow_alert_2.jpg'  alt='{l s='Ordinabile'}' /> {l s='Prodotto ordinabile'}</p>
								{if $product->available_later != ''}
								<img src='{$img_dir}icon-spedizione.png' alt='' title='' />  {$product->available_later}
								{else}
								<img src='{$img_dir}icon-spedizione.png' alt='' title='' />  Attualmente non disponibile, richiedi quotazione con tempi di consegna <a href='https://www.ezdirect.it/preventivi-quotazioni-miglior-prezzo-cuffie-centralini'>qui</a>, oppure contatta il nostro staff allo 0585 821163
								{/if}
								<!-- - <a href="{$link->getPageLink('cms.php?id_cms=39')}">{l s='info'}</a> --></p>
							{/if}
						{/if}
					{/if}
				
				{/if}
				
			{/if}
	
		{if $product->id_category_default != 266}
			<p><img src='{$img_dir}icon-condizione.png' alt='' title='' />  {l s='Product condition'}: 
				{if $product->condition == 'new'}
				{l s='New'}
				{else if $product->condition == 'refurbished'}
				{l s='Refurbished'}
				{else}
				{l s='Used'}
				{/if}</p>
		{/if}
				
		{if $product->fuori_produzione == 1 || preg_match("/fuori produzione/i", $product->description_short)} 
			{else}
			
			{if $garanzia != ''}
						<p><img src='{$img_dir}icon-garanzia.png' alt='' title='' /> {l s='Warranty'}: {$garanzia}</p>
						{else}

						{/if}
						
						

			{if $cookie->id_default_group == 3 || $cookie->id_default_group == 12}{else}
						{if $freeshipping || $product->price > 4999999999999999}
						<p><strong style="color:#d60000">{l s='Free shipping'}</strong></p>

						{else}
						
						{/if}
			{/if}

			


		{/if}
		<div>
		<table style="width:100%"><td style="width:50%">
		{if $HOOK_EXTRA_RIGHT}
			{$HOOK_EXTRA_RIGHT}
			
			{/if}
		</td>
		
		<td style="width:50%; text-align:right">
			<!-- VISUALIZZAZIONE IMPIEGATO -->
			<script type="text/javascript">
			$(function() {
			$('#wishlist_img').tooltip({ showURL: false  });
			});
			</script>

			<a href="javascript:void(0)" id="wishlist_button" onclick="WishlistCart('wishlist_block_list', 'add', '{$id_product|intval}', $('#idCombination').val(), document.getElementById('quantity_wanted').value); fbq('track', 'AddToWishlist'); return false;"><img src='{$img_dir}new-wishlist.png' alt='Wishlist' id="wishlist_img" title="{l s='Clicca qui per aggiungere questo prodotto alla tua wishlist. Con la wishlist puoi creare un promemoria per acquisti futuri, o una lista da inviare a colleghi o amici che debbano acquistare i prodotti per conto tuo.' mod='blockwishlist'}" /></a>
			
			</td></tr></table>
			</div>
			<!-- fine disponibilita -->


<!-- prezzi v -->


<div class="availability">

</div>

	</div><!-- fine prezzi prodotto -->
	<div class="tab" id="quotation">
		<script type="text/javascript">
		$('document').ready(function() {
			$.ajax({
				type: 'POST',
				async: false,
				url: baseDir + 'payment-quotation-ajax.php',
				data: "payment_quotation=quotation&id_product={$product->id}",
				success: function(resp)
					{
						$('#quotation-content').append(resp);
					},
				error: function(XMLHttpRequest, textStatus, errorThrown)
					{
						// alert("TECHNICAL ERROR: unable to add the product.\n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
					}
								
			});
		});					
		</script>
		<div id="quotation-content">
		</div>
		<p><span style="color:0000ff">Ezdirect invia preventivo gratuito con garanzia del 100% (soddisfatto o rimborsato)</span>:<br />
		Per tutelare il nostro cliente, che ripone in noi la sua fiducia, &egrave; valida la garanzia "<strong>preventivo soddisfatto o rimborsato al 100%</strong>"*. La garanzia &egrave; valida per un periodo di <strong>15 giorni</strong> dalla data di spedizione. &Egrave; possibile sostituire il prodotto fornito con altro prodotto o richiedere il rimborso. Il rimborso include il 100% del valore dei beni (esclsui trasporti e servizi). I materiali resi dovranno essere in perfette condizioni, pari al nuovo (garanzia accessoria Ezdirect non disponibile per il "consumatore/privato", per il quale valgono i diritti di cui al codice del consumo).<br />
		(garanzia accessoria Ezdirect non disponibile per il "consumatore/privato", per il quale valgono i diritti di cui al codice del consumo).
		<br /><br />
		<strong>Condizioni: Riservato ad aziende, professionisti (P.IVA)</strong> che richiedono quotazione, compilando il modulo <a href='https://www.ezdirect.it/preventivi-quotazioni-miglior-prezzo-cuffie-centralini' target='_blank'>richiesta preventivo</a> e specificando nello stesso modulo esigenze e risorse.<br /><br />
		* (La garanzia 100% soddisfatto &egrave; valida solo per preventivi inviati da un consulente Ezdirect, su richieste ricevute da clienti che compilano il modulo richiesta preventivi, presente sul sito <a href='https://www.ezdirect.it' target='_blank'>www.ezdirect.it</a>. La garanzia non &egrave; valida per acquisti errati da parte del cliente senza aver richiesto una consulenza/preventivo scritto). Il cliente deve indicare in modo dettagliato le esigenze e le risorse disponibili. Fa fede quanto indicato nel preventivo inviato da Ezdirect in cui saranno presenti le esigenze scritte dal cliente e la proposta tecnico economica Ezdirect.
		</p>
	</div>
	<div class="tab" id="payment">
		<script type="text/javascript" src="{$js_dir}etc/payment_quotation.js">
</script>
		<div id="payment-content">
		</div>

	</div>
	{if $isLoggedBackPrd == 1}
		<div class="tab" id="prezzi-impiegato">
	
	
	

	<script type="text/javascript" src="{$js_dir}jquery.tooltip.js"></script>
	<script type="text/javascript">
	$(function() {
	$('.span-impiegato').tooltip({ showURL: false  });
	});
	</script>

	<table style='background-color:#ebebeb; font-size:14px'>
	<span class='span-impiegato' title="I prezzi principali. Il prezzo vendita &egrave; il prezzo a cui il cliente finale acquista il prodotto, senza calcolare eventuali sconti quantit&agrave; o sconti rivenditore se si tratta di un rivenditore. Il risparmio &egrave; dato dalla differenza tra prezzo di listino e prezzo di vendita."  style='cursor:pointer; text-decoration:underline'><strong>Listino e vendita</strong></span><br />
	<tr><th style='width:160px; text-align:right'>Prezzo listino</th>
	<th style='width:160px; text-align:right'>Prezzo vendita</th>
	<th style='width:160px; text-align:right'>Risparmio</th>

	<tr>
	<td style='text-align:right'><strong> {if $product->listino == 0} -- {else} {convertPrice price=$product->listino} {/if} </strong></td>
	<td style='text-align:right'><strong> {convertPrice price=$product->price} </strong></td>
	<td style='text-align:right'><strong> {if $product->listino == 0} -- {else} {convertPrice price=$product->listino-$product->price} {/if} </strong></td>

	</tr>
	</table>
	<br />

	<span class='span-impiegato' title="Gli sconti d'acquisto sono calcolati sul prezzo di listino. Sottratti al prezzo di listino d&agrave;nno come risultato il prezzo d'acquisto. La marginalit&agrave; &egrave; la nostra percentuale di guadagno sul prodotto. Diminuendo il prezzo di vendita, la marginalit&agrave; diminuisce e di conseguenza diminuisce il nostro guadagno."  style='cursor:pointer; text-decoration:underline'><strong>Sconti acquisto</strong></span>

	<br />
	<table style='background-color:#ffff00; font-size:14px'>
	<tr><th style='width:96px; text-align:right'>Sconto 1</th>
	<th style='width:96px; text-align:right'>Sconto 2</th>
	<th style='width:96px; text-align:right'>Sconto 3</th>
	<th style='width:96px; text-align:right'>Prezzo acq.</th>
	<th style='width:96px; text-align:right'>Marginalit&agrave;</th></tr>
	</tr>
	<tr>
	<td style='text-align:right'> {if $sc_acq_1 > 0} {$sc_acq_1}% {else} -- {/if} </td>
	<td style='text-align:right'> {if $sc_acq_2 > 0} {$sc_acq_2}% {else} -- {/if}</td>
	<td style='text-align:right'> {if $sc_acq_3 > 0} {$sc_acq_3}% {else} -- {/if}</td>
	<td style='text-align:right'> {if $netto_acq > 0} {convertPrice price=$netto_acq} {else} -- {/if}</td>
	<td style='text-align:right'> {((($product->price-$netto_acq)*100)/$product->price)|number_format:2:",":"."}%</td>
	</tr>
	</table>
	<br />

	<span class='span-impiegato' title="Gli quantit&agrave; sono sconti che si applicano sul prodotto se il cliente acquista una certa quantit&agrave; di questo prodotto. Il campo quantit&agrave; indica il numero minimo di pezzi di questo prodotto che il cliente deve acquistare per ottenere lo sconto abbinato."  style='cursor:pointer; text-decoration:underline'><strong>Sconti quantit&agrave;</strong></span><br />
	<table style='background-color:#99cc99; font-size:14px'>
	<tr><th style='width:60px; text-align:right'>Nr.</th>
	<th style='width:105px; text-align:right'>% sconto</th>
	<th style='width:105px; text-align:right'>Quantit&agrave;</th>
	<th style='width:105px; text-align:right'>Prezzo</th>
	<th style='width:105px; text-align:right'>Marginalit&agrave;</th>
	</tr>
	<tr>
	<td style='text-align:right'> 1 </td>
	<td style='text-align:right'> {if $sc_qta_1 > 0} {$sc_qta_1_rid}% {else} -- {/if}</td>
	<td style='text-align:right'> {if $sc_qta_1 > 0} {$sc_qta_1_pz} pz. {else} -- {/if}</td>
	<td style='text-align:right'> {if $sc_qta_1 > 0} {convertPrice price=$sc_qta_1} {else} -- {/if}</td>
	<td style='text-align:right'> {if $sc_qta_1 > 0} {((($sc_qta_1-$netto_acq)*100)/$sc_qta_1)|number_format:2:",":"."}% {else} -- {/if}</td>
	</tr>
	<tr>
	<td style='text-align:right'> 2 </td>
	<td style='text-align:right'> {if $sc_qta_2 > 0} {$sc_qta_2_rid}% {else} -- {/if}</td>
	<td style='text-align:right'> {if $sc_qta_2 > 0} {$sc_qta_2_pz} pz. {else} -- {/if}</td>
	<td style='text-align:right'> {if $sc_qta_2 > 0} {convertPrice price=$sc_qta_2} {else} -- {/if}</td>
	<td style='text-align:right'> {if $sc_qta_2 > 0} {((($sc_qta_2-$netto_acq)*100)/$sc_qta_2)|number_format:2:",":"."}% {else} -- {/if}</td>
	</tr>
	
	</table>

	
	
	<br />
	<span class='span-impiegato' title="Sono gli sconti riservati ai venditori. ATTENZIONE!!! Se esiste un prezzo derivante da uno sconto quanti&agrave; che risulta INFERIORE al prezzo applicato con sconto rivenditore, al rivenditore SI APPLICA il prezzo con sconto quantit&agrave; pi&ugrave; basso perch&eacute; al rivenditore si applica il prezzo pi&ugrave; basso sempre e comunque." style='cursor:pointer; text-decoration:underline'><strong>Sconti rivenditore</strong></span>
	<table style='background-color:#ff99ff; font-size:14px'>
	<tr><th style='width:60px'></th>
	<th style='width:140px; text-align:right'>% sconto</th>
	<th style='width:140px; text-align:right'>Prezzo</th>
	<th style='width:140px; text-align:right'>Marginalit&agrave;</th>
	</tr>
	<tr>
	<td style='text-align:right'> Riv.1 </td>
	<td style='text-align:right'> {if $sc_riv_1 > 0} {$sc_riv_1_rid}% {else} -- {/if}</td>
	<td style='text-align:right'> {if $sc_riv_1 > 0} {convertPrice price=$sc_riv_1} {else} -- {/if}</td>
	<td style='text-align:right'> {if $sc_riv_1 > 0} {((($sc_riv_1-$netto_acq)*100)/$sc_riv_1)|number_format:2:",":"."}% {else} -- {/if}</td>
	</tr>
	


	</table>
	<br />
	<!-- quantita impiegato -->
	<table style="font-size:14px">
	<tr>
	<td style="width:230px">Quantit&agrave; in magazzino EZ</td><td style="text-align:right"><strong>{$product->stock_quantity}</strong></td><td></td>
	</tr>
	
	<tr>
	<td>Quantit&agrave; Allnet {if $product->id_supplier == 11}<strong>(primario)</strong>{/if}</td><td style="text-align:right"><strong>{$product->supplier_quantity}</strong></td><td>({$product->arrivo_quantity} in arrivo)</td>
	</tr>
	
	<tr>
	<td>Quantit&agrave; Esprinet {if $product->id_supplier == 8}<strong>(primario)</strong>{/if}</td><td style="text-align:right"><strong>{$product->esprinet_quantity}</strong></td><td>({$product->arrivo_esprinet_quantity} in arrivo)</td>
	</tr>
	
	<tr>
	<td>Quantit&agrave; Itancia {if $product->id_supplier == 42}<strong>(primario)</strong>{/if}</td><td style="text-align:right"><strong>{$product->itancia_quantity}</strong></td><td></td>
	</tr>
	
		<tr>
	<td>Quantit&agrave; Log. Amazon </td><td style="text-align:right"><strong>{$amazon_quantity}</strong></td><td></td>
	</tr>
	
	<tr>
	<td>Quantit&agrave; totale</td><td style="text-align:right"><strong>{$product->quantity}</strong></td><td></td>
	</tr>
	
	<tr>
	<td>Impegnato <strong>(sito)</strong></td><td style="text-align:right"><strong>{$qta_ord_clienti}</strong></td><td></td>
	</tr>
	
	<tr>
	<td>Quantit&agrave; netta <strong>(sito)</strong></td><td style="text-align:right"><strong>{$real_quantity}</strong></td><td></td>
	</tr>
	
	<tr>
	<td>Impegnato <strong>(gestionale)</strong></td><td style="text-align:right"><strong>{$product->impegnato_quantity}</strong></td><td></td>
	</tr>
	
	<tr>
	<td>Quantit&agrave; netta <strong>(gestionale)</strong></td><td style="text-align:right"><strong>{$real_quantity_gst}</strong></td><td></td>
	</tr>
	
	</table>

	</div> <!-- fine prezzi impiegato -->
	<!-- FINE VISUALIZZAZIONE IMPIEGATO -->

		
	{else}{/if}
		<script type="text/javascript" src="{$js_dir}etc/tabber.js">
</script>
</div><!-- fine prezzi container -->








</div>
</form>

</div>



<!-- fine prezzi v -->

<div>

<br />
	
			
			
			
			
			<meta itemprop="brand" content="{$product->manufacturer_name}" />
			
		{else}
		{/if}
			
			
			

			
			
								
		
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		</div>	
			
				
		</div>	
		
		
			{if isset($groups)}
			<!-- attributes -->
			<div id="attributes">
			{foreach from=$groups key=id_attribute_group item=group}
			{if $group.attributes|@count}
			<p>
				<label for="group_{$id_attribute_group|intval}">{$group.name|escape:'htmlall':'UTF-8'} :</label>
				{assign var="groupName" value="group_$id_attribute_group"}
				<select name="{$groupName}" id="group_{$id_attribute_group|intval}" onchange="javascript:findCombination();{if $colors|@count > 0}$('#wrapResetImages').show('slow');{/if};">
					{foreach from=$group.attributes key=id_attribute item=group_attribute}
						<option value="{$id_attribute|intval}"{if (isset($smarty.get.$groupName) && $smarty.get.$groupName|intval == $id_attribute) || $group.default == $id_attribute} selected="selected"{/if} title="{$group_attribute|escape:'htmlall':'UTF-8'}">{$group_attribute|escape:'htmlall':'UTF-8'}</option>
					{/foreach}
				</select>
			</p>
			{/if}
			{/foreach}
			</div>
			{/if}

			
			
			{if $product->online_only}
				<p>{l s='Online only'}</p>
			{/if}

			
			{if isset($HOOK_PRODUCT_ACTIONS) && $HOOK_PRODUCT_ACTIONS}{$HOOK_PRODUCT_ACTIONS}{/if}
			<div class="clear"></div>
	
		
		{/if}
		
		
		</div>
	
			
			
			<div style="clear:both"></div>
			

<!-- description and features -->
{if $product->description || $features || $accessories || $HOOK_PRODUCT_TAB || $attachments}
<div id="more_info_block" class="clear" style="position:relative">
{if $features}


<a style="display:block; position:absolute; height:20px; width:110px; top:0px; left:135px; background-image:url({$img_ps_dir}linkscheda.gif)" href="#scheda" onclick='$(".idTabs").idTabs("idTab1");

			 $("html,body").animate({
				scrollTop: $("#scheda").offset().top - 0},
			1900);
			
'></a>
{/if}

<div id="itabs1">
	<ul id="" class="idTabs1 idTabsVeryShort"  style="font-weight:normal; margin-left:5px;">
	{if $product->description}<li onclick='{literal}$("html,body").animate({scrollTop: $("#idTab1").offset().top - 150},1900);{/literal}'><h2><span style="font-style:normal" id="more_info_tab_more_info" title="{l s='More info'}" rel="#idTab1" class="idTabHrefShort">{l s='More info'}</a></h2></li>{/if}
	
		{if $features}
		
		
		<li><h2><span id="schedalink" style="font-style:normal" title="{l s='Data sheet'}" rel="#schedina" class="idTabHrefShort">{l s='Data sheet'}</span></h2></li>
			
		{/if}
		
		{if !empty($bundles) && ($product->fuori_produzione == 0 || (($product->fuori_produzione == 1 && $product->price > 0 && $product->quantity > 0)))}
			<li onclick='{literal}$("html,body").animate({scrollTop: $("#bundles").offset().top - 260},1900);{/literal}'><h2><span id="tab_bundles" style="font-style:normal" title="{l s='Our bundles'}" rel="#bundles" class="idTabHrefShort">{l s='Our bundles'} ({$bundles|count})</span></h2></li>
		{/if}
		
		{if $attachments} <li onclick='{literal}$("html,body").animate({scrollTop: $("#idTab9").offset().top - 230},1900);{/literal}'><h2><span  style="font-style:normal; width:130px" id="more_info_tab_attachments" title="{l s='Download'}" rel="#idTab9" class="idTabHrefShort"><img style="position:relative; display:block; float:left; margin-top:0px; margin-left:5px; margin-right:5px" src="{$img_ps_dir}pdf_mini.png" alt="Download" title="Download" />  {l s='Download'} ({$attachments|count})</span></h2></li>{/if}
		{$HOOK_PRODUCT_TAB}
		{if $accessories}
			<li  onclick='{literal}$("html,body").animate({scrollTop: $("#accessories-carousel-int").offset().top - 230},1900);{/literal}'><h2><span id="tab_accessories" title="{l s='Accessories'}" rel="#accessories-carousel" class="idTabHrefShort" >{l s='Accessories'} ({$accessories|count})</span></h2></li>
		{/if}
		<!-- <li><span id="more_info_quotation" title="{l s='Quotation'}" rel="#quotation" 
		onclick='var d = document.getElementById("quotation");
d.className = d.className + "selected";

'>{l s='Quotation'}</span></li> -->
	</ul>
</div>

<br />
	<div id="more_info_sheets" class="sheets align_justify" style="margin-top:-15px">
	

	<div id="idTab1">
	{if $product_description}
		
		<!-- full description -->
		<!-- {if $isLoggedBackPrd == 1}
		{literal}
			 <script src="https://www.ezdirect.it/js/in-tinymce/js/tinymce/tinymce.min.js"></script>
  <script type="text/javascript">
  tinymce.init({
    selector: '#descrprodotto',
    inline: true,
	language: 'it',
	spellchecker_language: 'it',
	
	browser_spellcheck: true,
	 plugins: [
      'link', 'image', 'table',
      'textcolor',
      'lists',
      'contextmenu',
	  'spellchecker',
      'autolink',
    ],toolbar: [
      'undo redo | bold italic underline | fontselect fontsizeselect',
      'forecolor backcolor | alignleft aligncenter alignright alignfull | spellchecker numlist bullist outdent indent'
    ],
	open_manager_upload_path: './img/cms/',
	file_browser_callback: 'openmanager',
	spellchecker_languages: 'Italian=it',contextmenu: "copy paste cut link image | inserttable cell row column deletetable"

  });
  </script>
		{/literal}{/if} 
	-->	
		<div id='descrprodotto' class='descrprodotto'>
			<div class="rte">
			{$product_description|regex_replace:"/\\xC2\\x83/":" "|regex_replace:"/<div>/":" "|regex_replace:"/<div[^>]*>/":" "|regex_replace:"/<\/div>/":" "|regex_replace:"/<div class=\"display\">/":" "|regex_replace:"/<h1[^>]*?>/":"<h2 class='in-product'>"|regex_replace:"/<\/h1>/":"</h2>"|regex_replace:"/src=\"images/":"src=\"https://www.ezdirect.it/images"}
			
			{if $product->id_manufacturer == 198} <strong>Informazioni importanti sulla spedizione</strong>. Per proteggere al meglio la spedizione dei monitor e tutelare il bene fino alla sede del cliente (manlevandolo da rischi e responsabilità sul trasporto), i monitor LG  professionali, (a partire dai 40"), vengono consegnati dalla logistica di LG con trasporto assicurato (a  rischio di LG). <br />Questo servizio tutela il cliente da ogni rischio, ma richiede un tempo di spedizione di alcuni giorni (da 3 a 7 giorni circa).{/if}
			<p style='text-align:center'>
			{if $category_link != ''}<a href="{$category_link}">Vedi altri prodotti</a>{/if} {if $manufacturer_ext_link != ''}| <a href="{$manufacturer_ext_link}" target="_blank">Sito del costruttore</a>{else}{/if}
			</p>
			</div>
		</div>
		
	{/if} 
	{if $features}	
	
		{* Added by module for grouped features *}
		<!-- product's features -->
		
		{* Hello from exfeatures !!! *}
		
<hr style="border:none; background-color: #d4d4d4; height:1px" />
<br />
		<a id="scheda" class="anchor"></a><h2 style="background-color: #e6e6e6;padding: 6px;font-size: 18px;font-weight: normal; text-align:left">{l s='Data sheet'}</h2><br /><br />
		<ul id="idTab2" class="bullet">
		{foreach from=$features item=group}
		
		
		
            <li>
			<div style="background-color:#e6e6e6">
			
          <strong style="color:#383838; font-size:13px">{$group.name}</strong></div>
                <table class="features-table">
				{assign var='k' value='1'}
				
                {foreach from=$group.features item=feature} 
				
				{if $k%2 != 0}
				<tr class="even-f">
				{else}
				<tr class="odd-f" style="background-color:#f5f5f5">
				{/if}
				<td class="feature-name" style="width:300px">
			
    	           <strong>{$feature.name|escape:'htmlall':'UTF-8'}</strong></td>
				   
				   <td class="feature-value" style="text-align:left"> 
				   {$feature.value|escape:'htmlall':'UTF-8'|replace:'4*':'<img src="https://www.ezdirect.it/images/4.gif" alt="4" title="4" width="64" height="12" />'|replace:'5*':'<img src="https://www.ezdirect.it/images/5.gif" alt="5" title="5" width="64" height="12" />'|replace:'3*':'<img src="https://www.ezdirect.it/images/3.gif" alt="3" title="3" width="64" height="12" />'|replace:'2*':'<img src="https://www.ezdirect.it/images/2.gif" alt="2" title="2" width="64" height="12" />'|replace:'1*':'<img src="https://www.ezdirect.it/images/1.gif" alt="1" title="1" width="64" height="12" />'|replace:'s&igrave;':'<img src="https://www.ezdirect.it/images/si.gif" alt="s&igrave;" title="s&igrave;" width="14" height="14" />'}</td>
				</tr>
				{$k = $k+1}
				   {/foreach}
                </table><br />
            </li>
		{/foreach}
		</ul>
		{* End added by module for grouped features *}
	{/if}
	</div>
	
	{if $attachments}
		<hr style="border:none; background-color: #d4d4d4; height:1px" />
<br /><h2 style="background-color: #e6e6e6;padding: 6px;font-size: 18px;font-weight: normal; text-align:left" onclick="$('#idTab9').toggle();">Download</h2>
	<br /><br />
	
	<div id="idTab9">

		<ul class="bullet">
		{foreach from=$attachments item=attachment}
			<li><a href="{$link->getAttachmentLink2($attachment.id_attachment, $attachment.file_name)}">{$attachment.name|escape:'htmlall':'UTF-8'}</a><br />{if $attachment.name == $attachment.description}{else}{$attachment.description|escape:'htmlall':'UTF-8'}{/if}</li>
		{/foreach}
		</ul>
	</div>	
	{/if}
	
	{if !empty($bundles)}
	
	<hr style="border:none; background-color: #d4d4d4; height:1px" />
<br /><h2 style="background-color: #e6e6e6;padding: 6px;font-size: 18px;font-weight: normal; text-align:left">Kit</h2><br /><br />
	{l s='WARNING: kit\'s prices could change if kit\'s products have quantity discounts. Please enter the quantity to verify the price of the kit for the desired quantity. You can always change the quantity of your products by entering the cart summary'}
	<br /><br />
	<div id="bundles">
	<table style="width:98%; margin: 0 auto;">
	{foreach from=$bundles item=bundle name=bundle_list}
			<tr>
				<td style='border-bottom:1px dotted #d4d4d4; text-align:left'>
					<table class="bundle_positioning" style="position:relative">
							<tr>
						{$product->getBundlesDisplay($bundle.id_bundle)}
							</tr>
					</table>
					
					<a href="{$link->getBundleLink($bundle.id_bundle)}"  id="bundle_name_title_bundle-{$bundle.id_bundle}">{if $cookie->id_default_group == 3 || $cookie->id_default_group == 12}
							{$bundle.bundle_name|regex_replace:"/in omaggio/":""|regex_replace:"/omaggio/":""}
						{else}
							{$bundle.bundle_name}
						{/if}</a><br /><span style="color:#000000">(Codice:{$bundle.bundle_ref})</span><br class="bundle_positioning" /><br  class="bundle_positioning" />
				</td>
				<td class="bundle_equals" style='border-bottom:1px dotted #d4d4d4'>
				
					<img src="{$img_ps_dir}e_bundle.png" alt="" title="" />
				</td>
				<td style="text-align:center;width:190px;border-bottom:1px dotted #d4d4d4">
				
					{assign var='bundleprezzi' value=$product->getBundlesPrices($bundle.id_bundle)}
				
						
						{if $bundleprezzi.0 > $bundleprezzi.2}
						<table>
						<tr>
						<td>
						{l s='Starting from'}</td><td style='text-align:right'><span style="font-size:18px; color:#e62026">{convertPrice price=$bundleprezzi.2}</span><span style="font-size:13px; color:#e62026"></td><td>{l s='t.e.'}</td><td>{l s='for'} {$bundleprezzi.3}+ {l s='kits'}</td></tr>
						<tr>
						<td></td><td style='text-align:right'> {convertPrice price=$bundleprezzi.0}</td><td>{l s='t.e.'}</td><td>{l s='for 1 kit'}</td></tr>
						</table>
						{else}
						<span style="font-size:18px; color:#e62026">{convertPrice price=$bundleprezzi.0}</span> <span style="font-size:13px; color:#e62026">{l s='t.e.'}</span>
						{/if}
						 
					
					</td>
					<td style='margin-right:0px; vertical-align:center; text-align:right; border-bottom:1px dotted #d4d4d4; padding-top:-15px'>
					<div style="float:right">
						<div class="quantity_wanted_bdl" style="width:80px; text-align:center; display:block; margin-left:30px; margin-bottom:-10px">{l s='Quantity'}<br />
						
						<input type='hidden' value='https://www.ezdirect.it/img/b/{$bundle.id_image}-{$bundle.id_bundle}-small.jpg' id='bundle_img_link_bundle-{$bundle.id_bundle}' />
						
						<div class="quantity_input_div">
							<input type="text" name="qty_bdl_{$bundle.id_bundle}" id="qty_bdl_{$bundle.id_bundle}" class="text quantity_input quantity_bundle" value="{if isset($quantityBackup)}{$quantityBackup|intval}{else}1{/if}" size="2" maxlength="3" />
							<div class="quantity_input_buttons">
								<span class="span_up" onclick="quantity_cart_add('qty_bdl_{$bundle.id_bundle}');">+</span>
								<span class="span_down" onclick="quantity_cart_rmv('qty_bdl_{$bundle.id_bundle}');">-</span>
							</div>
						</div>
						<div style="clear:both"></div>
							
						</div><br />
						<div class="acquista-listing-button-product">		
							<form id="buy_block" {if $PS_CATALOG_MODE AND !isset($groups)}class="hidden"{/if} action="{$link->getPageLink('cart.php')}" method="post">
								<input type="hidden" name="bundle" value="{$bundle.id_bundle}" />
								<input type="submit" value="{l s='Add to cart'}" title="{l s='Add to cart'}" id="bundle_{$bundle.id_bundle}" class="ajax_add_to_cart_button" style="width:80px; background-image:none; background-color:#e62026; margin-left:30px; padding:5px" />
							</form>
						</div>
					</div>
				
				</td>
			</tr>
		{/foreach}
	</table>
	<script type='text/javascript' src="{$js_dir}bundle_quantity_preview.js"></script>
</div>
{/if}


{if $accessories}

<div id="accessories-carousel" style="border:0px; margin-top:10px; margin-left:-7px">
<hr style="border:none; background-color: #d4d4d4; height:1px" />
<br /><h2 style="background-color: #e6e6e6;padding: 6px;font-size: 18px;font-weight: normal; text-align:left">Accessori e prodotti simili</h2><br /><br />
				<div id="accessories-carousel-int">
					<table style="display:block; margin:0 auto" cellspacing="12">
					{assign var=j value=0}
					{foreach from=$accessories item=accessory name=accessories_list}
						{assign var='accessoryLink' value=$link->getProductLink($accessory.id_product, $accessory.link_rewrite, $accessory.category)}
						
						{if $j == 0}
							<tr>
						{else}
						{/if}
						
						
						<td>
							
							<div class="product_desc" style="margin-top:0px"><br />
								<a href="{$accessoryLink|escape:'htmlall':'UTF-8'}" title="{$accessory.name|escape:'htmlall':'UTF-8'}" class="product_image"><img src="{$link->getImageLink($accessory.link_rewrite, $accessory.id_image, 'medium')}" alt="{$accessory.name|escape:'htmlall':'UTF-8'}" width="{$mediumSize.width}" height="{$mediumSize.height}" /></a>
					
							</div>
							<div style='text-align:left'>
								<span style="font-size:13px; display:block; width:168px; height:50px"><a href="{$accessoryLink|escape:'htmlall':'UTF-8'}" title="{$accessory.name|escape:'htmlall':'UTF-8'}">{$accessory.name|truncate:70:'...':true|escape:'htmlall':'UTF-8'}</a></span>
								<p class="product_accessories_price" >
								
								{if $accessory.price == 0 || ($accessory.fuori_produzione == 1 && $accessory.real_quantity == 0)}
								{l s='Out of production'}
								{else}
									{if $accessory.quantity_discount && $accessory.prezzospeciale == 0}
					
										{assign var="i" value="0"}
										{assign var=numItems value=$accessory.quantity_discount|@count}
										{foreach from=$accessory.quantity_discount item='quantity_discount' name='product.quantity_discounts'}
										{if $i == $numItems-1}				

										{l s='From '} <span class="price" style="display: inline;"> {convertPrice price=$quantity_discount.price - ($quantity_discount.price * $quantity_discount.reduction)|floatval}</span> 
										{else}	
										{/if}
										{$i = $i+1}
										{/foreach}
					
									{else}
									{if isset($accessory.show_price) && $accessory.show_price && !isset($restricted_country_mode)}<span class="price" style="display: inline;">{if !$priceDisplay}{convertPrice price=$accessory.price}{else}{convertPrice price=$accessory.price_tax_exc}{/if}</span>{/if}

									{/if}

									</p>
									<p style="padding-left:0px; height:190px">
									<a href="{$accessory.link|escape:'htmlall':'UTF-8'}#idTab5" rel="nofollow"><span class="stars">{$accessory.averageTotal}</span> </a>
									<br /><br />
									
									{$accessory.description_short}
									</p>
									
									{if $accessory.available_for_order AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}
									 <div class="acquista-listing-button">
									<a class="ajax_add_to_cart_button" name="{$accessory.id_product}" href="{$link->getPageLink('cart.php')}?qty=1&amp;id_product={$accessory.id_product|intval}&amp;token={$static_token}&amp;add" id="ajax_id_product_{$accessory.id_product|intval}" title="{l s='Add'} {$accessory.name|escape:'htmlall':'UTF-8'} {l s='to cart'}" style="float:none; margin:0 auto; width:80%">
									
									{l s='Add to cart'}
									
									</a>
									</div>
									{/if}
									<br />
								{/if}
								
							</div>
						</td>
					{if $smarty.foreach.accessories_list.last} 
						</tr>
					{else}
						{if $j == 3}
							</tr>
						{assign var=j value="0"}
						{else}
						{assign var=j value=$j+1}
						{/if}	
					{/if}
					{/foreach}
					</table>
				</div>	
		</div>

{/if}
	



		<!-- accessories -->
		
			<div class="block products_block accessories_block clearfix">
			

	</div>
	<hr style="border:none; background-color: #d4d4d4; height:1px" />
<br /><h2 style="background-color: #e6e6e6;padding: 6px;font-size: 18px;font-weight: normal; text-align:left">Recensioni</h2><br /><br />
<div id="hook_product_tab_content">
	{$HOOK_PRODUCT_TAB_CONTENT}
	</div>
	</div>
</div>
{/if}

<br />{if $product->price == 0  || ($product->fuori_produzione == 1 && $real_quantity == 0)}
{else}
	<div id="cart_footer">
		
		<div class="col">
			<div class="acquista-listing-button">
				<span style="font-size:16px; color:#e62026">{convertPrice price=$product->getPrice(false, $smarty.const.NULL)}</span>
				<br />
				<div style="float:left; margin-left:16px; padding-top:4px">{l s='Quantity :'}</div>
								
				<div class="quantity_input_div">
					<input type="text" name="ajax_qty_to_add_to_cart[{$product->id|intval}]" id="quantity_wanted_{$product->id|intval}" class="text quantity_input" value="{if isset($quantityBackup)}{$quantityBackup|intval}{else}1{/if}" size="2" maxlength="3" {if $product->minimal_quantity > 1}onkeyup="checkMinimalQuantity({$product->minimal_quantity});"{/if} />
					<div class="quantity_input_buttons">
						<span class="span_up" onclick="quantity_cart_add('quantity_wanted_{$product->id|intval}');">+</span>
						<span class="span_down" onclick="quantity_cart_rmv('quantity_wanted_{$product->id|intval}');">-</span>
					</div>
				</div>
				<div style="clear:both"></div>
				<a style="margin-top:3px" class="ajax_add_to_cart_button" name="{$product->id}" href="{$link->getPageLink('cart.php')}?qty=1&amp;id_product={$product->id|intval}&amp;token={$static_token}&amp;add" id="ajax_id_product_{$product->id|intval}" title="{l s='Add'} {$product->name|escape:'htmlall':'UTF-8'} {l s='to cart'}">{l s='Add to cart'}</a>				
			</div>
			<div style="clear:both"></div>
		</div>
		<div class="col" style='margin-right:30px;'>
			
			<span style='font-size:14px'>{$product_category}</span>
			<h2 style='text-align:right; font-size:16px'>{$product->name}</h2>
		</div>
		<div style="clear:both"></div>
	</div>
{/if}

<!-- Customizable products -->
{if $product->customizable}
	<ul class="idTabs">
		<li><a style="cursor: pointer">{l s='Product customization'}</a></li>
	</ul>
	<div class="customization_block">
		<form method="post" action="{$customizationFormTarget}" enctype="multipart/form-data" id="customizationForm">
			<p>
				<img src="{$img_dir}icon/infos.gif" alt="Informations" />
				{l s='After saving your customized product, remember to add it to your cart.'}
				{if $product->uploadable_files}<br />{l s='Allowed file formats are: GIF, JPG, PNG'}{/if}
			</p>
			{if $product->uploadable_files|intval}
			<h2>{l s='Pictures'}</h2>
			<ul id="uploadable_files">
				{counter start=0 assign='customizationField'}
				{foreach from=$customizationFields item='field' name='customizationFields'}
					{if $field.type == 0}
						<li class="customizationUploadLine{if $field.required} required{/if}">{assign var='key' value='pictures_'|cat:$product->id|cat:'_'|cat:$field.id_customization_field}
							{if isset($pictures.$key)}<div class="customizationUploadBrowse"><img src="{$pic_dir}{$pictures.$key}_small" alt="" /><a href="{$link->getUrlWith('deletePicture', $field.id_customization_field)}"><img src="{$img_dir}icon/delete.gif" alt="{l s='Delete'}" class="customization_delete_icon" width="11" height="13" /></a></div>{/if}
							<div class="customizationUploadBrowse"><input type="file" name="file{$field.id_customization_field}" id="img{$customizationField}" class="customization_block_input {if isset($pictures.$key)}filled{/if}" />{if $field.required}<sup>*</sup>{/if}
							<div class="customizationUploadBrowseDescription">{if !empty($field.name)}{$field.name}{else}{l s='Please select an image file from your hard drive'}{/if}</div></div>
						</li>
						{counter}
					{/if}
				{/foreach}
			</ul>
			{/if}
			<div class="clear"></div>
			{if $product->text_fields|intval}
			<h2>{l s='Texts'}</h2>
			<ul id="text_fields">
				{counter start=0 assign='customizationField'}
				{foreach from=$customizationFields item='field' name='customizationFields'}
					{if $field.type == 1}
						<li class="customizationUploadLine{if $field.required} required{/if}">{assign var='key' value='textFields_'|cat:$product->id|cat:'_'|cat:$field.id_customization_field}
							{if !empty($field.name)}{$field.name}{/if}{if $field.required}<sup>*</sup>{/if}<textarea type="text" name="textField{$field.id_customization_field}" id="textField{$customizationField}" rows="1" cols="40" class="customization_block_input" />{if isset($textFields.$key)}{$textFields.$key|stripslashes}{/if}</textarea>
						</li>
						{counter}
					{/if}
				{/foreach}
			</ul>
			{/if}
			<p style="clear: left;" id="customizedDatas">
				<input type="hidden" name="quantityBackup" id="quantityBackup" value="" />
				<input type="hidden" name="submitCustomizedDatas" value="1" />
				<input type="button" class="button" value="{l s='Save'}" onclick="javascript:saveCustomization()" />
				<span id="ajax-loader" style="display:none"><img src="{$img_ps_dir}loader.gif" alt="loader" width="24" height="24" /></span>
			</p>
		</form>
		<p class="clear required"><sup>*</sup> {l s='required fields'}</p>
	</div>
{/if}

{if $packItems|@count > 0}
	<div>
		<h2>{l s='Pack content'}</h2>
		{include file="$tpl_dir./product-list.tpl" products=$packItems}
	</div>
{/if}


{$HOOK_PRODUCT_FOOTER}
<br />
<p style='text-align:center'>{l s='Product updated on'} {$product->date_upd}</p>
</div>
{if isset($smarty.cookies.jsCookiewarning29Check)}
	{if $product->price > 0}
	<script type="text/javascript">
	var google_tag_params = {
	ecomm_prodid: "{$product->id}",
	ecomm_pagetype: "product",
	ecomm_totalvalue: "{$product->price}",
	};
	</script>
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 1058514372;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1058514372/?value=0&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>
	{else}
	{/if}
{else}
{/if}


