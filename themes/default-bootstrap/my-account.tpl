{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}


{capture name=path}{l s='Il mio profilo'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<div style="padding: 25px 0; display: none;">
	{if ($cookie->customer_phone == '' && $cookie->customer_phone_mobile == '') || $cookie->customer_tax_code == '' || ($cookie->is_company == 1 && $cookie->customer_vat_number == '')}
	<strong style="color:red">{l s='WARNING'}</strong>
	<br />
	{l s='There are some missing required data in your account. If you do not fill them you will not able to order products on our website. Here there are the missing or not valid fields:'}
	<br /><br />
	{if $cookie->customer_phone == '' && $cookie->customer_phone_mobile == ''}
	{l s='Phone number'} - <a href="{$link->getPageLink('addresses.php', true)}">{l s='Click here to update your phone number'}</a><br />
	{/if}
	{if $cookie->customer_tax_code == ''}
	{l s='Tax code'} - <a href="{$link->getPageLink('identity.php', true)}">{l s='Click here to update your tax code'}</a><br />
	{/if}
	{if $cookie->is_company == 1}
	{if $cookie->customer_vat_number == ''}
	{l s='VAT number'} - <a href="{$link->getPageLink('identity.php', true)}">{l s='Click here to update your vat number'}</a><br />
	{/if}
	{/if}
</div>

<div style="display: flex; justify-content: space-between; padding-top: 30px;">
	<div id="left-column-profile">
		<div> 

			<div style="width: 80%; border-bottom: 1px solid #ddd9d9; margin-left: 20px;"><h4 id="my-profile-title">Area dati personali <span><button id="change-icon" onclick="myFunction()"  style="border:none;"><i class="fas fa-chevron-down"></i></button></span></h4></div>
		
			<ul id="myDIV">
				<li><a style="color: #545454" href="{$link->getPageLink('identity.php', true)}" title="">{l s='I miei dati personali'}</a></li>
				<li><a style="color: #545454" href="{$link->getPageLink('addresses.php', true)}" title="">{l s='I miei indirizzi'}</a></li>
				<li><a style="color: #545454" href="{$base_dir_ssl}modules/mytickets/tickets.php" title="">{l s='I miei ticket'}</a> </li>
				
			</ul>
		</div>

		<div> 
			<div style="width: 80%; border-bottom: 1px solid #ddd9d9; margin-left: 20px;"><h4 id="my-profile-title">Area ordini e offerte <span><button id="change-icon1" onclick="myFunction1()"  style="border:none;"><i class="fas fa-chevron-down"></i></button></span></h4></div>
			<ul id="myDIV1">
				<li><a style="color: #545454" href="{$link->getPageLink('history.php', true)}" title="">{l s='I miei ordini'}</a></li>
				<li><a style="color: #545454" href="{$base_dir_ssl}modules/mieofferte/offerte.php" title="">{l s='Le mie offerte'}</a></li>
				<li><a style="color: #545454" href="{$base_dir_ssl}modules/miefatture/fatture.php" title="">{l s='Le mie fatture'}</a></li>
				<li><a style="color: #545454" href="{$link->getPageLink('order-slip.php', true)}" title="">{l s='Le mie note di credito'}</a></li>
				<li><a style="color: #545454" href="{$link->getPageLink('discount.php', true)}" title="">{l s='I miei buoni'}</a> </li>
				<li><a style="color: #545454" href="{$base_dir_ssl}modules/loyalty/loyalty-program.php" title="">{l s='I miei punti fedeltà'}</a></li>
			</ul>
		</div>

		<div> 
			<div style="width: 80%; border-bottom: 1px solid #ddd9d9; margin-left: 20px;"><h4 id="my-profile-title">Area contatti <span><button id="change-icon2" onclick="myFunction2()" style="border:none;"><i class="fas fa-chevron-down"></i></button></span></h4></div>
			<ul id="myDIV2">
				<li><a style="color: #545454" href="{$link->getPageLink('contact-form.php', true)}?step=contabilita" title="">{l s='Amministrazione contabilità'}</a></li>
				<li><a style="color: #545454" href="{$link->getPageLink('contact-form.php', true)}?step=assistenza-ordini" title="">{l s='Amministrazione ordini'}</a></li>
				<li><a style="color: #545454" href="{$link->getPageLink('contact-form.php', true)}?step=tecnica" title="">{l s='Assistenza tecnica'}</a></li>
				<li><a style="color: #545454" href="{$link->getPageLink('cms.php?id_cms=36', true)}" title="">{l s='RMA'}</a></li>
				<li><a style="color: #545454" href="{$base_dir_ssl}modules/formprevendita/form.php" title="">{l s='Quotazioni'}</a></li>
				<li><a style="color: #545454" href="{$link->getPageLink('contact-form.php', true)}?step=rivenditori" title="">{l s='Rivenditori'}</a></li>
			</ul>
		</div>

		<div> 
			<div style="width: 80%; border-bottom: 1px solid #ddd9d9; margin-left: 20px;"><h4 id="my-profile-title">
				<a href="{$link->getPageLink('index.php')}?logout" id="logout-link" rel="nofollow" title="">{l s='Esci' mod='blockuserinfo'}</a>
			</h4></div>
			
		</div>

		<div> 
			<div style="width: 80%; margin-left: 20px;"><h4 id="my-profile-title" style="text-transform: none;">
				<a href="https://www.ezdirect.it/modules/cancellami/cancella-miei-dati.php">{l s='Cancella dati'}</a>
			</h4></div>
			
		</div>
	</div>

	<script>

		

		function myFunction() {

		$('#change-icon').find("i").toggleClass("fa-chevron-down fa-chevron-right");
		var x = document.getElementById("myDIV");
	
		if (x.style.display === "none") {
			x.style.display = "block";
		
		} else {
			x.style.display = "none";
		
		}
		}

		function myFunction1() {

		$('#change-icon1').find("i").toggleClass("fa-chevron-down fa-chevron-right");
		var x = document.getElementById("myDIV1");
		
		if (x.style.display === "none") {
			x.style.display = "block";
			
		} else {
			x.style.display = "none";
			
		}
		}

		function myFunction2() {

		$('#change-icon2').find("i").toggleClass("fa-chevron-down fa-chevron-right");
		var x = document.getElementById("myDIV2");
		
		if (x.style.display === "none") {
			x.style.display = "block";
			
		} else {
			x.style.display = "none";
			
		}
		}

		
	</script>

	<div id="center-col-my-profile">
		<div style="background-color: #f4f4f4; padding: 10px 20px; width: 100%; margin-bottom: 25px;">
		<h1 style='text-align:left; color: #000099; font-size: 25px; margin: 0; padding: 0;'>{l s='Il mio profilo'}</h1>
		<br />
		<h4 style="font-size: 14px; font-weight: normal; color: #62717C; line-height: 23px;">{l s='Benvenuto nella tua area personale! Qui potrai gestire i tuoi ordini, modificare dati personali, modificare e aggiungere indirizzi, gestire le comunicazioni con Ezdirect. '}</h4>

		<p><br /></p>
		<p style="color: #62717C;">CODICE CLIENTE: <strong>{$cookie->id_customer}</strong><span style='margin-left:13px;margin-right:13px'> | </span> REGISTRATO DAL: <strong>{dateFormat date=$data_reg full=0}</strong> <span style='margin-left:13px;margin-right:13px'> | </span>  EMAIL DI REGISTRAZIONE: <strong>{$cookie->email}</strong></p>
		<p></p>
		<br />

		<div><a href="{$link->getPageLink('index.php')}?logout" id="logout-link-responsive" rel="nofollow" title="">{l s='Esci' mod='blockuserinfo'}</a></div>

		</div>




		{* {else}

		{if $orders && count($orders)} *}

			<div class='my-account-block' >
			<h4 class="h4tab1" style="text-align:center;">{l s='Last orders'} <a style="font-size:11px; color:#ff6600;" href="{$link->getPageLink('history.php', true)}" title="">({l s='View all'})</a></h4>
			<table id="order-list" class="std" style="width:100%">
				<thead>
					<tr id="order-my-profile">
						<td><strong>{l s='Order'}</strong></th>
						<td><strong>{l s='Date'}</strong></th>
						<td><strong>{l s='Total price'}</strong></th>
						<td><strong>{l s='Payment'}</strong></th>
						<td><strong>{l s='Status'}</strong></th>
						<td><strong>{l s='Invoice'}</strong></th>
						<td style="width:65px">&nbsp;</strong></th>
					</tr>
				</thead>
				<tbody>
				{foreach from=$orders item=order name=myLoop}
					<tr class="{if $smarty.foreach.myLoop.first}first_item{elseif $smarty.foreach.myLoop.last}last_item{else}item{/if} {if $smarty.foreach.myLoop.index % 2}alternate_item{/if}">
						<td class="history_link bold">
							{if isset($order.invoice) && $order.invoice && isset($order.virtual) && $order.virtual}<img src="{$img_dir}icon/download_product.gif" class="icon" alt="{l s='Products to download'}" width="22" height="22" title="{l s='Products to download'}" />{/if}
							{l s='#'}{$order.id_order}
						</td>
						<td class="history_date bold">{dateFormat date=$order.date_add full=0}</td>
						<td class="history_price"><span class="price">{displayPrice price=$order.total_paid_real currency=$order.id_currency no_utf8=false convert=false}</span></td>
						<td class="history_method">{$order.payment|escape:'htmlall':'UTF-8'}</td>
						<td class="history_state">{if isset($order.order_state)}{$order.order_state|escape:'htmlall':'UTF-8'}{/if}</td>
						<td class="history_invoice">
						{if (isset($order.invoice) && $order.invoice && isset($order.invoice_number) && $order.invoice_number) && isset($invoiceAllowed) && $invoiceAllowed == true}
							<a href="{$link->getPageLink('pdf-invoice.php', true)}?id_order={$order.id_order|intval}" title="{l s='Invoice'}"><img src="{$img_dir}icon/pdf.gif" alt="{l s='Invoice'}" width="16" height="16" class="icon" /></a>
							<a href="{$link->getPageLink('pdf-invoice.php', true)}?id_order={$order.id_order|intval}" title="{l s='Invoice'}">{l s='PDF'}</a>
						{else}-{/if}
						</td>
						<td class="history_detail">
							
						</td>
					</tr>
				{/foreach}
				</tbody>
			</table>
			
			<br />
			</div>
		
		<div id="block-order-detail" class="hidden">&nbsp;</div>
		{else}
			
		{/if}

		{if $are_there_offers == 1}
		<div class='my-account-block' style='font-size:16px; padding-top:5px; padding-bottom:5px; text-align:center'> 
		<a href="{$link->getPageLink('modules/mieofferte/offerte.php', true)}"><img src="{$img_ps_dir}ci-sono-offerte-per-te.jpg" alt="{l s='There are offers for you'}" title="{l s='There are offers for you'}" class="icon" /></a>
		</div>
		{/if}
		
		<div class='account-areas'>
		<div class='my-account-block'> 
		<h4 class="h4tab1" style='text-align:center'>{l s='My addresses'} <a style="font-size:11px; color:#ff6600" href="{$link->getPageLink('addresses.php', true)}" title="">({l s='View all'})</a></h4>
		<div style='display:block; float:left; width: 250px; margin-left:15px; text-align:left'>
		<strong>{l s='Invoice address'}</strong><br /><br />
		<table class="my_account_table">
		<tr><td style="width:90px">{l s='Name'}</td><td><strong>{$fatturazione.firstname}</strong></td></tr>
		<tr><td>{l s='Last name'}</td><td><strong>{$fatturazione.lastname}</strong></td></tr>
		{if $fatturazione.is_company == 1}
		<tr><td>{l s='Company'}</td><td><strong>{$fatturazione.company}</strong></td></tr>
		<tr><td>{l s='VAT number'}</td><td><strong>{$fatturazione.vat_number}</strong></td></tr>
		{else}{/if}
		<tr><td>{l s='Tax code'}</td><td><strong>{$fatturazione.tax_code}</strong></td></tr>
		<tr><td>{l s='Address'}</td><td><strong>{$fatturazione.address1}</strong></td></tr>
		<tr><td>{l s='Zip'}</td><td><strong>{$fatturazione.postcode}</strong></td></tr>
		<tr><td>{l s='City'}</td><td><strong>{$fatturazione.city}</strong></td></tr>
		<tr><td>{l s='State'}</td><td><strong>{$fatturazione.iso_code}</strong></td></tr>
		<tr><td>{l s='Country'}</td><td><strong>{$fatturazione.country}</strong></td></tr>
		</table>
		</div>
		
		<div style='display:block; float:left; width: 250px; margin-left:15px; text-align:left'>
			<strong>{l s='Shipping address'}</strong><br /><br />
			<table class="my_account_table">
			<tr><td style="width:80px">{l s='Name'}</td><td><strong>{$spedizione1.firstname}</strong></td></tr>
			<tr><td>{l s='Last name'}</td><td><strong>{$spedizione1.lastname}</strong></td></tr>
			
			<tr><td>{l s='Address'}</td><td><strong>{$spedizione1.address1}</strong></td></tr>
			<tr><td>{l s='Zip'}</td><td><strong>{$spedizione1.postcode}</strong></td></tr>
			<tr><td>{l s='City'}</td><td><strong>{$spedizione1.city}</strong></td></tr>
			<tr><td>{l s='State'}</td><td><strong>{$spedizione1.iso_code}</strong></td></tr>
			<tr><td>{l s='Country'}</td><td><strong>{$spedizione1.country}</strong></td></tr>
			</table>
		</div>
		
		
			{* <div style='display:block; float:left; width: 230px; margin-left:15px; text-align:left'>
			<br /><br /><br /><br /><br /><br /><br /><br /><br /> *}
		{* <a href="{$link->getPageLink('addresses.php', true)}" title="">{l s='View all my addresses'}...</a> *}
		
			{* </div> *}
		
		
		
		<div style='clear:both'></div>
		<br />
		</div>
		
		{if $fido_tot > 0}
		<div class='my-account-block'> 
		<h4 class="h4tab1"  style='text-align:center'>{l s='Area finance'}</h4>
		
		<p style='text-align:center'>
		{l s='Hai un credito residuo di'}<br /><br />
		<span style="font-size:24px; {if $crediti_fido > 0} color:#0c8f2d; {else} {/if}">{$crediti_fido} &euro;</span><br /><br />
		Il tuo fido &egrave; di <span>{$fido} &euro;</span>
		</p>
			
		</div>
		
		{/if}
		
		<div class='my-account-block'> 
		<h4 class="h4tab1" style='text-align:center'>{l s='Account area'}</h4>
		<table style='width:100%'>
		<tr id="my-profile-tr">
			<td style='width:120px'>
			<a href="{$link->getPageLink('identity.php', true)}"><img width="36" height="36" src='{$img_dir}icon/account/user.png' alt="{l s='Information'}" title="{l s='Information'}" /></a><br />
			<a href="{$link->getPageLink('identity.php', true)}" title="{l s='Information'}">{l s='My personal information'}</a>
			</td>
			
			<td style='width:120px'>
			<a href="{$link->getPageLink('addresses.php', true)}"><img width="36" height="36" src='{$img_dir}icon/account/address.png' alt="{l s='Addresses'}" title="{l s='Addresses'}" /></a><br />
			<a href="{$link->getPageLink('addresses.php', true)}" title="{l s='Addresses'}">{l s='My addresses'}</a>
			</td>
			
			<td style='width:120px'>
			<a href="{$base_dir_ssl}modules/mieofferte/offerte.php"><img src='{$img_dir}icon/account/offerte.png' alt="{l s='My offers'}" width="36" height="36" title="{l s='My offers'}" /></a><br />
			<a href="{$base_dir_ssl}modules/mieofferte/offerte.php" title="{l s='My offers'}">{l s='My offers'}</a>
			</td>
			
			<td style='width:120px'>
			<a href="{$base_dir_ssl}modules/mytickets/tickets.php"><img width="36" height="36" src='{$img_dir}icon/account/ticket.png' alt="{l s='My tickets'}" title="{l s='My tickets'}" /></a><br />
			<a href="{$base_dir_ssl}modules/mytickets/tickets.php" title="{l s='My tickets'}">{l s='My tickets'}</a>
			</td>
		
			
			
		</tr>
		</table>
			
		</div>

		
		
			<div class='my-account-block'> 
		<h4 class="h4tab1" style='text-align:center'>{l s='Orders area'}</h4>
		<table style='width:100%'>
		<tr id="my-profile-tr">
			<td style='width:110px'>
			<a href="{$link->getPageLink('history.php', true)}"><img src='{$img_dir}icon/account/dettagli-ordini.png' alt="{l s='Orders'}" width="36" height="36" title="{l s='Orders'}" /></a><br />
			<a href="{$link->getPageLink('history.php', true)}" title="{l s='Orders'}">{l s='History and details of my orders'}</a>
			</td>
			
			
			
			
			
			<td style='width:110px'>
			<a href="{$base_dir_ssl}modules/miefatture/fatture.php"><img src='{$img_dir}icon/account/fatture.png' alt="{l s='My invoices'}" width="36" height="36" title="{l s='My invoices'}" /></a><br />
			<a href="{$base_dir_ssl}modules/miefatture/fatture.php" title="{l s='My invoices'}">{l s='My invoices'}</a>
			</td>
			
			<td style='width:110px'>
			<a href="{$link->getPageLink('order-slip.php', true)}"><img src='{$img_dir}icon/account/note.png' alt="{l s='Credit slips'}" width="36" height="36" title="{l s='Credit slips'}" /></a><br />
			<a href="{$link->getPageLink('order-slip.php', true)}" title="{l s='Credit slips'}">{l s='My credit slips'}</a>
			</td>
			
			<td style='width:110px'>
			<a href="{$link->getPageLink('discount.php', true)}"><img src='{$img_dir}icon/account/coupon.png' alt="{l s='Vouchers'}" width="36" height="36" title="{l s='Vouchers'}" /></a><br />
			<a href="{$link->getPageLink('discount.php', true)}" title="{l s='Vouchers'}">{l s='My vouchers'}</a>
			</td>
			{if $cookie->id_default_group == 3}
		{else}
			<td style='width:110px'>
			<a href="{$base_dir_ssl}modules/loyalty/loyalty-program.php"><img src='{$img_dir}icon/account/punti.png' alt="{l s='My loyalty points'}" width="36" height="36" title="{l s='My loyalty points'}" /></a><br />
			<a href="{$base_dir_ssl}modules/loyalty/loyalty-program.php" title="{l s='My loyalty points'}">{l s='My loyalty points'}</a>
			</td>
		{/if}
		</tr>
		</table>
		
		</div>

			<div class='my-account-block '> 
		<h4 class="h4tab1" style='text-align:center'>{l s='Contacts area'}</h4>
		<table style='width:100%; vertical-align:top'>
		<tr id="my-profile-tr">
			<td style='width:110px; vertical-align:top'>
			<a href="{$base_dir_ssl}modules/formprevendita/form.php"><img src='{$img_dir}icon/account/quotazione.png' title="{l s='Products quotation'}" width="36" height="36" alt="{l s='Products quotation'}" /></a><br />
			<a href="{$base_dir_ssl}modules/formprevendita/form.php" title="{l s='Products quotation'}">{l s='Products quotation'}</a>
			</td>
			
			<td style='width:110px; vertical-align:top'>
			<a href="{$link->getPageLink('contact-form.php', true)}?step=contabilita"><img src='{$img_dir}icon/account/amministrazione.png' width="36" height="36" title="{l s='Administrative assistance'}" alt="{l s='Administrative assistance'}" /></a><br />
			<a href="{$link->getPageLink('contact-form.php', true)}?step=contabilita" title="{l s='Administrative assistance'}">{l s='Administrative assistance'}</a>
			</td>
			
			<td style='width:110px; vertical-align:top'>
			<a href="{$link->getPageLink('contact-form.php', true)}?step=assistenza-ordini"><img src='{$img_dir}icon/account/ordini.png' width="36" height="36" title="{l s='Administrative assistance'}" alt="{l s='Orders assistance'}" /></a><br />
			<a href="{$link->getPageLink('contact-form.php', true)}?step=assistenza-ordini" title="{l s='Orders assistance'}">{l s='Orders assistance'}</a>
			</td>
			
			<td style='width:110px; vertical-align:top'>
			<a href="{$link->getPageLink('contact-form.php', true)}?step=tecnica"><img src='{$img_dir}icon/account/assistenza.png' width="36" height="36" title="{l s='Technical assistance'}" alt="{l s='Technical assistance'}" /></a><br />
			<a href="{$link->getPageLink('contact-form.php', true)}?step=tecnica" title="{l s='Technical assistance'}">{l s='Technical assistance'}</a>
			</td>
			
			<td style='width:110px; vertical-align:top'>
			<a href="{$link->getPageLink('cms.php?id_cms=36', true)}"><img src='{$img_dir}icon/account/reso.png' title="{l s='RMA'}" width="36" height="36" alt="{l s='RMA'}" /></a><br />
			<a href="{$link->getPageLink('cms.php?id_cms=36', true)}" title="{l s='RMA'}">{l s='RMA'}</a>
			</td>
		
		
			
			<td style='width:110px; vertical-align:top'>
			<a href="{$link->getPageLink('contact-form.php', true)}?step=rivenditori"><img src='{$img_dir}icon/account/riivenditori.png' width="36" height="36" title="{l s='Resellers'}" alt="{l s='Resellers'}" /></a><br />
			<a href="{$link->getPageLink('contact-form.php', true)}?step=rivenditori" title="{l s='Resellers'}">{l s='Resellers'}</a>
			</td>
		
		</tr>
		
		</table>
			
		</div>
		
		<div style="width: 100%; text-align:center;"> 
		
		<table style='width:100%; vertical-align:top'>
		<tr>
			<td style='width:110px; vertical-align:top'>
			<a href="https://www.ezdirect.it/modules/cancellami/cancella-miei-dati.php"><img width="36" height="36" src='{$img_dir}icon/account/delete.png' alt="{l s='Data deletion area'}" title="{l s='Data deletion area'}" /></a><br />
			
			<a href="https://www.ezdirect.it/modules/cancellami/cancella-miei-dati.php">{l s='Cancella i tuoi dati'}</a> <br /><br />
			</td>
		
		</tr>
		
		</table>
			
		</div>
		
		</div>
		
		
	</div>	
</div>


{* {/if} *}