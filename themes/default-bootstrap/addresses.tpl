{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{*
** Retro compatibility for PrestaShop version < 1.4.2.5 with a recent theme
*}

{* Two variable are necessaries to display the address with the new layout system *}
{* Will be deleted for 1.5 version and more *}
{if !isset($multipleAddresses)}
	{$ignoreList.0 = "id_address"}
	{$ignoreList.1 = "id_country"}
	{$ignoreList.2 = "id_state"}
	{$ignoreList.3 = "id_customer"}
	{$ignoreList.4 = "id_manufacturer"}
	{$ignoreList.5 = "id_supplier"}
	{$ignoreList.6 = "date_add"}
	{$ignoreList.7 = "date_upd"}
	{$ignoreList.8 = "active"}
	{$ignoreList.9 = "deleted"}	
	
	{* PrestaShop < 1.4.2 compatibility *}
	{if isset($addresses)}
		{$address_number = 0}
		{foreach from=$addresses key=k item=address}
			{counter start=0 skip=1 assign=address_key_number}
			{foreach from=$address key=address_key item=address_content}
				{if !in_array($address_key, $ignoreList)}
					{$multipleAddresses.$address_number.ordered.$address_key_number = $address_key}
					{$multipleAddresses.$address_number.formated.$address_key = $address_content}
					{counter}
				{/if}
			{/foreach}
		{$multipleAddresses.$address_number.object = $address}
		{$address_number = $address_number  + 1}
		{/foreach}
	{/if}
{/if}

{* Define the style if it doesn't exist in the PrestaShop version*}
{* Will be deleted for 1.5 version and more *}
{if !isset($addresses_style)}
	{$addresses_style.company = 'address_company'}
	{$addresses_style.vat_number = 'address_company'}
	{$addresses_style.firstname = 'address_name'}
	{$addresses_style.lastname = 'address_name'}
	{$addresses_style.address1 = 'address_address1'}
	{$addresses_style.address2 = 'address_address2'}
	{$addresses_style.suburb = 'address_suburb'}
	{$addresses_style.c_o = 'address_c_o'}
	{$addresses_style.city = 'address_city'}
	{$addresses_style.country = 'address_country'}
	{$addresses_style.phone = 'address_phone'}
	{$addresses_style.phone_mobile = 'address_phone_mobile'}
		{$addresses_style.fax = 'address_fax'}
	{$addresses_style.alias = 'address_title'}
{/if}

<script type="text/javascript" src="{$js_dir}etc/resizeAddressesBox.js">
</script>

{capture name=path}<a href="{$link->getPageLink('my-account.php', true)}">{l s='My account'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='My addresses'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<h1>{l s='My addresses'}</h1>
<p>{l s='Please configure the desired billing and delivery addresses to be preselected when placing an order. You may also add additional addresses, useful for sending gifts or receiving your order at the office.'}</p>

{if isset($multipleAddresses) && $multipleAddresses}
<div class="addresses" style="width:1000px">
	<h3>{l s='Your addresses are listed below.'}</h3>
	<p>{l s='Be sure to update them if they have changed.'}</p><br />
	{assign var="adrs_style" value=$addresses_style}
	
	{foreach from=$multipleAddresses item=address name=myLoop}
		{if $address.object.fatturazione == 1}
		<img src='{$img_dir}icon/fatturazione.gif' width="25" height="25" alt="{l s='Invoice address'}" title="{l s='Invoice address'}" style="display:block; float:left" />
		<strong>{l s='Invoice address'}</strong>
		<p>{l s='This is your invoice address. If you want to change your tax code and/or your VAT number, please go to your personal details page'}.</p>
			<p class="clear" ></p>
		<ul class="address">
			<li><strong>{l s='Address name:'} {$address.object.alias}</strong></li>
			<li>
			<table>
			{foreach from=$address.ordered name=adr_loop item=pattern}
				{assign var=addressKey value=" "|explode:$pattern}
				
				
				{foreach from=$addressKey item=key name="word_loop"}
				
				{if $address.formated[$key] == ''}
				{else}
					<tr><td style="width:80px; border-bottom:1px solid #d4d4d4">
					{$addresses_names[$key]}</td><td style="width:230px; border-bottom:1px solid #d4d4d4">{$address.formated[$key]|escape:'htmlall':'UTF-8'}</td></tr>
					
				{/if}
				{/foreach}
				</li>
			{/foreach}
			</table>
			
			<li class="address_update"><a href="{$link->getPageLink('address.php', true)}?id_address={$address.object.id|intval}" title="{l s='Update'}">{l s='Update'}</a></li>
			{if $address.object.fatturazione == 1}
			{else}
			<li class="address_delete"><a href="{$link->getPageLink('address.php', true)}?id_address={$address.object.id|intval}&amp;delete" onclick="return confirm('{l s='Are you sure?'}');" title="{l s='Delete'}">{l s='Delete'}</a></li>
			{/if}
		</ul>
		{else}
		{/if}
	{/foreach}
	
	
			<p class="clear" ></p>
		<img src='{$img_dir}icon/spedizione.gif' width="25" height="25" alt="{l s='Shipping addresses'}" title="{l s='Shipping addresses'}" style="display:block; float:left" />
	<strong>{l s='Shipping addresses'}</strong><br />
	<p style="padding-top: 7px;">{l s='Here you can find your delivery addresses. If your invoice address and your delivery address are the same, do not enter any address here. If your delivery address is different from your invoice address then click on "Add address" and add a new delivery address'}.</p>
	<br /><br />
	<div id="shipping_addresses">
	{foreach from=$multipleAddresses item=address name=myLoop}
		{if $address.object.fatturazione == 0}
		<ul class="address">
			<li><strong>{l s='Address name:'} {$address.object.alias}</strong></li>
			
			<li>
			<table>
			{foreach from=$address.ordered name=adr_loop item=pattern}
				{assign var=addressKey value=" "|explode:$pattern}
				
				{foreach from=$addressKey item=key name="word_loop"}
				{if $address.formated[$key] == ''}
				{else}
					<tr><td style="width:80px; border-bottom:1px solid #d4d4d4">
					{$addresses_names[$key]}</td><td style="width:230px; border-bottom:1px solid #d4d4d4">
						{$address.formated[$key]|escape:'htmlall':'UTF-8'} {/if}
					</td></tr>
				{/foreach}
			{/foreach}
			</table>
			</li>
			
			<li class="address_update"><a href="{$link->getPageLink('address.php', true)}?id_address={$address.object.id|intval}" title="{l s='Update'}">{l s='Update'}</a></li>
			<li class="address_delete"><a href="{$link->getPageLink('address.php', true)}?id_address={$address.object.id|intval}&amp;delete" onclick="return confirm('{l s='Are you sure?'}');" title="{l s='Delete'}">{l s='Delete'}</a></li>
			
		</ul>
		{else}
		{/if}
	{/foreach}
	</div>
	<p class="clear" >
	<br /><br />
</div>
{else}
	<p class="warning">{l s='No addresses available.'}</p>
{/if}

<div class="clear address_add" style="text-align: center;"><a  style="padding: 10px 15px; background-color: #ff6600;
    color: white;" href="{$link->getPageLink('address.php', true)}" title="{l s='Add an address'}" class="button_large">{l s='Add an address'}</a></div>

<div class="footer_links" style="display: flex; justify-content: space-between;">
	<div style="display: flex; align-items: center;"><a href="{$link->getPageLink('my-account.php', true)}"><img src="{$img_dir}icon/my-account.gif" width="22" height="22" alt="{l s='Back to Your Account'}" class="icon" /></a><a style="margin-bottom: 14px;" href="{$link->getPageLink('my-account.php', true)}">{l s='Back to Your Account'}</a></div>
	<div style="display: flex; align-items: center;"><a href="{$base_dir}"><img src="{$img_dir}icon/home.gif" alt="" class="icon" /></a><a a style="margin-bottom: 14px;" href="{$base_dir}">{l s='Home'}</a></div>
</div>