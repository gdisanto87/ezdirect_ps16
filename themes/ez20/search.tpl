{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='Search'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<!-- {if $smarty.server.PHP_SELF|basename != "search2.php"}<h1 {if isset($instantSearch) && $instantSearch}id="instant_search_results"{/if}>
{l s='Search'}&nbsp;{if $nbProducts > 0}"{if isset($search_query) && $search_query}{$search_query|escape:'htmlall':'UTF-8'}{elseif $search_tag}{$search_tag|escape:'htmlall':'UTF-8'}{elseif $ref}{$ref|escape:'htmlall':'UTF-8'}{/if}"{/if}
{if isset($instantSearch) && $instantSearch}<a href="#" class="close">{l s='Return to previous page'}</a>{/if}
</h1>{/if} -->
<h1>{l s='Advanced search filters'}</h1>

{include file="$tpl_dir./errors.tpl"}

{if $search_query|lower == 'centralino virtuale' || $search_query|lower == 'centralino' || $search_query|lower == 'virtuale' || $search_query|lower == 'centralini' || $search_query|lower == 'virtuali' || $search_query|lower == 'centralini virtuali' || $search_query|lower == 'ezcloud' || $search_query|lower == 'centralino ezcloud' || $search_query|lower == 'centralino virtuale ezcloud' || $search_query|lower == 'ezcloud centralino' || $search_query|lower == 'ezcloud centralini' || $search_query|lower == 'ezcloud centralini virtuali' || $search_query|lower == 'cnetralini' || $search_query|lower == 'cetralini' || $search_query|lower == 'cnetralino' || $search_query|lower == 'cetralino'}
	<p style='text-align:center'>
	<a href='https://www.ezdirect.it/centralino-virtuale/'><img src='https://www.ezdirect.it/img/cms/banner%20categoria%20ezcloud.jpg' alt='Centralino virtuale Ezcloud' title='Centralino virtuale Ezcloud' /></a>
	</p>
{/if}
	
{if !$nbProducts}
	<p class="warning">
		{if isset($search_query) && $search_query}
			{l s='No results found for your search'}&nbsp;"{if isset($search_query)}{$search_query|escape:'htmlall':'UTF-8'}{/if}"
		{elseif isset($search_tag) && $search_tag}
			{l s='No results found for your search'}&nbsp;"{$search_tag|escape:'htmlall':'UTF-8'}"
		{else}
			{l s='Please type a search keyword'}
		{/if}
	</p>
	
	
	<form action='{$smarty.server.REQUEST_URI}' name='estensione' method='get'>

	<input type='hidden' name='orderby' value='{$smarty.get.orderby}' />
	<input type='hidden' name='orderway' value='{$smarty.get.orderway}' />
	<input type='hidden' name='search_query' value='{$smarty.get.search_query}' />
	<input type='checkbox' value='on' name='nnn' {if $smarty.get.nnn == 'on'}checked='checked'{/if} />{l s='Product name'}
	<input type='checkbox' value='on' name='cod' {if $smarty.get.cod == 'on'}checked='checked'{/if} />{l s='Reference'}
	<input type='checkbox' value='on' name='sd' {if $smarty.get.sd == 'on'}checked='checked'{/if} />{l s='Short description'}
	<input type='checkbox' value='on' name='ld' {if $smarty.get.ld == 'on'}checked='checked'{/if} />{l s='Long description'}
	<input type='checkbox' value='on' name='td' {if $smarty.get.td == 'on'}checked='checked'{/if} />{l s='Technical details'}
	{if preg_match("/ or /", $smarty.get.search_query)}
	{else}
	<input type='checkbox' value='on' name='xctm' {if $smarty.get.xctm == 'on'}checked='checked'{/if} />{l s='Search for the exact match'}
	{/if}

	<input type='submit' name='submit_search' class='button_large' value='{l s='Extend your search'}' />
	</form>
	<br /><br />
	
{else}
	<!-- <h3><span class="big">{$nbProducts|intval}</span>&nbsp;{if $nbProducts == 1}{l s='result has been found.'}{else}{l s='results have been found.'}{/if}</h3> -->
	
	
	<form action='{$smarty.server.REQUEST_URI}' name='estensione' method='get'>

	<input type='hidden' name='orderby' value='{$smarty.get.orderby}' />
	<input type='hidden' name='orderway' value='{$smarty.get.orderway}' />
	<input type='hidden' name='search_query' value='{$smarty.get.search_query}' />
	<input type='checkbox' value='on' name='nnn' {if $smarty.get.nnn == 'on'}checked='checked'{/if} />{l s='Product name'}
	<input type='checkbox' value='on' name='cod' {if $smarty.get.cod == 'on'}checked='checked'{/if} />{l s='Reference'}
	<input type='checkbox' value='on' name='sd' {if $smarty.get.sd == 'on'}checked='checked'{/if} />{l s='Short description'}
	<input type='checkbox' value='on' name='ld' {if $smarty.get.ld == 'on'}checked='checked'{/if} />{l s='Long description'}
	<input type='checkbox' value='on' name='td' {if $smarty.get.td == 'on'}checked='checked'{/if} />{l s='Technical details'}
	{if preg_match("/ or /", $smarty.get.search_query)}
	{else}
	<input type='checkbox' value='on' name='xctm' {if $smarty.get.xctm == 'on'}checked='checked'{/if} />{l s='Search for the exact match'}
	{/if}

	<input type='submit' name='submit_search' class='button_large' style='width:110px' value='{l s='Extend your search'}' />
	</form>
	<br /><br />
	
	
	{if !isset($instantSearch) || (isset($instantSearch) && !$instantSearch)}
		<div class='block_listing_header'>
<div class='block-compare'>
{include file="$tpl_dir./product-compare.tpl"}
&nbsp;&nbsp;&nbsp;{include file="$tpl_dir./category-count.tpl"}
</div>
<div class='block-sort'>
				{include file="$tpl_dir./product-sort.tpl"}
</div>
</div>	
	{/if}
	{include file="$tpl_dir./product-list.tpl" products=$search_products}
	{include file="$tpl_dir./product-compare.tpl"}
<br /><br />
	{if !isset($instantSearch) || (isset($instantSearch) && !$instantSearch)}{include file="$tpl_dir./pagination.tpl"}{/if}
{/if}

<script type="text/javascript">
  {literal} fbq('track', 'Search', {search_string: '{/literal}{$smarty.get.search_query}{literal}'}); {/literal}
</script>