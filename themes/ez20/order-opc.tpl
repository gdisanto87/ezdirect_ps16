{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $smarty.now|date_format:"%Y-%m-%d" < "2019-08-19"}
{literal}
<script type="text/javascript">
				$(document).ready(function() {
					$("#hidden_link").fancybox().trigger("click");
				});
				</script>
			<a id="hidden_link" href="#data"></a>

			<div style="display:none"><div id="data">
			<p><br /><br /></p>
<p>
<strong>Gentile cliente, ti informiamo che le consegne per ordini effettuati sul nostro portale, saranno gestite a partire dal giorno 19 Agosto.
<br /><br />
Ci scusiamo per l'eventuale disagio.</strong></p><br /><br /></div></div>
{/literal}
{/if}
<div id="cart_env">

{if $PS_CATALOG_MODE}
	{capture name=path}{l s='Your shopping cart'}{/capture}
	{include file="$tpl_dir./breadcrumb.tpl"}
	<h2 id="cart_title">{l s='Your shopping cart'}</h2>
	<p class="warning">{l s='This store has not accepted your new order.'}</p>
{else}
<script type="text/javascript">
	// <![CDATA[
	var imgDir = '{$img_dir}';
	var authenticationUrl = '{$link->getPageLink("authentication.php", true)}';
	var orderOpcUrl = '{$link->getPageLink("order-opc.php", true)}';
	var historyUrl = '{$link->getPageLink("history.php", true)}';
	var guestTrackingUrl = '{$link->getPageLink("guest-tracking.php", true)}';
	var addressUrl = '{$link->getPageLink("address.php", true)}';
	var orderProcess = 'order-opc';
	var guestCheckoutEnabled = {$PS_GUEST_CHECKOUT_ENABLED|intval};
	var currencySign = '{$currencySign|html_entity_decode:2:"UTF-8"}';
	var currencyRate = '{$currencyRate|floatval}';
	var currencyFormat = '{$currencyFormat|intval}';
	var currencyBlank = '{$currencyBlank|intval}';
	var displayPrice = {$priceDisplay};
	var taxEnabled = {$use_taxes};
	var conditionEnabled = {$conditions|intval};
	var countries = new Array();
	var countriesNeedIDNumber = new Array();
	var countriesNeedZipCode = new Array();
	var vat_management = {$vat_management|intval};

	var txtWithTax = "{l s='(tax incl.)'}";
	var txtWithoutTax = "{l s='(tax excl.)'}";
	var txtHasBeenSelected = "{l s='has been selected'}";
	var txtNoCarrierIsSelected = "{l s='No carrier has been selected'}";
	var txtNoCarrierIsNeeded = "{l s='No carrier is needed for this order'}";
	var txtConditionsIsNotNeeded = "{l s='No terms of service must be accepted'}";
	var txtTOSIsAccepted = "{l s='Terms of service is accepted'}";
	var txtTOSIsNotAccepted = "{l s='Terms of service have not been accepted'}";
	var txtThereis = "{l s='There is'}";
	var txtErrors = "{l s='error(s)'}";
	var txtDeliveryAddress = "{l s='Delivery address'}";
	var txtInvoiceAddress = "{l s='Invoice address'}";
	var txtModifyMyAddress = "{l s='Modify my address'}";
	var txtInstantCheckout = "{l s='Instant checkout'}";
	var errorCarrier = "{$errorCarrier}";
	var errorTOS = "{$errorTOS}";
	var checkedCarrier = "{if isset($checked)}{$checked}{else}0{/if}";

	var addresses = new Array();
	var isLogged = {$isLogged|intval};
	var isGuest = {$isGuest|intval};
	var isVirtualCart = {$isVirtualCart|intval};
	var isPaymentStep = {$isPaymentStep|intval};
	//]]>
</script>

{if $cart_by_ezdirect == 1}
	{if $sta_usando_diverso == 1}
		<script type="text/javascript">alert("{l s='You have edited an offer made by Ezdirect'}.");</script>
	{/if}

{/if}

	{if $productNumber}
	
		{if $isLogged && $are_there_offers == 1}
		<div style="width:100%; display:inline-block; margin:0 auto">
		{if $preventivo == 1}
			<div class="box-carrello-img" style="font-size:18px; float:left; background-image:url({$img_ps_dir}id-offerta.jpg)">{$id_cart}-{$revisioni}</div>
			<div class="box-carrello-img" style="font-size:18px; float:left; background-image:url({$img_ps_dir}referente.jpg)">{$referente}</div>
		{/if}
		
			<!-- <a href="{$base_dir_ssl}modules/mieofferte/offerte.php" class="button_large" title="{l s='View all my offers'}" style="display:block; float:left; margin-top:10px; margin-bottom:20px; width:28%; padding-top:7px; height:23px"><img src='{$img_dir}button-icon/cart.png' alt='View all my offers' title='View all my offers' /><span>{l s='View all my offers'}</span></a> -->
			
			<!-- <a href="order-opc.php" class="button_large" title="{l s='View all my offers'}" style="display:block; float:left; margin-left:2%; margin-top:10px; margin-bottom:20px; width:28%; padding-top:7px; height:23px"><img src='{$img_dir}button-icon/update.png' alt='{l s='Refresh cart'}' title='{l s='Refresh cart'}' /><span>{l s='Refresh cart'}</span></a> -->
			
			<div class="box-carrello-img" style="font-size:18px; float:left; background-image:url({$img_ps_dir}vedi-le-offerte.jpg)"><a href="{$base_dir_ssl}modules/mieofferte/offerte.php" style="display:block;margin-top:-20px; text-decoration:none">{l s='View all my offers'}</a></div>
			
			<div class="box-carrello-img" style="font-size:18px; float:left; background-image:url({$img_ps_dir}aggiorna-offerta.jpg)"><a href="order-opc.php" style="display:block;margin-top:-20px; text-decoration:none">{l s='Refresh cart'}</a></div>
	
			
		</div>
	<div style="clear:both"></div>
	{/if}
	
		{if $premessa != ''}
		<div class="shopping_cart_table" {if !$opc}{else}style="width:98.3%; padding:5px; margin-left:1px"{/if}>
		<h2>Premessa</h2>
		{$premessa}
		</div><br /><div style="clear:both"></div>
		{/if}
		
		{if !isset($smarty.get.step)}
			<div id="title_cart" style="width:100%">
				<div style="float:left; width:19%">
				<h1 style="text-align:left; font-size:20px;">Il tuo carrello</h1>
				</div>
				<div id="title_cart_button" style="float:left; text-align:right; width:80%">
				</div>
				
			</div>
			<div style="clear:both"></div>
		{/if}
	
		<div class="shopping_cart_table" id="shopping_cart_steps" style="position:relative; display:block; margin:0 auto; width:99.2%; border:0px;
		
		{if !isset($smarty.get.step)}display:none{/if}
		">
		<div class="shopping_cart_header" style="z-index:1; height:35px; background-repeat:no-repeat;">
		<div id="order-step-1" style="background-color:#89c; width:32%; background-color:#89c;  display:block;padding-top: 7px; padding-bottom: 11px; margin-right:10px; float:left">
			<h2 style="color:#fff">{l s='Your data'}</h2>
		</div>
		<div id="order-step-2" style="width: 33%; display: block; float: left; background-color: #f4f4f4; color: #009; padding-top: 7px; padding-bottom: 11px;">
			<h2 style="color:#000099">Metodo di spedizione e pagamento</h2>
		</div>
		<div id="order-step-3" style="width:34.1%; display: block; float: left; background-color: #eee; color: #009; padding-top: 7px; padding-bottom: 11px;">
			<h2 style="color:#000099">Rivedi il tuo ordine</h2>
		</div>
		</div><div class="clear:both"></div>
		
			<div id="opc-account" style="width:32%; display:block; float:left;">
			 <h2 class="shopping-h2">1. {l s='Your data'}</h2>
				{if $isLogged AND !$isGuest}
					{include file="$tpl_dir./order-address-quick.tpl"}
				{else}
					<!-- Create account / Guest account / Login block -->
					{include file="$tpl_dir./order-opc-new-account.tpl"}
					<!-- END Create account / Guest account / Login block -->
				{/if}
			</div>
			<div id="opc-shipping" style="width:33%; padding-left:10px; padding-right:10px; position:relative; display:block; float:left">
			<form action="{if !isset($smarty.get.step)}https://www.ezdirect.it/ordine-veloce?step=2{else}order-payment.php{/if}" method="post">
				<h2 class="shopping-h2">2. {l s='Delivery methods'}</h2> 
				<!-- Carrier -->
				{include file="$tpl_dir./order-carrier-quick.tpl"}
				<!-- END Carrier -->
				{if $check_fido == 'no'}
				
				{else}
					<div style="display:none" id="check_fido">
					{if ($orderTotal > $crediti_fido)}
					
					{assign var="stop_fido" value="true"}
					
					{else}
					{/if}
					</div>
				{/if}
						{if $cfcheck == 0}
		<p><br />
		
		<div style="position:relative;height:350px; "><span style="color:red">	{l s='There are some details missing in your account: if you are a end user customer you must specify your tax code in your account details, and if you are a company you must specify both tax code and vat number. You must also specify a valid address. Please click on the single details to fix them'}.</span><br />
			{l s='Here there are your missing details'}: <br />
			{if $check_tx == 1} <a href="{$link->getPageLink('identity.php', true)}" target="_blank">{l s='Tax code'}</a> | {else}{/if}
			{if $check_vat == 1} <a href="{$link->getPageLink('identity.php', true)}" target="_blank">{l s='VAT number'}</a> | {else}{/if}
			{if $check_phone == 1} <a href="{$link->getPageLink('address.php', true)}?id_address={$id_address|intval}" target="_blank">{l s='Phone number'}</a> | {else}{/if}
			{if $check_address == 1} <a href="{$link->getPageLink('address.php', true)}?id_address={$id_address|intval}" target="_blank">{l s='Address'}</a> | {else}{/if}
			{if $check_postcode == 1} <a href="{$link->getPageLink('address.php', true)}?id_address={$id_address|intval}" target="_blank">{l s='Postcode'}</a> | {else}{/if}
			{if $check_city == 1} <a href="{$link->getPageLink('address.php', true)}?id_address={$id_address|intval}" target="_blank">{l s='City'}  |</a>{else} {/if}
			{if $check_state == 1} <a href="{$link->getPageLink('address.php', true)}?id_address={$id_address|intval}" target="_blank">{l s='State'}  |</a>{else} {/if}
			{if $check_country == 1} <a href="{$link->getPageLink('address.php', true)}?id_address={$id_address|intval}" target="_blank">{l s='Country'} |</a>{else} {/if}
			</div>
			{else if $cfcheck == 2}
			{l s='You must login to continue'}
			{else}
			{/if}
			<div style="clear:both"></div>
			</div>
			
			<script type="text/javascript">
				{if $validity_check == 1}
					alert("{l s='Sorry, your offer has expired. Please contact our staff at'} 0585 821163");
				{/if}
				
				{literal}
				function controlla_termini() {
				{/literal}			
				
				{if ($payment_method_cart == "R.B. 30 GG. D.F. F.M." || $payment_method_cart == "R.B. 60 GG. D.F. F.M." || $payment_method_cart == "R.B. 60 GG. D.F. F.M." || $payment_method_cart == "Bonifico 30 gg. fine mese" || $payment_method_cart == "Bonifico 60 gg. fine mese" || $payment_method_cart == "Bonifico 90 gg. fine mese")}
				
					{if ($check_fido == 'yes' && $orderTotal > $crediti_fido)}
					{literal}
						if(document.getElementById('other_payment_p').checked) {
							$('#check_fido').show();
							$.fancybox('Gentile Cliente, non &egrave; possibile gestire questo ordine con pagamento differito.<br /><br />Ti invitiamo a comunicare con il nostro ufficio amministrativo per i chiarimenti del caso.<br /><br />Chiama lo 0585821163 (selezione 4), oppure apri un ticket <a href="https://www.ezdirect.it/contattaci?step=contabilita">qui</a>',
								 {
									
									'autoScale': true,
									 'transitionIn': 'elastic',
									 'transitionOut': 'elastic',
									 'speedIn': 100,
									 'speedOut': 100,
									 'autoDimensions': true,
									 'centerOnScroll': true,
									 'overlayShow': true
									 
								 });
							return false;
						}
						else
						{
							$('#check_fido').hide();
						}
					{/literal}
					{else}
					{/if}
				{else}
				{/if}
				{if $validity_check == 1}
					alert("{l s='Sorry, your offer has expired. Please contact our staff at'} 0585 821163");
					return false;
				{/if}

				 {if !isset($carriers) || !$carriers || count($carriers) == 0}
					alert("{l s='Sorry, shipping is currently not available for your country. Please contact our staff at'} 0585 821163");
					return false;
				 {/if}
				 
				 {literal}
					if ($('input[name=payment]:checked').length > 0) {
						
					}
					else {
						
						$('#payment_notice').show();
						$('html, body').animate({
							scrollTop: ($("#payment_notice").offset().top)-155
						}, 300);
						alert("{/literal}{l s='You must choose a payment method before continue'}. {l s='If you have problems please contact our customer service'}{literal}");
							return false;
					
					}
					
				
					
					if(document.getElementById('cgv').checked) {

					}
						
						
					else {
						$('#cgv_notice').show();
						$('html, body').animate({
							scrollTop: ($("#cgv_notice").offset().top)-155
						}, 300);
						alert("{/literal}{l s='You must accept our terms before continue'}. {l s='If you have problems please contact our customer service'}{literal}");
						return false;
					}
				
					
					fbq('track', 'InitiateCheckout');
			

				}
				
				{/literal}
			</script>
			
			<div id="opc-payment" style="width:31.5%; display:block; float:left; padding-left:10px;  ">
				<h2 class="shopping-h2">3. {l s='Payment methods'}</h2>
				<!-- Payment -->
				{include file="$tpl_dir./order-payment-quick.tpl"}
				<!-- END Payment -->
				

			</div>
			
			<div style="clear:both"></div>
			
		
			
			
			
			
			
			<script type="text/javascript">$('a.iframe').fancybox();</script>
			
			<div style="clear:both"></div>
			
		
			<div style="clear:both"></div>
		</div>
		<div style="clear:both"></div>
		<!-- Shopping Cart -->
		<br />
		
		<div id="HOOK_SHOPPING_CART" style="border:0px">{$HOOK_SHOPPING_CART}</div>
		
		<br />
		
			{include file="$tpl_dir./shopping-cart-quick.tpl"}
		
		<!-- End Shopping Cart -->
	{else}
		{capture name=path}{l s='Your shopping cart'}{/capture}
		{include file="$tpl_dir./breadcrumb.tpl"}
		<h2>{l s='Your shopping cart'}</h2>
		<div style="margin-top:5px; text-align:center; margin:0auto">
			<!-- <div class="block myaccount" style=" display: inline-block; height:71px; overflow:hidden;">
			<a href="order-opc.php"><img src="{$base_dir_ssl}/img/aggiorna-carrello.gif" title="{l s='Refresh cart'}" alt="{l s='Refresh cart'}" /></a>
			</div> -->
			
			{if $are_there_offers == 1}
			<div class="block myaccount" style=" display: inline-block; height:71px; overflow:hidden;">
			<a href="{$link->getPageLink('modules/mieofferte/offerte.php', true)}?id_cart={$ultima_offerta}&id_customer={$cookie->id_customer}"><img src="{$base_dir_ssl}/img/clicca-per-preventivo.gif" title="{l s='My offers' mod='blockpreventivi'}" alt="{l s='My offers' mod='blockpreventivi'}" /></a>
			</div>

			{/if}
			<div style="clear:both"></div>
		</div>	
		<p class="warning" style="margin-left:0px; margin-right:0px">{l s='Your shopping cart is empty.'}</p>
		
			
		
		
		<div class="shopping_cart_table" style="height:127px; position:relative; margin-left:0px; margin-top:-7px;">
<div style="display:block;float:left;">
<img src="{$img_ps_dir}contatta-servizio-clienti.gif" alt="Customer care 0585 821163" width="122" height="127" />
</div>
<div style="display:block;float:left">
<a href="tel:+39-0585091298"><img src="{$img_ps_dir}se-hai-bisogno-di-aiuto.gif" alt="{l s='Se hai bisogno di aiuto per il tuo ordine, contatta il servizio clienti al numero 0585 821163'}" width="602" height="90" /></a><br />
<a target='_blank' href='https://www.ezdirect.it/contattaci?step=assistenza-ordini'><img src="{$img_ps_dir}oppure-apri-ticket.gif" width="454" height="15" alt="{l s='Oppure apri un ticket cliccando qui'}" style="border:0px" /></a>
</div>
<p class="clear"></p>
</div>
		<div style="clear:both"></div></div>
	{/if}
{/if}



<div><div>


{if !isset($smarty.get.step)}
<script type="text/javascript">
	var element = $('#payment_button').detach();
	$('#title_cart_button').append(element);
</script>
{else}
<script type="text/javascript">
	var element = $('#payment_button_shopping').detach();
	$('#cart_summary_mini_div_button').append(element);
	document.getElementById('payment_button_shopping').style.position = 'relative';
	document.getElementById('payment_button_shopping').style.width = '100%';
</script>
{/if}
