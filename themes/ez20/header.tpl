<!DOCTYPE html>
<html lang="{$lang_iso}-{$lang_iso|upper}" dir="ltr">
	<head>
		<title>{$meta_title|escape:'htmlall':'UTF-8'}</title>
		
		<!-- Google Tag Manager -->
		{literal}<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-TK28N4X');</script>{/literal}
		<!-- End Google Tag Manager -->

		{if $id_cms_category != 7}
		
		{if isset($meta_description) AND $meta_description}
			<meta name="description" content="{$meta_description|escape:html:'UTF-8'}" />
		{/if}
		{if isset($meta_keywords) AND $meta_keywords}
			<meta name="keywords" content="{$meta_keywords|escape:html:'UTF-8'}" />
		{/if}

		{if isset($noindex_nofollow) && $noindex_nofollow == 'y'}
			<meta name="robots" content="noindex,nofollow" />
		{else}
			{if $lang_iso == 'it'}
				<meta name="robots" content="noindex,nofollow" />
			{else}
				<meta name="robots" content="noindex,nofollow" />
			{/if}

		{/if}
		
		<meta name="viewport" content="width=device-width, height=device-height, minimum-scale=1, maximum-scale=1, initial-scale=1.0">
	
	
		{* <link rel="preconnect" href="https://fonts.gstatic.com"> *}
		{* <link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">  *}

		<!--FONT MONTSERRAT-->

		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
		
		<!--Bootstrap 4.3 e 3.4-->
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet" />
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
		
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>


		<!--CSS GLOBAL-->
		<link rel="stylesheet" href="{$css_dir}global.css">

		<!--FONTAWESOME-->

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

		<meta charset="utf-8" />
		{if isset($canonical) AND $canonical}
			<link rel="canonical" href="{$link->getProductLink($canonical, $canonical_rew, $canonical_cat)}" />
		{else}
			{if isset($cat_canonical) AND $cat_canonical}
			{else if isset($cat_canonicalz) AND $cat_canonicalz}
			
				<link rel="canonical" href="{$cat_canonicalz}" />
			
			{else}
			{/if}
			
			{if isset($cat_canonicalz) AND $cat_canonicalz}
			
				<link rel="canonical" href="{$cat_canonicalz}" />
			
			{else}
			{/if}
		{/if}

		{if strpos($smarty.server.REQUEST_URI, "?") || strpos($smarty.server.REQUEST_URI, "_")}
			{assign var='nobots' value=true}
		{/if}
		
		<link rel="shortcut icon" type="image/x-icon" href="{$img_ps_dir}favicon.ico?{$img_update_time}" />
		<link rel="author" href="https://plus.google.com/105789606735008987399?rel=publisher" />
		<link rel="alternate" type="application/rss+xml" title="Ezdirect.it le novit&agrave;!"  href="https://www.ezdirect.it/rss/" />
		{if isset($smarty.get.id_category) && $smarty.get.id_category}
		
			{if ($smarty.get.p == 0) || ($smarty.get.p == 1)}
			
			{else}
			
				{if ($smarty.get.p == 2)}
					<link rel="prev" href="{$cat_canonical}" />
				{else}
					<link rel="prev" href="{$cat_canonical}pag/{($smarty.get.p)-1}" />
				{/if}
			{/if}
			
			{if ($smarty.get.p == $pages_nb)}
			{else}
				{if ($smarty.get.p == 0)}
					<link rel="next" href="{$cat_canonical}pag/{($smarty.get.p)+2}" />
				{else}
					<link rel="next" href="{$cat_canonical}pag/{($smarty.get.p)+1}" />
				{/if}
			{/if}
		{else if isset($smarty.get.id_manufacturer) && $smarty.get.id_manufacturer}
		
			{if ($smarty.get.p == 0) || ($smarty.get.p == 1)}
			
			{else}
				{if ($smarty.get.p == 2)}
					<link rel="prev" href="{$cat_canonical}" />
				{else}
					<link rel="prev" href="{$cat_canonical}pag/{($smarty.get.p)-1}" />
				{/if}
			{/if}
			
			{if ($smarty.get.p == $pages_nb)}
			{else}
			{if ($smarty.get.p == 0)}
					<link rel="next" href="{$cat_canonical}pag/{($smarty.get.p)+2}" />
				{else}
					<link rel="next" href="{$cat_canonical}pag/{($smarty.get.p)+1}" />
				{/if}
			{/if}
		{/if}
		
		<meta property="og:locale" content="{$lang_iso}_{$lang_iso|upper}" />
		<meta property="og:title" content="{$meta_title|escape:'htmlall':'UTF-8'}" />
		<meta property="og:site_name" content="Ezdirect" />
		{if isset($canonical) AND $canonical}
			<meta property="og:url" content="{$link->getProductLink($canonical, $canonical_rew, $canonical_cat)}" />
		{else}
			{if isset($cat_canonical) AND $cat_canonical}
	
			{else if isset($cat_canonicalz) AND $cat_canonicalz}
				<meta property="og:url" content="{$cat_canonicalz}" />
			{/if}
		{/if}
		{if isset($cover_image) AND $cover_image}
			<meta property="og:image" content="{$cover_image}" />
		{/if}
		
		{if isset($css_files)}
		{foreach from=$css_files key=css_uri item=media}
			<link href="{$css_uri}?v=22.40" rel="stylesheet" type="text/css" media="all" />
		{/foreach}
		{/if}
		
	
		<!--[if lte IE 8]>
			<link href="{$css_dir}global-ie.css" rel="stylesheet" type="text/css" media="all" />
			
			<style>
				#loyalty-div {
				margin-top:-50px;
				}
			</style>
		<![endif] -->
		<!--[if !(lte IE 8)]>-->
			<link href="{$css_dir}checkbox.css" rel="stylesheet" type="text/css" media="none" onload="if(media!='all')media='all'"><noscript><link rel="stylesheet" href="{$css_dir}checkbox.css"></noscript> 
		
		<!-- <![endif]-->	
			<link href="{$css_dir}select2.css" rel="stylesheet" type="text/css" media="none" onload="if(media!='all')media='all'"><noscript><link rel="stylesheet" href="{$css_dir}select2.css"></noscript> 
		
		
		{if isset($js_files)}
			{foreach from=$js_files item=js_uri}
			<script type="text/javascript" src="{$js_uri}"></script>
			{/foreach}
		{/if}
		
		
		<script type="text/javascript">
			var baseDir = '{$content_dir}';
			var static_token = '{$static_token}';
			var token = '{$token}';
			var priceDisplayPrecision = {$priceDisplayPrecision*$currency->decimals};
			var priceDisplayMethod = {$priceDisplay};
			var roundMode = {$roundMode};
		</script>
		
		
		<!-- cVetta -->
		<link href="//ws10b.cvetta.io/css/cvetta.min.css" rel="stylesheet" type="text/css" id="cVettaStyle" />
		<script type="text/javascript" src="//ws10b.cvetta.io/js/cvetta.min.js" charset="UTF-8"></script>
		<!-- End cVetta -->
		
		
		<!-- Analytics -->
		<script type="text/javascript" src="{$js_dir}etc/analytics.js"></script>
		<!-- End Analytics -->
	
		<script  type="text/javascript" async>{literal}(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5320644"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");{/literal}</script><noscript><img src="//bat.bing.com/action/0?ti=5320644&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>
		
		<!-- Hotjar Tracking Code for www.ezdirect.it -->
		<script async type="text/javascript">
		{literal}
			(function(h,o,t,j,a,r){
				h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
				h._hjSettings={hjid:665698,hjsv:6};
				a=o.getElementsByTagName('head')[0];
				r=o.createElement('script');r.async=1;
				r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
				a.appendChild(r);
			})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
			
		{/literal}
		
		</script>
		<!-- Global site tag (gtag.js) - Google AdWords: 1058514372 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-1058514372"></script><script type="text/javascript">{literal} window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-1058514372');{/literal} 
		</script> 
		
		<!-- Event snippet for conv chiamate per mobile conversion page In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. --> <script async type="text/javascript"> {literal}function gtag_report_conversion(url) { var callback = function () { if (typeof(url) != 'undefined') { window.location = url; } }; gtag('event', 'conversion', { 'send_to': 'AW-1058514372/W4UOCMzBy4YBEMTL3vgD', 'event_callback': callback }); return false; } {/literal} 
		</script> 
		<!-- Facebook Pixel Code -->
		<script async type="text/javascript">
		{literal}
		  !function(f,b,e,v,n,t,s)
		  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		  n.queue=[];t=b.createElement(e);t.async=!0;
		  t.src=v;s=b.getElementsByTagName(e)[0];
		  s.parentNode.insertBefore(t,s)}(window, document,'script',
		  'https://connect.facebook.net/en_US/fbevents.js');
		  fbq('init', '497290980961225'); <!-- Leila; pixel del mio account fb: 786072842186503; originale: 452917518612258 -->
		  fbq('track', 'PageView');
		{/literal}
		</script>
		<noscript><img height="1" width="1" style="display:none"
		  src="https://www.facebook.com/tr?id=497290980961225&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->

		<!-- Sendinblue Tracker -->
		<script type="text/javascript">
		{literal}(function() {
			window.sib = {
				equeue: [],
				client_key: "ek4dimw3ofqbgq8kz9jnwmvj"
			};
			/* OPTIONAL: email for identify request*/
			// window.sib.email_id = {/literal}{$cookie->email}{literal};
			window.sendinblue = {};
			for (var j = ['track', 'identify', 'trackLink', 'page'], i = 0; i < j.length; i++) {
			(function(k) {
				window.sendinblue[k] = function() {
					var arg = Array.prototype.slice.call(arguments);
					(window.sib[k] || function() {
							var t = {};
							t[k] = arg;
							window.sib.equeue.push(t);
						})(arg[0], arg[1], arg[2]);
					};
				})(j[i]);
			}
			var n = document.createElement("script"),
				i = document.getElementsByTagName("script")[0];
			n.type = "text/javascript", n.id = "sendinblue-js", n.async = !0, n.src = "https://sibautomation.com/sa.js?key=" + window.sib.client_key, i.parentNode.insertBefore(n, i), window.sendinblue.page();
		})();{/literal}
		</script>
		<!-- End Sendinblue Tracker -->
		
		<!-- Sendinblue Chat -->
		<script type="text/javascript">
		{literal}(function() {
			window.sib = { equeue: [], client_key: "ek4dimw3ofqbgq8kz9jnwmvj" };
			/* OPTIONAL: email to identify request*/
			// window.sib.email_id = 'example@domain.com';
			/* OPTIONAL: to hide the chat on your script uncomment this line (0 = chat hidden; 1 = display chat) */
			// window.sib.display_chat = 0;
			// window.sib.display_logo = 0;
			/* OPTIONAL: to overwrite the default welcome message uncomment this line*/
			// window.sib.custom_welcome_message = 'Hello, how can we help you?';
			/* OPTIONAL: to overwrite the default offline message uncomment this line*/
			// window.sib.custom_offline_message = 'We are currently offline. In order to answer you, please indicate your email in your messages.';
			window.sendinblue = {}; for (var j = ['track', 'identify', 'trackLink', 'page'], i = 0; i < j.length; i++) { (function(k) { window.sendinblue[k] = function(){ var arg = Array.prototype.slice.call(arguments); (window.sib[k] || function() { var t = {}; t[k] = arg; window.sib.equeue.push(t);})(arg[0], arg[1], arg[2]);};})(j[i]);}var n = document.createElement("script"),i = document.getElementsByTagName("script")[0]; n.type = "text/javascript", n.id = "sendinblue-js", n.async = !0, n.src = "https://sibautomation.com/sa.js?key=" + window.sib.client_key, i.parentNode.insertBefore(n, i), window.sendinblue.page();
		  })();{/literal}
		</script>
		<!-- End Sendinblue Chat -->

		{else}
		{include file="$tpl_dir./landing_head.tpl"}
		{/if}
	</head>

	<body {if $page_name}id="{$page_name|escape:'htmlall':'UTF-8'}"{/if}>
	
	<div id="over" onclick="document.getElementById('over').style.display = 'none'; document.getElementById('menu-blu-outer').style.display = 'none'; document.getElementById('menu-blu').style.display = 'none';"></div> <!-- end over -->
		<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TK28N4X"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->



{if $id_cms_category != 7}
		<!-- <a href="https://www.ezdirect.it/offerte-speciali" class="bodylink" rel="nofollow"></a> -->
		
		<div id="menu-alto" style="z-index:999999999999999999999999999999">
			<div id="menu-alto-in" style="background-color:#eeeeee; width:100%">

				<div class="container">

					<div class="row">

						<!-- Block user information module HEADER -->
						<div class="container" id="header_user">

							<div class="row">
								<!--RECENSIONI-->
									<div class="col-sm-4" style="padding-top:4px;">
												
										<div style="display:flex;">
											<a href="https://www.ekomi.it/opinioni-ezdirectit.html">
												<div style="display:flex; height: 20px;">
													<img style="height:100%;" src='{$img_ps_dir}sss.png' alt='Stelle recensioni' title='Stelle recensioni' />
													<img style="height:100%;" src='{$img_ps_dir}sss.png' alt='Stelle recensioni' title='Stelle recensioni' />
													<img style="height:100%;" src='{$img_ps_dir}sss.png' alt='Stelle recensioni' title='Stelle recensioni' />
													<img style="height:100%;" src='{$img_ps_dir}sss.png' alt='Stelle recensioni' title='Stelle recensioni' />
													<img style="height:100%;" src='{$img_ps_dir}sss.png' alt='Stelle recensioni' title='Stelle recensioni' />
												</div>
											</a>
											<a href="https://www.ekomi.it/opinioni-ezdirectit.html" style="margin-left:10px; font-size:12px; font-weight:bold; color:#545454; text-decoration:none; align-self: center;"><span style="font-size:15px; padding-right:10px;">4.9/5</span><span id="hide-review">Recensioni certificate by eKomi</span></a>
										</div>

										
										
														
									</div>
								<!--END RECENSIONI-->
												
								<!--SPEDIZIONE GRATUITA-->

									<div class="col-sm-3" id="social-header">
													
										<div style="display:flex; margin-top:5px;     justify-content: center; width: 104%;">
											<div>
												<i class="fas fa-phone-alt"></i>
											</div>

											<p style="margin:0; margin-left:10px; font-size:12px; font-weight:bold; align-self: center;">ASSISTENZA CLIENTI <a href="#" style="color:#05a040;">800-529767</a></p>
															
										</div>

									</div>

								<!--END SPEDIZIONE GRATUITA-->
							
								<!--PICCOLA NAVBAR-->
									<div class="col-sm-5">
										<ul class='blockuserinfoul' style='font-size:14px; padding-top: 6px;' id="header_user">
											{if $cookie->isLogged()}
												
												<li><strong><a href="{$link->getPageLink('my-account.php', true)}" rel="nofollow" title="{l s='Il tuo account' mod='blockuserinfo'}"><span>{l s='Il tuo account' mod='blockuserinfo'}</span></a></strong></li>
											{else}
											
											<li><strong><a href="{$link->getPageLink('authentication.php', true)}?back=my-account.php" rel="nofollow" title="{l s='Il tuo account' mod='blockuserinfo'}"><span>{l s='Il tuo account' mod='blockuserinfo'}</span></a></strong></li>
											{/if}
											
											{if $latest_offer > 0}
											
											<li><strong><a href="{$link->getPageLink('modules/mieofferte/offerte.php', true)}?id_cart={$latest_offer}&id_customer={$cookie->id_customer}" rel="nofollow" title="{l s='View my latest offer' mod='blockuserinfo'}"><span>{l s='View my latest offer' mod='blockuserinfo'}</span></a></strong></li>
											
											
											{else}
											{/if}
											
											
											<li><strong><a href="
											{if $offer_count > 0}
											{$link->getPageLink('modules/mieofferte/offerte.php', true)}
											{else}
											{$link->getPageLink('modules/formprevendita/form.php', true)}
											{/if}
											"><span>{l s='Preventivi' mod='blockuserinfo'}</span> {if $offer_count > 0} <span style="color:#ff0000; font-weight:bold">({$offer_count})</span>{/if}</a></strong>
											
											</li>
											
											
											<li><strong><a href="{$link->getPageLink('contact-form.php', true)}?step=tecnica" title=""><span>{l s='Assistenza' mod='blockuserinfo'}</span></a></strong></li>
											
											<li><strong><a href="https://www.ezdirect.it/autenticazione?cliente=rivenditore" rel="nofollow"><span>{l s='Rivenditori' mod='blockuserinfo'}</span></a></strong></li>
											
											<li style="border-right:0px"><strong><a href="https://www.ezdirect.it/blog/"><span>{l s='Blog' mod='blockuserinfo'}</span></a></strong></li>
											
											{if $lang_iso == 'en'}
										
											{foreach from=$languages key=k item=language name="languages"}
												<li {if $language.iso_code == $lang_iso}class="selected_language"{/if}>
													{if $language.iso_code != $lang_iso}
														{assign var=indice_lang value=$language.id_lang}
														{if isset($lang_rewrite_urls.$indice_lang)}
															<a href="{$lang_rewrite_urls.$indice_lang|escape:htmlall}" title="{$language.name}">
														{else}
															<a href="{$link->getLanguageLink($language.id_lang)|escape:htmlall}" title="{$language.name}">
														{/if}

													{/if}
														<img src="{$img_lang_dir}{$language.id_lang}.jpg" alt="{$language.iso_code}" width="16" height="11" />
													{if $language.iso_code != $lang_iso}
														</a>
													{/if}
												</li>
											{/foreach}


											{/if}
									
										</ul>
									</div>
								<!--END PICCOLA NAVBAR-->
							</div>

							

						</div>
						<!-- /Block user information module HEADER -->

						{* <!--RECENSIONI-->
						<div class="col-sm-3" >
							
							<div style="display:flex; margin-top:5px;">
								<div>
									<i class="fas fa-star" style="color: #f4b716"></i>
									<i class="fas fa-star" style="color: #f4b716"></i>
									<i class="fas fa-star" style="color: #f4b716"></i>
									<i class="fas fa-star" style="color: #f4b716"></i>
									<i class="fas fa-star" style="color: #f4b716"></i>
								</div>

								<span style="margin-left:10px; font-size:11px; font-weight:bold;     align-self: center;">RECENSIONI</span>
							</div>
							

							
						</div>
						<!--END RECENSIONI-->
						
						<!--SPEDIZIONE GRATUITA-->

						<div class="col-sm-3" id="social-header">
						
							<div style="display:flex; margin-top:5px;">
								<div>
									<i class="fas fa-truck"></i>
								</div>

								<span style="margin-left:10px; font-size:11px; font-weight:bold;     align-self: center;">SPEDIZIONE GRATUITA DA 189€</span>
								
							</div>

						</div>

						<!--END SPEDIZIONE GRATUITA--> *}

						<!--SMALL NAVBAR-->
							{* <div class="col-sm-6" id="social-header">
								<ul class="menu-navbar-custom">
									<li>
										<a href="#">Preventivi</a>
									</li>
									<li>
										<a href="#">Assistenza</a>
									</li>
									<li>
										<a href="#">Rivenditori</a>
									</li>
									<li>
										<a href="#">Blog</a>
									</li>
								</ul>

							</div> *}
							
							
						<!--END SMALL NAVBAR-->
					</div>
					
				</div>
				
				
			</div>
		</div>

		
		<div id="contenitore">
			<!--[if gte IE 9]><!-->
				
			<!--<![endif]-->
			<div id="header-outer">
				<div class="container" id="header">
		
					<div class="row" id="header-in">
					
						<script type="text/javascript">
						var WRInitTime=(new Date()).getTime();
						</script>

						

							
						<div class="col-sm-3">
							<a id="header_logo" href="{$link->getPageLink('index.php')}" title="Ezdirect">
								<img class="logo" src="{$img_ps_dir}Logo-senza-sfondo.png" alt="Ezdirect" {if $logo_image_width}width="80%"{/if} {if $logo_image_height}height="auto" {/if} style="margin:0px 20px;" />
							</a>
						</div>
								
		
						{* <label for="show-menu" class="show-menu" onclick="document.getElementById('over').style.display = 'block'; document.getElementById('menu-blu-outer').style.display = 'block'; document.getElementById('menu-blu').style.display = 'block';"><img src='{$img_ps_dir}menu_mobile_3.png' alt='Menu' title='Menu' /></label> *}
							 
							
						<div class="col-sm-6">

								
										<!-- Block search module TOP -->
								<div id="blocco_ricerca">
									
									<!-- Block search module TOP -->
									<div id="search_block_top" >
										<form method="get" action="https://www.ezdirect.it/search.php" class="cVetta-form" id="cVetta-form">
											<div class="form-search input-box" id="cvetta-form-search">
											<fieldset>
												<label for="search_query_top"><!-- image on background --></label>
												<input type="hidden" name="orderby" value="position" />
												<input type="hidden" name="orderway" value="desc" />
												<input type="hidden" name="nnn" value="on" />
												<input type="hidden" name="cod" value="on" />
												<input type="hidden" name="sd" value="on" />
												<input class="search_query" type="text"  id="cvetta-search{if $cVetta_test == 1}{else}1{/if}" 
												onfocus="{literal}if{/literal}(this.value=='{l s='Cerca nel catalogo...'}'){literal}{this.value='';};return false;{/literal}" 
												onblur="{literal}if{/literal}(this.value==''){literal}{this.value='{/literal}{l s='Cerca nel catalogo...'}{literal}';};return false;{/literal}" 
												name="search_query" value="{if $smarty.server.PHP_SELF|basename != 'search2.php'}{if isset($smarty.get.search_query)}{$smarty.get.search_query|htmlentities:$ENT_QUOTES:'utf-8'|stripslashes}{else}{l s='Cerca nel catalogo...'}{/if}{else}{/if}" />
												
												<div class="search" style="display: none;">
													<span class="icons" id="search-icon-custom"><i class="fas fa-search"></i></span>
													
													<input type="submit" name="submit_search" value="" id="vaicerca">
												</div>
												
												
												<a href="#" id="cvetta-search-close"></a> 
												<script type="text/javascript">
												//<![CDATA[
													var cVettaParams = {
														idvetrina: "ezdirect_it",
														lang: "it_IT",
														currency: "€",
														noprice_text: "",
														viewMode: "0"
													};

													var cVettaLabels = {
														resultsCount: "<b>{0}</b> Risultati",
														activeFilters: "Filtri attivi",
														removeFilters: "Rimuovi filtri",
														showMore: "Vedi altro",
														showLess: "Vedi di meno",
														noResults: "Nessun risultato.",
														didYouMean: "Cercavi forse:",
														from: "Da",
														to: "A",
													};

													var cVettaFacetLabels = {
														'categories': 'Categorie',
														'accessories_size': 'Accessories Size',
														'apparel_type': 'Type',
														'bag_luggage_type': 'Bag & Luggage Type',
														'bed_bath_type': 'Bed & Bath Type',
														'color': 'Color',
														'decor_type': 'Decor Type',
														'description': 'Description',
														'electronic_type': 'Electronic Type',
														'fit': 'Fit',
														'format': 'format',
														'frame_style': 'Frame Style',
														'gender': 'Gender',
														'home_decor_type': 'Home & Decor Type',
														'image': 'Base Image',
														'jewelry_type': 'Jewelry Type',
														'length': 'Length',
														'manufacturer': 'Manufacturer',
														'name': 'Name',
														'necklace_length': 'Necklace Length',
														'occasion': 'Occasion',
														'price': 'Prezzo',
														'shoe_size': 'Shoe size',
														'shoe_type': 'Shoe type',
														'size': 'Size',
														'sku': 'SKU',
														'sleeve_length': 'Sleeve Length',
														'special_from_date': 'Special Price From Date',
														'special_price': 'Special Price',
														'special_to_date': 'Special Price To Date',
														'style': 'Style',
														'url_key': 'URL Key',
													};
												//]]>
												</script>
											</div>
												
											</fieldset>
											</div>
										</form>
										
										<script type="text/javascript" src="/js/jquery/jquery.autocomplete-base2.js"></script>
						
										<script type="text/javascript">
										// <![CDATA[
										{literal}
											$('document').ready( function() {
												$('.apri-contattaci').attr('href', 'javascript:void(0)');
												
												$('.apri-contattaci').click(function(event) {
													$('#id01').show(); $('#preventivo_v').hide();
												});
											
												$("#search_query_top")
													.autocomplete(
														'{/literal}{if $search_ssl == 1}{$link->getPageLink('search.php', true)}{else}{$link->getPageLink('search.php')}{/if}{literal}', {
															minChars: 3,
															max: 10,
															width: 500,
															selectFirst: false,
															scroll: false,
															dataType: "json",
															formatItem: function(data, i, max, value, term) {
																return value;
															},
															parse: function(data) {
																var mytab = new Array();
																for (var i = 0; i < data.length; i++)
																{
																	mytab[mytab.length] = { data: data[i], value: data[i].pname };
																}
																return mytab;
															},
															extraParams: {
																ajaxSearch: 1,
																id_lang: {/literal}{$cookie->id_lang}{literal}
															}
														}
													)
													.result(function(event, data, formatted) {
														$('#search_query_top').val(data.pname);
														document.location.href = data.product_link;
													})
											});
										{/literal}
										// ]]>
										</script>
									</div>

									<!-- /Block search module TOP -->
						</div>
						
								

						<div class="col-sm-3" style="display: flex; align-items: center;">
							<div id="account-block-new" >
								<a href="{$link->getPageLink('authentication.php', true)}?back=my-account.php" rel="nofollow" title="{l s='Login' mod='blockuserinfo'}"><i style="color: #ff6600; font-size: 25px;" class="fas fa-user-alt"></i></a>
								{if $cookie->isLogged()}
											{* <span>Sei online</span> *}
											{* <br /><a href="{$link->getPageLink('index.php')}?logout" id="logout-link" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}">{l s='Log out' mod='blockuserinfo'}</a> *}
										
								{else}
								
								{* SCRITTA LOGIN : facoltativa?  *}
								{* <strong><a href="{$link->getPageLink('authentication.php', true)}?back=my-account.php" rel="nofollow" title="{l s='Login' mod='blockuserinfo'}">{l s='Login' mod='blockuserinfo'}</a></strong> *}
								{/if}

							</div>

							<div id="info-utente">
								{$HOOK_TOP} 
							</div>
						</div>
						
						
						
						

						<div id="burger-menu">

						
							{* <!--Navbar-->
							<nav class="navbar navbar-light light-blue lighten-4">

							

							<!-- Collapse button -->
							<button class="navbar-toggler toggler-example" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1"
								aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation" style="border: none; font-size: 28px; position: absolute; top: 4px; left: -10px;"><span class="dark-blue-text"><i
									class="fas fa-bars" style="color: #000099; font-size: 25px;"></i></span></button>

							<!-- Collapsible content -->
							<div class="collapse navbar-collapse" id="navbarSupportedContent1">

								<!-- Links -->
								<ul class="navbar-nav mr-auto" style="background-color: white; font-size: 15px; width: 18em; height: 20em; position: absolute; top: 35px;  left: -4px; border: 1px solid #f4f4f4;">
								<li class="nav-item" style="text-align: end;">
									<a class="navbar-toggler toggler-example" type="button" data-toggle="collapse" href="#" id="nav-item-custom" style="border:none;"><i style="color: #545454; font-size: 14px;" class="fas fa-times"></i></a>
								</li>
								
								<li class="nav-item active">
									<a class="nav-link" href="#" id="nav-item-custom">Home <span class="sr-only">(current)</span></a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#" id="nav-item-custom">Account</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#" id="nav-item-custom">Carrello</a>
								</li>
								</ul>
								<!-- Links -->

							</div>
							<!-- Collapsible content -->

							</nav>
							<!--/.Navbar--> *}

							<nav class="navbar navbar-default">
								<div class="container-fluid">
									<!-- Brand and toggle get grouped for better mobile display -->
									<div class="navbar-header" style="display: flex; align-items: center; width: 100%;">
										<div>
											<button id="ChangeToggle" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" style="    height: max-content; display: block; background-color: #f4f4f4" id="btn-burger" class="button-burger-menu">
												<div id="navbar-hamburger">
													<span class="sr-only">Toggle navigation</span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
												</div>
												
												<div id="navbar-close" class="hidden">

													<div style="display: flex; align-items: center;">
														<span class="glyphicon glyphicon-remove"></span>

														<div style="width: 100%; display: flex; justify-content: center;">
															<a class="navbar-brand" href="{$link->getPageLink('index.php')}" title="Ezdirect" style="height: min-content;">
																<img class="logo" src="{$img_ps_dir}Logo-senza-sfondo.png" alt="Ezdirect" {if $logo_image_width}width="80%"{/if} {if $logo_image_height}height="auto" {/if} style="margin:0px 20px;" />
															</a>
														</div>
													</div>

													<div class="collapse navbar-collapse custom" id="bs-example-navbar-collapse-1" toggle="collapse" data-target=".navbar-collapse">
											<ul class="nav navbar-nav" style="font-size: 15px;">
												<li class="active" style="padding: 10px 9px;">
													<form method="get" action="https://www.ezdirect.it/search.php" class="cVetta-form" id="cVetta-form" >
																					<div class="form-search input-box" id="cvetta-form-search" style="width: 100%; display: flex; justify-content: space-between;">
																						<fieldset style="width: 100%;">
																							<label for="search_query_top"><!-- image on background --></label>
																							<input type="hidden" name="orderby" value="position" />
																							<input type="hidden" name="orderway" value="desc" />
																							<input type="hidden" name="nnn" value="on" />
																							<input type="hidden" name="cod" value="on" />
																							<input type="hidden" name="sd" value="on" />
																							<input style="width: 96%; padding: 4px; color: #545454;" class="search_query" type="text"  id="cvetta-search{if $cVetta_test == 1}{else}1{/if}" 
																							onfocus="{literal}if{/literal}(this.value=='{l s='Cerca nel catalogo...'}'){literal}{this.value='';};return false;{/literal}" 
																							onblur="{literal}if{/literal}(this.value==''){literal}{this.value='{/literal}{l s='Cerca nel catalogo...'}{literal}';};return false;{/literal}" 
																							name="search_query" value="{if $smarty.server.PHP_SELF|basename != 'search2.php'}{if isset($smarty.get.search_query)}{$smarty.get.search_query|htmlentities:$ENT_QUOTES:'utf-8'|stripslashes}{else}{l s='Cerca nel catalogo...'}{/if}{else}{/if}" />
																							
																							
																						
																							
																						</fieldset>

																						<div class="search">
																							<span class="icons" id="search-icon-custom"><i class="fas fa-search"></i></span>
																								
																							
																						</div>
																					</div>
																				</form>
																				
												</li>

												<li class="nav-item"><a href="{$link->getPageLink('my-account.php', true)}" rel="nofollow" title="{l s='Il tuo account' mod='blockuserinfo'}">Il tuo account</a>
												</li>

												<li class="nav-item"><a href="{$link->getPageLink("order.php", true)}">Carrello</a>
												</li>

												<hr>
												<li class="nav-item"><a href="{$link->getCategoryLink(23)|escape:'html':'UTF-8'}">Centralini telefonici</a>
												</li>

												<li class="nav-item"><a href="{$link->getCategoryLink(115)|escape:'html':'UTF-8'}">Telefoni fissi</a>
												</li>

												<li class="nav-item"><a href="{$link->getCategoryLink(128)|escape:'html':'UTF-8'}">Telefoni cordless</a>
												</li>

												<li class="nav-item"><a href="{$link->getCategoryLink(106)|escape:'html':'UTF-8'}">Cuffie</a>
												</li>

												<li class="nav-item"><a href="{$link->getCategoryLink(76)|escape:'html':'UTF-8'}">Videoconferenza</a>
												</li>

												<li class="nav-item"><a href="{$link->getCategoryLink(75)|escape:'html':'UTF-8'}">Audioconferenza</a>
												</li>

												<li class="nav-item"><a href="{$link->getCategoryLink(208)|escape:'html':'UTF-8'}">Citofoni</a>
												</li>
												
												<li class="nav-item"><a href="{$link->getCategoryLink(27)|escape:'html':'UTF-8'}">Gateway GSM</a>
												</li>

												<li class="nav-item"><a href="{$link->getCategoryLink(46)|escape:'html':'UTF-8'}">Gateway VoIP</a>
												</li>
											

											</ul>
										</div>
												</div>
											</button>

											
										</div>

											<div style="width: 100%; display: flex; justify-content: center;" id="logo">
															<a class="navbar-brand" href="{$link->getPageLink('index.php')}" title="Ezdirect" style="height: min-content;">
																<img class="logo" src="{$img_ps_dir}Logo-senza-sfondo.png" alt="Ezdirect" {if $logo_image_width}width="80%"{/if} {if $logo_image_height}height="auto" {/if} style="margin:0px 20px;" />
															</a>
														</div>
									</div>

										
								</div>
							</nav>

<script>
	$(function() {
  $('#ChangeToggle').click(function() {
    $('#navbar-hamburger').toggleClass('hidden');
    $('#navbar-close').toggleClass('hidden');  
	$('#logo').toggleClass('hidden');  
	$('#btn-burger').toggleClass('button-burger-menu'); 
	
  });
});
</script>
							
						</div>
						
						
					</div>
						
		
				</div> <!-- fine header-in -->
			</div> <!-- fine header -->
		</div> <!-- fine header-outer -->

		
			<div id="menu-blu-outer" style="background-color:white; ">
				<input type="checkbox" class="checkbox-menu" id="show-menu" role="button" /> 
				<div id="menu-blu">
				<div id="close-mobile-menu"><img src='{$img_ps_dir}close-menu.jpg' alt='Chiudi' title='Chiudi' style='cursor:pointer' onclick="document.getElementById('over').style.display = 'none'; document.getElementById('menu-blu').style.display = 'none';" /></div>
			<ul class="menucategory" style="height: auto;">
	
	
	
	
	
	{* CENTRALINI *}
		<li class="cat" id="menu-cuffie" style="border-left:0px; margin-left:0px;"><a  class="menu-principale-link" href="{$link->getCategoryLink(23)|escape:'html':'UTF-8'}">Centralini telefonici <i class="fas fa-chevron-down"></i></a>
	
	 <label for="show-menu-centralini" class="show-menu-triangle"><img src='{$img_ps_dir}menu_arrow_black.png' alt='Submenu' title='Submenu' /></label>
			<input type="checkbox" class="checkbox-menu-triangle" id="show-menu-centralini" role="button" /> 
			
	<div class="menu-category" style="height:300px; border: 1px solid #f1f2f4; border-radius:6px;">
		<div class="container" style="display:flex; justify-content:space-around;">

			<div class="row" style="display:flex; flex-direction:column;">
				<div class="submenu" style="width:130px">
					<span class="exh3-category">Tipo centralino</span>
					<ul class="int-category">
						<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo-centralino">Centralini hardware</a></li>
						<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo-centralino_virtuale">Centralino virtuale</a></li>
						<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo-centralino_software">Centralino software</a></li>
						<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo-centralino_dect">Centralino DECT</a></li>
						
						<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo-componente">Componenti</a></li>

						<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo-licenza">Licenze</a></li>

						<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo-licenza_upgrade">Upgrade</a></li>

						<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo-licenza_maintanance">Maintenance</a></li>
					</ul>
				</div>
			</div>

			<div class="row" style="display:flex; flex-direction:column; padding: 0 30px;">
				<div class="submenu" style="width:130px">
					<span class="exh3-category">Tipo di linea</span>
					<ul class="int-category">
						<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo_linee-analogica-analogica_voip_isdn-isdn_voip_analogica-voip_analogica-analogica_voip-analogica_gsm-analogica_isdn-analogiciedigitali">Analogica</a></li>
						<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo_linee-voip-analogica_isdn-isdn_voip-analogica_voip_isdn-isdn_voip_analogica">ISDN</a></li>
						<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo_linee-voip-isdn_voip-analogica_voip_isdn-isdn_voip_analogica-voip_analogica-analogica_voip">VoIP</a></li>
						
					</ul>
				</div>
			</div>

			<div class="row" style="display:flex; flex-direction:column;">
				<div class="submenu">
					<span class="exh3-category">Tipo interni</span>
					<ul class="int-category">
						<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo_di_derivati_int_collegabili-analogici_digitali_voip-int_analogici_digita-analogici_voip-analogici">Analogici</a></li>
						<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo_di_derivati_int_collegabili-analogici_digitali_voip">Analogici + digitali + VoIP</a></li>
						<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo_di_derivati_int_collegabili-analogici_voip">Analogici + VoIP</a></li>
						<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo_di_derivati_int_collegabili-analogici_digitali_voip-analogici_voip">VoIP</a></li>
				
					
					</ul>
					<br />
					
				</div>
			</div>

			<div class="row" style="display:flex; flex-direction:column;">
				<div class="submenu">
					<span class="exh3-category">Funzioni</span>
					<ul class="int-category">
					<li><a href="https://www.ezdirect.it/centralini-telefonici/#/messaggi_vocali_su_linee_opa-si">Messaggi vocali</a></li>
					<li><a href="https://www.ezdirect.it/centralini-telefonici/#/registrazione_conversazioni-si">Registrazione</a></li>
					<li><a href="https://www.ezdirect.it/centralini-telefonici/#/funzioni_hotel-si">Funzioni hotel</a></li>
					<li><a href="https://www.ezdirect.it/centralini-telefonici/#/riconoscimento_fax_automatico-si">Riconoscimento FAX</a></li>
					<li><a href="https://www.ezdirect.it/centralini-telefonici/#/least_cost_routing_lcr-si">Instradamento LCR</a></li>
					
					</ul>
				</div>
			</div>
			
			<div class="row" style="display:flex; flex-direction:column;">
				<div class="submenu" style="width:135px">
					<span class="exh3-category">Produttori</span>
					<ul class="int-category">
						<li style="width:130px"><a href="https://www.ezdirect.it/centralini-telefonici/#/produttore-3cx" title="Centralini 3CX" >3CX</a></li>
						<li style="width:130px"><a href="https://www.ezdirect.it/centralini-telefonici/#/produttore-ezdirect" title="Centralini Ezdirect" >Ezdirect</a></li>
						<li style="width:130px"><a href="https://www.ezdirect.it/centralini-telefonici/#/produttore-siemens/produttore-gigaset" title="Centralini Gigaset">Gigaset</a></li>
						<li style="width:130px"><a href="https://www.ezdirect.it/centralini-telefonici/#/produttore-grandstream" title="Centralini Grandtream">Grandstream</a></li>
						<li style="width:130px"><a href="https://www.ezdirect.it/centralini-telefonici/#/produttore-openvox" title="Centralini Openvox">Openvox</a></li>
						<li style="width:130px"><a href="https://www.ezdirect.it/centralini-telefonici/#/produttore-panasonic" title="Centralini Panasonic">Panasonic</a></li>
						<li style="width:130px"><a href="https://www.ezdirect.it/centralini-telefonici/#/produttore-samsung" title="Centralini Samsung">Samsung</a></li>
						<li style="width:130px"><a href="https://www.ezdirect.it/centralini-telefonici/#/produttore-yeastar" title="Centralini Yeastar">Yeastar</a></li>
				
					</ul>
						
				</div>
			</div>

			<div class="row" style="display:flex; flex-direction:column;">
				<div class="cat-separatore">
					<div style="margin-left:25px; padding-top: 9px;">

						<span class="exh3-category special" ><a href="{$link->getCategoryLink(23)|escape:'html':'UTF-8'}" style="margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">Tutti i centralini</a></span>
						
						<span class="exh3-category special" ><a href="https://www.ezdirect.it/nuovi-prodotti-telefoni-fissi-voip-cordless-centralini?cat=23" style="margin-top:-10px;color: #ff6600; border:none; font-size: 13px;">Nuovi prodotti</a></span>
				
						<span class="exh3-category special"><a href="https://www.ezdirect.it/prodotti-venduti-migliori-sito-web-ezdirect?cat=23" style="width:110px; margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">I pi&ugrave; venduti</a></span>
			
							
						<span class="exh3-category special"><a href="https://www.ezdirect.it/offerte-speciali?category=23" style="margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">Offerte speciali</a></span> 
						
						<span class="exh3-category"><a href='https://www.ezdirect.it/guide/category/5-guide-e-consigli' style='width:110px; margin-top:-10px; color:#ff6600; border:none; font-size: 13px;'>Guide</a></span>
						<ul class="int-categ-marg" style="margin-top:-20px;">
							<li><a href="https://www.ezdirect.it/guide/category/6-centralino-virtuale-ezcloud" title="Guide centralino virtuale">Guide centralino virtuale</a></li>
							<li><a href="https://www.ezdirect.it/guide/8-centralini-funzioni" title="Centralini funzioni">Centralini funzioni</a></li>
							<li><a href="https://www.ezdirect.it/guide/10-centralini-guida-alla-scelta-migliore-centralino-telefonico-linea-voip-isdn" title="Centralini guida">Centralini guida</a></li>
							<li><a href="https://www.ezdirect.it/guide/46-skype-cuffie-e-telefoni-accessori-per-pc-usb-compatibili" title="Skype cuffie e telefoni">Skype cuffie e telefoni</a></li>
							
						</ul>
				

					</div>
				</div>
			</div>

		</div>
		
		{* <br /><br /><br /><a href='https://www.ezdirect.it/centralino-virtuale/' target='_blank'><img src='{$img_ps_dir}ezcloud.jpeg' alt='Centralino virtuale Ezcloud' title='Centralino virtuale Ezcloud' /></a> *}
		{* <span class="exh3-category"><a href='https://www.ezdirect.it/guide/category/6-centralino-virtuale-ezcloud' >Guide e manuali Centralino Virtuale</a></span> *}
		
		
	
	
		
	
		{* <div class="submenu" style="width:130px">
						<span class="exh3-category">Prezzo</span>
		<ul class="int-category">
		<li><a href="https://www.ezdirect.it/centralini-telefonici/#/price-0-200">Fino a 200 &euro;</a></li>
		<li><a href="https://www.ezdirect.it/centralini-telefonici/#/price-201-500">Da 201 a 500 &euro;</a></li>
		<li><a href="https://www.ezdirect.it/centralini-telefonici/#/price-501-1000">Da 501 a 1000 &euro;</a></li>
		<li><a href="https://www.ezdirect.it/centralini-telefonici/#/price-1000-9000">Oltre 1000 &euro;</a></li>

		
		</ul>
		<div class="submenu">
		
		
		</div>
		
		
		
		
		
		</div> *}
		
		
		
						
		
		{* <div style="clear:both"></div> *}
	</div>
	
	</li>

	{* TELEFONI FISSI + CORDLESS *}
	<li class="cat" id="menu-telefoni"><a  class="menu-principale-link" href="{$link->getCategoryLink(115)|escape:'html':'UTF-8'}">Telefoni <i class="fas fa-chevron-down"></i></a>

	{* TELEFONI FISSI *}
	
	
	<label for="show-menu-fissi" class="show-menu-triangle"><img src='{$img_ps_dir}menu_arrow_black.png' alt='Submenu' title='Submenu' /></label>
			<input type="checkbox" class="checkbox-menu-triangle" id="show-menu-fissi" role="button" /> 
			
	<div class="menu-category" style="height:420px; border: 1px solid #f1f2f4; border-radius:6px;">

	<div class="container" style="display:flex; justify-content: space-between; ">
		<div class="row" style="display:flex; flex-direction:column;">
			<div class="submenu" style="padding-bottom:0px;">
				<span class="exh3-category"><a  class="menu-principale-link" href="{$link->getCategoryLink(115)|escape:'html':'UTF-8'}">Telefoni fissi</a></span>
			</div>	

			<div class="container" style="display:flex; justify-content: space-between; width:100%; ">
				<div class="row" style="display:flex; ">
					<div class="submenu" style="width:45%; margin-top: 8px;">
						<span class="exh3-category">Tipo</span>
						<ul class="int-category">
						<li><a href="https://www.ezdirect.it/telefoni-fissi/#/tipo-tptelanalogico">Analogico</a></li>
						<li><a href="https://www.ezdirect.it/telefoni-fissi/#/tipo-tel_specifico_pabx">Specifici (PABX)</a></li>
						<li><a href="https://www.ezdirect.it/telefoni-fissi/#/tipo-tel_voip-tel_voip_wifi">VoIP</a></li>
						<li><a href="https://www.ezdirect.it/telefoni-fissi/#/tipo-tel_usb">USB</a></li>
						<li><a href="https://www.ezdirect.it/telefoni-fissi/#/tipo-accessori">Accessori</a></li>

						</ul>
				
				
					</div>
				
					<div class="submenu" style="width:45%; margin-top: 8px;">
						<span class="exh3-category">Funzioni</span>
						<ul class="int-category">
						<li><a href="https://www.ezdirect.it/telefoni-fissi/#/attacco_cuffia-jack_25_mm-jack_35_mm-plug_rj9-plug_rj9_jack_25_mm">Attacco per cuffia</a></li>
						<li><a href="https://www.ezdirect.it/telefoni-fissi/#/telecamera-si">Telecamera</a></li>
						<li><a href="https://www.ezdirect.it/telefoni-fissi/#/android-si">Android</a></li>
						</ul>

					
					</div>
				</div>
			</div>
			
		</div>

		<div class="row" style="display:flex; flex-direction:column;">

		
			<div class="submenu" style="padding-bottom:0px;">
				<span class="exh3-category"><a  class="menu-principale-link" href="{$link->getCategoryLink(128)|escape:'html':'UTF-8'}">Telefoni Cordless</a></span>
			</div>	

			<div class="container" style="display:flex; justify-content: space-between; width:100%; ">
				<div class="row" style="display:flex; ">
					<div class="submenu" style="width:25%; margin-top: 8px;">
						<span class="exh3-category">Tipo</span>
						<ul class="int-category">
							<li><a href="https://www.ezdirect.it/telefoni-cordless/#/tipo-cordless_tipo_analog">Analogico</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-cordless/#/tipo-cordless_tipo_voip-cordless_tipo_voip_wifi-voip_analogico">VoIP</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-cordless/#/tipo-cordless_tipo_wifi-cordless_tipo_voip_wifi">Wi-fi</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-cordless/#/tipo-cordless_tipo_pabx">Specifici (PABX)</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-cordless/#/tipo-terminali_aggiuntivi">Portatili aggiuntivi</a></span></li>
							<li><a href="https://www.ezdirect.it/telefoni-cordless/#/tipo-ripetitori_e_accessori">Accessori</a></span></li>
							

						</ul>
				
				
					</div>
				 
					<div class="submenu" style="width:25%; margin-top: 8px;">
						<span class="exh3-category">Funzioni</span>
						<ul class="int-category">
							<li><a href="https://www.ezdirect.it/telefoni-cordless/#/touchscreen-si">Touchscreen</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-cordless/#/attacco_cuffia-jack_25_mm-jack_35_mm-attacco_specifico-specifico">Attacco per cuffia</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-cordless/#/viva_voce-vivavoce_si">Viva voce</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-cordless/#/vibrazione-si">Vibrazione</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-cordless/#/ricarica_indipendente_dalla_base-si">Ricarica indipendente dalla base</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-cordless/#/segreteria_telefonica-si">Segreteria telefonica</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-cordless/#/bluetooth_integrato-si">Bluetooth</a></li>

						</ul>
					
					</div>

					<div class="submenu" style="width:25%; padding-left: 60px; margin-top: 8px;">
						<span class="exh3-category" id="produttori" style="padding: 15px 0px ;">Produttori</span>
						
						<ul class="int-category">
							<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-cordless/#/produttore-ascom">Ascom</a></li>
							<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-fissi/#/produttore-avaya">Avaya</a></li>
							<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-fissi/#/produttore-cisco" title="Telefoni fissi Cisco">Cisco</a></li>
							<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-fissi/#/produttore-fanvil" title="Telefoni fissi Fanvil">Fanvil</a></li>
							<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-cordless/#/produttore-gigaset">Gigaset</a></li>
							<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-fissi/#/produttore-grandstream" title="Telefoni con filo Grandstream">Grandstream</a></li>
							<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-fissi/#/produttore-htek">Htek</a></li>
							<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-fissi/#/produttore-mitel_aastra" title="Telefoni fissi Aastra">Mitel Aastra</a></li>
							<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-fissi/#/produttore-panasonic" title="Telefoni fissi Panasonic">Panasonic</a></li>
							<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-fissi/#/produttore-polycom" title="Telefoni con filo Polycom">Polycom</a></li>
							<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-fissi/#/produttore-samsung" title="Telefoni fissi Samsung">Samsung</a></li>
							<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-fissi/#/produttore-snom" title="Telefoni fissi Snom">Snom</a></li>
							<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-cordless/#/produttore-spectralink">Spectralink</a></li>
							<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-fissi/#/produttore-yealink" title="Telefoni con filo Yealink">Yealink</a></li>
						</ul>
						
					</div>

					<div class="cat-separatore" style="width: 25%;">
						<div style="margin-left:25px; padding-top: 10px; margin-top: 18px;">

							<span class="exh3-category special" ><a href="{$link->getCategoryLink(115)|escape:'html':'UTF-8'}" style="margin-top:-10px;color: #ff6600; border:none; font-size: 13px;">Tutti i fissi</a></span>

							<span class="exh3-category special" ><a href="{$link->getCategoryLink(128)|escape:'html':'UTF-8'}" style="margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">Tutti i cordless</a></span>
						
							<span class="exh3-category special"><a href="https://www.ezdirect.it/nuovi-prodotti-telefoni-fissi-voip-cordless-centralini?cat=115" style="margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">Nuovi prodotti</a></span>
				
							<span class="exh3-category special"><a href="https://www.ezdirect.it/prodotti-venduti-migliori-sito-web-ezdirect?cat=115" style="width:110px; margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">I pi&ugrave; venduti</a></span>

							<span class="exh3-category special"><a href="https://www.ezdirect.it/offerte-speciali?category=115" style="margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">Offerte speciali</a></span> 
			
							
						
						
							<span class="exh3-category"><a href='https://www.ezdirect.it/guide/category/5-guide-e-consigli' style='margin-top:-10px; color: #ff6600; border:none; width:110px; font-size: 13px;'>Guide</a></span>
							<ul class="int-categ-marg" style="margin-top:-20px;">
								<li><a href="https://www.ezdirect.it/guide/42-telefoni-voip-guida-alla-scelta-migliore-per-acquisto-ip-sip-codec-linee" title="Telefoni VoIP guida">Telefoni VoIP guida</a></li>
				
								<li><a href="https://www.ezdirect.it/guide/46-skype-cuffie-e-telefoni-accessori-per-pc-usb-compatibili" title="Skype cuffie e telefoni">Skype cuffie e telefoni</a></li>
				
							</ul>
				

						</div>
					</div>
				</div>
			</div>
			
		</div>

		<div class="row">
			
			
						
		
			<div style="clear:both"></div>
		</div>
		
	</div>

	</li>
	
	
	
	{* <div class="submenu">
					<span class="exh3-category">Prezzo</span>
		<ul class="int-category">
		<li><a href="https://www.ezdirect.it/telefoni-fissi/#/price-0-25">Fino a 25 &euro;</a></li>
		<li><a href="https://www.ezdirect.it/telefoni-fissi/#/price-26-50">Da 26 a 50 &euro;</a></li>
		<li><a href="https://www.ezdirect.it/telefoni-fissi/#/price-51-100">Da 51 a 100 &euro;</a></li>
		<li><a href="https://www.ezdirect.it/telefoni-fissi/#/price-101-150">Da 101 a 150 &euro;</a></li>
<li><a href="https://www.ezdirect.it/telefoni-fissi/#/price-150-1000">Oltre 150 &euro;</a></li>

		
		</ul>
			<br />
		
		
		
		
		
		</div> *}
		

		{* TELEFONI CORDLESS *}
	{* <li class="cat" id="menu-cordless"><a  class="menu-principale-link" href="https://www.ezdirect.it/telefoni-cordless/">Telefoni cordless</a>
	
	 <label for="show-menu-cordless" class="show-menu-triangle"><img src='{$img_ps_dir}menu_arrow_black.png' alt='Submenu' title='Submenu' /></label>
			<input type="checkbox" class="checkbox-menu-triangle" id="show-menu-cordless" role="button" /> 
			
		<div class="menu-category">

		<div class="submenu">
		<span class="exh3-category" id="menu-telefoni"><a href="https://www.ezdirect.it/telefoni-cordless">Telefoni fissi</span>
		</div>	 *}
	{* <div class="submenu">
		<span class="exh3-category">Tipo</span>
		<ul class="int-category">
		<li><a href="https://www.ezdirect.it/telefoni-cordless/#/tipo-cordless_tipo_analog">Analogico</a></li>
		<li><a href="https://www.ezdirect.it/telefoni-cordless/#/tipo-cordless_tipo_voip-cordless_tipo_voip_wifi-voip_analogico">VoIP</a></li>
		<li><a href="https://www.ezdirect.it/telefoni-cordless/#/tipo-cordless_tipo_wifi-cordless_tipo_voip_wifi">Wi-fi</a></li>
		<li><a href="https://www.ezdirect.it/telefoni-cordless/#/tipo-cordless_tipo_pabx">Specifici (PABX)</a></li>
		<li><a href="https://www.ezdirect.it/telefoni-cordless/#/tipo-terminali_aggiuntivi">Portatili aggiuntivi</a></span></li>
		<li><a href="https://www.ezdirect.it/telefoni-cordless/#/tipo-ripetitori_e_accessori">Accessori</a></span></li>

		</ul>
	
	
	</div> *}
	{* <div class="submenu">
		<span class="exh3-category">Funzioni</span>
		<ul class="int-category">
		<li><a href="https://www.ezdirect.it/telefoni-cordless/#/touchscreen-si">Touchscreen</a></li>
		<li><a href="https://www.ezdirect.it/telefoni-cordless/#/attacco_cuffia-jack_25_mm-jack_35_mm-attacco_specifico-specifico">Attacco per cuffia</a></li>
		<li><a href="https://www.ezdirect.it/telefoni-cordless/#/viva_voce-vivavoce_si">Viva voce</a></li>
		<li><a href="https://www.ezdirect.it/telefoni-cordless/#/vibrazione-si">Vibrazione</a></li>
		<li><a href="https://www.ezdirect.it/telefoni-cordless/#/ricarica_indipendente_dalla_base-si">Ricarica indipendente dalla base</a></li>
		<li><a href="https://www.ezdirect.it/telefoni-cordless/#/segreteria_telefonica-si">Segreteria telefonica</a></li>
<li><a href="https://www.ezdirect.it/telefoni-cordless/#/bluetooth_integrato-si">Bluetooth</a></li>

		</ul>
	
	
	</div> *}
		{* <div class="submenu">
			<span class="exh3-category">Prezzo</span>
			<ul class="int-category">
				<li><a href="https://www.ezdirect.it/telefoni-cordless/#/price-0-30">Fino a 30 &euro;</a></li>
				<li><a href="https://www.ezdirect.it/telefoni-cordless/#/price-31-50">Da 31 a 50 &euro;</a></li>
				<li><a href="https://www.ezdirect.it/telefoni-cordless/#/price-51-100">Da 51 a 100 &euro;</a></li>
				<li><a href="https://www.ezdirect.it/telefoni-cordless/#/price-100-1000">Oltre 100 &euro;</a></li>

			
			</ul>
		</div> *}

		{* <div class="submenu"  style="width:205px">
		<span class="exh3-category">Produttori</span>
		<ul class="int-category">
		<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-cordless/#/produttore-gigaset">Gigaset</a></li>
		<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-cordless/#/produttore-cisco">Cisco</a>
		<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-cordless/#/produttore-snom">Snom</a></li>
		<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-cordless/#/produttore-yealink">Yealink</a></li>
		<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-cordless/#/produttore-grandstream">Grandstream</a></li>
		<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-cordless/#/produttore-panasonic">Panasonic</a></li>
		<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-cordless/#/produttore-mitel_aastra">Mitel Aastra</a></li>
		<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-cordless/#/produttore-ascom">Ascom</a></li>
		<li style="width:130px"><a href="https://www.ezdirect.it/telefoni-cordless/#/produttore-spectralink">Spectralink</a></li>
		</li>
		</ul>
		<br />
		<strong><a style="font-size:10px; font-weight:normal" href="https://www.ezdirect.it/telefoni-cordless/">Tutti i cordless...</a></strong>
		
		</div> *}
				{* <div class="cat-separatore">
				<div style="margin-left:25px;">
				
					<span class="exh3-category"><a href="https://www.ezdirect.it/nuovi-prodotti-telefoni-fissi-voip-cordless-centralini?cat=128" >Nuovi prodotti</a></span>
		
		<span class="exh3-category"><a href="https://www.ezdirect.it/prodotti-venduti-migliori-sito-web-ezdirect?cat=128" style="width:110px; margin-top:-10px">I pi&ugrave; venduti</a></span>
	
					
				
				
		<span class="exh3-category"><a href='https://www.ezdirect.it/guide/category/5-guide-e-consigli' style='width:110px'>Guide</a></span>
		<ul class="int-categ-marg">
		
		
		<li><a href="https://www.ezdirect.it/guide/46-skype-cuffie-e-telefoni-accessori-per-pc-usb-compatibili" title="Skype cuffie e telefoni">Skype cuffie e telefoni</a></li>
			<li><a href="https://www.ezdirect.it/guide/47-telefoni-cordless-guida-all-acquisto-scelta-per-comprare-miglior-dect-eco" title="Skype cuffie e telefoni">Guida cordless</a></li>
		
		</ul>
		

		</div>
		</div>
						
		
		<div style="clear:both"></div> *}
	
{* 	
	</div>
	</li>
	</li>
	</li> *}
	
	
	
	

	{* CUFFIE *}
	<li class="cat" id="menu-cuffie" style="border-left:0px; margin-left:0px;"><a class="menu-principale-link" title="Cuffie telefoniche" href="{$link->getCategoryLink(106)|escape:'html':'UTF-8'}">Cuffie <i class="fas fa-chevron-down"></i></a>
	
	
	
		<label for="show-menu-cuffie" class="show-menu-triangle"><img src='{$img_ps_dir}menu_arrow_black.png' alt='Submenu' title='Submenu' /></label>
			<input type="checkbox" class="checkbox-menu-triangle" id="show-menu-cuffie" role="button" /> 
	
		<div class="menu-category" style="height:390px; border: 1px solid #f1f2f4; border-radius:6px;">

			<div class="container"  style="display:flex; justify-content:space-around;">
				<div class="row" style="display:flex; flex-direction:column;">
					<div class="submenu" style="width:160px">
						
						<span class="exh3-category"><a class="menu-principale-link" style="font-size:13px;" title="Cuffie telefoniche senza filo DECT e Bluetooth" href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-con_filo">Cuffie con filo</a></span>
						<ul class="int-category">	
							<li><a title="Cuffie filari per telefono" href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-con_filo/utilizzabile_con-telefono_fisso-telefoni_con_presa_jack_25_mm-dispositivi_con_jack_35_mm-telefoni_con_presa_jack_35_mm-telefono_fisso_panasonic">Per telefono fisso</a></li>
							<li><a title="Cuffie filari per cordless" href="https://www.ezdirect.it/cuffie-telefoniche/per-telefono-cordless-dect-gap/#/tipo-con_filo/">Per telefono cordless</a></li>
							<li><a title="Cuffia con microfono per PC" href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-con_filo/utilizzabile_con-pc_usb-pc_usb_bluetooth-pc_scheda_audio">Per PC</a></li>
							<li><a title="Cuffie utilizzabili su pi&ugrave; telefoni o PC" href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-con_filo/utilizzabile_con-telefono_fisso_pc_usb-pc_usb_bluetooth">Multiuso</a></li>
							<li><a title="Cuffie telefoniche ottimizzate per softphone Microsoft Lync" href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-con_filo/ottimizzato_per-ms_skype_for_business">Microsoft Teams</a></li>
									<li><a title="Cuffie compatibili con Skype" href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-con_filo/compatibile_skype-si">Compatibile Skype</a></li>
							<li><a title="Cuffia mono con microfono, singolo auricolare" href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-con_filo/versione-monoauricolare">Monoauricolari</a></li>
							<li><a title="Cuffie microfoniche con doppio auricolare" href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-con_filo/versione-biauricolare">Biauricolari</a></li>
							<li><a title="Cuffia telefonica professionale per uso intensivo" href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-con_filo/utilizzo_suggerito-intensivo_call_center">Per call center</a></li>
							
					
						</ul>
				
				
					</div>
				</div>

				<div class="row" style="display:flex; flex-direction:column;">
					<div class="submenu">	
						<span class="exh3-category"><a class="menu-principale-link" style="font-size:13px;" title="Cuffie telefoniche senza filo DECT e Bluetooth" href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-senza_filo">Cuffie senza filo</a></span>
						<ul class="int-category">
							<li><a title="Cuffia wireless per collegamento a telefoni fissi" href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-senza_filo/utilizzabile_con-telefono_fisso-telefoni_con_presa_jack_25_mm-dispositivi_con_jack_35_mm-telefoni_con_presa_jack_35_mm-telefono_fisso_panasonic">Per telefono fisso</a></li>
							<li><a title="Cuffie filari per cordless" href="https://www.ezdirect.it/cuffie-telefoniche/per-telefono-cordless-dect-gap/#/tipo-senza_filo/">Per telefono cordless</a></li>
							<li><a title="Cuffia senza filo con microfono per softphones su PC Skype VoIP" href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-senza_filo/utilizzabile_con-telefono_fisso_pc_usb-telefono_fisso_pc_usb_bluetooth-fisso_pc-fisso_pc_bluetooth-pc_usb-pc_usb_bluetooth">Per PC</a></li>
							<li><a title="Cuffie con microfono da utilizzare su pi&ugrave; dispositivi, telefono, PC, cellulare, tablet" href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-senza_filo/utilizzabile_con-telefono_fisso_pc_usb-telefono_fisso_pc_usb_bluetooth-pc_usb_bluetooth/multiuso-si">Multiuso</a></li>
							<li><a title="Cuffie telefoniche senza filo compatibili con softphone Microsoft Lync" href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-senza_filo/ottimizzato_per-ms_skype_for_business">Microsoft Teams</a></li>
									<li><a title="Cuffie compatibili con Skype" href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-senza_filo/compatibile_skype-si">Compatibile Skype</a></li>
							<li><a title="Cuffia cordless con microfono e singolo auricolare" href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-senza_filo/versione-monoauricolare">Monoauricolari</a></li>
							<li><a title="Cuffie cordless con microfono, biauricolari" href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-senza_filo/versione-biauricolare">Biauricolari</a></li>
							<li><a title="Cuffie auricolari bluetooth senza filo" href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-senza_filo/utilizzabile_con-bluetooth-telefono_fisso_pc_usb_bluetooth-pc_usb_bluetooth-telefono_fisso_bluetooth">Bluetooth</a></li>

						</ul>
		
	
			
					</div>
				</div>

				<div class="row" style="display:flex; flex-direction:column;">
					<div class="submenu">
		
						<span class="exh3-category"><a style="font-size:13px;" href="https://www.ezdirect.it/cuffie-telefoniche/#/accessori-si">Accessori</a></span>
						<ul class="int-category">
							<li><a title="Cavi" href="https://www.ezdirect.it/cuffie-telefoniche/#/accessori-si/tipo_accessori-cavi">Cavi</a></li>
							<li><a title="Sganciatori" href="https://www.ezdirect.it/cuffie-telefoniche/#/accessori-si/tipo_accessori-sganciatori">Sganciatori</a></li>
							<li><a title="Adattatori" href="https://www.ezdirect.it/cuffie-telefoniche/#/accessori-si/tipo_accessori-adattatori">Adattatori</a></li>
							<li><a title="Copriauricolare" href="https://www.ezdirect.it/cuffie-telefoniche/#/accessori-si/tipo_accessori-copriauricolari">Copriauricolari</a></li>
							<li><a title="Cavi PC" href="https://www.ezdirect.it/cuffie-telefoniche/#/accessori-si/tipo_accessori-cavi_pc">Cavi PC</a></li>
							<li><a title="Cuffia aggiuntiva" href="https://www.ezdirect.it/cuffie-telefoniche/#/accessori-si/tipo_accessori-cuffia_aggiuntiva">Cuffia aggiuntiva</a></li>
							<li><a title="Cavi mobile" href="https://www.ezdirect.it/cuffie-telefoniche/#/accessori-si/tipo_accessori-cavi_mobile">Cavi mobile</a></li>

							

						</ul>
					</div>
				</div>

				<div class="row" style="display:flex; flex-direction:column;">
					<div class="submenu" style="width:115px; ">
						<span class="exh3-category" style="padding-top: 20px">Produttori</span>
						<ul class="int-category">
							<li style="width:130px"><a href="https://www.ezdirect.it/cuffie-telefoniche/#/produttore-ezdirect">Ezdirect</a></li>
							<li style="width:130px"><a href="https://www.ezdirect.it/cuffie-telefoniche/#/produttore-fanvil">Fanvil</a></li>
							<li style="width:130px"><a href="https://www.ezdirect.it/cuffie-telefoniche/#/produttore-jabra">Jabra</a></li>
							<li style="width:130px"><a href="https://www.ezdirect.it/cuffie-telefoniche/#/produttore-plantronics">Poly</a></li>
							<li style="width:130px"><a href="https://www.ezdirect.it/cuffie-telefoniche/#/produttore-sennheiser">Sennheiser - Epos</a></li>
							<li style="width:130px"><a href="https://www.ezdirect.it/cuffie-telefoniche/#/produttore-snom">Snom</a></li>
							<li style="width:130px"><a href="https://www.ezdirect.it/cuffie-telefoniche/#/produttore-yealink">Yealink</a></li>
						
							</li>
						</ul>
						<br />
					
					
					
					</div>
				</div>

				<div class="row" style="display:flex; flex-direction:column;">
					<div class="cat-separatore">
						<div style="margin-left:25px; padding-top: 14px;">

							<span class="exh3-category special" ><a href="{$link->getCategoryLink(106)|escape:'html':'UTF-8'}" style="margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">Tutte le cuffie</a></span>
						
							<span class="exh3-category special"><a href="https://www.ezdirect.it/nuovi-prodotti-telefoni-fissi-voip-cordless-centralini?cat=106" style="margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">Nuovi prodotti</a></span>
				
							<span class="exh3-category special"><a href="https://www.ezdirect.it/prodotti-venduti-migliori-sito-web-ezdirect?cat=106" style="width:110px; margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">I pi&ugrave; venduti</a></span>
			
							<span class="exh3-category special"><a href="https://www.ezdirect.it/offerte-speciali?category=106" style="margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">Offerte speciali</a></span> 
						
						
							<span class="exh3-category"><a href='https://www.ezdirect.it/guide/category/5-guide-e-consigli' style='margin-top:-10px; width:110px; color:#ff6600; border:none; font-size: 13px;'>Guide</a></span>
							<ul class="int-categ-marg" style="margin-top:-20px;">
								<li><a href="https://www.ezdirect.it/guide/20-cuffie-plantronics-jabra-glossario-guida" title="Glossario cuffie">Glossario cuffie</a></li>
								<li><a href="https://www.ezdirect.it/guide/25-cuffie-guida-alla-scelta-telefoniche-con-microfono" title="Cuffie, guida alla scelta">Cuffie guida alla scelta</a></li>
								<li><a href="https://www.ezdirect.it/guide/13-salute-uso-delle-cuffie-telefoniche" title="Salute e uso delle cuffie">Salute e uso delle cuffie</a></li>
								<li><a href="https://www.ezdirect.it/guide/75-compatibilita-cavi-per-cuffie-telefoniche" target="_blank">Compatibilit&agrave; EzDirect</a></li> 
								<li><a href="http://www.it.jabra.com/headsets-and-speakerphones/compatibilityguide" target="_blank">Compatibilit&agrave; Jabra</a></li>
								<li><a href="https://www.plantronics.com/it/it/support/compatibility-guide" target="_blank">Compatibilit&agrave; Plantronics</a></li>
								<li><a href="https://www.eposaudio.com/en/it/enterprise/headsets/compatibility-guide" target="_blank">Compatibilit&agrave; Epos Sennheiser</a></li>
								<li><a href=" https://support.yealink.com/en/portal/compatible" target="_blank">Compatibilit&agrave; Yealink</a></li> 
							</ul>
							

						</div>
				
					</div>
				</div>
			</div>
		
			

		
		{* <div class="submenu" style="width:180px">
		<span class="exh3-category"><a title="Auricolari bluetooth per cellulari smartphones tablets e telefoni vari" href="https://www.ezdirect.it/auricolari-bluetooth-professionali/">Auricolari bluetooth</a></span>
		<ul class="int-category">
		<li><a title="Cuffie stereo bluetooth per musica e chiamate da cellulare e VoIP" href="https://www.ezdirect.it/auricolari-bluetooth-professionali/#/stereo-si">Stereo</a></li>
		<li><a title="Auricolari utilizzabili con pi&ugrave; dispositivi bluetooth" href="https://www.ezdirect.it/auricolari-bluetooth-professionali/#/multipunto_bluetooth-si">Multipunto</a></li>
		<li><a title="Auricolari bluetooth utilizzabili su dispositivi con diversa tecnologia" href="https://www.ezdirect.it/auricolari-bluetooth-professionali/#/multiuso-si">Multiuso</a></li>
		
		<li><a  href="https://www.ezdirect.it/auricolari-bluetooth-professionali/#/ottimizzato_per-ms_skype_for_business">Lync - Skype for Business</a></li>
		
		</ul>
		
		<span class="exh3-category"><a title="Cuffie antirumore passive comunicanti 3M Peltor" href="https://www.ezdirect.it/cuffie-telefoniche/peltor-3m-passive-antirumore-atex/">Cuffie antirumore</a></span>
		<ul class="int-category">
	
		
		</div> *}
		{* <div class="submenu" style="width:130px">
		
						<span title="Filtra per prezzo" class="exh3-category">Prezzo</span>
		<ul class="int-category">
		<li><a href="https://www.ezdirect.it/cuffie-telefoniche/#/price-0-50">Fino a 50 &euro;</a></li>
		<li><a href="https://www.ezdirect.it/cuffie-telefoniche/#/price-51-100">Da 51 a 100 &euro;</a></li>
		<li><a href="https://www.ezdirect.it/cuffie-telefoniche/#/price-101-150">Da 101 a 150 &euro;</a></li>
		<li><a href="https://www.ezdirect.it/cuffie-telefoniche/#/price-151-200">Da 151 a 200 &euro;</a></li>
		<li><a href="https://www.ezdirect.it/cuffie-telefoniche/#/price-200-1100">Oltre 200 &euro;</a></li>
		
		</ul>
		</div> *}
		
			
		
						
		
		<div style="clear:both"></div>
		</div>
	</li>
	
	
	
	{* CONFERENZA *}
	<li class="cat" id="menu-audioconferenza"><a  class="menu-principale-link" href="{$link->getCategoryLink(76)|escape:'html':'UTF-8'}">Conferenza <i class="fas fa-chevron-down"></i></a>

	
	{* AUDIOCONFERENZA E AUDIOGUIDE *}
	{* <li class="cat" id="menu-audioconferenza"><a  class="menu-principale-link" href="https://www.ezdirect.it/audioconferenza-teleconferenza/">Audioconferenza e audioguide</a> *}
	
	 <label for="show-menu-teleconferenza" class="show-menu-triangle"><img src='{$img_ps_dir}menu_arrow_black.png' alt='Submenu' title='Submenu' /></label>
			<input type="checkbox" class="checkbox-menu-triangle" id="show-menu-teleconferenza" role="button" /> 
			
	<div class="menu-category" style="height:375px; border: 1px solid #f1f2f4; border-radius:6px;">

	<div class="container" style="display:flex; justify-content:space-around;">

		{* AUDIOCONFERENZA *}
		<div class="row" style="display:flex; flex-direction:column;">

			<div class="submenu">
				<span class="exh3-category"><a href="{$link->getCategoryLink(75)|escape:'html':'UTF-8'}">Audioconferenza</a></span>
			</div>	
			<div class="submenu">
				<span class="exh3-category">Tipo</span>
				<ul class="int-category">
				
				<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/tipo-audc_anlg">Analogico</a></li>
				<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/tipo-audioconferenze_voip">VoIP</a></li>
				<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/tipo-audc_usb">USB</a></li>
				<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/tipo-multiuso">Multiuso</a></li>
				<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/tipo-multiuso-solo_bluetooth">Bluetooth</a></li>
				</ul>
				
			</div>
	
		</div>

		{* VIDEO CONFERENZA *}
		<div class="row" style="display:flex; flex-direction:column;">

			<div class="submenu">
					<span class="exh3-category"><a href="{$link->getCategoryLink(76)|escape:'html':'UTF-8'}">Videoconferenza</a></span>
					
			</div>	
			

			
			
			<div class="row" style="display:flex;  ">
				<div class="submenu" style="width: 50%; padding-left: 15px;">
						<span class="exh3-category">Tipo</span>
						<ul class="int-category">
						
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-sistema">Sistema</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-videocamera_usb">Videocamera USB</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-clickshare">Clickshare</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-servizio_cloud">Servizio cloud</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-licenza-licenza_maintanance">Licenze</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-accessori-accessori_clickshare">Accessori</a></li>

						</ul>
					</div>

				<div class="submenu" style="  width: 50%;">
					<span class="exh3-category">Funzioni</span>
					<ul class="int-category">
						<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/telecamera-si">Telecamera</a></li>
						<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/zoom-ottico_12x-si-digitale_4x-digitale_5x-ottico_5x-digitale_8x-digitale_3x-15x-ottico_4x-digitale_16x">Zoom</a></li>
						<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/ptz-si">Ptz</a></li>
						<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/hdmi-si-2-1-3">HDMI</a></li>
						<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/vga-si">VGA</a></li>
						<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/monitor_collegabili-2-1">Monitor collegabili</a></li>
						<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/usb-si-1-2-3-4">USB</a></li>
						<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/condivisione_contenuti-si">Condivisione contenuti</a></li>
						<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/punto_punto-si">Punto/Punto</a></li>
						<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/multi_punto-si">Multi/Punto </a></li>

					</ul>
		
		
				</div>
			</div>
			
		</div>

		

		{* VISITE GUIDATE *}
		<div class="row" style="display:flex; flex-direction:column;">

			<div class="submenu">
					<span class="exh3-category"><a href="{$link->getCategoryLink(144)|escape:'html':'UTF-8'}">Visite guidate</a></span>
			</div>	
			
			<div class="row" style="display:flex; ">

				<div class="submenu" style="width:30%; padding-left:15px;">
					<span class="exh3-category">Tipo</span>
					<ul class="int-category">
					
						<li><a href="https://www.ezdirect.it/radioguide-audioguide-visite-guidate-musei/#/tipo_radioguide-via_radio_con_guida">Via radio con guida</a></li>
						<li><a href="https://www.ezdirect.it/radioguide-audioguide-visite-guidate-musei/#/tipo_radioguide-con_file_vocali-file_vocali_mp3_wav">Con file vocali</a></li>
						
					
					</ul>
				</div>

		
				<div class="submenu" style="">
					<span class="exh3-category">Produttori</span>
					<ul class="int-category">
						<li style="width:130px"><a href="https://www.ezdirect.it/radioguide-audioguide-visite-guidate-musei/#/produttore-ezdirect" title="Audioguide e radioguide Ezdirect">Ezdirect</a></li>
						<li style="width:130px"><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/produttore-grandstream" title="Conferenza Grandstream">Grandstream</a></li>
						<li style="width:130px"><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/produttore-jabra" title="Audioconferenza Jabra">Jabra</a></li>
						<li style="width:130px"><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/produttore-konftel" title="Audioconferenza Konftel">Konftel</a></li>
						<li style="width:130px"><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/produttore-panasonic" title="Audioconferenza e videoconferenza Panasonic">Panasonic</a></li>
						<li style="width:130px"><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/produttore-plantronics" title="Audioconferenza Plantronics">Poly</a></li>
						<li style="width:130px"><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/produttore-revolabs" title="Audioconferenza Revolabs">Revolabs</a></li>
						<li style="width:130px"><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/produttore-yealink" title="Conferenza Yealink">Yealink</a></li>
						
						
				
					
					</ul>
				
				</div>

				<div class="cat-separatore">
					<div style=" padding-top: 8px;">

						<span class="exh3-category special" style="padding: 15px 0px 25px 0px;"><a href="{$link->getCategoryLink(75)|escape:'html':'UTF-8'}" style="margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">Tutte le audioconferenze</a></span>

						<span class="exh3-category special" style=""><a href="{$link->getCategoryLink(76)|escape:'html':'UTF-8'}" style="margin-top:-10px; color: #ff6600; border:none; font-size: 13px; width: 70%; ">Tutte le videoconferenze</a></span>
					
						<span class="exh3-category special"><a href="https://www.ezdirect.it/nuovi-prodotti-telefoni-fissi-voip-cordless-centralini?cat=75" style="margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">Nuovi prodotti</a></span>
				
						<span class="exh3-category special"><a href="https://www.ezdirect.it/prodotti-venduti-migliori-sito-web-ezdirect?cat=75" style="width:110px; margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">I pi&ugrave; venduti</a></span>

						<span class="exh3-category special"><a href="https://www.ezdirect.it/offerte-speciali?category=75" style="margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">Offerte speciali</a></span> 
		
						
					
					
						<span class="exh3-category"><a href='https://www.ezdirect.it/guide/category/5-guide-e-consigli' style='margin-top:-10px; width:110px; color: #ff6600; border:none; font-size: 13px;'>Guide</a></span>
						<ul class="int-categ-marg" style="margin-top:-20px;">
							<li><a href="https://www.ezdirect.it/blog/sistemi-e-dispositivi-per-audioconferenza/">Audioconferenza guida</a></li>
				
							<li><a href="https://www.ezdirect.it/guide/46-skype-cuffie-e-telefoni-accessori-per-pc-usb-compatibili" title="Skype cuffie e telefoni">Skype cuffie e telefoni</a></li>
			
						</ul>
		

					</div>
				</div>
			</div>
			

			{* PRODUTTORI *}
		
		</div>
	
		
		

		

		{* NUOVI PRODOTTI E OFFERTE *}
		<div class="row" style="display:flex; flex-direction:column;">

				
						
		
		<div style="clear:both"></div>
		</div>
	</div>
		
	
		
	{* <div class="submenu" >
		<span class="exh3-category"><a href="https://www.ezdirect.it/radioguide-audioguide-visite-guidate-musei/">Visite guidate</a></span>
		<ul class="int-category">
		<li><a href="https://www.ezdirect.it/radioguide-audioguide-visite-guidate-musei/#/tipo_radioguide-via_radio_con_guida">Via radio con guida</a></li>
		<li><a href="https://www.ezdirect.it/radioguide-audioguide-visite-guidate-musei/#/tipo_radioguide-con_file_vocali-file_vocali_mp3_wav">Con file vocali</a></li>
		<li><a style="font-size:13px; font-weight:normal" href="https://www.ezdirect.it/radioguide-audioguide-visite-guidate-musei/">Tutte le audioguide e radioguide...</a></li>
		
		</ul>
	</div> *}

		{* <div class="submenu">
		<span class="exh3-category">Tipo</span>
		<ul class="int-category">
		<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-sistema">Sistema</a></li>
		<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-videocamera_usb">Videocamera USB</a></li>
		<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-clickshare">Clickshare</a></li>
		<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-servizio_cloud">Servizio cloud</a></li>
		<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-licenza-licenza_maintanance">Licenze</a></li>
		<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-accessori-accessori_clickshare">Accessori</a></li>

		</ul>
	
	</div> *}
	
	{* <div class="submenu" style="width:205px">
		<span class="exh3-category">Produttori</span>
		<ul class="int-category">
		

		
		 </ul>
			<p>
		
		
		</p> 
	</div> *}
	
	
	
	
	
	{* <div class="submenu" style="width:205px">
		<span class="exh3-category"><a href="https://www.ezdirect.it/ricetrasmittenti/">Ricetrasmittenti</a></span>
			<ul class="int-category">
		<li><a href="https://www.ezdirect.it/ricetrasmittenti/#/resistente_all_acqua-si">Resistente all'acqua</a></li>
		<li><a href="https://www.ezdirect.it/ricetrasmittenti/#/frequenza-pmr446">PMR 446</a></li>

		</ul>
	</div> *}
	{* <div class="submenu">
		<span class="exh3-category">Produttori</span>
		<ul class="int-category">
		<li style="width:130px"><a href="https://www.ezdirect.it/ricetrasmittenti/#/produttore-midland" title="Ricetrasmittenti Midland">Midland</a></li>
	
		
		</ul>
		<br />
			<p style="font-size:10px">
		<strong><a style="font-weight:normal" href="https://www.ezdirect.it/ricetrasmittenti/">Tutte le ricetrasmittenti...</a></strong>
		
		</p>
	</div> *}
	
	
	
		
	
	

	
	
	
	
	
	
	</div>
	</li>
	
	
	{* VIDEOCONFERENZA *}
		{* <li class="cat" id="menu-videoconferenza"><a  class="menu-principale-link" href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/">Video conferenza</a>
		
		 <label for="show-menu-videoconferenza" class="show-menu-triangle"><img src='{$img_ps_dir}menu_arrow_black.png' alt='Submenu' title='Submenu' /></label>
			<input type="checkbox" class="checkbox-menu-triangle" id="show-menu-videoconferenza" role="button" /> <div class="menu-category"  style="height:320px">
	<div class="submenu">
		<span class="exh3-category">Tipo</span>
		<ul class="int-category">
		<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-sistema">Sistema</a></li>
		<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-videocamera_usb">Videocamera USB</a></li>
<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-clickshare">Clickshare</a></li>
<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-servizio_cloud">Servizio cloud</a></li>
<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-licenza-licenza_maintanance">Licenze</a></li>
<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-accessori-accessori_clickshare">Accessori</a></li>

		</ul>
	
	</div>
	<div class="submenu" >
		<span class="exh3-category">Funzioni</span>
		<ul class="int-category">
		<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/telecamera-si">Telecamera</a></li>
		<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/zoom-ottico_12x-si-digitale_4x-digitale_5x-ottico_5x-digitale_8x-digitale_3x-15x-ottico_4x-digitale_16x">Zoom</a></li>
		<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/ptz-si">Ptz</a></li>
		<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/hdmi-si-2-1-3">HDMI</a></li>
		<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/vga-si">VGA</a></li>
		<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/monitor_collegabili-2-1">Monitor collegabili</a></li>
<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/usb-si-1-2-3-4">USB</a></li>
<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/condivisione_contenuti-si">Condivisione contenuti</a></li>
<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/punto_punto-si">Punto/Punto</a></li>
<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/multi_punto-si">Multi/Punto </a></li>

		</ul>
	
	
	</div>
		<div class="submenu">
						<span class="exh3-category">Prezzo</span>
		<ul class="int-category">
		<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/price-0-100">Fino a 100 &euro;</a></li>
		<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/price-101-500">Da 101 a 500 &euro;</a></li>
		<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/price-501-1000">Da 501 a 1000 &euro;</a></li>
		<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/price-1000-10000">Oltre 1000 &euro;</a></li>

		
		</ul>
	
		
		
		
		
		</div>
		<div class="submenu" style="width:205px">
		<span class="exh3-category">Produttori</span>
		<ul class="int-category">
		<li style="width:130px"><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/produttore-ezdirect">Ezdirect</a></li>
		<li style="width:130px"><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/produttore-huawei">Huawei</a></li>
		<li style="width:130px"><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/produttore-polycom">Polycom</a></li>
		<li style="width:130px"><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/produttore-lifesize">Lifesize</a></li>
		<li style="width:130px"><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/produttore-grandstream">Grandstream</a></li>
		<li style="width:130px"><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/produttore-yealink">Yealink</a></li>
		<li style="width:130px"><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/produttore-tely_labs">Tely</a></li>
		<li style="width:130px"><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/produttore-logitech">Logitech</a></li>
		<li style="width:130px"><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/produttore-3cx">3CX</a></li>
		<li style="width:130px"><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/produttore-konftel">Konftel</a></li>
		<li style="width:130px"><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/produttore-hrt">HRT</a></li>
		<li style="width:130px"><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/produttore-huddly">Huddly</a></li>
		</ul>
	
						<br />
		<strong><a style="font-size:10px; font-weight:normal"  href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/">Tutte le videoconferenze...</a></strong>
		
		</div>
		
				<div class="cat-separatore">
						<div style="margin-left:25px;">
				
					<span class="exh3-category"><a href="https://www.ezdirect.it/nuovi-prodotti-telefoni-fissi-voip-cordless-centralini?cat=76" >Nuovi prodotti</a></span>
		
		<span class="exh3-category"><a href="https://www.ezdirect.it/prodotti-venduti-migliori-sito-web-ezdirect?cat=76" style="width:110px; margin-top:-10px">I pi&ugrave; venduti</a></span>
	
		
		</div>
		</div>
						
		
		<div style="clear:both"></div>
	</div>
	
	</li> *}
	
	</li>

	{* CITOFONI *}

	<li class="cat" id="menu-citofoni"><a  class="menu-principale-link" href="{$link->getCategoryLink(208)|escape:'html':'UTF-8'}">Citofoni <i class="fas fa-chevron-down"></i></a>
	
	 <label for="show-menu-citofoni" class="show-menu-triangle"><img src='{$img_ps_dir}menu_arrow_black.png' alt='Submenu' title='Submenu' /></label>
			<input type="checkbox" class="checkbox-menu-triangle" id="show-menu-citofoni" role="button" /> 
			
	<div class="menu-category" style="height: 200px; border: 1px solid #f1f2f4; border-radius:6px;">
		<div class="container" style="display:flex; justify-content:space-around;">
			<div class="row">
				<div class="submenu">
					<span class="exh3-category">Tipo</span>
					<ul class="int-category">
					<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/tipo-cit_an">Analogico</a></li>
					<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/tipo-voipcitofoni">VoIP</a></li>
					
					</ul>

				</div>
				
				
			</div>

			<div class="row" style="display:flex; flex-direction:column;">
				<div class="submenu">
					<span class="exh3-category">Tasti</span>
					<ul class="int-category">
						<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/tastiera-1_tasto-1_tasto_e_tastiera">1 tasto</a></li>
						<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/tastiera-2_tasti">2 tasti</a></li>
						<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/tastiera-4_tasti">4 tasti</a></li>
						<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/tastiera-6_tasti-6_tasti_e_tastiera">6 tasti</a></li>
							
					</ul>
					
				</div>
			</div>

			<div class="row" style="display:flex; flex-direction:column;">
				<div class="submenu">
					<span class="exh3-category">Telecamera</span>
					<ul class="int-category">
						<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/telecamera-si">S&igrave;</a></li>
						<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/telecamera-opzione">Opzione</a></li>
				
					</ul>
				</div>
			</div>

			<div class="row" style="display:flex; flex-direction:column;">
				<div class="submenu">
					<span class="exh3-category">Rel&egrave;</span>
					<ul class="int-category">
						<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/rele_apriporta-1">1</a></li>
						<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/rele_apriporta-1_1_opz-2">2</a></li>
				
					</ul>
				
				</div>
			</div>

			<div class="row" style="display:flex; flex-direction:column;">
				<div class="submenu">
					<span class="exh3-category">Produttori</span>
					<ul class="int-category">
					<li style="width:130px"><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/produttore-akuvox">Akuvox</a></li>
					<li style="width:130px"><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/produttore-2n">2N</a></li>
					<li style="width:130px"><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/produttore-dahua">Dahua</a></li>
					<li style="width:130px"><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/produttore-fanvil">Fanvil</a></li>
					<li style="width:130px"><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/produttore-tema">Tema</a></li>
					<li style="width:130px"><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/produttore-snom">Snom</a></li>
					</ul>
			
					<br />
				</div>
			</div>

			<div class="row" style="display:flex; flex-direction:column;">
				<div class="cat-separatore" style="margin-left:15px;">
					<div style="margin-left:25px; padding-top: 10px;">

						<span class="exh3-category special"><a href="{$link->getCategoryLink(208)|escape:'html':'UTF-8'}" style="margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">Tutti i citofoni</a></span>
						
						<span class="exh3-category special"><a href="https://www.ezdirect.it/nuovi-prodotti-telefoni-fissi-voip-cordless-centralini?cat=208" style="margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">Nuovi prodotti</a></span>
				
						<span class="exh3-category special"><a href="https://www.ezdirect.it/prodotti-venduti-migliori-sito-web-ezdirect?cat=208" style="width:110px; margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">I pi&ugrave; venduti</a></span>

						<span class="exh3-category special"><a href="https://www.ezdirect.it/offerte-speciali?category=208" style="margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">Offerte speciali</a></span> 
			
					</div>
				</div>
			</div>
		</div>
	
	

	

	{* <div class="submenu">
					<span class="exh3-category">Prezzo</span>
		<ul class="int-category">
		<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/price-0-100">Fino a 100 &euro;</a></li>
		<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/price-101-200">Da 101 a 200 &euro;</a></li>
		<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/price-201-500">Da 201 a 500 &euro;</a></li>
<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/price-501-4000">Oltre 500 &euro;</a></li>

		
		</ul>
	
		
		
		</div> *}
		
		
						
		
		{* <div style="clear:both"></div> *}
	</div>
	</li>
	
	{* VIDEOCAMERE IP NVR *}
	
	

	{* GATEWAY *}

	<li class="cat" id="menu-gsm"><a  class="menu-principale-link" href="{$link->getCategoryLink(27)|escape:'html':'UTF-8'}">Gateway <i class="fas fa-chevron-down"></i></a>

	{* GSM-UMTS Gateway *}
	
	
	
	<label for="show-menu-gsm" class="show-menu-triangle"><img src='{$img_ps_dir}menu_arrow_black.png' alt='Submenu' title='Submenu' /></label>
			<input type="checkbox" class="checkbox-menu-triangle" id="show-menu-gsm" role="button" /> 
			
	<div class="menu-category" style="height: 360px; border: 1px solid #f1f2f4; border-radius:6px;">

		<div class="container" style="display:flex; justify-content:space-around;">
			<div class="row" style="display:flex; flex-direction:column;">
				<div class="submenu">
					<span class="exh3-category"><a  class="menu-principale-link" href="{$link->getCategoryLink(27)|escape:'html':'UTF-8'}">GSM Gateway</a></span>
				</div>	

				<div class="container" style="display: flex; justify-content:space-around; width:100%; ">
					<div class="row" style="display:flex; flex-direction:column;">
						<div class="submenu" style="width:160px">
							<span class="exh3-category">Tipo</span>
							<ul class="int-category">
								<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-gateway_gsm-scheda_gsm">Gateway GSM</a></li>
								<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-gateway_lte_4g">Gateway LTE</a></li>
								<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-telefono_gsm-cellulare_atex-smartphone_atex-tablet_atex-wearable_tablet_atex-terminale_per_vigilanza-tablet_rugged">Dispositivi GSM</a></li>
								<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-tracker_gps">Tracker GPS</a></li>
								<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-router_umts-router_lte_4g">Router</a></li>
								<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-ripetitore_gsm-ripetitore_gsm_umts_lte-ripetitore_lte-ripetitore_lte_gsm-ripetitore_umts-ripetitore_umts_gsm-ripetitore_umts_lte">Ripetitori</a></li>
								<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-cellulare_atex-smartphone_atex-tablet_atex-wearable_tablet_atex">ATEX</a></li>
								<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-accessori_gsm">Accessori</a></li>
								
							</ul>
					
						</div>

						
					</div>

					<div class="row" style="display:flex; flex-direction:column;">
						<div class="submenu" style="width:180px">
							<span class="exh3-category">SIM gestite</span>
							<ul class="int-category">
								<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/quantita_sim_gestite-1">1</a></li>
								<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/quantita_sim_gestite-2">2</a></li>
								<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/quantita_sim_gestite-4-8-16-32">Pi&ugrave; di 2</a></li>
							</ul>
						</div>

						<div class="submenu">
							<span class="exh3-category">Connessione</span>
							<ul class="int-category">
								<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/connessione-isdn">ISDN</a></li>
								<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/connessione-voip-voip_analogica">VoIP</a></li>
								<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/connessione-analogica-voip_analogica">Analogica</a></li>
							</ul>
						</div>
					</div>
				</div>
				
				

				
	
			</div>

			<div class="row" style="display:flex; flex-direction:column;">
				<div class="submenu">
					<span class="exh3-category"><a  class="menu-principale-link" href="{$link->getCategoryLink(46)|escape:'html':'UTF-8'}">VoIP Gateway</a></span>
				</div>

				<div class="container" style="display: flex; justify-content:space-around; width:100%; p">
					<div class="row">
						<div class="submenu" style="width:160px">
							<span class="exh3-category">Tipo</span>
							<ul class="int-category">
								<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-gateway_gsm">Analogico</a></li>
								<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-gateway_umts">ISDN</a></li>
								<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-accessori_gsm">VoIP</a></li>
								<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-telefono_gsm">Ibrido</a></li>
								<li><a href="https://www.ezdirect.it/telefoni-fissi/#/tipo-tel_voip-tel_voip_wifi">Telefoni VoIP</a></li>
								<li><a style="margin-top:-10px" href="https://www.ezdirect.it/gsm-umts-gateway-router/#/connessione-voip-voip_analogica">Gateway GSM VoIP</a></li>
								<li><a style="margin-top:-20px" href="https://www.ezdirect.it/skype-accessori-telefono/">Speciale Skype</a></li>
								<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-tracker_gps">Accessori</a></li>
							</ul>
				
						</div>

						

						
					</div>

					<div class="row" style="display:flex;">
						<div class="submenu">
							<span class="exh3-category">Connessione</span>
							<ul class="int-category">
								<li><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/#/porte_fxo-1-2-4-8-16-24-32-48">Gateway VoIP FXO</a></li>
								<li><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/#/porte_fxs-1-2-4-8-16-24-32-48-fino_a_288">Gateway VoIP FXS</a></li>
								<li><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/#/porte_fxs-1-2-4-16-24-32-48/porte_fxo-1-2-4-8-16-24-32-48">Gateway VoIP FXS + FXO</a></li>
								<li><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/#/porte_isdn-1-2-4-3-5-1_pri-2_pri-4_pri-8">Gateway VoIP ISDN</a></li>
							</ul>
						</div>

						<div class="submenu" style="width:115px">
							<span class="exh3-category" id="produttori" style="padding-bottom:25px;">Produttori</span>
							<ul class="int-category" style="padding: 0;">
								<li style="width:130px"><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/produttore-2n">2N</a></li>
								<li style="width:130px"><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/#/produttore-audiocodes">Audiocodes</a></li>
								<li style="width:130px"><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/#/produttore-cisco">Cisco</a></li>
								<li style="width:130px"><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/produttore-hiboost">Hiboost</a></li>
								<li style="width:130px"><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/#/produttore-patton">Patton</a></li>
								<li style="width:130px"><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/produttore-teltonika">Teltonika</a></li>
								<li style="width:130px"><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/produttore-twig">Twig</a></li>
								<li style="width:130px"><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/#/produttore-welltech">Welltech</a></li>
								<li style="width:130px"><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/produttore-yeastar">Yeastar</a></li>
							</ul>
					
							<br />
									
						
						</div>

						<div class="cat-separatore">
							<div style="margin-left:25px; padding-top: 8px;">

								<span class="exh3-category special"><a href="{$link->getCategoryLink(27)|escape:'html':'UTF-8'}" style="margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">Tutti i GSM</a></span>

								<span class="exh3-category special"><a href="{$link->getCategoryLink(46)|escape:'html':'UTF-8'}" style="margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">Tutti i VoIP</a></span>
								
								<span class="exh3-category special"><a href="https://www.ezdirect.it/nuovi-prodotti-telefoni-fissi-voip-cordless-centralini?cat=27" style="margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">Nuovi prodotti</a></span>
						
								<span class="exh3-category special"><a href="https://www.ezdirect.it/prodotti-venduti-migliori-sito-web-ezdirect?cat=27" style="width:110px; margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">I pi&ugrave; venduti</a></span>

								<span class="exh3-category special"><a href="https://www.ezdirect.it/offerte-speciali?category=27" style="margin-top:-10px; color: #ff6600; border:none; font-size: 13px;">Offerte speciali</a></span> 
					
								<span class="exh3-category special"><a href='https://www.ezdirect.it/guide/category/5-guide-e-consigli' style='margin-top:-10px; width:110px; color: #ff6600; border:none; font-size: 13px;'>Guide</a></span>
								
								<ul class="int-categ-marg" style="margin-top:-20px;">
									<li><a href="https://www.ezdirect.it/guide/44-gateway-gsm-guida-come-funziona-comprare-acquisto-scelta" title="Gateway GSM guida">Gateway GSM guida</a></li>
						
									<li><a href="https://www.ezdirect.it/guide/70-ripetitori-di-segnale-gsm-umts-lte-come-funzionano" title="Guida Ripetitori di segnale GSM UMTS LTE">Guida Ripetitori di segnale GSM UMTS LTE</a></li>	
						
									<li><a href="https://www.ezdirect.it/guide/45-gateway-voip-guida-come-funziona-cos-e-scelta-comprare-fxs-fxo" title="Gateway VoIP guida">Gateway VoIP guida</a></li>
								</ul>
						

							</div>
						</div>
					</div>

					

				</div>	

				

						

				
						
					

			</div>

			

			<div class="row" style="display:flex; flex-direction:column;">
				
			</div>
		</div>
		
	
	
	
	
		
		{* <div class="submenu">
			<span class="exh3-category">Tecnologia</span>
			<ul class="int-category">
				<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-gateway_gsm-accessori_gsm-telefono_gsm-tracker_gsm-telecomando_gsm-ripetitore_gsm-ripetitore_umts_gsm-ripetitore_lte_gsm-ripetitore_gsm_umts_lte">GSM</a></li>
				<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-gateway_umts-router_umts-ripetitore_umts_gsm-ripetitore_gsm_umts_lte-ripetitore_umts">UMTS</a></li>
	
			</ul>
		</div> *}
		
		
		
	
		
		{* <div class="submenu">
			<span class="exh3-category">Funzioni</span>
			<ul class="int-category">
				<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/gestione_fax_diretto-si">Gestione fax</a></li>
				<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/sensori-si_1-si_2-si">Sensori</a></li>
				<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/rele-si_1-si_2-si">Rel&egrave;</a></li>
				<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/avviso_fine_credito-si">Gestione credito</a></li>
			</ul>
		</div>	 *}
		
		
		
	
	
	
		{* <div class="submenu" style="width:130px">
			<span class="exh3-category">Prezzo</span>
			<ul class="int-category">
				<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/price-0-170">Fino a 170 &euro;</a></li>
				<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/price-171-300">Da 171 a 300 &euro;</a></li>
				<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/price-301-500">Da 301 a 500 &euro;</a></li>
				<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/price-501-1000">Oltre 500 &euro;</a></li>

		
			</ul>
	
		
		
		
		
		</div> *}

		
			
		
						
		
		<div style="clear:both"></div>
	
	</div>
	
</li>
	
{* END GSM-UMTS Gateway *}

{* VoIP Gateway *}
	
	
	
	
	


{* END GATEWAY *}
	
	
	
	
	
	
	

	
	
	
	<li class="cat" id="menu-altre"><a  class="menu-principale-link" href="javascript:void(0)"> <!--onclick="document.getElementById('show-menu-altro').click(); "-->Altre categorie <i class="fas fa-chevron-down"></i></a>

	
	
	 <label for="show-menu-altro" class="show-menu-triangle"><img src='{$img_ps_dir}menu_arrow_black.png' alt='Submenu' title='Submenu' /></label>
			<input type="checkbox" class="checkbox-menu-triangle" id="show-menu-altro" role="button" /> 
	
	<div class="menu-category" style="height:210px; border: 1px solid #f1f2f4; border-radius:6px;">

		<div class="container" style="display:flex; justify-content:space-around;">
			<div class="row" style="display:flex; flex-direction:column;">
				<div class="submenu" style="width:210px">
	
					<span class="exh3-category"><a style="font-size:12px;" href="{$link->getCategoryLink(210)|escape:'html':'UTF-8'}">Networking Informatica</a></span>
					
					<span class="exh3-category"><a style="font-size:12px;" href="{$link->getCategoryLink(247)|escape:'html':'UTF-8'}">CTI Client <br /> Softphone VoIP</a></span>
					
					<span class="exh3-category"><a style="font-size:12px;" href="{$link->getCategoryLink(103)|escape:'html':'UTF-8'}">Speciale Skype</a></span>
					
					<span class="exh3-category"><a style="font-size:12px;" href="{$link->getCategoryLink(26)|escape:'html':'UTF-8'}">Controllo costi</a></span>
					
					<span class="exh3-category"><a style="font-size:12px;" href="{$link->getCategoryLink(146)|escape:'html':'UTF-8'}">Usato e ricondizionato</a></span>
				
				
				
				
				</div>
			</div>

			<div class="row" style="display:flex; flex-direction:column;">
				<div class="submenu" style="width:210px">
					<span class="exh3-category"><a style="font-size:12px;" href="{$link->getCategoryLink(243)|escape:'html':'UTF-8'}">Domotica e sicurezza</a></span>
					<span class="exh3-category"><a style="font-size:12px;" href="{$link->getCategoryLink(25)|escape:'html':'UTF-8'}">Registratori</a></span>
					<span class="exh3-category"><a style="font-size:12px;" href="{$link->getCategoryLink(156)|escape:'html':'UTF-8'}">Ricetrasmittenti</a></span>
					<span class="exh3-category"><a style="font-size:12px;" href="{$link->getCategoryLink(250)|escape:'html':'UTF-8'}">Centralino virtuale</a></span>
				
					<span class="exh3-category"><a style="font-size:12px;" href="{$link->getCategoryLink(245)|escape:'html':'UTF-8'}">Cablaggio</a></span>
				
				

					<br />
				
				</div>
			</div>

			<div class="row" style="display:flex; flex-direction:column;">
				<div class="submenu" style="width:210px">
					<span class="exh3-category"><a style="font-size:12px;" href="{$link->getCategoryLink(82)|escape:'html':'UTF-8'}">Gruppi di continuit&agrave;</a></span>
				
					<span class="exh3-category"><a style="font-size:12px;" href="{$link->getCategoryLink(266)|escape:'html':'UTF-8'}">Dispositivi di protezione individuale</a></span>
					
					<span class="exh3-category"><a style="font-size:12px;" href="{$link->getCategoryLink(119)|escape:'html':'UTF-8'}">Supporto e assistenza</a></span>
					
					
					<span class="exh3-category"><a style="font-size:12px;" href="{$link->getCategoryLink(262)|escape:'html':'UTF-8'}">Accessori per ufficio</a></span>
				</div>
			</div>

			<div class="row" style="display:flex; flex-direction:column;">
				<div class="cat-separatore">
					<div style="margin-left:30px;">
						<span class="exh3-category special" style="font-size: 12px;">Assistenza</span>
						<ul class="int-category">
							<li><a href="https://www.ezdirect.it/guide/55-assistenza" title="Assistenza tecnica">Assistenza tecnica</a></li>
							<li><a href="https://www.ezdirect.it/guide/35-modulo-di-richiesta-informazioni-ordine-ezdirect" title="Richiesta documenti contabili">Richiesta documenti</a></li>
							<li><a href="https://www.ezdirect.it/guide/33-assistenza-ordini-spedizioni" title="Assistenza ordini">Assistenza ordini</a></li>
							<li><a href="https://www.ezdirect.it/guide/36-rma-rientro-merce-per-siparazione-e-reso-assistenza-tecnica-" title="RMA">RMA</a></li>
						</ul>
						<span class="exh3-category special"><a href="https://www.ezdirect.it/sitemap" style=" margin-top:-10px; color: #ff6600; border:none;  font-size: 12px;">Mappa del sito</a></span>
					</div>
					
				
					
				</div>

			</div>
		</div>
	
	
	
	{* <div class="submenu">
	<span class="exh3-category"><a href="https://www.ezdirect.it/droni-industriali-e-professionali/">Droni industriali e professionali</a></span>
		
		</div> *}
	{* <div class="submenu" style="width:165px">
		&nbsp;
		
	</div> *}
				
	
						
		
	{* <div style="clear:both"></div> *}
	</div>
	

	
	
	</li>
	</ul>
	
	</div> <!-- fine menu-blu -->
	</div> <!-- fine menu-blu-outer -->


	


	
	{if !$content_only}
		{if isset($restricted_country_mode) && $restricted_country_mode}
		<div id="restricted-country">
			<p>{l s='You cannot place a new order from your country.'} <span class="bold">{$geolocation_country}</span></p>
		</div>
		{/if}
		<div id="page">
		
		
		
		
		<!--BANNER CON IMMAGINI SCORREVOLI-->
			{if $page_name == 'index'}
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner">
					<div class="item active">
					<img src="{$img_ps_dir}banner-fanvil.png" alt="Banner Fanvil">
					</div>

					<div class="item">
					<img src="{$img_ps_dir}yealink-banner.jpg" alt="Banner Yealink">
					</div>

					<div class="item">
					<img src="{$img_ps_dir}yeastar.jpg" alt="Banner Yeastar">
					</div>
				</div>

				<!-- Left and right controls -->
				<a class="left carousel-control" href="#myCarousel" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" style="color:#cacaca;"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#myCarousel" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" style="color:#cacaca;"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
			{/if}

			<!--END BANNER CON IMMAGINI SCORREVOLI-->
	
	
	{if $smarty.server.PHP_SELF|basename == 'index.php'}
	
		<!-- <hr class="separation" /> -->
		
	
		{* <div id="home-title-outer" style="background-color: #4a62c5; padding:20px 0; width:100%; height:80px; text-align:center;">
			<div class="container" id="home-title">
				<h1 class='home' style='font-size:14px; color:white;'><strong>EzDirect</strong> con oltre 30.000 prodotti a catalogo e 50.000 clienti soddisfatti è dal 1993 il <strong>leader nella vendita online di Telefonia Professionale.</strong> </h1>
			</div>
		</div> *}
		{if $page_name == 'index'}
		<div id="home-title-outer" style=" padding:20px 0; width:100%; height:auto;">
			<div id="home-title">
				<div class="row">
					<div class="col-sm-3" id="orange_section">
						<img src='{$img_ps_dir}spedizionegratuita.png' alt='Spedizione' title='Spedizione' />
						<h5><strong>Spedizione gratuita</strong></h5>
						<p>Per ordini pari o superiori a 189€</p>
					</div>

					<div class="col-sm-3" id="orange_section">
						<img src='{$img_ps_dir}consulenza.png' alt='Consulenza' title='Consulenza' />
						<h5><strong>Consulenza e assistenza</strong></h5>
						<p>Assistenza specializzata</p>
					</div>

					<div class="col-sm-3" id="orange_section">
						<img src='{$img_ps_dir}spedizione24.png' alt='Spedizione24' title='Spedizione24' />
						<h5><strong>Spedizione in 24/48h</strong></h5>
						<p>Per i prodotti a stock</p>
					</div>

					<div class="col-sm-3" id="orange_section">
						<img src='{$img_ps_dir}prodotti.png' alt='Prodotti' title='Prodotti' />
						<h5><strong>Migliaia di prodotti</strong></h5>
						<p>oltre 30.000 prodotti a catalogo</p>
					</div>
				</div>
			</div>
		</div>

		{/if}
		
		
	
	
	{else}
		
		<!--	<hr class="separation" /> -->

		
		
		
	{/if}
	
	
				
						{if $smarty.get.id_category == 250 || $default_cat == 250}
						<img src="https://www.ezdirect.it/img/prova-gratuita-v.png" id="preventivo_v" style="cursor:pointer; position:fixed; bottom:35%; right:0px; z-index:999999" onclick="$('#id01').show(); $('#preventivo_v').hide();" />
						{else}
						<img src="https://www.ezdirect.it/img/contattaci-v.png" id="preventivo_v" style="cursor:pointer; position:fixed; bottom:35%; right:0px; z-index:999999" onclick="$('#id01').show(); $('#preventivo_v').hide();" />
						{/if}
						<div id="id01" class="modal-cv" {if $smarty.get.contact == "y"} {else} style="display:none" {/if}>
  
						  <form class="modal-content-cv animatemodal-cv" id="contatto_form_n" method="post" action="/modules/formprevendita/form.php?step=2&cv=y">
							<div class="imgcontainermodal-cv">
							  <span onclick="$('#id01').hide(); $('#preventivo_v').show();" class="closemodal-cv" title="Close Modal">&times;</span>
							</div>

							<div class="containermodal-cv">
								<div style='font-size:13px'>
								<p style='text-align:center; font-size:11px'><strong style="font-size:22px">{if $smarty.get.id_category == 250 || $default_cat == 250}Prova gratuita{else}Contattaci{/if}</strong></p>
								<br />Fornisci il tuo recapito e ti chiameremo per fornirti tutti i chiarimenti e predisporre insieme una quotazione su misura. <br /><br />
								{if $smarty.get.id_category == 250 || $default_cat == 250}<strong>Sei gi&agrave; nostro cliente?</strong><br />
								Desideri provare subito il servizio EzCloud senza nessun impegno di attivazione o acquisto? Attiveremo un centralino virtuale di test per 15 giorni.<br /><br />{/if}
								</div>
								<div id="cv-form" style='color:#ebebeb'>
							  <input style="margin-left:0px; margin-right: 8px;" class="input-modal-cv" type="radio" name="az[]" onclick="$('#comunica_esigenze').show();" value="az" required>Cliente finale
							  <input class="input-modal-cv" style=" margin-right: 8px;" type="radio" name="az[]" onclick="$('#comunica_esigenze').hide();" value="riv" required>Rivenditore
							  <br />
							  <table style="width:100%">
							 <tr><td>
							  <input class="input-modal-cv" placeholder="Nome*" type="text" name="firstname" required></td></tr>

							  <tr></td><td>
							  <input class="input-modal-cv" placeholder="Cognome*" type="text" name="lastname" required></td></tr>
								
								<tr><td>
							  <input class="input-modal-cv" placeholder="Azienda*" type="text"  name="company" required></td></tr>
							  
							  <tr><td>
							  <input class="input-modal-cv" placeholder="Email*" type="email"  name="email" required></td></tr>
							  
							 <tr><td>
							  <input class="input-modal-cv" placeholder="Telefono*" type="text" name="phone" required></td></tr>
							  
							  <tr><td>
							  <input class="input-modal-cv" placeholder="Partita IVA" type="text" name="vat_number"></td></tr></table>
							  <div id="comunica_esigenze" style="color:#000; background-color:#fff;
    padding: 4px 7px;
    margin: 8px 0;">
							  <strong>Comunicaci le tue esigenze{if $smarty.get.id_category == 250 || $default_cat == 250}{else}*{/if}</strong><br />
							  {if $smarty.get.id_category == 250 || $default_cat == 250}
							 <table><tr><td style="width:80%"> Quante linee hai?</td><td style="text-align:right"> <input style='' class="input-modal-cv" type="text"  name="linee"></td></tr>
							 <tr><td style="width:80%">  Quanti interni? </td><td style="text-align:right"> <input style='' class="input-modal-cv" type="text" name="interni" ></td></tr></table>
							
							 <label for="uname"><b>Descrivi esigenze e risorse disponibili*:</b></label><br />
							 {/if}
							  <input type="hidden" name="category" value="{$smarty.get.id_product}" />
							 <input type="hidden" name="source" value="https://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}" />
							  <textarea class="input-modal-cv" name="message" required></textarea></div>
							  
							  <script src="https://www.google.com/recaptcha/api.js" async defer></script>
							   <div class="g-recaptcha" data-sitekey="6LciANsZAAAAALhFLBBxrvJJICdKqsPVxXHfPh2i" style="width:80%"></div>
							  <input style="margin-left:0px" type="checkbox" name="acconsento" required /> Spuntando questa casella acconsento di contattarmi in merito a questo prodotto/prova e al trattamento dei dati personali secondo la nuova normativa. <a style='color:#fff' href='https://www.ezdirect.it/guide/6-informativa-sulla-privacy' target='_blank'>Clicca qui per visualizzare l'informativa sulla privacy</a>
							 <!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->
							  <!-- <div class="g-recaptcha" data-sitekey="6Lea5X8UAAAAAK5ra4seLbimZp3Z3s_Z8-sk8UGW" data-size="invisible"></div>-->
							  <button class="input-modal-cv" id="submitMessage" name="submitMessage" type="submit">Invia richiesta</button>
							  <script>
								$('#contatto_form_n').submit(function(event) {
									console.log('validation completed.');

									event.preventDefault(); //prevent form submit before captcha is completed
									grecaptcha.execute();
								});

								onCompleted = function() {
									console.log('captcha completed.');
								} 
							</script>
							  

							</div>
							</div>
							
						  </form>
						</div>

			<!-- Header -->

			
			
	
				{if $page_name == 'order-opc'  || isset($smarty.get.letter) || ($page_name == 'category' && $smarty.get.id_category == 250)}
			
				{if $smarty.get.id_category == 250}
				<!-- Modal Thickbox content -->
					<script type="text/javascript">
					// <![CDATA[
						ThickboxI18nImage = 'Immagine';
						ThickboxI18nOf = 'di';
						ThickboxI18nClose = '<img src="https://www.ezdirect.it/themes/ezdirect/img/icon/icon_delete.gif" alt="Chiudi" title="Chiudi" />';
						ThickboxI18nOrEscKey = '';
						ThickboxI18nNext = 'Avanti &gt;';
						ThickboxI18nPrev = '&lt; Indietro';
						tb_pathToImage = 'https://www.ezdirect.it/img/loadingAnimation.gif';
					//]]>
					</script>
							<div id="checkoutorcontinue" style="display:none; height:150px">
								
								<div id="thickbox_product_details" class="cart_navigation">
								</div>
								<br /><br />
								<table width="100%">
									<TR>
										<TD style="text-align:left">
										<a href="#" onclick="tb_remove(); return false;" class="button_large" style="padding:5px; height:23px; width:120px; text-decoration:none" title="{l s='Continue Shopping' mod='blockcart'}">{l s='Continue Shopping' mod='blockcart'}</a>
										</TD>
										<TD style="text-align:right">
										<a href="{$base_dir_ssl}order.php?step=1" id="button_order_cart" class="button_large"  style="padding:5px; width:120px; height:23px; text-decoration:none"  title="{l s='Check out' mod='blockcart'}">{l s='Check out' mod='blockcart'}</a>
										</TD>
									</TR>
								</table>
								
							</div>
							<a class="thickbox" style="display:none;" href="#TB_inline?width=300&height=150&inlineId=checkoutorcontinue" id="checkoutthickbutton">&nbsp;</a>
					<!-- /Modal Thickbox content -->
				{/if}
				{else}

				{if $page_name == 'category'}
				<div id="columns" style="display: flex; overflow-x : hidden;">
								<!-- Left -->
							
									<div id="left_column" class="column">
											
										{$HOOK_LEFT_COLUMN}
									</div>
								
								<!-- Center -->
								<div id="center_column">
								
								
				{/if}


				
				
				
			<div id="columns" >
				{* <!-- Left -->
			
					<div id="left_column" class="column">
							
						{$HOOK_LEFT_COLUMN}
					</div> *}
				
				<!-- Center -->
				<div id="center_column">
				
				
				
				{/if}

				
				{/if}

				

				
{else}
{include file="$tpl_dir./landing_header.tpl"}



{/if}



	
</body>