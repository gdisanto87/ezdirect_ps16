{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='Your shopping cart'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}
<div style="font-family:Verdana;">
<h1 id="cart_title" style="color: #000099;
	font-size: 18px;
	text-align: left; font-weight: 600; padding: 20px 0;">{l s='Il tuo carrello'}</h1>

{assign var='current_step' value='summary'}
{include file="$tpl_dir./errors.tpl"}



{if isset($empty)}
	<p class="warning">{l s='Your shopping cart is empty.'}</p>
{elseif $PS_CATALOG_MODE}
	<p class="warning">{l s='This store has not accepted your new order.'}</p>
{else}
	<script type="text/javascript">
	// <![CDATA[
	var currencySign = '{$currencySign|html_entity_decode:2:"UTF-8"}';
	var currencyRate = '{$currencyRate|floatval}';
	var currencyFormat = '{$currencyFormat|intval}';
	var currencyBlank = '{$currencyBlank|intval}';
	var txtProduct = "{l s='product'}";
	var txtProducts = "{l s='products'}";
	// ]]>
	</script>
	<p style="display:none" id="emptyCartWarning" class="warning">{l s='Your shopping cart is empty.'}</p>
<p>{l s='Your shopping cart contains'} <span id="summary_products_quantity">{$productNumber} {if $productNumber == 1}{l s='product'}{else}{l s='products'}{/if}</span></p>



{include file="$tpl_dir./order-steps.tpl"}
<div class="shopping_cart_table">

	<table id="cart_summary" style=" padding:5px; border: 1px solid #e5e8eb; border-top: none;" class="std">
		<thead>
			<tr style="display:none;">
				<th style="background-image: none; background-color: #8095c4; padding-left: 10px;" class="cart_product first_item">{l s='Product'}</th>
				<th style="background-image: none; background-color: #9baad1; padding-left: 10px;" class="cart_description item">{l s='Description'}</th>
				<th style="background-image: none; background-color: #8095c4; padding-left: 10px;" class="cart_ref item">{l s='Ref.'}</th>
				<th style="background-image: none; background-color: #9baad1; padding-left: 10px;" class="cart_availability item">{l s='Avail.'}</th>
				<th style="background-image: none; background-color: #8095c4; padding-left: 10px;" class="cart_unit item">{l s='Unit price'}</th>
				<th style="background-image: none; background-color: #9baad1; padding-left: 10px;" class="cart_quantity item">{l s='Delete'}</th>
				<th style="background-image: none; background-color: #8095c4; padding-left: 10px;" class="cart_quantity item">{l s='Qty'}</th>
				<th style="background-image: none; background-color: #9baad1; padding-left: 10px;" class="cart_total last_item">{l s='Total'}</th>
			</tr>
		</thead>
		
		<tbody>
		{foreach from=$products item=product name=productLoop}
			{assign var='productId' value=$product.id_product}
			{assign var='productAttributeId' value=$product.id_product_attribute}
			{assign var='quantityDisplayed' value=0}
			{* Display the product line *}
			{include file="$tpl_dir./shopping-cart-product-line.tpl"}
			{* Then the customized datas ones*}
			{if isset($customizedDatas.$productId.$productAttributeId)}
				{foreach from=$customizedDatas.$productId.$productAttributeId key='id_customization' item='customization'}
					<tr style="border-bottom:1px solid #f4f4f4;" id="product_{$product.id_product}_{$product.id_product_attribute}_{$id_customization}" class="alternate_item cart_item">
						<td colspan="5">
							{foreach from=$customization.datas key='type' item='datas'}
								{if $type == $CUSTOMIZE_FILE}
									<div class="customizationUploaded">
										<ul class="customizationUploaded">
											{foreach from=$datas item='picture'}<li><img src="{$pic_dir}{$picture.value}_small" alt="" class="customizationUploaded" /></li>{/foreach}
										</ul>
									</div>
								{elseif $type == $CUSTOMIZE_TEXTFIELD}
									<ul class="typedText">
										{foreach from=$datas item='textField' name='typedText'}<li>{if $textField.name}{$textField.name}{else}{l s='Text #'}{$smarty.foreach.typedText.index+1}{/if}{l s=':'} {$textField.value}</li>{/foreach}
									</ul>
								{/if}
							{/foreach}
						</td>
						<td class="cart_quantity">
							<div style="float:right">
								<a rel="nofollow" id="{$product.id_product}_{$product.id_product_attribute}_{$id_customization}" href="{$link->getPageLink('cart.php', true)}&delete&amp;id_product={$product.id_product|intval}&amp;ipa={$product.id_product_attribute|intval}&amp;id_customization={$id_customization}&amp;token={$token_cart}"><img src="{$img_dir}icon/delete.gif" alt="{l s='Delete'}" title="{l s='Delete this customization'}" width="18" height="18" class="icon" /></a>
							</div>
							<div id="cart_quantity_button" style="float:left">
							<a rel="nofollow" id="cart_quantity_up_{$product.id_product}_{$product.id_product_attribute}_{$id_customization}" href="{$link->getPageLink('cart.php', true)}?add&amp;id_product={$product.id_product|intval}&amp;ipa={$product.id_product_attribute|intval}&amp;id_customization={$id_customization}&amp;token={$token_cart}" title="{l s='Add'}"><img src="{$img_dir}icon/quantity_up.gif" alt="{l s='Add'}" width="14" height="9" /></a><br />
							{if $product.minimal_quantity < ($customization.quantity -$quantityDisplayed) OR $product.minimal_quantity <= 1}
							<a rel="nofollow" id="cart_quantity_down_{$product.id_product}_{$product.id_product_attribute}_{$id_customization}" href="{$link->getPageLink('cart.php', true)}?add&amp;id_product={$product.id_product|intval}&amp;ipa={$product.id_product_attribute|intval}&amp;id_customization={$id_customization}&amp;op=down&amp;token={$token_cart}" title="{l s='Subtract'}">
								<img src="{$img_dir}icon/quantity_down.gif" alt="{l s='Subtract'}" width="14" height="9" />
							</a>
							{else}
							<a style="opacity: 0.3;" id="cart_quantity_down_{$product.id_product}_{$product.id_product_attribute}_{$id_customization}" href="#" title="{l s='Subtract'}">
								<img src="{$img_dir}icon/quantity_down.gif" alt="{l s='Subtract'}" width="14" height="9" />
							</a>
							{/if}
							</div>
							<input type="hidden" value="{$customization.quantity}" name="quantity_{$product.id_product}_{$product.id_product_attribute}_{$id_customization}_hidden"/>
							<input size="2" type="text" value="{$customization.quantity}" class="cart_quantity_input" name="quantity_{$product.id_product}_{$product.id_product_attribute}_{$id_customization}"/>
						</td>
						<td class="cart_total"></td>
					</tr>
					{assign var='quantityDisplayed' value=$quantityDisplayed+$customization.quantity}
				{/foreach}
				{* If it exists also some uncustomized products *}
				{if $product.quantity-$quantityDisplayed > 0}{include file="$tpl_dir./shopping-cart-product-line.tpl"}{/if}
			{/if}
		{/foreach}
		</tbody>
	{if sizeof($discounts)}
		<tbody>
		{foreach from=$discounts item=discount name=discountLoop}
			<tr class="cart_discount {if $smarty.foreach.discountLoop.last}last_item{elseif $smarty.foreach.discountLoop.first}first_item{else}item{/if}" id="cart_discount_{$discount.id_discount}">
				<td class="cart_discount_name" colspan="1">{$discount.name}</td>
				<td class="cart_discount_name" colspan="1">
				{if $prodotti_con_sconto}
					{assign var='i' value=0}
					
					{l s='Discount applied to: '}
					{foreach from=$prodotti_con_sconto item=prodotto_con_sconto}
						{$i = $i+1}
						{$prodotto_con_sconto}
						{if $i == ($prodotti_con_sconto|@count)}{else} - {/if} 
						
					{/foreach}
				{else}
				{/if}
				</td>
				<td class="cart_discount_description" colspan="3">{$discount.description}</td>
				<td class="cart_discount_delete"><a href="{if $opc}{$link->getPageLink('order-opc.php', true)}{else}{$link->getPageLink('order.php', true)}{/if}&deleteDiscount={$discount.id_discount}" title="{l s='Delete'}"><img src="{$img_dir}icon/delete.gif" alt="{l s='Delete'}" class="icon" width="18" height="18" /></a></td>
				<td class="cart_discount_price"><span class="price-discount">
					{if $discount.value_real > 0}
						{if !$priceDisplay}{displayPrice price=$discount.value_real*-1}{else}{displayPrice price=$discount.value_tax_exc*-1}{/if}
					{/if}
				</span></td>
			</tr>
		{/foreach}
		</tbody>
	{/if}
	</table>

		<div  id="riepilogo-carrello">

		<div class="row" id="codice-sconto">
				<div style="background-color: #f4f4f4; padding: 20px; border: 1px solid #e5e8eb; margin-bottom: 30px;">
					<h5 style="    padding: 20px 30px; font-size: 16px; text-align: center;">Hai un codice sconto?</h5>

					<div id="coupon-input">
						<input style="    color: #f4f4f4; padding: 8px;  " for="discount-code" placeholder="Inserisci il tuo codice">
						<button style="    background-color: #ef6018; color: white; border: none; width:100%; padding: 5px;">Applica codice</button>
					</div>
				</div>

				
				
			</div>

		<div>
			<div style="    padding: 20px; background-color: #f4f4f4; display: flex; flex-direction: column-reverse; border: 1px solid #e5e8eb;">
				<tr>
				<td colspan="3">
					<table>
						<tbody>
							<tr>
								{if $use_taxes}
								{if $priceDisplay}
								<tr class="cart_total_price">
								<td colspan="7" style="padding: 5px 30px 10px 0px;"> <span>{l s='Importo prodotti'} </span>{if $display_tax_label}  {l s='(iva escl.)'}{/if}{l s=':'}</td>
								<td class="price" style="font-size:16px; text-align: right;" id="total_product">{displayPrice price=$total_products}</td>
							</tr>
							{else}
							<tr class="cart_total_price">
								<td colspan="7" style="padding: 5px 30px 10px 0px;"><span>{l s='Importo prodotti'} </span>{if $display_tax_label} {l s='(iva incl.)'}{/if}{l s=':'}</td>
								<td class="price" style="font-size:16px; text-align: right;" id="total_product">{displayPrice price=$total_products_wt}</td>
							</tr>
							{/if}
							{else}
							<tr class="cart_total_price">
								<td colspan="7" style="padding: 5px 30px 10px 0px;"><span>{l s='Importo prodotti'}</span></td>
								<td class="price" style="font-size:16px; text-align: right;" id="total_product">{displayPrice price=$total_products}</td>
							</tr>
							{/if}
							<tr class="cart_total_voucher" {if $total_discounts == 0}style="display: none;"{/if}>
								<td colspan="7" style="padding: 5px 30px 10px 0px;">
								{if $use_taxes}
									{if $priceDisplay}
										<span >{l s='Total vouchers'}</span>{if $display_tax_label} {l s='(iva escl.)'}{/if}{l s=':'}
									{else}
										<span >{l s='Total vouchers'}</span>{if $display_tax_label} {l s='(iva incl.)'}{/if}{l s=':'}
									{/if}
								{else}
									<span >{l s='Total vouchers:'}</span>
								{/if}
								</td>
								<td class="price-discount" id="total_discount">
								{if $use_taxes}
									{if $priceDisplay}
										{displayPrice price=$total_discounts_tax_exc}
									{else}
										{displayPrice price=$total_discounts}
									{/if}
								{else}
									{displayPrice price=$total_discounts_tax_exc}
								{/if}
							</tr>

							<tr class="cart_total_voucher" {if $total_wrapping == 0}style="display: none;"{/if}>
								<td colspan="7" style="padding: 5px 30px 10px 0px;">
								{if $use_taxes}
									{if $priceDisplay}
										<span >{l s='Total gift-wrapping'}</span>{if $display_tax_label} {l s='(iva escl.)'}{/if}{l s=':'}
									{else}
										<span >{l s='Total gift-wrapping'}</span>{if $display_tax_label} {l s='(iva incl.)'}{/if}{l s=':'}
									{/if}
								{else}
									<span >{l s='Total gift-wrapping:'}</span>
								{/if}
								</td>
								<td class="price-discount" id="total_wrapping">
								{if $use_taxes}
									{if $priceDisplay}
										{displayPrice price=$total_wrapping_tax_exc}
									{else}
										{displayPrice price=$total_wrapping}
									{/if}
								{else}
									{displayPrice price=$total_wrapping_tax_exc}
								{/if}
								</td>
							</tr>
							{if $use_taxes}
								{if $priceDisplay}
									<tr class="cart_total_delivery">
										<td colspan="7" style="padding: 5px 30px 10px 0px;"><span>{l s='Spedizione'}</span>{if $display_tax_label} {l s='(iva escl.)'}{/if}{l s=':'}</td>
										<td class="price" syle="text-align: right;" id="total_shipping">{displayPrice price=$shippingCostTaxExc}</td>
									</tr>
								{else}
									<tr class="cart_total_delivery">
										<td colspan="7" style="padding: 5px 30px 10px 0px;"><span >{l s='Spedizione'}</span>{if $display_tax_label} {l s='(iva incl.)'}{/if}{l s=':'}</td>
										<td class="price" style="text-align: right;" id="total_shipping" >{displayPrice price=$shippingCost}</td>
									</tr>
								{/if}
							{else}
								<tr class="cart_total_delivery">
									<td colspan="7" style="padding: 5px 30px 10px 0px;"><span >{l s='Spedizione'}</span></td>
									<td class="price" style="text-align: right;" id="total_shipping" >{displayPrice price=$shippingCostTaxExc}</td>
								</tr>
							{/if}

							{if $use_taxes}
							<tr class="cart_total_price">
								<td colspan="7" style="padding: 5px 30px 10px 0px;">
									{if $display_tax_label}
										<span >{l s='Totale'}</span>  {l s='(iva escl.):'}
									{else}
										<span >{l s='Subtotale'}</span>
									{/if}
								</td>
								<td class="price" style="font-size:16px; text-align: right;" id="total_price_without_tax">{displayPrice price=$total_price_without_tax}</td>
							</tr>
							<tr class="cart_total_tax" style="border-bottom: 1px solid #d6d3d3;">
								<td colspan="7" style="padding: 5px 30px 10px 0px;">
									{if $display_tax_label}
										<span >{l s='I.V.A.*'}</span>
									{else}
										<span >{l s='I.V.A.*'}</span>
									{/if}
								</td>
								<td class="price" style="text-align: right;" id="total_tax">{displayPrice price=$total_tax}</td>
							</tr>
							<tr class="cart_total_price">
								<td colspan="7" style="padding: 15px 30px 10px 0px;">
									{if $display_tax_label}
										<span >{l s='Totale'}</span>  {l s='(iva incl.):'}
									{else}
										<span >{l s='Totale'}</span>
									{/if}
									
								</td>
								<br /><span style="font-size: 10px;">*I.V.A. dove applicabile</span>
								<td class="price" style="font-size:25px; text-align: right;  font-weight:600; color: #b7160f;" id="total_price">{displayPrice price=$total_price}</td>
							</tr>
							{else}
							<tr class="cart_total_price">
								<td colspan="7" style="padding: 5px 30px 10px 0px;"><span >{l s='Totale'}</span></td>
								<td class="price" style="font-size:25px; text-align: right; font-weight:600; color: #b7160f;" id="total_price">{displayPrice price=$total_price_without_tax}</td>
							</tr>
							{/if}
							<tr class="cart_free_shipping" {if $free_ship <= 0 || $isVirtualCart} style="display: none;" {/if}>
									<td colspan="7" style="padding: 5px 30px 10px 0px;" style="white-space: normal;">{l s='Remaining amount to be added to your cart in order to obtain free shipping:'}</td>
									<td id="free_shipping" class="price">{displayPrice price=$free_ship}</td>
							</tr>

							<p class="cart_navigation" >
								{if !$opc}<a href="{$link->getPageLink('order.php', true)}?step=1{if $back}&amp;back={$back}{/if}" class="exclusive" title="{l s='Next'}" style="padding: 10px 10px; font-size:12px; height: 35px;">{l s='Next'} &raquo;</a>{/if}
								<a href="{if (isset($smarty.server.HTTP_REFERER) && strstr($smarty.server.HTTP_REFERER, $link->getPageLink('order.php'))) || !isset($smarty.server.HTTP_REFERER)}{$link->getPageLink('index.php')}{else}{$smarty.server.HTTP_REFERER|escape:'htmlall':'UTF-8'|secureReferrer}{/if}" class="button_large" title="{l s='Continua gli acquisti'}" style="height: 35px; 
								padding: 10px 10px; font-size:12px; background-color: #ef6018;">&laquo; {l s='Continua gli acquisti'}</a>
							</p>

							
						</tbody>
					</table>
				</td>
			</tr>

		
			</div>

			{* <div style="height:fit-content; text-align:center;">
			
				<img src='{$img_dir}carte-footer.jpg' alt='Carte' title='Carte' />
			</div> *}

		</div>
			
			
				
			
		</div>
</div>
<br /><br />
{if $id_group == 3  || $id_group == 10 || $id_group == 12}
{else}
<div class="shopping_cart_table">
{if $voucherAllowed}
<div id="cart_voucher" class="table_block" style="width:700px; border:0px" >
	{if isset($errors_discount) && $errors_discount}
		<ul class="error" >
		{foreach from=$errors_discount key=k item=error}
			<li>{$error|escape:'htmlall':'UTF-8'}</li>
		{/foreach}
		</ul>
	{/if}
	<form action="{if $opc}{$link->getPageLink('order-opc.php', true)}{else}{$link->getPageLink('order.php', true)}{/if}" method="post" id="voucher" style="border:0px" >
		<fieldset style="width:700px; border:0px; margin-bottom:-30px">
			<h4>{l s='Vouchers'}</h4>
			<p style="border:0px" >
				<label for="discount_name">{l s='Code:'}</label>
				<input type="text" id="discount_name" name="discount_name" value="{if isset($discount_name) && $discount_name}{$discount_name}{/if}" />
			</p>
			<p class="submit"><input type="hidden" name="submitDiscount" /><input type="submit" name="submitAddDiscount" id="submitAddDiscount" value="{l s='Add'}" class="button" style="font-weight:bold; height:25px" /></p>
		{if $displayVouchers}
			<h4>{l s='Take advantage of our offers:'}</h4>
			<div id="display_cart_vouchers" style="border:0px" >
			{foreach from=$displayVouchers item=voucher}
				<span onclick="$('#discount_name').val('{$voucher.name}');return false;" class="voucher_name">{$voucher.name}</span> - {$voucher.description} <br />
			{/foreach}
			</div>
		{/if}
		</fieldset>
	</form>
</div>
{/if}
</div>
{/if}


<br /><br />
<div id="HOOK_SHOPPING_CART" style="border:0px">{$HOOK_SHOPPING_CART}</div>

{* Define the style if it doesn't exist in the PrestaShop version*}
{* Will be deleted for 1.5 version and more *}
{if !isset($addresses_style)}
	{$addresses_style.company = 'address_company'}
	{$addresses_style.vat_number = 'address_company'}
	{$addresses_style.firstname = 'address_name'}
	{$addresses_style.lastname = 'address_name'}
	{$addresses_style.address1 = 'address_address1'}
	{$addresses_style.address2 = 'address_address2'}
	{$addresses_style.city = 'address_city'}
	{$addresses_style.country = 'address_country'}
	{$addresses_style.phone = 'address_phone'}
	{$addresses_style.phone_mobile = 'address_phone_mobile'}
	{$addresses_style.alias = 'address_title'}
{/if}
{if $carrier->id AND !isset($virtualCart)}
	{* <div id="order_carrier">
		<h4>{l s='Carrier:'}</h4>
		{if isset($carrierPicture)}<img src="{$img_ship_dir}{$carrier->id}.jpg" alt="{l s='Carrier'}" />{/if}
		<span>{$carrier->name|escape:'htmlall':'UTF-8'}</span>
	</div> *}
	{/if}
{if (($carrier->id AND !isset($virtualCart)) OR $delivery->id OR $invoice->id) AND !$opc}
<div class="order_delivery">
	
	
</div>

{/if}

<p class="clear"><br /><br /></p>
<div class="clear"></div>
<p class="cart_navigation_extra">
	<span id="HOOK_SHOPPING_CART_EXTRA">{$HOOK_SHOPPING_CART_EXTRA}</span>
</p>
{/if}
</div>

{* <div class="shopping_cart_table" style="height:127px; position:relative">
<div style="display:block;float:left;">
<img src="{$img_ps_dir}contatta-servizio-clienti.gif" alt="Customer care 0585 821163" width="122" height="127" />
</div>
<div style="display:block;float:left">
<img src="{$img_ps_dir}se-hai-bisogno-di-aiuto.gif" alt="{l s='Se hai bisogno di aiuto per il tuo ordine, contatta il servizio clienti al numero 0585 821163'}" width="602" height="65" /><br />
<a target='_blank' href='https://www.ezdirect.it/contattaci?step=assistenza-ordini'><img src="{$img_ps_dir}oppure-apri-ticket.gif" width="454" height="15" alt="{l s='Oppure apri un ticket cliccando qui'}" style="border:0px" /></a>
</div>

<p class="clear"></p>


</div> *}


{* <div style="    display: flex; flex-direction: column; align-self: center;">

					<div class="row" style="display:flex; padding-top: 20px; border-bottom: 1px solid #f4f4f4;;">
						<div class="col-md-6" style="display:flex; align-items: center; border-right: 1px solid #f4f4f4; "> 
							<div style="padding-right:15px;"> 
								<img src='{$img_ps_dir}spedizione-24-48.png' alt='Spedizione 24-48' title='Spedizione 24-48' />
							</div>

							<div style="color: #3775eb;">
								<h5 style="font-weight:600;">Spedizione in 24/48h</h5>
								<p style="font-size: 11px;">Per i prodotti a stock</p>
							</div>
						</div>

						<div class="col-md-6" style="display:flex; align-items: center;">
							<div style="padding-right:15px;">
								<img src='{$img_ps_dir}sicurezza-garantita.png' alt='Sicurezza garantita' title='Sicurezza garantita' />
							</div>

							<div style="color: #00099c;"> 
								<h5 style="font-weight:600;">Sicurezza garantita</h5>
								<p style="font-size: 11px;">Paga online in totale tranquillità</p>
							</div>
						</div>
					</div>
					

					<div class="row" style="display:flex;">

						<div class="col-md-6" style="display:flex; align-items: center; border-right: 1px solid #f4f4f4;">
							<div style="padding-right:15px;">
								<img src='{$img_ps_dir}consulenza-asistenza.png' alt='Consulenza e assistenza' title='Consulenza e assistenza' />
							</div>

							<div style="color: #f78245;">
								<h5 style="font-weight:600;">Consulenza e assistenza</h5>
								<p style="font-size: 11px;">Assistenza specializzata</p>
							</div>
						</div>
						<div class="col-md-6" style="display:flex; align-items: center; ">
							<div style="padding-right:15px;">
								<img src='{$img_ps_dir}pagamento-sicuro.jpeg' alt='Pagamento sicuro' title='Pagamento sicuro' />
							</div>

							<div style="color: #87624b;">
								<h5 style="font-weight:600;">Pagamento sicuro</h5>
								<p style="font-size: 11px;">Le migliori soluzioni di pagamento online</p>
							</div>
						</div>
					</div>
				</div> *}

	<div style="border: 1px solid #e5e8eb; 
			box-shadow: 0 1px 1px rgba(0,0,0,0.15), 
        	0 2px 2px rgba(0,0,0,0.15), 
            0 4px 4px rgba(0,0,0,0.15), 
        	0 8px 8px rgba(0,0,0,0.15);
			border-radius:12px;
			padding:30px; 
			display: flex;" id="need-help">
		<div style="height: 140px; padding-right: 30px;">
			<img style="height:100%;" src='{$img_ps_dir}business-woman.jpg' alt='Aiuto' title='Aiuto' />
		</div>
		<div>
			<h5 style="font-size: 22px; font-weight: 600;">Hai bisogno di aiuto con il tuo ordine?</h5>
			<div style="display:flex; align-items:center;" id="numero-verde"><p>Chiamaci al numero verde</p><a href="#" style="padding-left: 12px; padding-bottom: 10px;"><img style="height:50px;" src='{$img_ps_dir}numero-verde.jpg' alt='Numero verde' /></a></div>
			
			<p>Oppure apri un ticket cliccando <a target='_blank' href='https://www.ezdirect.it/contattaci?step=assistenza-ordini'>qui</a>. Un esperto di risponderà quanto prima.</p>
		</div>
	</div>


	<div id="home-title-outer" style=" padding:20px 0; width:100%; height:auto;">
			<div class="container" id="home-title">
				<div class="row" style="display: flex; justify-content: center;">
					

					<div class="col-sm-3" id="orange_section">
						<img src='{$img_ps_dir}consulenza.png' alt='Consulenza' title='Consulenza' />
						<h5><strong>Consulenza e assistenza</strong></h5>
						<p>Assistenza specializzata</p>
					</div>

					<div class="col-sm-3" id="orange_section">
						<img src='{$img_ps_dir}spedizione24.png' alt='Spedizione24' title='Spedizione24' />
						<h5><strong>Spedizione in 24/48h</strong></h5>
						<p>Per i prodotti a stock</p>
					</div>

					<div class="col-sm-3" id="orange_section">
						<img src='{$img_ps_dir}pagamento-sicuro.png' alt='Prodotti' title='Prodotti' />
						<h5><strong>Pagamento sicuro</strong></h5>
						<p>Paga online in totale tranquillità</p>
					</div>
				</div>
			</div>
		</div>