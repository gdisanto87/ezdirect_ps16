{*
* 2007-2011 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2011 PrestaShop SA
*  @version  Release: $Revision: 1.4 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<div itemscope itemtype="http://schema.org/Product">
{include file="$tpl_dir./errors.tpl"}
{if $errors|@count == 0}
<script type="text/javascript">
// <![CDATA[

// PrestaShop internal settings
var currencySign = '{$currencySign|html_entity_decode:2:"UTF-8"}';
var currencyRate = '{$currencyRate|floatval}';
var currencyFormat = '{$currencyFormat|intval}';
var currencyBlank = '{$currencyBlank|intval}';
var taxRate = {$tax_rate|floatval};
var jqZoomEnabled = {if $jqZoomEnabled}true{else}false{/if};

//JS Hook
var oosHookJsCodeFunctions = new Array();

// Parameters
var id_product = '{$product->id|intval}';
var productHasAttributes = {if isset($groups)}true{else}false{/if};
var quantitiesDisplayAllowed = {if $display_qties == 1}true{else}false{/if};
var quantityAvailable = {if $display_qties == 1 && $product->quantity}{$product->quantity}{else}0{/if};
var allowBuyWhenOutOfStock = {if $allow_oosp == 1}true{else}false{/if};
var availableNowValue = '{$product->available_now|escape:'quotes':'UTF-8'}';
var availableLaterValue = '{$product->available_later|escape:'quotes':'UTF-8'}';
var productPriceTaxExcluded = {$product->getPriceWithoutReduct(true)|default:'null'} - {$product->ecotax};
var reduction_percent = {if $product->specificPrice AND $product->specificPrice.reduction AND $product->specificPrice.reduction_type == 'percentage'}{$product->specificPrice.reduction*100}{else}0{/if};
var reduction_price = {if $product->specificPrice AND $product->specificPrice.reduction AND $product->specificPrice.reduction_type == 'amount'}{$product->specificPrice.reduction}{else}0{/if};
var specific_price = {if $product->specificPrice AND $product->specificPrice.price}{$product->specificPrice.price}{else}0{/if};
var specific_currency = {if $product->specificPrice AND $product->specificPrice.id_currency}true{else}false{/if};
var group_reduction = '{$group_reduction}';
var default_eco_tax = {$product->ecotax};
var ecotaxTax_rate = {$ecotaxTax_rate};
var currentDate = '{$smarty.now|date_format:'%Y-%m-%d %H:%M:%S'}';
var maxQuantityToAllowDisplayOfLastQuantityMessage = {$last_qties};
var noTaxForThisProduct = {if $no_tax == 1}true{else}false{/if};
var displayPrice = {$priceDisplay};
var productReference = '{$product->reference|escape:'htmlall':'UTF-8'}';
var productAvailableForOrder = {if (isset($restricted_country_mode) AND $restricted_country_mode) OR $PS_CATALOG_MODE}'0'{else}'{$product->available_for_order}'{/if};
var productShowPrice = '{if !$PS_CATALOG_MODE}{$product->show_price}{else}0{/if}';
var productUnitPriceRatio = '{$product->unit_price_ratio}';
var idDefaultImage = {if isset($cover.id_image_only)}{$cover.id_image_only}{else}0{/if};

// Customizable field
var img_ps_dir = '{$img_ps_dir}';
var customizationFields = new Array();
{assign var='imgIndex' value=0}
{assign var='textFieldIndex' value=0}
{foreach from=$customizationFields item='field' name='customizationFields'}
	{assign var="key" value="pictures_`$product->id`_`$field.id_customization_field`"}
	customizationFields[{$smarty.foreach.customizationFields.index|intval}] = new Array();
	customizationFields[{$smarty.foreach.customizationFields.index|intval}][0] = '{if $field.type|intval == 0}img{$imgIndex++}{else}textField{$textFieldIndex++}{/if}';
	customizationFields[{$smarty.foreach.customizationFields.index|intval}][1] = {if $field.type|intval == 0 && isset($pictures.$key) && $pictures.$key}2{else}{$field.required|intval}{/if};
{/foreach}

// Images
var img_prod_dir = '{$img_prod_dir}';
var combinationImages = new Array();

{if isset($combinationImages)}
	{foreach from=$combinationImages item='combination' key='combinationId' name='f_combinationImages'}
		combinationImages[{$combinationId}] = new Array();
		{foreach from=$combination item='image' name='f_combinationImage'}
			combinationImages[{$combinationId}][{$smarty.foreach.f_combinationImage.index}] = {$image.id_image|intval};
		{/foreach}
	{/foreach}
{/if}

combinationImages[0] = new Array();
{if isset($images)}
	{foreach from=$images item='image' name='f_defaultImages'}
		combinationImages[0][{$smarty.foreach.f_defaultImages.index}] = {$image.id_image};
	{/foreach}
{/if}

// Translations
var doesntExist = '{l s='The product does not exist in this model. Please choose another.' js=1}';
var doesntExistNoMore = '{l s='This product is no longer in stock' js=1}';
var doesntExistNoMoreBut = '{l s='with those attributes but is available with others' js=1}';
var uploading_in_progress = '{l s='Uploading in progress, please wait...' js=1}';
var fieldRequired = '{l s='Please fill in all required fields' js=1}';

{if isset($groups)}
	// Combinations
	{foreach from=$combinations key=idCombination item=combination}
		addCombination({$idCombination|intval}, new Array({$combination.list}), {$combination.quantity}, {$combination.price}, {$combination.ecotax}, {$combination.id_image}, '{$combination.reference|addslashes}', {$combination.unit_impact}, {$combination.minimal_quantity});
	{/foreach}
	// Colors
	{if $colors|@count > 0}
		{if $product->id_color_default}var id_color_default = {$product->id_color_default|intval};{/if}
	{/if}
{/if}
//]]>
</script>

{include file="$tpl_dir./breadcrumb.tpl"}

<div id="primary_block" class="clearfix">


	{if isset($adminActionDisplay) && $adminActionDisplay}
	<div id="admin-action">
		<p>{l s='This product is not visible to your customers.'}
		<input type="hidden" id="admin-action-product-id" value="{$product->id}" />
		<input type="submit" value="{l s='Publish'}" class="exclusive" onclick="submitPublishProduct('{$base_dir}{$smarty.get.ad}', 0)"/>
		<input type="submit" value="{l s='Back'}" class="exclusive" onclick="submitPublishProduct('{$base_dir}{$smarty.get.ad}', 1)"/>
		</p>
		<div class="clear" ></div>
		<p id="admin-action-result"></p>
		</p>
	</div>
	{/if}

	{if isset($confirmation) && $confirmation}
	<p class="confirmation">
		{$confirmation}
	</p>
	{/if}

	<!-- right infos-->
	<div id="pb-right-column">
		<!-- product img-->
		<div id="image-block">
		{if $have_image}
			<img itemprop="image" src="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large')}"
				{if $jqZoomEnabled}class="jqzoom" alt="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'thickbox')}"{else} title="{$product->name|escape:'htmlall':'UTF-8'}" alt="{$product->name|escape:'htmlall':'UTF-8'}" {/if} id="bigpic" width="280px" height="280px" />
		{else}
			<img src="{$img_dir}{$lang_iso}-default-large.jpg" alt="" title="{$product->name|escape:'htmlall':'UTF-8'}" width="110px" height="110px" style="display:block; margin:0 auto; margin-top:75px;" />
		{/if}
		</div>

	
		<!-- thumbnails -->
		<div id="views_block" {if isset($images) && count($images) < 2}class="hidden"{/if}>
		{if isset($images) && count($images) > 3}<span class="view_scroll_spacer"><a id="view_scroll_left" class="hidden" title="{l s='Other views'}" href="javascript:{ldelim}{rdelim}"></a></span>{/if}
		<div id="thumbs_list">
		{if isset($images)}
			<ul id="thumbs_list_frame">
				
					{foreach from=$images item=image name=thumbnails}
					{assign var=imageIds value="`$product->id`-`$image.id_image`"}
					<li id="thumbnail_{$image.id_image}">
						<a href="{$link->getImageLink($product->link_rewrite, $imageIds, 'thickbox')}" rel="other-views" class="thickbox {if $smarty.foreach.thumbnails.first}shown{/if}" title="{$image.legend|htmlspecialchars}">
							<img id="thumb_{$image.id_image}" src="{$link->getImageLink($product->link_rewrite, $imageIds, 'medium')}" alt="{$image.legend|htmlspecialchars}" height="50px" width="50px" />
						</a>
					</li>
					{/foreach}
					
						</ul>
						{/if}
			</div>
				
		
			

		{if isset($images) && count($images) > 3}<span class="view_scroll_spacer"><a id="view_scroll_right" title="{l s='Other views'}" href="javascript:{ldelim}{rdelim}"></a></span>{/if}
		</div>
						<div id="video_list">
						<script type="text/javascript">
	$('document').ready(function() {
	$("a.video-fc").fancybox({
				'width'				: '75%',
				'height'			: '75%',
				'autoScale'			: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});

<!-- $("#a.video-fc").fancybox().trigger('click'); -->

	});

	</script>
	{if $videos}
	<ul style="width:280px">
    {foreach from=$videos item=v}
        <li><a href='{$v.content|escape:'quotes':'UTF-8'}' class='video-fc'><img src="{$img_dir}video.gif" alt="Video" title="Video" /></a></li>
    {/foreach}
</ul>
{/if}
</div>
{if $HOOK_EXTRA_RIGHT}{$HOOK_EXTRA_RIGHT}{/if}

<div style="clear:both"></div>
		<div style="float:left; margin-left:0px">	
		
		<p class="align_center clear"><span id="wrapResetImages" style="display: none;"><img src="{$img_dir}icon/cancel_16x18.gif" alt="{l s='Cancel'}" width="16" height="18"/> <a id="resetImages" href="{$link->getProductLink($product)}" onclick="$('span#wrapResetImages').hide('slow');return (false);">{l s='Display all pictures'}</a></span></p>{/if}
		

			</div>
			
	</div>

	<!-- left infos-->
	<div id="pb-left-column">
	<h1 itemprop="name">{$product->name|escape:'htmlall':'UTF-8'}</h1>
	
		<div class="mini-description">
			{if $product->description_short}
				<div id="short_description_content" class="rte align_justify"><span itemprop="description">{$product->description_short}</span></div>
			{/if}
			
		</div>
		
		
	
		
<div class='codes'>	<!-- codici -->
			
			{if $product->reference}
			{l s='Reference:'} <span itemprop="productID" content="mpn:{$product->reference|escape:'htmlall':'UTF-8'}">{$product->reference|escape:'htmlall':'UTF-8'}</span> &nbsp;&nbsp;
			{else}{/if}
			{if $product->supplier_reference}
			{l s='SKU reference:'} {$product->supplier_reference|escape:'htmlall':'UTF-8'} &nbsp;&nbsp;
			{else}{/if}
				{if $product->ean13}
			{l s='EAN:'} {$product->ean13|escape:'htmlall':'UTF-8'}
			{else}{/if}

			<!-- fine codici -->
	</div>
{if $isLoggedBackPrd == 1}
<br />
{else}{/if}
		<div class="prices">

		{if ($product->show_price AND !isset($restricted_country_mode)) OR isset($groups) OR $product->reference OR (isset($HOOK_PRODUCT_ACTIONS) && $HOOK_PRODUCT_ACTIONS)}
		
		<!-- add to cart form-->
		
		<form id="buy_block" {if $PS_CATALOG_MODE AND !isset($groups) AND $product->quantity > 0}class="hidden"{/if} action="{$link->getPageLink('cart.php')}" method="post">
		 <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
			<!-- hidden datas -->

				<input type="hidden" name="token" value="{$static_token}" />
				<input type="hidden" name="id_product" value="{$product->id|intval}" id="product_page_product_id" />
				<input type="hidden" name="add" value="1" />
				<input type="hidden" name="id_product_attribute" id="idCombination" value="" />
		<!-- fine hidden datas -->

			
			<!-- prezzi -->
				{if $product->price == 0}
				{if preg_match("/fuori produzione/i", $product->description_short)}  
				
				<span class="improve-your-search">{l s='This product is out of production'}.</span><br /><br />
			<a href="{$link->getCategoryLink($product->id_category_default,$link->getCatRewrite($product->id_category_default))}">{l s='Search for similar products'}</a>
			
				
				{else}
			<a href="#quotation" 
		onclick='var d = document.getElementById("quotation");
d.className = d.className + "selected";

'>{l s='Ask for a quotation'}</a>
			{/if}
		
			
			{else}
			{if $product->show_price AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}
				
					{if !$priceDisplay || $priceDisplay == 2}
					{assign var='productPrice' value=$product->getPrice(true, $smarty.const.NULL)}
					{assign var='productPriceWithoutRedution' value=$product->getPriceWithoutReduct(false, $smarty.const.NULL)}
					{elseif $priceDisplay == 1}
						{assign var='productPrice' value=$product->getPrice(false, $smarty.const.NULL)}
						{assign var='productPriceWithoutRedution' value=$product->getPriceWithoutReduct(true, $smarty.const.NULL)}
					{/if}
					{if $product->on_sale}
						
					{elseif $product->specificPrice AND $product->specificPrice.reduction AND $productPriceWithoutRedution > $productPrice}
						
					{/if}
					
					
					{if $product->listino>0 && $product->listino>$product->getPrice(false, $smarty.const.NULL)}
			
			{else}
			
			
			{/if}
					
					
					{if $priceDisplay > 0 && $priceDisplay <= 2}
						
						{if $product->listino>0 && $product->listino>$product->getPrice(false, $smarty.const.NULL)}

						{/if}
					
						{if $product->listino>0 && $product->listino>$product->getPrice(false, $smarty.const.NULL)}
						
						{else}
					
						{/if}
					
						
							{if $tax_enabled}
								{if $priceDisplay == 1}{else}{l s='tax incl.'}{/if}
							{/if}
					{/if}
					
					{if $priceDisplay == 2}
					{if $product->price == 0}{else}	{convertPrice price=$product->getPrice(false, $smarty.const.NULL)}{/if}
					{/if}
					
				{if $product->specificPrice AND $product->specificPrice.reduction}
					
					{if $priceDisplay >= 0 && $priceDisplay <= 2}
						{if $productPriceWithoutRedution > $productPrice}
							
								{if $tax_enabled}
									{if $priceDisplay == 1}<!-- {l s='tax excl.'} -->{else}{/if}
								{/if}
						{/if}
					{/if}
					
				{/if}
				{if $product->specificPrice AND $product->specificPrice.reduction_type == 'percentage'}
				<!-- {$product->specificPrice.reduction*100} -->
				{/if}
				
			
			
				{*close if for show price*}
			{/if}
			{/if}
			<!-- fine prezzi -->
		
 
 
 
<!-- sconti quantita -->	
<div id="prezzi-container">
<!-- VISUALIZZAZIONE IMPIEGATO -->

	{if $isLoggedBackPrd == 1}
	
	<ul id="prezzi-container-nav" style="position:absolute; list-style-type:none; display:inline; top:-21px; left:10px">
	<style type="text/css">
	
	#prezzi-container-nav li {
				border-top-left-radius:7px !important;
			-webkit-border-top-left-radius:7px !important;
		-moz-border-top-left-radius:7px !important;
		-o-border-top-left-radius:7px !important;
				border-top-right-radius:7px !important;
			-webkit-border-top-right-radius:7px !important;
		-moz-border-top-right-radius:7px !important;
		-o-border-top-right-radius:7px !important;
	}

	.activeli a {
	color:#ffffff;
	}
	
	.active {
	color:#ffffff;
	}
	
	.przcont-a {
	color:#0000cc;
	}
	
	</style>
			<li style='width:90px;  background-color:#de7613; text-align:center; display:block; float:left; height:18px; border:1px solid #d4d4d4'><a class='przcont-a' style="text-decoration:none" href="#prezzi-prodotto"><strong>Clienti</strong></a></li>
			<li style='width:90px; margin-left:10px; text-align:center; background-color:#de7613; display:block; float:left;height:18px; border:1px solid #d4d4d4'><a class='przcont-a' style="text-decoration:none" href="#prezzi-impiegato"><strong>Riservato</strong></a></li>
			
	</ul>
	<script type="text/javascript" src="{$js_dir}yetii.js"></script>
	{else}{/if}

	<div class="tab" id="prezzi-prodotto">
	
		{if $product->price == 0}{else}	
			{if $quantity_discounts}

				<div id="quantityDiscount">
					<table class="tabella-sconti-quantita">
					<tr>
					<th style="font-weight:normal">{if $product->listino > 0}<span style="font-size:12px; color:#383838">{l s='List price'}: {convertPrice price=$product->listino} {l s='t.e.'}</span>{else}{/if}
					</th>
					<th style="font-weight:normal; border:0px" colspan="3">{l s='Quantity discounts'}</th>
					</tr>
					<tr>
					{assign var="i" value="0"}
					{assign var=numItems value=$quantity_discounts|@count}
					{foreach from=$quantity_discounts item='quantity_discount' name='quantity_discounts'}
					
						{if $i == $numItems-1}
					 
							<td style="border:0px; font-size:12px; padding-left:0px; color:#383838">1 + {l s='pieces'}</td>
					
						{else}	
			
						{/if}
		  
						{$i = $i+1}
						
					{/foreach}

					{foreach from=$quantity_discounts item='quantity_discount' name='quantity_discounts'}
					
						{if $i == $numItems-1}
					 
							<td style="color:#383838; border:0px">{$quantity_discount.quantity|intval}
							+ {l s='pieces'}
				
						{else}	
							<td style="color:#383838; border:0px">{$quantity_discount.quantity|intval}
							+ {l s='pieces'}
						
						{/if}
		  
						
		  
						{$i = $i+1}
						</td>
					{/foreach}
					</tr>
					<tr>
					{assign var="j" value="0"}
					{foreach from=$quantity_discounts item='quantity_discount' name='quantity_discounts'}
						{if $j == $numItems-1}
					 
							<td style="border:0px; vertical-align:bottom; padding-left:0px">
								<div style="position:relative; margin-top:-18px">
									<meta itemprop="priceCurrency" content="EUR" />
									<span style="font-size:22px; color:#e62026" itemprop="price"><strong>{assign var="firstPrice" value=$productPrice - $quantity_discount.real_value|floatval}
									{convertPrice price=$productPrice} </strong></span>
									<br />
									{convertPrice price=$product->getPrice(true, $smarty.const.NULL)} {l s='t.i.'}	
								</div>
							</td>
						{else}
						{/if}
						
						{$j = $j+1}
					{/foreach}
					
					{foreach from=$quantity_discounts item='quantity_discount' name='quantity_discounts'}
						<td style="vertical-align:bottom">
						
							<strong>{convertPrice price=$productPrice - $quantity_discount.real_value|floatval}</strong>
							<br />
							<span style='font-size:10px'>({convertPrice price=$product->getPrice(true, $smarty.const.NULL) - ($product->getPrice(true, $smarty.const.NULL) * $quantity_discount.reduction)})
							</span>
						</td>
					{/foreach}
					</tr>
					</table>
				</div>
			{else}
				<div id="quantityDiscount">
				<table class="std">
				<tr>
				<th style="font-weight:normal"><span style="font-size:12px; color:#383838">{if $product->listino > 0}{l s='List price'}: {convertPrice price=$product->listino} {l s='t.e.'}{/if}</span>
				</th>
				</tr>
				<tr>

				<td style="border:0px; font-size:14px; color:#e62026">
				{if $prezzospeciale == 1}
					<strong>{l s='Special price until'} {$scontofinoa}</strong>
				{else}
					<strong>{l s='Price'}</strong>
				{/if}
				
				</td></tr>
				<tr>
				<td style="border:0px">
				<meta itemprop="priceCurrency" content="EUR" />
				<span style="font-size:22px; color:#e62026" itemprop="price"><strong>{convertPrice price=$productPrice} </strong></span><br />
				{convertPrice price=$product->getPrice(true)} {l s='t.i.'}	
				</td>
				</tr>
				</table>
				</div>
		
			{/if}
			<!-- fine sconti quantita -->
			
		{/if}
	</div><!-- fine prezzi prodotto -->
	
	{if $isLoggedBackPrd == 1}
		<div class="tab" id="prezzi-impiegato">
	
	
	

	<script type="text/javascript" src="{$js_dir}jquery.tooltip.js"></script>
	<script type="text/javascript">
	$(function() {
	$('.span-impiegato').tooltip({ showURL: false  });
	});
	</script>

	<table style='background-color:#ebebeb; font-size:12px'>
	<span class='span-impiegato' title="I prezzi principali. Il prezzo vendita &egrave; il prezzo a cui il cliente finale acquista il prodotto, senza calcolare eventuali sconti quantit&agrave; o sconti rivenditore se si tratta di un rivenditore. Il risparmio &egrave; dato dalla differenza tra prezzo di listino e prezzo di vendita."  style='cursor:pointer; text-decoration:underline'><strong>Listino e vendita</strong></span><br />
	<tr><th style='width:120px'>Prezzo listino</th>
	<th style='width:120px'>Prezzo vendita</th>
	<th style='width:120px'>Risparmio</th>

	<tr>
	<td><strong> {if $product->listino == 0} -- {else} {convertPrice price=$product->listino} {/if} </strong></td>
	<td><strong> {convertPrice price=$product->price} </strong></td>
	<td><strong> {if $product->listino == 0} -- {else} {convertPrice price=$product->listino-$product->price} {/if} </strong></td>

	</tr>
	</table>
	<br />

	<span class='span-impiegato' title="Gli sconti d'acquisto sono calcolati sul prezzo di listino. Sottratti al prezzo di listino d&agrave;nno come risultato il prezzo d'acquisto. La marginalit&agrave; &egrave; la nostra percentuale di guadagno sul prodotto. Diminuendo il prezzo di vendita, la marginalit&agrave; diminuisce e di conseguenza diminuisce il nostro guadagno."  style='cursor:pointer; text-decoration:underline'><strong>Sconti acquisto</strong></span>

	<br />
	<table style='background-color:#ffff00; font-size:11px'>
	<tr><th style='width:72px'>Sconto 1</th>
	<th style='width:72px'>Sconto 2</th>
	<th style='width:72px'>Sconto 3</th>
	<th style='width:72px'>Prezzo acq.</th>
	<th style='width:72px'>Marginalit&agrave;</th></tr>
	</tr>
	<tr>
	<td> {if $sc_acq_1 > 0} {$sc_acq_1}% {else} -- {/if} </td>
	<td> {if $sc_acq_2 > 0} {$sc_acq_2}% {else} -- {/if}</td>
	<td> {if $sc_acq_3 > 0} {$sc_acq_3}% {else} -- {/if}</td>
	<td> {if $netto_acq > 0} {convertPrice price=$netto_acq} {else} -- {/if}</td>
	<td> {((($product->price-$netto_acq)*100)/$product->price)|number_format:2:",":"."}%</td>
	</tr>
	</table>
	<br />

	<span class='span-impiegato' title="Gli quantit&agrave; sono sconti che si applicano sul prodotto se il cliente acquista una certa quantit&agrave; di questo prodotto. Il campo quantit&agrave; indica il numero minimo di pezzi di questo prodotto che il cliente deve acquistare per ottenere lo sconto abbinato."  style='cursor:pointer; text-decoration:underline'><strong>Sconti quantit&agrave;</strong></span><br />
	<table style='background-color:#99cc99; font-size:11px'>
	<tr><th style='width:40px'>Nr.</th>
	<th style='width:80px'>% sconto</th>
	<th style='width:80px'>Quantit&agrave;</th>
	<th style='width:80px'>Prezzo</th>
	<th style='width:80px'>Marginalit&agrave;</th>
	</tr>
	<tr>
	<td> 1 </td>
	<td> {if $sc_qta_1 > 0} {$sc_qta_1_rid}% {else} -- {/if}</td>
	<td> {if $sc_qta_1 > 0} {$sc_qta_1_pz} pz. {else} -- {/if}</td>
	<td> {if $sc_qta_1 > 0} {convertPrice price=$sc_qta_1} {else} -- {/if}</td>
	<td> {if $sc_qta_1 > 0} {((($sc_qta_1-$netto_acq)*100)/$sc_qta_1)|number_format:2:",":"."}% {else} -- {/if}</td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> {if $sc_qta_2 > 0} {$sc_qta_2_rid}% {else} -- {/if}</td>
	<td> {if $sc_qta_2 > 0} {$sc_qta_2_pz} pz. {else} -- {/if}</td>
	<td> {if $sc_qta_2 > 0} {convertPrice price=$sc_qta_2} {else} -- {/if}</td>
	<td> {if $sc_qta_2 > 0} {((($sc_qta_2-$netto_acq)*100)/$sc_qta_2)|number_format:2:",":"."}% {else} -- {/if}</td>
	</tr>
	<tr>
	<td> 3 </td>
	<td> {if $sc_qta_3 > 0} {$sc_qta_3_rid}% {else} -- {/if}</td>
	<td> {if $sc_qta_3 > 0} {$sc_qta_3_pz} pz. {else} -- {/if}</td>
	<td> {if $sc_qta_3 > 0} {convertPrice price=$sc_qta_3} {else} -- {/if}</td>
	<td> {if $sc_qta_3 > 0} {((($sc_qta_3-$netto_acq)*100)/$sc_qta_3)|number_format:2:",":"."}% {else} -- {/if}</td>
	</tr>
	</table>

	<br />
	<span class='span-impiegato' title="Sono gli sconti riservati ai venditori. ATTENZIONE!!! Se esiste un prezzo derivante da uno sconto quanti&agrave; che risulta INFERIORE al prezzo applicato con sconto rivenditore, al rivenditore SI APPLICA il prezzo con sconto quantit&agrave; pi&ugrave; basso perch&eacute; al rivenditore si applica il prezzo pi&ugrave; basso sempre e comunque." style='cursor:pointer; text-decoration:underline'><strong>Sconti rivenditore</strong></span>
	<table style='background-color:#ff99ff; font-size:11px'>
	<tr><th style='width:60px'>Nr.</th>
	<th style='width:100px'>% sconto</th>
	<th style='width:100px'>Prezzo</th>
	<th style='width:100px'>Marginalit&agrave;</th>
	</tr>
	<tr>
	<td> 1 </td>
	<td> {if $sc_riv_1 > 0} {$sc_riv_1_rid}% {else} -- {/if}</td>
	<td> {if $sc_riv_1 > 0} {convertPrice price=$sc_riv_1} {else} -- {/if}</td>
	<td> {if $sc_riv_1 > 0} {((($sc_riv_1-$netto_acq)*100)/$sc_riv_1)|number_format:2:",":"."}% {else} -- {/if}</td>
	</tr>
	<tr>
	<td> 2 </td>
	<td> {if $sc_riv_2 > 0} {$sc_riv_2_rid}% {else} -- {/if}</td>
	<td> {if $sc_riv_2 > 0} {convertPrice price=$sc_riv_2} {else} -- {/if}</td>
	<td> {if $sc_riv_2 > 0} {((($sc_riv_2-$netto_acq)*100)/$sc_riv_2)|number_format:2:",":"."}% {else} -- {/if}</td>
	</tr>


	</table>





	<br /><br />
	</div> <!-- fine prezzi impiegato -->
	<!-- FINE VISUALIZZAZIONE IMPIEGATO -->

			<script type="text/javascript">
var tabber1 = new Yetii({
id: "prezzi-container",
active: 1
});
</script>
	{else}{/if}
	
</div><!-- fine prezzi container -->







<div class="carrello">
<!-- quantita -->
	{if $product->price == 0}
		{else}
			<p style="font-size:12px" id="quantity_wanted_p"{if (!$allow_oosp && $product->quantity == 0) OR $virtual OR !$product->available_for_order OR $PS_CATALOG_MODE} style="display: none;"{/if}>{l s='Quantity'}<br />
				
				<img src="{$img_ps_dir}cart1.png" style="border:0px" alt="{l s='Buy'}" title="{l s='Buy'}" />
				<input type="text" name="qty" id="quantity_wanted" class="text" value="{if isset($quantityBackup)}{$quantityBackup|intval}{else}{if $product->minimal_quantity > 1}{$product->minimal_quantity}{else}1{/if}{/if}" size="2" maxlength="3" {if $product->minimal_quantity > 1}onkeyup="checkMinimalQuantity({$product->minimal_quantity});"{/if} />
			</p>

		
			<!-- minimal quantity wanted -->
			<p id="minimal_quantity_wanted_p"{if $product->minimal_quantity <= 1 OR !$product->available_for_order OR $PS_CATALOG_MODE} style="display: none;"{/if}>{l s='You must add '}<b id="minimal_quantity_label">{$product->minimal_quantity}</b>{l s=' as a minimum quantity to buy this product.'}</p>
			{if $product->minimal_quantity > 1}
			<script type="text/javascript">
				checkMinimalQuantity();
			</script>
			{/if}
<!-- fine quantita -->
<p{if (!$allow_oosp && $product->quantity <= 0) OR !$product->available_for_order OR (isset($restricted_country_mode) AND $restricted_country_mode) OR $PS_CATALOG_MODE} style="display: none;"{/if} id="add_to_cart" class="buttons_bottom_block"><input type="submit" value="{l s='Add to cart'}" title="{l s='Add to cart'}" class="ajax_add_to_cart_button" name="acquista-listing" style="width:80%; background-image:none; display:block; background-color:#e62026; margin-left:10px; margin-top:20px; padding:5px" /></p>
{/if}
</div>

	




<!-- prezzi v -->

<div style="position:relative;margin-left:135px">

<span style="font-size:12px; color:#383838">
		
				
		
</span>
</div>
{if $product->price == 0}
{else}
	{if $quantity_discounts}
		{if $product->listino > 0}
			<span style="font-size:12px; color:#383838">{assign var="i" value="0"}
			{assign var=numItems value=$quantity_discounts|@count}
			{foreach from=$quantity_discounts item='quantity_discount' name='quantity_discounts'}
			
				{if $i == $numItems-1}
			 
					{l s='From'}
				{else}	
	
				{/if}
  
				{$i = $i+1}
				
			{/foreach}

		
			{assign var="j" value="0"}
			{foreach from=$quantity_discounts item='quantity_discount' name='quantity_discounts'}
				{if $j == $numItems-1}
					<strong>
					{assign var="firstPrice" value=$productPrice - $quantity_discount.real_value|floatval}
					{convertPrice price=$productPrice - $quantity_discount.real_value|floatval} 
				
					</strong>
				{else}
				{/if}
				
				{$j = $j+1}
			{/foreach} - 
			{l s='You save up to'}</span><span style="font-size:12px; color:#e62026"> <strong>{convertPrice price=$product->listino - $firstPrice}</strong></span><br />
		{else}{/if}
	{else}
		{if $product->listino > 0 && $product->listino > $productPrice}
			{assign var="firstPrice" value=$productPrice|floatval}
			<span style="font-size:12px; color:#383838">
			{l s='You save'}</span><span style="font-size:12px; color:#e62026"> <strong>{convertPrice price=$product->listino - $firstPrice}</strong></span><br />
		{else}
		{/if}
	{/if}
{/if}
<div class="availability">
<!-- disponibilita -->

				{if $product->price == 0}
		{else}
					{if $product->quantity == 0}<strong>{l s='Not available'}</strong> <!-- Out of stock hook -->
							{$HOOK_PRODUCT_OOS}
			{else}
				{if $smarty.now|date_format:"%Y-%m-%d" < $product->date_available} 
				<strong>{l s='Available from'} {$product->date_available|date_format:"%d-%m-%Y"}</strong>
				{else}
				<strong>{l s='Available'}</strong>
				{/if}
			
			
			
			{/if}
			
			{/if}
			<!-- fine disponibilita -->
</div>

</div>
</form>
</div>



<!-- fine prezzi v -->
<div class="prodotto-info">
<table style="margin-left:5px">
{if $product->price != 0}
<tr>
<td style="width:90px"><strong>{l s='Shipping'}</strong></td><td> 
{if $product->quantity > 0}
	{if $product->available_now != ''}
		{if $product->available_now == 'Disponibile'}
		{l s='24h for available products'}
		{else}
		{$product->available_now}
		{/if}
	{else}
	{l s='24h for available products'}
	{/if}
{else}
{l s='5 to 7 days'}
{/if}
{if $cookie->id_default_group == 3}{else}
			{if $freeshipping || $product->price > 399} - <strong style="color:#d60000">{l s='Free shipping'}</strong>

			{else}
			
			{/if}
{/if}
</td>
</tr>
{else}
{/if}
{if $garanzia != ''}
			<tr>
			<td style="width:90px"><strong>{l s='Warranty'}</strong></td><td> {$garanzia}</td>
			</tr>
			{else}
			{/if}

{if $confezione != ''}
			<tr>
			<td>
			<strong>{l s='Package'}</strong></td><td> {$confezione}</td>
			</tr>
			
			{else}
			{/if}

{if $product->condition == 'new'}{else}<tr><td>{l s='Product condition'}</strong></td><td> {l s='Second-hand'}</td></tr>{/if}


</table>


<br />
	
			
			
			
			<div class='manufacturer'>
			{assign var=xx value=$img_manu_dir|cat:$product->id_manufacturer|cat:'-manufacturerprod.jpg'}
			{if $product->manufacturer_name}
			
			<a title="{$product->manufacturer_name}" href="{$link->getManufacturerLink($product->id_manufacturer)}"><img src="{$img_manu_dir}{$product->id_manufacturer}-manufacturerprod.jpg" alt="{$product->manufacturer_name}" title="{$product->manufacturer_name}" style="border:0px" /></a>
		{else}
		{/if}
			</div>
			{if $attachments}
			<div class='manuali-pdf'>
			<ul style="list-style-type:none">
			{foreach from=$attachments item=attachment}
			<li><a href="{$base_dir}attachment.php?id_attachment={$attachment.id_attachment}" title="{$attachment.name|escape:'htmlall':'UTF-8'}">{$attachment.name|truncate:20:'...':true|escape:'htmlall':'UTF-8'}</a>
		{/foreach}</li>
		</ul>
			</div>
			{else}{/if}
			
			<div style="clear:both"></div>

			
			
								
		
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		</div>	
			
				
		</div>	
		
		
			{if isset($groups)}
			<!-- attributes -->
			<div id="attributes">
			{foreach from=$groups key=id_attribute_group item=group}
			{if $group.attributes|@count}
			<p>
				<label for="group_{$id_attribute_group|intval}">{$group.name|escape:'htmlall':'UTF-8'} :</label>
				{assign var="groupName" value="group_$id_attribute_group"}
				<select name="{$groupName}" id="group_{$id_attribute_group|intval}" onchange="javascript:findCombination();{if $colors|@count > 0}$('#wrapResetImages').show('slow');{/if};">
					{foreach from=$group.attributes key=id_attribute item=group_attribute}
						<option value="{$id_attribute|intval}"{if (isset($smarty.get.$groupName) && $smarty.get.$groupName|intval == $id_attribute) || $group.default == $id_attribute} selected="selected"{/if} title="{$group_attribute|escape:'htmlall':'UTF-8'}">{$group_attribute|escape:'htmlall':'UTF-8'}</option>
					{/foreach}
				</select>
			</p>
			{/if}
			{/foreach}
			</div>
			{/if}

			
			

			
		

			<p class="warning_inline" id="last_quantities"{if ($product->quantity > $last_qties OR $product->quantity == 0) OR $allow_oosp OR !$product->available_for_order OR $PS_CATALOG_MODE} style="display: none;"{/if} >{l s='Warning: Last items in stock!'}</p>

			{if $product->online_only}
				<p>{l s='Online only'}</p>
			{/if}

			
			{if isset($HOOK_PRODUCT_ACTIONS) && $HOOK_PRODUCT_ACTIONS}{$HOOK_PRODUCT_ACTIONS}{/if}
			<div class="clear"></div>
	
		
		{/if}
		
		
		</div>
	
<div class="social">

			<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http%3A//{php} $url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; echo $url; {/php}" target="_blank"><img src="{$img_ps_dir}linkedin.gif" style="margin-left:0px" class="image-social" alt="{l s='Linkedin'}" title="{l s='Linkedin'}" /></a>
			<a href="http://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2F{php} $url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; echo $url; {/php}" target="_blank"><img src="{$img_ps_dir}facebook.gif"  class="image-social" alt="{l s='Facebook'}" title="{l s='Facebook'}" /></a>
			<a href="http://twitter.com/home/?status=Check+out+EZDIRECT+@+http%3A%2F%2F{php} $url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; echo $url; {/php}%2F" target="_blank"><img src="{$img_ps_dir}twitter.gif" class="image-social" alt="{l s='Twitter'}" title="{l s='Twitter'}" /></a>
			
			<a href="http://www.myspace.com/Modules/PostTo/Pages/?t=Ezdirect&amp;c={php} $url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; echo $url; {/php}" target="_blank"><img src="{$img_ps_dir}myspace.gif" class="image-social" alt="{l s='MySpace'}" title="{l s='MySpace'}" /></a>
			<a href="https://m.google.com/app/plus/x/?v=compose&amp;content=http://{php} $url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; echo $url; {/php}" onclick="window.open('https://m.google.com/app/plus/x/?v=compose&amp;content=http://{php} $url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; echo $url; {/php}','gplusshare','width=450,height=300,left='+(screen.availWidth/2-225)+',top='+(screen.availHeight/2-150)+'');return false;"><img src="{$img_ps_dir}googleplus.gif" class="image-social" alt="{l s='Google Plus'}" title="{l s='Google Plus'}" /></a>
			
			</div>
<div class="skype">
<a href='callto://ezdirect.it1'><img src="{$img_ps_dir}skypecall.jpg" class="pulsanti-prodotto" alt="{l s='Skype call'}" title="{l s='Skype call'}" style="border:0px" /></a>
</div>			
			
			
			<div class="prodotto-info" style="margin-left:12px">
			{if $HOOK_EXTRA_LEFT}{$HOOK_EXTRA_LEFT}{/if}
		
			<a href="{$link->getPageLink('cms.php?id_cms=31')}"><img src="{$img_ps_dir}tirichiamiamonoi2.jpg" class="pulsanti-prodotto" style="border:0px" alt="{l s='We will call you back'}" title="{l s='We will call you back'}" /></a>
				<a href="#quotation" 
		onclick='var d = document.getElementById("quotation");
d.className = d.className + "selected";

'><img src="{$img_ps_dir}preventivogratuito2.jpg" class="pulsanti-prodotto" style="border:0px" alt="{l s='Free quotation'}" title="{l s='Free quotation'}" /></a>
			<a href="https://www.ezdirect.it/mibew/client.php?locale=it" target="_blank" onclick="if(navigator.userAgent.toLowerCase().indexOf('opera') != -1 &amp;&amp; window.event.preventDefault) window.event.preventDefault();this.newWindow = window.open('https://www.ezdirect.it/mibew/client.php?locale=it&amp;url='+escape(document.location.href)+'&amp;referrer='+escape(document.referrer), 'webim', 'toolbar=0,scrollbars=0,location=0,status=1,menubar=0,width=640,height=480,resizable=1');this.newWindow.focus();this.newWindow.opener=window;return false;"><img src="https://www.ezdirect.it/mibew/c.php?i=webim&amp;lang=it" class="pulsanti-prodotto" style="border:0px" alt="{l s='Live help desk'}" title="{l s='Live help desk'}" /></a>
			<div style="clear:both"></div>
			
	
			</div>
			
			<div style="clear:both"></div>
			
			

{if $accessories}

<div id="accessories-carousel" style="margin-top:10px">
<h4 class="h4tab">{l s='Accessories'}</h4>
				<div id="accessories-carousel-int">
					<table>
					<tr>
					{foreach from=$accessories item=accessory name=accessories_list}
						{assign var='accessoryLink' value=$link->getProductLink($accessory.id_product, $accessory.link_rewrite, $accessory.category)}
						<td>
							<h5 style="font-size:10px; display:block; width:130px"><a href="{$accessoryLink|escape:'htmlall':'UTF-8'}" title="{$accessory.legend|escape:'htmlall':'UTF-8'}">{$accessory.name|truncate:40:'...':true|escape:'htmlall':'UTF-8'}</a></h5>
							<div class="product_desc" style="margin-top:0px"><br />
								<a href="{$accessoryLink|escape:'htmlall':'UTF-8'}" title="{$accessory.legend|escape:'htmlall':'UTF-8'}" class="product_image"><img src="{$link->getImageLink($accessory.link_rewrite, $accessory.id_image, '80x80')}" alt="{$accessory.legend|escape:'htmlall':'UTF-8'}" width="{$ottantaSize.width}" height="{$ottantaSize.height}" /></a>
					
							</div>
							<p class="product_accessories_price" style="position:absolute; width:130px; text-align:center; bottom:0px">

								{if $accessory.quantity_discount}
				
								{assign var="i" value="0"}
								{assign var=numItems value=$accessory.quantity_discount|@count}
								{foreach from=$accessory.quantity_discount item='quantity_discount' name='product.quantity_discounts'}
								{if $i == $numItems-1}				

								{l s='From '} <span class="price" style="display: inline;"> {convertPrice price=$quantity_discount.price - ($quantity_discount.price * $quantity_discount.reduction)|floatval}</span> 
								{else}	
								{/if}
								{$i = $i+1}
								{/foreach}
				
								{else}
								{if isset($accessory.show_price) && $accessory.show_price && !isset($restricted_country_mode)}<span class="price" style="display: inline;">{if !$priceDisplay}{convertPrice price=$accessory.price}{else}{convertPrice price=$accessory.price_tax_exc}{/if}</span>{/if}

								{/if}

							
								{if $accessory.available_for_order AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}
								 
								<a class="ajax_add_to_cart_button" href="{$link->getPageLink('cart.php')}?qty=1&amp;id_product={$accessory.id_product|intval}&amp;token={$static_token}&amp;add" rel="ajax_id_product_{$accessory.id_product|intval}" title="{l s='Add'} {$accessory.name} {l s='to cart'}"><img style="border:0px" src="{$img_ps_dir}cart1.png" alt="{l s='Add to cart'}" title="{l s='Add'} {$accessory.name} {l s='to cart'}" onmouseover="this.src='{$img_ps_dir}cart1_on.png'" onmouseout="this.src='{$img_ps_dir}cart1.png'" /></a>
								
								{/if}
							</p>
						</td>
					{/foreach}
					</tr></table>
				</div>	
		</div>

{/if}



<!-- description and features -->
{if $product->description || $features || $accessories || $HOOK_PRODUCT_TAB || $attachments}
<div id="more_info_block" class="clear" style="position:relative">
{if $features}


<a style="display:block; position:absolute; height:20px; width:120px; top:0px; left:165px; background-image:url({$img_ps_dir}linkscheda.gif)" href="#schedina" onclick='$(".idTabs").idTabs("idTab1"); 
'></a>
{/if}
<div id="itabs">
	<ul id="more_info_tabs" class="idTabs idTabsShort">
		{if $product->description}<li><a id="more_info_tab_more_info" title="{l s='More info'}" href="#idTab1">{l s='More info'}</a></li>{/if}
		
	
		{if $features}
			
		<li><a id="schedalink" title="{l s='Data sheet'}">{l s='Data sheet'}</a></li>
			
		{/if}

		{if $attachments}<!-- <li><a id="more_info_tab_attachments" title="{l s='Download'}" href="#idTab9">{l s='Download'}</a></li> -->{/if}
		{$HOOK_PRODUCT_TAB}
		
		<li><a id="more_info_quotation" title="{l s='Quotation'}" href="#quotation" 
		onclick='var d = document.getElementById("quotation");
d.className = d.className + "selected";

'>{l s='Quotation'}</a></li>
		<li><a id="more_info_tab_payment" title="{l s='Payment and shipping'}" href="#payment">{l s='Payment and shipping'}</a></li>
	</ul>
</div>
	<div id="more_info_sheets" class="sheets align_justify">

	
	
	<div id="idTab1" class="rte">
	{if $product->description}
					
<script type="text/javascript" src="{$js_dir}jquery.expander.js"></script>
<script type="text/javascript">
        $(function () {
            $('.descrprodotto').expander({
                slicePoint: 900,
				expandEffect: 'slideDown',
				collapseEffect: 'slideUp',
                expandText: 'Espandi per leggere il testo completo',
                userCollapseText: 'Riduci il testo sopra'
            });
        });
</script>

		
		<!-- full description -->
		<div class='descrprodotto'>{$product->description|regex_replace:"/<div>/":""|regex_replace:"/<\/div>/":""|regex_replace:"/<div class=\"display\">/":""|regex_replace:"/<h1[^>]*?>/":"<h1 class='in-product'>"|regex_replace:"/src=\"images/":"src=\"https://www.ezdirect.it/images"}
		</div>
	{/if}
	{if $features}	
	
		{* Added by module for grouped features *}
		<!-- product's features -->
		<ul id="idTab2" class="bullet">
		{* Hello from exfeatures !!! *}
				
<br />
		<strong><a id="schedina">{l s='Data sheet'}</a></strong><br /><br />

		{foreach from=$features item=group}
		
		
		
            <li>
			<div style="width:720px; background-color:#e6e6e6">
			
          <strong style="color:#383838; font-size:13px">{$group.name}</strong></div>
                <table class="features-table">
				{assign var='k' value='1'}
				
                {foreach from=$group.features item=feature} 
				
				{if $k%2 != 0}
				<tr class="even-f">
				{else}
				<tr class="odd-f" style="width:690px; background-color:#f5f5f5">
				{/if}
				<td class="feature-name" style="width:300px">
			
    	           <strong>{$feature.name|escape:'htmlall':'UTF-8'}</strong></td>
				   
				   <td class="feature-value" style="text-align:left"> 
				   {$feature.value|escape:'htmlall':'UTF-8'|replace:'4*':'<img src="https://www.ezdirect.it/images/4.gif" alt="4" title="4" />'|replace:'5*':'<img src="https://www.ezdirect.it/images/5.gif" alt="5" title="5" />'|replace:'3*':'<img src="https://www.ezdirect.it/images/3.gif" alt="3" title="3" />'|replace:'2*':'<img src="https://www.ezdirect.it/images/2.gif" alt="2" title="2" />'|replace:'1*':'<img src="https://www.ezdirect.it/images/1.gif" alt="1" title="1" />'|replace:'s&igrave;':'<img src="https://www.ezdirect.it/images/si.gif" alt="s&igrave;" title="s&igrave;" />'}</td>
				</tr>
				{$k = $k+1}
				   {/foreach}
                </table><br />
            </li>
		{/foreach}
		</ul>
		{* End added by module for grouped features *}
	{/if}
	{if $attachments}
	<br /><br />
		<ul id="idTab9" class="bullet">
		{foreach from=$attachments item=attachment}
			<li><a href="{$base_dir}attachment.php?id_attachment={$attachment.id_attachment}">{$attachment.name|escape:'htmlall':'UTF-8'}</a><br />{$attachment.description|escape:'htmlall':'UTF-8'}</li>
		{/foreach}
		</ul>
	{/if}
	</div>

	<div id="quotation">
	<script type="text/javascript">var errorquotstring="{l s='ERROR: Enter the code as it is shown'}";</script>
	<script type="text/javascript" src="{$js_dir}md5.js"></script>
	<script type="text/javascript" src="{$js_dir}jcap.js"></script>



{if $formquotazioneinviato == 'no'}

{l s='Ask for informations or for a quotation about:'}<br />

<strong>{$product->name}</strong><br /><br />

{l s='This module is only for VAT number possessors, companies, professionals, public administrations, etc.'}<br />
{l s='We do not make any quotation to private customers, if you are a private customer you can buy our products on this website.'}
<br /><br />
<strong>{l s='Your details'}</strong><br />
<form method="post" action="{$pagina}#quotation" name="richiedi_quotazione" onsubmit="return jcap();">
<input type="hidden" name="prodottorichiesto" value="{$product->name}" />
<table>
<tr>
<tr><td>{l s='Company'}*</td><td><input name='ragione_sociale' type='text' style='width:370px' /><br /></td></tr>
<tr><td>{l s='VAT number or Company Tax Code'}*</td><td><input name='vat_number' type='text' style='width:370px' /><br /></td></tr>
<tr><td>{l s='First name'}*</td><td><input name='nome' type='text' style='width:370px' /><br /></td></tr>
<tr><td>{l s='Last name'}*</td><td><input name='cognome' type='text' style='width:370px' /><br /></td></tr>
<tr><td>{l s='Email'}*</td><td><input name='email_address' type='text' style='width:370px' /><br /></td></tr>
<tr><td>{l s='Phone number'}*</td><td><input name='telefono_fisso' type='text' style='width:370px' /><br /></td></tr>
<tr><td>{l s='Requested quantity'}*</td><td><input name='quantita' type='text' style='width:370px' /><br /></td></tr>

<tr><td>{l s='Additional informations'}</td><td><textarea name='addinfo' style='width:370px'></textarea><br /></td></tr>
<tr><td><a href='https://www.ezdirect.it/privacy.php' target='_blank'>{l s='I have read and I accept privacy policy'}</a></td><td><input type='checkbox' name='privacyquotazione' /><br /></td></tr>
<tr>
<td>
{l s='Insert the security code you see in the image below'}*
<script type="text/javascript">sjcap();
</script>
<input type='submit' name='submit_quotation' class='button' value='{l s='Send request'}' onclick="Invia()" />
</td>
</tr>
<tr>
<td></td><td></td></tr>



</table>
<br /><br />
{l s='Click on the following link to ask for our paper catalog'}: <a href='https://www.ezdirect.it/catalogo.php' target='_blank'> https://www.ezdirect.it/catalogo.php</a>

{else if $formquotazioneinviato == 'errors'}
<script type="text/javascript"> 
  $(".idTabs").idTabs("quotation"); 
</script>
<div class="error">

		<p>{if $errors_quotation|@count > 1}{l s='There are'}{else}{l s='There is'}{/if} {$errors_quotation|@count} {if $errors_quotation|@count > 1}{l s='errors'}{else}{l s='error'}{/if} :</p>
		<ol>
		{foreach from=$errors_quotation key=k item=error}
			<li>{$error}</li>
		{/foreach}
		</ol>
	</div>
{l s='Ask for informations or for a quotation about:'}<br />

	<strong>{$product->name}</strong><br /><br />
{l s='This module is only for VAT number possessors, companies, professionals, public administrations, etc.'}<br /><br />
<strong>{l s='Your details'}</strong><br />
<form method="post" action="{$pagina}#quotation" name="richiedi_quotazione" onsubmit="return jcap();">
<input type="hidden" name="prodottorichiesto" value="{$product->name}" />
<table>
<tr>
<tr><td>{l s='Company'}*</td><td><input name='ragione_sociale' value="{if isset($smarty.post.ragione_sociale)}{$smarty.post.ragione_sociale|escape:'htmlall':'UTF-8'|stripslashes}{/if}" type='text' style='width:370px' /><br /></td></tr>
<tr><td>{l s='VAT number or Company Tax Code'}*</td><td><input name='vat_number' value="{if isset($smarty.post.vat_number)}{$smarty.post.vat_number|escape:'htmlall':'UTF-8'|stripslashes}{/if}" type='text' style='width:370px' /><br /></td></tr>
<tr><td>{l s='First name'}*</td><td><input name='nome' value="{if isset($smarty.post.nome)}{$smarty.post.nome|escape:'htmlall':'UTF-8'|stripslashes}{/if}" type='text' style='width:370px' /><br /></td></tr>
<tr><td>{l s='Last name'}*</td><td><input name='cognome' value="{if isset($smarty.post.cognome)}{$smarty.post.cognome|escape:'htmlall':'UTF-8'|stripslashes}{/if}" type='text' style='width:370px' /><br /></td></tr>
<tr><td>{l s='Email'}*</td><td><input name='email_address' value="{if isset($smarty.post.email_address)}{$smarty.post.email_address|escape:'htmlall':'UTF-8'|stripslashes}{/if}" type='text' style='width:370px' /><br /></td></tr>
<tr><td>{l s='Phone number'}*</td><td><input name='telefono_fisso' value="{if isset($smarty.post.telefono_fisso)}{$smarty.post.telefono_fisso|escape:'htmlall':'UTF-8'|stripslashes}{/if}" type='text' style='width:370px' /><br /></td></tr>
<tr><td>{l s='Requested quantity'}*</td><td><input name='quantita' value="{if isset($smarty.post.quantita)}{$smarty.post.quantita|escape:'htmlall':'UTF-8'|stripslashes}{/if}" type='text' style='width:370px' /><br /></td></tr>

<tr><td>{l s='Additional informations'}</td><td><textarea name='addinfo' style='width:370px'>{if isset($smarty.post.addinfo)}{$smarty.post.addinfo|escape:'htmlall':'UTF-8'|stripslashes}{/if}</textarea><br /></td></tr>
<tr><td><a href='https://www.ezdirect.it/privacy.php' target='_blank'>{l s='I have read and I accept privacy policy'}</a></td><td><input type='checkbox' name='privacyquotazione' /><br /></td></tr>
<tr>
<td>
{l s='Insert the security code you see in the image below'}*
<script type="text/javascript">sjcap();</script>
<input type='submit' name='submit_quotation' class='button' value='{l s='Send request'}' onclick="Invia()" />
</td>
</tr>
<tr>
<td></td><td></td></tr>



</table>
	<br /><br />
{l s='Click on the following link to ask for our paper catalog'}: <a href='https://www.ezdirect.it/catalogo.php' target='_blank'> https://www.ezdirect.it/catalogo.php</a>
	
	

{else if $formquotazioneinviato == 'yes'}
<script type="text/javascript"> 
  $(".idTabs").idTabs("quotation"); 
</script>
<!-- Google Code for Richiesta di preventivo Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1058514372;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "8C5eCMypmAQQxMve-AM";
var google_conversion_value = 0;
/* ]]> */

_gaq.push(['_trackEvent', 'Quotazione', 'Invio', 'Prodotto']);

</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1058514372/?value=0&amp;label=8C5eCMypmAQQxMve-AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

{l s='Your request has been successfully sent. Thank you!'}
<br /><br />
<a href='{$link->getProductLink($product)}'>{l s='Click here to reload the page if you want to send another quotation for the same product'}</a>
{/if}



</div>
		<div id="payment">
<h4>Pagamento</h4><br />
<p>
<strong>Carta di credito</strong><br />
Ezdirect srl non viene a conoscenza dei dati sensibili della tua carta di credito poich&eacute; la transazione avviene su circuito sicuro SSL3 a 128 bit. Le carte di credito accettate sono quelle previste per i circuiti <strong>Visa, Master Card, Postepay</strong>.<br /><br /></p>

<p><strong>Contrassegno</strong><br />
Il pagamento pu&ograve; avvenire nei seguenti modi:<br> <strong>L'assegno bancario</strong> viene accettato per ordini di importo massimo pari o <strong>inferiori a 516,00 &euro; iva inclusa</strong>.<br> Per ordini di importo <strong>superiore a 516,00 &euro; iva inclusa</strong>, viene richiesto il pagamento con assegno circolare.<br> <strong>L'assegno bancario o circolare dovr&agrave; essere intestato a: Ezdirect srl</strong><br> <strong>Il pagamento in contanti</strong> al corriere &egrave; sempre accettato*<br> *Il pagamento in contanti pu&ograve; avvenire anche se nella fattura accompagnatoria o D.D.T. &egrave; indicata la modalit&agrave; "contrassegno". <br /> Il denaro contante dovr&agrave; essere consegnato al ns corriere.<br /><br />
<strong>Costo per gestione pagamento in contanti/assegno (contrassegno)</strong><br />
Il contrassegno ha un costo aggiuntivo di 3,50 &euro; iva esclusa, per ordini inferiori a 399,00 &euro; iva esclusa.
<br />
<em>Rivenditore</em>: Il contrassegno ha un costo aggiuntivo di 5,00 &euro; iva esclusa per ordini fino a 516,00 &euro;. Oltre 516,00 &euro; si applica l'aliquota dell' 1,5 % sull'importo.<br /><br /></p>

<p><strong>Bonifico Bancario Anticipato</strong><br />
Il pagamento a mezzo bonifico bancario deve essere effettuato a favore di Ezdirect srl. Via Nerino Garbuio snc 54038 Montignoso MS. <br /> C/C: 000080701380 - cod. ABI 06110 Cod. CAB:69943 IBAN:IT72N0611069943000080701380<br /><br /></p>

<p><strong>Paypal</strong><br />
Il pagamento pu&ograve; essere effettuato tramite il sistema Paypal.<br /><br /></p>

<p><strong>POS virtuale telefonico</strong><br />
Il cliente comunica telefonicamente al numero 0585 821163 i dati della carta di credito per effettuare la transazione.<br /><br /></p>

<h4>Consegna</h4><br />
<p>La consegna avviene in 2 giorni sui prodotti disponibili ed entro 3-7 giorni su prodotti in approvvigionamento. Per richiedere conferma della disponibilit&agrave; chiama il numero 0585 821163 oppure <a href='callto://ezdirect.it1'>via Skype cliccando su questo link</a> oppure con una mail a <strong>ordini@ezdirect.it</strong>.</p>


</div>



		<!-- accessories -->
		
			<div class="block products_block accessories_block clearfix">
			

	</div>
	{$HOOK_PRODUCT_TAB_CONTENT}
	</div>
</div>
{/if}

<!-- Customizable products -->
{if $product->customizable}
	<ul class="idTabs">
		<li><a style="cursor: pointer">{l s='Product customization'}</a></li>
	</ul>
	<div class="customization_block">
		<form method="post" action="{$customizationFormTarget}" enctype="multipart/form-data" id="customizationForm">
			<p>
				<img src="{$img_dir}icon/infos.gif" alt="Informations" />
				{l s='After saving your customized product, remember to add it to your cart.'}
				{if $product->uploadable_files}<br />{l s='Allowed file formats are: GIF, JPG, PNG'}{/if}
			</p>
			{if $product->uploadable_files|intval}
			<h2>{l s='Pictures'}</h2>
			<ul id="uploadable_files">
				{counter start=0 assign='customizationField'}
				{foreach from=$customizationFields item='field' name='customizationFields'}
					{if $field.type == 0}
						<li class="customizationUploadLine{if $field.required} required{/if}">{assign var='key' value='pictures_'|cat:$product->id|cat:'_'|cat:$field.id_customization_field}
							{if isset($pictures.$key)}<div class="customizationUploadBrowse"><img src="{$pic_dir}{$pictures.$key}_small" alt="" /><a href="{$link->getUrlWith('deletePicture', $field.id_customization_field)}"><img src="{$img_dir}icon/delete.gif" alt="{l s='Delete'}" class="customization_delete_icon" width="11" height="13" /></a></div>{/if}
							<div class="customizationUploadBrowse"><input type="file" name="file{$field.id_customization_field}" id="img{$customizationField}" class="customization_block_input {if isset($pictures.$key)}filled{/if}" />{if $field.required}<sup>*</sup>{/if}
							<div class="customizationUploadBrowseDescription">{if !empty($field.name)}{$field.name}{else}{l s='Please select an image file from your hard drive'}{/if}</div></div>
						</li>
						{counter}
					{/if}
				{/foreach}
			</ul>
			{/if}
			<div class="clear"></div>
			{if $product->text_fields|intval}
			<h2>{l s='Texts'}</h2>
			<ul id="text_fields">
				{counter start=0 assign='customizationField'}
				{foreach from=$customizationFields item='field' name='customizationFields'}
					{if $field.type == 1}
						<li class="customizationUploadLine{if $field.required} required{/if}">{assign var='key' value='textFields_'|cat:$product->id|cat:'_'|cat:$field.id_customization_field}
							{if !empty($field.name)}{$field.name}{/if}{if $field.required}<sup>*</sup>{/if}<textarea type="text" name="textField{$field.id_customization_field}" id="textField{$customizationField}" rows="1" cols="40" class="customization_block_input" />{if isset($textFields.$key)}{$textFields.$key|stripslashes}{/if}</textarea>
						</li>
						{counter}
					{/if}
				{/foreach}
			</ul>
			{/if}
			<p style="clear: left;" id="customizedDatas">
				<input type="hidden" name="quantityBackup" id="quantityBackup" value="" />
				<input type="hidden" name="submitCustomizedDatas" value="1" />
				<input type="button" class="button" value="{l s='Save'}" onclick="javascript:saveCustomization()" />
				<span id="ajax-loader" style="display:none"><img src="{$img_ps_dir}loader.gif" alt="loader" /></span>
			</p>
		</form>
		<p class="clear required"><sup>*</sup> {l s='required fields'}</p>
	</div>
{/if}

{if $packItems|@count > 0}
	<div>
		<h2>{l s='Pack content'}</h2>
		{include file="$tpl_dir./product-list.tpl" products=$packItems}
	</div>
{/if}


{$HOOK_PRODUCT_FOOTER}
<br />
<p style='text-align:center'>{l s='Product updated on'} {$product->date_upd}</p>
</div>






