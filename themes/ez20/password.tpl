{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='Password reset'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<h1>{l s='Password reset'}</h1>

{include file="$tpl_dir./errors.tpl"}

{if isset($confirmation) && $confirmation == 1}
<form method="post" class="std">
<p style="text-align:center;">{l s='Please reset your password using this personal form'}.</p>
 <fieldset>
  <p class="password" style="text-align:center;">
   <label for="passwd">{l s='New Password'}</label>
   <input type="password" name="passwd" id="passwd" />
  </p>
  <p class="password">
   <label for="confirmation">{l s='Confirmation'}</label>
   <input type="password" name="confirmation" id="confirmation" />
  </p>
  <p class="submit">
   <input type="submit" class="button_large" name="submitPass" value="{l s='Save'}" style="margin-left:0px" />
  </p>
 </fieldset>
</form>
{elseif isset($confirmation) && $confirmation == 2}
<p class="success">{l s='A confirmation e-mail has been sent to your address:'} {$email|escape:'htmlall':'UTF-8'}</p>
{else}
<p style="text-align:center;">{l s='Please enter the e-mail address used to register. We will e-mail you your new password.'}</p>
<form action="{$request_uri|escape:'htmlall':'UTF-8'}" method="post" class="std">
	<fieldset>
		<p class="text" style="text-align: center;">
			<label for="email">{l s='E-mail:'}</label>
			<input type="text" style="height:20px; width:45%; margin-left: 10px;" id="email" name="email" value="{if isset($smarty.post.email)}{$smarty.post.email|escape:'htmlall':'UTF-8'|stripslashes}{/if}" />
		</p>
		<p class="submit">
			<input type="submit" class="button_large" value="{l s='Retrieve Password'}" style="padding: 5px 10px;"/>
		</p>
	</fieldset>
</form>
{/if}
<p class="clear" style="display: flex;
    align-items: center;">
	<a href="{$link->getPageLink('authentication.php', true)}" title="{l s='Torna al Login'}"><img src="{$img_dir}icon/my-account.gif" alt="{l s='Torna al Login'}" class="icon" width="22" height="22" style="margin-bottom: 5px;"/></a><a href="{$link->getPageLink('authentication.php')}" title="{l s='Torna al Login'}">{l s='Torna al Login'}</a>
</p>
