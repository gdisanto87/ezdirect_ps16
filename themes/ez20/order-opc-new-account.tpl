{*
{*
** Compatibility code for Prestashop older than 1.4.2 using a recent theme
** Ignore list isn't require here
** $address exist in every PrestaShop version
*}

{* Will be deleted for 1.5 version and more *}
{* If ordered_adr_fields doesn't exist, it's a PrestaShop older than 1.4.2 *}
{if !isset($dlv_all_fields)}
		{$dlv_all_fields.0 = 'company'}
		{$dlv_all_fields.1 = 'firstname'}
		{$dlv_all_fields.2 = 'lastname'}
		{$dlv_all_fields.3 = 'address1'}
		{$dlv_all_fields.4 = 'address2'}
		{$dlv_all_fields.5 = 'suburb'}
		{$dlv_all_fields.6 = 'address2'}
		{$dlv_all_fields.7 = 'postcode'}
		{$dlv_all_fields.8 = 'city'}
		{$dlv_all_fields.9 = 'country'}
		{$dlv_all_fields.10 = 'state'}
{/if}


		
<div id="opc_new_account" class="opc-main-block">
	<div id="opc_new_account-overlay" class="opc-overlay" style="display: none;"></div>
	
	<form action="{$link->getPageLink('order.php', true)}" method="post" id="login_form" class="std">
		<fieldset>
		<br />
			<h2>{l s='Already registered?'} <a href="#" id="openLoginFormBlock">{l s='Click here'}</a></h2>
			<div id="login_form_content" style="display:none;">
				<!-- Error return block -->
				<div id="opc_login_errors" class="error" style="display:none;"></div>
				<!-- END Error return block -->
				<table><tr>
					<td style="border:0px">{l s='E-mail address'}</td>
					<td style="border:0px"><input type="text" class="text" id="login_email" name="email" /></td>
				</tr>
				<tr>
					<td style="border:0px">{l s='Password'}</td>
					<td style="border:0px"><input type="password" class="text" id="login_passwd" name="passwd" /></td>
				</tr>
				<tr>
				<td colspan="2" style="border:0px">
				<p class="submit" style="float:left">
					{if isset($back)}<input type="hidden" class="hidden" name="back" value="{$back|escape:'htmlall':'UTF-8'}" />{/if}
					<input type="submit" id="SubmitLogin" name="SubmitLogin" class="button_large" value="{l s='Log in'}" />
				</p>
				</td></tr></table>
				<p class="lost_password"><a href="{$link->getPageLink('password.php', true)}">{l s='Forgot your password?'}</a></p>
			</div>
		</fieldset>
	</form>
	<form action="#" method="post" id="new_account_form" class="std">
		<fieldset>
			<h2>{l s='New Customer'}</h2>
			
			<div id="opc_account_form">
				{$HOOK_CREATE_ACCOUNT_TOP}
				<script type="text/javascript">
				// <![CDATA[
				idSelectedCountry = {if isset($guestInformations) && $guestInformations.id_state}{$guestInformations.id_state|intval}{else}10{/if};
				{if isset($countries)}
					{foreach from=$countries item='country'}
						{if isset($country.states) && $country.contains_states}
							countries[{$country.id_country|intval}] = new Array();
							{foreach from=$country.states item='state' name='states'}
								countries[{$country.id_country|intval}].push({ldelim}'id' : '{$state.id_state}', 'name' : '{$state.name|escape:'htmlall':'UTF-8'}'{rdelim});
							{/foreach}
						{/if}
						{if $country.need_identification_number}
							countriesNeedIDNumber.push({$country.id_country|intval});
						{/if}
						{if isset($country.need_zip_code)}
							countriesNeedZipCode[{$country.id_country|intval}] = {$country.need_zip_code};
						{/if}
					{/foreach}
				{/if}
				//]]>
				{if $vat_management}
					{literal}
					function vat_number()
					{
						if ($('#company').val() != '')
							$('#vat_number_block').show();
						else
							$('#vat_number_block').hide();
					}
					function vat_number_invoice()
					{
						if ($('#company_invoice').val() != '')
							$('#vat_number_block_invoice').show();
						else
							$('#vat_number_block_invoice').hide();
					}

					$(document).ready(function() {
						$('#company').blur(function(){
							vat_number();
						});
						$('#company_invoice').blur(function(){
							vat_number_invoice();
						});
						vat_number();
						vat_number_invoice();
					});
					{/literal}
				{/if}
				</script>
				<!-- Error return block -->
				<div id="opc_account_errors" class="error" style="display:none;"></div>
				<!-- END Error return block -->
				<!-- Account -->
				<input type="hidden" id="is_new_customer" name="is_new_customer" value="0" />
				<input type="hidden" id="opc_id_customer" name="opc_id_customer" value="{if isset($guestInformations) && $guestInformations.id_customer}{$guestInformations.id_customer}{else}0{/if}" />
				<input type="hidden" id="opc_id_address_delivery" name="opc_id_address_delivery" value="{if isset($guestInformations) && $guestInformations.id_address_delivery}{$guestInformations.id_address_delivery}{else}0{/if}" />
				<input type="hidden" id="opc_id_address_invoice" name="opc_id_address_invoice" value="{if isset($guestInformations) && $guestInformations.id_address_invoice}{$guestInformations.id_address_invoice}{else}0{/if}" />
				<p class="required text">
					<label for="type">{l s='Type'}</label>
					<input type="radio" name="is_company1" id="is_company_0" value="0" />
					{l s='End user'}
					<input type="radio" name="is_company1" id="is_company_1" value="1" />
					{l s='Company'}
				</p>
				<p class="required text">
					<label for="email">{l s='E-mail'}</label>
					<input type="text" class="text" id="email" name="email" value="{if isset($guestInformations) && $guestInformations.email}{$guestInformations.email}{/if}" />
					<sup>*</sup>
				</p>
				<p class="required text">
					<label for="email">Ripeti {l s='E-mail'}</label>
					<input type="text" class="text" id="ripeti_email" name="ripeti_email" value="" />
					<sup>*</sup>
				</p>
				<p class="required password is_customer_param">
					<label for="passwd">{l s='Password'}</label>
					<input type="password" class="text" name="passwd" id="passwd" />
					<sup>*</sup>
				</p>
				
				<p class="required text">
					<label for="firstname">{l s='First name'}</label>
					<input type="text" class="text" id="customer_firstname" name="customer_firstname" onblur="$('#firstname').val($(this).val());" value="{if isset($guestInformations) && $guestInformations.customer_firstname}{$guestInformations.customer_firstname}{/if}" />
					<sup>*</sup>
				</p>
				<p class="required text">
					<label for="lastname">{l s='Last name'}</label>
					<input type="text" class="text" id="customer_lastname" name="customer_lastname" onblur="$('#lastname').val($(this).val());" value="{if isset($guestInformations) && $guestInformations.customer_lastname}{$guestInformations.customer_lastname}{/if}" />
					<sup>*</sup>
				</p>
				
				
				{$stateExist = false}
				<script type="text/javascript" src="{$js_dir}etc/checkCompany.js"></script>
				<script type="text/javascript" src="{$js_dir}tools.js">
				</script>
				<script type="text/javascript" src="{$js_dir}jquery.tooltip.js"></script>
				<script type="text/javascript" src="{$js_dir}etc/tax_code_help.js"></script>
				{foreach from=$dlv_all_fields item=field_name}
				{if $field_name eq "company"}
				<p class="required text" id="company_p">
					<label for="company">{l s='Company'}</label>
					<input type="text" class="text" id="company" name="company" value="{if isset($guestInformations) && $guestInformations.company}{$guestInformations.company}{/if}" />
					<sup>*</sup>
				</p>
				
				<p class="required text">
					<label for="tax_code">{l s='Tax code'} <span id="tax_code_az_p">{l s='(company)'}</span> </label>
					<input type="text" class="text" id="tax_code" name="tax_code" value="{if isset($guestInformations) && $guestInformations.tax_code}{$guestInformations.tax_code}{/if}" />
					<sup>* <span id="tax_code_help" style="cursor:pointer; position:absolute; right:20px" title="Se operi per conto di una societ&agrave; di capitali (esempio S.R.L o S.P.A.) il C.F. da inserire &egrave; sempre quello aziendale, quindi non devi usare il C.F. personale. Se operi per conto di una societ&agrave; di persone o ditta individuale, il C.F. necessario per la registrazione, &egrave; comuqnue quello aziendale, che, in alcuni casi, potrebbe coincidere con quello personale (es. ditte individuali). Per avere supporto in fase di registrazione, contatta il nostro staff al numero 0585821163. Grazie per la cortese collaborazione." ><img src="{$img_dir}icon/help.gif" alt="{l s='Help'}" /></span></sup>
				</p>
				{/if}
				{if $field_name eq "vat_number"}
				<p class="required text" id="vat_number_p">
					<label for="vat_number">{l s='VAT number'}</label>
					<input type="text" class="text" id="vat_number" name="vat_number" value="{if isset($guestInformations) && $guestInformations.vat_number}{$guestInformations.vat_number}{/if}" />
					<sup>*</sup>
				</p>
				
				<p class="required text" id="employees_number_p">
						<label for="employees_number">{l s='Employees number'}</label>
						<select name="employees_number">
						<option value="Da 1 a 10">{l s='1 to 10'}</option>
						<option value="Da 11 a 50">{l s='11 to 50'}</option>
						<option value="50 piu">{l s='More than 50'}</option>
						</select>
						
						<sup>*</sup>
					</p>
				{/if}
				
				{if $field_name eq "address1"}
				<p class="required text">
					<label for="address1">{l s='Address'}</label>
					<input type="text" class="text" name="address1" id="address1" value="{if isset($guestInformations) && $guestInformations.address1}{$guestInformations.address1}{/if}" />
					<sup>*</sup>
				</p>
				{/if}
				{if $field_name eq "suburb"}
				<p class="required suburb text">
					<label for="suburb">{l s='Suburb'}</label>
					<input type="text" class="text" name="suburb" id="suburb" value="{if isset($smarty.post.suburb)}{$smarty.post.suburb}{/if}" >
				</p>	
				{/if}
				{if $field_name eq "c_o"}
				<p class="required c_o text">
					<label for="c_o">{l s='C/O'}</label>
					<input type="text" class="text" name="c_o" id="c_o" value="{if isset($smarty.post.c_o)}{$smarty.post.c_o}{/if}" >
				</p>
				{/if}
				{if $field_name eq "country" || $field_name eq "Country:name"}
				
				<p class="required select">
					<label for="id_country">{l s='Country'}</label>
					<select name="id_country" id="id_country" style="width:150px"  onchange="countryChange();">
						<option value="">-</option>
						{foreach from=$countries item=v}
						<option value="{$v.id_country}" {if (isset($guestInformations) AND $guestInformations.id_country == $v.id_country) OR (!isset($guestInformations) && $sl_country == $v.id_country)} selected="selected"{/if}>{$v.name|escape:'htmlall':'UTF-8'}</option>
						{/foreach}
					</select>
					<sup>*</sup>
				</p>
				<script type="text/javascript">
				$(document).ready(function(){
					countryChange();
				});
				</script>
				{/if}
				<div class="ee_address">
				{if $field_name eq "postcode"}
				<p class="required postcode text">
					<label for="postcode" style="text-align:left">{l s='Zip / Postal code'}</label>
					<input type="text" class="text" name="postcode" id="postcode" value="{if isset($guestInformations) && $guestInformations.postcode}{$guestInformations.postcode}{/if}" onkeyup="$('#postcode').val($('#postcode').val().toUpperCase());" />
					<sup>*</sup>
				</p>
				{/if}
				{if $field_name eq "city"}
				<p class="required text">
					<label for="city">{l s='City'}</label>
					<input type="text" class="text" name="city" id="city" value="{if isset($guestInformations) && $guestInformations.city}{$guestInformations.city}{/if}" />
					<sup>*</sup>
				</p>
				{/if}
				
				{if $field_name eq "state" || $field_name eq 'State:name'}
				{$stateExist = true}
				<p class="required id_state select">
					<label for="id_state">{l s='State'}</label>
					<select name="id_state" id="id_state">
						<option value="">-</option>
					</select>
					<sup>*</sup>
				</p>
				{/if}
				</div>
				<div class="it_address">
					{if $field_name eq "postcode"}
						<p class="required postcode text">
							<label for="cap">{l s='Zip / Postal Code'}</label>
							<input type="text" class="text" name="postcode" id="cap" value="{if isset($smarty.post.postcode)}{$smarty.post.postcode}{/if}" onblur="$('#cap').val($('#cap').val().toUpperCase());" onkeyup="cercaCitta();">
							<sup>*</sup>
						</p>
						{/if}
					
					{if $field_name eq "city"}
					<p class="required text">
						<label for="citta">{l s='City'}</label>
						<select name='city' id='citta' onclick='cercaProvincia();'>
							<option name=''>-- Prima selezionare CAP --</option>
							{if isset($smarty.post.city)}
								<option name='{$smarty.post.city}' selected='selected'>{$smarty.post.city}</option>
							{else}
								{if isset($address->city)}
									<option name='{$address->city|escape:'htmlall':'UTF-8'}' selected='selected'>{$address->city|escape:'htmlall':'UTF-8'}</option>
								{/if}
							{/if}
							
						</select><sup>*</sup>
					</p>
						<!--
							if customer hasn't update his layout address, country has to be verified
							but it's deprecated
						-->
					{/if}
					{if $field_name eq "State:name" || $field_name eq 'state'}
					{assign var='stateExist' value=true}

					<p class="required id_state select">
						<label for="provincia">{l s='State'}</label>
						
						<input id="provincia" name="provincia_mark" readonly="readonly" type="text" value="{if isset($smarty.post.provincia_mark)}{$smarty.post.provincia_mark}{else}{$this_state}{/if}" />
						
						<sup>*</sup>
						<input id="provincia_hidden" name="id_state" type="text" readonly="readonly" style="border:0px; width:1px;height:1px" value="{if isset($smarty.post.id_state)}{$smarty.post.id_state}{else}{if isset($address->id_state)}{$address->id_state|escape:'htmlall':'UTF-8'}{/if}{/if}" />
					</p>
					{/if}
				</div>
				{/foreach}
				
				{if !$stateExist}
				<p class="required id_state select">
					<label for="id_state">{l s='State'}</label>
					<select name="id_state" id="id_state">
						<option value="">-</option>
					</select>
					<sup>*</sup>
				</p>
				{/if}
				
				<p class="text">
					<label for="phone">{l s='Phone'}</label>
					<input type="text" class="text" name="phone" id="phone" value="{if isset($guestInformations) && $guestInformations.phone}{$guestInformations.phone}{/if}" /> <sup>*</sup>
				</p>
				<p class="text">
					<label for="codice_univoco">{l s='SDI'}</label>
					<input type="text" class="text" name="codice_univoco" id="codice_univoco" value="{if isset($guestInformations) && $guestInformations.codice_univoco}{$guestInformations.codice_univoco}{/if}" /> <sup>**</sup>
				</p>
				<p class="text">
					<label for="pec">{l s='PEC'}</label>
					<input type="text" class="text" name="pec" id="pec" value="{if isset($guestInformations) && $guestInformations.pec}{$guestInformations.pec}{/if}" /> <sup>**</sup>
				</p>
				
				<p style="text-align:center; display:block;">
				** Le aziende italiane sono tenute a inserire almeno o il codice SDI o la PEC. Non accettiamo comunicazioni del codice SDI via mail.  Ti ricordiamo che in mancanza di tali dati, potrai collegarti al sito dell'Agenzia delle Entrate e scaricare la fattura.
				</p>
				
				<p class="required text">
					
					<input type="checkbox" name="newsletter" id="newsletter" value="1" {if isset($guestInformations) && $guestInformations.newsletter}checked="checked"{/if} style="margin-left:0px" />
					{l s='Sign up for our newsletter'}
				</p>
				
				<p style="text-align:center; display:block;">
				Ho letto e compreso la Vostra <a href='https://www.ezdirect.it/guide/6-informativa-sulla-privacy' target='_blank'>Privacy Policy</a> e:
				<br /><br />
					
					<input type="radio" name="consenso_1" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
					<input type="radio" name="consenso_1" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
					
					<br /><br />
					al trattamento dei dati personali per l’esecuzione dei servizi forniti tramite il Sito da parte di Ezdirect S.r.l., o ad una o più obbligazioni contrattualmente convenute, ai sensi dell’art. 1, comma 1, lettera a), b) e c) della citata informativa (consenso obbligatorio; la mancata prestazione comporterà l’impossibilità di fruire dei servizi offerti, di effettuare l’iscrizione, e di effettuare acquisti tramite il Sito).
					<br /><br />
					
					<input type="radio" name="consenso_2" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
					<input type="radio" name="consenso_2" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
					
					<br /><br />
					al trattamento dei dati personali per la realizzazione, da parte di Ezdirect S.r.l., di indagini dirette a verificare il grado di soddisfazione sui servizi offerti, ai sensi dell’art. 1, comma 2, lettera a) della citata informativa (consenso facoltativo).

					<br /><br />
					
					<input type="radio" name="consenso_3" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
					<input type="radio" name="consenso_3" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
					
					<br /><br />
					al trattamento dei dati personali a fini di marketing e/o comunicazione commerciale da parte di Ezdirect S.r.l., connesse alle attività svolte da parte della stessa e/o da parte di soggetti terzi, ai sensi dell’art. 1, comma 2, lettera b), della citata informativa (consenso facoltativo).
					<br /><br />
					

				</p>
				
				<input type='hidden' id='fatturazione' name='fatturazione' value='1' />
				<input type="hidden" name="alias" id="alias" value="{l s='My address'}" />

				<p class="text">
					
					<input type="checkbox" name="invoice_address" id="invoice_address" style="margin-left:0px" />
					{l s='Please use another address for delivery'}
				</p>
			
				<div id="opc_invoice_address" class="is_customer_param">
					{assign var=stateExist value=false}
					
					{foreach from=$inv_all_fields item=field_name}
					{if $field_name eq "company"}
					<p class="text is_customer_param">
						<label for="company_invoice">{l s='Company'}</label>
						<input type="text" class="text" id="company_invoice" name="company_invoice" value="" />
					</p>
					{/if}
					
					{if $field_name eq "firstname"}
					<p class="required text">
						<label for="firstname_invoice">{l s='First name'}</label>
						<input type="text" class="text" id="firstname_invoice" name="firstname_invoice" value="" />
						<sup>*</sup>
					{/if}	
					</p>
					{if $field_name eq "lastname"}
					<p class="required text">
						<label for="lastname_invoice">{l s='Last name'}</label>
						<input type="text" class="text" id="lastname_invoice" name="lastname_invoice" value="" />
						<sup>*</sup>
					</p>
					{/if}
					{if $field_name eq "address1"}
					<p class="required text">
						<label for="address1_invoice">{l s='Address'}</label>
						<input type="text" class="text" name="address1_invoice" id="address1_invoice" value="" />
						<sup>*</sup>
					</p>
					{/if}
					
					{if $field_name eq "country" || $field_name eq "Country:name"}
					
					<p class="required select">
						<label for="id_country_invoice">{l s='Country'}</label>
						<select name="id_country_invoice" style="width:150px" id="id_country_invoice"  onchange="countryChange_invoice();">
							<option value="">-</option>
							{foreach from=$countries item=v}
							<option value="{$v.id_country}" {if ($sl_country == $v.id_country)} selected="selected"{/if}>{$v.name|escape:'htmlall':'UTF-8'}</option>
							{/foreach}
						</select>
						<sup>*</sup>
					</p>
					<script type="text/javascript">
					$(document).ready(function(){
						countryChange_invoice();
					});
					</script>
					{/if}
					
					<div class="ee_address_invoice">
						{if $field_name eq "postcode"}
						<p class="required postcode text">
							<label for="postcode_invoice">{l s='Zip / Postal Code'}</label>
							<input type="text" class="text" name="postcode_invoice" id="postcode_invoice" value="" onkeyup="$('#postcode').val($('#postcode').val().toUpperCase());" />
							<sup>*</sup>
						</p>
						{/if}
						{if $field_name eq "city"}
						<p class="required text">
							<label for="city_invoice">{l s='City'}</label>
							<input type="text" class="text" name="city_invoice" id="city_invoice" value="" />
							<sup>*</sup>
						</p>
						{/if}
						
						{if $field_name eq "state" || $field_name eq 'State:name'}
						{$stateExist = true}
						<p class="required id_state_invoice select" style="display:none;">
							<label for="id_state_invoice">{l s='State'}</label>
							<select name="id_state_invoice" id="id_state_invoice">
								<option value="">-</option>
							</select>
							<sup>*</sup>
						</p>
						{/if}
					</div>
					
					<div class="it_address_invoice">
						{if $field_name eq "postcode"}
						<p class="required postcode text">
							<label for="postcode_invoice">{l s='Zip / Postal Code'}</label>
							<input type="text" class="text" name="postcode_invoice" id="cap_invoice" value="" onblur="$('#cap_invoice').val($('#cap_invoice').val().toUpperCase());" onkeyup="cercaCitta_invoice();" />
							<sup>*</sup>
						</p>
						{/if}
						{if $field_name eq "city"}
						<p class="required text">
							<label for="city_invoice">{l s='City'}</label>
							<select name='city_invoice' id='citta_invoice' onclick='cercaProvincia_invoice();'>
								<option name=''>-- Prima selezionare CAP --</option>
								{if isset($smarty.post.city)}
									<option name='{$smarty.post.city_invoice}' selected='selected'>{$smarty.post.city_invoice}</option>
								
								{/if}
						
							</select>
							<sup>*</sup>
						</p>
						{/if}
						
						{if $field_name eq "state" || $field_name eq 'State:name'}
						{$stateExist = true}
						<p class="required id_state_invoice select" style="display:none;">
							<label for="provincia_invoice">{l s='State'}</label>
								<input id="provincia_invoice" name="provincia_mark_invoice" readonly="readonly" type="text" value="{if isset($smarty.post.provincia_mark_invoice)}{$smarty.post.provincia_mark_invoice}{else}{$this_state}{/if}" />
								
							<sup>*</sup>
							<input id="provincia_hidden_invoice" name="id_state_invoice" type="text" readonly="readonly" style="border:0px; width:1px;height:1px" value="{if isset($smarty.post.id_state_invoice)}{$smarty.post.id_state_invoice}{/if}" />
						</p>
						{/if}
					</div>
					
					{/foreach}
					{if !$stateExist}
					<p class="required id_state_invoice select" style="display:none;">
						<label for="id_state_invoice">{l s='State'}</label>
						<select name="id_state_invoice" id="id_state_invoice">
							<option value="">-</option>
						</select>
						<sup>*</sup>
					</p>
					{/if}
					<p class="textarea is_customer_param">
						<label for="other_invoice">{l s='Additional information'}</label>
						<textarea name="other_invoice" id="other_invoice" cols="26" rows="3"></textarea>
					</p>
					<p class="text">
						<label for="phone_invoice">{l s='Home phone'}</label>
						<input type="text" class="text" name="phone_invoice" id="phone_invoice" value="" /> <sup>*</sup>
					</p>
					<p class="text is_customer_param">
						<label for="phone_mobile_invoice">{l s='Mobile phone'}</label>
						
						<input type="text" class="text" name="phone_mobile_invoice" id="phone_mobile_invoice" value="" />
					</p>
					<input type='hidden' id='fatturazione_invoice' name='fatturazione_invoice' value='0' />
					<input type='hidden' id='address2_invoice' name='address2_invoice' value='' />
					<input type="hidden" name="alias_invoice" id="alias_invoice" value="{l s='My delivery address'}" />
				</div>
				{$HOOK_CREATE_ACCOUNT_FORM}
				
				 <!-- <script src="https://www.google.com/recaptcha/api.js" async defer></script> -->
				
					 <!--<div class="g-recaptcha" style="margin:0 auto; margin-left:8px" data-sitekey="6LciANsZAAAAALhFLBBxrvJJICdKqsPVxXHfPh2i"></div> -->
					 <br />
				<div style="float: left; margin-left:-8px;" id="save_p">
					<input type='hidden' id='address2' name='address2' value='' />
					<input type='hidden' id='other' name='other' value='' />
					
					<input type="submit" class="button_large" name="submitAccount" id="submitAccount" value="{l s='Save'}" />
				</div><br /><br />
				<p style="float: right;color: green;display: none;" id="opc_account_saved">
					{l s='Account information saved successfully'}
					
				</p>
				<p style="clear: both;">
					<sup>*</sup>{l s='Required field'}
				</p>
				<!-- END Account -->
			</div>
		</fieldset>
	</form>
	<div class="clear"></div>
</div>

