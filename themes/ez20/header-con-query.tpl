<!DOCTYPE html>
<html lang="{$lang_iso}-{$lang_iso|upper}" dir="ltr">
	<head>
		<title>{$meta_title|escape:'htmlall':'UTF-8'}</title>
{if isset($meta_description) AND $meta_description}
		<meta name="description" content="{$meta_description|escape:html:'UTF-8'}" />
{/if}
{if isset($meta_keywords) AND $meta_keywords}
		<meta name="keywords" content="{$meta_keywords|escape:html:'UTF-8'}" />
{/if}

{if isset($noindex_nofollow) && $noindex_nofollow == 'y'}
	<meta name="robots" content="noindex,nofollow" />
{else}
	{if $lang_iso == 'it'}
		<meta name="robots" content="{if isset($nobots)}no{/if}index,follow" />
	{else}
		<meta name="robots" content="noindex,nofollow" />
	{/if}

{/if}

	<meta charset="utf-8" />
{if isset($canonical) AND $canonical}
		<link rel="canonical" href="{$link->getProductLink($canonical, $canonical_rew, $canonical_cat)}" />
{else}
	{if isset($cat_canonical) AND $cat_canonical}
		
	{else}
	{/if}
{/if}

{if strpos($smarty.server.REQUEST_URI, "?") || strpos($smarty.server.REQUEST_URI, "_")}
{assign var='nobots' value=true}
{/if}
		<link rel="shortcut icon" type="image/x-icon" href="{$img_ps_dir}favicon.ico?{$img_update_time}" />
		<link rel="author" href="https://plus.google.com/105789606735008987399?rel=publisher" />
		<link rel="alternate" type="application/rss+xml" title="Ezdirect.it le novit&agrave;!"  href="https://www.ezdirect.it/rss.php" />
		
		{if isset($smarty.get.id_category) && $smarty.get.id_category}
		
			{if ($smarty.get.p == 0) || ($smarty.get.p == 1)}
			
			{else}
			
				{if ($smarty.get.p == 2)}
					<link rel="prev" href="{$cat_canonical}" />
				{else}
					<link rel="prev" href="{$cat_canonical}pag/{($smarty.get.p)-1}" />
				{/if}
			{/if}
			
			{if ($smarty.get.p == $pages_nb)}
			{else}
				{if ($smarty.get.p == 0)}
					<link rel="next" href="{$cat_canonical}pag/{($smarty.get.p)+2}" />
				{else}
					<link rel="next" href="{$cat_canonical}pag/{($smarty.get.p)+1}" />
				{/if}
			{/if}
		{else if isset($smarty.get.id_manufacturer) && $smarty.get.id_manufacturer}
		
			{if ($smarty.get.p == 0) || ($smarty.get.p == 1)}
			
			{else}
				{if ($smarty.get.p == 2)}
					<link rel="prev" href="{$cat_canonical}" />
				{else}
					<link rel="prev" href="{$cat_canonical}pag/{($smarty.get.p)-1}" />
				{/if}
			{/if}
			
			{if ($smarty.get.p == $pages_nb)}
			{else}
			{if ($smarty.get.p == 0)}
					<link rel="next" href="{$cat_canonical}pag/{($smarty.get.p)+2}" />
				{else}
					<link rel="next" href="{$cat_canonical}pag/{($smarty.get.p)+1}" />
				{/if}
			{/if}
		{/if}
		
		
		
		<meta property="og:locale" content="{$lang_iso}_{$lang_iso|upper}" />
		<meta property="og:title" content="{$meta_title|escape:'htmlall':'UTF-8'}" />
		<meta property="og:site_name" content="Ezdirect" />
		{if isset($canonical) AND $canonical}
		<meta property="og:url" content="{$link->getProductLink($canonical, $canonical_rew, $canonical_cat)}" />
		{else}
			{if isset($cat_canonical) AND $cat_canonical}
	
			{/if}
		{/if}
		{if isset($cover_image) AND $cover_image}
		<meta property="og:image" content="{$cover_image}" />
		{/if}
		
		<link href="{$css_dir}select2.css" rel="stylesheet" type="text/css" media="all" />
		<!--[if lte IE 8]>
		<link href="{$css_dir}global-ie.css" rel="stylesheet" type="text/css" media="all" />
		
		
		<style>
		#loyalty-div {
		margin-top:-50px;
		}
		</style>
		<![endif] -->
		<!--[if !(lte IE 8)]>-->
		<link href="{$css_dir}checkbox.css" rel="stylesheet" type="text/css" media="all" />
		

		<!-- <![endif]-->	
		{if isset($css_files)}
		{foreach from=$css_files key=css_uri item=media}
		<link href="{$css_uri}?v=11.20" rel="stylesheet" type="text/css" media="{$media}" />
		{/foreach}
		{/if}
		{if isset($js_files)}
			{foreach from=$js_files item=js_uri}
			<script type="text/javascript" src="{$js_uri}"></script>
			{/foreach}
		{/if}
		
		<script type="text/javascript">
			var baseDir = '{$content_dir}';
			var static_token = '{$static_token}';
			var token = '{$token}';
			var priceDisplayPrecision = {$priceDisplayPrecision*$currency->decimals};
			var priceDisplayMethod = {$priceDisplay};
			var roundMode = {$roundMode};
		</script>

		<script type="text/javascript" src="{$js_dir}etc/analytics.js">
		</script>
	
	</head>
	
	<body {if $page_name}id="{$page_name|escape:'htmlall':'UTF-8'}"{/if}>
	<!-- <a href="https://www.ezdirect.it/offerte-speciali" class="bodylink" rel="nofollow"></a> -->
	<div id="contenitore">
<!--[if gte IE 9]><!-->
	<script type="text/javascript" src="{$js_dir}etc/fixedHeader.js">
</script>
<!--<![endif]-->
	<div id="header">
	
	<div id="header-in">
	
<script type="text/javascript">
var WRInitTime=(new Date()).getTime();
</script>


	
	<div id="barra-bianca">
	<a id="header_logo" href="{$link->getPageLink('index.php')}" title="Centralini telefonici">
					<img class="logo" src="{$img_ps_dir}logo.jpg" alt="Centralini telefonici" {if $logo_image_width}width="{$logo_image_width}"{/if} {if $logo_image_height}height="{$logo_image_height}" {/if} />
				</a>
	
	<!-- Block search module TOP -->
<div id="blocco_ricerca">

	<!-- Block search module TOP -->
<div id="search_block_top">
	<form method="get" action="{$link->getPageLink('search.php')}" id="searchbox">
		<fieldset>
			<label for="search_query_top"><!-- image on background --></label>
			<input type="hidden" name="orderby" value="position" />
			<input type="hidden" name="orderway" value="desc" />
			<input type="hidden" name="nnn" value="on" />
			<input type="hidden" name="cod" value="on" />
			<input type="hidden" name="sd" value="on" />
			<input type="hidden" name="ld" value="on" />
			<input class="search_query" type="text" id="search_query_top" 
			onfocus="{literal}if{/literal}(this.value=='{l s='Search...'}'){literal}{this.value='';};return false;{/literal}" 
			onblur="{literal}if{/literal}(this.value==''){literal}{this.value='{/literal}{l s='Search...'}{literal}';};return false;{/literal}" 
			name="search_query" value="{if $smarty.server.PHP_SELF|basename != "search2.php"}{if isset($smarty.get.search_query)}{$smarty.get.search_query|htmlentities:$ENT_QUOTES:'utf-8'|stripslashes}{else}{l s='Search...'}{/if}{else}{/if}" />
			
			<input type="submit" name="submit_search" value="{l s='Search'}" id="vaicerca" />
		</fieldset>
	</form>
</div>

<!-- /Block search module TOP -->

	</div>
	<div id="cart-block-new" style="z-index:999999">
	{$HOOK_RIGHT_COLUMN}

	</div>
	
	<div id="info-utente">
	{$HOOK_TOP} 
	
	
	</div>
	<div id="servizio-clienti">
	
	{if $is_mobile == 1}
	<a href="tel:+0585821163">
	{/if}
	<img src="{$img_ps_dir}servizioclienti.gif" alt="{l s='Customer care'}" style="border:0px" title="{l s='Customer care'} 0585 821163" width="138" height="58" /> 
	
	{if $is_mobile == 1}
	</a>
	{/if}
	</div>
	
	
	
	
	
	
	
	
	
	</div>
	<div id="t_breadcrumbs">
	<script type="text/javascript" src="{$js_dir}etc/bcAppend.js">
</script>

	<div id="bc">
</div>
	</div>
	
	</div> <!-- fine header-in -->
		</div> <!-- fine header -->
	
	<div id="menu-blu">
	<ul class="menucategory">
	
	
	
	
	
	<li class="cat" style="border-left:0px; margin-left:7px; width:58px; {if $link->getCategoryForHeader() == 'cuffie'}background-color:#da6600; height:36px{else}{/if}"><a title="Cuffie con microfono" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}">Cuffie telefoniche</a>
		<div class="menu-category" style="height:330px">
		<div class="submenu" style="width:160px">
		<h3 class="category"><a title="Cuffie con microfono con filo" style="width:135px" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/tipo-con_filo">Cuffie con filo</a></h3>
		<ul class="int-category">	
		<li><a title="Cuffie filari per telefono" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/tipo-con_filo/utilizzabile_con-telefono_fisso-telefoni_con_presa_jack_25_mm-dispositivi_con_jack_35_mm-telefoni_con_presa_jack_35_mm-telefono_fisso_panasonic">Per telefono fisso</a></li>
		<li><a title="Cuffie filari per cordless" href="{$link->getCategoryLink('39',$link->getCatRewrite(39))}#/tipo-con_filo/">Per telefono cordless</a></li>
		<li><a title="Cuffia con microfono per PC" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/tipo-con_filo/utilizzabile_con-pc_usb-pc_usb_bluetooth-pc_scheda_audio">Per PC</a></li>
		<li><a title="Cuffie utilizzabili su pi&ugrave; telefoni o PC" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/tipo-con_filo/utilizzabile_con-telefono_fisso_pc_usb-pc_usb_bluetooth">Multiuso</a></li>
		<li><a title="Cuffie telefoniche ottimizzate per softphone Microsoft Lync" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/tipo-con_filo/ottimizzato_per-lync_skype_for_business">Lync - Skype for Business</a></li>
		<li><a title="Cuffie con microfono compatibili con sistema operativo iOS MAC" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/tipo-con_filo/compatibile_con_mac-si">Compatibile MAC</a></li>
				<li><a title="Cuffie compatibili con Skype" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/tipo-con_filo/compatibile_skype-si">Compatibile Skype</a></li>
		<li><a title="Cuffia mono con microfono, singolo auricolare" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/tipo-con_filo/versione-monoauricolare">Monoauricolari</a></li>
		<li><a title="Cuffie microfoniche con doppio auricolare" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/tipo-con_filo/versione-biauricolare">Biauricolari</a></li>
		<li><a title="Cuffia telefonica professionale per uso intensivo" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/tipo-con_filo/utilizzo_suggerito-intensivo_call_center">Per call center</a></li>
	
		</ul>
		
		
		</div>
				<div class="submenu">
		<h3 class="category"><a title="Cuffie telefoniche senza filo DECT e Bluetooth" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/tipo-senza_filo">Cuffie senza filo</a></h3>
		<ul class="int-category">
		<li><a title="Cuffia wireless per collegamento a telefoni fissi" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/tipo-senza_filo/utilizzabile_con-telefono_fisso-telefoni_con_presa_jack_25_mm-dispositivi_con_jack_35_mm-telefoni_con_presa_jack_35_mm-telefono_fisso_panasonic">Per telefono fisso</a></li>
		<li><a title="Cuffie filari per cordless" href="{$link->getCategoryLink('39',$link->getCatRewrite(39))}#/tipo-senza_filo/">Per telefono cordless</a></li>
		<li><a title="Cuffia senza filo con microfono per softphones su PC Skype VoIP" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/tipo-senza_filo/utilizzabile_con-telefono_fisso_pc_usb-telefono_fisso_pc_usb_bluetooth-fisso_pc-fisso_pc_bluetooth-pc_usb-pc_usb_bluetooth">Per PC</a></li>
		<li><a title="Cuffie con microfono da utilizzare su pi&ugrave; dispositivi, telefono, PC, cellulare, tablet" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/tipo-senza_filo/utilizzabile_con-telefono_fisso_pc_usb-telefono_fisso_pc_usb_bluetooth-pc_usb_bluetooth/multiuso-si">Multiuso</a></li>
		<li><a title="Cuffie telefoniche senza filo compatibili con softphone Microsoft Lync" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/tipo-senza_filo/ottimizzato_per-lync_skype_for_business">Lync - Skype for Business</a></li>
		<li><a title="Cuffie senza filo con microfono compatibili con MAC" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/tipo-senza_filo/compatibile_con_mac-si">Compatibile MAC</a></li>
				<li><a title="Cuffie compatibili con Skype" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/tipo-senza_filo/compatibile_skype-si">Compatibile Skype</a></li>
		<li><a title="Cuffia cordless con microfono e singolo auricolare" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/tipo-senza_filo/versione-monoauricolare">Monoauricolari</a></li>
		<li><a title="Cuffie cordless con microfono, biauricolari" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/tipo-senza_filo/versione-biauricolare">Biauricolari</a></li>
		<li><a title="Cuffie auricolari bluetooth senza filo" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/tipo-senza_filo/utilizzabile_con-bluetooth-telefono_fisso_pc_usb_bluetooth-pc_usb_bluetooth-telefono_fisso_bluetooth">Bluetooth</a></li>

		</ul>
		<div class="submenu">
		
		<h3 class="category"><a href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/accessori-si">Accessori</a></h3>
		</div>
	
			
		</div>
		<div class="submenu" style="width:180px">
		<h3 class="category"><a title="Auricolari bluetooth per cellulari smartphones tablets e telefoni vari" href="{$link->getCategoryLink('113',$link->getCatRewrite(113))}">Auricolari bluetooth</a></h3>
		<ul class="int-category">
		<li><a title="Cuffie stereo bluetooth per musica e chiamate da cellulare e VoIP" href="{$link->getCategoryLink('113',$link->getCatRewrite(113))}#/stereo-si">Stereo</a></li>
		<li><a title="Auricolari utilizzabili con pi&ugrave; dispositivi bluetooth" href="{$link->getCategoryLink('113',$link->getCatRewrite(113))}#/multipunto_bluetooth-si">Multipunto</a></li>
		<li><a title="Auricolari bluetooth utilizzabili su dispositivi con diversa tecnologia" href="{$link->getCategoryLink('113',$link->getCatRewrite(113))}#/multiuso-si">Multiuso</a></li>
		
		<li><a  href="{$link->getCategoryLink('113',$link->getCatRewrite(113))}#/ottimizzato_per-lync_skype_for_business">Lync - Skype for Business</a></li>
		
		</ul>
		
		<h3 class="category"><a title="Cuffie antirumore passive comunicanti 3M Peltor" href="{$link->getCategoryLink('241',$link->getCatRewrite(241))}">Cuffie antirumore</a></h3>
		<ul class="int-category">
		<li><a title="Cuffia antirumore comunicante" href="{$link->getCategoryLink('241',$link->getCatRewrite(241))}#/tipo-cuffia_antirumore_comunicante">Antirumore comunicanti</a></li>
		<li><a title="Cuffie antirumore passive" href="{$link->getCategoryLink('241',$link->getCatRewrite(241))}#/tipo-cuffia_antirumore_passiva">Antirumore passive</a></li>
		<li><a title="Accessori per cuffie antirumore" href="{$link->getCategoryLink('241',$link->getCatRewrite(241))}#/tipo-access_antirumore">Accessori</a></li>
	
		
		</ul>
		
		
		</div>
		<div class="submenu" style="width:130px">
		
		{if $categoryHeader->checkSpecialiPerMacroCategoria(106) > 0}
			<h3 class="category"><a title="Offerte speciali" style="width:105px; background-color:#cc0000" href="{$link->getPageLink('prices-drop.php', true)}?category=106">Offerte speciali</a></h3>
		{else}
		{/if}
		<h3 title="Filtra per prezzo" class="category">Prezzo</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/price-0-50">Fino a 50 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/price-51-100">Da 51 a 100 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/price-101-150)">Da 101 a 150 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/price-151-200">Da 151 a 200 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/price-200-1100">Oltre 200 &euro;</a></li>
		
		</ul>
		</div>
						<div class="submenu" style="width:115px">
		<h3 class="category">Produttori</h3>
		<ul class="int-category">
		<li style="width:130px"><a href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/produttore-ezdirect">Ezdirect</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/produttore-jabra">Jabra</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/produttore-plantronics">Plantronics</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}#/produttore-sennheiser">Sennheiser</a></li>

		<li style="width:130px"><a href="{$link->getCategoryLink('241',$link->getCatRewrite(241))}#/produttore-peltor_3m">Peltor 3M</a>
		</li>
		</ul>
		<br />
		<strong><a style="font-size:10px; font-weight:normal" href="{$link->getCategoryLink('106',$link->getCatRewrite(106))}">Tutte le cuffie...</a></strong>
		
		
		</div>
			
		<div class="cat-separatore">
				<div style="margin-left:25px;">
				
					<h3 class="category"><a href="{$link->getPageLink('new-products.php')}?cat=106" style="width:110px">Nuovi prodotti</a></h3>
		
		<h3 class="category"><a href="{$link->getPageLink('best-sales.php')}?cat=106" style="width:110px; margin-top:-10px">I pi&ugrave; venduti</a></h3>
	
					
				
				
		<h3 class="category"><a href='https://www.ezdirect.it/guide/category/5-guide-e-consigli' style='width:110px'>Guide</a></h3>
		<ul class="int-categ-marg">
		<li><a href="{$link->getCMSLink(20, $link->getCMSRewrite(20))}" title="Glossario cuffie">Glossario cuffie</a></li>
		<li><a href="{$link->getCMSLink(25, $link->getCMSRewrite(25))}" title="Cuffie, guida alla scelta">Cuffie guida alla scelta</a></li>
		<li><a href="{$link->getCMSLink(13, $link->getCMSRewrite(13))}" title="Salute e uso delle cuffie">Salute e uso delle cuffie</a></li>
		<li><a href="{$link->getCMSLink(46, $link->getCMSRewrite(46))}" title="Skype cuffie e telefoni">Skype cuffie e telefoni</a></li>
		<li><a href="http://www.it.jabra.com/headsets-and-speakerphones/compatibilityguide" target="_blank">Compatibilit&agrave; Jabra</a></li>
		
		
<li><a href="https://www.plantro.net/compatibility.php" target="_blank">Compatibilit&agrave; Plantronics</a></li>
<li><a href="http://en-de.sennheiser.com/telephone-headset-compatibility" target="_blank">Compatibilit&agrave; Sennheiser</a></li>
		</ul>
		

		</div>
		
		</div>
						
		
		<div style="clear:both"></div>
		</div>
	</li>
	
	
	
	
	
	
	
	
	<li class="cat" style="width:55px; {if $link->getCategoryForHeader() == 'centralini'}background-color:#da6600; height:36px{else}{/if}"><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}">Centralini telefonici</a>
	<div class="menu-category" style="height:300px">
		<div class="submenu" style="width:130px">
		<h3 class="category">Tipo di linea</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/tipo_linee-analogica-analogica_voip_isdn-isdn_voip_analogica-voip_analogica-analogica_voip-analogica_gsm-analogica_isdn-analogiciedigitali">Analogica</a></li>
		<li><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/tipo_linee-voip-analogica_isdn-isdn_voip-analogica_voip_isdn-isdn_voip_analogica">ISDN</a></li>
		<li><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/tipo_linee-voip-isdn_voip-analogica_voip_isdn-isdn_voip_analogica-voip_analogica-analogica_voip">VoIP</a></li>
		<li><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/tipo-centralino_dect">Centralini IP DECT</a></li>
		</ul>
		</div>
	
	<div class="submenu">
		<h3 class="category">Tipo interni</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/tipo_di_derivati_int_collegabili-analogici_digitali_voip-int_analogici_digita-analogici_voip-analogici">Analogici</a></li>
		<li><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/tipo_di_derivati_int_collegabili-analogici_digitali_voip">Analogici + digitali + VoIP</a></li>
		<li><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/tipo_di_derivati_int_collegabili-analogici_voip">Analogici + VoIP</a></li>
		<li><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/tipo_di_derivati_int_collegabili-analogici_digitali_voip-analogici_voip">VoIP</a></li>
	
		
		</ul>
		<br />
		<strong><a style="font-size:10px; font-weight:normal"  href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}">Tutti i centralini...</a></strong>
		</div>
		
			<div class="submenu">
		<h3 class="category">Funzioni</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/messaggi_vocali_su_linee_opa-si">Messaggi vocali</a></li>
		<li><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/registrazione_conversazioni-si">Registrazione</a></li>
		<li><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/funzioni_hotel-si">Funzioni hotel</a></li>
		<li><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/riconoscimento_fax_automatico-si">Riconoscimento FAX</a></li>
		<li><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/least_cost_routing_lcr-si">Instradamento LCR</a></li>
		
		</ul>
		</div>
		<div class="submenu" style="width:130px">
		{if $categoryHeader->checkSpecialiPerMacroCategoria(23) > 0}
			<h3 class="category"><a title="Offerte speciali" style="width:105px; background-color:#cc0000" href="{$link->getPageLink('prices-drop.php', true)}?category=23">Offerte speciali</a></h3>
		{else}
		{/if}
		<h3 class="category">Prezzo</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/price-0-200">Fino a 200 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/price-201-500">Da 201 a 500 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/price-501-1000">Da 501 a 1000 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/price-1000-9000">Oltre 1000 &euro;</a></li>

		
		</ul>
		<div class="submenu">
		
		
		</div>
		
		
		
		
		
		</div>
		<div class="submenu" style="width:135px">
		<h3 class="category">Produttori</h3>
		<ul class="int-category">
		<li style="width:130px"><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/produttore-ezdirect" title="Centralini Ezdirect" >Ezdirect</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/produttore-siemens/produttore-gigaset" title="Centralini Siemens">Siemens Gigaset</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/produttore-panasonic" title="Centralini Panasonic">Panasonic</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/produttore-samsung" title="Centralini Samsung">Samsung</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/produttore-yeastar" title="Centralini Yeastar">Yeastar</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/produttore-cisco" title="Centralini Cisco">Cisco</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/produttore-snom" title="Centralini Snom">Snom</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/produttore-voismart" title="Centralini Voismart">Voismart</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/produttore-esse_ti" title="Centralini Esse-Ti">Esse-Ti</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/produttore-avm_fritz" title="Centralini AVM Fritz">AVM Fritz</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('23',$link->getCatRewrite(23))}#/produttore-grandstream" title="Centralini Grandtream">Grandstream</a></li>
	
		</ul>
				
		</div>
		
	<div class="cat-separatore">
			<div style="margin-left:25px;">
				
					<h3 class="category"><a href="{$link->getPageLink('new-products.php')}?cat=23" style="width:110px">Nuovi prodotti</a></h3>
		
		<h3 class="category"><a href="{$link->getPageLink('best-sales.php')}?cat=23" style="width:110px; margin-top:-10px">I pi&ugrave; venduti</a></h3>
	
					
				
				
		<h3 class="category"><a href='https://www.ezdirect.it/guide/category/5-guide-e-consigli' style='width:110px'>Guide</a></h3>
		<ul class="int-categ-marg">
		<li><a href="{$link->getCMSLink(8, $link->getCMSRewrite(8))}" title="Centralini funzioni">Centralini funzioni</a></li>
		<li><a href="{$link->getCMSLink(10, $link->getCMSRewrite(10))}" title="Centralini guida">Centralini guida</a></li>
		<li><a href="{$link->getCMSLink(46, $link->getCMSRewrite(46))}" title="Skype cuffie e telefoni">Skype cuffie e telefoni</a></li>
		
		</ul>
		

		</div>
		</div>
						
		
		<div style="clear:both"></div>
	</div>
	
	</li>
	
	
	
	
	
	
	
	
	
	
	<li class="cat" style="width:43px; {if $link->getCategoryForHeader() == 'fissi'}background-color:#da6600; height:36px{else}{/if}"><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}">Telefoni fissi</a>
	
	<div class="menu-category">
		
	<div class="submenu">
		<h3 class="category">Tipo</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/tipo-tptelanalogico">Analogico</a></li>
		<li><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/tipo-tel_specifico_pabx">Specifici (PABX)</a></li>
		<li><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/tipo-tel_voip-tel_voip_wifi">VoIP</a></li>
		<li><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/tipo-tel_usb">USB</a></li>
		</ul>
	
	
	</div>
	
	<div class="submenu">
		<h3 class="category">Funzioni</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/attacco_cuffia-jack_25_mm-jack_35_mm-plug_rj9-plug_rj9_jack_25_mm">Attacco per cuffia</a></li>
		<li><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/telecamera-si">Telecamera</a></li>
		<li><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/android-si">Android</a></li>
		</ul>
		</div>
	
	
	
	<div class="submenu">
	{if $categoryHeader->checkSpecialiPerMacroCategoria(115) > 0}
			<h3 class="category"><a title="Offerte speciali" style="width:105px; background-color:#cc0000" href="{$link->getPageLink('prices-drop.php', true)}?category=115">Offerte speciali</a></h3>
		{else}
		{/if}
		<h3 class="category">Prezzo</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/price-0-25">Fino a 25 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/price-26-50">Da 26 a 50 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/price-51-100">Da 51 a 100 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/price-101-150">Da 101 a 150 &euro;</a></li>
<li><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/price-150-1000">Oltre 150 &euro;</a></li>

		
		</ul>
			<br />
		<strong><a style="font-size:10px; font-weight:normal"  href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}">Tutti i telefoni...</a></strong>
		
		
		
		
		</div>
		<div class="submenu" style="width:205px">
		<h3 class="category">Produttori</h3>
		<ul class="int-category">
		<li style="width:130px"><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/produttore-gigaset" title="Telefoni fissi Siemens Gigaset">Gigaset</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/produttore-grandstream" title="Telefoni con filo Grandstream">Grandstream</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/produttore-fanvil" title="Telefoni fissi Fanvil">Fanvil</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/produttore-escene" title="Telefoni fissi Escene">Escene</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/produttore-cisco" title="Telefoni fissi Cisco">Cisco</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/produttore-panasonic" title="Telefoni fissi Panasonic">Panasonic</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/produttore-samsung" title="Telefoni fissi Samsung">Samsung</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/produttore-aastra" title="Telefoni fissi Aastra">Aastra</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/produttore-ericsson" title="Telefoni con filo Ericsson">Ericsson</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/produttore-yealink" title="Telefoni con filo Yealink">Yealink</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/produttore-alcatel" title="Telefoni fissi Alcatel">Alcatel</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/produttore-avaya" title="Telefoni fissi Alcatel">Avaya</a></li>
		</ul>
				
		</div>
				<div class="cat-separatore">
		<div style="margin-left:25px;">
				
					<h3 class="category"><a href="{$link->getPageLink('new-products.php')}?cat=115" style="width:110px">Nuovi prodotti</a></h3>
		
		<h3 class="category"><a href="{$link->getPageLink('best-sales.php')}?cat=115" style="width:110px; margin-top:-10px">I pi&ugrave; venduti</a></h3>
	
					
				
				
		<h3 class="category"><a href='https://www.ezdirect.it/guide/category/5-guide-e-consigli' style='width:110px'>Guide</a></h3>
		<ul class="int-categ-marg">
		<li><a href="{$link->getCMSLink(42, $link->getCMSRewrite(42))}" title="Telefoni VoIP guida">Telefoni VoIP guida</a></li>
		
		<li><a href="{$link->getCMSLink(46, $link->getCMSRewrite(46))}" title="Skype cuffie e telefoni">Skype cuffie e telefoni</a></li>
		
		</ul>
		

		</div>
		</div>
						
		
		<div style="clear:both"></div>
		</div>
	</li>
	
	
	
	
	<li class="cat" style="width:50px; {if $link->getCategoryForHeader() == 'cordless'}background-color:#da6600; height:36px{else}{/if}"><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}">Telefoni cordless</a>
		<div class="menu-category">
	<div class="submenu">
		<h3 class="category">Tipo</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/tipo-cordless_tipo_analog">Analogico</a></li>
		<li><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/tipo-cordless_tipo_voip-cordless_tipo_voip_wifi">VoIP</a></li>
<li><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/tipo-cordless_tipo_wifi-cordless_tipo_voip_wifi">Wi-fi</a></li>
<li><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/tipo-cordless_tipo_pabx">Specifici (PABX)</a></li>

		</ul>
	<div class="submenu" style="width:180px">
		<h3 class="category"><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/tipo-terminali_aggiuntivi">Portatili aggiuntivi</a></h3>
		</div>
	
	</div>
	<div class="submenu">
		<h3 class="category">Funzioni</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/touchscreen-si">Touchscreen</a></li>
		<li><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/attacco_cuffia-jack_25_mm-jack_35_mm-attacco_specifico-specifico">Attacco per cuffia</a></li>
		<li><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/viva_voce-vivavoce_si">Viva voce</a></li>
		<li><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/vibrazione-si">Vibrazione</a></li>
		<li><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/ricarica_indipendente_dalla_base-si">Ricarica indipendente dalla base</a></li>
		<li><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/segreteria_telefonica-si">Segreteria telefonica</a></li>
<li><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/bluetooth_integrato-si">Bluetooth</a></li>

		</ul>
	
	
	</div>
		<div class="submenu">
		{if $categoryHeader->checkSpecialiPerMacroCategoria(128) > 0}
			<h3 class="category"><a title="Offerte speciali" style="width:105px; background-color:#cc0000" href="{$link->getPageLink('prices-drop.php', true)}?category=128">Offerte speciali</a></h3>
		{else}
		{/if}
		<h3 class="category">Prezzo</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/price-0-30">Fino a 30 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/price-31-50">Da 31 a 50 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/price-51-100">Da 51 a 100 &euro;</a></li>
<li><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/price-100-1000">Oltre 100 &euro;</a></li>

		
		</ul>
	
		
		
		
		
		</div>
		<div class="submenu"  style="width:205px">
		<h3 class="category">Produttori</h3>
		<ul class="int-category">
		<li style="width:130px"><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/produttore-gigaset">Siemens Gigaset</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/produttore-yealink">Yealink</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/produttore-panasonic">Panasonic</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/produttore-snom">Snom</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}#/produttore-engenius">Engenius</a>
		</li>
		</ul>
		<br />
		<strong><a style="font-size:10px; font-weight:normal" href="{$link->getCategoryLink('128',$link->getCatRewrite(128))}">Tutti i cordless...</a></strong>
		
		</div>
				<div class="cat-separatore">
				<div style="margin-left:25px;">
				
					<h3 class="category"><a href="{$link->getPageLink('new-products.php')}?cat=128" style="width:110px">Nuovi prodotti</a></h3>
		
		<h3 class="category"><a href="{$link->getPageLink('best-sales.php')}?cat=128" style="width:110px; margin-top:-10px">I pi&ugrave; venduti</a></h3>
	
					
				
				
		<h3 class="category"><a href='https://www.ezdirect.it/guide/category/5-guide-e-consigli' style='width:110px'>Guide</a></h3>
		<ul class="int-categ-marg">
		
		
		<li><a href="{$link->getCMSLink(46, $link->getCMSRewrite(46))}" title="Skype cuffie e telefoni">Skype cuffie e telefoni</a></li>
			<li><a href="{$link->getCMSLink(47, $link->getCMSRewrite(47))}" title="Skype cuffie e telefoni">Guida cordless</a></li>
		
		</ul>
		

		</div>
		</div>
						
		
		<div style="clear:both"></div>
	
	
	</div>
	</li>
	<li class="cat" style="width:94px; {if $link->getCategoryForHeader() == 'audioconferenze'}background-color:#da6600; height:36px{else}{/if}"><a href="{$link->getCategoryLink('75',$link->getCatRewrite(75))}">Audioconferenza e audioguide</a>
	<div class="menu-category" style="height:320px">
	<div class="submenu">
		<h3 class="category"><a href="{$link->getCategoryLink('75',$link->getCatRewrite(75))}">Audioconferenza</a></h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('75',$link->getCatRewrite(75))}#/tipo-audc_anlg">Analogico</a></li>
		<li><a href="{$link->getCategoryLink('75',$link->getCatRewrite(75))}#/tipo-voip">VoIP</a></li>
		<li><a href="{$link->getCategoryLink('75',$link->getCatRewrite(75))}#/tipo-audc_usb">USB</a></li>
<li><a href="{$link->getCategoryLink('75',$link->getCatRewrite(75))}#/tipo-multiuso">Multiuso</a></li>
<li><a href="{$link->getCategoryLink('75',$link->getCatRewrite(75))}#/tipo-solo_bluetooth">Solo bluetooth</a></li>
		</ul>
		<p>
			<strong><a style="font-size:10px; font-weight:normal" href="{$link->getCategoryLink('75',$link->getCatRewrite(75))}">Tutte le audioconferenze...</a></strong>
		
		</p>
	</div>
	
		
				<div class="submenu">
		<h3 class="category">Produttori</h3>
		<ul class="int-category">
			<li style="width:130px"><a href="{$link->getCategoryLink('75',$link->getCatRewrite(75))}#/produttore-konftel" title="Audioconferenza Konftel">Konftel</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('76',$link->getCatRewrite(76))}#/produttore-polycom" title="Audioconferenza e videoconferenza Polycom">Polycom</a></li>
				<li style="width:130px"><a href="{$link->getCategoryLink('75',$link->getCatRewrite(75))}#/produttore-jabra" title="Audioconferenza Jabra">Jabra</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('75',$link->getCatRewrite(75))}#/produttore-plantronics" title="Audioconferenza Plantronics">Plantronics</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('76',$link->getCatRewrite(76))}#/produttore-panasonic" title="Audioconferenza e videoconferenza Panasonic">Panasonic</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('76',$link->getCatRewrite(76))}#/produttore-lifesize" title="Conferenza Lifesize">Lifesize</a></li>
	<li style="width:130px"><a href="{$link->getCategoryLink('76',$link->getCatRewrite(76))}#/produttore-yealink" title="Conferenza Yealink">Yealink</a></li>
	<li style="width:130px"><a href="{$link->getCategoryLink('76',$link->getCatRewrite(76))}#/produttore-grandstream" title="Conferenza Grandstream">Grandstream</a></li>
	
		
		</ul>
			
		</div>
		
	<div class="submenu" >
		<h3 class="category"><a href="{$link->getCategoryLink('144',$link->getCatRewrite(144))}">Visite guidate</a></h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('144',$link->getCatRewrite(144))}#/tipo_dispositivo-via_radio_con_guida">Via radio con guida</a></li>
		<li><a href="{$link->getCategoryLink('144',$link->getCatRewrite(144))}#/tipo_dispositivo-file_vocali_mp3_wav">Con file vocali</a></li>
		
		</ul>
	
			<div class="submenu" style="width:205px">
		<h3 class="category">Produttori</h3>
		<ul class="int-category">
		<li style="width:130px"><a href="{$link->getCategoryLink('144',$link->getCatRewrite(144))}#/produttore-cobra" title="Audioguide e radioguide Cobra">Cobra</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('144',$link->getCatRewrite(144))}#/produttore-logos" title="Audioguide e radioguide Logos">Logos</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('144',$link->getCatRewrite(144))}#/produttore-midland" title="Audioguide e radioguide Midland">Midland</a></li>

		
		</ul>
			<p>
		<strong><a style="font-size:10px; font-weight:normal" href="{$link->getCategoryLink('144',$link->getCatRewrite(144))}">Tutte le audioguide e radioguide...</a></strong>
		
		</p>
		</div>
		</div>
	
	
	
	
		<div class="submenu" style="width:205px">
		<h3 class="category"><a href="{$link->getCategoryLink('156',$link->getCatRewrite(156))}">Ricetrasmittenti</a></h3>
			<ul class="int-category">
		<li><a href="{$link->getCategoryLink('156',$link->getCatRewrite(156))}#/resistente_all_acqua-si">Resistente all'acqua</a></li>
		<li><a href="{$link->getCategoryLink('156',$link->getCatRewrite(156))}#/frequenza-pmr446">PMR 446</a></li>
		<li><a href="{$link->getCategoryLink('156',$link->getCatRewrite(156))}#/frequenza-pmr446_lpd">PMR 446/LPD</a></li>

		</ul>
		
			<div class="submenu">
		<h3 class="category">Produttori</h3>
		<ul class="int-category">
		<li style="width:130px"><a href="{$link->getCategoryLink('156',$link->getCatRewrite(156))}#/produttore-midland" title="Ricetrasmittenti Midland">Midland</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('156',$link->getCatRewrite(156))}#/produttore-motorola" title="Ricetrasmittenti Motorola">Motorola</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('156',$link->getCatRewrite(156))}#/produttore-alan" title="Ricetrasmittenti Alan">Alan</a></li>
	
		
		</ul>
		<br />
			<p style="font-size:10px">
		<strong><a style="font-weight:normal" href="{$link->getCategoryLink('156',$link->getCatRewrite(156))}">Tutte le ricetrasmittenti...</a></strong>
		
		</p>
		</div>
		</div>
	
	
		
		<div class="cat-separatore">
				<div style="margin-left:25px;">
				
					<h3 class="category"><a href="{$link->getPageLink('new-products.php')}?cat=75" style="width:110px">Nuovi prodotti</a></h3>
		
		<h3 class="category"><a href="{$link->getPageLink('best-sales.php')}?cat=75" style="width:110px; margin-top:-10px">I pi&ugrave; venduti</a></h3>
	
					
				
				
		<h3 class="category"><a href='https://www.ezdirect.it/guide/category/5-guide-e-consigli' style='width:110px'>Guide</a></h3>
		<ul class="int-categ-marg">
		<li><a href="{$link->getCMSLink(43, $link->getCMSRewrite(43))}" title="Teleconferenza guida">Teleconferenza guida</a></li>
		
		<li><a href="{$link->getCMSLink(46, $link->getCMSRewrite(46))}" title="Skype cuffie e telefoni">Skype cuffie e telefoni</a></li>
		
		</ul>
		

		</div>
		</div>
						
		
		<div style="clear:both"></div>
	

	
	
	
	
	
	
	</div>
	</li>
	
		<li class="cat" style="width:60px;  {if $link->getCategoryForHeader() == 'videoconferenze'}background-color:#da6600; height:36px{else}{/if}"><a href="{$link->getCategoryLink('76',$link->getCatRewrite(76))}">Video conferenza</a>
	<div class="menu-category">
	<div class="submenu">
		&nbsp;
	</div>
	
	
		<div class="submenu">
		&nbsp;
		</div>
		<div class="submenu">
		{if $categoryHeader->checkSpecialiPerMacroCategoria(76) > 0}
			<h3 class="category"><a title="Offerte speciali" style="width:105px; background-color:#cc0000" href="{$link->getPageLink('prices-drop.php', true)}?category=76">Offerte speciali</a></h3>
		{else}
		{/if}
		<h3 class="category">Prezzo</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('76',$link->getCatRewrite(76))}#/price-0-100">Fino a 100 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('76',$link->getCatRewrite(76))}#/price-101-500">Da 101 a 500 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('76',$link->getCatRewrite(76))}#/price-501-1000">Da 501 a 1000 &euro;</a></li>
<li><a href="{$link->getCategoryLink('76',$link->getCatRewrite(76))}#/price-1000-10000">Oltre 1000 &euro;</a></li>

		
		</ul>
	
		
		
		
		
		</div>
		<div class="submenu" style="width:205px">
		<h3 class="category">Produttori</h3>
		<ul class="int-category">
		<li style="width:130px"><a href="{$link->getCategoryLink('76',$link->getCatRewrite(76))}#/produttore-polycom">Polycom</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('76',$link->getCatRewrite(76))}#/produttore-panasonic">Panasonic</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('76',$link->getCatRewrite(76))}#/produttore-lifesize">Lifesize</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('76',$link->getCatRewrite(76))}#/produttore-grandstream">Grandstream</a></li>
		</ul>
	
						<br />
		<strong><a style="font-size:10px; font-weight:normal"  href="{$link->getCategoryLink('76',$link->getCatRewrite(76))}">Tutte le videoconferenze...</a></strong>
		
		</div>
		
				<div class="cat-separatore">
						<div style="margin-left:25px;">
				
					<h3 class="category"><a href="{$link->getPageLink('new-products.php')}?cat=76" style="width:110px">Nuovi prodotti</a></h3>
		
		<h3 class="category"><a href="{$link->getPageLink('best-sales.php')}?cat=76" style="width:110px; margin-top:-10px">I pi&ugrave; venduti</a></h3>
	
		
		</div>
		</div>
						
		
		<div style="clear:both"></div>
	</div>
	
	</li>
	
	
	
	
	<li class="cat" style="width:73px; {if $link->getCategoryForHeader() == 'videocamere'}background-color:#da6600; height:36px{else}{/if}"><a href="{$link->getCategoryLink('147',$link->getCatRewrite(147))}">Videocamere IP e NVR</a>
	<div class="menu-category">
	<div class="submenu">
		<h3 class="category">Tipo</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('147',$link->getCatRewrite(147))}#/tipo-con_cavo_di_rete">Con cavo di rete</a></li>
		<li><a href="{$link->getCategoryLink('147',$link->getCatRewrite(147))}#/tipo-network_video_recorder">NVR (Network Video Recorder)</a></li>
		<li><a href="{$link->getCategoryLink('147',$link->getCatRewrite(147))}#/tipo-wireless">Wireless</a></li>
		<li><a href="{$link->getCategoryLink('147',$link->getCatRewrite(147))}#/tipo-access_videocam">Accessori</a></li>
		</ul>
	</div>
	
	
		<div class="submenu">
		<h3 class="category">Utilizzo</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('147',$link->getCatRewrite(147))}#/utilizzo-da_interno">Da interno</a></li>
		<li><a href="{$link->getCategoryLink('147',$link->getCatRewrite(147))}#/utilizzo-interno_esterno">Interno - esterno</a></li>

	
	
		</ul>
		</div>
		<div class="submenu">
		{if $categoryHeader->checkSpecialiPerMacroCategoria(147) > 0}
			<h3 class="category"><a title="Offerte speciali" style="width:105px; background-color:#cc0000" href="{$link->getPageLink('prices-drop.php', true)}?category=147">Offerte speciali</a></h3>
		{else}
		{/if}
		<h3 class="category">Prezzo</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('147',$link->getCatRewrite(147))}#/price-0-100">Fino a 100 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('147',$link->getCatRewrite(147))}#/price-101-500">Da 101 a 500 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('147',$link->getCatRewrite(147))}#/price-501-1000">Da 501 a 1000 &euro;</a></li>
<li><a href="{$link->getCategoryLink('147',$link->getCatRewrite(147))}#/price-1000-10000">Oltre 1000 &euro;</a></li>

		
		</ul>
	
		
		
		
		
		</div>
		<div class="submenu" style="width:205px">
		<h3 class="category">Produttori</h3>
		<ul class="int-category">
		
		<li style="width:130px"><a href="{$link->getCategoryLink('147',$link->getCatRewrite(147))}#/produttore-milesight">Milesight</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('147',$link->getCatRewrite(147))}#/produttore-recovision">Recovision</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('147',$link->getCatRewrite(147))}#/produttore-panasonic">Panasonic</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('147',$link->getCatRewrite(147))}#/produttore-raytalk">Raytalk</a></li>
		</ul>
	
						<br />
		<strong><a style="font-size:10px; font-weight:normal"  href="{$link->getCategoryLink('147',$link->getCatRewrite(147))}">Tutte le videocamere...</a></strong>
		
		</div>
		
				<div class="cat-separatore">
						<div style="margin-left:25px;">
				
					<h3 class="category"><a href="{$link->getPageLink('new-products.php')}?cat=147" style="width:110px">Nuovi prodotti</a></h3>
		
		<h3 class="category"><a href="{$link->getPageLink('best-sales.php')}?cat=147" style="width:110px; margin-top:-10px">I pi&ugrave; venduti</a></h3>
	
		
			<h3 class="category"><a href='https://www.ezdirect.it/guide/category/5-guide-e-consigli' style='width:110px'>Guide</a></h3>
		<ul class="int-categ-marg">
		
		
		
			<li><a href="{$link->getCMSLink(64, $link->getCMSRewrite(64))}" title="Come funziona una telecamera IP">Come funziona una telecamera IP?</a></li>
		
		</ul>

		</div>
		</div>
						
		
		<div style="clear:both"></div>
	</div>
	
	
	
	
	
	
	
	</li>
	

	
	
	<li class="cat" style="width:72px; {if $link->getCategoryForHeader() == 'gsm'}background-color:#da6600; height:36px{else}{/if}"><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}">GSM-UMTS Gateway</a>
	
	<div class="menu-category">
	<div class="submenu" style="width:160px">
	<h3 class="category">Tipo</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/tipo-gateway_gsm">Gateway GSM</a></li>
		<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/tipo-gateway_umts">Gateway UMTS</a></li>
		<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/tipo-accessori_gsm">Accessori GSM</a></li>
		<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/tipo-telefono_gsm">Telefoni GSM</a></li>
		<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/tipo-tracker_gps">Tracker GPS</a></li>
		<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/tipo-router_umts-router_lte_4g">Router 3G 4G</a></li>
		<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/tipo-telecomando_gsm">Telecomando GSM</a></li>
		<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/tipo-amplificatore_umts_3g-amplificatore_gsm-amplificatore_umts_gsm">Amplificatori</a></li>
		</ul>
	
	
	
	</div>
	
	
	
	
	<div class="submenu">
		<h3 class="category">Connessione</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/connessione-isdn">ISDN</a></li>
		<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/connessione-voip-voip_analogica">VoIP</a></li>
	<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/connessione-analogica-voip_analogica">Analogica</a></li>
		</ul>
			<div class="submenu">
		<h3 class="category">Tecnologia</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/tipo-gateway_gsm-accessori_gsm-telefono_gsm-tracker_gsm-telecomando_gsm">GSM</a></li>
		<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/tipo-gateway_umts-router_umts">UMTS</a></li>
	
		</ul>
		</div>
		
		
	</div>
	
		<div class="submenu" style="width:180px">
		<h3 class="category">SIM gestite</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/quantita_sim_gestite-1">1</a></li>
		<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/quantita_sim_gestite-2">2</a></li>
	<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/quantita_sim_gestite-4-8-16-32">Pi&ugrave; di 2</a></li>
		</ul>
		<div class="submenu">
		<h3 class="category">Funzioni</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/gestione_fax_diretto-si">Gestione fax</a></li>
		<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/sensori-si_1-si_2-si">Sensori</a></li>
	<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/rele-si_1-si_2-si">Rel&egrave;</a></li>
		<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/avviso_fine_credito-si">Gestione credito</a></li>
		</ul>
	</div>	
		
		
	</div>
	
	
	
	<div class="submenu" style="width:130px">
	{if $categoryHeader->checkSpecialiPerMacroCategoria(27) > 0}
			<h3 class="category"><a title="Offerte speciali" style="width:105px; background-color:#cc0000" href="{$link->getPageLink('prices-drop.php', true)}?category=27">Offerte speciali</a></h3>
		{else}
		{/if}
		<h3 class="category">Prezzo</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/price-0-170">Fino a 170 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/price-171-300">Da 171 a 300 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/price-301-500">Da 301 a 500 &euro;</a></li>
<li><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/price-501-1000">Oltre 500 &euro;</a></li>

		
		</ul>
	
		
		
		
		
		</div>
		<div class="submenu" style="width:115px">
			<h3 class="category">Produttori</h3>
		<ul class="int-category">
	
		<li style="width:130px"><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/produttore-yeastar">Yeastar</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/produttore-tema">Tema</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/produttore-2n">2N</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/produttore-imagicle">Imagicle</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/produttore-teltonika">Teltonika</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/produttore-mtt">M.T.T.</a></li>
		</ul>
	
						<br />
		<strong><a style="font-size:10px; font-weight:normal" href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}">Tutti i GSM-UMTS...</a></strong>
		
		</div>
			
	<div class="cat-separatore">
						<div style="margin-left:25px;">
				
					<h3 class="category"><a href="{$link->getPageLink('new-products.php')}?cat=27" style="width:110px">Nuovi prodotti</a></h3>
		
		<h3 class="category"><a href="{$link->getPageLink('best-sales.php')}?cat=27" style="width:110px; margin-top:-10px">I pi&ugrave; venduti</a></h3>
	
					
				
				
		<h3 class="category"><a href='https://www.ezdirect.it/guide/category/5-guide-e-consigli' style='width:110px'>Guide</a></h3>
		<ul class="int-categ-marg">
		<li><a href="{$link->getCMSLink(44, $link->getCMSRewrite(44))}" title="Gateway GSM guida">Gateway GSM guida</a></li>
		
		
		
		</ul>
		

		</div>
		</div>
						
		
		<div style="clear:both"></div>
	
	</div>
	
	</li>
	
	
	
	
	
	
	
	
	
	
	
	
	<li class="cat" style="width:51px; {if $link->getCategoryForHeader() == 'voipgateway'}background-color:#da6600; height:36px{else}{/if}"><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}">VoIP Gateway</a>
	
	
	<div class="menu-category" style="height:330px">
	<div class="submenu">
		<h3 class="category">Connessione</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/porte_fxo-1-2-4-8-16-24-32-48">Gateway VoIP FXO</a></li>
		<li><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/porte_fxs-1-2-4-8-16-24-32-48">Gateway VoIP FXS</a></li>
	<li><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/porte_fxs-1-2-4-16-24-32-48/porte_fxo-1-2-4-8-16-24-32-48">Gateway VoIP FXS + FXO</a></li>
		<li><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/porte_isdn-1-2-4-3-5">Gateway VoIP ISDN</a></li>
		</ul>
			<div class="submenu">
			<h3 class="category">Funzioni</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/qos_quality_of_service-si">Quality of Service</a></li>
		<li><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/vlan-si">VLAN</a></li>
	
		</ul>
		
		
		</div>
			

	</div>
	<div class="submenu">
		<h3 class="category">Canali</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/quantita_canali-2">Fino a 2</a></li>
		<li><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/quantita_canali-4">4</a></li>
	<li><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/quantita_canali-8">8</a></li>
		<li><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/quantita_canali-24">Oltre 8</a></li>
		</ul>
		<div class="submenu">
			<h3 class="category">Protocolli</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/protocolli_gestiti-sip">SIP</a></li>
		<li><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/protocolli_gestiti-sip_h323">SIP H323</a></li>
	<li><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/protocolli_gestiti-sip_h323_mgcp">SIP H323 MGCP</a></li>
		</ul>
		
		
		</div>
	
	</div>
	<div class="submenu">
	{if $categoryHeader->checkSpecialiPerMacroCategoria(46) > 0}
			<h3 class="category"><a title="Offerte speciali" style="width:105px; background-color:#cc0000" href="{$link->getPageLink('prices-drop.php', true)}?category=46">Offerte speciali</a></h3>
		{else}
		{/if}
		<h3 class="category">Prezzo</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/price-0-50">Fino a 50 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/price-51-200">Da 51 a 200 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/price-201-500">Da 201 a 500 &euro;</a></li>
<li><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/price-501-4000">Oltre 500 &euro;</a></li>

		
		</ul>
	<div class="submenu">
	<br />
	<strong>Altri prodotti VoIP...</strong>
		<h3 class="category"><a href="{$link->getCategoryLink('115',$link->getCatRewrite(115))}#/tipo-voip">Telefoni VoIP</a></h3>
		<h3 class="category"><a style="margin-top:-10px" href="{$link->getCategoryLink('27',$link->getCatRewrite(27))}#/connessione-voip-voip_analogica">Gateway GSM VoIP</a></h3>
		
		<h3 class="category"><a style="margin-top:-20px" href="{$link->getCategoryLink('103',$link->getCatRewrite(103))}">Speciale Skype</a></h3>
		
		</div>
		
		
		
		
		</div>
		<div class="submenu" style="width:205px">
		<h3 class="category">Produttori</h3>
				<ul class="int-category">
				<li style="width:130px"><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/produttore-yeastar">Yeastar</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/produttore-patton">Patton</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/produttore-audiocodes">Audiocodes</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}#/produttore-cisco">Cisco</a></li>
	</ul>
	<br />
						
		<strong><a style="font-size:10px; font-weight:normal" href="{$link->getCategoryLink('46',$link->getCatRewrite(46))}">Tutti i gateway VoIP...</a></strong>
		
		
		</div>
				<div class="cat-separatore">
								<div style="margin-left:25px;">
				
					<h3 class="category"><a href="{$link->getPageLink('new-products.php')}?cat=46" style="width:110px">Nuovi prodotti</a></h3>
		
		<h3 class="category"><a href="{$link->getPageLink('best-sales.php')}?cat=46" style="width:110px; margin-top:-10px">I pi&ugrave; venduti</a></h3>
	
					
				
				
		<h3 class="category"><a href='https://www.ezdirect.it/guide/category/5-guide-e-consigli' style='width:110px'>Guide</a></h3>
		<ul class="int-categ-marg">
		<li><a href="{$link->getCMSLink(45, $link->getCMSRewrite(45))}" title="Gateway VoIP guida">Gateway VoIP guida</a></li>
		
		
		
		</ul>
		

		</div>
		</div>
						
		
		<div style="clear:both"></div>
	</div>
	
	</li>
	
	
	
	
	
	
	<li class="cat" style="width:65px; {if $link->getCategoryForHeader() == 'informatica'}background-color:#da6600; height:36px{else}{/if}"><a href="{$link->getCategoryLink('210',$link->getCatRewrite(210))}">Informatica <br />Rete</a>
	<div class="menu-category">
	<div class="submenu">
		<h3 class="category"><a href="{$link->getCategoryLink('118',$link->getCatRewrite(118))}">Hot-Spot Wi-fi</a></h3>
		
		</div>
		<div class="submenu" style="width:230px">
		<h3 class="category"><a style="width:180px" href="{$link->getCategoryLink('210',$link->getCatRewrite(210))}">Informatica - Rete</a></h3>
		
		</div>

	<div class="submenu">
		<h3 class="category"><a href="{$link->getCategoryLink('51',$link->getCatRewrite(51))}">Fax server</a></h3>
		
		</div>		
		</div>
	
	
	</li>
	<li class="cat" style="width:60px; {if $link->getCategoryForHeader() == 'citofoni'}background-color:#da6600; height:36px{else}{/if}"><a href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}">Citofoni e citotelefoni</a>
	<div class="menu-category">
	<div class="submenu">
		<h3 class="category">Tipo</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}#/tipo-cit_an">Analogico</a></li>
		<li><a href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}#/tipo-voip">VoIP</a></li>
		</ul>
			<div class="submenu">
			<h3 class="category">Tasti</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}#/tastiera-1_tasto-1_tasto_e_tastiera">1 tasto</a></li>
		<li><a href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}#/tastiera-2_tasti">2 tasti</a></li>
		<li><a href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}#/tastiera-4_tasti">4 tasti</a></li>
		<li><a href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}#/tastiera-6_tasti-6_tasti_e_tastiera">6 tasti</a></li>
		
		</ul>
		
		
		</div>
			

	</div>
	<div class="submenu">
	<h3 class="category">Telecamera</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}#/telecamera-si">S&igrave;</a></li>
		<li><a href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}#/telecamera-opzione">Opzione</a></li>
	
		</ul>
		<div class="submenu">
	<h3 class="category">Rel&egrave;</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}#/rele_apriporta-1">1</a></li>
		<li><a href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}#/rele_apriporta-1_1_opz-2">2</a></li>
	
		</ul>
	</div>
	</div>

	<div class="submenu">
	{if $categoryHeader->checkSpecialiPerMacroCategoria(208) > 0}
			<h3 class="category"><a title="Offerte speciali" style="width:105px; background-color:#cc0000" href="{$link->getPageLink('prices-drop.php', true)}?category=208">Offerte speciali</a></h3>
		{else}
		{/if}
		<h3 class="category">Prezzo</h3>
		<ul class="int-category">
		<li><a href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}#/price-0-100">Fino a 100 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}#/price-101-200">Da 101 a 200 &euro;</a></li>
		<li><a href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}#/price-201-500">Da 201 a 500 &euro;</a></li>
<li><a href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}#/price-501-4000">Oltre 500 &euro;</a></li>

		
		</ul>
	
		
		
		</div>
		<div class="submenu">
		<h3 class="category">Produttori</h3>
		<ul class="int-category">
		<li style="width:130px"><a href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}#/produttore-2n_italia">2N Italia</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}#/produttore-fanvil">Fanvil</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}#/produttore-tema">Tema</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}#/produttore-ezdirect">Ezdirect</a></li>
		<li style="width:130px"><a href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}#/produttore-snom">Snom</a></li>
		</ul>
	
	<br />
		<strong><a style="font-size:10px; font-weight:normal" href="{$link->getCategoryLink('208',$link->getCatRewrite(208))}">Tutti i citofoni...</a></strong>
		
		
		</div>
				<div class="cat-separatore" style="margin-left:15px;">
					<div style="margin-left:25px;">
				
					<h3 class="category"><a href="{$link->getPageLink('new-products.php')}?cat=208" style="width:110px">Nuovi prodotti</a></h3>
		
		<h3 class="category"><a href="{$link->getPageLink('best-sales.php')}?cat=208" style="width:110px; margin-top:-10px">I pi&ugrave; venduti</a></h3>
	
					
	
		

		</div>
		</div>
						
		
		<div style="clear:both"></div>
	</div>
	
	
	
	
	
	
	
	
	
	
	
	
	</li>
	<li class="cat" style="width:50px; padding-right:11px; {if $link->getCategoryForHeader() == 'altro'}background-color:#da6600; height:36px{else}{/if}"><a href="#">Altre categorie</a>
	
		<div class="menu-category" style="height:330px">
	<div class="submenu" style="width:210px">
		
<h3 class="category"><a href="{$link->getCategoryLink('247',$link->getCatRewrite(247))}">CTI Client <br /> Softphone VoIP</a></h3>
		
		<h3 class="category"><a href="{$link->getCategoryLink('103',$link->getCatRewrite(103))}">Speciale Skype</a></h3>
		
<h3 class="category"><a href="{$link->getCategoryLink('26',$link->getCatRewrite(26))}">Controllo costi</a></h3>
		
		<h3 class="category"><a href="{$link->getCategoryLink('146',$link->getCatRewrite(146))}">Usato e ricondizionato</a></h3>
	
		<h3 class="category"><a href="{$link->getCategoryLink('119',$link->getCatRewrite(119))}">Supporto e assistenza</a></h3>
		</div>
	
	<div class="submenu" style="width:210px">
	<h3 class="category"><a href="{$link->getCategoryLink('243',$link->getCatRewrite(243))}">Domotica e sicurezza</a></h3>
	<h3 class="category"><a href="{$link->getCategoryLink('25',$link->getCatRewrite(25))}">Registratori</a></h3>
	<h3 class="category"><a href="{$link->getCategoryLink('156',$link->getCatRewrite(156))}">Ricetrasmittenti</a></h3>
	<h3 class="category"><a href="{$link->getCategoryLink('250',$link->getCatRewrite(250))}">Centralino virtuale</a></h3>
	<h3 class="category"><a href="{$link->getCategoryLink('254',$link->getCatRewrite(254))}">Music server Hi-Fi</a></h3>
		<br />
	
	</div>
	<div class="submenu">
	&nbsp;
		
		</div>
		<div class="submenu" style="width:165px">
		&nbsp;
		
		
		</div>
				<div class="cat-separatore">
				<div style="margin-left:30px;">
		<h3 class="category">Assistenza</h3>
		<ul class="int-category">
		<li><a href="{$link->getCMSLink(55, $link->getCMSRewrite(55))}" title="Assistenza tecnica">Assistenza tecnica</a></li>
		<li><a href="{$link->getCMSLink(35, $link->getCMSRewrite(35))}" title="Richiesta documenti contabili">Richiesta documenti</a></li>
		<li><a href="{$link->getCMSLink(33, $link->getCMSRewrite(33))}" title="Assistenza ordini">Assistenza ordini</a></li>
<li><a href="{$link->getCMSLink(36, $link->getCMSRewrite(36))}" title="RMA">RMA</a></li>
</ul>
<h3 class="category"><a href="{$link->getPageLink('sitemap.php')}" style="width:110px; margin-top:-10px">Mappa del sito</a></h3>
</div>
		
	
		
		</div>
						
		
		<div style="clear:both"></div>
	</div>
	

	
	
	</li>
	</ul>
	
	
	</div>
	
	
	{if $smarty.server.PHP_SELF|basename == 'index.php'}
	
		<!-- <hr class="separation" /> -->
		
		
	
	<div id="home-title"><h1 class='home'>Centralini telefonici, cuffie, auricolari e molto altro</h1> <p class='home-title'> sul sito web specializzato in telefonia professionale per aziende a P.A.</p></div>
	{else}
		
			<hr class="separation" />
		
		
	{/if}
	
	{if !$content_only}
		{if isset($restricted_country_mode) && $restricted_country_mode}
		<div id="restricted-country">
			<p>{l s='You cannot place a new order from your country.'} <span class="bold">{$geolocation_country}</span></p>
		</div>
		{/if}
		<div id="page">

			<!-- Header -->
	
			{if $page_name == 'order-opc' || isset($smarty.get.letter)}
				{else}
			<div id="columns">
				<!-- Left -->
				
					<div id="left_column" class="column">
						{$HOOK_LEFT_COLUMN}
					</div>
				
				<!-- Center -->
				<div id="center_column" style="margin-left:-15px">
				{/if}
				{/if}
