{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}



	<script type="text/javascript">
		var txtFree = "{l s='Free!'}";
	</script>


{if !$virtual_cart && $giftAllowed && $cart->gift == 1}
<script type="text/javascript">
{literal}
// <![CDATA[
    $('document').ready( function(){
		if ($('input#gift').is(':checked'))
			$('p#gift_div').show();
    });
//]]>
{/literal}
</script>
{/if}



{if $virtual_cart}
	<input id="input_virtual_carrier" class="hidden" type="hidden" name="id_carrier" value="0" />
{else}
	<h3 class="carrier_title">{l s='Choose your delivery method'}</h3>

	
	{if $trasporto_gratuito == 1}
	
		{l s='For this cart you have free shipping'}
		<input type="hidden" name="id_carrier" value="{$selected_carrier}" checked="checked" id="id_carrier{$selected_carrier}" />
	
	{else}
	
		<div id="HOOK_BEFORECARRIER">{if isset($carriers)}{$HOOK_BEFORECARRIER}{/if}</div>
		{if isset($isVirtualCart) && $isVirtualCart}
		<p class="warning">{l s='No carrier needed for this order'}</p>
		{else}
		{if $recyclablePackAllowed}
		<p class="checkbox">
			<input type="checkbox" name="recyclable" id="recyclable" value="1" {if $recyclable == 1}checked="checked"{/if} />
			<label for="recyclable">{l s='I agree to receive my order in recycled packaging'}.</label>
		</p>
		{/if}
		<p class="warning" id="noCarrierWarning" {if isset($carriers) && $carriers && count($carriers)}style="display:none;"{/if}>{l s='There are no carriers available that deliver to this address.'}</p>
		<script type="text/javascript">
		updateCarrierSelectionAndGift();
		</script>
	
		<table id="carrierTable" class="std" {if !isset($carriers) || !$carriers || !count($carriers)}style="display:none;"{/if}>
			
			<tbody>
			{if isset($carriers)}
				{foreach from=$carriers item=carrier name=myLoop}
					<tr class="{if $smarty.foreach.myLoop.first}first_item{elseif $smarty.foreach.myLoop.last}last_item{/if} {if $smarty.foreach.myLoop.index % 2}alternate_item{else}item{/if}">
						<td class="carrier_action radio">
							{if $carrier.name != 'Ritiro in sede Ezdirect'}
							<input type="radio" name="id_carrier" value="{$carrier.id_carrier|intval}" id="id_carrier{$carrier.id_carrier|intval}"  {if $opc}onclick="updateCarrierSelectionAndGift();"{/if} {if !($carrier.is_module AND $opc AND !$isLogged)}{if $carrier.id_carrier == $checked}checked="checked"{/if}{else}{/if} />
							{else}
							{/if}
						</td>
						<td class="carrier_name">
							<label for="id_carrier{$carrier.id_carrier|intval}">
								{if $carrier.img}<img src="{$carrier.img|escape:'htmlall':'UTF-8'}" alt="{$carrier.name|escape:'htmlall':'UTF-8'}" />{else}{$carrier.name|escape:'htmlall':'UTF-8'}{/if}
								
							</label>
						</td>
						
						<td class="carrier_price">
						{if $carrier.name != 'Ritiro in sede Ezdirect'}
								
							{if $carrier.price || ($transport_cost != '' && $transport_cost > 0)}
								<span class="price">
									{if $carrier.id_carrier == $edited_carrier}
										{if $transport_cost != ''}
											
											{if $priceDisplay == 1}{convertPrice price=$transport_cost}{else}{convertPrice price=$transport_cost}{/if}
										{else}
											
											{if $priceDisplay == 1}{convertPrice price=$carrier.price_tax_exc}{else}{convertPrice price=$carrier.price}{/if}
										
										{/if}
									{else}
										{if $priceDisplay == 1}{convertPrice price=$carrier.price_tax_exc}{else}{convertPrice price=$carrier.price}{/if}
									{/if}
								</span>
								{if $use_taxes}{if $priceDisplay == 1} {l s='(tax excl.)'}{else} {l s='(tax incl.)'}{/if}{/if}
							{else}
								{l s='Free!'}
							{/if}
						{else}
							<em>Gentile Cliente, in periodo di emergenza Covid-19 non si effettua il ritiro presso la nostra sede, ma soltanto la spedizione via corriere espresso.</em>
						{/if}
						</td>
					</tr>
				{/foreach}
				<tr id="HOOK_EXTRACARRIER">{$HOOK_EXTRACARRIER}</tr>
			{/if}
			</tbody>
		</table>
	{/if}
	
	
	
	<div style="display: none;" id="extra_carrier"></div>

		{if $giftAllowed}
		<h3 class="gift_title">{l s='Gift'}</h3>
		<p class="checkbox">
			<input type="checkbox" name="gift" id="gift" value="1" {if $cart->gift == 1}checked="checked"{/if} onclick="$('#gift_div').toggle('slow');" />
			<label for="gift">{l s='I would like the order to be gift-wrapped.'}</label>
			<br />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			{if $gift_wrapping_price > 0}
				({l s='Additional cost of'}
				<span class="price" id="gift-price">
					{if $priceDisplay == 1}{convertPrice price=$total_wrapping_tax_exc_cost}{else}{convertPrice price=$total_wrapping_cost}{/if}
				</span>
				{if $use_taxes}{if $priceDisplay == 1} {l s='(tax excl.)'}{else} {l s='(tax incl.)'}{/if}{/if})
			{/if}
		</p>
		<p id="gift_div" class="textarea">
			<label for="gift_message">{l s='If you wish, you can add a note to the gift:'}</label>
			<textarea rows="5" cols="35" id="gift_message" name="gift_message">{$cart->gift_message|escape:'htmlall':'UTF-8'}</textarea>
		</p>
		{/if}
	{/if}
{/if}



<h3 class="payment_title">{l s='Choose your payment method'}</h3>

	<p class="bubble" style="right: 135px; position:absolute; top:30px; margin-top:3px; display:none" id="payment_notice">
	{l s='You must choose a payment method before continue'}
	</p>

	<table id="paymentTable" class="std">
		
		<tbody>
			<tr class="first_item">
			<td style="border-left: 1px solid #d4d4d4 "><input type="radio" onclick="updateCarrierSelectionAndGift(); $('#payment_notice').hide();" name="payment" {if $payment_method_cart == "Bonifico" || $payment_method_cart == "Bonifico anticipato" || $payment_method_cart == "Bonifico Anticipato"}checked="checked"{/if} id="bonifico_p" value="modules/bankwire/payment.php" /></td>
			<td><label for="bonifico_p">{l s='Bankwire'} - <span style='font-size:10px'>IBAN: IT58T0503469948000000001454 </span></label></td>
			<td style="border-right: 1px solid #d4d4d4 "></td>
			</tr>
			{if $blacklist == 1}
			{else}
			<tr class="alternate_item">
			<td style="border-left: 1px solid #d4d4d4 "><input type="radio" onclick="updateCarrierSelectionAndGift(); $('#payment_notice').hide();" name="payment" {if $payment_method_cart == "Paypal"}checked="checked"{/if} id="paypal_p" value="modules/paypal/payment/submit.php" /></td>
			<td><label for="paypal_p">{l s='Paypal'}</label></td>
			<td style="border-right: 1px solid #d4d4d4 "></td>
			</tr>
			{if ($is_company == 0 && $is_registered == 1 && $payment_method_cart != "Contanti alla consegna") || $cookie->risk == 1}
			{else}
			<tr class="first_item">
			<td style="border-left: 1px solid #d4d4d4 "><input type="radio" onclick="updateCarrierSelectionAndGift(); $('#payment_notice').hide();" name="payment" id="cheque_p" {if $payment_method_cart == "Contanti alla consegna"}checked="checked"{/if} value="modules/cheque/payment.php" /></td>
			<td><label for="contanti_p">Contanti alla consegna (non si accettano assegni) </label></td>
			<td style="border-right: 1px solid #d4d4d4 "></td>
			</tr>
			{/if}
			
			{if ($is_company == 0 && $is_registered == 1 && $payment_method_cart != "Contrassegno") || $cookie->risk == 1}
			{else}
			<!-- <tr class="first_item">
			<td style="border-left: 1px solid #d4d4d4 "><input type="radio" onclick="updateCarrierSelectionAndGift(); $('#payment_notice').hide();" name="payment" id="contrassegno_p" {if $payment_method_cart == "Contrassegno"}checked="checked"{/if} value="modules/cashondeliverywithfee/validation.php" /></td>
			<td><label for="contrassegno_p">Contanti alla consegna (non si accettano assegni) <span id="spese_contrassegno" rel="{$id_group}" style="color:#da0f00; font-weight:bold"></span></label></td>
			<td style="border-right: 1px solid #d4d4d4 "></td>
			</tr> -->
			{/if}
			<tr class="alternate_item">
			<td style="border-left: 1px solid #d4d4d4 "><input type="radio" onclick="updateCarrierSelectionAndGift(); $('#payment_notice').hide();" name="payment" id="gestpay_p" {if $payment_method_cart == "Carta"}checked="checked"{/if} value="modules/gestpay/payment.php" /></td>
			<td><label for="gestpay_p">{l s='Credit card'}</label></td>
			<td style="border-right: 1px solid #d4d4d4 "></td>
			</tr>
			{if ($payment_method_default != "Bonifico" && $payment_method_default != 'Carta Amazon' && $payment_method_default != "Bonifico Bancario" && $payment_method_cart != "Bonifico anticipato" && $payment_method_cart != "Bonifico Anticipato" && $payment_method_default != "GestPay" && $payment_method_default != "nessuno" && $payment_method_default != "Carta" && $payment_method_default != "Paypal" && $payment_method_default != "PayPal" && $payment_method_default != "paypal" && $payment_method_default != "Contrassegno" && $payment_method_default != "" && $payment_method_default != $payment_method_cart) && $cookie->risk == 0}
			<tr class="alternate_item">
			<td style="border-left: 1px solid #d4d4d4 "><input type="radio" onclick="updateCarrierSelectionAndGift(); $('#payment_notice').hide();" name="payment" checked="checked" id="other_payment_p_2" value="modules/other_payment/payment.php" /></td>
			<td><label for="other_payment_p_2">{$payment_method_default}</label></td>
			<td style="border-right: 1px solid #d4d4d4 "></td>
			</tr>
			{/if}
			
			{if ($payment_method_cart != "Bonifico" && $payment_method_cart != "Bonifico anticipato" && $payment_method_cart != "Bonifico Anticipato"  && $payment_method_cart != "Bonifico Bancario" && $payment_method_cart != "GestPay"   && $payment_method_cart != "nessuno" && $payment_method_cart != "Carta" && $payment_method_cart != "Paypal" && $payment_method_default != "PayPal" && $payment_method_default != "paypal" && $payment_method_cart != "Contrassegno" && $payment_method_cart != "") && $cookie->risk == 0}
			<tr class="alternate_item">
			<td style="border-left: 1px solid #d4d4d4 "><input type="radio" onclick="updateCarrierSelectionAndGift(); $('#payment_notice').hide();" name="payment" checked="checked" id="other_payment_p" value="modules/other_payment/payment.php" /></td>
			<td><label for="other_payment_p">{$payment_method_cart}</label></td>
			<td style="border-right: 1px solid #d4d4d4 "></td>
			</tr>
			{/if}
			
			<tr><td style="border-left: 1px solid #d4d4d4 "></td><td><a href="{$base_dir_ssl}modules/paypal/express/submit.php">Paga con PayPal Express Checkout, pagamento veloce </a></td><td  style="border-right: 1px solid #d4d4d4 "></td>
			{/if}
		</tbody>
	</table>
			
			
								{if $id_group == 3  || $id_group == 10 || $id_group == 12}
{else}
<!-- <br /><a href="#voucher" id="codici_coupon">{l s='Got coupon codes? Click here!'}</a> -->


	
{/if}
				
