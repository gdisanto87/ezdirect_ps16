
<br />
<div id="cart_summary_mini_div_button">
</div>
<br />
<div id="cart_summary_mini_div">
	<table id="cart_summary_mini" style="padding:5px" class="std">

		{foreach from=$products item=product name=productLoop}
			{* Display the product line *}
			{include file="$tpl_dir./shopping-cart-product-line-2.tpl"}
			{* Then the customized datas ones*}
			{if isset($customizedDatas.$productId.$productAttributeId)}
				{foreach from=$customizedDatas.$productId.$productAttributeId key='id_customization' item='customization'}
					<tr id="product_{$product.id_product}_{$product.id_product_attribute}_{$id_customization}" class="alternate_item cart_item">
						<td colspan="5">
							{foreach from=$customization.datas key='type' item='datas'}
								{if $type == $CUSTOMIZE_FILE}
									<div class="customizationUploaded">
										<ul class="customizationUploaded">
											{foreach from=$datas item='picture'}<li><img src="{$pic_dir}{$picture.value}_small" alt="" class="customizationUploaded" /></li>{/foreach}
										</ul>
									</div>
								{elseif $type == $CUSTOMIZE_TEXTFIELD}
									<ul class="typedText">
										{foreach from=$datas item='textField' name='typedText'}<li>{if $textField.name}{$textField.name}{else}{l s='Text #'}{$smarty.foreach.typedText.index+1}{/if}{l s=':'} {$textField.value}</li>{/foreach}
									</ul>
								{/if}
							{/foreach}
						</td>
						<td class="cart_quantity">
							<div style="float:right">
								<a rel="nofollow" id="{$product.id_product}_{$product.id_product_attribute}_{$id_customization}" href="{$link->getPageLink('cart.php', true)}?delete&amp;id_product={$product.id_product|intval}&amp;ipa={$product.id_product_attribute|intval}&amp;id_customization={$id_customization}&amp;token={$token_cart}"><img src="{$img_dir}icon/icon_delete.gif" alt="{l s='Delete'}" title="{l s='Delete this customization'}" width="18" height="18" class="icon" /></a>
							</div>
							<div id="cart_quantity_button" style="float:left">
							<a rel="nofollow" id="cart_quantity_up_{$product.id_product}_{$product.id_product_attribute}_{$id_customization}" href="{$link->getPageLink('cart.php', true)}?add&amp;id_product={$product.id_product|intval}&amp;ipa={$product.id_product_attribute|intval}&amp;id_customization={$id_customization}&amp;token={$token_cart}" title="{l s='Add'}"><img src="{$img_dir}icon/quantity_up.gif" alt="{l s='Add'}" width="14" height="9" /></a><br />
							{if $product.minimal_quantity < ($customization.quantity -$quantityDisplayed) OR $product.minimal_quantity <= 1}
							<a rel="nofollow" id="cart_quantity_down_{$product.id_product}_{$product.id_product_attribute}_{$id_customization}" href="{$link->getPageLink('cart.php', true)}?add&amp;id_product={$product.id_product|intval}&amp;ipa={$product.id_product_attribute|intval}&amp;id_customization={$id_customization}&amp;op=down&amp;token={$token_cart}" title="{l s='Subtract'}">
								<img src="{$img_dir}icon/quantity_down.gif" alt="{l s='Subtract'}" width="14" height="9" />
							</a>
							{else}
							<a style="opacity: 0.3;" id="cart_quantity_down_{$product.id_product}_{$product.id_product_attribute}_{$id_customization}" href="#" title="{l s='Subtract'}">
								<img src="{$img_dir}icon/quantity_down.gif" alt="{l s='Subtract'}" width="14" height="9" />
							</a>
							{/if}
							</div>
							<input type="hidden" value="{$customization.quantity}" name="quantity_{$product.id_product}_{$product.id_product_attribute}_{$id_customization}_hidden"/>
							<input size="2" type="text" value="{$customization.quantity}" class="cart_quantity_input" name="quantity_{$product.id_product}_{$product.id_product_attribute}_{$id_customization}"/>
						</td>
						<td class="cart_total"></td>
					</tr>
					{assign var='quantityDisplayed' value=$quantityDisplayed+$customization.quantity}
				{/foreach}
				{* If it exists also some uncustomized products *}
				{if $product.quantity-$quantityDisplayed > 0}{include file="$tpl_dir./shopping-cart-product-line.tpl"}{/if}
			{/if}
		{/foreach}
		</tbody>
	{if sizeof($discounts)}
		<tbody>
		{foreach from=$discounts item=discount name=discountLoop}
		<!--	<tr class="cart_discount {if $smarty.foreach.discountLoop.last}last_item{elseif $smarty.foreach.discountLoop.first}first_item{else}item{/if}" id="cart_discount_{$discount.id_discount}" style="font-weight:bold">
				<td></td>
				<td class="cart_discount_name" colspan="1">
				{if $prodotti_con_sconto}
					<strong>Sezione coupon</strong><br /><br />
					{assign var='i' value=0}
					
					 <strong>{l s='Discount applied to: '}</strong>
					{foreach from=$prodotti_con_sconto item=prodotto_con_sconto}
						{$i = $i+1}
						{$prodotto_con_sconto}
						{if $i == ($prodotti_con_sconto|@count)}{else} - {/if} 
						
					{/foreach} 
				{else}
				{/if}
				<span style="float:right; margin-top:-15px">Codice coupon: </span>
				</td>
				
				<td style="text-align:center" colspan="1">{$discount.name}</td>
				<td></td>
				<td></td>
				<td style="text-align:center"><a href="{if $opc}{$link->getPageLink('order-opc.php', true)}{else}{$link->getPageLink('order.php', true)}{/if}?deleteDiscount={$discount.id_discount}" title="{l s='Delete'}"><img src="{$img_dir}icon/icon_delete.gif" alt="{l s='Delete'}" class="icon" width="18" height="18" /></a></td>
				<td></td>
				<td class="cart_discount_price"><strong>Sconto</strong>: <span class="price-discount">
					
					{if $discount.value_real > 0}
						{if !$priceDisplay}{displayPrice price=$discount.value_real*-1}{else}{displayPrice price=$discount.value_tax_exc*-1}{/if}
					{/if}
				</span></td>
			</tr> -->
		{/foreach}
		</tbody>
	{/if}
	</table>
	<br /><hr class="footer_hr" /><br />	
	
	<table id="cart_foot_2">
	<tfoot style="font-size:16px">
				
			{if $use_taxes}
				{if $priceDisplay}
					<tr class="cart_total_price">
						<td style="font-size:14px" colspan="7">{l s='Total products'}{if $display_tax_label} {l s='(tax excl.)'}{/if}{l s=':'}</td>
						<td class="price" style="text-align:right; font-size:16px;" id="total_product">{displayPrice price=$total_products}</td>
					</tr>
				{else}
					<tr class="cart_total_price">
						<td style="font-size:14px" colspan="7">{l s='Total products'}{if $display_tax_label} {l s='(tax incl.)'}{/if}{l s=':'}</td>
						<td class="price" style="text-align:right; font-size:16px;" id="total_product">{displayPrice price=$total_products_wt}</td>
					</tr>
				{/if}
			{else}
				<tr class="cart_total_price">
					<td style="font-size:14px" colspan="7">{l s='Total products:'}</td>
					<td class="price" style="text-align:right; font-size:16px;" id="total_product">{displayPrice price=$total_products}</td>
				</tr>
			{/if}
			<tr class="cart_total_voucher" {if $total_discounts == 0}style="display: none;"{/if}>
				<td style="font-size:14px" colspan="7">
				{if $use_taxes}
					{if $priceDisplay}
						{l s='Total vouchers'}{if $display_tax_label} {$discount_name} <a href="{if $opc}{$link->getPageLink('order-opc.php', true)}{else}{$link->getPageLink('order.php', true)}{/if}?deleteDiscount={$id_discount}" title="{l s='Delete'}" style="text-decoration:none; color:#0000FF"><img src="{$img_dir}icon/icon_delete.gif" alt="{l s='Delete'}" class="icon" width="18" height="18" /></a> {l s='(tax excl.)'}{/if}{l s=':'}
					{else}
						{l s='Total vouchers'}{if $display_tax_label} {l s='(tax incl.)'}{/if}{l s=':'}
					{/if}
				{else}
					{l s='Total vouchers:'}
				{/if}
				</td>
				<td class="price-discount" id="total_discount" style="text-align:right;" >
				{if $use_taxes}
					{if $priceDisplay}
						{displayPrice price=$total_discounts_tax_exc}
					{else}
						{displayPrice price=$total_discounts}
					{/if}
				{else}
					{displayPrice price=$total_discounts_tax_exc}
				{/if}
				</td>
			</tr>
			<tr class="cart_total_voucher" {if $total_wrapping == 0}style="display: none;"{/if}>
				<td style="font-size:14px" colspan="7">
				{if $use_taxes}
					{if $priceDisplay}
						{l s='Total gift-wrapping'}{if $display_tax_label} {l s='(tax excl.)'}{/if}{l s=':'}
					{else}
						{l s='Total gift-wrapping'}{if $display_tax_label} {l s='(tax incl.)'}{/if}{l s=':'}
					{/if}
				{else}
					{l s='Total gift-wrapping:'}
				{/if}
				</td>
				<td class="price-discount" id="total_wrapping" style="text-align:right;">
				{if $use_taxes}
					{if $priceDisplay}
						{displayPrice price=$total_wrapping_tax_exc}
					{else}
						{displayPrice price=$total_wrapping}
					{/if}
				{else}
					{displayPrice price=$total_wrapping_tax_exc}
				{/if}
				</td>
			</tr>
			{if $use_taxes}
				{if $priceDisplay}
					<tr class="cart_total_delivery">
						<td style="font-size:14px" colspan="7">{l s='Total shipping'}{if $display_tax_label} {l s='(tax excl.)'}{/if}{l s=':'}</td>
						<td id="total_shipping" style="text-align:right;">{displayPrice price=$shippingCostTaxExc}</td>
					</tr>
				{else}
					<tr class="cart_total_delivery">
						<td style="font-size:14px" colspan="7">{l s='Total shipping'}{if $display_tax_label} {l s='(tax incl.)'}{/if}{l s=':'}</td>
						<td id="total_shipping"style="text-align:right;" >{displayPrice price=$shippingCost}</td>
					</tr>
				{/if}
			{else}
				<tr class="cart_total_delivery">
					<td style="font-size:14px" colspan="7">{l s='Total shipping:'}</td>
					<td  id="total_shipping" style="text-align:right;">{displayPrice price=$shippingCostTaxExc}</td>
				</tr>
			{/if}

			{if $use_taxes}
			<tr class="cart_total_price">
				<td style="font-size:14px" colspan="7">
					{if $display_tax_label}
						{l s='Total (tax excl.):'}
					{else}
						{l s='Subtotal:'}
					{/if}
				</td>
				<td class="price" style="text-align:right; font-size:16px;" id="total_price_without_tax">{displayPrice price=$total_price_without_tax}</td>
			</tr>
			<tr class="cart_total_tax">
				<td style="font-size:14px" colspan="7">
					{if $display_tax_label}
						{l s='Total tax:'}
					{else}
						{l s='Estimated Sales Tax:'}
					{/if}
				</td>
				<td style="text-align:right;" id="total_tax">{displayPrice price=$total_tax}</td>
			</tr>
			<tr>
			<td colspan="8"><br /><hr class="footer_hr" /><br /></td></tr>
			<tr class="cart_total_price">
				<td style="font-size:14px" colspan="7">
					{if $display_tax_label}
						{l s='Total (tax incl.):'}
					{else}
						{l s='Total:'}
					{/if}
					<br />(IVA dove applicabile)
				</td>
				<td class="price" style="text-align:right;font-size:16px;font-size:20px; font-weight:bold" id="total_price">{displayPrice price=$total_price}</td>
			</tr>
			{else}
			<tr class="cart_total_price">
				<td style="font-size:14px" colspan="7">{l s='Total:'}</td>
				<td class="price" style="text-align:right;font-size:20px; font-weight:bold" id="total_price">{displayPrice price=$total_price_without_tax}</td>
			</tr>
			{/if}
			<tr class="cart_free_shipping" {if $free_ship <= 0 || $isVirtualCart} style="display: none;" {/if}>
					<td style="font-size:14px" colspan="7" style="white-space: normal;">{l s='Remaining amount to be added to your cart in order to obtain free shipping:'}</td>
					<td id="free_shipping" style="text-align:right;" class="price">{displayPrice price=$free_ship}</td>
				</tr>
		</tfoot>
	</table>

	
			
</div>

<p>
			&nbsp;
			{l s='Would you like to add a comment?'}<br /> <input type="text" value="{if isset($oldMessage)}{$oldMessage}{else}{l s='Insert text here'}{/if}" id="message" name="message" onfocus="if {literal} (this.value == {/literal} '{l s='Insert text here'}') {literal} { this.value = '' } else { } {/literal}" />
			</p>
			
			{if $conditions AND $cms_id}<br />
	
			<p class="bubble" style="right: 70px; position:absolute; bottom:198px; margin-top:3px; display:none" id="cgv_notice">
			{l s='You must accept our terms before continue'}
			</p>
	
			<p>
		    <input type="checkbox" onclick="$('#cgv_notice').hide();" name="cgv" id="cgv" value="1" {if $checkedTOS}checked="checked"{/if} />
				<label for="cgv"><strong>{l s='I agree to the terms of service and adhere to them unconditionally.'}</strong></label> <a href="cms.php?id_cms=3" target="_blank">{l s='(read)'}</a><br />
			</p>
	
			{/if}

			
				{if $preventivo == 1 && $date_check == 1}
		
			<script type="text/javascript">
		
					alert("Gentile cliente, ti inviamo questo messaggio, in quanto sei registrato sul nostro sito web www.ezdirect.it, ed hai nel carrello (inseriti da te o da ns consulenti a fronte di una tua richiesta di preventivo), uno o piu prodotti, i quali hanno subito una variazione di prezzo. Suggeriamo di verificare, accedendo al tuo account. Per ulteriore supporto contatta il nostro team allo 0585821163, oppure apri un ticket dalla tua area personale.");

			</script>
			
			{else}
			{if $cfcheck == 0 || $cfcheck == 2}
			<p style="">
				{if !$isLogged}
				<input type="submit" class='button_large' id='payment_button' onclick='alert("{l s='You must login to continue'} {l s='If you have problems please contact our customer service'}."); return false;' style="display:block; padding-top:3px " value="{l s='Continue'}" />
				{else if $errors|@count > 0}
				<input type="submit" class='button_large' id='payment_button' onclick='alert("{l s='There are some problems in your cart. Please check the errors dialog'}. {l s='If you have problems please contact our customer service'}"); return false;' style="display:block; padding-top:3px; position:absolute; right:5px" value="{l s='Continue'}" />
				{else}
				<input type="submit" class='button_large' id='payment_button' onclick='alert("{l s='There are some details missing in your account: if you are a end user customer you must specify your tax code in your account details, and if you are a company you must specify both tax code and vat number. Please go to your account and update your profile details. MISSING DETAILS: '}{if $check_tx == 1}{l s='Tax code'}{else}{/if}{if $check_vat == 1}{l s='VAT number'}{else}{/if}{if $check_phone == 1}{l s='Phone number'}{else}{/if}. {l s='If your details seem correct, please update this page. If your problem does not solve, please contact our customer service (800 529767)'}"); return false;' style="display:block;  padding-top:3px ;margin-left:5px;" value="{l s='Continue'}" />
				{/if}
			{else}
			<p>
					{if $errors|@count > 0}
					{else}
		
			
		
				<br />
					<input type="submit" class='button_large' id='payment_button' {if !isset($smarty.get.step)}onclick="$('#payment_button_shopping').click();"{else}onclick="return controlla_termini();"{/if} style="{if !isset($smarty.get.step)}{else}width:100%;{/if} float:none; height: 40px;background-color: #f60 !important;border: 0px !important;color: #fff !important;" value="{l s='Continua e finalizza'}" />
			
			
			
			{/if}
		
		{/if}
			{/if}
			</p>
			