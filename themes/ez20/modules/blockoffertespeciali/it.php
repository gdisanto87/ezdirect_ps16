<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockcompatibility}prestashop>blockcompatibility_1ba88f200df5d53c735464f8419d0929'] = 'Blocco compatibilità cuffie';
$_MODULE['<{blockcompatibility}prestashop>blockcompatibility_f415a1b05f4cd3f805d469a40a4204f0'] = 'Blocco con i loghi di compatibilità delle cuffie';
$_MODULE['<{blockcompatibility}prestashop>blockcompatibility_65adccaafaddaa7bcb7875a373295fca'] = 'Compatibilità cuffie';
$_MODULE['<{blockcompatibility}prestashop>blockcompatibility_53ef5650f7e4dbe2be6f659e9aa240ff'] = 'Verifica della compatibilità tra le cuffie e il tuo dispositivo';
