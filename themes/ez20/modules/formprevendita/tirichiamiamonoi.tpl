{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='Contact' mod='formprevendita'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}
<script type='text/javascript'>
$(document).ready(function(){
	$('select#id_country').change(function(){
		updateState();
		updateNeedIDNumber();
		updateZipCode();
	});
	
	$('select#id_country2').change(function(){
		updateState2();
		updateNeedIDNumber();
		updateZipCode();
	});
	
	updateState2();
	updateState();
	
	updateNeedIDNumber();
	updateZipCode();
	
	if ($('select#id_country_invoice').length != 0)
	{
		$('select#id_country_invoice').change(function(){
			updateState('invoice');
			updateNeedIDNumber('invoice');
			updateZipCode();
		});
		if ($('select#id_country_invoice:visible').length != 0)
		{
			updateState('invoice');
			updateNeedIDNumber('invoice');
			updateZipCode('invoice');
		}
	}
});

function updateState(suffix)
{
	$('select#id_state'+(suffix !== undefined ? '_'+suffix : '')+' option:not(:first-child)').remove();
	var states = countries[$('select#id_country'+(suffix !== undefined ? '_'+suffix : '')).val()];
	if(typeof(states) != 'undefined')
	{
		$(states).each(function (key, item){
			$('select#id_state'+(suffix !== undefined ? '_'+suffix : '')).append('<option value="'+item.id+'"'+ (idSelectedCountry == item.id ? ' selected="selected"' : '') + '>'+item.name+'</option>');
		});
		
		$('p.id_state'+(suffix !== undefined ? '_'+suffix : '')+':hidden').slideDown('slow');
	}
	else
		$('p.id_state'+(suffix !== undefined ? '_'+suffix : '')).slideUp('fast');
		
}	
	
function updateState2(suffix)
{
	$('select#id_state2'+(suffix !== undefined ? '_'+suffix : '')+' option:not(:first-child)').remove();
	var states = countries[$('select#id_country2'+(suffix !== undefined ? '_'+suffix : '')).val()];
	if(typeof(states) != 'undefined')
	{
		$(states).each(function (key, item){
			$('select#id_state2'+(suffix !== undefined ? '_'+suffix : '')).append('<option value="'+item.id+'"'+ (idSelectedCountry == item.id ? ' selected="selected"' : '') + '>'+item.name+'</option>');
		});
		
		$('p.id_state'+(suffix !== undefined ? '_'+suffix : '')+':hidden').slideDown('slow');
	}
	else
		$('p.id_state'+(suffix !== undefined ? '_'+suffix : '')).slideUp('fast');
		
}


function updateNeedIDNumber(suffix)
{
	var idCountry = parseInt($('select#id_country'+(suffix !== undefined ? '_'+suffix : '')).val());

	if ($.inArray(idCountry, countriesNeedIDNumber) >= 0)
		$('.dni'+(suffix !== undefined ? '_'+suffix : '')).slideDown('slow');
	else
		$('.dni'+(suffix !== undefined ? '_'+suffix : '')).slideUp('fast');
}

function updateZipCode(suffix)
{
	var idCountry = parseInt($('select#id_country'+(suffix !== undefined ? '_'+suffix : '')).val());
	
	var idCountry = parseInt($('select#id_country2'+(suffix !== undefined ? '_'+suffix : '')).val());
	
	if (countriesNeedZipCode[idCountry] != 0)
		$('.postcode'+(suffix !== undefined ? '_'+suffix : '')).slideDown('slow');
	else
		$('.postcode'+(suffix !== undefined ? '_'+suffix : '')).slideUp('fast');
}
</script>


<h1>{l s='Customer Service - Call me back' mod='formprevendita'}</h1>


{if !isset($smarty.get.step)}

	{if isset($smarty.get.category)}
	{assign var='category' value=$smarty.get.category}
	{else if isset($smarty.post.category)}
	{assign var='category' value=$smarty.post.category}
	{else}
	{assign var='category' value=''}
	{/if}
	
		{include file="$tpl_dir./errors.tpl"}
		{include file="$tpl_dir./errors.tpl"}
			<style type="text/css">
	
		#div-preventivo
		{
			width:907px;
			height:620px;
			background-image:url('img/new_callmeback2.jpg');
			position:relative;
		}
		
		#div-preventivo:before, #div-preventivo:after
		{
		  z-index: -1;
		  position: absolute;
		  content: "";
		  bottom: 15px;
		  left: 10px;
		  width: 50%;
		  top: 80%;
		  max-width:907px;
		  background: #777;
		  -webkit-box-shadow: 0 15px 10px #777;
		  -moz-box-shadow: 0 15px 10px #777;
		  box-shadow: 0 15px 10px #777;
		  -webkit-transform: rotate(-3deg);
		  -moz-transform: rotate(-3deg);
		  -o-transform: rotate(-3deg);
		  -ms-transform: rotate(-3deg);
		  transform: rotate(-3deg);
		}
		#div-preventivo:after
		{
		  -webkit-transform: rotate(3deg);
		  -moz-transform: rotate(3deg);
		  -o-transform: rotate(3deg);
		  -ms-transform: rotate(3deg);
		  transform: rotate(3deg);
		  right: 10px;
		  left: auto;
		}
		
		#form-preventivo {
			position:absolute;
			left:600px;
			top:90px;
			width:270px;
			height:265px;
			z-index:4;
			color:#ffffff;
			font-family:arial,sans-serif;
			font-size:12px;
		}
		
	
		input.input-stile-preventivo {
			color: #000000;
			font-size: 14px;
			height: 24px;
			border:0px;
			margin-left:10px;
			padding-left:5px;
			background-color:#ffffff;
			z-index:9;
			width:185px;
			position:relative;
		}
		
		h2.titolo-preventivo-1, h2.titolo-preventivo-2 
		{
			color:#eb5d1b;
			font-size:20px;
			font-family:Arial,sans-serif;
			font-weight:bold;
			position:absolute;
			top:431px;
			left:45px;
		}
		
		div.box-preventivo-1, div.box-preventivo-2 {
			font-family:Arial,sans-serif;
			font-size:12px;
			color:#000000;
			position:absolute;
			left:10px;
			top:465px;
		
		}
		
		h2.titolo-preventivo-2 {
			
			left:277px;
		}
		
		div.box-preventivo-2 {
			width:230px;
			left:242px;
		}
		
		input.bottone-preventivo {
			background-image:url('img/richiamami.png');
			height:32px;
			width:176px;
			z-index:9;
			margin-top:7px;
			margin-bottom:11px;
			margin-left:70px;
			border:0px;
			position:relative;
			padding:0px;
			background-color: Transparent;
            background-repeat:no-repeat;
            border: none;
            cursor:pointer;
            overflow: hidden;
			cursor:pointer;
		}
		
		</style>
			<div id="div-preventivo">
				<div id="form-preventivo">
					Se sei cliente gi&agrave; registrato, fai login con le credenziali del tuo account (email e password)<br />
					<p style='text-align:center'><a href="{$link->getPageLink('password.php')}" style="color:#ffffff" target="_blank">Hai dimenticato la password? Clicca qui</a></p>
				
					<form action="{$link->getPageLink('authentication.php', true)}" method="post" id="login_form" class="std">
					<table><tr><td style="border:0px">Email</td><td style="border:0px"><input type="text" id="email" name="email" value="{if isset($smarty.post.email)}{$smarty.post.email|escape:'htmlall':'UTF-8'|stripslashes}{/if}" class="input-stile-preventivo" /></td></tr>
					<tr><td style="border:0px">Password</td><td style="border:0px"><input type="password" id="passwd" name="passwd" value="{if isset($smarty.post.passwd)}{$smarty.post.passwd|escape:'htmlall':'UTF-8'|stripslashes}{/if}" class="input-stile-preventivo" /></td></tr></table>
					<input type="hidden" class="hidden" name="back" value="modules/formprevendita/tirichiamiamonoi.php" />
					<input type="hidden" class="hidden" name="category" value="{$smarty.get.category}" />
					<input type="submit" class="bottone-preventivo" value=""  id="SubmitLogin" name="SubmitLogin" />
					</form>
					<br /><br /><br />
					
					Se non sei registrato, clicca sul pulsante sotto
					<br />
					<form action="{$link->getPageLink('modules/formprevendita/tirichiamiamonoi.php', true)}?step=2" method="post" class="std">
					
					
					<input type="hidden" class="hidden" name="category" value="{$smarty.get.category}" />
					<input type="submit" class="bottone-preventivo" value="" />
					</form>
					
				</div>
				
				<h2 class="titolo-preventivo-1">Ti richiamiamo noi</h2>
				<div class="box-preventivo-1">
				Gentile cliente, con questo modulo puoi chiedere di essere ricontattato da un nostro esperto, per ricevere supporto alla scelta del prodotto e trovare una soluzione alle tue esigenze, in fase di acquisto. Non &egrave; utilizzabile per chiedere assistenza tecnica n&eacute; amministrativa. Vedi sotto link utili per altre necessit&agrave;:<br /><br />
				Per ricevere assistenza tecnica post vendita <a href="https://www.ezdirect.it/guide/39-contatti">clicca qui</a><br />
				Per assistenza amministrativa e contabilit&agrave; <a href="https://www.ezdirect.it/guide/39-contatti">clicca qui</a><br />
				Per richiedere rientro di prodotti <a href="https://www.ezdirect.it/guide/36-rma-rientro-merce-per-siparazione-e-reso-assistenza-tecnica-">clicca qui</a><br />
				Per informazioni sul tuo ordine in corso <a href="https://www.ezdirect.it/guide/39-contatti">clicca qui</a><br />
				Per richiedere un preventivo <a href="https://www.ezdirect.it/preventivi-quotazioni-miglior-prezzo-cuffie-centralini">clicca qui</a><br />
				</div>
				
			</div>	
		<br />
	
		
		
		
		
		
		
		
		
		
		
		
		
{else if (isset($smarty.get.step) && $smarty.get.step == 2)}

	
<script type="text/javascript">
// <![CDATA[
idSelectedCountry = {if isset($smarty.post.id_state)}{$smarty.post.id_state|intval}{else}false{/if};
countries = new Array();
countriesNeedIDNumber = new Array();
countriesNeedZipCode = new Array();
{if isset($countries)}
	{foreach from=$countries item='country'}
		{if isset($country.states) && $country.contains_states}
			countries[{$country.id_country|intval}] = new Array();
			{foreach from=$country.states item='state' name='states'}
				countries[{$country.id_country|intval}].push({ldelim}'id' : '{$state.id_state}', 'name' : '{$state.name|escape:'htmlall':'UTF-8'}'{rdelim});
			{/foreach}
		{/if}
		{if $country.need_identification_number}
			countriesNeedIDNumber.push({$country.id_country|intval});
		{/if}
		{if isset($country.need_zip_code)}
			countriesNeedZipCode[{$country.id_country|intval}] = {$country.need_zip_code};
		{/if}
	{/foreach}
{/if}
$(function(){ldelim}
	$('.id_state option[value={if isset($smarty.post.id_state)}{$smarty.post.id_state}{else}{if isset($address)}{$address->id_state|escape:'htmlall':'UTF-8'}{else}{$provincia}{/if}{/if}]').attr('selected', 'selected');
{rdelim});
//]]>
{if $vat_management}
	{literal}
	$(document).ready(function() {
		$('#company').blur(function(){
			vat_number();
		});
		vat_number();
		function vat_number()
		{
			if ($('#company').val() != '')
				$('#vat_number').show();
			else
				$('#vat_number').hide();
		}
	});
	{/literal}
{/if}
</script>
	{include file="$tpl_dir./errors.tpl"}
	{assign var='stateExist' value=false}
	
		{if !$inviato}
		
			{if $statusp == 'closed'}
			
			{l s='You cannot answer to this ticket because it has been closed by our staff' mod='formprevendita'}. 
			<a href="{$base_dir_ssl}modules/formprevendita/tirichiamiamonoi.php">{l s='Click here to open another ticket for the same subject' mod='formprevendita'}.</a>
			
				{else}
				<form method="post" class="std" enctype="multipart/form-data">
					<fieldset>
						<p class="select">
							<label for="id_contact"><strong>{l s='Subject Heading' mod='formprevendita'}</strong></label>
						
							&nbsp;&nbsp;&nbsp;{l s='Customer service - Call me back' mod='formprevendita'}
							
						</p>
					
						<input type="hidden" id="id_thread" name="id_thread" value="{$id_thread}" />
						<input type="hidden" id="tipo_richiesta" name="tipo_richiesta" value="tirichiamiamonoi" />
						<p class="text">
							<label for="firstname"><strong>{l s='Firstname' mod='formprevendita'}*</strong></label>
								<input type="text" id="firstname" style="width:400px" name="firstname" value="{if !empty($smarty.post.firstname)}{$smarty.post.firstname|escape:'htmlall':'UTF-8'|stripslashes}{else}{$firstname}{/if}" />
							
						</p>
						<p class="text">
							<label for="lastname"><strong>{l s='Lastname' mod='formprevendita'}*</strong></label>
								<input type="text" id="lastname" style="width:400px" name="lastname" value="{if !empty($smarty.post.lastname)}{$smarty.post.lastname|escape:'htmlall':'UTF-8'|stripslashes}{else}{$lastname}{/if}" />
							
						</p>
							<p class="text">
							<label for="company"><strong>{l s='Company' mod='formprevendita'} {if $required_piva == 1}*{else}{/if}</strong></label>
								<input type="text" id="company" style="width:400px" name="company" value="{if !empty($smarty.post.company)}{$smarty.post.company|escape:'htmlall':'UTF-8'|stripslashes}{else}{$company}{/if}" />
							
						</p>
						
						<p class="text">
							<label for="vat_number"><strong>{l s='VAT Number' mod='formprevendita'} {if $required_piva == 1}{else}{/if}</strong></label>
								<input type="text" id="vat_number" style="width:400px" name="vat_number" value="{if !empty($smarty.post.vat_number)}{$smarty.post.vat_number|escape:'htmlall':'UTF-8'|stripslashes}{else}{$vat_number}{/if}" />
							
						</p>
						<p class="text">
							<label for="tax_code"><strong>{l s='Tax code' mod='formprevendita'}</strong></label>
								<input type="text" id="tax_code" style="width:400px" name="tax_code" value="{if !empty($smarty.post.tax_code)}{$smarty.post.tax_code|escape:'htmlall':'UTF-8'|stripslashes}{else}{$tax_code}{/if}" />
							
						</p>
						<p class="text">
							<label for="phone"><strong>{l s='Phone' mod='formprevendita'}*</strong></label>
								<input type="text" id="phone" style="width:400px" name="phone" value="{if !empty($smarty.post.phone)}{$smarty.post.phone|escape:'htmlall':'UTF-8'|stripslashes}{else}{$phone}{/if}" />
							
						</p>
						<p class="text">
							<label for="email"><strong>{l s='E-mail address' mod='formprevendita'}*</strong></label>
								<input type="text" id="email" style="width:400px" name="email" value="{if !empty($smarty.post.email)}{$smarty.post.email|escape:'htmlall':'UTF-8'|stripslashes}{else}{$email}{/if}" />
							
						</p>
							<p class="text" style="display:none">
							<label for="address1"><strong>{l s='Address' mod='formprevendita'}*</strong></label>
								<input type="text" id="address1" style="width:400px" name="address1" value="{if !empty($smarty.post.address1)}{$smarty.post.address1|escape:'htmlall':'UTF-8'|stripslashes}{else}{$address1}{/if}" />
							
						</p>
							<p class="text" style="display:none">
							<label for="postcode"><strong>{l s='Postcode' mod='formprevendita'}*</strong></label>
								<input type="text" id="postcode" style="width:400px" name="postcode" value="{if !empty($smarty.post.postcode)}{$smarty.post.postcode|escape:'htmlall':'UTF-8'|stripslashes}{else}{$postcode}{/if}" />
							
						</p>
							<p class="text" style="display:none">
							<label for="city"><strong>Citt&agrave;*</strong></label>
								<input type="text" id="city" style="width:400px" name="city" value="{if !empty($smarty.post.city)}{$smarty.post.city|escape:'htmlall':'UTF-8'|stripslashes}{else}{$city}{/if}" />
							
						</p>
						<p class="required select" style="display:none">
									<label for="id_country"><strong>{l s='Country' mod='formprevendita'}</strong></label>
									<select name="id_country" id="id_country">
										<option value="">-</option>
										{foreach from=$countries item=v}
										<option value="{$v.id_country}" {if ($paese == $v.id_country)} selected="selected"
										{else}
										{if ($smarty.post.id_country == $v.id_country)} selected="selected" {else}{/if}
										{/if}>{$v.name|escape:'htmlall':'UTF-8'}</option>
										{/foreach}
									</select>
									<sup>*</sup>
								</p>
								{assign var='stateExist' value=true}

								<p class="required id_state select" style="display:none">
									<label for="id_state"><strong>{l s='State' mod='formprevendita'}</strong></label>
									<select name="id_state" id="id_state">
										<option value="">-</option>
									</select>
									<sup>*</sup>
								</p>
							
						
									{if isset($smarty.get.category)}
									{assign var='categoryric' value=$smarty.get.category}
									{else if isset($smarty.post.category) && is_numeric($smarty.post.category)}
									{assign var='categoryric' value=$smarty.post.category}
									{else}
									{assign var='categoryric' value=''}
									{/if}
		
									{if $categoryric != ''} 
									<p class="text">
									<label for="category"><strong>{l s='Requested product' mod='formprevendita'}*</strong></label>
									<input type="hidden" name="category" value="{$categoryric}" />
									&nbsp;&nbsp;&nbsp;
									
									{$prodottorichiesto}</p>
									{else}
									{/if}
					
					
					
					<p class="textarea">
						<label for="message"><strong>{l s='Notes' mod='formprevendita'}*</strong><br />
						{l s='Message' mod='formprevendita'}</label>
						 <textarea id="message" name="message" rows="15" cols="20" style="width:400px;height:220px">{if !empty($smarty.post.message)}{$smarty.post.message|escape:'htmlall':'UTF-8'|stripslashes}{else}{$message|escape:'htmlall':'UTF-8'|stripslashes}{/if}</textarea>
					</p>
										<style>
.g-recaptcha{
   margin: 0px auto !important;
   width: auto !important;
   height: auto !important;
   text-align: -webkit-center;
   text-align: -moz-center;
   text-align: -o-center;
   text-align: -ms-center;
   margin-left: -33px !important;
}

.grecaptcha-badge { 
    visibility: hidden;
}

.rc-anchor-pt {
	margin: 2px 11px 0 0  !important;
padding-right: 2px  !important;
position: relative !important;
text-align: right !important;
width: 86px !important;
	}
</style>
					 <script src="https://www.google.com/recaptcha/api.js" async defer></script>

					<div class="text-xs-center">
					 <div class="g-recaptcha" style="width:30%; margin:0 auto" data-sitekey="6LciANsZAAAAALhFLBBxrvJJICdKqsPVxXHfPh2i"></div></div>
					 
					<p style="text-align:center; display:block; width:80%">
		Ho letto e compreso la Vostra <a href='https://www.ezdirect.it/guide/6-informativa-sulla-privacy' target='_blank'>Privacy Policy</a> e:
		<br /><br />
			
			<input type="radio" name="consenso_1" id="consenso11" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
			<input type="radio" name="consenso_1" id="consenso10" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
			
			<br /><br />
			al trattamento dei dati personali per l’esecuzione dei servizi forniti tramite il Sito da parte di Ezdirect S.r.l., o ad una o più obbligazioni contrattualmente convenute, ai sensi dell’art. 1, comma 1, lettera a), b) e c) della citata informativa (consenso obbligatorio; la mancata prestazione comporterà l’impossibilità di fruire dei servizi offerti, di effettuare l’iscrizione, e di effettuare acquisti tramite il Sito).
			<br /><br />
			
			<input type="radio" name="consenso_2"  id="consenso21" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
			<input type="radio" name="consenso_2"  id="consenso20" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
			
			<br /><br />
			al trattamento dei dati personali per la realizzazione, da parte di Ezdirect S.r.l., di indagini dirette a verificare il grado di soddisfazione sui servizi offerti, ai sensi dell’art. 1, comma 2, lettera a) della citata informativa (consenso facoltativo).

			<br /><br />
			
			<input type="radio" name="consenso_3"  id="consenso31" value="1" /> Presto il consenso &nbsp;&nbsp;&nbsp;
			<input type="radio" name="consenso_3"  id="consenso30" value="0" /> Nego il consenso &nbsp;&nbsp;&nbsp;
			
			<br /><br />
			al trattamento dei dati personali a fini di marketing e/o comunicazione commerciale da parte di Ezdirect S.r.l., connesse alle attività svolte da parte della stessa e/o da parte di soggetti terzi, ai sensi dell’art. 1, comma 2, lettera b), della citata informativa (consenso facoltativo).
			<br /><br />
			

		</p>
					
					<p class="submit">
						<br /><br />
						<input type="submit" name="submitMessage" id="submitMessage" value="{l s='Send' mod='formprevendita'}" class="button_large" onclick="$(this).hide(); fbq('track', 'Contact');" />
					</p>
					</fieldset>
				
				</form>	
			{/if}
	{else}
 <!-- Google Code for Richiesta di preventivo Conversion Page -->
 
  {if $smarty.post.id_thread == ""}
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1058514372;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "8C5eCMypmAQQxMve-AM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1058514372/?value=0&amp;label=8C5eCMypmAQQxMve-AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<script type="text/javascript">
					var _gaq = _gaq || [];
					_gaq.push(['_setAccount', 'UA-19658774-1']);
					// Recommanded value by Google doc and has to before the trackPageView
					_gaq.push(['_setSiteSpeedSampleRate', 5]);

					  _gaq.push(['_addTrans',
						'{$idrichiesta}',		
						'Ezdirect',		
						'0.01',		
						'',			
						'',	
						'{$smarty.post.city}',	
					    '',		
						''		
					  ]);

						
							_gaq.push(['_addItem',
							'{$idrichiesta}',	
							'Ti richiamiamo noi',			
							'Ti richiamiamo noi',	
							'',		
							'0.01',	
							'1'		
							]);
					
					{literal}
					  _gaq.push(['_trackTrans']);	
					{/literal}
				
					{literal}
					(function() {
						var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
						ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
						var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
					})(); {/literal}
					</script>

				{else}
				{/if}

		<p>{l s='Your message has been successfully sent to our team.' mod='formprevendita'}</p>
		<ul class="footer_links">
			<li><a href="{$base_dir}"><img class="icon" width="22" height="22" alt="{l s='Home'}" src="{$img_dir}icon/home.gif"/></a><a href="{$base_dir}">{l s='Home' mod='formprevendita'}</a></li>
		</ul>
		
		{/if}



{/if}	
	
	
	
	
	
	
	
	