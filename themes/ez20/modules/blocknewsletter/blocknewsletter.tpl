{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- Block Newsletter module-->
{if isset($msg) && $msg}
		
		<div style="z-index:2147483647" id="{if $nw_error}error_newsletter{else}success_newsletter{/if}">
		<p class="{if $nw_error}warning_inline{else}success_inline{/if}"><a name="newsletter-success" id="newsletter-success">{$msg}</a></p>
		</div>
		
		<script type="text/javascript">
		{if $nw_error}
		{literal}
		$(document).ready(function () {
			$('#error_newsletter').delay(7000).fadeOut('slow');
			$('#error_newsletter').css('zIndex', '2147483647');
		});
		{/literal}
		{else}
		{literal}
		$(document).ready(function () {
			$('#success_newsletter').delay(7000).fadeOut('slow');
			$('#success_newsletter').css('zIndex', '2147483647');
		});
		{/literal}
		{/if}
		</script>
	{/if}
	
<div id="newsletter_block_left" class="block">
	
	<div class="block_content">
	{if isset($msg) && $msg}
		<p class="{if $nw_error}warning_inline{else}success_inline{/if}"><a name="newsletter-success" id="newsletter-success">{$msg}</a></p>
	{/if}
		<form action="https://www.ezdirect.it/index.php#newsletter-success" method="post">
			<p style='text-align:center'>
			<br />
			<input type="text" name="email" style="height: 25px; margin-left:0px; width:100%; text-align:center; margin-bottom:7px" value="{if isset($value) && $value}{$value}{else}{l s='your e-mail' mod='blocknewsletter'}{/if}" onfocus="javascript:if(this.value=='{l s='your e-mail' mod='blocknewsletter'}')this.value='';" onblur="javascript:if(this.value=='')this.value='{l s='your e-mail' mod='blocknewsletter'}';" /><br />
			<input type="hidden" name="action" value="0" /><input type="checkbox" name="privacy_newsletter" />
			<a href="#" onclick="window.open('{$link->getPageLink('privacy.php', true)}')" style="text-decoration:underline">Acconsento al trattamento dei dati</a><br />
				
				<input type="submit" class="button" style='margin-left:0px; margin-top:8px;border:0px;' value='Mi iscrivo' name="submitNewsletter" /></p>

			<p style='text-align:center'>
		{l s='Free subscription' mod='blocknewsletter'}
		</p>
		</form>
	</div>
</div>

<!-- /Block Newsletter module-->
