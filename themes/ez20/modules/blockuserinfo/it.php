<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockuserinfo}ez20>blockuserinfo_a2e9cd952cda8ba167e62b25a496c6c1'] = 'Blocco info utente ';
$_MODULE['<{blockuserinfo}ez20>blockuserinfo_970a31aa19d205f92ccfd1913ca04dc0'] = 'Aggiungi un blocco che visualizza le informazioni sul cliente';
$_MODULE['<{blockuserinfo}ez20>blockuserinfo_a0623b78a5f2cfe415d9dbbd4428ea40'] = 'Il tuo account';
$_MODULE['<{blockuserinfo}ez20>blockuserinfo_f2fc7490cbdb0db5cd6f1a4e7edb0da1'] = 'Il tuo account';
$_MODULE['<{blockuserinfo}ez20>blockuserinfo_1d80334b6d2eb65a913dcd5eb0aefb6e'] = 'Vedi ultimo preventivo';
$_MODULE['<{blockuserinfo}ez20>blockuserinfo_9aa698f602b1e5694855cee73a683488'] = 'Contatti';
$_MODULE['<{blockuserinfo}ez20>blockuserinfo_eb8f9a9ec38815aa8ed81682de0c6e18'] = 'Preventivi';
$_MODULE['<{blockuserinfo}ez20>blockuserinfo_12641546686fe11aaeb3b3c43a18c1b3'] = 'Il tuo carrello';
$_MODULE['<{blockuserinfo}ez20>blockuserinfo_7fc68677a16caa0f02826182468617e6'] = 'Carrello';
$_MODULE['<{blockuserinfo}ez20>blockuserinfo_1741ec6ed69cf571adac9eeb641c4f87'] = 'Assistenza';
$_MODULE['<{blockuserinfo}ez20>blockuserinfo_d990da2bc5b38dcee62d9d326cb60fbd'] = 'Rivenditori';
$_MODULE['<{blockuserinfo}ez20>blockuserinfo_2377be3c2ad9b435ba277a73f0f1ca76'] = 'Marchi';
