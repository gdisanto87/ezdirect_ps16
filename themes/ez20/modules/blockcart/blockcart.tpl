{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{*************************************************************************************************************************************}
{* IMPORTANT : If you change some data here, you have to report these changes in the ./blockcart-json.js (to let ajaxCart available) *}
{*************************************************************************************************************************************}
{if $ajax_allowed}
<script type="text/javascript">
/* var CUSTOMIZE_TEXTFIELD = '{$CUSTOMIZE_TEXTFIELD}'; */
var customizationIdMessage = '{l s='Customization #' mod='blockcart' js=1}';
var removingLinkText = '{l s='remove this product from my cart' mod='blockcart' js=1}';
</script>
{/if}

<!-- MODULE Block cart -->
<div id="cart_block2" class="block exclusive" style="border:0px;" >

	<div id="cart_block2_in">
		<div id="cart_block2_in_spans">
			<span class="ajax_cart_quantity" {if $cart_qties <= 0}style="display:none;"{/if}><a href="{$link->getPageLink("order.php", true)}">{$cart_qties}</a></span>
		<span class="ajax_cart_product_txt_s" {if $cart_qties <= 1}style="display:none"{/if}><a href="{$link->getPageLink("order.php", true)}">{l s='products' mod='blockcart'}</a></span>
		<span class="ajax_cart_product_txt" {if $cart_qties > 1}style="display:none"{/if}><a href="{$link->getPageLink("order.php", true)}">{l s='prod.' mod='blockcart'}</a></span>
		<span class="ajax_cart_no_product" {if $cart_qties != 0}style="display:none"{/if}><a href="{$link->getPageLink("order.php", true)}">{l s='(empty)' mod='blockcart'}</a></span>
		<br />
		
		<span class="ajax_cart_total" style="font-weight:bold{if $cart_qties <= 0};display:none{/if}"><a href="{$link->getPageLink("order.php", true)}">{if $priceDisplay == 1}{convertPrice price=$product_total}{else}{convertPrice price=$product_total}{/if}</a></span>

		</div>
		
		<div id="cart_icon" onmouseover="document.getElementById('cart_block_modal_fixed').style.display = 'block';">
		
		{* ON MOUSE OUT -> per chiudere finestra carrello quando esco con il mouse dall'icona carrello *}
		 {* <div id="cart_icon" onmouseleave="document.getElementById('cart_block_modal_fixed').style.display = 'none';">  *}

		<span><a style="display: inline-block; height: 100%;" href="{$link->getPageLink("order.php", true)}" class="cart-block-span"><i style="font-size: 25px; color: #ff6600;" class="fas fa-shopping-cart"></i></a></span>
		</div>
		
	</div>
	<div class="block_content" style="position:relative">
	<!-- block summary -->
	<div id="cart_block_summary2" style="position:relative; margin-top:-25x; left:0px; z-index:9999" class="{if isset($colapseExpandStatus) && $colapseExpandStatus eq 'expanded' || !$ajax_allowed || !isset($colapseExpandStatus)}collapsed{else}expanded{/if}">
	
		
	</div>
	<!-- block list of products -->
	</div>

<!-- /MODULE Block cart -->

</div>



<div id="cart_block_modal_fixed" class="block exclusive" >

	<div id="modal_cart_title">
		<table style="width:100%"><tr><td><a style="text-decoration:none;" href="{$link->getPageLink("order.php", true)}"><h3 style="margin-left: 50px;
    color: #545454;
    font-size: 18px;
	font-weight:600;
    text-align: center;
    padding-bottom: 15px;
    text-transform: uppercase;">Il tuo carrello</h3></a></td><td style="text-align:right"><a href="javascript:void(0)" style="cursor:pointer" onclick="document.getElementById('cart_block_modal_fixed').style.display = 'none';" title="Chiudi" ><img src="https://www.ezdirect.it/img/icon-close.jpg" alt="Chiudi" title="Chiudi"></a></td></tr></table>
<br />
	<table class="block_cart_table">
	<!-- block summary -->
	
	
	<!-- block list of products -->
	
	{if $products}
		
		{foreach from=$products item='product' name='myLoop'}
		{assign var='productId' value=$product.id_product}
			{assign var='productAttributeId' value=$product.id_product_attribute}
			
			<tr>
			<td style="border-top:1px solid #e5e5e5">
			<img src='https://www.ezdirect.it/img/p/{$product.id_image}-medium.jpg' height="80" width="80" alt="" title="" />
			</td>
			<td style="border-top:1px solid #e5e5e5">
			<a class="cart_block_product_name" href="{$link->getProductLink($product.id_product, $product.link_rewrite, $product.category)}" title="{$product.name|escape:html:'UTF-8'}" style="color:#0000ff; font-weight:bold">{$product.name|escape:html:'UTF-8'}</a>
			
			
			
				<!-- Customizable datas -->
				{if isset($customizedDatas.$productId.$productAttributeId)}
					<br /><br />
					<ul class="cart_block_customizations" id="customization_{$productId}_{$productAttributeId}">
						{foreach from=$customizedDatas.$productId.$productAttributeId key='id_customization' item='customization' name='customizations'}
							<li name="customization">
								<div class="deleteCustomizableProduct" id="deleteCustomizableProduct_{$id_customization|intval}_{$product.id_product|intval}_{$product.id_product_attribute|intval}"><a class="ajax_cart_block_remove_link" href="{$link->getPageLink('cart.php')}&delete&amp;id_product={$product.id_product|intval}&amp;ipa={$product.id_product_attribute|intval}&amp;id_customization={$id_customization}&amp;token={$static_token}"> </a></div>
								<span class="quantity-formated"><span class="quantity">{$customization.quantity}</span>x</span>{if isset($customization.datas.$CUSTOMIZE_TEXTFIELD.0)}
								{$customization.datas.$CUSTOMIZE_TEXTFIELD.0.value|escape:html:'UTF-8'|replace:"<br />":" "|truncate:28}
								{else}
								{l s='Customization #' mod='blockcart'}{$id_customization|intval}{l s=':' mod='blockcart'}
								{/if}
							</li>
						{/foreach}
					</ul>
					{if !isset($product.attributes_small)}</dd>{/if}
				{/if}
				
			<br /><br />
			<span class="price"><strong>{if $priceDisplay == $smarty.const.PS_TAX_EXC}{displayWtPrice p="`$product.total`"}{else}{displayWtPrice p="`$product.total_wt`"}{/if}</strong></span>
			</td>
			
			<td style="border-top:1px solid #e5e5e5">
				<span class="quantity-formated"><span class="quantity">{$product.cart_quantity}</span></span>
			</td>	
			<td style="border-top:1px solid #e5e5e5">	
				{if $cart_ez == 0}
					<span class="remove_link">{if !isset($customizedDatas.$productId.$productAttributeId)}<a rel="nofollow" class="ajax_cart_block_remove_link" href="{$link->getPageLink('cart.php')}&delete&amp;id_product={$product.id_product}&amp;ipa={$product.id_product_attribute}&amp;token={$static_token}" title="{l s='remove this product from my cart' mod='blockcart'}"><img style="margin-bottom: 2px;" src="https://www.ezdirect.it/themes/ezdirect-new/img/icon/icon_delete.gif" alt="Cancella" title="Cancella" /></a>{/if}</span>
				{else}
					<span class="remove_link" style="visibility:hidden">{if !isset($customizedDatas.$productId.$productAttributeId)}<a rel="nofollow" style="visibility:hidden" class="ajax_cart_block_remove_link" href="{$link->getPageLink('cart.php')}&delete&amp;id_product={$product.id_product}&amp;ipa={$product.id_product_attribute}&amp;token={$static_token}" title="{l s='remove this product from my cart' mod='blockcart'}"></a>{/if}</span>
				{/if}
			</td>	
		</tr>
		{/foreach}
		
	{/if}
	</table>
<br /><hr class="footer_hr" /><br />
	<table class="block_cart_table_2">
		<tr {if $products}style="display:none"{/if}><td colspan="2"><br />{l s='Nessun prodotto' mod='blockcart'}</td></tr>

		{if $discounts|@count > 0}<table id="vouchers">
			<tr>
			{foreach from=$discounts item=discount}
					<td class="name" title="{$discount.description}">{$discount.name|cat:' : '|cat:$discount.description|truncate:18:'...'|escape:'htmlall':'UTF-8'} <a href="{$link->getPageLink("order.php", true)}&deleteDiscount={$discount.id_discount}" title="{l s='Delete'}"><img src="{$img_dir}icon/delete.gif" width="11" height="11" alt="{l s='Delete'}" width="11" height="13" class="icon" /></a></td>
					<td class="price">-{if $discount.value_real != '!'}{if $priceDisplay == 1}{convertPrice price=$discount.value_tax_exc}{else}{convertPrice price=$discount.value_real}{/if}{/if}</td>
					
			</tr>
			{/foreach}
			
		{/if}

		<tr>
		<td><br /><span>{l s='Spedizione' mod='blockcart'}</td>
		</td><td  class="price"><br />{$shipping_cost}</td>

<tr><td>{l s='Imponibile' mod='blockcart'}</td>
<td  class="price">{convertPrice price=$total_wt}</td>
			</tr>
			

			<tr><td>{l s='IVA' mod='blockcart'}</td>
				<td class="price">{$tax_cost}</td>
			
			
	</table>		
	<hr class="footer_hr" /><br />
	<table class="block_cart_table_2">		
		<tr><td><br />{l s='Totale' mod='blockcart'}</td>
			<td class="price" style="font-size:20px" ><br />{$total}</td></tr></table>
	<hr class="footer_hr" /><br />	

		<p id="cart-buttons" style="text-align:center; margin-bottom:100px;">
			<a class="ajax_add_to_cart_button_home" href="{$link->getPageLink("order.php", true)}" id="button_order_cart" title="{l s='Check out' mod='blockcart'}" style="margin: 0 auto;
display: block;
width: 85%;
padding-top: 12px; " >{l s='Vai alla cassa' mod='blockcart'}</a>
<br />
<img src="https://www.ezdirect.it/themes/ez20/img/carte-carrello.jpg" alt="" title="">
		</p>
		<div style="clear:both"></div>
	</div>
	</div>
</div>
</div> <!-- fine mostra carrello -->

<script type="text/javascript">
var element = $('#cart_block_modal_fixed').detach();
$('#menu-alto').append(element);
</script>