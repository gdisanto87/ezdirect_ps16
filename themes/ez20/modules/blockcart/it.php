<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockcart}ez20>blockcart-orig_ed6e9a09a111035684bb23682561e12d'] = 'rimuovi questo prodotto dal carrello';
$_MODULE['<{blockcart}ez20>blockcart-orig_a85eba4c6c699122b2bb1387ea4813ad'] = 'Carrello';
$_MODULE['<{blockcart}ez20>blockcart-orig_86024cad1e83101d97359d7351051156'] = 'prodotti';
$_MODULE['<{blockcart}ez20>blockcart-orig_f5bf48aa40cad7891eb709fcf1fde128'] = 'prodotto';
$_MODULE['<{blockcart}ez20>blockcart-orig_9e65b51e82f2a9b9f72ebe3e083582bb'] = '(vuoto)';
$_MODULE['<{blockcart}ez20>blockcart-orig_853ae90f0351324bd73ea615e6487517'] = ':';
$_MODULE['<{blockcart}ez20>blockcart-orig_09dc02ecbb078868a3a86dded030076d'] = '0 prodotti';
$_MODULE['<{blockcart}ez20>blockcart-orig_ea9cf7e47ff33b2be14e6dd07cbcefc6'] = 'Spedizione';
$_MODULE['<{blockcart}ez20>blockcart-orig_b2f40690858b404ed10e62bdf422c704'] = 'Totale Iva Escl.';
$_MODULE['<{blockcart}ez20>blockcart-orig_ba794350deb07c0c96fe73bd12239059'] = 'Carta';
$_MODULE['<{blockcart}ez20>blockcart-orig_96b0141273eabab320119c467cdcaf17'] = 'Totale Iva Incl.';
$_MODULE['<{blockcart}ez20>blockcart-orig_0d11c2b75cf03522c8d97938490466b2'] = 'Prezzi iva incl.';
$_MODULE['<{blockcart}ez20>blockcart-orig_41202aa6b8cf7ae885644717dab1e8b4'] = 'Prezzi iva escl.';
$_MODULE['<{blockcart}ez20>blockcart-orig_f2a6c498fb90ee345d997f888fce3b18'] = 'Rimuovi';
$_MODULE['<{blockcart}ez20>blockcart-orig_377e99e7404b414341a9621f7fb3f906'] = 'Vai alla cassa';
$_MODULE['<{blockcart}ez20>blockcart-orig_be53a0541a6d36f6ecb879fa2c584b08'] = 'Immagine';
$_MODULE['<{blockcart}ez20>blockcart-orig_8bf8854bebe108183caeb845c7676ae4'] = 'di';
$_MODULE['<{blockcart}ez20>blockcart-orig_10ac3d04253ef7e1ddc73e6091c0cd55'] = 'Avanti';
$_MODULE['<{blockcart}ez20>blockcart-orig_14230d11143a03f4330c6433d5032a9d'] = 'Indietro';
$_MODULE['<{blockcart}ez20>blockcart-orig_c20905e8fdd34a1bf81984e597436134'] = 'Continua lo shopping';
$_MODULE['<{blockcart}ez20>blockcart_20351b3328c35ab617549920f5cb4939'] = 'Personalizzazione n.';
$_MODULE['<{blockcart}ez20>blockcart_ed6e9a09a111035684bb23682561e12d'] = 'rimuovi questo prodotto dal mio carrello';
$_MODULE['<{blockcart}ez20>blockcart_86024cad1e83101d97359d7351051156'] = 'prodotti';
$_MODULE['<{blockcart}ez20>blockcart_9e65b51e82f2a9b9f72ebe3e083582bb'] = '(vuoto)';
$_MODULE['<{blockcart}ez20>blockcart_377e99e7404b414341a9621f7fb3f906'] = 'Pagamento';
