{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14011 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
</div>
<div id="home_banners">
	<script type="text/javascript" src="{$js_dir}unslider.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$(function() {
			$('.banner').unslider({
				speed: 500,               //  The speed to animate each slide (in milliseconds)
				delay: 5000,              //  The delay between slide animations (in milliseconds)
				keys: true,               //  Enable keyboard (left, right) arrow shortcuts
				dots: true,               //  Display dot navigation
			});
		});
	});
	</script>
	{* <div id="imgs_home_left">
		<a href='https://www.ezdirect.it/prodotti-venduti-migliori-sito-web-ezdirect' target='_blank'><img src='{$img_dir}piu-venduti.jpg' alt='I piu venduti' title='I piu venduti' /></a><br />
		<img src='{$img_dir}pronta-consegna.jpg' alt='Pronta consegna' title='Pronta consegna' /><br />
		<a href='https://www.ezdirect.it/offerte-speciali' target='_blank'><img src='{$img_dir}le-offerte.jpg' alt='Le offerte' title='Le offerte' /></a>
	
	</div> *}
	{* <div id="banner-home">
		<div class="banner" style="height:295px">
			<ul style="height:295px">
			
			{foreach from=$banners item='banner' name='banners'}
				<li><a href="{$banner.link}" target="_blank"><img src="{$img_ps_dir}banner-home/{$banner.immagine}" alt="{$banner.descrizione}" title="{$banner.descrizione}" /></a></li>			
			{/foreach}

				
			</ul>
		</div>
	</div> *}


	{* <div id="links-home">
		<ul>
			<li><table><tr><td style='width:50px; text-align:center'><img src='{$img_ps_dir}ez.jpg' alt='Ezdirect' title='Ezdirect' /></td><td style='width:150px; font-size:13px'>Il primo sito di telefonia per aziende in cerca di soluzioni per comunicare</td></tr></table></li>
			<li><table><tr><td style='width:50px; text-align:center'><a href='{$link->getCMSLink2(3, $link->getCMSRewrite(3))}'><img src='{$img_ps_dir}coccarda.jpg' alt='Garanzia Ezdirect' title='Garanzia Ezdirect' /></a></td><td style='width:150px; font-size:13px'><a href='{$link->getCMSLink2(3, $link->getCMSRewrite(3))}'>100% soddisfatto<br /> (garanzia Ezdirect)</a></td></tr></table></li>
			<li><table><tr><td style='width:50px; text-align:center'><a href='https://www.ezdirect.it/preventivi-quotazioni-miglior-prezzo-cuffie-centralini'><img src='{$img_ps_dir}preventivi-piccolo.jpg' alt='Preventivi gratuiti' title='Preventivi gratuiti' /></a></td><td style='width:150px; font-size:13px'><a href='https://www.ezdirect.it/preventivi-quotazioni-miglior-prezzo-cuffie-centralini'>Preventivi gratuiti: richiedi una quotazione al miglior prezzo</a></td></tr></table></li>
			<li><table><tr><td style='width:50px; text-align:center'><a href='{$link->getCMSLink2(65, $link->getCMSRewrite(65))}'><img src='{$img_ps_dir}corporate-piccolo.jpg' alt='Corporate account' title='Corporate account' /></a></td><td style='font-size:13px'><a href='{$link->getCMSLink2(65, $link->getCMSRewrite(65))}'>Corporate Account: Account manager dedicato per grandi aziende e P.A.</a></td></tr></table></li>
		</ul>
	</div> *}
</div>
<div style="clear:both"></div>

{* <div id="home_orange">
	<div id="home_orange_in">
		<img src='{$img_dir}orange_spedizione_gratuita.jpg' alt='Spedizione gratuita' title='Spedizione gratuita' />
		<img src='{$img_dir}orange_consulenza_e_assistenza.jpg' alt='Consulenza e assistenza' title='Consulenza e assistenza' />
		<img src='{$img_dir}orange_spedizione.jpg' alt='Spedizione in 24h' title='Spedizione in 24h' />
		<img src='{$img_dir}orange_migliaia_di_prodotti.jpg' alt='Migliaia di prodotti' title='Migliaia di prodotti' />
	</div>
</div> *}


<div class="center_column" style="width: 100%; ">

	<div class="container" >

		<h4 class='homepage' style="width:100%; margin: 10px auto; height: fit-content;">{l s='Le migliori soluzioni per comunicare' mod='homefeatured'}</h4>

		<div id="categories-home">

			<div class="row" >

				<div class="col-12 col-sm-6 col-lg-4">
				
					<div class='categories-home-in' style="overflow-x:auto;">
						
						<table style='width:100%; height:100%; table-layout:fixed;'>
							<tr>
								<td style="width:250px;">
									<h3><a href="{$link->getCategoryLink(23)|escape:'html':'UTF-8'}">Centralini telefonici</a></h3>
								</td>

							</tr>

							<tr style="display: block;">
								<td style='width:50%; vertical-align:top  !important'>
									
									<ul style="margin-left:18px;">
									<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo_linee-analogica_voip_isdn/tipo_di_derivati_int_collegabili-analogici_voip-analogici_digitali_voip/supporta_linee_voip_sip-si-si_opzionale">VoIP</a></li>
									<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo-centralino_virtuale">Cloud</a></li>
									<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo-centralino_software">Solo Software</a></li>
									<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo-centralino_dect">DECT</a></li>
									<li><a href="https://www.ezdirect.it/centralini-telefonici/#/tipo_di_derivati_int_collegabili-analogici_voip-analogici_digitali_voip-analogici-int_analogici_digita">Analogici</a></li>
									<li><a href="https://www.ezdirect.it/centralini-telefonici/">Tutti</a></li>
									
									
									</ul>
								</td>
								
								<td style='width:50%; vertical-align:middle !important'>
								<div class='categories-home-in-img'>
									<img src='{$img_ps_dir}centralini-home.png'  alt='Centralini telefonici' title='Centralini telefonici' />
								</div>
								</td>
							</tr>
						</table>
					</div>
				</div>

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
				
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="{$link->getCategoryLink(115)|escape:'html':'UTF-8'}">Telefoni</a></h3>
								</td>

							</tr>
							
							<tr style="display: block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							<li><a href="https://www.ezdirect.it/telefoni-fissi/">Fissi</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-cordless/#/tipo-cordless_tipo_voip">Cordless IP</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-fissi/#/ottimizzato_per-microsoft_teams">Teams</a></li>
							<li><a href="https://www.ezdirect.it/cerca-ricerca-cercare-prodotti-telefonia?orderby=position&orderway=desc&nnn=on&cod=on&sd=on&search_query=certificato%20zoom&submit_search=Cerca">Zoom</a></li>
							<li><a href="https://www.ezdirect.it/cerca-ricerca-cercare-prodotti-telefonia?orderby=position&orderway=desc&nnn=on&cod=on&sd=on&search_query=videotelefoni&submit_search=Cerca">Videotelefoni</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-cordless/#/tipo-cordless_tipo_wifi">WiFi</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-fissi/">Tutti i fissi</a></li>
							<li><a href="https://www.ezdirect.it/telefoni-cordless/">Tutti i cordless</a></li>
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<img src='{$img_ps_dir}telefoni-home.png' alt='Ezdirect' title='Ezdirect' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>

				
			

			

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
			
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="{$link->getCategoryLink(106)|escape:'html':'UTF-8'}">Cuffie e auricolari</a></h3>
								</td>

							</tr>
						<tr style="display:block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							<li><a href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-con_filo">Con filo</a></li>
							<li><a href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-senza_filo">Senza filo</a></li>
							<li><a href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-ibrida">Ibride</a></li>
							<li><a href="https://www.ezdirect.it/cuffie-telefoniche/#/tipo-con_filo/utilizzabile_con-pc_usb_bluetooth-telefono_fisso_pc_usb">Multiuso</a></li>
							<li><a href="https://www.ezdirect.it/cuffie-telefoniche/#/ottimizzato_per-microsoft_teams">Teams</a></li>
							<li><a href="https://www.ezdirect.it/cuffie-telefoniche/">Tutti</a></li>
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<img src='{$img_ps_dir}cuffie-home.png' alt='Cuffie e auricolari' title='Cuffie e auricolari' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
				
						<table style='width:100%; height:100%; table-layout:fixed;'>
						

							<tr>
								<td style="width:250px;">
									<h3><a href="{$link->getCategoryLink(75)|escape:'html':'UTF-8'}">Audioconferenza</a></h3>
								</td>

							</tr>
						<tr style="display:block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/tipo-audc_usb">USB</a></li>
							<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/tipo-audioconferenze_voip">IP</a></li>
							<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/tipo-multiuso">Multiuso</a></li>
							<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/tipo-gsm">GSM</a></li>
							<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/#/tipo-sistema_pro">Sistema PRO</a></li>
							<li><a href="https://www.ezdirect.it/audioconferenza-teleconferenza/">Tutti</a></li>
							
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<img src='{$img_ps_dir}audioconferenza.png' alt='Audioconferenza' title='Audioconferenza' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
				
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="{$link->getCategoryLink(76)|escape:'html':'UTF-8'}">Videoconferenza</a></h3>
								</td>

							</tr>
							<tr style="display: block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-videocamera_usb">Videocamera USB</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-sistema">Sistema</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-servizio_cloud">Cloud</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-clickshare">Clickshare</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-accessori_clickshare">Accessori clickshare</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/#/tipo_videoconferenza-room_manager">Room Management</a></li>
							<li><a href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/">Tutti</a></li>
							
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<img src='{$img_ps_dir}videoconferenza.png' alt='Videoconferenza' title='Videoconferenza' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>
				

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in' style="width:100%;">
			
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="{$link->getCategoryLink(208)|escape:'html':'UTF-8'}">Citofoni IP</a></h3>
								</td>

							</tr>
						<tr style="display: block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/tipo-voipcitofoni">Videocitofoni IP</a></li>
							<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/tipo-controllo_accessi">Controllo Accessi</a></li>
							<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/tipo-cit_an">Interfacce</a></li>
							<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/#/tipo-cit_an">Analogici</a></li>
							<li><a href="https://www.ezdirect.it/citofoni-videocitofoni/">Tutti</a></li>
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important; padding-top: 4px;'>
							<div class='categories-home-in-img'>
								<img src='{$img_ps_dir}2nhome.png' alt='Citofoni IP' title='Citofoni IP' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>

			

			

				

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
				
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="{$link->getCategoryLink(46)|escape:'html':'UTF-8'}">Gateway VoIP</a></h3>
								</td>

							</tr>
						<tr style="display: block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							
							<li><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/#/tipo-gateway_isdn">ISDN</a></li>
							<li><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/#/porte_fxs-2-4-1-8-24-16-32-48-fino_a_288">Analogico (FXS)</a></li>
							<li><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/#/porte_fxo-4-2-8-1-16">Analogico (FXO)</a></li>
							<li><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/#/tipo-gateway_ibrido">Ibrido</a></li>
							<li><a href="https://www.ezdirect.it/voip-gateway-ata-fxo-fxs/">Tutti</a></li>
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<img src='{$img_ps_dir}gateway-voip.png' alt='Gateway VoIP' title='Gateway VoIP' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
				
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="{$link->getCategoryLink(27)|escape:'html':'UTF-8'}">Gateway GSM</a></h3>
								</td>

							</tr>
						<tr style="display: block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul style="margin-left:18px;">
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-gateway_gsm">GSM</a></li>
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-gateway_umts-router_umts">WCDMA</a></li>
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-gateway_lte_4g-scheda_lte-ripetitore_lte_gsm-router_lte_4g">LTE</a></li>
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/connessione-analogica">Analogico</a></li>
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/connessione-isdn">ISDN</a></li>
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/#/tipo-ripetitore_gsm_umts_lte-ripetitore_umts_gsm-ripetitore_lte_gsm-ripetitore_gsm_umts_dcs">Ripetitore di segnale</a></li>
							<li><a href="https://www.ezdirect.it/gsm-umts-gateway-router/">Tutti</a></li>
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<img src='{$img_ps_dir}gateway-gsm.png' alt='Gateway GSM' title='Gateway GSM' />
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>

				<div class="col-12 col-sm-6 col-lg-4">
					<div class='categories-home-in'>
				
						<table style='width:100%; height:100%; table-layout:fixed;'>

							<tr>
								<td style="width:250px;">
									<h3><a href="https://www.ezdirect.it/offerte-speciali">Offerte Speciali</a></h3>
								</td>

							</tr>
						<tr style="display: block;">
							<td style='width:50%; vertical-align:top !important'>
							
							<ul id="categories-home-in-menu">
							<li><a href="https://www.ezdirect.it/offerte-speciali">Approfitta subito delle nostre promozioni!</a></li>
							
							
							</ul>
							</td>
							
							<td style='width:50%; vertical-align:middle !important'>
							<div class='categories-home-in-img'>
								<a href="https://www.ezdirect.it/offerte-speciali">
									<img style="width:100%;" src='{$img_ps_dir}offerte-speciali.png' alt='Offerte speciali' title='Offerte speciali' />
								</a>
							</div>
							</td>
						</tr>
						</table>
					</div>
				</div>
			</div>
			
			
			
			
			
			
			
			
			
			{* <div style="clear:both"></div> *}
		
		
		</div>
	</div>
	
</div>

<!--I NOSTRI MARCHI-->
<div class="marchi">
	<h2 style="color:#000099; text-align:center; padding: 30px 0; font-size:25px; text-transform: uppercase; letter-spacing: 4px; font-weight: 600; ">I nostri migliori marchi</h2>

	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators" style="display:none;">
		<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		<li data-target="#carousel-example-generic" data-slide-to="1"></li>
		<li data-target="#carousel-example-generic" data-slide-to="2"></li>
	</ol>

	<!-- Wrapper for slides -->
	<div class="carousel-inner" role="listbox">
		<div class="item active">
			<div class="container">
				<div class="row col-sm-12" style="display: flex; align-items: center;">
					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}2n.png" alt="Banner">
						<p id="nome-marchio">2n</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}yeastar.png" alt="Banner">
						<p id="nome-marchio">Yeastar</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}3cx.png" alt="Banner">
						<p id="nome-marchio">3cx</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}jabra.png" alt="Banner">
						<p id="nome-marchio">jabra</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}openvox.jpeg" alt="Banner">
						<p id="nome-marchio">Open Vox</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}ezdirect.png" alt="Banner">
						<p id="nome-marchio">EzDirect</p></a>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
		<div class="container">
				<div class="row col-sm-12" style="display: flex; align-items: center;">
					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}patton.png" alt="Banner">
						<p id="nome-marchio">Patton</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}ascom.png" alt="Banner">
						<p id="nome-marchio">Ascom</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}cisco.png" alt="Banner">
						<p id="nome-marchio">Cisco</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}fanvil.png" alt="Banner">
						<p id="nome-marchio">Fanvil</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}gigaset.png" alt="Banner">
						<p id="nome-marchio">Gigaset</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}snom.png" alt="Banner">
						<p id="nome-marchio">Snom</p></a>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="container">
				<div class="row col-sm-12" style="display: flex; align-items: center;">
					<div class="col-sm-2" id="col-marchio-nome">
						<a href="https://shop.samsung.com/it/?cid=it_pd_ppc_google_samsung_brand_shop_text_text_brand-kw&gclid=Cj0KCQjwkIGKBhCxARIsAINMioIwvbdiBw2CoJ4ZV0Eeue3wrPQ7whtIY0e1eu_6hUUVgHUabp9oLB0aAnyBEALw_wcB&gclsrc=aw.ds" id="marchio-link"><img src="{$img_ps_dir}samsung.png" alt="Banner">
						<p id="nome-marchio">Samsung</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}panasonic.png" alt="Banner">
						<p id="nome-marchio">Panasonic</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}dahua.png" alt="Banner">
						<p id="nome-marchio">Dahua</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}yealink.png" alt="Banner">
						<p id="nome-marchio">Yealink</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}hiboost.png" alt="Banner">
						<p id="nome-marchio">Hiboost</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}grandstream.png" alt="Banner">
						<p id="nome-marchio">Grandstream</p></a>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="container">
				<div class="row col-sm-12" style="display: flex; align-items: center;">
					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}dinstar.png" alt="Banner">
						<p id="nome-marchio">Dinstar</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}lucky-tone.jpeg" alt="Banner">
						<p id="nome-marchio">Lucky Tone</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}avaya.png" alt="Banner">
						<p id="nome-marchio">Avaya</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}alcatel.png" alt="Banner">
						<p id="nome-marchio">Alcatel</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}epos.png" alt="Banner">
						<p id="nome-marchio">Epos</p></a>
					</div>

					<div class="col-sm-2" id="col-marchio-nome">
						<a href="#" id="marchio-link"><img src="{$img_ps_dir}poly.png" alt="Banner">
						<p id="nome-marchio">Poly</p></a>
					</div>
				</div>
			</div>
		</div>


	

	<!-- Controls -->
	<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev" style="color: #f4f4f4; background-image: none; width: 85px; background-color: white; opacity: 1; z-index: 0;">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next" style="color: #f4f4f4; background-image: none; width: 115px; background-color: white; opacity: 1; z-index: 0;">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
	</div>
</div>

</div>

<!--fine I NOSTRI MARCHI-->
{* BOX: PREVENTIVO gratuito, DIVENTA RIVENDITORE, etc *}
		{* <div class="container" style="background-color: #f4f4f4; width: 100%;">
			<div class="row" style="display: flex; justify-content: center; padding:10px; flex-wrap:wrap;">
				<a href="#" class="col-md-5 col-lg-4" id="title-btn-section" style="text-decoration:none;">

					<div id="image-boxes">
						<img src='{$img_ps_dir}preventivogratuito.png' alt='PreventivoGratuito' title='PreventivoGratuito' />
					</div>

					<div >
						<h5 >Preventivo gratuito</h5>
						<p>Entro un'ora. Garanzia 100% soddisfatto</p>
					</div>

					<div id="chevron-right-btn">
						<i class="fas fa-chevron-right" ></i>
					</div>
					
					
				</a>

				<a href="#" class="col-md-5 col-lg-4" id="title-btn-section" style="text-decoration:none;">

					<div id="image-boxes">
						<img src='{$img_ps_dir}ti-richiamiamo.png' alt='PreventivoGratuito' title='PreventivoGratuito' />
					</div>

					<div>
						<h5>Ti richiamiamo noi</h5>
						<p>I nostri esperti a tua disposizione</p>
					</div>
					
					<div id="chevron-right-btn">
						<i class="fas fa-chevron-right"></i>
					</div>
					
				</a>
			

			
				<a href="#" class="col-md-5 col-lg-4" id="title-btn-section" style="text-decoration:none;">

					<div id="image-boxes">
						<img src='{$img_ps_dir}diventarivenditore.png' alt='PreventivoGratuito' title='PreventivoGratuito' />
					</div>

					<div>
						<h5>Diventa rivenditore</h5>
						<p>Registrati per diventare nostro rivenditore</p>
					</div>
				 	
					<div id="chevron-right-btn">
						<i class="fas fa-chevron-right"></i>
					</div>
					
				</a>

				<a href="#" class="col-md-5 col-lg-4" id="title-btn-section" style="text-decoration:none;">

					<div id="image-boxes">
						<img src='{$img_ps_dir}acquistirete.png' alt='PreventivoGratuito' title='PreventivoGratuito' />
					</div>

					<div>
						<h5>Acquisti in rete</h5>
						<p>Per la pubblica amministrazione</p>
					</div>
					
					<div id="chevron-right-btn">
						<i class="fas fa-chevron-right"></i>
					</div>
					
				</a>
			</div>
		</div> *}

		

{* END BOX: PREVENTIVO gratuito, DIVENTA RIVENDITORE, etc *}
<div class="center_column">

<h4 class='homepage'>{l s='In evidenza' mod='homefeatured'}</h4>

	<!-- MODULE Home Featured Products -->
	<div id="home_featured_module">
		<div id='coin-slider'>
			{if isset($products) AND $products}
				{assign var='liHeight' value=342}
				{assign var='nbItemsPerLine' value=4}
				{assign var='nbLi' value=$products|@count}
				{math equation="nbLi/nbItemsPerLine" nbLi=$nbLi nbItemsPerLine=$nbItemsPerLine assign=nbLines}
				{math equation="nbLines*liHeight" nbLines=$nbLines|ceil liHeight=$liHeight assign=ulHeight}
				{assign var='i' value='0'}
				{foreach from=$products item=product name=homeFeaturedProducts}
				{if $i==0 || $i==1 || $i==2 || $i==3 || $i==4 || $i==5 || $i==6 || $i==7 || $i==8 || $i==9 || $i==10 || $i==11  || $i==12 || $i==13 || $i==14 || $i==15 || $i==16 || $i==17 || $i==18 || $i==19 || $i==20 || $i==21 || $i==22 || $i==23}
			{if $i==3 || $i==7 || $i==11  || $i==15  || $i==19  || $i== 23}
			<div class='home-low-level' style='margin-right:-1px {if $i > 15}; height:280px{/if}'>
			{else}
			<div class='home-low-level' style=' {if $i > 15}; height:280px{/if}'>
			{/if}
			
			<div style=''>
			
			
		
						
						
						{if $product.cheapnet == 'cheapnet_6'}
			<div class='cheapnet' style="top:20px; left:0px; width:30px; z-index:9">
			<a href='https://www.ezdirect.it/cheapnetcoupons.php'><img src="{$img_ps_dir}cheapnet-300.png" alt="Con questo prodotto hai diritto a un Coupon Cheapnet da 300 minuti gratis" title="Con questo prodotto hai diritto a un Coupon Cheapnet da 300 minuti gratis" width="57" height="57" /></a>
			</div>
			{else if $product.cheapnet == 'cheapnet_30'}
			<div class='cheapnet' style="top:20px; left:0px;  width:30px; z-index:9">
			<a href='https://www.ezdirect.it/cheapnetcoupons.php'><img src="{$img_ps_dir}cheapnet-600.png" alt="Con questo prodotto hai diritto a un Coupon Cheapnet da 600 minuti gratis" title="Con questo prodotto hai diritto a un Coupon Cheapnet da 600 minuti gratis" width="57" height="57" /></a>
			</div>
			{else}
			{/if}			
						<a href="{$product.link}" style="display:block; position:relative" title="{$product.name|escape:html:'UTF-8'}"  class="product_img_link"><img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home')}" {if $product.secondary_image != ''}onmouseover="this.src='{$link->getImageLink($product.link_rewrite, $product.secondary_image, 'home')}'" onmouseout="this.src='{$link->getImageLink($product.link_rewrite, $product.id_image, 'home')}'"{/if} class="classhome" alt="{$product.name|escape:html:'UTF-8'}" /></a>
			</div>
			
			
			
			
			<div style='margin-top:20px'>
			<h5 style='font-size:15px'><a class="product_name_title" style="line-height:22px" href="{$product.link}" title="{$product.name|escape:'htmlall':'UTF-8'}">{$product.name|escape:'htmlall':'UTF-8'|truncate:75:'...'}</a></h5>
			</div>	
			
			<div class='home-medium-category'>
<span><a style='font-weight:normal' href="{$product.macro_link}">{if $product.macrocategory == 'Dispositivi di protezione individuale DPI'}Dispositivi DPI{else}{$product.macrocategory}{/if}</a></span>
			</div>

			
			
<div style="display: flex; justify-content: space-between; align-items: center; ">
			<div class='home-medium-price'>
		{if $product.fuori_produzione == 1}
{else}	
			{if $product.prezzospeciale == 1}
					<span style='font-size:25px; font-weight:bold; color: #ff6600;'>{convertPrice price=$product.special_price}</span>
				{else}
							{if $product.quantity_discount}
						{assign var="xx" value="0"}
						{assign var=numItems value=$product.quantity_discount|@count}
						{foreach from=$product.quantity_discount item='quantity_discount' name='product.quantity_discounts'}
						{if $xx == 0}				
						{l s='From' mod='homefeatured'}
						
						<span style='font-size:25px; font-weight:bold; color: #ff6600;'><strong>{convertPrice price=$quantity_discount.price - ($quantity_discount.price * $quantity_discount.reduction)|floatval}</strong></span> 
						{else}	
						{/if}
						{$xx = $xx+1}
						{/foreach}
						
					{else}
					{if $product.show_price AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}<span style='font-size:25px; font-weight:bold; color: #ff6600;'><strong>{if !$priceDisplay}{if $product.price_tax_exc > 0}{convertPrice price=$product.price_tax_exc}{/if}{else}
					{if $product.price_tax_exc > 0}
					{convertPrice price=$product.price_tax_exc}{/if}
					{/if}
					</strong></span>{else}{/if}
					{/if}
				{/if}
		{/if}	
			</div>
			{if $i==16 || $i==17 || $i==18 || $i==19 || $i==20 || $i==21 || $i==22 || $i==23}
			{else}
			<div class="home-medium-cart-slot">
			
			
									
									
				<a name="{$product.id_product}" href="{$link->getPageLink('cart.php')}?qty=1&amp;id_product={$product.id_product}&amp;token={$static_token}&amp;add" id="ajax_id_product_{$product.id_product}" title="{l s='Add'} {$product.name|escape:'htmlall':'UTF-8'} {l s='to cart'}">
									
					<i class="fas fa-cart-plus" id="cart-plus" style="color: #ff6600; font-size: 26px;"></i>
									
				</a>
			</div>	
				</div>
		{/if}
			
			{if $i==15}
			
			</div><div style='clear:both'></div>
			
			
			<div id="home_center">
				<div id="home_center_1">
					<a href='https://www.ezdirect.it/preventivi-quotazioni-miglior-prezzo-cuffie-centralini' target='_blank'><img style="display:block; float:left; margin-right: 27px" src='{$img_dir}preventivo-gratuito-2.jpg' alt='Preventivo gratuito' title='Preventivo gratuito' /></a>
					<a href='https://www.ezdirect.it/ti-richiamiamo-noi' target='_blank'><img style="display:block; float:left; margin-right: 27px" src='{$img_dir}ti-richiamiamo-noi-2.jpg' alt='Ti richiamiamo noi' title='Ti richiamiamo noi' /></a>
					<a href='https://www.ezdirect.it/guide/65-corporate-account-ezdirect-account-manager-dedicato-per-grandi-aziende-e-pa' target='_blank'><img style="display:block; float:left; margin-right: 27px" src='{$img_dir}corporate-account-2.jpg' alt='Corporate account' title='Corporate account' /></a>
					<a href='https://www.ezdirect.it/guide/69-ezdirect-azienda-accreditata-mepa' target='_blank'><img style="display:block; float:left;" src='{$img_dir}acquisti-in-rete-2.jpg' alt='Acquisti in rete' title='Acquisti in rete' /></a>
					<div style="clear:both"></div>
				</div>
			
			</div>
			
			
			
<script type="text/javascript">
	$(document).ready(function(){
		$(function() {
			$('.banner-manufacturer').unslider({
				speed: 500,               //  The speed to animate each slide (in milliseconds)
				delay: 5000,              //  The delay between slide animations (in milliseconds)
				keys: true,               //  Enable keyboard (left, right) arrow shortcuts
				dots: true,               //  Display dot navigation
			});
		});
	});
	</script>
<h4 class='homepage'>{l s='I nostri marchi' mod='homefeatured'}</h4><br /><br />
	<div id="banner-manufacturers">
		<div class="banner-manufacturer">
			<ul style="height:40px">
			
			
				<li style="width:100% !important">
				<a href='https://www.ezdirect.it/105m-ezdirect/'><img src='https://www.ezdirect.it/img/m/105-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.ezdirect.it/10m-plantronics/'><img src='https://www.ezdirect.it/img/m/10-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.ezdirect.it/12m-panasonic/'><img src='https://www.ezdirect.it/img/m/12-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.ezdirect.it/37m-jabra/'><img src='https://www.ezdirect.it/img/m/37-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.ezdirect.it/17m-gigaset/'><img src='https://www.ezdirect.it/img/m/17-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.ezdirect.it/27m-sennheiser/'><img src='https://www.ezdirect.it/img/m/27-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				</li>			
					
				<li style="width:100% !important">
				<a href='https://www.ezdirect.it/43m-grandstream/'><img src='https://www.ezdirect.it/img/m/43-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.ezdirect.it/47m-plantronics/'><img src='https://www.ezdirect.it/img/m/47-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.ezdirect.it/172m-flir/'><img src='https://www.ezdirect.it/img/m/172-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.ezdirect.it/179m-3cx/'><img src='https://www.ezdirect.it/img/m/37-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.ezdirect.it/117m-yealink/'><img src='https://www.ezdirect.it/img/m/117-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.ezdirect.it/132m-yeastar/'><img src='https://www.ezdirect.it/img/m/132-manufacturerprod.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				</li>	
				
			</ul>
		</div>
</div>
	<br /><br /><h5 style="text-align: center;
color: #009;
font-size: 16px;">I marchi in ordine alfabetico</h5><br />
			
			
			<table style='text-align:left; width:80%; margin:0 auto'>

	<tr><td>
	<a href="{$link->getManufacturerLink('108', '2N')}">2N</a><br />
	
	<a href="{$link->getManufacturerLink('179', '3CX')}">3CX</a><br />
	
	
	
	
	
	<a href="{$link->getManufacturerLink('158', 'Akuvox')}">Akuvox</a><br />
	
		<a href="{$link->getManufacturerLink('124', 'Alcatel')}">Alcatel</a><br />
	
	
	
		
		<a href="{$link->getManufacturerLink('45', 'Alcatel Lucent')}">Alcatel Lucent&nbsp;&nbsp;</a><br />
	
			<a href="{$link->getManufacturerLink('122', 'Ascom')}">Ascom</a><br />
	
	
	
		
			
	<a href="{$link->getManufacturerLink('199', 'Aten')}">Aten</a><br />
	</td><td>
	
	<a href="{$link->getManufacturerLink('47', 'Audiocodes')}">Audiocodes</a><br />
	
	
	
	
		
			<a href="{$link->getManufacturerLink('46', 'Avaya')}">Avaya</a><br />
	
	<a href="{$link->getManufacturerLink('219', 'Barco')}">Barco</a><br />	
	
	
	
	
		<a href="{$link->getManufacturerLink('103', 'Cisco')}">Cisco</a><br />
	
	<a href="{$link->getManufacturerLink('206', 'Dahua')}">Dahua</a><br />
	
	
	
	
		
		<a href="{$link->getManufacturerLink('202', 'Eaton')}">Eaton</a><br />
		
		

	
	
	<a href="{$link->getManufacturerLink('130', 'Engenius')}">Engenius</a><br />
	
	
	</td><td>
		
		<a href="{$link->getManufacturerLink('134', 'Escene')}">Escene</a><br />
	
	<a href="{$link->getManufacturerLink('105', 'Ezdirect')}">Ezdirect</a><br />
	
	
	
	<a href="{$link->getManufacturerLink('133', 'Fanvil')}">Fanvil</a><br />
	
	<a href="{$link->getManufacturerLink('17', 'Gigaset')}">Gigaset</a><br />
	<a href="{$link->getManufacturerLink('43', 'Grandstream')}">Grandstream</a><br />
	
	<a href="{$link->getManufacturerLink('182', 'Hiboost')}">Hiboost</a><br />
	
		
	<a href="{$link->getManufacturerLink('195', 'Huawei')}">Huawei</a><br />
	</td><td>
	<a href="{$link->getManufacturerLink('37', 'Jabra')}">Jabra GN</a><br />
	
	


<a href="{$link->getManufacturerLink('57', 'Konftel')}">Konftel</a><br />


	
	<a href="{$link->getManufacturerLink('50', 'Logitech')}">Logitech</a><br />
	
	
		
	<a href="{$link->getManufacturerLink('106', 'Mitel Aastra')}">Mitel Aastra</a><br />	
	
		<a href="{$link->getManufacturerLink('191', 'Openvox')}">Openvox</a><br />
	
		
	
	<a href="{$link->getManufacturerLink('12', 'Panasonic')}">Panasonic</a><br />
	
		
	<a href="{$link->getManufacturerLink('120', 'Patton')}">Patton</a><br />
	
	
	</td><td>
	<a href="{$link->getManufacturerLink('142', 'Peltor 3M')}">Peltor 3M</a><br />
	
	<a href="{$link->getManufacturerLink('10', 'Plantronics')}">Plantronics</a><br />
	
		
	<a href="{$link->getManufacturerLink('25', 'Polycom')}">Polycom</a><br />	
	
	<a href="{$link->getManufacturerLink('31', 'Samsung')}">Samsung</a><br />
	
	
	<a href="{$link->getManufacturerLink('27', 'Sennheiser')}">Sennheiser</a><br />
	<a href="{$link->getManufacturerLink('111', 'Siemens')}">Siemens</a><br />
	
	
	<a href="{$link->getManufacturerLink('109', 'Snom')}">Snom</a><br />
	</td><td>
	
	<a href="{$link->getManufacturerLink('146', 'Spectralink')}">Spectralink</a><br />
	
	
	
	
	
	<a href="{$link->getManufacturerLink('192', 'Starleaf Italia')}">Starleaf Italia</a><br />
	
	
	<a href="{$link->getManufacturerLink('116', 'Teltonika')}">Teltonika</a><br />
	
	
	
	
	<a href="{$link->getManufacturerLink('136', 'Twig')}">Twig</a><br />
	<a href="{$link->getManufacturerLink('221', 'Univois')}">Univois</a><br />
	
	
	
	<a href="{$link->getManufacturerLink('117', 'Yealink')}">Yealink</a><br />
	
	<a href="{$link->getManufacturerLink('132', 'Yeastar')}">Yeastar</a><br />
	</td></tr>
	</table>
	
			
				<h4 class='homepage'><a href="https://www.ezdirect.it/prodotti-venduti-migliori-sito-web-ezdirect">In evidenza</a></h4><br />
				{else if $i==19}
				</div><div style='clear:both'></div>
					<h4 class='homepage'><a href="https://www.ezdirect.it/nuovi-prodotti-telefoni-fissi-voip-cordless-centralini">{l s='New products' mod='homefeatured'}</a></h4>
				
				{else}
				</div>
				{/if}
				
				
				{else if $i==16 || $i==17 || $i==18 || $i==19}
				{/if}
			{$i = $i+1}
			{/foreach}
			<div style='clear:both'></div>

	{else}
		<p>{l s='No featured products' mod='homefeatured'}</p>
	{/if}
	
	{* <br /><br /><a href='https://www.ezdirect.it/guide/category/5-guide-e-consigli' target='_blank'><h4 class="homepage" style="font-size:25px;">Guide e consigli </h4></a><br />
			
			
			<table style='text-align:left; width:80%; margin:0 auto'>
			
			<tr>
			{foreach from=$cms_3 item='cms' name='cms'}
			
			<td style="vertical-align:top; padding:10px">
			<span style="font-size:11px">Scritto da Ezdirect | <a href='https://www.ezdirect.it/guide/category/5-guide-e-consigli'>Guide e consigli</a></span>
			<br />
			<h5 style="font-size:18px">{$cms.meta_title}</h5>
			<br />
			{$cms.meta_description}
			<br /><br />
			<a href='https://www.ezdirect.it/cms.php?id_cms={$cms.id_cms}'>Leggi di più...</a>
			</td>
			
			{/foreach}
			</tr>
			
			</table><br /><br />
			
			
			
			
			
			<div style="display: wrap;display: flex;flex-wrap: wrap;justify-content: center;">
				<a class="guide_home" href="https://www.ezdirect.it/blog/sistemi-e-dispositivi-per-audioconferenza/" target="_blank">Audioconferenza</a>
				<a class="guide_home" href="https://www.ezdirect.it/file/54405/fanvil%20telefoni%202019-80.pdf" target="_blank">Citofoni VoIP</a>
			
				<a class="guide_home" href="https://www.ezdirect.it/guide/25-cuffie-guida-alla-scelta-telefoniche-con-microfono" title="Cuffie telefoniche">Cuffie bluetooth e con cavo</a>

							
							<a class="guide_home" href="https://www.ezdirect.it/centralini-telefonici/3201-centralino-telefonico-ez308-pbx-3-linee-disa-fax.html">Centralino telefonico</a>


				<a class="guide_home" href="https://www.ezdirect.it/centralini-telefonici/">Centralino VoIP</a>


				<a class="guide_home" href="https://www.ezdirect.it/centralino-virtuale/">Centralino virtuale</a>

				<a class="guide_home" href="https://www.ezdirect.it/guide/76-centralino-virtuale-come-funziona" title="Come funziona Ezcloud">Come funziona Ezcloud</a>	
				<a class="guide_home" href="https://www.ezdirect.it/guide/77-configurazione-telefoni-e-softphone-centralino-virtuale-ezcloud" title="Configurazioni Ezcloud">Configurazioni Ezcloud</a>	
				<a class="guide_home" href="https://www.ezdirect.it/blog/radioguide-whisper-per-gruppi-e-visite-guidate/" title="Radioguide">Radioguide</a>	

				<a class="guide_home" href="https://www.ezdirect.it/guide/70-ripetitori-di-segnale-gsm-umts-lte-come-funzionano" title="Ripetitori di segnale GSM UMTS LTE">Ripetitori GSM UMTS LTE</a>	

				<a class="guide_home" href="https://www.ezdirect.it/telefoni-fissi/">Telefoni VoIP</a>



				<a class="guide_home" href="https://www.ezdirect.it/file/54405/fanvil%20telefoni%202019-80.pdf">Telefoni Fanvil</a>



				<a class="guide_home" href="https://www.ezdirect.it/videoconferenza-voip-ip-cloud/" title="Videoconferenza Cloud">Videoconferenza Cloud</a>


				<a class="guide_home" href="https://www.ezdirect.it/blog/videoconferenza-professionale-videocamere-sistemi-app/">Videoconferenza</a>



				<a class="guide_home" href="https://www.ezdirect.it/file/54403/yeastar-serie%20s-2019.pdf">Yeastar guide</a><br /><br />
			</div>
</div>
</div> *}

<script type="text/javascript">
	/*$(document).ready(function(){
		$(function() {
			$('.banner-parlano').unslider({
				speed: 500,               //  The speed to animate each slide (in milliseconds)
				delay: 5000,              //  The delay between slide animations (in milliseconds)
				keys: true,               //  Enable keyboard (left, right) arrow shortcuts
				dots: true,               //  Display dot navigation
			});
		});
	});*/
	</script>
	

	<div id="banner-parlano">
		<h4 class='homepage' style="margin-bottom: 0;">{l s='Parlano di noi' mod='homefeatured'}</h4><br /><br />
		<div class="banner-parlano">
			<ul style="height:40px">
			
			
				<li style="width:100% !important">
				<a href='https://www.notizie.it/economia/2019/10/01/ezdirect-vendita-online-soluzioni-per-comunicare/'><img src='https://www.ezdirect.it/themes/ez20/img/parlano/notizie.jpg' width='150' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.datamanager.it/2021/01/ezdirect-le-soluzioni-per-comunicare/'><img src='https://www.ezdirect.it/themes/ez20/img/parlano/datamanager.jpg' width='150' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.punto-informatico.it/ezdirect-soluzioni-per-la-unified-communication/'><img src='https://www.ezdirect.it/themes/ez20/img/parlano/puntoinformatico.jpg' width='150' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.adnkronos.com/ezdirect-la-risposta-professionale-alle-esigenze-di-soluzioni-per-comunicare-delle-imprese_1oRbAnwIjAeMOM9mIbBjRf?refresh_ce'><img src='https://www.ezdirect.it/themes/ez20/img/parlano/adnkronos.jpg' width='150' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.affaritaliani.it/comunicati/notiziario/ezdirect_la_risposta_%E2%80%9Cprofessionale%E2%80%9D_alle_esigenze_di_soluzioni_per_comunicare_delle_imprese-86213.html?refresh_cens'><img width='150' src='https://www.ezdirect.it/themes/ez20/img/parlano/affariitaliani.jpg' alt='Ezdirect' title='Ezdirect' /></a>
				<a href='https://www.techcompany360.it/distributori/le-competenze-ezdirect-sempre-in-linea-con-le-esigenze-dei-clienti/'><img src='https://www.ezdirect.it/themes/ez20/img/parlano/techcompany360.jpg' width='150' alt='Ezdirect' title='Ezdirect' /></a>
				</li>			
					
			</ul><br /><br /><br />
		</div>
</div>

<!-- /MODULE Home Featured Products -->
