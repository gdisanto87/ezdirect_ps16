{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}




<tr id="product_{$product.id_product}_{$product.id_product_attribute}" class="{if $smarty.foreach.productLoop.last}last_item{elseif $smarty.foreach.productLoop.first}first_item{/if}{if isset($customizedDatas.$productId.$productAttributeId) AND $quantityDisplayed == 0}alternate_item{/if} cart_item">
	<td class="cart_product" style=" border-right:0px">
		{if $product.active == 1}<a href="{$link->getProductLink($product.id_product, $product.link_rewrite, $product.category)|escape:'htmlall':'UTF-8'}">{else}{/if}<img style="border:0px" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'medium')}" alt="{$product.name|escape:'htmlall':'UTF-8'}" {if isset($mediumSize)}width="{$mediumSize.width}" height="{$mediumSize.height}" {/if} />{if $product.active == 1}</a>{else}{/if}
	</td>

	
	<td class="cart_description" style="border-left:0px; width:264px; display:flex; flex-direction: column;">
		<div class="cart_ref" style="color: #9e9e9e; font-size: 12px;">{if $product.reference}{l s='Cod : '}{$product.reference|escape:'htmlall':'UTF-8'}{else}--{/if}</div>
		<h5 style="font-weight:600; font-size: 16px;">{if $product.active == 1}<a href="{$link->getProductLink($product.id_product, $product.link_rewrite, $product.category)|escape:'htmlall':'UTF-8'}" style="font-size:16px; color:#000099">{else}<span style="font-size:16px; color:#000099">{/if}{$product.name|escape:'htmlall':'UTF-8'}{if $product.active == 1}</a>{else}</span>{/if}</h5>
		
		<span style="font-size: 12px;">{$product.description_short}</span>
		{if isset($product.attributes) && $product.attributes}<br /><br />{if $product.active == 1}<a href="{$link->getProductLink($product.id_product, $product.link_rewrite, $product.category)|escape:'htmlall':'UTF-8'}">{else}{/if}{$product.attributes|escape:'htmlall':'UTF-8'}{if $product.active == 1}</a>{else}{/if}{/if}
		{if $smarty.now|date_format:"%Y-%m-%d" < $product.date_available}
		<br />
		{l s='This product will be available from'} {$product.date_available|date_format:"%d-%m-%Y"}
		{else}
		{/if}
	</td>
	
	
	
	
	{* <td class="cart_unit">
		<span class="price" id="product_price_{$product.id_product}_{$product.id_product_attribute}">
			{if !$priceDisplay}{convertPrice price=$product.price_wt}{else}{convertPrice price=$product.price}{/if}
		</span>
	</td> *}
	{* <td>
	
			</td> *}
	
	<td class="cart_quantity" style="text-align: center; vertical-align:center"{if isset($customizedDatas.$productId.$productAttributeId) AND $quantityDisplayed == 0} {/if}>
		{if isset($customizedDatas.$productId.$productAttributeId) AND $quantityDisplayed == 0}<span id="cart_quantity_custom_{$product.id_product}_{$product.id_product_attribute}" >{$product.customizationQuantityTotal}</span>{/if}
		{if !isset($customizedDatas.$productId.$productAttributeId) OR $quantityDisplayed > 0}
		{* <div style="position:relative; width:55px; display:block; margin:0 auto; {if $preventivo == 1}text-align:right{/if}">
			{if $preventivo == 1} <strong>{$product.cart_quantity-$quantityDisplayed}</strong> {/if}
			<div id="cart_quantity_button" style="float:left; margin-top:-5px">

			</div>
			<div style="float: left; margin-top:0px">
			{if $preventivo == 1}
			
			{else}<input type="hidden" value="{if $quantityDisplayed == 0 AND isset($customizedDatas.$productId.$productAttributeId)}{$customizedDatas.$productId.$productAttributeId|@count}{else}{$product.cart_quantity-$quantityDisplayed}{/if}" name="quantity_{$product.id_product}_{$product.id_product_attribute}_hidden" />
				<div class="quantity_input_div">
				
					<input size="2" {if $product.id_product == 31110 || $product.id_product == 31109  || $product.reference == 'OMAGGIO' || ( $product.cart_free == 1)} readonly="readonly" 
					{else}{/if}  class="cart_quantity_input quantity_input" type="text" value="{if $quantityDisplayed == 0 AND isset($customizedDatas.$productId.$productAttributeId)}{$customizedDatas.$productId.$productAttributeId|@count}{else}{$product.cart_quantity-$quantityDisplayed}{/if}"  name="quantity_{$product.id_product}_{$product.id_product_attribute}"  id="quantity_{$product.id_product}_{$product.id_product_attribute}" style="margin-top:0px; margin-right:0px" />{/if}
					
					{if $product.id_product == 31110 || $product.id_product == 31109 || $product.reference == 'OMAGGIO' || ( $product.cart_free == 1)}
					{else}
						{if $preventivo == 1}
						{else}
							<div class="quantity_input_buttons">
								<span class="cart_quantity_span span_up cart_quantity_up" id="cart_quantity_up_{$product.id_product}_{$product.id_product_attribute}" onclick="quantity_cart_add('quantity_{$product.id_product}_{$product.id_product_attribute}'); ">+</span>
								<span class="cart_quantity_span span_down cart_quantity_down" id="cart_quantity_down_{$product.id_product}_{$product.id_product_attribute}" onclick="quantity_cart_rmv('quantity_{$product.id_product}_{$product.id_product_attribute}'); ">-</span>
							</div>
						{/if}
					{/if}
				</div>
			
			</div>
			<div style="clear:both"></div>
		</div> *}

		<div class="quantity-div" style="width: 100%; display: flex; justify-content: space-between; padding-left:6px; align-items:center;">
									<div style="float:left; padding-top:4px; text-transform:uppercase; font-size:10px; width:100%;">{l s='Qtà : '}
									</div>
								
									
									<div class="quantity_input_div" style="display:flex; border: 1px solid #f4f4f4; padding: 6px 0px 6px 12px;">
										<div class="quantity_input_buttons" style="border:none;">
											
											<span class="span_down"  onclick="quantity_cart_rmv('quantity_wanted_{$product.id_product|intval}');">-</span>
										</div>
										<input type="text" style="border:none; padding:0px 10px;" name="ajax_qty_to_add_to_cart[{$product.id_product|intval}]" id="quantity_wanted_{$product.id_product|intval}" class="text quantity_input" value="{if $quantityDisplayed == 0 AND isset($customizedDatas.$productId.$productAttributeId)}{$customizedDatas.$productId.$productAttributeId|@count}{else}{$product.cart_quantity-$quantityDisplayed}{/if}" size="2" maxlength="3" />
										<div class="quantity_input_buttons"  style="border:none;">
											<span id="cart_quantity_up_{$product.id_product}_{$product.id_product_attribute}"  class="span_up cart_quantity_span cart_quantity_up" onclick="quantity_cart_add('quantity_wanted_{$product.id_product|intval}');">+</span>
											
										</div>
									</div>
									<div style="clear:both"></div>
								</div>
		{/if}
	</td>
	<td class="cart_total" style="padding-right:5px;">

		<div class="cart_availability" style="padding-right: 30px; padding-bottom: 20px;">
			{if $product.active AND $product.quantity_def > 0 AND !$PS_CATALOG_MODE}
				<img src="{$img_dir}green_alert_2.jpg" alt="{l s='Available'}" />
			{else}
				<img style='width:13px; margin-bottom: 3px; margin-right: 3px;' src='{$img_dir}orange_alert_2.jpg'  alt='{l s='Ordinabile'}' />
			{/if}
		</div>

		<div style="text-align:center">
			<span class="price" id="total_product_price_{$product.id_product}_{$product.id_product_attribute}">
				{if $quantityDisplayed == 0 AND isset($customizedDatas.$productId.$productAttributeId)}
					{if !$priceDisplay}<span style="font-size: 17px; font-weight:600; color: #333333;">{displayPrice price=$product.total_customization_wt}</span><span style="font-size: 11px; color:#545454; padding-left: 10px; font-weight:100;">{l s='I.V.A. esclusa'}</span>{else}<span style="font-size: 17px; font-weight:600; color: #333333;">{displayPrice price=$product.total_customization}</span><span style="font-size: 11px; color:#545454; padding-left: 10px; font-weight:100;">{l s='I.V.A. esclusa'}</span>{/if}
				{else}
					{if !$priceDisplay}<span style="font-size: 17px; font-weight:600; color: #333333;">{displayPrice price=$product.total_wt}</span><span style="font-size: 11px; color:#545454; padding-left: 10px;  font-weight:100;">{l s='I.V.A. esclusa'}</span>{else}<span style="font-size: 17px; font-weight:600; color: #333333;">{displayPrice price=$product.total}</span><span style="font-size: 11px; color:#545454; padding-left: 10px; font-weight:100;">{l s='I.V.A. esclusa'}</span>{/if}
				{/if}
			</span>
		</div>
		<div style="text-align:center; font-size: 12px; padding-top: 30px;">
			{if $preventivo == 1}{else}<a rel="nofollow" style="color:#545454; text-decoration:none;" class="cart_quantity_delete_edit" id="{$product.id_product}_{$product.id_product_attribute}" href="{$link->getPageLink('cart.php', true)}&delete&amp;id_product={$product.id_product|intval}&amp;ipa={$product.id_product_attribute|intval}&amp;token={$token_cart}" title="{l s='Delete'}"><i style="color:#545454;" class="fas fa-trash-alt"></i> Rimuovi dal carrello</a>
				
				{* <a rel="nofollow" href="order-opc.php" title="{l s='Refresh Cart'}"><img src="{$img_dir}icon/refresh2.gif" alt="{l s='Refresh'}" class="icon" height="18" width="18" /></a> *}
			{/if}
		</div>
		
	</td>
</tr>
