{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA

*}

<!--Bootstrap 3.4-->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


{if $id_cms_category != 7}

		{if !$content_only}
				</div>

<!-- Right -->
				
			</div>

<!-- Footer -->
			<div style="clear:both"></div>
		</div>
	{/if}
	

</div> <!-- contenitore -->

<div id="footer">


{if $page_name == 'index'}

<!-- Block Newsletter module-->
{if isset($msg) && $msg}
		
		<div style="z-index:2147483647" id="{if $nw_error}error_newsletter{else}success_newsletter{/if}">
		<p class="{if $nw_error}warning_inline{else}success_inline{/if}"><a name="newsletter-success" id="newsletter-success">{$msg}</a></p>
		</div>
		
		<script type="text/javascript">
		{if $nw_error}
		{literal}
		$(document).ready(function () {
			$('#error_newsletter').delay(7000).fadeOut('slow');
			$('#error_newsletter').css('zIndex', '2147483647');
		});
		{/literal}
		{else}
		{literal}
		$(document).ready(function () {
			$('#success_newsletter').delay(7000).fadeOut('slow');
			$('#success_newsletter').css('zIndex', '2147483647');
		});
		{/literal}
		{/if}
		</script>
	{/if}
	
<div id="newsletter_block_left">
	<div class="container" id="newsletter_block_left_in">
		{if isset($msg) && $msg}
			<p class="{if $nw_error}warning_inline{else}success_inline{/if}"><a name="newsletter-success" id="newsletter-success">{$msg}</a></p>
		{/if}

		
			<form action="https://www.ezdirect.it/index.php#newsletter-success" method="post">

				
				
				<div  style='text-align:left; line-height:25px; color:#4d4b4b; '>
					<div style='font-size:22px; text-align:center; padding-bottom:10px; '> Newsletter <i class="fas fa-envelope-open-text"></i></div>
					
					{* <p style="font-weight:100; text-align:center; padding-bottom:10px;"> Iscriviti alla newsletter per rimanere aggiornato sulle nostre novit&agrave;. </p> *}
					
					<div class="row" style="display:flex; justify-content: end;">
						<div class="col-sm-6" style="align-self:center;"><input type="text" name="email" style="width:100%; color: #bbbbbb; background-color:transparent; border: none; border-bottom: 1px solid #ff6600;" value="{if isset($value) && $value}{$value}{else}{l s='Inserisci la tua e-mail' mod='blocknewsletter'}{/if}" onfocus="javascript:if(this.value=='{l s='Inserisci la tua e-mail' mod='blocknewsletter'}')this.value='';" onblur="javascript:if(this.value=='')this.value='{l s='Inserisci la tua e-mail' mod='blocknewsletter'}';" /><br /> </div>
						
						
					
						<div class="col-sm-4"> <button type="submit" class="button" style="width:50%; padding:7px; border:none; line-height:normal; background-color:#ff6600; color: white;" name="submitNewsletter">Iscriviti </button></div>
					
					</div>
					<div style="clear:both"></div>
					<div class="row" style="display: flex; justify-content: center; padding-top: 15px; align-items:center;">
						<input type="hidden" name="action" value="0" /><input type="checkbox" name="privacy_newsletter" />
						<a href="#" onclick="window.open('{$link->getPageLink('privacy.php', true)}')" style="text-decoration:none; color:#4d4b4b; font-size: 12px; padding-left:5px;">Acconsento al trattamento dei dati</a><br />
					</div>
					
						
					
					
				</div>

				
			</form>
		
			
		
	</div>
</div>

<!-- /Block Newsletter module-->

{/if}

<!--prova I nostri marchi -->
{* <div>
	<h2 style="color:#000099; text-align:center; padding: 30px 0; font-size:18px;">I nostri migliori marchi</h2>

	<div id="myCarousel" class="carousel slide" data-ride="carousel" style="height:100px; ">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner">
					<div class="item active">
						<div class="container">
							<div class="row col-sm-12" style="display: flex; align-items: center;">
								<div class="col-sm-2">
									<img src="{$img_ps_dir}2n.png" alt="Banner">
								</div>

								<div class="col-sm-2">
									<img src="{$img_ps_dir}yeastar.png" alt="Banner">
								</div>

								<div class="col-sm-2">
									<img src="{$img_ps_dir}3cx.png" alt="Banner">
								</div>

								<div class="col-sm-2">
									<img src="{$img_ps_dir}jabra.png" alt="Banner">
								</div>

								<div class="col-sm-2">
									<img src="{$img_ps_dir}openvox.jpeg" alt="Banner">
								</div>

								<div class="col-sm-2">
									<img src="{$img_ps_dir}ezdirect.png" alt="Banner">
								</div>
							</div>
						</div>
					</div>

					<div class="item">
					<div class="container">
							<div class="row col-sm-12" style="display: flex; align-items: center;">
								<div class="col-sm-2">
									<img src="{$img_ps_dir}patton.png" alt="Banner">
								</div>

								<div class="col-sm-2">
									<img src="{$img_ps_dir}ascom.png" alt="Banner">
								</div>

								<div class="col-sm-2">
									<img src="{$img_ps_dir}cisco.png" alt="Banner">
								</div>

								<div class="col-sm-2">
									<img src="{$img_ps_dir}fanvil.png" alt="Banner">
								</div>

								<div class="col-sm-2">
									<img src="{$img_ps_dir}gigaset.png" alt="Banner">
								</div>

								<div class="col-sm-2">
									<img src="{$img_ps_dir}snom.png" alt="Banner">
								</div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="container">
							<div class="row col-sm-12" style="display: flex; align-items: center;">
								<div class="col-sm-2">
									<img src="{$img_ps_dir}samsung.png" alt="Banner">
								</div>

								<div class="col-sm-2">
									<img src="{$img_ps_dir}panasonic.png" alt="Banner">
								</div>

								<div class="col-sm-2">
									<img src="{$img_ps_dir}dahua.png" alt="Banner">
								</div>

								<div class="col-sm-2">
									<img src="{$img_ps_dir}yealink.png" alt="Banner">
								</div>

								<div class="col-sm-2">
									<img src="{$img_ps_dir}hiboost.png" alt="Banner">
								</div>

								<div class="col-sm-2">
									<img src="{$img_ps_dir}grandstream.png" alt="Banner">
								</div>
							</div>
						</div>
					</div>

					<div class="item">
						<div class="container">
							<div class="row col-sm-12" style="display: flex; align-items: center;">
								<div class="col-sm-2">
									<img src="{$img_ps_dir}dinstar.png" alt="Banner">
								</div>

								<div class="col-sm-2">
									<img src="{$img_ps_dir}lucky-tone.jpeg" alt="Banner">
								</div>

								<div class="col-sm-2">
									<img src="{$img_ps_dir}avaya.png" alt="Banner">
								</div>

								<div class="col-sm-2">
									<img src="{$img_ps_dir}alcatel.png" alt="Banner">
								</div>

								<div class="col-sm-2">
									<img src="{$img_ps_dir}epos.png" alt="Banner">
								</div>

								<div class="col-sm-2">
									<img src="{$img_ps_dir}poly.png" alt="Banner">
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Left and right controls -->
				<a class="left carousel-control" href="#myCarousel" data-slide="prev" style="background-color:transparent;">
					<span class="glyphicon glyphicon-chevron-left" style="color:#4d4b4b;"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#myCarousel" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" style="color:#4d4b4b;"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
</div> *}



<!-- End prova I nostri marchi -->




<div id="footer-in">

{* {if $page_name == 'index'} 
		
		
		{$HOOK_FOOTER}
		
		{else}		
	
		{/if}	 *}
	
	
<br />
{if $page_name == 'index'}
		
		
			<p style='text-align:center; background-color:#ffffff; padding:3px; '>
		{l s='I nostri prodotti in ordine alfabetico'}<br />
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_a/" >A</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_b/" >B</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_c/" >C</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_d/" >D</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_e/" >E</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_f/" >F</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_g/" >G</a> - 
		H - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_i/" >I</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_j/" >J</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_k/" >K</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_l/" >L</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_m/" >M</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_n/" >N</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_o/" >O</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_p_1/" >PA-PK</a> - 
		<a style='padding:2px;' href="https://www.ezdirect.it/lettera_p_2/" >PL-PZ</a> - 
		Q - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_r/" >R</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_s/" >S</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_t/" >T</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_u/" >U</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_v/" >V</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_w/" >W</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_x/" >X</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_y/" >Y</a> - 
		<a style="padding:2px;" href="https://www.ezdirect.it/lettera_z/" >Z</a> - 
		</p>
			<br />

			{* GUIDE E CONSIGLI *}
	<h4 class="homepage" style="font-size:16px; color: darkgrey; margin-top:0; "><i class="fas fa-hand-point-right"></i><a id="guide-e-consigli" href='https://www.ezdirect.it/guide/category/5-guide-e-consigli' target='_blank'>Guide e consigli all'acquisto</a><i class="fas fa-hand-point-left"></i></h4>

{/if}

			<hr class="footer_hr" />
			<br />
			<div style="width: 100%; position:relative">



	{* <td class="tdfooter" style="font-size:12px">
  
		<img src='https://www.ezdirect.it/img/logo.jpg' width='140' alt='Ezdirect' title='Ezdirect' />
              <br /><br />
			  &copy; Ezdirect Srl a socio unico<br />
			  Via Nerino Garbuio (SS1 Aurelia) Snc<br />
			  54038, Montignoso, Massa Carrara<br />
			  P.iva CCIAA 01165670455, REA 118272<br />
			  capitale sociale 80.000 &euro;<br />
			  (interamente versato),<br />
			  SDI: USAL8PV<br /><br />
			  
			  </td>
	
	*}



 <table id="footer-table-blocks" style="table-layout:fixed;">
  <tbody><tr style="display: flex;" id="footer-tab-responsive">

	<div class="container">
		<td class="tdfooter">
			<h3>Azienda</h3>
			<ul class="menufooter">
				<li><a href="{$link->getCMSLink2(4, $link->getCMSRewrite(4))}" title="Chi siamo">Chi siamo</a></li>
				<li><a href="{$link->getCMSLink2(39, $link->getCMSRewrite(39))}" title="Contatti">Contatti</a></li>
				<li><a href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}" title="Condizioni">Condizioni</a></li>
				<li><a target="_blank" href="https://www.ezdirect.it/sitemap.php" title="Sitemap">Sitemap</a></li>
				<li><a target="_blank" href="https://www.ezdirect.it/guide/6-informativa-sulla-privacy" title="Sitemap">Privacy</a></li>
				<li><a target="_blank" href="https://www.ezdirect.it/ti-richiamiamo-noi" title="Sitemap">Consulente dedicato</a></li>
				<li><a target="_blank" href="https://www.ezdirect.it/guide/69-ezdirect-azienda-accreditata-mepa" title="Sitemap">Acquisti in rete</a></li>
   			</ul>
		</td>
    	<td class="tdfooter">
			<h3>Pagamenti</h3>
			<ul class="menufooter">
				<li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#contrassegno" title="Contrassegno">Contrassegno</a></li>
				<li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#bonifico" title="Bonifico all'ordine">Bonifico all'ordine</a></li>
				<li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#paypal" title="Paypal">Paypal</a></li>
				<li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#noleggio" title="Noleggio Leasing">Noleggio Leasing</a></li>
				<li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#prezzi" title="Prezzi">Prezzi</a></li>
    		</ul>
		</td>
    	<td class="tdfooter">
			<h3 style="width:100%;">Ordini e consegne</h3>
				<ul class="menufooter">
					<li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#consegne" title="Spedizione">Spedizione</a></li>
					<li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#consegne" title="Modalita di consegna">Modalit&agrave;&nbsp;di consegna</a></li>
					<li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#consegne" title="Spese di consegna">Spese di consegna</a></li>
					<li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#consegne" title="Fatturazione">Fatturazione</a></li>
					<li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#consegne" title="Fatturazione">RMA + Diritto di recesso</a></li>
					
				</ul>
		</td>
    	<td class="tdfooter">
			<div style="">
				<h3>Servizi</h3>
	
				<ul class="menufooter">
					<li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#supporto" title="Supporto pre vendita">Supporto pre vendita</a></li>
					<li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#assistenza" title="Assistenza tecnica">Assistenza tecnica</a></li>
					<li><a rel="nofollow" href="{$link->getCMSLink2(3, $link->getCMSRewrite(3))}#rma" title="Riparazioni">Riparazioni</a></li>
					<li><a href="https://www.ezdirect.it/preventivi-quotazioni-miglior-prezzo-cuffie-centralini" title="Preventivi">Preventivi</a></li>
					<li><a rel="nofollow" href="{$link->getPageLink('authentication.php', true)}?cliente=rivenditore">Rivenditori</a></li>
				</ul>
			</div>
		</td>
    </div>
	 	
		 
	<td class="tdfooter" style="vertical-align:middle; padding-top: 50px;">
	
   
       
	   <!-- eKomiWidget START -->
<div id="eKomiWidget_default"></div>
<!-- eKomiWidget END -->

<!-- eKomiLoader START, only needed once per page -->
<script type="text/javascript"> 
{literal}
	(function(){
		eKomiIntegrationConfig = new Array(
			{certId:'44D97E5AAD9DE69'}
		);
		if(typeof eKomiIntegrationConfig != "undefined"){for(var eKomiIntegrationLoop=0;eKomiIntegrationLoop<eKomiIntegrationConfig.length;eKomiIntegrationLoop++){
			var eKomiIntegrationContainer = document.createElement('script');
			eKomiIntegrationContainer.type = 'text/javascript'; eKomiIntegrationContainer.defer = true;
			eKomiIntegrationContainer.src = (document.location.protocol=='https:'?'https:':'http:') +"//connect.ekomi.de/integration_1609180206/" + eKomiIntegrationConfig[eKomiIntegrationLoop].certId + ".js";
			document.getElementsByTagName("head")[0].appendChild(eKomiIntegrationContainer);
		}}else{if('console' in window){ console.error('connectEkomiIntegration - Cannot read eKomiIntegrationConfig'); }}
	})();
{/literal}
</script>
<!-- eKomiLoader END, only needed once per page -->

	   
	   </td>
	
  </tr>
</tbody></table>
<br />
			<hr class="footer_hr" />
			<br />

	<div style="padding-bottom: 20px;">
		<div class="row" style="display: flex; align-items: center; flex-wrap: wrap; justify-content: center;">
			<div class="col-sm-6">
				<div id="carte-ssl">
	
				<img  src='{$img_dir}carte-footer-new.png' alt='Carte' title='Carte' />

				<img  style="height: 28px;" src='{$img_dir}gls-logo.png' alt='Corriere GLS' title='Corriere GLS' />

				<span style=" height:40px;"><img id="pagamento-ssl" style="height:100%" src='{$img_ps_dir}pagamento-sicuro.jpeg' alt='Pagamento sicuro' title='Pagamento sicuro' /></span>
	
				</div>
			</div>

			<div class="col-sm-3">
				<div id="servizio-clienti" style="text-align:end;">
						
				{if isset($smarty.get.id_cms) && $smarty.get.id_cms == 37}
				{else}
				<a href="tel:+39-0585821163">
								
					<img src="{$img_dir}esperto-risponde.jpg" alt="{l s='Servizio clienti'}" style="border:0px; margin-bottom: 5px;" title="{l s='Servizio clienti'} 0585 821163" /> 
								
				</a>
				{/if}
				</div>
			</div>

			<div class="col-sm-3">
				<div style="display: flex; align-items: stretch; justify-content: center; height: 38px;">
	
					
	
					<a href='https://it-it.facebook.com/pages/Ezdirect-srl/53805768063' target='_blank' title='Facebook' rel='nofollow'><i style="    color: #f47a3c; font-size: 23px; padding: 7px;"  class="fab fa-facebook"></i></a>
					<a href='https://twitter.com/ezdirect_it' target='_blank'  title='Twitter' rel='nofollow'><i style="    color: #f47a3c; font-size: 23px; padding: 7px;" class="fab fa-twitter"></i></a>
					<a href='http://www.linkedin.com/company/ezdirect-srl' target='_blank'  title='Linkedin' rel='nofollow'><i style="    color: #f47a3c; font-size: 23px; padding: 7px;" class="fab fa-linkedin"></i></a>
					<a href='https://www.youtube.com/user/ezdirectit' target='_blank'  title='Youtube' rel='nofollow'><i style="    color: #f47a3c; font-size: 23px; padding: 7px;" class="fab fa-youtube"></i></a>
				</div>
			</div>
		</div>
	</div>
	

	
	
	{* <div style="clear:both"></div> *}
</div>


</div>
			
</div>	<!-- fine footer -->

<div id="footer-2">
	<div id="footer-2-in">
			
			<div id="footer-bottom">
				
				<div>
					<p style="font-size: 13px; font-family: arial; margin:0;">&copy; Ezdirect srl. P. IVA CCIAA 01164670455 - Via Nerino Garbuio snc, 54038, Montignoso, MS - Tel. 0585 821163 - SDI: USAL8PV<br />
					{* Le informazioni riportate su www.ezdirect.it possono essere soggette a modifiche senza preavviso | I marchi e i loghi in questo sito sono di propriet&agrave; dei legittimi proprietari.

					<br />
					&Egrave; vietata la riproduzione anche parziale dei contenuti e della grafica del sito senza autorizzazione.
					<br /> *}
					 
					</p><br />
				</div>

			</div>
			<div style="clear:both"></div>
		</div>	
	</div>	
			
			
			
	
	
<!--	{$footer_mobile} -->
	
	<a href="#0" class="to-top" title="Back To Top"><div class="back-to-top-arrow-up"></div></a>
	
	
		
	</body>
</html>

{else}
{include file="$tpl_dir./landing_footer.tpl"}
{/if}

