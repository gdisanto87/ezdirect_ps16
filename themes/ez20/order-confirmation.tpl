{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='Order confirmation'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<h1>{l s='Order confirmation'}</h1>

{assign var='current_step' value='payment'}


{include file="$tpl_dir./errors.tpl"}



<div >
<div style="vertical-align:top">
<h2 style="text-align:left; font-weight:normal; font-size:28px; font-weight: bold; text-align: center;">{l s='Thanks for your order on Ezdirect'}</h2>
<br />
<div style="text-align: center;">
<span style="color: #ef6018;  font-size:18px; width: 260px; ">
{l s='Your order ID is:'} <span style="font-weight: bold; font-size: 20px; color: #000099;">{$order.id_order}</span>
</span>
</div>
<br />




</div>
<br />

{$HOOK_ORDER_CONFIRMATION}
{$HOOK_PAYMENT_RETURN}




<br />
{if $is_guest}
	<p>{l s='Your order ID is:'} <span class="bold">{$id_order_formatted}</span> . {l s='Your order ID has been sent to your e-mail.'}</p>
	<a href="{$link->getPageLink('guest-tracking.php', true)}?id_order={$id_order}" title="{l s='Follow my order'}"><img src="{$img_dir}icon/order.gif" alt="{l s='Follow my order'}"  width="22" height="22" class="icon" /></a>
	<a href="{$link->getPageLink('guest-tracking.php', true)}?id_order={$id_order}" title="{l s='Follow my order'}">{l s='Follow my order'}</a>
{else}
	<a href="{$link->getPageLink('history.php', true)}" title="{l s='Back to orders'}"><img src="{$img_dir}icon/order.gif"  width="22" height="22" alt="{l s='Back to orders'}" class="icon" /></a>
	<a href="{$link->getPageLink('history.php', true)}" title="{l s='Back to orders'}">{l s='Back to orders'}</a>
{/if}

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1058514372;
var google_conversion_language = "it";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "Ec95CPTB7gEQxMve-AM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1058514372/?value=0&amp;label=Ec95CPTB7gEQxMve-AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


<!-- Become Sales Tracking Script V 1.0.0 - All rights reserved -->

<script type="text/javascript" language="JavaScript">

  var pg_pangora_merchant_id='23843';

var pg_order_id='$order_id';

var pg_cart_size='$cart_size';

var pg_cart_value='$cart_value';

var pg_currency='EUR';

var pg_customer_flag='$customer_flag';

var pg_product_id='$product_id';

var pg_product_name='$product_name';

var pg_product_price='$product_price';

var pg_product_units='$product_units';

</script>

<script type="text/javascript" language="JavaScript" src="https://clicks.pangora.com/sales-tracking/salesTracker.js">

</script>

<noscript>

<img src="https://clicks.pangora.com/sales-tracking/23843/salesPixel.do"/>

</noscript>


<script type="text/javascript">
					var _gaq = _gaq || [];
					_gaq.push(['_setAccount', 'UA-19658774-1']);
					// Recommanded value by Google doc and has to before the trackPageView
					_gaq.push(['_setSiteSpeedSampleRate', 5]);

					_gaq.push(['_trackPageview', 'orderconfirmation']);
					
					  _gaq.push(['_addTrans',
						'{$shop_transaction_id}',		
						'Ezdirect',		
						'{$amount}',		
						'0',			
						'0',	
						'x',	
					    '',		
						'x'		
					  ]);

						
					{foreach from=$items item=item}
		_gaq.push(['_addItem',
		'{$item.OrderId}',		{* order ID - required		*}
		'{$item.SKU}',			{* SKU/code - required		*}
		'{$item.Product}',		{* product name				*}
		'{$item.Category}',		{* category or variation	*}
		'{$item.Price}',		{* unit price - required	*}
		'{$item.Quantity}'		{* quantity - required		*}
		]);
	{/foreach}
					
					{literal}
					  _gaq.push(['_trackTrans']);	
					{/literal}
				
					{literal}
					(function() {
						var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
						ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
						var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
					})(); {/literal}
					</script>
					
					<script type="text/javascript">
						{literal} fbq('track', 'Purchase', {value: 0.00, currency: 'EUR'}); {/literal}
					</script>