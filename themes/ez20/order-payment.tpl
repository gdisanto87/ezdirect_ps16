{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if !$opc}
	<script type="text/javascript">
	// <![CDATA[
	var currencySign = '{$currencySign|html_entity_decode:2:"UTF-8"}';
	var currencyRate = '{$currencyRate|floatval}';
	var currencyFormat = '{$currencyFormat|intval}';
	var currencyBlank = '{$currencyBlank|intval}';
	var txtProduct = "{l s='product'}";
	var txtProducts = "{l s='products'}";
	// ]]>
	</script>

	{capture name=path}{l s='Your payment method'}{/capture}
	{include file="$tpl_dir./breadcrumb.tpl"}
{/if}

{if !$opc}<h1>{l s='Choose your payment method'}</h1>{else}<h2>3. {l s='Choose your payment method'}</h2>{/if}

{if !$opc}
	{assign var='current_step' value='payment'}
	{include file="$tpl_dir./errors.tpl"}
	{include file="$tpl_dir./order-steps.tpl"}
<div class="shopping_cart_table">
	
{else}

	<div id="opc_payment_methods" class="opc-main-block">
		<div id="opc_payment_methods-overlay" class="opc-overlay" style="display: none;"></div>
{/if}

{if $cfcheck == 0}
{l s='There are some details missing in your account: if you are a end user customer you must specify your tax code in your account details, and if you are a company you must specify both tax code and vat number'}. <a href="{$link->getPageLink('my-account.php', true)}">{l s='Click here to go to your account and update your profile details'}</a><br /><br />
{l s='Here there are your missing details'}:<br /><br />
{if $check_tx == 1} <a href="{$link->getPageLink('identity.php', true)}" target="_blank">{l s='Tax code'}</a>{else}{/if}
{if $check_vat == 1} <a href="{$link->getPageLink('identity.php', true)}" target="_blank">{l s='VAT number'}</a>{else}{/if}
{if $check_phone == 1} <a href="{$link->getPageLink('addresses.php', true)}" target="_blank">{l s='Phone number'}</a>{else}{/if}
{else}
<div  id="HOOK_TOP_PAYMENT">{$HOOK_TOP_PAYMENT}</div>

<div id="address-form-short">
{if $HOOK_PAYMENT}
	{if !$opc}<h4>{l s='Please select your preferred payment method to pay the amount of'}&nbsp;<span class="price">{convertPrice price=$total_price}</span> {if $taxes_enabled}{l s='(tax incl.)'}{/if}</h4><br />{/if}
	{if $opc}<div id="opc_payment_methods-content">{/if}
		<div style="" id="metodi-pagamento" id="HOOK_PAYMENT">{$HOOK_PAYMENT}</div>
	{if $opc}</div>{/if}
{else}
	<p class="warning">{l s='No payment modules have been installed.'}</p>
{/if}

{if !$opc}
<div>
	<p class="cart_navigation"><a href="{$link->getPageLink('order.php', true)}?step=2" title="{l s='Previous'}"  class="button_large" style="padding: 8px 20px; height:auto;">&laquo; {l s='Previous'}</a></p>
</div>
{else}
	</div>
	
{/if}
{/if}
<br /><br />
</div>


</div>

<br />
<div style="border: 1px solid #e5e8eb; 
			box-shadow: 0 1px 1px rgba(0,0,0,0.15), 
        	0 2px 2px rgba(0,0,0,0.15), 
            0 4px 4px rgba(0,0,0,0.15), 
        	0 8px 8px rgba(0,0,0,0.15);
			border-radius:12px;
			padding:30px; 
			display: flex;
			width: 100%;" id="need-help">
		<div style="height: 140px; padding-right: 30px;">
			<img style="height:100%;" src='{$img_ps_dir}business-woman.jpg' alt='Aiuto' title='Aiuto' />
		</div>
		<div>
			<h5 style="font-size: 22px; font-weight: 600;">Hai bisogno di aiuto con il tuo ordine?</h5>
			<div style="display:flex; align-items:center;" id="numero-verde"><p>Chiamaci al numero verde</p><a href="#" style="padding-left: 12px; padding-bottom: 10px;"><img style="height:50px;" src='{$img_ps_dir}numero-verde.jpg' alt='Numero verde' /></a></div>
			
			<p>Oppure apri un ticket cliccando <a target='_blank' href='https://www.ezdirect.it/contattaci?step=assistenza-ordini'>qui</a>. Un esperto di risponderà quanto prima.</p>
		</div>
</div>

<p class="clear"></p>