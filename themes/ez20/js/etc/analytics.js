 var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-19658774-1']);
		  _gaq.push (['_gat._anonymizeIp']);
		  _gaq.push(['_trackPageview']);

		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		  
		  
		  
		  $(document).ready(function(){
// Scroll (in pixels) after which the "To Top" link is shown
var offset = 300,
//Scroll (in pixels) after which the "back to top" link opacity is reduced
offset_opacity = 1200,
//Duration of the top scrolling animation (in ms)
scroll_top_duration = 700,
//Get the "To Top" link
$back_to_top = $('.to-top');
 
//Visible or not "To Top" link
$(window).scroll(function(){
( $(this).scrollTop() > offset ) ? $back_to_top.addClass('top-is-visible') : $back_to_top.removeClass('top-is-visible top-fade-out');
if( $(this).scrollTop() > offset_opacity ) {
//$back_to_top.addClass('top-fade-out');
}
});
 
//Smoothy scroll to top
$back_to_top.bind('click', function(event){
event.preventDefault();
$('body,html').animate({
scrollTop: 0 ,
}, scroll_top_duration
);
});

	$('#center_column div img').each(function(){

		if($(this).width() > 250){

			$(this).addClass('img_lrg');

		}

	});
 
}); 