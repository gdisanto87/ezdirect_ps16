
$(document).ready(function(){

$.fn.stars = function() {
    return $(this).each(function() {
	
        // Get the value
        var val = parseFloat($(this).html());
        // Make sure that the value is in 0 - 5 range, multiply to get width
        var size = Math.max(0, (Math.min(5, val))) * 16;
        // Create stars holder
        var $span = $('<span />').width(size);
        // Replace the numerical value with stars
        $(this).html($span);
    });
}

$('span.stars').stars();

  $(".ajax_add_to_cart_button").click(function(){
 var f=$(this).attr('href');
var id = f.split('&amp;')[1].split('=')[1];
var p="#quantity_wanted_"+id;
var qty=$(p).attr('value');
g=f+"&amp;qty="+qty;
window.location=''+g;
return false;
  });
});