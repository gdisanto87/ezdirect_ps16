$(document).ready(function() {
	var changeTooltipPosition = function(event) {
	  var tooltipX = event.pageX - 8;
	  var tooltipY = event.pageY + 8;
	  $('div.tooltip').css({top: tooltipY, left: tooltipX});
	};
	var tooltip_content = '';
	
 
	
	var showTooltip = function(event) {
	  $('div.tooltip').remove();
	  $.ajax({
	type: 'POST',
	data: 'id='+$(this).attr('id')+'&qty='+$(this).attr('value'),
	cache: false,
	async: false,
	url: 'https://www.ezdirect.it/bundle_quantity_preview.php?timestamp=' + new Date().getTime(),
	success: function(resp)
		{
			tooltip_content = resp;						
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			tooltip_content = '';
			alert('ERROR' + textStatus + ' - ' + XMLHttpRequest.responseText);					
		}
								
	});
	 
	  $("<div class='tooltip'>"+tooltip_content+"</div>")
            .appendTo('body');
	  changeTooltipPosition(event);
	};
	
	var hideTooltip = function() {
	   $('div.tooltip').remove();
	};
 
	$('input.quantity_bundle').bind({
	   mousemove : changeTooltipPosition,
	   mouseenter : showTooltip,
	   mouseleave: hideTooltip
	});


});