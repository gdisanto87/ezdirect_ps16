/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

function ps_round(value, precision)
{
	if (typeof(roundMode) == 'undefined')
		roundMode = 2;
	if (typeof(precision) == 'undefined')
		precision = 2;
	
	method = roundMode;
	if (method == 0)
		return ceilf(value, precision);
	else if (method == 1)
		return floorf(value, precision);
	precisionFactor = precision == 0 ? 1 : Math.pow(10, precision);
	return Math.round(value * precisionFactor) / precisionFactor;
}

function ceilf(value, precision)
{
	if (typeof(precision) == 'undefined')
		precision = 0;
	precisionFactor = precision == 0 ? 1 : Math.pow(10, precision);
	tmp = value * precisionFactor;
	tmp2 = tmp.toString();
	if (tmp2[tmp2.length - 1] == 0)
		return value;
	return Math.ceil(value * precisionFactor) / precisionFactor;
}

function floorf(value, precision)
{
	if (typeof(precision) == 'undefined')
		precision = 0;
	precisionFactor = precision == 0 ? 1 : Math.pow(10, precision);
	tmp = value * precisionFactor;
	tmp2 = tmp.toString();
	if (tmp2[tmp2.length - 1] == 0)
		return value;
	return Math.floor(value * precisionFactor) / precisionFactor;
}

function formatedNumberToFloat(price, currencyFormat, currencySign)
{
	price = price.replace(currencySign, '');
	if (currencyFormat == 1)
		return parseFloat(price.replace(',', '').replace(' ', ''));
	else if (currencyFormat == 2)
		return parseFloat(price.replace(' ', '').replace(',', '.'));
	else if (currencyFormat == 3)
		return parseFloat(price.replace('.', '').replace(' ', '').replace(',', '.'));
	else if (currencyFormat == 4)
		return parseFloat(price.replace(',', '').replace(' ', ''));
	return price;
}

//return a formatted price
function formatCurrency(price, currencyFormat, currencySign, currencyBlank)
{
	// if you modified this function, don't forget to modify the PHP function displayPrice (in the Tools.php class)
	blank = '';
	price = parseFloat(price);
	price = ps_round(price, priceDisplayPrecision);
	if (currencyBlank > 0)
		blank = ' ';
	if (currencyFormat == 1)
		return currencySign + blank + formatNumber(price, priceDisplayPrecision, ',', '.');
	if (currencyFormat == 2)
		return (formatNumber(price, priceDisplayPrecision, ' ', ',') + blank + currencySign);
	if (currencyFormat == 3)
		return (currencySign + blank + formatNumber(price, priceDisplayPrecision, '.', ','));
	if (currencyFormat == 4)
		return (formatNumber(price, priceDisplayPrecision, ',', '.') + blank + currencySign);
	return price;
}

//return a formatted number
function formatNumber(value, numberOfDecimal, thousenSeparator, virgule)
{
	value = value.toFixed(numberOfDecimal);
	var val_string = value+'';
	var tmp = val_string.split('.');
	var abs_val_string = (tmp.length == 2) ? tmp[0] : val_string;
	var deci_string = ('0.' + (tmp.length == 2 ? tmp[1] : 0)).substr(2);
	var nb = abs_val_string.length;

	for (var i = 1 ; i < 4; i++)
		if (value >= Math.pow(10, (3 * i)))
			abs_val_string = abs_val_string.substring(0, nb - (3 * i)) + thousenSeparator + abs_val_string.substring(nb - (3 * i));

	if (parseInt(numberOfDecimal) == 0)
		return abs_val_string;
	return abs_val_string + virgule + (deci_string > 0 ? deci_string : '00');
}

//change the text of a jQuery element with a sliding effect (velocity could be a number in ms, 'slow' or 'fast', effect1 and effect2 could be slide, fade, hide, show)
function updateTextWithEffect(jQueryElement, text, velocity, effect1, effect2, newClass)
{
	if(jQueryElement.text() != text)
		if(effect1 == 'fade')
			jQueryElement.fadeOut(velocity, function(){
				$(this).addClass(newClass);
				if(effect2 == 'fade') $(this).text(text).fadeIn(velocity);
				else if(effect2 == 'slide') $(this).text(text).slideDown(velocity);
					else if(effect2 == 'show')	$(this).text(text).show(velocity, function(){});
			});
		else if(effect1 == 'slide')
			jQueryElement.slideUp(velocity, function(){
				$(this).addClass(newClass);
				if(effect2 == 'fade') $(this).text(text).fadeIn(velocity);
				else if(effect2 == 'slide') $(this).text(text).slideDown(velocity);
					else if(effect2 == 'show')	$(this).text(text).show(velocity);
			});
			else if(effect1 == 'hide')
				jQueryElement.hide(velocity, function(){
					$(this).addClass(newClass);
					if(effect2 == 'fade') $(this).text(text).fadeIn(velocity);
					else if(effect2 == 'slide') $(this).text(text).slideDown(velocity);
						else if(effect2 == 'show')	$(this).text(text).show(velocity);
				});
}

//show a JS debug
function dbg(value)
{
	var active = false;//true for active
	var firefox = true;//true if debug under firefox

	if (active)
		if (firefox)
			console.log(value);
		else
			alert(value);
}

/**
* Function : print_r()
* Arguments: The data  - array,hash(associative array),object
*            The level - OPTIONAL
* Returns  : The textual representation of the array.
* This function was inspired by the print_r function of PHP.
* This will accept some data as the argument and return a
* text that will be a more readable version of the
* array/hash/object that is given.
*/
function print_r(arr, level)
{
	var dumped_text = "";
	if (!level)
		level = 0;

	//The padding given at the beginning of the line.
	var level_padding = "";
	for (var j = 0 ; j < level + 1; j++)
		level_padding += "    ";

	if (typeof(arr) == 'object')
	{ //Array/Hashes/Objects 
		for (var item in arr)
		{
			var value = arr[item];
			if (typeof(value) == 'object') { //If it is an array,
				dumped_text += level_padding + "'" + item + "' ...\n";
				dumped_text += dump(value,level+1);
			}
			else
			{
				dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
			}
		}
	}
	else
	{ //Stings/Chars/Numbers etc.
		dumped_text = "===>" + arr + "<===("+typeof(arr)+")";
	}
	return dumped_text;
}

//verify if value is in the array
function in_array(value, array)
{
	for (var i in array)
		if (array[i] == value)
			return true;
	return false;
}

function resizeAddressesBox(nameBox)
{
	maxHeight = 0;

	if (typeof(nameBox) == 'undefined')
		nameBox = '.address';
	$(nameBox).each(function()
	{
		$(this).css('height', 'auto');
		currentHeight = $(this).height();
		if (maxHeight < currentHeight)
			maxHeight = currentHeight;
		
	});
	//$(nameBox).height(maxHeight);
}

function countryChange()
{
	
	var country_select = document.getElementById('id_country');
	
	var country = country_select.options[country_select.selectedIndex].value;
	if(country == 10)
	{
		
		$('.ee_address').hide();
		$('.it_address').show();
		$('#city').attr('name', 'city1');
		$('#postcode').attr('name', 'postcode1');
		$('#id_state').attr('name', 'id_state1');
		
		$('#citta').attr('name', 'city');
		$('#cap').attr('name', 'postcode');
		$('#provincia_hidden').attr('name', 'id_state');
		
	}
	else
	{
		$('.it_address').hide();
		$('.ee_address').show();
		
		$('#city').attr('name', 'city');
		$('#postcode').attr('name', 'postcode');
		$('#id_state').attr('name', 'id_state');
		
		$('#citta').attr('name', 'city1');
		$('#cap').attr('name', 'postcode1');
		$('#provincia_hidden').attr('name', 'id_state1');
	}
}


function cercaCitta() {
	var this_cap = $('#cap').val();
	$.ajax({
	  url:"/prestashop16/cap.php?cercacitta=y",
	  type: "POST",
	  data: { 
	  cap: this_cap
	  },
	  success:function(r){
		 if(r != '')
		 {
			$( "#citta" ).html( r );
			console.log(r);
			cercaProvincia();
		 }	
	  },
	  error: function(xhr,stato,errori){
	 alert("Errore:"+xhr.status);
	  }
	});

}

function cercaProvincia() {
	var this_citta = $('#citta option:selected').attr('rel');
	$.ajax({
	  url:"/prestashop16/cap.php?cercaprovincia=y",
	  type: "POST",
	  data: { 
	  citta: this_citta
	  },
	  success:function(r){
		  if(r != '')
		  {
			  var prv = r.split(':::');
			$( "#provincia" ).val(prv[0]);
			$( "#provincia_hidden" ).val(prv[1]);
			
		}
	  },
	  error: function(xhr,stato,errori){
	 alert("Errore:"+xhr.status);
	  }
	});

}


function countryChange2()
{
	var country_select = document.getElementById('id_country2');
	var country = country_select.options[country_select.selectedIndex].value;
	if(country == 10)
	{
		$('.ee_address').hide();
		$('.it_address').show();
		
		$('#city2').attr('name', 'city2');
		$('#postcode2').attr('name', 'postcode2');
		$('#id_state2').attr('name', 'id_state2');
		
		$('#citta2').attr('name', 'city');
		$('#cap2').attr('name', 'postcode');
		$('#provincia_hidden2').attr('name', 'id_state');
		
	}
	else
	{
		$('.it_address').hide();
		$('.ee_address').show();
		
		$('#city2').attr('name', 'city');
		$('#postcode2').attr('name', 'postcode');
		$('#id_state2').attr('name', 'id_state');
		
		$('#citta2').attr('name', 'city2');
		$('#cap2').attr('name', 'postcode2');
		$('#provincia_hidden2').attr('name', 'id_state2');
	}
}

function cercaCitta2() {
	var this_cap = $('#cap2').val();
	$.ajax({
	  url:"/prestashop16/cap.php?cercacitta=y",
	  type: "POST",
	  data: { 
	  cap: this_cap
	  },
	  success:function(r){
		 if(r != '')
		 {
			$( "#citta2" ).html( r );
			console.log(r);
			cercaProvincia2();
		 }	
	  },
	  error: function(xhr,stato,errori){
	 alert("Errore:"+xhr.status);
	  }
	});

}

function cercaProvincia2() {
	var this_citta = $('#citta2 option:selected').attr('rel');
	$.ajax({
	  url:"/prestashop16/cap.php?cercaprovincia=y",
	  type: "POST",
	  data: { 
	  citta: this_citta
	  },
	  success:function(r){
		  if(r != '')
		  {
			  var prv = r.split(':::');
			$( "#provincia2" ).val(prv[0]);
			$( "#provincia_hidden2" ).val(prv[1]);
			
		}
	  },
	  error: function(xhr,stato,errori){
	 alert("Errore:"+xhr.status);
	  }
	});

}


function countryChange_invoice()
{
	var country_select = document.getElementById('id_country_invoice');
	var country = country_select.options[country_select.selectedIndex].value;
	if(country == 10)
	{
		$('.ee_address_invoice').hide();
		$('.it_address_invoice').show();
		
		$('#city_invoice').attr('name', 'city1');
		$('#postcode_invoice').attr('name', 'postcode1');
		$('#id_state_invoice').attr('name', 'id_state1');
		
		$('#citta_invoice').attr('name', 'city_invoice');
		$('#cap_invoice').attr('name', 'postcode_invoice');
		$('#provincia_hidden_invoice').attr('name', 'id_state_invoice');
		
	}
	else
	{
		$('.it_address_invoice').hide();
		$('.ee_address_invoice').show();
		
		$('#city_invoice').attr('name', 'city_invoice');
		$('#postcode_invoice').attr('name', 'postcode_invoice');
		$('#id_state_invoice').attr('name', 'id_state_invoice');
		
		$('#citta_invoice').attr('name', 'city1');
		$('#cap_invoice').attr('name', 'postcode1');
		$('#provincia_hidden_invoice').attr('name', 'id_state1');
	}
}


function cercaCitta_invoice() {
	var this_cap = $('#cap_invoice').val();
	$.ajax({
	  url:"/prestashop16/cap.php?cercacitta=y",
	  type: "POST",
	  data: { 
	  cap: this_cap
	  },
	  success:function(r){
		 if(r != '')
		 {
			$( "#citta_invoice" ).html( r );
			console.log(r);
			cercaProvincia_invoice();
		 }	
	  },
	  error: function(xhr,stato,errori){
	 alert("Errore:"+xhr.status);
	  }
	});

}

function cercaProvincia_invoice() {
	var this_citta = $('#citta_invoice option:selected').attr('rel');
	$.ajax({
	  url:"/prestashop16/cap.php?cercaprovincia=y",
	  type: "POST",
	  data: { 
	  citta: this_citta
	  },
	  success:function(r){
		  if(r != '')
		  {
			  var prv = r.split(':::');
			$( "#provincia_invoice" ).val(prv[0]);
			$( "#provincia_hidden_invoice" ).val(prv[1]);
			
		}
	  },
	  error: function(xhr,stato,errori){
	 alert("Errore:"+xhr.status);
	  }
	});

}

