{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{include file="$tpl_dir./breadcrumb.tpl"}
{include file="$tpl_dir./errors.tpl"}

{if $products}
{assign var='i' value=0}
{assign var='maiuscola' value=$smarty.get.letter|upper}
{if $maiuscola == 'P_1'}
{$maiuscola = 'PA-PK'}
{else if $maiuscola == 'P_2'}
{$maiuscola = 'PL-PZ'}
{else}
{/if}

<h1>Prodotti per la lettera {$maiuscola}</h1>
<table>
<tr>
{foreach from=$products item=product name=products}
	{if $i % 3 === 0}
		</tr><tr>
	{/if}
	<td style="border:1px solid #d4d4d4; vertical-align:top; padding:3px; margin-right:8px"><a href="{$product.link|escape:'htmlall':'UTF-8'}" style="" title="{$product.name|escape:'htmlall':'UTF-8'}">{$product.name}</a></td>

	{assign var='i' value=$i+1}
	  
{/foreach}
</tr>
</table>

{/if}
<br /><br />

		<div><div>
