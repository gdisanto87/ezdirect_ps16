{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='My account'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}
<h1>{l s='Grazie!'}</h1>


{if $smarty.get.ordine == 'si'}
{assign var=current_step value='thanks'} 

{include file="$tpl_dir./order-steps.tpl"}
<div class="shopping_cart_table">
{else}
<div>
{/if}


<br />
<strong>&nbsp;{l s='Grazie per esserti registrato sul nostro sito'}</strong>. 

<br /><br />
{if $smarty.get.cliente == 'rivenditore'}

Ti ricordiamo che per ottenere l'abilitazione come rivenditore, devi inviarci una visura camerale (non antecedente 3 mesi) utilizzando il form che trovi <a href='https://www.ezdirect.it/contattaci?step=rivenditori'>cliccando su questo link</a>. Allega la tua visura camerale selezionando il file nel campo "Allega file".<br /><br />
Riceverai quindi un messaggio di posta elettronica per conferma abilitazione profilo rivenditore (salvo verifica della visura, che dovr&agrave; espressamente indicare le categorie merceologiche e /o l'attivit&agrave; sociale compatibile con l'attivit&agrave; di impiantistica, installazione, rivendita di sistemi, software, e prodotti informatici, TLC, forniture per ufficio etc).<br /><br />


{else if $smarty.get.ordine == 'si'}

&nbsp;<a href='ordine?step=1'>{l s='Clicca qui per procedere con il tuo ordine'}.</a>
<br /><br />

{else}

<a href='my-account.php'>{l s='Clicca qui per entrare nel tuo account'}!</a>

{/if}

</div>

<!-- Google Code for Registrazione Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1058514372;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "666666";
var google_conversion_label = "HwxGCIa-zAEQxMve-AM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="https://www.googleadservices.com/pagead/conversion/1058514372/?value=0&amp;label=HwxGCIa-zAEQxMve-AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<script type="text/javascript">
	{literal} fbq('track', 'CompleteRegistration', {value: 0.00, currency: 'EUR'}); {/literal}
</script>



