{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14008 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if isset($orderby) AND isset($orderway)}
<!-- Sort products -->
{if isset($smarty.get.id_category) && $smarty.get.id_category}
	{assign var='request' value=$link->getPaginationLink('category', $category, false, true)}
{elseif isset($smarty.get.id_manufacturer) && $smarty.get.id_manufacturer}
	{assign var='request' value=$link->getPaginationLink('manufacturer', $manufacturer, false, true)}
{elseif isset($smarty.get.id_supplier) && $smarty.get.id_supplier}
	{assign var='request' value=$link->getPaginationLink('supplier', $supplier, false, true)}
{else}
	{assign var='request' value=$link->getPaginationLink(false, false, false, true)}
{/if}
<script type="text/javascript">


//<![CDATA[
{literal}
$(document).ready(function()
{

	


	$('#selectPrductSort').change(function()
	{
		var requestSortProducts = '{/literal}{$request}{literal}';
		var splitData = $(this).val().split(':');
		document.location.href = requestSortProducts + ((requestSortProducts.indexOf('?') < 0) ? '?' : '&') + 'orderby=' + splitData[0] + '&orderway=' + splitData[1];
	});
	
	$('#nameasc').bind('click', function() {
		$('#nameascord').attr("selected", "selected"); 
		$('#nameascord').trigger('change');
	});

		$('#namedesc').bind('click', function() {
		$('#namedescord').attr("selected", "selected"); 
		$('#namedescord').trigger('change');
	});
	
		$('#priceasc').bind('click', function() {
		$('#priceascord').attr("selected", "selected"); 
		$('#priceascord').trigger('change');
	});
	
		$('#pricedesc').bind('click', function() {
		$('#pricedescord').attr("selected", "selected"); 
		$('#pricedescord').trigger('change');
	});
	
		$('#dateadddesc').bind('click', function() {
		$('#dateadddescord').attr("selected", "selected"); 
		$('#dateadddescord').trigger('change');
	});
	
		$('#dateadddescimg').bind('click', function() {
		$('#dateadddescord').attr("selected", "selected"); 
		$('#dateadddescord').trigger('change');
	});
	
		$('#vendutidesc').bind('click', function() {
		$('#vendutidescord').attr("selected", "selected"); 
		$('#vendutidescord').trigger('change');
	});
	
		$('#vendutidescimg').bind('click', function() {
		$('#vendutidescord').attr("selected", "selected"); 
		$('#vendutidescord').trigger('change');
	});
	
		$('#disponibilidescimg').bind('click', function() {
		$('#disponibilidescord').attr("selected", "selected"); 
		$('#disponibilidescord').trigger('change');
	});
	
		$('#disponibilidesc').bind('click', function() {
		$('#disponibilidescord').attr("selected", "selected"); 
		$('#disponibilidescord').trigger('change');
	});
});
//]]>

{/literal}
</script>


<div id="ordinamento-prodotti">
	<div style="display:none">
	<div style="float:left" id="the_products">
	<img {if $smarty.get.orderby}{else}{/if} src="{$img_dir}icon/alfabetico.png" style="float:left; margin-right:3px" width="26" height="26" alt="{l s='Sort by name'}" title="{l s='Sort by name'}" />
	<img src="{$img_dir}icon/sort_asc.gif" id="nameasc" class="ordinamentoimg" alt="{l s='Sort by name from A to Z'}" height="9" width="9" title="{l s='Sort by name from A to Z'}" /><br /><br />
	<img src="{$img_dir}icon/sort_desc.gif" class="ordinamentoimg" id="namedesc" style="display:block; margin-top:-15px" height="9" width="9" alt="{l s='Sort by name from Z to A'}" title="{l s='Sort by name from Z to A'}" />
	</div>

	<div style="display:block; float:left; margin-left:40px">
	<img src="{$img_dir}icon/euro.png" style="float:left; margin-right:3px" alt="{l s='Sort by price'}" height="26" width="26"  title="{l s='Sort by price'}" />
	<img src="{$img_dir}icon/sort_asc.gif" class="ordinamentoimg" id="priceasc" alt="{l s='Sort by price from less to more expensive'}" height="9" width="9" title="{l s='Sort by price from less to more expensive'}" /><br /><br />
	<img src="{$img_dir}icon/sort_desc.gif" class="ordinamentoimg" id="pricedesc" style="display:block; margin-top:-15px" height="9" width="9" alt="{l s='Sort by price from more to less expensive'}" title="{l s='Sort by price from more to less expensive'}" />
	</div>

	<div style="display:block; float:left; margin-left:40px">
	<img src="{$img_dir}icon/clock.png" style="display:block; float:left; margin-right:3px; cursor:pointer" id="dateadddescimg" height="26" width="26" alt="{l s='New products'}" title="{l s='New products'}" />
	<span style="display:block; float:left;  width:50px; margin-top:-2px"><a id="dateadddesc" style="cursor:pointer">Nuovi arrivi</a></span>
	</div>

	<div style="display:block; float:left; margin-left:40px">
	<img src="{$img_dir}icon/chart.png" style="display:block; float:left; margin-right:3px; cursor:pointer" height="26" width="26" id="vendutidescimg" alt="{l s='Best sellers'}" title="{l s='Best sellers'}" />
	<span style="display:block;float:left; margin-top:-2px; width:50px"><a id="vendutidesc" style="cursor:pointer">I pi&ugrave; venduti</a></span>
	</div>
	
	
	</div>
	<form id="productsSortForm" name="formordinamento" action="{$request|escape:'htmlall':'UTF-8'}">
		<fieldset>
			<select name="selectordinamento" id="selectPrductSort" style="color: #737373">
				<option value="posizione:asc" {if $orderby eq $orderbydefault}selected="selected"{/if}>{l s='--'}</option>
				{if !$PS_CATALOG_MODE}
					<option id="priceascord" value="price:asc" {if $orderby eq 'price' AND $orderway eq 'asc'}selected="selected"{/if}>{l s='Price: lowest first'}</option>
					<option id="pricedescord" value="price:desc" {if $orderby eq 'price' AND $orderway eq 'desc'}selected="selected"{/if}>{l s='Price: highest first'}</option>
				{/if}
				<option id="nameascord" value="name:asc" {if $orderby eq 'name' AND $orderway eq 'asc'}selected="selected"{/if}>{l s='Product Name: A to Z'}</option>
				<option id="namedescord" value="name:desc" {if $orderby eq 'name' AND $orderway eq 'desc'}selected="selected"{/if}>{l s='Product Name: Z to A'}</option>
				{if !$PS_CATALOG_MODE}
					<option id="dateadddescord" value="date_add:desc" {if $orderby eq 'date_add' AND $orderway eq 'desc'}selected="selected"{/if}>{l s='Date added'}</option>
					<option id="vendutidescord" value="venduti:desc" {if $orderby eq 'venduti' AND $orderway eq 'desc'}selected="selected"{/if}>{l s='Best sellers'}</option>
					<option id="disponibilidescord" value="disponibili:desc" {if $orderby eq 'disponibili' AND $orderway eq 'desc'}selected="selected"{/if}>{l s='In stock'}</option>
				{/if}
			</select>
			<label for="selectPrductSort" style="color: #737373; font-weight: 400; padding-right: 10px;">{l s='Ordina per'}</label>
		</fieldset>
	</form>


</div>

<!-- /Sort products -->
{/if}
